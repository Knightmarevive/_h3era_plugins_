﻿// dllmain.cpp : Definiuje punkt wejścia dla aplikacji DLL.
// #include "pch.h"

#define _CRT_SECURE_NO_WARNINGS
#define SS_variable_types_count 8
#define o_ActivePlayerID *(int*)0x69CCF4

enum new_skills {
    Pathfinding = 0,
    Archery = 1,
    Scouting = 3,
    Nobility = 4,
    Astromancy = 5,
    Inspiration = 6,
    Life_Magic = 9,
    Engineering = 10,
    Scavenging = 11,
    Dark_Magic = 12,
    Estates = 13,
    Monasticism = 18,
    Tactics = 19,
    Recruitment = 20,
    Learning = 21,
    Offence = 22,
    Sorcery = 25,
    Machinery = 27,
    Damned = 28,
    Exorcism = 29,
    Ancient_Magic = 30,
};


#include <windows.h>
#include "../__include__/Era.h"
#include <stdio.h>

#include "../__include__/patcher_x86_commented.hpp"
#include"../__include__/H3API/single_header/H3API.hpp"

Patcher* globalPatcher;
PatcherInstance* SS_KK;
#define PINSTANCE_MAIN "SS_KK"

using namespace Era;
using namespace h3;


int   (*get_Hero_SSI_with_bonuses)(H3Hero* hero, int skill);
float (*get_Hero_SSF_with_bonuses)(H3Hero* hero, int skill);


int   (*get_Hero_zSSI_with_bonuses)(H3Hero* hero, int skill, int type);
float (*get_Hero_zSSF_with_bonuses)(H3Hero* hero, int skill, int type);
void  (*ChangeCreatureTable)(int target, char* buf);
h3::H3Hero* (__cdecl* GetAzylumHero)(h3::H3Town*);

int(__fastcall* Random)(int Low, int High) = (int(__fastcall*)(int Low, int High)) 0x0050C7C0;

inline int Good_or_Evil(H3CombatCreature* mon) {
    int town_from = mon->info.town;
    if (town_from >= 0 && town_from <= 2) return 1;
    if (town_from >= 3 && town_from <= 5) return -1;
    return 0;
}

inline int SkillSpecialist(H3Hero* hero, int skill) {
    int hero_ID = hero->id;
    auto spec = &H3HeroSpecialty::Get()[hero_ID];
    if (spec->type == 0 && spec->bonusId == skill) {
        return hero->level;
    }
    return 0;
}

void __stdcall Alignment_OnStackToStackDamage(TEvent* e) {
    // if (x[9]) return;

    auto batman = H3CombatManager::Get(); 
    // auto AHero = &batman->hero[0];
    // auto DHero = &batman->hero[1];
    auto AStack = &batman->stacks[0][x[1]];
    auto DStack = &batman->stacks[0][x[2]];

    // sprintf(z[3], "debug %s attacks %s", AStack->GetCreatureName(), DStack->GetCreatureName() ); ExecErmCmd("BU:Mz3");

    auto myhero = AStack->GetOwner(); if (!myhero) return;
    // if (Good_or_Evil(DStack) == 0) return;
    int& FINAL_DAMAGE_CONST = x[3];
    int& FINAL_DAMAGE = x[4];
    long long bonus = 0;
    if (Good_or_Evil(DStack) < 0) {
        bonus += get_Hero_zSSI_with_bonuses(myhero, Exorcism, 0);
        bonus *= 100 + get_Hero_zSSI_with_bonuses(myhero, Exorcism, 1);
        bonus *= 100 + 5 * SkillSpecialist(myhero, Exorcism);
        bonus *= FINAL_DAMAGE;
        bonus /= 1000000;
        if(bonus && !x[9])
        {
            sprintf(z[3], "Exorcism damage bonus: %i", bonus);
            ExecErmCmd("BU:Mz3");
        }
    }
    else if (Good_or_Evil(DStack) > 0)
    {
        bonus += get_Hero_zSSI_with_bonuses(myhero, Damned, 0);
        bonus *= 100 + get_Hero_zSSI_with_bonuses(myhero, Damned, 1);
        bonus *= 100 + 5 * SkillSpecialist(myhero, Damned);
        bonus *= FINAL_DAMAGE;
        bonus /= 1000000;
        if (bonus && !x[9])
        {
            sprintf(z[3], "Damnation damage bonus: %i", bonus);
            ExecErmCmd("BU:Mz3");
        }
    }
    FINAL_DAMAGE += bonus;

}

int tactics_usage[42];
int melee_usage[42];
int shot_usage[42];
void __stdcall Tactics_Archery_Offense(TEvent* e) {
    auto batman = H3CombatManager::Get();
    auto AStack = batman->activeStack; if(!AStack) return;
    auto myhero = AStack->GetOwner(); if (!myhero) return;
    auto stackID = AStack->Index();

    int shot_chance = get_Hero_zSSI_with_bonuses(myhero, Archery, 0);
    shot_chance += SkillSpecialist(myhero, Archery) * 3;
    int max_shot = get_Hero_zSSI_with_bonuses(myhero, Archery, 1);

    while (Random(0, 999) < shot_chance && shot_usage[stackID]<max_shot) {
        ChangeCreatureTable(-1-stackID,(char*)"AdditionalShot+=1");
        --(shot_usage[stackID]);
    } shot_usage[stackID] = 0;


    int melee_chance = get_Hero_zSSI_with_bonuses(myhero, Offence, 0);
    melee_chance += SkillSpecialist(myhero, Offence) * 3;
    int max_melee = get_Hero_zSSI_with_bonuses(myhero, Offence, 1);

    while (Random(0, 999) < melee_chance && melee_usage[stackID] < max_melee) {
        ChangeCreatureTable(-1 - stackID, (char*)"AdditionalMelee+=1");
        --(melee_usage[stackID]);
    } melee_usage[stackID] = 0;


    int tactics_chance = get_Hero_zSSI_with_bonuses(myhero, Tactics, 0);
    tactics_chance += SkillSpecialist(myhero, Tactics) * 3;
    int max_tactics = get_Hero_zSSI_with_bonuses(myhero, Tactics, 1);

    if (Random(0, 999) < tactics_chance && tactics_usage[stackID] < max_tactics) {
        ChangeCreatureTable(-1 - stackID, (char*)"AdditionalMove+=1");
    } if (tactics_usage[stackID]) --(tactics_usage[stackID]);

}


void __stdcall init_battle_round(TEvent* e) {
    for (int i = 0; i < 42; ++i) {
        tactics_usage[i] = 0;
        melee_usage[i] = 0;
        shot_usage[i] = 0;
    }
}

void __stdcall link_more_SS_levels(TEvent* e) {
    HMODULE more_SS_levels = 0;

    more_SS_levels = GetModuleHandleA("more_SS_levels.era");
    if (more_SS_levels) {
        get_Hero_zSSI_with_bonuses = (int(*)(H3Hero*, int, int))   GetProcAddress(more_SS_levels, "get_Hero_zSSI_with_bonuses");
        get_Hero_zSSF_with_bonuses = (float(*)(H3Hero*, int, int)) GetProcAddress(more_SS_levels, "get_Hero_zSSF_with_bonuses");;

        get_Hero_SSI_with_bonuses = (int(*)(H3Hero*, int))   GetProcAddress(more_SS_levels, "get_Hero_SSI_with_bonuses");
        get_Hero_SSF_with_bonuses = (float(*)(H3Hero*, int)) GetProcAddress(more_SS_levels, "get_Hero_SSF_with_bonuses");;

        if(!(get_Hero_zSSI_with_bonuses && get_Hero_zSSF_with_bonuses))
            MessageBoxA(0, "more_SS_levels.era not matches", "Knightmare Skills Plugin", MB_OK);
    }
    else { MessageBoxA(0, "couldn't link to more_SS_levels", "Knightmare Skills Plugin", MB_OK); }


    HMODULE amethyst = 0;

    amethyst = GetModuleHandleA("amethyst2_4.dll");
    if (amethyst) {
        ChangeCreatureTable = (void(*)(int, char*)) GetProcAddress(amethyst, "ChangeCreatureTable");
        if(!ChangeCreatureTable)
            MessageBoxA(0, "amethyst2_4.dll not matches", "Knightmare Skills Plugin", MB_OK);
    }
    else { MessageBoxA(0, "couldn't link to amethyst2_4.dll", "Knightmare Skills Plugin", MB_OK); }

    HMODULE new_towns = GetModuleHandle(L"new_towns.era");
    if (new_towns) {
        GetAzylumHero = (h3::H3Hero * (__cdecl*)(h3::H3Town*)) GetProcAddress(new_towns, "get_azylum_hero");
    }
}

void hero_apply_learning(h3::H3Hero* who) {
    int is_specialist = SkillSpecialist(who, Learning);
    int learning_bonus = get_Hero_zSSI_with_bonuses(who, Learning, 0);
    if (is_specialist) {
        learning_bonus <<= 1;
    }

    int ret = learning_bonus;
    THISCALL_4(int, 0x004E3620, who, ret, 1, 1);
}

void hero_apply_estates(h3::H3Hero* who) {
    auto current_day = h3::H3Main::Get()->date.day;
    if (current_day != 7) return;
    auto current_player = h3::H3Main::Get()->GetPlayer();
    int is_specialist = SkillSpecialist(who, Estates);
    int estates_bonus = get_Hero_zSSI_with_bonuses(who, Estates, 0);
    if (is_specialist) {
        estates_bonus <<= 1;
    }
    for (int i = 0; i < estates_bonus; ++i) {
        int res_type = Random(0, 5);
        ++(current_player->playerResources[res_type]);
        if (res_type == 0 || res_type == 2)
            ++(current_player->playerResources[res_type]);
    }
    /*
    if(res_type == 0 || res_type == 2)
        estates_bonus <<= 1;

    current_player->playerResources[res_type] += estates_bonus;
    */
}

void __stdcall OnEveryDay(TEvent* e) {
    auto current_player = h3::H3Main::Get()->players + o_ActivePlayerID; // h3::H3Main::Get()->GetPlayer();

    for (int i = 0; i < current_player->numberHeroes; ++i) {
        int hero_ID = current_player->heroIDs[i];
        if (hero_ID < 0) continue;
        h3::H3Hero* who = h3::H3Main::Get()->GetHero(hero_ID);
        
        hero_apply_learning(who);
        hero_apply_estates(who);
    }

    for (auto& town : h3::H3Main::Get()->towns) {
        int hero_ID = town.garrisonHero;
        if (hero_ID < 0) continue;
        h3::H3Hero* who = h3::H3Main::Get()->GetHero(hero_ID);
        if (who->owner == current_player->ownerID) {
            hero_apply_learning(who);
        }

    }
    
    if (GetAzylumHero && current_player->townsCount)
        for (auto &town : h3::H3Main::Get()->towns) {
            if (current_player->ownerID != town.owner) continue;
            h3::H3Hero* who = GetAzylumHero(&town);
            if (!who) continue;
            who->owner = current_player->ownerID;
           
            hero_apply_learning(who);
            current_player->playerResources.gold += get_Hero_SSI_with_bonuses(who, Estates);
            hero_apply_estates(who);

            h3::H3Main::Get()->heroOwner[who->id] = 64;
            who->owner = -1;
    }
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {
        globalPatcher = GetPatcher();
        SS_KK = globalPatcher->CreateInstance((char*)PINSTANCE_MAIN);

        ConnectEra();
        RegisterHandler(link_more_SS_levels, "OnAfterWoG");
        RegisterHandler(Alignment_OnStackToStackDamage, "OnStackToStackDamage");
        RegisterHandler(Tactics_Archery_Offense, "OnBeforeBattleAction");
        RegisterHandler(init_battle_round, "OnBattleRound");


        RegisterHandler(OnEveryDay, "OnEveryDay");

    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

