#include "main.h"
 
 
#define OPCODE_JUMP 0xE9
#define OPCODE_CALL 0xE8
#define OPCODE_NOP 0x90

#define HOOKTYPE_JUMP 0
#define HOOKTYPE_CALL 1

#define HOOK_SIZE 5

__fastcall int (*sub_4EA800)(int _this, int edx, short a2, short a3, short a4, short a5, short a6, const char *Source, int a8, int a9, char a10, short a11, short a12) =
(__fastcall int (*)(int , int , short , short , short , short , short , const char*, int, int, char, short, short))(0x4EA800);

inline void WriteHook( void *pOriginal, void *pNew, char type )
{
   *(byte*)pOriginal = (type==HOOKTYPE_JUMP)?OPCODE_JUMP:OPCODE_CALL;
   *(void**)( (byte*)pOriginal + 1 ) = (void*)( (byte*)pNew - (byte*)pOriginal - HOOK_SIZE );
}


__fastcall int HeroScrCoordFix(int _this, int edx, short a2, short a3, short a4, short a5, short a6, const char *Source, int a8, int a9, char a10, __int16 a11, __int16 a12)
{
   return(sub_4EA800( _this,  edx, 366+2, 135,  a4,  a5,  a6, Source,  a8,  a9,  a10,  a11,  a12));
}


void PatchGame()
{
   char _hack[10]={0xC7,0x05,0x0C,0x39,0x69,0x00,0x00,0x1E,0x05,0x00};

   *(char*)(0x5AFA8F+2) = 0xFF;    //spell book can be exchanged
   *(char*)(0x5AFA98+2) = 0xFF;    //catapult   can be exchanged

   //warfare and spellbook can be putted into backpack
   *(char*)0x004E321C = 0xA3;
   *(char*)0x004E3225 = 0x9A;
   *(char*)0x004E322E = 0x91;
   *(char*)0x004E3237 = 0x88;


   *(char*)(0x4DE112+2) = 0xFF; //unlocking catapult slot

   //*(char*)(0x69390E) = 5; //allowing to misc arts be placed in catapult slot
   //memset((void*)0x44C9AA,0x90,5);

   //*(int*)0x44C9AB = 0x693910;

   //memset((void*)0x4D8BDD,0x90,10); //no more catapult

   memcpy ((void*)0x4D8BDD, (void*)_hack, 10);

   //coords
   //change screen patch

   *(int*)0x5AD153 = 528;
   *(int*)0x5ADCA7 = 528;

   *(int*)0x5AD14E = 293;
   *(int*)0x5ADCA2 = 293;


   *(int*)0x5ACB6B = 96;
   *(int*)0x5AD6F9 = 96;

   *(int*)0x5ACB66 = 293;
   *(int*)0x5AD6F4 = 293;

   //altar patch
   *(int*)0x6412B8 = 65;
   *(int*)0x6412BC = 131;

   //merchant
 //  *(char*)0x5EEAB5 = 1;  //unlocking slots
 //  *(char*)0x5EEAB6 = 1;
 //  *(char*)0x5EEAB7 = 1;
   *(char*)0x5EEAB8 = 1;   //unlocking catapult slot
 //  *(char*)0x5EEAB9 = 1;

    *(int*)0x5E78B1 = 187;
    *(int*)0x5E78B6 = 68;

    *(int*)0x5E6CEF = 189;
    *(int*)0x5E6CF4 = 70;

 //mainheroscreen
 WriteHook((void*)0x4E049C, (void*)HeroScrCoordFix,HOOKTYPE_CALL);
 WriteHook((void*)0x4E0A9A, (void*)HeroScrCoordFix,HOOKTYPE_CALL);



}

extern "C" __stdcall BOOL DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
   switch (fdwReason)
   {
       case DLL_PROCESS_ATTACH:
           PatchGame();
           break;

       case DLL_PROCESS_DETACH:
           // detach from process
           break;

       case DLL_THREAD_ATTACH:
           // attach to thread
           break;

       case DLL_THREAD_DETACH:
           // detach from thread
           break;
   }
   return TRUE; // succesful
}
