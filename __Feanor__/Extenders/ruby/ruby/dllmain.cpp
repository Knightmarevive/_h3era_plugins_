// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.
#include "ruby.h"


Patcher * globalPatcher;
PatcherInstance *patcher;

int __stdcall HasSpellLoHook(LoHook* h, HookContext* c)
{
	HERO* hero = (HERO*)(c->ecx);
	int spell = c->edi;

	if(spell < 70) 
	{
		return EXEC_DEFAULT;
	}
	else 
	{
		c->eax = 1;
		c->return_address = 0x59CD62;
		return NO_EXEC_DEFAULT;
	}
}

void __stdcall SetupSpells(PEvent e)
{
	ExecErmCmd("SS45:F?v1;");
	ExecErmCmd("SS70:Fv1;");
	ExecErmCmd("SS71:Fv1;");
	ExecErmCmd("SS72:Fv1;");
	ExecErmCmd("SS73:Fv1;");
	ExecErmCmd("SS74:Fv1;");
	ExecErmCmd("SS75:Fv1;");
	ExecErmCmd("SS76:Fv1;");
	ExecErmCmd("SS77:Fv1;");
	ExecErmCmd("SS78:Fv1;");
	ExecErmCmd("SS79:Fv1;");
	ExecErmCmd("SS80:Fv1;");
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{

	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{

		globalPatcher = GetPatcher();
		patcher =  globalPatcher->CreateInstance("ruby");
		ConnectEra();

		patcher->WriteLoHook(0x59CD5B,(void*)HasSpellLoHook);
		patcher->WriteDword(0x59CDBF,0x2530+0x88*10);

		RegisterHandler(SetupSpells,"OnAfterErmInstructions");
	}
	return TRUE;
}

