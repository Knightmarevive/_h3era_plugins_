// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.

#include <windows.h>
#include <stdio.h>
#include "..\..\include\patcher_x86_commented.hpp"
#include "..\..\include\HotA\HoMM3.h"
#include "..\..\include\heroes.h"


Patcher * globalPatcher;
PatcherInstance *patcher;

int __stdcall LoHook_AttackBadluck(LoHook* h, HookContext* c)
{
	_BattleStack_* stack= (_BattleStack_*)c->esi;
	_BattleMgr_* mgr = o_BattleMgr;
	int luck = stack->Field<int>(0x4EC);
	
	if (luck<0)
	{
		if (luck<-3) luck =-3;
		if ( Randint(1, 24) <= -luck )
		{
			stack->field_70 = -1;

			if(!o_BattleMgr->ShouldNotRenderBattle())
			{
				CALL_3(void, __fastcall, 0x59A890, "badluck.82m", -1, 3);
				_cstr_ v18 = GetCreatureName(stack->creature_id, stack->count_current);
				sprintf(o_TextBuffer, (const char *)*(int*)(*(int*)((*(int*)0x6A5DC4) + 32) + 180), v18);
				CALL_5(void, __fastcall, 0x4729D0, o_BattleMgr->dlg, 0, o_TextBuffer, 1,0);
				CALL_6(void, __fastcall, 0x4963C0,  o_BattleMgr, 0, 48, stack, 100, 0);
			}
		}
	}

	//c->return_address = 0x004415D8;
	return EXEC_DEFAULT;
}

/*int __stdcall OnDamage(LoHook* h, HookContext* c)
{
	_BattleStack_* stack= (_BattleStack_*)c->ebx;
	if (stack->field_70<0)
	{
				//sprintf(o_TextBuffer, "������� %i %i", *(int*)(c->ebp-0x10), *(int*)(c->ebp+8));
				//CALL_5(void, __fastcall, 0x4729D0, o_BattleMgr->dlg, 0, o_TextBuffer, 1,0);

		*(int*)(c->ebp-0x10)-=*(int*)(c->ebp+0x08)/2;
				//sprintf(o_TextBuffer, "������� %i %i", *(int*)(c->ebp-0x10), *(int*)(c->ebp+8));
				//CALL_5(void, __fastcall, 0x4729D0, o_BattleMgr->dlg, 0, o_TextBuffer, 1,0);
	}

	return EXEC_DEFAULT;
}*/



double __stdcall HiHook_BadLuck_Penalty(HiHook* h, _BattleStack_* this_,  _BattleStack_* target, _bool8_ is_shot)
{
  // ���������� ���������, ����������� ����� ��� ����������� �������.
  return ((this_->field_70 == -1) ? 0.5 : 1.0)*CALL_3(double, __thiscall, h->GetDefaultFunc(), this_, target, is_shot);
}




// ������� �������� ���� ��������� ���� � ������ ������������� ����� �����.
int __stdcall LoHook_HeroLuckModifsList_Hourglass(LoHook* h, HookContext* c)
{
  // �����.
  _Hero_* hero = (_Hero_*)c->ebx;
  
  // ���� ���� �������� ���� ��������� ���� � ��� ���� �� �����.
  if (!(hero->temp_mod_flags & 0x400000) && hero->DoesWearArtifact(AID_HOURGLASS_OF_THE_EVIL_HOUR))
  {
    // ����� � �����.
    CALL_VA(_HStringF_*, 0x50C7F0, c->ebp - 40, ((_cstr_*)0x6A5394)[23], GetArtifactRecord(AID_HOURGLASS_OF_THE_EVIL_HOUR)->name); // ������������ sprintf
    
    // ��������������� ������������ �������.
    c->ebx = 0;
    
    // ���������� ������.
    c->return_address = 0x4DD4B4;
    
    return NO_EXEC_DEFAULT;
  }
  else
  {
    return EXEC_DEFAULT;
  }
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		globalPatcher = GetPatcher();
		patcher =  globalPatcher->CreateInstance("badluck");
		//ConnectEra();
		
		//��� �� ����������� � ��� �����
		//patcher->WriteLoHook(0x44152A,(void*)LoHook_AttackBadluck);
		//patcher->WriteLoHook(0x4430A3,(void*)OnDamage);

		patcher->WriteLoHook(0x44152A, LoHook_AttackBadluck); // ��� �����
		patcher->WriteLoHook(0x43F648, LoHook_AttackBadluck); // ��� ��������

  
		patcher->WriteHiHook(0x4438B0, SPLICE_, EXTENDED_, THISCALL_, HiHook_BadLuck_Penalty);

	  
	  // ������� �������� ���� ��������� ���� � ������ ������������� ����� �����
		patcher->WriteLoHook(0x4DCDA6, LoHook_HeroLuckModifsList_Hourglass);
	  
	  
	  // �������� ����� ��� ����� � ������� ���.
		patcher->WriteCodePatch(0x441512, "%n", 5); // 5 nop
		patcher->WriteCodePatch(0x44151D, "%n", 13); // 13 nop
	  
	  // �������� ����� ��� ����� ��� ��������.
		patcher->WriteCodePatch(0x43F630, "%n", 24); // 24 nop
	  
	  
	  
	  // ��������� �������� ������ �� ����� ����� ��� ������ �������� ��������� �����.
	  
	  // ��������� �����.
		patcher->WriteCodePatch(0x528A95, "%n", 17); // 17 nop
	  
	  // ����� ���.
		patcher->WriteCodePatch(0x528D93, "%n", 17); // 17 nop
	  
	  // �������, ���� �������, ������ ��������.
		patcher->WriteCodePatch(0x528277, "%n", 15); // 15 nop
		patcher->WriteCodePatch(0x528288, "%n", 2); // 2 nop

	}
	return TRUE;
}


/*


int __stdcall LoHook_AttackBadluck(LoHook* h, HookContext* c)
{
  // ����.
  _BattleStack_* stack = (_BattleStack_*)c->esi;
  
  // �������� �����.
  _BattleMgr_* mgr = o_BattleMgr;
  
  // ����� �����.
  int luck = stack->Field<_int32_>(0x4EC); 
  
  // �������� �������.
  if (luck < 0)
  {
    
    // ����� �� ����� ���� < -3.
    if (luck < -3) luck = -3;
    
    if ( Randint(1, 24) <= -luck )
    {
      // ��� ����� ����������� ���� ����� (��� ������������ ����� = 1), ���������� �������������; ������������� �� �������.
      stack->field_70 = -1; 
      
      // ���������� �������.
      if(!o_BattleMgr->ShouldNotRenderBattle())
      {
        CALL_3(void, __fastcall, 0x59A890, "badluck.82m", -1, 3); // ����
        _cstr_ v18 = GetCreatureName(stack->creature_id, stack->count_current);
        sprintf(o_TextBuffer, (const char *)*(int*)(*(int*)((*(int*)0x6A5DC4) + 32) + 180), v18);
        CALL_5(void, __fastcall, 0x4729D0, o_BattleMgr->dlg, 0, o_TextBuffer, 1,0); // ���
        CALL_6(void, __fastcall, 0x4963C0,  o_BattleMgr, 0, 48, stack, 100, 0); // �������� ������� (�48)
      }
      
    }
  }
  
  return EXEC_DEFAULT;
}


// ���������� ����� �� �������.
double __stdcall HiHook_BadLuck_Penalty(HiHook* h, _BattleStack_* this_,  _BattleStack_* target, _bool8_ is_shot)
{
  // ���������� ���������, ����������� ����� ��� ����������� �������.
  return ((this_->field_70 == -1) ? 0.5 : 1.0)*CALL_3(double, __thiscall, h->GetDefaultFunc(), this_, target, is_shot);
}




// ������� �������� ���� ��������� ���� � ������ ������������� ����� �����.
int __stdcall LoHook_HeroLuckModifsList_Hourglass(LoHook* h, HookContext* c)
{
  // �����.
  _Hero_* hero = (_Hero_*)c->ebx;
  
  // ���� ���� �������� ���� ��������� ���� � ��� ���� �� �����.
  if (!(hero->temp_mod_flags & 0x400000) && hero->DoesWearArtifact(AID_HOURGLASS_OF_THE_EVIL_HOUR))
  {
    // ����� � �����.
    CALL_VA(_HStringF_*, 0x50C7F0, c->ebp - 40, ((_cstr_*)0x6A5394)[23], ArtsInfo[AID_HOURGLASS_OF_THE_EVIL_HOUR].name); // ������������ sprintf
    
    // ��������������� ������������ �������.
    c->ebx = 0;
    
    // ���������� ������.
    c->return_address = 0x4DD4B4;
    
    return NO_EXEC_DEFAULT;
  }
  else
  {
    return EXEC_DEFAULT;
  }
}



// �������������.
void MonsterAblInit()
{
  // ������������ �������.
  patcher->WriteLoHook(0x44152A, LoHook_AttackBadluck); // ��� �����
  patcher->WriteLoHook(0x43F648, LoHook_AttackBadluck); // ��� ��������
  
  // ���������� ����� �� �������.
  patcher->WriteHiHook(0x4438B0, SPLICE_, EXTENDED_, THISCALL_, HiHook_BadLuck_Penalty);
  
  // ������� �������� ���� ��������� ���� � ������ ������������� ����� �����
  patcher->WriteLoHook(0x4DCDA6, LoHook_HeroLuckModifsList_Hourglass);
  
  
  // �������� ����� ��� ����� � ������� ���.
  patcher->WriteCodePatch(0x441512, "%n", 5); // 5 nop
  patcher->WriteCodePatch(0x44151D, "%n", 13); // 13 nop
  
  // �������� ����� ��� ����� ��� ��������.
  patcher->WriteCodePatch(0x43F630, "%n", 24); // 24 nop
  
  
  
  // ��������� �������� ������ �� ����� ����� ��� ������ �������� ��������� �����.
  
  // ��������� �����.
  patcher->WriteCodePatch(0x528A95, "%n", 17); // 17 nop
  
  // ����� ���.
  patcher->WriteCodePatch(0x528D93, "%n", 17); // 17 nop
  
  // �������, ���� �������, ������ ��������.
  patcher->WriteCodePatch(0x528277, "%n", 15); // 15 nop
  patcher->WriteCodePatch(0x528288, "%n", 2); // 2 nop
  
}






*/


