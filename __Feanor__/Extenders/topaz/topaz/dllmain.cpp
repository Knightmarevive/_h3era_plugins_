// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.

#include "..\..\include\HotA\HoMM3.h"
#include <windows.h>
#include <stdio.h>
#include "..\..\include\era.h"
#include "..\..\include\heroes.h"
#include "..\..\include\patcher_x86_commented.hpp"


Patcher * globalPatcher;
PatcherInstance *patcher;

#pragma pack (push, 1)
typedef struct
{
	unsigned int mask;

	char offset_y;
	short unk;
	char offset_x;

	int unk_1;
	int unk_2;

	char* defname;
}OBSTACLE;
#pragma pack (pop)

char* RecreateTable(char* oldtable, int oldsize, int newsize, int first_link, ...)
{

	char *newtable = (char*)malloc(newsize);
	memcpy((void*)newtable,(void*)oldtable, oldsize);
	
	int *p = &first_link;        //--������������ �� ������ ������ ����������


    while (*p)         //--���� �������� �� ����� ����
    { 
		//char buf[64];
		//sprintf(buf, "%X %X %X %X %X\r\n", *p, *(int*)(*p), (int)newtable, (int)oldtable, *(int*)(*p)+(int)newtable- (int)oldtable);
		//WriteConsoleA(GetStdHandle(STD_OUTPUT_HANDLE),buf,strlen(buf),0,0);

	 *(int*)(*p)+=((int)newtable /*+ *(int*)(*p)*/ - (int)oldtable);
	  p++;             //--������������� �� ��������� ��������
    }
	return newtable;
}



void __stdcall Topaz (PEvent e)
{
		OBSTACLE* newtable = (OBSTACLE*)RecreateTable((char*)0x63C7C8,sizeof(OBSTACLE)*91,sizeof(OBSTACLE)*255,0x465C21,0x466404,0x466411,0x466447,0x4666BF,0x4666CC,0x7156CF,0);

		memcpy((char*)(newtable+100), (char*)(newtable+62),20);
		newtable[100].defname = "obstdr03.def";
		newtable[100].offset_y = 2;
		//newtable[100].mask = 0x10000;

		memcpy((char*)(newtable+101), (char*)(newtable+65),20);
		newtable[101].defname = "obstdr02.def";
		newtable[101].offset_y = 2;
		//newtable[101].mask = 0x10000;

		memcpy((char*)(newtable+102), (char*)(newtable+67),20);
		newtable[102].defname = "obstdr01.def";
		newtable[102].offset_y = 4;
		//newtable[102].mask = 0x10000;

		memcpy((char*)(newtable+100), (char*)(newtable+62),20);
		newtable[0].defname = "obdino1.def";
		newtable[0].offset_y = 12;
		newtable[0].offset_x = -2;


		char ss[512]; 
		char *hex[] = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
		FILE * pFile;

		pFile = fopen ( "obstacles.txt" , "w" );
		for(int i=0; i!=91; i++)
		{
			fwrite("\r\n[Obstacle", 1, strlen("\r\n[Obstacle"), pFile);
			fwrite(itoa(i,ss,10), 1, strlen(itoa(i,ss,10)), pFile);
			fwrite("]", 1, strlen("]"), pFile);

			fwrite("\r\nData=", 1, strlen("\r\nData="), pFile);
			
			char* p = (char*)(newtable+i);
			for (int j=0; j!=16; j++)
			{
				sprintf(ss,"%02X",p[j]);
				fwrite(ss, 1, 2, pFile);
			}


			fwrite("\r\nDefname=", 1, strlen("\r\nDefname="), pFile);
			fwrite(newtable[i].defname, 1, strlen(newtable[i].defname), pFile);
		}
	  fclose (pFile);
}

int __stdcall NewObstacleDraw(HiHook *h, int combatmanager, _Def_ *def, int cadre, int x, int y, int mirror)
{
	return CALL_8(int,__thiscall, 0x494E30 ,combatmanager, def, 0, cadre, x, y, 0, mirror);

}

int __stdcall MoarObstacles(HiHook *h, int thiss, int a, int b)
{
	return CALL_3(int,__thiscall, 0x50C8D0, thiss, 0,255);

}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		//���� ���, ���� �������
		globalPatcher = GetPatcher();
		patcher =  globalPatcher->CreateInstance("topaz");
		ConnectEra();
		
		RegisterHandler(Topaz, "OnAfterCreateWindow");

		patcher->WriteHiHook(0x494532,CALL_,EXTENDED_,THISCALL_,(void*)NewObstacleDraw);

		patcher->WriteHiHook(0x4663D8,CALL_,EXTENDED_,THISCALL_,(void*)MoarObstacles);
		patcher->WriteHiHook(0x46669C,CALL_,EXTENDED_,THISCALL_,(void*)MoarObstacles);

		

		//RegisterHandler(TimerTest,"OnGlobalTimer");
	}
	return TRUE;
}



