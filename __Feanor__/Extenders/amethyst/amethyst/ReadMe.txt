========================================================================
���������� ������������ ����������. ����� �������� amethyst
========================================================================

��� ���������� DLL amethyst ������� ������������� � 
������� ������� ����������.

   � ���� ����� ������������ ������ ����������� ���� ������, �������� � 
   ������ ���������� amethyst.


amethyst.vcproj
   �������� ���� ������� VC++, ������������� ����������� � ������� 
   ������� ����������.
   � ����� ������������ �������� � ������ Visual C++, �������������� 
   ��� �������� �����, � ����� � ���������� ���������, ������������ � 
   �������, �������� � ������� ������� ����������.

amethyst.cpp
   �������� �������� ���� ���������� DLL.

   ��� �������� ���� ���������� DLL ������� �������� �� �����������. 
   ������� ��� �� ���������� �� ��������� ���� .lib. ����� ���������� 
   ������ � �������� ���������� �� ������� �������, �������� ��� ��� 
   �������� �������� �� ���������� DLL, � ���������� ���� ����� ������� 
   ���������� �������� ����� ����, ����� ������� ���������� ���� ��������� 
   �������, ����� � ����� ������������ �� �������� ������ �������� 
   ���������� ��� ��������� ����������� ������� ���������� �������� ���.

/////////////////////////////////////////////////////////////////////////////
������ ����������� �����:

StdAfx.h, StdAfx.cpp
   ��� ����� ������������ ��� ���������� ����� ����������������� ���������� 
   (PCH) amethyst.pch � ����� ����������������� ����� 
   StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
����� �������:

� ������� ������������ �TODO:� � ������� ���������� ������������ ��������� 
��������� ����, ������� ���������� ��������� ��� ��������.

/////////////////////////////////////////////////////////////////////////////
