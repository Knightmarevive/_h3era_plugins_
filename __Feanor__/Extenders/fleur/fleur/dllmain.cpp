// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.
#include "stdafx.h"

Patcher * globalPatcher;
PatcherInstance *emerald;

int ReadIntegerFromIni(char* Key, char* SectionName, char* FilePath)
{
	volatile char ret[256];
	ReadStrFromIni(Key, SectionName, FilePath, (char*)ret);
	return atoi((char*)ret);
}

float ReadFloatFromIni(char* Key, char* SectionName, char* FilePath)
{
	volatile char ret[256];
	ReadStrFromIni(Key, SectionName, FilePath, (char*)ret);
	return atof((char*)ret);
}


extern "C" __declspec(dllexport) void __stdcall SetColorParameters(float p1, float p2, float p3, float p4)
{
		*(float*)0x55AD09 = p4;
		*(float*)0x55AD0E = p3;
		*(float*)0x55AD13 = p2;
		*(float*)0x55AD18 = p1;

		*(float*)0x55AF0A = p4;
		*(float*)0x55AF0F = p3;
		*(float*)0x55AF14 = p2;
		*(float*)0x55AF19 = p1;

		*(float*)0x55B137 = p4;
		*(float*)0x55B13C = p3;
		*(float*)0x55B141 = p2;
		*(float*)0x55B146 = p1;

		*(float*)0x55B330 = p4;
		*(float*)0x55B335 = p3;
		*(float*)0x55B33A = p2;
		*(float*)0x55B33F = p1;

		*(float*)0x55B550 = p4;
		*(float*)0x55B555 = p3;
		*(float*)0x55B55A = p2;
		*(float*)0x55B55F = p1;

		*(float*)0x55B756 = p4;
		*(float*)0x55B75B = p3;
		*(float*)0x55B760 = p2;
		*(float*)0x55B765 = p1;

		*(float*)0x55B924 = p4;
		*(float*)0x55B929 = p3;
		*(float*)0x55B92E = p2;
		*(float*)0x55B933 = p1;

		*(float*)0x55CFB0 = p4;
		*(float*)0x55CFB5 = p3;
		*(float*)0x55CFBA = p2;
		*(float*)0x55CFBF = p1;


		*(float*)(0x55A331+1) = p4;
		*(float*)(0x55A331+6) = p3;
		*(float*)(0x55A331+11) = p2;
		*(float*)(0x55A331+16) = p1;

		
		*(float*)(0x55A356+1) = p4;
		*(float*)(0x55A356+6) = p3;
		*(float*)(0x55A356+11) = p2;
		*(float*)(0x55A356+16) = p1;
		
}


extern "C" __declspec(dllexport)  void __stdcall SetColorParamsFromEVars(void)
{
		SetColorParameters(ErmE[1], ErmE[2], ErmE[3], ErmE[4]);
}

HiHook *h;

int __stdcall HSLtoRGB32(int hu, int s, int l, int ret)
{
	int a = CALL_4(int,__stdcall,  h->GetDefaultFunc(), hu, s, l, ret);
	//(*()ret)|=0xFF;
	return a;
}


int __stdcall RGBtoHSL(HiHook* hk, int r, int g, int b, int h, int s, int l)
{
	g |= 0x30;
	int a = CALL_6(int,__stdcall,  hk->GetDefaultFunc(), r, g, b, h, s, l);
	return a;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{


	if(ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		ConnectEra();	

		ReadStrFromIni = (TReadStrFromIni)GetProcAddress(LoadLibraryA("era.dll"), "ReadStrFromIni");

		*(int*)0x69E600 = ReadIntegerFromIni("Enabled","General","fleur.ini");


		SetColorParameters
			(
					ReadFloatFromIni("ColorHue","Parameters","fleur.ini"),
					ReadFloatFromIni("ColorValue","Parameters","fleur.ini"),   //(0,1 - red)
					ReadFloatFromIni("Saturation","Parameters","fleur.ini"),   //grayscale - 1 - colorfool
					ReadFloatFromIni("Lightness","Parameters","fleur.ini") //darker - 1 - brighter
			);

		*(int*)0x69E600 = 1;
		SetColorParameters
			(
					0.6f,
					0.0f,   //(0,1 - red)
					1.8f,   //grayscale - 1 - colorfool
					0.3f //darker - 1 - brighter
			);


	}

	
		/*globalPatcher = GetPatcher();
		emerald =  globalPatcher->CreateInstance("fleur");




		h = emerald->WriteHiHook(0x5237E0,SPLICE_,DIRECT_,STDCALL_,(void*)HSLtoRGB32);*/
		//emerald->WriteHiHook(0x523680,SPLICE_,EXTENDED_,STDCALL_,(void*)RGBtoHSL);

	return TRUE;
}

