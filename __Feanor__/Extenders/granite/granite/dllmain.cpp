#include "granite.h"

extern void __stdcall  Granite(PEvent e);

extern void __stdcall ReallocProhibitionTables(PEvent e);
extern void __stdcall LoadConfigs(PEvent e);



GAMEDATA save;
Patcher * globalPatcher;
PatcherInstance *granite;

/*void ApplyData()
{
	granite->WriteDword(0x40EF33,save.grail_id);
}*/

void __stdcall InitData (PEvent e)
{
	save.grail_id = 2;

	save.ApplyData();
	//strcpy(save,"IF:L^11^;\0");

	//GetArtifactRecord(171)->rank = 0x40;
	//GetArtifactRecord(171)->slot = 14;
}


void __stdcall StoreData (PEvent e)
{
	WriteSavegameSection(sizeof(save), (void*)&save, PINSTANCE_MAIN);
}


void __stdcall RestoreData (PEvent e)
{
	ReadSavegameSection(sizeof(save), (void*)&save, PINSTANCE_MAIN);
	save.ApplyData();
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		//���� ���, ���� �������
		globalPatcher = GetPatcher();
		granite =  globalPatcher->CreateInstance(PINSTANCE_MAIN);
		ConnectEra();

		//Storing data
		RegisterHandler(InitData, "OnAfterErmInstructions");
		RegisterHandler(StoreData, "OnSavegameWrite");
		RegisterHandler(RestoreData, "OnSavegameRead");


		//�������� �������
		RegisterHandler(Granite, "OnAfterWoG");
	}
	return TRUE;
}

