// granite.cpp: ���������� ���������������� ������� ��� ���������� DLL.
//

#include "granite.h"

int search_result = -1;

extern "C" __declspec(dllexport) void SetGrailArtifact(int id)
{
	save.grail_id = id;
	save.ApplyData();
}



void InitWoGSpecials()
{
	int i = 5+7;
	int ara[i];


	if (*(int *)(*(int*)0x69CCFC + 4)==-1) return;
	HERO* currhero = (HERO*)GetHeroRecord(*(int *)(*(int*)0x69CCFC + 4));

	*(int*)0x27F9970 = (int)currhero;
	ErmV[998] = currhero->x;
	ErmV[999] = currhero->y;
	ErmV[1000] = currhero->l;

	ErmF[1000]= !CALL_1(int, __thiscall, 0x4BAA60, *(int*)0x69CCFC);
}

int __stdcall OnTryDigging(HiHook* h, int advManager, int x, int y, int z)
{
	
  //int v4 = *(int *)(*(int*)0x69CCF8 + 4);
  //HERO  v6 = (int)&Main_Structure->Heroes[v4];

	int flag = 1;

	
	InitWoGSpecials();
	ErmX[1] = *(int *)(*(int*)0x69CCFC + 4);
	ErmX[2] = (int)&flag;
	CallERM(4074001);   //����� �������� ��������
	


	if(flag) 
		CALL_4(int, __thiscall, h->GetDefaultFunc(),advManager, x, y, z);
	else     
		search_result = -1;
	
	
	
	
	InitWoGSpecials();
	ErmX[1] = *(int *)(*(int*)0x69CCFC + 4); 
	ErmX[2] = search_result;
	CallERM(4074002); //����� ��������

	return NO_EXEC_DEFAULT;
}


int __stdcall OnDiggingMessage(HiHook* h, const char *text, int type, int f1, int f2, int f3, int f4, int f5, int f6, int f7, int f8, int f9, int f10)
{
	int flag = 1;
	
	
	switch (h->GetReturnAddress()-5)
	{
		case 0x40ce9e: {search_result = 0; break;}
		case 0x40ed09: {search_result = 1; break;}
		case 0x40ee24: {search_result = 2; break;}
		case 0x40ee60: {search_result = 3; break;}
		case 0x40efa6: {search_result = 4; break;}
		case 0x40efdd: {search_result = 5; break;}
		case 0x40f07b: {search_result = 6; break;}
	}

	InitWoGSpecials();
	ErmX[1] = *(int *)(*(int*)0x69CCFC + 4);
	ErmX[2] = (int)&flag;
	ErmX[3] = search_result;
	CallERM(4074003); //��� ����������

	if(flag) CALL_12(int, __fastcall, h->GetDefaultFunc(), text, type, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10);

	return NO_EXEC_DEFAULT;
}

//on success digging

void __stdcall Granite(PEvent e)
{
  granite->WriteHiHook(0x40EBF0,SPLICE_,EXTENDED_,THISCALL_,(void*)OnTryDigging);

  granite->WriteHiHook(0x40ce9e,CALL_,EXTENDED_,FASTCALL_,(void*)OnDiggingMessage); //digging for arts /r/ whole day
  granite->WriteHiHook(0x40ed09,CALL_,EXTENDED_,FASTCALL_,(void*)OnDiggingMessage); //searching is fruitless 
  granite->WriteHiHook(0x40ee24,CALL_,EXTENDED_,FASTCALL_,(void*)OnDiggingMessage); //try looking on on land
  granite->WriteHiHook(0x40ee60,CALL_,EXTENDED_,FASTCALL_,(void*)OnDiggingMessage); //try searching on clear ground
  granite->WriteHiHook(0x40efa6,CALL_,EXTENDED_,FASTCALL_,(void*)OnDiggingMessage); //congrats
  granite->WriteHiHook(0x40efdd,CALL_,EXTENDED_,FASTCALL_,(void*)OnDiggingMessage); //carry the grail..
  granite->WriteHiHook(0x40f07b,CALL_,EXTENDED_,FASTCALL_,(void*)OnDiggingMessage); //nothing here

  //granite->WriteCodePatch(0x40EE95,"%n",12); //������� ��������� 

}