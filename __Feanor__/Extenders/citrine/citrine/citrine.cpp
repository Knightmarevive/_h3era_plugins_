// citrine.cpp: ���������� ���������������� ������� ��� ���������� DLL.
//
#include "citrine.h"

char tmp[256];
//void (__stdcall *execErmCmd)(char* );

int GetCreatureFromCastle(int offset, CASTLE* cstl)
{
	
	//ConnectEra();



	int type = offset/14;
	int level = (offset%14)%7;
	int upgrade = (offset%14)/7;

	int default_ret = ((int*)0x6747B4)[(cstl->Type*2+upgrade)*7+level];

	sprintf(tmp,"SN:W^citrine_01_%i_%i_%i_%i^/?v1;", cstl->Number, type, level, upgrade);

	

	ExecErmCmd((char*)tmp);

	if (ErmV[1]==-1) return 0;
	if (ErmV[1]!=0) return ErmV[1];
	return default_ret;

	/*if (type == 2 && level == 2 && upgrade ==0 && cstl->Number&1) return 32;
	if (type == 2 && level == 2 && upgrade ==1 && cstl->Number&1) return 117;

	if (type == 2 && level == 2 && upgrade ==0 && !(cstl->Number&1)) return 33;
	if (type == 2 && level == 2 && upgrade ==1 && !(cstl->Number&1)) return 116;*/

	//return ((int*)0x836CC8)[(type*2+upgrade)*7+level];
}

signed int __stdcall CorrectTownCreatureGrowth(HiHook *h, CASTLE* _cstl, int a2, int a3)
{
  signed int result; // eax@1
  int i = 0;


  result = 0;
  do
  {
    if ( GetCreatureFromCastle(14*_cstl->Type+i, _cstl) == a2 )
      break;

    ++result;
    ++i;
  }
  while ( result < 14 );
  if ( result != 14 )
  {
    //*(int *)((int)_cstl + 4 * result + 0x118) += a3;
	_cstl->GuardsT0[result]+=a3;
    if ( result < 7 )
		_cstl->GuardsN0[result]+=a3;
      //*(int *)((int)_cstl + 4 * result + 0x134) += a3;
  }
  return result;
}



int __stdcall GetMonsterUpgrade(HiHook *h, int a)
{
	/*if(a == 32) 
		return 117;
	
	if(a == 33)
		return 116;*/

	return CALL_1(int, __fastcall, h->GetDefaultFunc(), a);
}

int __stdcall GetMonsterDegrade(HiHook *h, int a)
{
	/*if(a == 32) 
		return 117;
	
	if(a == 33)
		return 116;*/

	return CALL_1(int, __fastcall, h->GetDefaultFunc(), a);

}

int __stdcall HasMonsterUpgrade(HiHook *h, int a)
{
	/*if(a == 32) 
		return 117;
	
	if(a == 33)
		return 116;*/

	return CALL_1(int, __fastcall, h->GetDefaultFunc(), a);
}

int Dummy(int offset, CASTLE* cstl)
{
	return 195;
}

//���� ������ � �.

void __stdcall splice_00551B50(HiHook *h, CASTLE* cstl, int edx) 
{
	int a1 = (int)cstl;
    //sub_00551B80(MonstersInTowns_9x2x7[edx0 + 14 * *(_BYTE *)(a1 + 4)], a1 + 2 * edx0 + 22);

	CALL_2(void,__fastcall,0x551B80,GetCreatureFromCastle(edx + 14 * cstl->Type, cstl), a1 + 2 * edx + 22);
	return;
}

int __stdcall hook_428602(LoHook* h, HookContext* c) 
	{c->edx = GetCreatureFromCastle(c->edx,(CASTLE*)(*(int*)(c->ebp + 0x8))); return NO_EXEC_DEFAULT;}

int __stdcall hook_428964(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)c->ebx); return NO_EXEC_DEFAULT;}

int __stdcall hook_429BB1(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)(*(int*)(c->ebp - 0x14))); return NO_EXEC_DEFAULT;}

int __stdcall hook_429DEC(LoHook* h, HookContext* c) 
	{c->ecx = GetCreatureFromCastle(c->eax,(CASTLE*)c->edi); return NO_EXEC_DEFAULT;}

int __stdcall hook_429F32(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->eax,(CASTLE*)c->edi); return NO_EXEC_DEFAULT;}

int __stdcall hook_42A026(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edi,(CASTLE*)c->eax); return NO_EXEC_DEFAULT;}

int __stdcall hook_42B538(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)c->ecx); return NO_EXEC_DEFAULT;}

int __stdcall hook_42B5D9(LoHook* h, HookContext* c) 
	{c->edi = GetCreatureFromCastle(c->edx,(CASTLE*)c->ecx); return NO_EXEC_DEFAULT;}

int __stdcall hook_42B5F3(LoHook* h, HookContext* c) 
	{c->esi = GetCreatureFromCastle(c->esi,(CASTLE*)c->ecx); return NO_EXEC_DEFAULT;}

int __stdcall hook_42B724(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->ecx,(CASTLE*)c->edi); return NO_EXEC_DEFAULT;}

int __stdcall hook_42BE42(LoHook* h, HookContext* c) 
	{c->edx = GetCreatureFromCastle(c->eax,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_42CF07(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)(*(int*)(c->ebp + 0xC))); return NO_EXEC_DEFAULT;}

int __stdcall hook_42D241(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->eax,(CASTLE*)(*(int*)(c->ebp + 0x8))); return NO_EXEC_DEFAULT;}

int __stdcall hook_432E94(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_432F5F(LoHook* h, HookContext* c) 
	{c->edx = GetCreatureFromCastle(c->ecx,(CASTLE*)c->edi); return NO_EXEC_DEFAULT;}

int __stdcall hook_43363B(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->eax,(CASTLE*)c->edx); return NO_EXEC_DEFAULT;}

int __stdcall hook_4BF302(LoHook* h, HookContext* c) 
	{c->edi = GetCreatureFromCastle(7+c->eax/4,(CASTLE*)(*(int*)(c->ebp - 0x10))); return NO_EXEC_DEFAULT;}

int __stdcall hook_4BF307(LoHook* h, HookContext* c) 
	{c->esi = GetCreatureFromCastle(c->eax/4,(CASTLE*)(*(int*)(c->ebp - 0x10))); return NO_EXEC_DEFAULT;}

int __stdcall hook_4C8D2D(LoHook* h, HookContext* c) 
	{c->edx = GetCreatureFromCastle(c->ecx,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_51CFD8(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)(*(int*)(c->ebp - 0x14))); return NO_EXEC_DEFAULT;}

int __stdcall hook_525A8B(LoHook* h, HookContext* c) 
	{c->edx = GetCreatureFromCastle(7+c->eax,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_525AAD(LoHook* h, HookContext* c) 
{c->return_address =  (c->eax == GetCreatureFromCastle(c->ecx,(CASTLE*)c->esi))?0x525ABA:0x525B56; return NO_EXEC_DEFAULT;}

int __stdcall hook_52A31B(LoHook* h, HookContext* c) 
	{c->ebx = GetCreatureFromCastle(c->edx,(CASTLE*)c->edi); return NO_EXEC_DEFAULT;}

int __stdcall hook_5519A7(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_5BEF9E(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->eax,(CASTLE*)c->ecx); return NO_EXEC_DEFAULT;}

//int __stdcall hook_551B68(LoHook* h, HookContext* c) 
//	{c->ecx = GetCreatureFromCastle(c->eax,(CASTLE*)c->ecx); return NO_EXEC_DEFAULT;}

int __stdcall hook_5BFC66(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->ecx,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_5BFFDF(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_5C0098(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_5C0264(LoHook* h, HookContext* c) 
	{c->edx = GetCreatureFromCastle(c->esi/2,(CASTLE*)c->ecx); return NO_EXEC_DEFAULT;}

int __stdcall hook_5C6023(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->eax,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_5C7196(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->ecx,(CASTLE*)(*(int*)(c->ebx + 0x38))); return NO_EXEC_DEFAULT;}

int __stdcall hook_5C7CE5(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)(*(int*)(c->ebx + 0x38))); return NO_EXEC_DEFAULT;}

int __stdcall hook_5C7D1E(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->eax,(CASTLE*)c->ecx); return NO_EXEC_DEFAULT;}


int __stdcall hook_5D9DE4(LoHook* h, HookContext* c) 
	{	int tmgr = *(int*)0x69954C;
		c->edx = GetCreatureFromCastle(c->edx,(CASTLE*)(*(int*)(0x38 + tmgr))); return NO_EXEC_DEFAULT;}

int __stdcall hook_5D9E5D(LoHook* h, HookContext* c) 
	{	int tmgr = *(int*)0x69954C;
		c->edx = GetCreatureFromCastle(c->ecx,(CASTLE*)(*(int*)(0x38 + tmgr))); return NO_EXEC_DEFAULT;}

int __stdcall hook_5DD099(LoHook* h, HookContext* c) 
	{	int tmgr = *(int*)0x69954C;
		c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_5DDAD6(LoHook* h, HookContext* c) 
	{	int tmgr = *(int*)0x69954C;
		c->eax = GetCreatureFromCastle(c->ecx,(CASTLE*)(*(int*)(0x38 + tmgr))); return NO_EXEC_DEFAULT;}

int __stdcall hook_5C8037(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->eax-30,(CASTLE*)(*(int*)(c->ebx + 0x38))); return NO_EXEC_DEFAULT;}

int __stdcall hook_5C0BEC(LoHook* h, HookContext* c) 
	{c->ebx = GetCreatureFromCastle(c->edx,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_551A14(LoHook* h, HookContext* c) 
	{c->edx = GetCreatureFromCastle(c->ecx-7,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_5DD96B(LoHook* h, HookContext* c) 
	{c->eax = GetCreatureFromCastle(c->edx,(CASTLE*)(*(int*)((*(int*)0x69954C) + 0x38))); return NO_EXEC_DEFAULT;}


int __stdcall hook_42BCC9(LoHook* h, HookContext* c)
	{c->edi = GetCreatureFromCastle(c->eax-30,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}

int __stdcall hook_4C69AF(LoHook* h, HookContext* c) 
{c->return_address =  (c->edi != GetCreatureFromCastle(c->eax-30,(CASTLE*)c->ebx))?0x4C69df:0x4C69b8; return NO_EXEC_DEFAULT;}


int __stdcall hook_5C0203(LoHook* h, HookContext* c)
	{c->ecx = GetCreatureFromCastle(c->ecx,(CASTLE*)c->esi); return NO_EXEC_DEFAULT;}


int saved_town_struct;

int __stdcall hook_521711(LoHook* h, HookContext* c)
	{saved_town_struct = c->edx; return EXEC_DEFAULT;}

int __stdcall hook_52190D(LoHook* h, HookContext* c)
	{c->eax = GetCreatureFromCastle(c->eax-19,(CASTLE*)saved_town_struct); return NO_EXEC_DEFAULT;}

int __stdcall hook_5218F7(LoHook* h, HookContext* c)
	{c->edi = GetCreatureFromCastle(c->eax-33,(CASTLE*)saved_town_struct); return NO_EXEC_DEFAULT;}

int __stdcall hook_5219B2(LoHook* h, HookContext* c)
	{c->edi = GetCreatureFromCastle(c->eax-69,(CASTLE*)saved_town_struct); return NO_EXEC_DEFAULT;}

int __stdcall hook_521951(LoHook* h, HookContext* c)
	{c->edi = GetCreatureFromCastle(c->eax-332,(CASTLE*)saved_town_struct); return NO_EXEC_DEFAULT;}


//	citrine->WriteLoHook(0x52190D,(void*)hook_52190D); //
//	citrine->WriteLoHook(0x5218F7,(void*)hook_5218F7); //
//	citrine->WriteLoHook(0x5219B2,(void*)hook_5219B2); //
//	citrine->WriteLoHook(0x521951,(void*)hook_521951); //

//
//   H   H  AAAAA  TTTTT  EEEEE !!
//   H   H  A   A    T    E     !!
//   HHHHH  AAAAA    T    EEE   !!
//   H   H  A   A    T    E     
//   H   H  A   A    T    EEEEE !!
//
// God curses Adam with the routine.




void __stdcall Citrine(PEvent e)
{
	citrine->WriteLoHook(0x5C0203,(void*)hook_5C0203); 

	citrine->WriteLoHook(0x42BCC9,(void*)hook_42BCC9); 
	citrine->WriteLoHook(0x4C69AF,(void*)hook_4C69AF);

	citrine->WriteLoHook(0x428602,(void*)hook_428602); //edx, [ebp+8] -> edx
	citrine->WriteLoHook(0x428964,(void*)hook_428964); //edx, ebx -> eax
	citrine->WriteLoHook(0x429BB1,(void*)hook_429BB1); //edx, [ebp-0x14] -> eax
	citrine->WriteLoHook(0x429DEC,(void*)hook_429DEC); //eax, edi -> ecx
	citrine->WriteLoHook(0x429F32,(void*)hook_429F32); //eax, edi -> eax
	citrine->WriteLoHook(0x42A026,(void*)hook_42A026); //eax, edi -> eax
	citrine->WriteLoHook(0x42B538,(void*)hook_42B538); //edx, ecx -> eax
	citrine->WriteLoHook(0x42B5D9,(void*)hook_42B5D9); //edx, ecx -> edi
	citrine->WriteLoHook(0x42B5F3,(void*)hook_42B5F3); //esi, ecx -> esi
	citrine->WriteLoHook(0x42B724,(void*)hook_42B724); //eax, edi -> eax

	citrine->WriteLoHook(0x42BE42,(void*)hook_42BE42); //eax, esi -> edx
	citrine->WriteLoHook(0x42CF07,(void*)hook_42CF07); //edx, [ebp+c] -> eax
	citrine->WriteLoHook(0x42D241,(void*)hook_42D241); //eax, [ebp+8] -> eax
	citrine->WriteLoHook(0x432E94,(void*)hook_432E94); //edx, esi -> eax
	citrine->WriteLoHook(0x432F5F,(void*)hook_432F5F); //ecx, edi -> edx
	citrine->WriteLoHook(0x43363B,(void*)hook_43363B); //eax, edx -> eax

	//��� ��������� �� 47AB00,11,33 - ��������� ��������
	citrine->WriteHiHook(0x47AAD0,SPLICE_,EXTENDED_,THISCALL_,(void*)GetMonsterUpgrade);
	citrine->WriteHiHook(0x47AB80,SPLICE_,EXTENDED_,THISCALL_,(void*)GetMonsterDegrade);
	citrine->WriteHiHook(0x47AA50,SPLICE_,EXTENDED_,THISCALL_,(void*)HasMonsterUpgrade);
	

	citrine->WriteLoHook(0x4BF302,(void*)hook_4BF302); //(eax/4)+7 [ebp-0x10] -> edi
	citrine->WriteLoHook(0x4BF308,(void*)hook_4BF307); //eax/4 [ebp-0x10] -> esi

	//����� �����
	citrine->WriteLoHook(0x4C8D2D,(void*)hook_4C8D2D); //ecx, esi -> edx



	//citrine->WriteLoHook(0x503290,(void*)hook_); //,  -> 
	//���� ������
	citrine->WriteLoHook(0x51CFD8,(void*)hook_51CFD8); //edx,[EBP-14]  -> eax 
	citrine->WriteLoHook(0x525A8B,(void*)hook_525A8B); //edx,[EBP-14]  -> eax 
	citrine->WriteLoHook(0x525AAD,(void*)hook_525AAD); //cmp ..
	citrine->WriteLoHook(0x52A31B,(void*)hook_52A31B); //edx, edi  -> ebx 
	//���� �� ������
	citrine->WriteLoHook(0x5519A7,(void*)hook_5519A7); //edx, edi  -> ebx 

	//����� �����������
	//citrine->WriteLoHook(0x551B68,(void*)hook_551B68); //eax, eci  -> ecx //������ ����
	
	//citrine->WriteLoHook(0x5BE383,(void*)hook_52A31B); //����� ����
	//citrine->WriteLoHook(0x5BE3AB,(void*)hook_52A31B); //����� ����

	citrine->WriteLoHook(0x5BEF9E,(void*)hook_5BEF9E); // 
	citrine->WriteLoHook(0x5BFC66,(void*)hook_5BFC66); // 
	citrine->WriteLoHook(0x5BFFDF,(void*)hook_5BFFDF); //
	citrine->WriteLoHook(0x5C0098,(void*)hook_5C0098); //
	//0x5C0203 - ����� ������

	//������. ������� �����������!!11
	//citrine->WriteLoHook(0x5C0264,(void*)hook_5C0264); //

	//citrine->WriteLoHook(0x5C0527,(void*)hook_); //absent in MoP
	//citrine->WriteLoHook(0x5C057E,(void*)hook_5C0264); //
	//citrine->WriteLoHook(0x5C0B34,(void*)hook_5C0264); //

	citrine->WriteLoHook(0x5C0BEC,(void*)hook_5C0BEC); //
	citrine->WriteLoHook(0x5C6023,(void*)hook_5C6023); //
	citrine->WriteLoHook(0x5C7196,(void*)hook_5C7196); //
	citrine->WriteLoHook(0x5C7CE5,(void*)hook_5C7CE5); //
	citrine->WriteLoHook(0x5C7D1E,(void*)hook_5C7D1E); //

	//�������� �������: 
	
	citrine->WriteLoHook(0x5C8037,(void*)hook_5C8037); //�����
	citrine->WriteLoHook(0x551A14,(void*)hook_551A14); //��������



	//���� �����
	citrine->WriteLoHook(0x5D9DE4,(void*)hook_5D9DE4); //edx, tmgr -> edx

	citrine->WriteLoHook(0x5D9E5D,(void*)hook_5D9E5D); //ecx, tmgr -> edx
	citrine->WriteLoHook(0x5D9ED3,(void*)hook_5D9E5D); //ecx, tmgr -> edx
	citrine->WriteLoHook(0x5D9F4C,(void*)hook_5D9E5D); //ecx, tmgr -> edx
	citrine->WriteLoHook(0x5D9FC5,(void*)hook_5D9E5D); //ecx, tmgr -> edx
	citrine->WriteLoHook(0x5DA03E,(void*)hook_5D9E5D); //ecx, tmgr -> edx
	citrine->WriteLoHook(0x5DA0C2,(void*)hook_5D9E5D); //ecx, tmgr -> edx
	citrine->WriteLoHook(0x5DA1BA,(void*)hook_5D9E5D); //ecx, tmgr -> edx 

	//
	citrine->WriteLoHook(0x5DD099,(void*)hook_5DD099); //edx, esi -> edx

	citrine->WriteLoHook(0x5DDAD6,(void*)hook_5DDAD6); //ecx, [edx+38] -> eax
	citrine->WriteLoHook(0x5DD96B,(void*)hook_5DD96B); //

	//�������

	citrine->WriteHiHook(0x551B50,SPLICE_,EXTENDED_,FASTCALL_,(void*)splice_00551B50);

	citrine->WriteHiHook(0x5C0250,SPLICE_,EXTENDED_,THISCALL_,(void*)CorrectTownCreatureGrowth);


	
	citrine->WriteLoHook(0x521711,(void*)hook_521711); //

	//��� ����� �����������
	citrine->WriteLoHook(0x52190D,(void*)hook_52190D); //
	citrine->WriteLoHook(0x5218F4,(void*)hook_5218F7); //
	citrine->WriteLoHook(0x5219B2,(void*)hook_5219B2); //
	citrine->WriteLoHook(0x521951,(void*)hook_521951); //

	//������ �� ����� �����!
	//citrine->CreateCodePatch(0x70F5AB,"%n",5);
	//citrine->CreateCodePatch(0x703D7A,"%n",5);

	/*for(int i=0; i!=9*14; i++)
	{
		((int*)0x6747B4)[i]=196;
	}*/

	
	HINSTANCE hAngel  = LoadLibraryA("era.dll");

	ExecErmCmd  = (void (__stdcall *)(char*))GetProcAddress(hAngel, "ExecErmCmd");
	//������� ��� ���� �������� ><
}


