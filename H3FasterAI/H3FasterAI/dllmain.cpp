// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"

#include "lib\H3API.hpp"
#include "patcher_x86_commented.hpp"
#include "era.h"
#include "heroes.h"

#define PINSTANCE_MAIN "H3FasterAI"

Patcher* globalPatcher;
PatcherInstance* H3FasterAI;


long z_timer = 0; 
long big_timer = 0;
long z_timeGetTime = 0x004F8970;


void __stdcall z_timer_reset(Era::TEvent* e)
{
    z_timer = CALL_0(long, __cdecl, z_timeGetTime);
}

void __stdcall big_timer_reset(Era::TEvent* e)
{
    big_timer = CALL_0(long, __cdecl, z_timeGetTime);
}

_LHF_(z_timer_reset_hook)
{
    z_timer = CALL_0(long, __cdecl, z_timeGetTime);
    return EXEC_DEFAULT;
}

constexpr long big_max_timer = 120000; // in miliseconds
constexpr long z_max_timer = 7000; // in miliseconds
char __stdcall sub_0056ADA0_hook(HiHook* h, HERO* hero, int a2) {
    long now = CALL_0(long, __cdecl, z_timeGetTime);

    if (now - z_timer > z_max_timer || now - big_timer > big_max_timer) {
        hero->Movement = 0;
        z_timer = now;
    }

    return CALL_2(int, __stdcall, h->GetDefaultFunc(), hero, a2);
}

long battle_army_power(h3::H3CombatMonster* stacks, int count) {
    long ret = 0;
    for (int i = 0; i < count; ++i) {
        if (stacks[i].numberAlive > 0)
            ret += stacks[i].info.aiValue * stacks[i].numberAlive;
    }
    return ret;
}

char better_side(long ecx) {
    auto batman = (h3::H3CombatManager*)ecx;
    //long* heroMonCount = (long*)(ecx+0x54BC);
    //H3CombatMonster &stacks[2][21]= (long*)(ecx + 0x54CC);
    int leftcount = batman->heroMonCount[0];
    long leftpower = battle_army_power(batman->stacks[0], leftcount);

    int rightcount = batman->heroMonCount[1];
    long rightpower = battle_army_power(batman->stacks[1], rightcount);
    
    return(rightpower > leftpower);
}

char __stdcall sub_0041E6F0_hook(HiHook* h, long ecx) {
    if (*(short*)(ecx + 0x54A4) == 0) {
        if (0==*(int*)(ecx + 0x53D0)) {
            long now = CALL_0(long, __cdecl, z_timeGetTime);
            if (now - z_timer > z_max_timer || now - big_timer > big_max_timer) {
                long o_GameMgr = *(long*)0x00699538;
                char is_cheater_scenario = *(char*)(o_GameMgr + 0x1F69C);
                char is_cheater_campaign = *(char*)(o_GameMgr + 0x1F458);

                long winner = 0;
                CALL_2(VOID, __thiscall, 0x468F80, ecx, 1 - winner);

                *(char*)(o_GameMgr + 0x1F69C) = is_cheater_scenario;
                *(char*)(o_GameMgr + 0x1F458) = is_cheater_campaign;

            }
        }
        else  {
            long now = CALL_0(long, __cdecl, z_timeGetTime);
            if (now - z_timer > z_max_timer || now - big_timer > big_max_timer) {
                long o_GameMgr = *(long*)0x00699538;
                char is_cheater_scenario = *(char*)(o_GameMgr + 0x1F69C);
                char is_cheater_campaign = *(char*)(o_GameMgr + 0x1F458);

                long winner = better_side(ecx);
                CALL_2(VOID, __thiscall, 0x468F80, ecx, 1 - winner);

                *(char*)(o_GameMgr + 0x1F69C) = is_cheater_scenario;
                *(char*)(o_GameMgr + 0x1F458) = is_cheater_campaign;
            }
        }
    }
    return CALL_1(char, __thiscall,h->GetDefaultFunc(),ecx);
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        globalPatcher = GetPatcher();
        H3FasterAI = globalPatcher->CreateInstance(PINSTANCE_MAIN);
        H3FasterAI->WriteHiHook(0x0056ADA0, SPLICE_, EXTENDED_, STDCALL_, sub_0056ADA0_hook);
        H3FasterAI->WriteLoHook(0x00402395, z_timer_reset_hook);
        H3FasterAI->WriteHiHook(0x0041E6F0, SPLICE_, EXTENDED_, THISCALL_, sub_0041E6F0_hook);

        Era::ConnectEra();
        Era::RegisterHandler(big_timer_reset, "OnEveryDay");

        Era::RegisterHandler(z_timer_reset, "OnEveryDay");
        Era::RegisterHandler(z_timer_reset, "OnAfterBattleUniversal");
        Era::RegisterHandler(z_timer_reset, "OnHeroMove");
        Era::RegisterHandler(z_timer_reset, "OnHeroGainLevel");
        // Era::RegisterHandler(z_timer_reset, "OnChat");
        Era::RegisterHandler(z_timer_reset, "OnBeforeBattleUniversal");

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

