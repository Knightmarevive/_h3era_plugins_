#include "emerald.h"
//extern GAMEDATA2 no_save;

int SuperVulnerabilityArt;
int SuperNegativityArt;

int NormalFrostArt;
int SuperFrostArt;

int NormalEarthArt;
int SuperEarthArt;

// #include "lib/H3API.hpp"
#include "../../__include__/H3API/single_header/H3API.hpp"
using namespace h3;

#define o_Spell ( *reinterpret_cast<h3::H3Spell**>(0x687FA8))
// extern double AbsorbDamage(int id, int customdata, int spell);

extern "C" __declspec(dllexport) double ArtifactResistanceEffect(int spell, int creature, h3::H3Hero* caster, h3::H3Hero* victim) {
	if (caster && caster != victim && HasArtifact(caster, SuperVulnerabilityArt)) return 1.01;
	//if (caster && caster != victim && (spell == 17|| spell == 19) &&HasArtifact(caster, SuperNegativityArt)) o = 1.0;
	//if (caster && caster != victim && (spell == 16 || spell == 20) && HasArtifact(caster, SuperForstArt)) o = 1.0;

	
	if (victim /* && (caster != victim) */ )
	{
		if ((spell == 17 || spell == 19) && (HasArtifact(victim, SuperNegativityArt) || HasArtifact(victim, 106))) 
			return 0.0;
		if ((spell == 16 || spell == 20) && (HasArtifact(victim, SuperFrostArt) || HasArtifact(victim, NormalFrostArt)))
			return 0.0;
		if ((spell == 23 || spell == 18) && (HasArtifact(victim, SuperEarthArt) || HasArtifact(victim, NormalEarthArt)))
			return 0.0;
	}

	if (caster && (caster != victim))
	{
		if ((spell == 17 || spell == 19) && HasArtifact(caster, SuperNegativityArt))
			return 1.01;
		if ((spell == 16 || spell == 20) && (HasArtifact(caster, SuperFrostArt)))
			return 1.01;
		if ((spell == 23 || spell == 18) && (HasArtifact(caster, SuperEarthArt)))
			return 1.01;
	}
	
	return std::numeric_limits<double>::quiet_NaN();
}


inline h3::H3CombatSquare* GetHexStr(int Pos)
{
	h3::H3CombatSquare* BPDummy;
	if ((Pos < 0) || (Pos > 0xBA)) return(0);
		__asm {
		mov    eax, Pos
		lea    ecx, [8 * eax]
		sub    ecx, eax
		shl    ecx, 4
		mov    eax, 0x699420 //-> CombatManager
		mov    eax, [eax]
		lea    eax, [ecx + eax]
		add    eax, 0x1C4
		mov    BPDummy, eax
	}
		return(BPDummy);
}

#ifndef Disable_ResistanceHook
double __stdcall ResistanceHook(HiHook* h, int spell, int creature, h3::H3Hero* caster, h3::H3Hero* victim)
{
	if (spell == 35) { 
		if (creature < 0 || creature>1024) {
			Era::ExecErmCmd("IF:M^Warning: Ambignous target of dispell^");
			creature = 139;
		}
		return  CALL_4(double, __fastcall, h->GetDefaultFunc(), spell, creature, caster, victim); 
	}

	//�������� �������� �������� ��� 
	// if (spell == .. && victim && HasArtifact(victim, ..) return 0.0; else return 1.0;
	if (creature < 0 || creature>1024) {
		char code[512] = {};
		sprintf(code, "IF:M^emerald: resistance.cpp - wrong creature number #%i \n spell is #%i, caster hero is #%i \n prepare for crash^", creature, spell, (long) caster);
		Era::ExecErmCmd(code);
		//creature = 139;

		return 0.0;
	}

	if (isReceptive && isReceptive(creature) &&
		(o_Spell[spell].defensive || o_Spell[spell].friendlyMass))
		return 1.01;
	/*
	auto batman = h3::H3CombatManager::Get(); 
	auto heroes = batman->hero; 
	auto caster2 = heroes[batman->currentActiveSide ];
	auto victim_hex = GetHexStr(batman->actionTarget);
	auto victim_stack = victim_hex ? victim_hex->GetCreature() : nullptr;
	auto victim2 = victim_stack ? heroes[victim_stack->side] : nullptr;
	*/
	double artEffect = ArtifactResistanceEffect(spell, creature,  caster, victim);
	if (!isnan(artEffect)) return artEffect;

	double o = CALL_4(double, __fastcall, h->GetDefaultFunc(), spell, creature, caster, victim);
	if (caster && caster != victim&& HasArtifact(caster, SuperVulnerabilityArt)) o=1.0; 
	//if (caster && caster != victim && (spell == 17|| spell == 19) &&HasArtifact(caster, SuperNegativityArt)) o = 1.0;
	//if (caster && caster != victim && (spell == 16 || spell == 20) && HasArtifact(caster, SuperForstArt)) o = 1.0;

	/*
	if (victim && caster != victim)
	{
		if ((spell == 17 || spell == 19) && HasArtifact(victim, SuperNegativityArt) || HasArtifact(victim, 106)) return 0.0;
		if ((spell == 16 || spell == 20) && (HasArtifact(victim, SuperFrostArt) || HasArtifact(victim, NormalFrostArt))) return 0.0;
		if ((spell == 23 || spell == 18) && (HasArtifact(victim, SuperEarthArt) || HasArtifact(victim, NormalEarthArt))) return 0.0;
	}

	if (caster && caster != victim)
	{
		if ((spell == 17 || spell == 19) && HasArtifact(caster, SuperNegativityArt)) return 1.0;
		if ((spell == 16 || spell == 20) && (HasArtifact(caster, SuperFrostArt))) return 1.0;
		if ((spell == 23 || spell == 18) && (HasArtifact(caster, SuperEarthArt))) return 1.0;
	}
	*/

	if (victim)
	{

		
		for (int i=0; i!=19; i++)
		{
			// auto id = victim->IArt[i][0];
			auto id = victim -> bodyArtifacts[i].id;
			if(id!=-1)
			{
				// o-=AbsorbDamage(victim->IArt[i][0],victim->IArt[i][1],spell);
				o -= no_save.spell_immunity[id][spell];
			}
		}
	}
	
	/*
	if (isReceptive && isReceptive(creature) &&
		(o_Spell[spell].defensive || o_Spell[spell].friendlyMass))
		return 1.01;
	*/
	return o<0.0?0.0:o;
}

// 0071DC31
double __stdcall wog_CrExpBon__ApplySpell(HiHook* h, h3::H3CombatCreature* Mon, h3::H3CombatManager* BatMan) {

	if (BatMan == 0) {
		__asm {
			mov   eax, 0x699420
			mov   eax, [eax]
			mov   BatMan, eax
		}
	}

	return CALL_2(double, __cdecl, h->GetDefaultFunc(), Mon, BatMan);
}

// 0071CC3B
int __stdcall wog_CrExpBon__DwarfResist(HiHook* h, h3::H3CombatCreature* Mon, int Res, int Spell) {

	/*
	long BatMan;
	__asm {
		mov   eax, 0x699420
		mov   eax, [eax]
		mov   BatMan, eax
	}
	INT32 currentActiveSide = *(int*)(BatMan + 0x132C0);
	INT32 opponentActiveSide = 1 - currentActiveSide;

	INT32 currentHero = ;// *(int*)(BatMan + 0x53CC + currentActiveSide * 4);
	INT32 opponentHero = ;// *(int*)(BatMan + 0x53CC + opponentActiveSide * 4);
	*/

	/*
	auto batman = h3::H3CombatManager::Get();
	INT32 currentActiveSide = batman->currentActiveSide;
	INT32 opponentSide = 1 - currentActiveSide;

	auto currentHero = batman->hero[currentActiveSide];
	auto opponentHero = batman->hero[opponentSide];

	auto victim = batman->hero[Mon->side];

	double artEffect = ArtifactResistanceEffect(Spell, Mon->type, currentHero, victim);
	if (!isnan(artEffect)) return artEffect*100;
	*/

	/*
	if (currentHero)
	{
		if ((Spell == 17 || Spell == 19) && HasArtifact(currentHero, SuperNegativityArt) || HasArtifact(currentHero, 106)) return 100;
		if ((Spell == 16 || Spell == 20) && (HasArtifact(currentHero, SuperFrostArt) || HasArtifact(currentHero, NormalFrostArt))) return 100;
		if ((Spell == 23 || Spell == 18) && (HasArtifact(currentHero, SuperEarthArt) || HasArtifact(currentHero, NormalEarthArt))) return 100;
	}

	if (opponentHero)
	{
		if ((Spell == 17 || Spell == 19) && HasArtifact(opponentHero, SuperNegativityArt)) return 0;
		if ((Spell == 16 || Spell == 20) && (HasArtifact(opponentHero, SuperFrostArt))) return 0;
		if ((Spell == 23 || Spell == 18) && (HasArtifact(opponentHero, SuperEarthArt))) return 0;
	}
	*/

	/*
	if (currentHero) {
		if (HasArtifact((HERO*)currentHero, SuperVulnerabilityArt))
			return Res;

		if ((Spell == 17 || Spell == 19) && HasArtifact((void*)currentHero, SuperNegativityArt)) return Res;
		if ((Spell == 16 || Spell == 20) && HasArtifact((void*)currentHero, SuperFrostArt)) return Res;
		if ((Spell == 23 || Spell == 18) && HasArtifact((void*)currentHero, SuperEarthArt)) return Res;
	}
	*/

	return CALL_3(int, __cdecl, h->GetDefaultFunc(), Mon, Res, Spell);
}

//005A83A0
double __stdcall BattleMgr_CreatureMagicResist_hook(HiHook* H, h3::H3CombatManager* ecx, int Spell, int side, h3::H3CombatMonster* targetMon, int a5, int a6, int noHero) {
	auto ret = CALL_7(double, __thiscall, H->GetDefaultFunc(), ecx, Spell, side, targetMon, a5, a6, noHero);
	if (!targetMon) return ret;
	// bool own = (targetMon->side == side);
	bool own = (targetMon->side == ecx->currentActiveSide);

	/*
	auto currentHero = ecx->hero[side];
	auto opponentHero = ecx->hero[1-side];
	auto victim = ecx->hero[targetMon->side];

	double artEffect = ArtifactResistanceEffect(Spell, targetMon->type, currentHero, targetMon->GetOwner());
	if (!isnan(artEffect)) return artEffect;
	*/

	/*
	if (currentHero && own)
	{
		if ((Spell == 17 || Spell == 19) && HasArtifact(currentHero, SuperNegativityArt) || HasArtifact(currentHero, 106)) return 0.0;
		if ((Spell == 16 || Spell == 20) && (HasArtifact(currentHero, SuperFrostArt) || HasArtifact(currentHero, NormalFrostArt))) return 0.0;
		if ((Spell == 23 || Spell == 18) && (HasArtifact(currentHero, SuperEarthArt) || HasArtifact(currentHero, NormalEarthArt))) return 0.0;
	}

	if (opponentHero && !own)
	{
		if ((Spell == 17 || Spell == 19) && HasArtifact(opponentHero, SuperNegativityArt)) return 1.0;
		if ((Spell == 16 || Spell == 20) && (HasArtifact(opponentHero, SuperFrostArt))) return 1.0;
		if ((Spell == 23 || Spell == 18) && (HasArtifact(opponentHero, SuperEarthArt))) return 1.0;
	}
	*/

	return ret;
}

#endif