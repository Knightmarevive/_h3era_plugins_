#define _CRT_SECURE_NO_WARNINGS
#include "emerald.h"
// #include <bitset>

// #include "lib\H3API.hpp"
#include "../../__include__/H3API/single_header/H3API.hpp"

#include "Reserve.h"

#ifndef THISCALL_1
#define THISCALL_1(return_type, address, a1) ((return_type(__thiscall *)(UINT))(address))((UINT)(a1))
#endif

#ifndef FASTCALL_2
#define FASTCALL_2(return_type, address, a1, a2) ((return_type(__fastcall *)(UINT, UINT))(address))((UINT)(a1), (UINT)(a2))
#endif

// extern int IsArtifactAllowWaterWalk(int id, int customdata);
// extern int IsArtifactAllowFly(int id, int customdata);

void* ArtGiveSpell = (void*) 0x004D95C0;
void* Exception1 = (void*)	0x004D1950;

int SpeedUP; int is_6th_misc_slot_enabled = 1;

extern HMODULE Grandmaster_Magic;
// typedef h3::H3Bitfield[5] SpellBitset;

extern char art_does_not_exist[2000];
char can_be_duplicate[NEW_ARTS_AMOUNT] = {};
int Creature2Artifact_cache[1024] = {};
int Creature2Artifact_2_cache[1024] = {};

void do_cache_Creature2Artifact() {
	for (int i = 0; i < 1024; ++i) {
		if (Creature2Artifact) {
			Creature2Artifact_cache[i] = Creature2Artifact(i);
			if (Creature2Artifact_cache[i]) 
				can_be_duplicate[Creature2Artifact_cache[i]] = 1;
		}
		if (Creature2Artifact_2) {
			Creature2Artifact_2_cache[i] = Creature2Artifact_2(i);
			if (Creature2Artifact_2_cache[i]) 
				can_be_duplicate[Creature2Artifact_2_cache[i]] = 1;
		}
	}
}

#ifndef Disable_Emerald_duplicate_cpp

inline int get_ERMVarH(int hero_id, int index) {
	return *(int*)(hero_id * 0x320 + index * 4 + 0xA4AB0C);
}

void SoulGiveSpell(HERO* hero) {
	if (SpeedUP) return;

	for (int art = 0; art < STORED_ARTS_AMOUNT; art++)
		if(save.SoulBound_Artifacts[hero->Number][art]
			&& EmeraldArtNewTable[art].spellflag)
	{

			_SpellBitset_ spells;
			FASTCALL_2(_SpellBitset_, 0x4D95C0, &spells, art);

			for (auto i = 0; i < 140; ++i)
			{
				if (Grandmaster_Magic) {
					if (spells[i] && !hero->Spell[i])
						hero->Spell[i] = 2;
				}
				else {
					if (spells[i] && i < 70)
						hero->LSpell[i] = true;
				}
			}
	}
}

void MonGiveSpell(HERO* hero) {
	//return;

	// SpellBitset spells;
	long &dword_27F9988 = *(long*)(0x27F9988);
	long tmp = dword_27F9988;

	if (Creature2Artifact) {
		_SpellBitset_ spells;
		for (int i = 0; i < 7; i++) {
			// int art = Creature2Artifact(hero->Ct[i]);
			int art = Creature2Artifact_cache[hero->Ct[i]];
			if (art > 0 && EmeraldArtNewTable[art].spellflag) {

				FASTCALL_2(_SpellBitset_&, 0x4D95C0, &spells, art);

				for (auto i = 0; i < 140; ++i)
				{

					if (Grandmaster_Magic) {
						if (spells[i] && !hero->Spell[i])
							hero->Spell[i] = 2;
					}
					else {
						if (spells[i] && i < 70)
							hero->LSpell[i] = true;
					}
				}
			}
		}

		if (auto reserve = getBonusSlots((h3::H3Hero*)hero)) {
			for (int i = 0; i < 8; ++i) {
				//int art = Creature2Artifact(reserve[i].type);
				int art = Creature2Artifact_cache[reserve[i].type];
				if (art > 0 && EmeraldArtNewTable[art].spellflag) {

					FASTCALL_2(_SpellBitset_&, 0x4D95C0, &spells, art);

					for (auto i = 0; i < 140; ++i)
					{

						if (Grandmaster_Magic) {
							if (spells[i] && !hero->Spell[i])
								hero->Spell[i] = 2;
						}
						else {
							if (spells[i] && i < 70)
								hero->LSpell[i] = true;
						}
					}
				}
			}
		}

		/*
		dword_27F9988 = hero->Number; 
		long tmp_v3 = Era::v[3]; long tmp_v2 = Era::v[2];
		ExecErmSequence("!!VRv3:Sw118; !!VRv2:Sw119;");
		int art = Creature2Artifact(Era::v[3]);
		if (art > 0 && Era::v[2] && EmeraldArtNewTable[art].spellflag) {

			FASTCALL_2(_SpellBitset_&, 0x4D95C0, &spells, art);

			for (auto i = 0; i < 140; ++i)
			{

				if (Grandmaster_Magic) {
					if (spells[i] && !hero->Spell[i])
						hero->Spell[i] = 2;
				}
				else {
					if (spells[i] && i < 70)
						hero->LSpell[i] = true;
				}
			}
		}
		Era::v[3] = tmp_v3; Era::v[2] = tmp_v2;
		*/
	}

	if (Creature2Artifact_2) {
		_SpellBitset_ spells;
		for (int i = 0; i < 7; i++) {
			//int art = Creature2Artifact_2(hero->Ct[i]);
			int art = Creature2Artifact_2_cache[hero->Ct[i]];
			if (art > 0 && EmeraldArtNewTable[art].spellflag) {

				FASTCALL_2(_SpellBitset_&, 0x4D95C0, &spells, art);
				for (auto i = 0; i < 140; ++i)
				{

					if (Grandmaster_Magic) {
						if (spells[i] && !hero->Spell[i])
							hero->Spell[i] = 2;
					}
					else {
						if (spells[i] && i < 70)
							hero->LSpell[i] = true;
					}

				}
			}
		}

		if (auto reserve = getBonusSlots((h3::H3Hero*)hero)) {
			for (int i = 0; i < 8; ++i) {
				//int art = Creature2Artifact_2(reserve[i].type);
				int art = Creature2Artifact_2_cache[reserve[i].type];
				if (art > 0 && EmeraldArtNewTable[art].spellflag) {

					FASTCALL_2(_SpellBitset_&, 0x4D95C0, &spells, art);

					for (auto i = 0; i < 140; ++i)
					{

						if (Grandmaster_Magic) {
							if (spells[i] && !hero->Spell[i])
								hero->Spell[i] = 2;
						}
						else {
							if (spells[i] && i < 70)
								hero->LSpell[i] = true;
						}
					}
				}
			}
		}

		/*
		dword_27F9988 = hero->Number;
		long tmp_v3 = Era::v[3]; long tmp_v2 = Era::v[2];
		ExecErmSequence("!!VRv3:Sw118; !!VRv2:Sw119;");
		int art = Creature2Artifact_2(Era::v[3]);
		if (art > 0 && Era::v[2] && EmeraldArtNewTable[art].spellflag) {

			FASTCALL_2(_SpellBitset_&, 0x4D95C0, &spells, art);

			for (auto i = 0; i < 140; ++i)
			{
				
					if (Grandmaster_Magic) {
						if (spells[i] && !hero->Spell[i])
							hero->Spell[i] = 2;
					}
					else {
						if (spells[i] && i < 70)
							hero->LSpell[i] = true;
					}
			}
		}
		Era::v[3] = tmp_v3; Era::v[2] = tmp_v2;
		*/
	}

	dword_27F9988 = tmp;
}
void ArtGiveSpell_Proc(HERO* hero) {
	
	//MonGiveSpell(hero); // should be enough
	(reinterpret_cast<void(__thiscall*)(HERO*)>(0x004D9840))(hero);
}
void __stdcall ArtGiveSpell_Proc_Monster(HiHook* h, HERO* hero) {
	// 0x004D9840
	// for (int i = 0; i < 70; ++i) hero->LSpell[i] = 0;

	if (Grandmaster_Magic)
		Grandmaster_Magic_Update_Spells(hero);
	else
		THISCALL_1(int, h->GetDefaultFunc(), hero);
	// MonGiveSpell(hero); // ToDo

	//SoulGiveSpell(hero);
		//return ret;
	
}

/*
int __stdcall ArtGiveSpell_Proc_Monster(HiHook* h, HERO* hero) {
	// 0x004D9840
	if (Creature2Artifact) {
		
		
		for (int i = 0; i < 7; i++) {
			if (hero->Cn[i] <= 0) continue;
			int art = Creature2Artifact(hero->Ct[i]);
			if (art > 0 && EmeraldArtNewTable[art].spellflag) {
				__asm {
					pusha

					mov edx, art
					lea ecx, dword ptr ss : [ebp - 0x50]
					call ArtGiveSpell
					mov ecx, dword ptr ds : [eax]
					lea esi, dword ptr ds : [hero + 0x430]
					mov dword ptr ss : [ebp - 0x38] , edx 
					xor edi, edi
					mov     ecx, [eax + 4]
					mov		[ebp - 4], esi
					mov		[ebp -0x34], ecx
					mov     edx, [eax + 8]
					lea     eax, [ebp - 0x38]
					mov[ebp - 0x30], edx
					mov     edx, eax
					lea     eax, [hero + 0x476]
					mov		[ebp -0x24], edx
					cmp     esi, eax
					jz      loc_004D9930

					loc_004D98E8 :
					cmp     edi, 0x46
					jb      loc_004D98F7
					mov     ecx, edx
					call    Exception1
					mov     edx, [ebp -0x24]

					loc_004D98F7 :
					cmp     byte ptr[esi], 0
					jnz     loc_004D9916
					mov     ecx, edi
					mov     eax, 1
				    and		ecx, 1Fh
					shl     eax, cl
					mov     ecx, edi
					shr     ecx, 5
					test[edx + ecx * 4], eax
					jnz     loc_004D9916
					xor eax, eax 
					jmp     loc_004D991B
						
					loc_004D9916 : 
					mov     eax, 1
					
					loc_004D991B :
						mov     ecx, [ebp - 4]
						 inc     esi
						 inc     edi
						 mov[ecx], al
						 lea     eax, [hero + 476h]
						 inc     ecx 
						 cmp     esi, eax 
						 mov[ebp -4], ecx
						 jnz     loc_004D98E8

					loc_004D9930 :
					mov     eax, [ebp - 0x10]
					
					//mov edx, art
					//lea ecx, [hero + 0x3ea]
					//call ArtGiveSpell
					
					//mov edx, art
					//lea ecx, [hero + 0x430]
					//call ArtGiveSpell

					popa
				}
			}
		}
	}


	return THISCALL_1(int, h->GetDefaultFunc(), hero);
}
*/
/*
_LHF_(negativeArtSlots) {
	return EXEC_DEFAULT;

	if(c->ecx > 0 )
		return EXEC_DEFAULT;
	
	if (c->ecx <= -7) {
		c->return_address = 0x004D9A4B;
		return NO_EXEC_DEFAULT;
	}

	*(DWORD*)(c->ebp - 0x18) = c->ecx;

	HERO* hero = (HERO*)(c->ebx);
	int i = -c->ecx;
	int art = Creature2Artifact(hero->Ct[i]);
	if (hero->Cn[i] <= 0 || art <= 0) art = -1;
	c->edx = art;

	int   IArt[2]; *IArt = art;
	*(int*)(c->ebp - 0x14) = (int)IArt;

	c->return_address = 0x004D987A;
	return NO_EXEC_DEFAULT;
}
*/

/*
_LHF_(handleCreatureAsArt) {

	//HERO* hero = (HERO*)(c->ebx);
	//for (int i = 0; i < 70; ++i) hero->LSpell[i] = 0;

	return EXEC_DEFAULT;

	//char tmp=*(char*)(c->eax + c->ecx + 0x1d);

	int slot = *(int*) (c->ebp - 0x18);

	if (slot > 0) {
		//c->return_address = 0x004D98B2;
		return EXEC_DEFAULT;
	}
	if (slot <= -7) {
		//c->return_address = 0x004D9933;
		return EXEC_DEFAULT;
	}

	//HERO* hero = (HERO*)(c->ebx);
	int i = -slot;
	int art = Creature2Artifact(hero->Ct[i]);
	if (hero->Cn[i] <= 0 || art <= 0) art = -1;
	c->edx = art;

	int   IArt[2]; *IArt = art;	
	 *(int*)(c->ebp - 0x14) = (int) IArt;
	//c->return_address = 0x004D98B2;
	return EXEC_DEFAULT;
}
*/
int __stdcall NewAI_Player_CalcGettingArt_Value(HiHook* h, int* a1, int a2) {
	int art = *a1;
	if (art < 0) return false;
	if (art > 2000) return false;
	if (art_does_not_exist[art]) return false;
	int ret = CALL_2(int, __fastcall, h->GetDefaultFunc(), a1, a2);

	return ret;
}

char __stdcall NewMayArtBePlacedInSlotInsteadOfWhatWasThere(HiHook* h, HERO* hero, int art, int slot) {
	// 0x004E2AB0
	if (art < 0) return false;
	if (art > 2000) return false;
	if (art_does_not_exist[art]) return false;
	char ret = CALL_3(char, __thiscall, h->GetDefaultFunc(), hero, art, slot);

	return ret;
}

char __stdcall NewMayThisArtBePlacedToThisSlot(HiHook* h, HERO* hero, int art, int slot) {
	// 0x004E27C0
	if (art < 0) return false;
	if (art > 2000) return false;
	if (art_does_not_exist[art]) return false;
	char ret = CALL_3(char, __thiscall, h->GetDefaultFunc(), hero, art, slot);

	return ret;
}

_LHF_(hook_004E27CE) {
	int &art = *(int*)(c->ebp + 8);
	
	if (art < 0 || art > 2000 || art_does_not_exist[art]) {
		c->return_address = 0x004E2AA1;
		return SKIP_DEFAULT;
	}

	/*
	if (art == 3) {
		auto &zart = ((HERO*)c->ecx)->IArt[16][0];
		if (zart == 3) zart = -1;
		c->return_address = 0x004E2AA1;
		return SKIP_DEFAULT;
	}
	*/

	auto& zart = ((HERO*)c->ecx)->IArt[16][0];
	if (zart == 3) zart = -1;

	/*
	int slot = *(int*)(c->ebp + 0xC);
	if (slot == 16) {
		auto &z_art = no_save.newtable[art];
		if (z_art.slot == 18) {
			
		}
	}
	*/

	return EXEC_DEFAULT;
}

int __fastcall OldHasArtifact(HERO* hero, int art) {
	for (int* p = &hero->IArt[0][0]; p < &hero->IArt[19][0];++++p) {
		if (*p == art) return true;
	}
	return false;
}

int __stdcall NewHasArtifact(HiHook* h, HERO* hero, int art) // 0x4D9460
{
	if(art < 0) return false;
	if (art > 2000) return false; // 2021-01-23
	if (art_does_not_exist[art]) return false;
	if (!hero) return false;
	// if((int)hero>0x20000000)return false;
	if (hero->Number < 0 || hero->Number >= STORED_HERO_AMOUNT) return false;

	if (hero && save.SoulBound_Artifacts[hero->Number][art]) return true;
	int ret = CALL_2(int, __thiscall, h->GetDefaultFunc(), hero, art);
	if (art<0 || art >= NEW_ARTS_AMOUNT || !can_be_duplicate[art]) return ret;

	if (h->GetReturnAddress() < 0x700000 && h->GetReturnAddress() > 0x400000) //������� ������ SoD;
	{
		for (int i=0; i!=19; i++)
		{
			if (art==ARTIFACT_SPELLBOOK && hero->IArt[i][0]!=-1 &&  //���� ����� ����� �� �������, �� 
				GetArtifactRecord(hero->IArt[i][0])->rank & 0x40)	//�� ��������� ������� ���������� �� ���� ������� � ��������� ����� 
			{
				//sprintf(buf,"NewHasArtifact::Spellbook found at slot %i", i);
				//WriteConsoleA(GetStdHandle(STD_OUTPUT_HANDLE),(const void*)buf,64,0,0);
				return true;
			}

			/*
			if (art==ARTIFACT_ANGELWINGS && hero->IArt[i][0]!=-1 && 
				IsArtifactAllowFly(hero->IArt[i][0],hero->IArt[i][1]))
			{
				return true;
			}
			*/
			if (art == ARTIFACT_ANGELWINGS && no_save.allow_fly[hero->IArt[i][0]])
				return true;


			/*
			if (art==ARTIFACT_WATERBOOTS && hero->IArt[i][0]!=-1 && 
				IsArtifactAllowWaterWalk(hero->IArt[i][0],hero->IArt[i][1]))
			{
				return true;
			}
			*/
			if (art == ARTIFACT_WATERBOOTS && no_save.allow_water[hero->IArt[i][0]])
				return true;


		}
	}

	
	if (Creature2Artifact && art > 0) {
		for (int i = 0; i < 7; i++) {
			if (hero->Cn[i] <= 0) continue;
			if (Creature2Artifact_cache[hero->Ct[i]] == art)
				return true;
		}

		if (auto reserve = getBonusSlots((h3::H3Hero*)hero)) {
			for (int i = 0; i < 8; ++i) {
				if (reserve[i].type < 0) continue;
				if (Creature2Artifact_cache[reserve[i].type] == art)
					return true;
			}
		}
	}
	
	
	if (Creature2Artifact_2 && art > 0) {
		for (int i = 0; i < 7; i++) {
			if (hero->Cn[i] <= 0) continue;
			if (Creature2Artifact_2_cache[hero->Ct[i]] == art)
				return true;
		}
		if (auto reserve = getBonusSlots((h3::H3Hero*)hero)) {
			for (int i = 0; i < 8; ++i) {
				if (reserve[i].type < 0) continue;
				if (Creature2Artifact_2_cache[reserve[i].type] == art)
					return true;
			}
		}
	}

	long& dword_27F9988 = *(long*)(0x27F9988);
	long tmp = dword_27F9988;

	/*
	dword_27F9988 = hero->Number;
	long tmp_v3 = Era::v[3]; long tmp_v2 = Era::v[2];
	
	ExecErmSequence("!!VRv3:Sw118; !!VRv2:Sw119;");
	*/

	// return ret; // Debug: Do not access W-Vars

	int w118 = get_ERMVarH(hero->Number, 118);
	int w119 = get_ERMVarH(hero->Number, 119);

	if (Creature2Artifact && art > 0 && w119/*Era::v[2]*/) {
		//int art = Creature2Artifact(Era::v[1]);
		if (Creature2Artifact_cache[w118/*Era::v[3]*/] == art)
			return true;
	}

	if (Creature2Artifact_2 && art > 0 && w119/*Era::v[2]*/) {
		//int art = Creature2Artifact_2(Era::v[1]);
		if (Creature2Artifact_2_cache[w118/*Era::v[3]*/] == art)
			return true;
	}


	/*
	Era::v[3] = tmp_v3; Era::v[2] = tmp_v2;
	dword_27F9988 = tmp;
	*/
	return ret;
}

int __stdcall NewHasArtifactInBP(HiHook* h, HERO* hero, int art)
{
	if (art_does_not_exist[art]) return false;
	int ret = CALL_2(int, __thiscall, h->GetDefaultFunc(), hero, art);
	if (art < 0 || art >= NEW_ARTS_AMOUNT || !can_be_duplicate[art]) return ret;

	if (h->GetReturnAddress() < 0x700000 && h->GetReturnAddress() > 0x400000)//������� ������ SoD;
	{
		for (int i=0; i!=19; i++)
		{
			if (art==ARTIFACT_SPELLBOOK && hero->IArt[i][0]!=-1 && (GetArtifactRecord(hero->IArt[i][0])->rank & 0x40))
			{
				//sprintf(buf,"NewHasArtifactInBP::Spellbook found at slot %i, artid:%i rank: %x", i, hero->IArt[i][0], GetArtifactRecord(hero->IArt[i][0]));
				//WriteConsoleA(GetStdHandle(STD_OUTPUT_HANDLE),(const void*)buf,64,0,0);
				return true;
			}

		}

		for (int i=0; i!=64; i++)
		{
			if (art==ARTIFACT_SPELLBOOK && hero->OArt[i][0]!=-1  && (GetArtifactRecord(hero->OArt[i][0])->rank & 0x40))
			{
				//sprintf(buf,"NewHasArtifactInBP::Spellbook found at bp slot %i", i);
				//WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE),(const void*)buf,64,0,0);
				return true;
			}
		}

		//����������, ���������� ���������� ����� ���� � ��������������. 
		//��, �����, ��� ������ ���-�� �����������.
	}
	return ret;
}


int __stdcall ErmCountArtifactsHook(LoHook* h, HookContext* c) {
	HERO* me = (HERO*)*(int* )(c->ebp - 0x380); 
	int art = *(int*)(c->ebp - 0x3c8);

	int art_at_Creature = 0;

	if (Creature2Artifact && art > 0) {
		for (int i = 0; i < 7; i++) {
			if (me->Cn[i] <= 0) continue;
			if (Creature2Artifact_cache[me->Ct[i]] == art)
				art_at_Creature++;
		}
	}

	if (Creature2Artifact_2 && art > 0) {
		for (int i = 0; i < 7; i++) {
			if (me->Cn[i] <= 0) continue;
			if (Creature2Artifact_2_cache[me->Ct[i]] == art)
				art_at_Creature++;
		}
	}

	/*
	long& dword_27F9988 = *(long*)(0x27F9988);
	long tmp = dword_27F9988;

	dword_27F9988 = me->Number;
	long tmp_v3 = Era::v[3]; long tmp_v2 = Era::v[2];

	ExecErmSequence("!!VRv3:Sw118; !!VRv2:Sw119;");
	*/
	if (Creature2Artifact && art > 0 && get_ERMVarH(me->Number,119)/*Era::v[2]*/) {
		if (Creature2Artifact_cache[get_ERMVarH(me->Number, 118)/*Era::v[3]*/] == art)
			art_at_Creature++;
	}

	if (Creature2Artifact_2 && art > 0 && get_ERMVarH(me->Number, 119)/*Era::v[2]*/) {
		if (Creature2Artifact_2_cache[get_ERMVarH(me->Number, 118)/*Era::v[3]*/] == art)
			art_at_Creature++;
	}

	/*
	Era::v[3] = tmp_v3; Era::v[2] = tmp_v2;
	dword_27F9988 = tmp;
	*/

	*(int*)(c->ebp - 0x3d4) = save.SoulBound_Artifacts[me->Number][art] + art_at_Creature;
	c->return_address = 0x00744E32;
	return NO_EXEC_DEFAULT;
}

//====================================//
// Catapult as Misc

long dword_0063B940[19] = {17,15,14,13,18,16,12,11,10,9,8,7,6,5,4,3,2,1,0};
int tmp_0044CBFD[19] = {}; int newLockSlotToArtSlots[32] = {};
int misc_slot_flags[2] = {};

_LHF_(hook_0044CBFD) {
	c->Push(c->eax); c->Push(c->ecx);

	int* v62 = (int*)(c->ebp - 0x24);
	c->ecx = (int)v62;
//	c->ecx = (int)(tmp_0044CBFD);

	c->return_address = 0x0044CC02;
	return SKIP_DEFAULT;
}

_LHF_(hook_0044CC16) {
	c->edi = int(newLockSlotToArtSlots); // 0x006938E8;
	c->return_address = 0x0044CC1B;
	return SKIP_DEFAULT;
}

int tmp_0044CC1B[19] = {};
_LHF_(hook_0044CC1B) {
	int* v17 = (int*)c->edi; int& v18 = c->eax;
	int* v19 = (int*)c->edx; int* v20 = (int*)c->ecx;
	int* v62 = (int*)(c->ebp - 0x24);
	// int* v62 = tmp_0044CBFD;
	int& v64 = *(int*)(c->ebp - 0x1C);
	// int* v64 = tmp_0044CC1B;
	if(*v62 == misc_slot_flags[0]
		|| *v62 == misc_slot_flags[1])
		*v62 = misc_slot_flags[is_6th_misc_slot_enabled];

LABEL_20:
	v18 = 0;
	v19 = v62;
	v20 = v17;
	do
	{
		
		if (v20 - newLockSlotToArtSlots >= 31 * 4) {
			// v64 = newLockSlotToArtSlots+9;

			--v18;
			--v20;
			--v19;
			break;
		}
		

		if (*v19 != *v20) // ((*v19 & *v20) == 0)// (*v19 != *v20)
		{
			++v17;
			++v64;
			goto LABEL_20;
		}
		--v18;
		--v20;
		--v19;
	} while (v18 >= 0);

	c->return_address = 0x0044CC41;
	return SKIP_DEFAULT;
}

/*
_LHF_(hook_0044C986) {
	c->Push(18); c->Push(16);
	c->Push(12); c->Push(11);
	c->return_address = 0x0044C98C;
	return SKIP_DEFAULT;
}

_LHF_(hook_0044C995) {
	c->eax = c->ebp - 4;

	// c->Push(16);
	c->Push(6);

	c->return_address = 0x0044C99A;
	return SKIP_DEFAULT;
}

_LHF_(hook_0044C9E1) {
	// return EXEC_DEFAULT;

	c->ecx = c->ebp - 4;
	c->Push(0);

	c->return_address = 0x0044C9E8;
	return SKIP_DEFAULT;
}
*/

_LHF_(hook_005AFA8F) {
	int ArtAndMod = c->ecx; // *(int*)(c->ebp + 0xC);
	if (ArtAndMod == 16) 
		c->return_address = 0x005AFAA1;
	else if (ArtAndMod == 17) 
		c->return_address = 0x005AFB46;
	else c->return_address = 0x005AFAA1;
	return SKIP_DEFAULT;
}

_LHF_(hook_004DE117) {
	// c->return_address = 0x004DDE40;
	// c->return_address = 0x004DD5B9;
	c->return_address = 0x004DE144;
	return SKIP_DEFAULT;
}

void swapper_0x6938E8() {
	emerald->WriteDword(0x0044CEC6 + 3, int(newLockSlotToArtSlots));
	emerald->WriteDword(0x004DB240 + 3, int(newLockSlotToArtSlots));
	emerald->WriteDword(0x004E283F + 3, int(newLockSlotToArtSlots));
	emerald->WriteDword(0x004E288C + 3, int(newLockSlotToArtSlots));
	emerald->WriteDword(0x004E29B3 + 3, int(newLockSlotToArtSlots));
	emerald->WriteDword(0x004E2AEE + 3, int(newLockSlotToArtSlots));
	emerald->WriteDword(0x005AF2D6 + 3, int(newLockSlotToArtSlots));
}
int __stdcall NewSet_ArtTypeBits(HiHook* h) {
	int result; // eax
	int v1; // ecx
	int v2; // [esp+0h] [ebp-4h]

	auto ArtPos_SlotsMask = (int* (*)(int*,int,...))  0x0044C3F0;

	v2 = v1;
	CALL_2(int, __thiscall, 0x0044D0B0, &v2, 0); // Art_PosBits_Clear(&v2, 0);
	newLockSlotToArtSlots[0] = v2;
	newLockSlotToArtSlots[1] = *ArtPos_SlotsMask(&v2, 1u, 0);
	newLockSlotToArtSlots[2] = *ArtPos_SlotsMask(&v2, 1u, 1);
	newLockSlotToArtSlots[3] = *ArtPos_SlotsMask(&v2, 1u, 2);
	newLockSlotToArtSlots[4] = *ArtPos_SlotsMask(&v2, 1u, 3);
	newLockSlotToArtSlots[5] = *ArtPos_SlotsMask(&v2, 1u, 4);
	newLockSlotToArtSlots[6] = *ArtPos_SlotsMask(&v2, 1u, 5);
	newLockSlotToArtSlots[7] = *ArtPos_SlotsMask(&v2, 2u, 6, 7);
	newLockSlotToArtSlots[8] = *ArtPos_SlotsMask(&v2, 1u, 8);
	
	misc_slot_flags[0] = *ArtPos_SlotsMask(&v2, 5u, 9, 10, 11, 12, 18);
	misc_slot_flags[1] = *ArtPos_SlotsMask(&v2, 6u, 9, 10, 11, 12, 16, 18);
	newLockSlotToArtSlots[9] = misc_slot_flags[is_6th_misc_slot_enabled];
	//newLockSlotToArtSlots[9] = *ArtPos_SlotsMask(&v2, 5u, 9, 10, 11, 12, 18);
	//newLockSlotToArtSlots[9] = *ArtPos_SlotsMask(&v2, 6u, 9, 10, 11, 12, 16, 18);

	newLockSlotToArtSlots[10] = *ArtPos_SlotsMask(&v2, 1u, 13);
	newLockSlotToArtSlots[11] = *ArtPos_SlotsMask(&v2, 1u, 14);
	newLockSlotToArtSlots[12] = *ArtPos_SlotsMask(&v2, 1u, 15);
	newLockSlotToArtSlots[13] = *ArtPos_SlotsMask(&v2, 1u, 16);
	newLockSlotToArtSlots[14] = *ArtPos_SlotsMask(&v2, 1u, 17);
	// newLockSlotToArtSlots[14] = result;
	// return result;
	newLockSlotToArtSlots[15] = *ArtPos_SlotsMask(&v2, 2u, 3, 4);
	return true; // newLockSlotToArtSlots[14];
}


void art_existence();
_LHF_(artraits_hook_0044CEF9) {
	// no_save.ArtSetUpBack[3].slot = 0;
	art_existence();
	return EXEC_DEFAULT;
}

#endif // Disable_Emerald_duplicate_cpp