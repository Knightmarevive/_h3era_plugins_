// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.
#include "emerald.h"

#ifndef Disable_Emerald_Combos
/*
void EnableCombos(void) {
	emerald->WriteDword(0x717079,NEW_ARTS_AMOUNT); //BuildUpCombo


	emerald->WriteDword(0x4E2D3D, (int)EmeraldArtNewTable + 4 * NEW_ARTS_AMOUNT);
	emerald->WriteDword(0x4E2DD7, (int)EmeraldArtNewTable + 4 * NEW_ARTS_AMOUNT);
	emerald->WriteDword(0x4E2EAC, (int)EmeraldArtNewTable + 4 * NEW_ARTS_AMOUNT);
	emerald->WriteDword(0x4E2F41, (int)EmeraldArtNewTable + 4 * NEW_ARTS_AMOUNT);
}
*/

int (__thiscall  *RemoveArtFromHero)(HERO* , int ) = (int(__thiscall *)(HERO*, int)) 0x004E2E40;
char (__thiscall *AddArtToHero)(HERO* , int*, unsigned ) = (char(__thiscall*)(HERO*, int*, unsigned))0x004E2C70;
// char* (__cdecl * Exception_InvalidBitsetPosition_004346A0)(const void* a1, const void* a2, void* a3) = (char* (__cdecl *)(const void* a1, const void* a2, void* a3)) 0x004346A0;
char* Exception_InvalidBitsetPosition_004346A0(void) {
	ExecErmSequence((char*)"!!IF:M^Exception_InvalidBitsetPosition_004346A0^;");
	return nullptr;
}

constexpr DWORD combo_artifact_bitfield_dwords_count = 30;
// Combo art
struct _CArtSetup_ {
	int   Index;
	//Dword ArtNums[5];
	//unsigned ArtNums[30];
	unsigned ArtNums[combo_artifact_bitfield_dwords_count];
};

extern int __fastcall OldHasArtifact(HERO* hero, int art);
extern bool current_hero_has_art(int art_num);
constexpr size_t z_combo_size = sizeof(_CArtSetup_);
constexpr DWORD new_combo_size = 124;

extern PatcherInstance* emerald;
_CArtSetup_ new_CArtSetup[128];
_CArtSetup_ default_CArtSetup[]={
  {0x81,{0x80000000,0x0000001F,0x00000000,0x00000000,0x00000000}}, // 0
  {0x82,{0x00000000,0x01C00000,0x00000000,0x00000000,0x00000000}}, // 1
  {0x83,{0x00000000,0x00000000,0xC0000000,0x00000001,0x00000000}}, // 2
  {0x84,{0x04104100,0x00000000,0x00000000,0x00000000,0x00000000}}, // 3
  {0x85,{0x00000000,0x00000000,0x00000000,0x07C00000,0x00000000}}, // 4
  {0x86,{0x00000000,0x00003FE0,0x00000000,0x00000000,0x00000000}}, // 5
  {0x87,{0x41041000,0x00000000,0x00000000,0x00000000,0x00000000}}, // 6
  {0x88,{0x00000000,0x00000000,0x00000080,0x08000000,0x00000000}}, // 7
  {0x89,{0x00000000,0x70000000,0x00000000,0x00000000,0x00000000}}, // 8
  {0x8A,{0x00000000,0x00000000,0x00000E00,0x00000000,0x00000000}}, // 9
  {0x8B,{0x00000000,0x00000000,0x00007000,0x00000000,0x00000000}}, // 10
  {0x8C,{0x00000000,0x00000000,0x00000000,0x0002E000,0x00000000}}, // 11
//  { 158,{0x10410400,0x00000000,0x00000000,0x00000000,0x00000000}}, // 12
//  {0x00,{0x00000000,0x00000000,0x00000000,0x00000000,0x00000000}}
};

constexpr long allowed_combo_parts = combo_artifact_bitfield_dwords_count * 32;

byte z_dword_00692E68 [32 * NEW_ARTS_AMOUNT + 32];
long z_dword_00692E70 = 8 + (int)z_dword_00692E68;
long z_dword_00693770 = z_dword_00692E70+ NEW_ARTS_AMOUNT * 16;

#include <bitset>
// typedef std::bitset<140> _SpellBitset_;

extern HMODULE Grandmaster_Magic;
extern "C" __declspec(dllexport) void Combo_Add_Spells(void* hero, int ComboNum) {
	if (ComboNum < 0) return;
	_CArtSetup_* current_combo = new_CArtSetup + ComboNum;
	HERO* h = (HERO*)hero;
	h3::H3Bitfield* ArtNums = (h3::H3Bitfield *) &current_combo->ArtNums;

	for (int iArt = 0; iArt < allowed_combo_parts; ++iArt) {
		if (ArtNums->GetState(iArt) && no_save.ArtSetUpBack[iArt].spellflag) {
			_SpellBitset_ spells;
			CALL_2(int, __fastcall, 0x4D95C0, &spells, iArt);

			for (std::size_t iSpell = 0; iSpell < spells.size(); ++iSpell)
			{
				if(Grandmaster_Magic)
				{
					if (spells[iSpell] && !h->Spell[iSpell])
						h->Spell[iSpell] = 2;
				}
				else {
					h->LSpell[iSpell] = true;
				}
			}

		}
	}
}

static _CArtSetup_ BUC_Str;


void CleanUpCombo(int CInd)
{
	for (int i = 0; i < NEW_ARTS_AMOUNT; i++) {
		if (no_save.newtable[i].combonum == CInd) no_save.newtable[i].combonum = -1;
		if (no_save.newtable[i].partof == CInd) no_save.newtable[i].partof = -1;
	}
	new_CArtSetup[CInd].Index = 0;
	
}

void  BuildUpCombo(int CNum, int CInd, int ArtNums, int* Arts) {
	CleanUpCombo(CInd);
	memset(&BUC_Str,0, sizeof(BUC_Str));
	__asm {
		mov   eax, Arts
		push  dword ptr[eax + 13 * 4]
		push  dword ptr[eax + 12 * 4]
		push  dword ptr[eax + 11 * 4]
		push  dword ptr[eax + 10 * 4]
		push  dword ptr[eax + 9 * 4]
		push  dword ptr[eax + 8 * 4]
		push  dword ptr[eax + 7 * 4]
		push  dword ptr[eax + 6 * 4]
		push  dword ptr[eax + 5 * 4]
		push  dword ptr[eax + 4 * 4]
		push  dword ptr[eax + 3 * 4]
		push  dword ptr[eax + 2 * 4]
		push  dword ptr[eax + 1 * 4]
		push  dword ptr[eax + 0 * 4]
		push  ArtNums
		lea   eax, BUC_Str.ArtNums
		push  eax
		mov   eax, 0x44C500
		call  eax
		add   esp, 16 * 4
	}
	BUC_Str.Index = CNum;
	new_CArtSetup[CInd] = BUC_Str;
	// set up combo nums
	no_save.newtable[CNum].partof = CInd;

	char hasSpell = no_save.newtable[CNum].spellflag;
	for (int i = 0; i < ArtNums; i++) {
		no_save.newtable[Arts[i]].combonum = CInd;
		hasSpell |= no_save.newtable[Arts[i]].spellflag;
	} no_save.newtable[CNum].spellflag = hasSpell;
}

char __stdcall HasAnyBitSet_004E6730(HiHook* h, DWORD* ptr) {
	signed v1 = combo_artifact_bitfield_dwords_count - 1;
	auto v2 = ptr + v1;

	while (!*v2)
	{
		--v1;
		--v2;
		if (v1 < 0)
			return 0;
	}
	return 1;
}


int __stdcall Disassemble_ComboArt_004D9DA0(HiHook* h, int a1) {
	// if(!OldHasArtifact(, a1)) return 0;
	CALL_1(int, __stdcall , h->GetDefaultFunc(), a1);
	
	return 0;
}


int __stdcall BuildUpCombo_Hook(HiHook* h, int CNum, int CInd, int ArtNums, int* Arts) {


	if (CInd < 0 || CInd >= 127) return(0);
	if (CNum < 0 || CNum >= NEW_ARTS_AMOUNT) return(0);
	if (ArtNums < 1 || ArtNums>14) return(0);
	for (int i = 0; i < ArtNums; i++) {
		if (Arts[i] < 0 || Arts[i] >= 32 * combo_artifact_bitfield_dwords_count)
			return(0);
	}

	BuildUpCombo(CNum, CInd, ArtNums, Arts);
	return (1);
}

void __stdcall BuildUpNewComboArts_Hook(HiHook* h) {
	// memcpy(new_CArtSetup,default_CArtSetup,sizeof(default_CArtSetup) );
	memset(new_CArtSetup, 0, sizeof(new_CArtSetup));

	{
		int Arts[10] = { 34,32, 31 , 35, 33, 36 };
		BuildUpCombo(129, 0, 6, Arts);
	}

	{
		int Arts[10] = { 0x38, 0x37, 0x36 };
		BuildUpCombo(130, 1, 3, Arts);
	}

	{
		// int Arts[10] = { 8, 20, 26, 14};

		int Arts[10] = { 94,95,96 };
		BuildUpCombo(131, 2, 3, Arts);
	}

	{
		// int Arts[10] = { 0x7a, 0x79, 0x78, 0x77, 0x76 };

		int Arts[10] = { 8, 20, 26, 14 };
		BuildUpCombo(132, 3, 4, Arts);
	}

	{
		int Arts[10] = { 118, 119,120,121,122};
		BuildUpCombo(133, 4, 5, Arts);
	}

	{
		int Arts[10] = { 0x28, 0x29, 0x2d, 0x25, 0x27, 0x26, 0x2a, 0x2b, 0x2c };
		BuildUpCombo(134, 5, 9, Arts);
	}

	{
		int Arts[10] = { 30, 12, 24, 18 };
		BuildUpCombo(135, 6, 4, Arts);
	}

	{
		int Arts[10] = { 71, 123};
		BuildUpCombo(136, 7, 2, Arts);
	}

	{
		int Arts[10] = { 60, 61, 62 };
		BuildUpCombo(137, 8, 3, Arts);
	}

	{
		int Arts[10] = { 73, 74, 75};
		BuildUpCombo(138, 9, 3, Arts);
	}

	{
		int Arts[10] = { 76,77,78 };
		BuildUpCombo(139, 10, 3, Arts);
	}

	{
		int Arts[10] = { 109, 110, 111, 113 };
		BuildUpCombo(140, 11, 4, Arts);
	}



	{
		int Arts[10] = { 10,16,22,28 };
		BuildUpCombo(158, 12, 4, Arts);
	}

	// no_save.newbtable->def = 2;
}


_LHF_(z_SetComboSize_004DC310) {
	c->eax = c->esi * new_combo_size;
	c->esi = 0;
	c->ebx = c->ecx + c->eax + 4;

	c->return_address = 0x004DC319;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004DC635) {
	c->eax *= new_combo_size;
	c->edx = *(int*)(c->ecx + c->eax);
	c->return_address = 0x004DC63B;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004DC68F) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);

	c->return_address = 0x004DC695;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004DC6E3) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004DC6EE;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004DC742){

	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004DC748;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004DC79B)
{
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);

	c->return_address = 0x004DC7A1;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004DCDD5)
{
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);

	c->return_address = 0x004DCDDB;
	return NO_EXEC_DEFAULT;
}


_LHF_(z_SetComboSize_004DCE2F)
{
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);

	c->return_address = 0x004DCE35;
	return NO_EXEC_DEFAULT;
}


_LHF_(z_SetComboSize_004DCE83) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004DCE8E;
	return NO_EXEC_DEFAULT;
}


_LHF_(z_SetComboSize_004DCEE2) {

	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004DCEE8;
	return NO_EXEC_DEFAULT;
}


_LHF_(z_SetComboSize_004DCF3B)
{
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);

	c->return_address = 0x004DCF41;
	return NO_EXEC_DEFAULT;
}


_LHF_(z_004DD558) {
	c->esp -= 0x16c + 2 * new_combo_size;
	return EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004DDF77) {
	c->esi = 0;
	c->edi = c->ecx + c->edi *new_combo_size + 0x4; //ok

	c->return_address = 0x004DDF80;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004DDFF8) { //ok
	c->ecx = c->ebx * new_combo_size;
	c->edi = c->ebp - 0x168 - new_combo_size; // - 0x40;
	c->esi = c->edx + c->ecx + 0x4;

	c->ecx = combo_artifact_bitfield_dwords_count; //= 5;
	c->return_address = 0x004DE007;
	return NO_EXEC_DEFAULT;

}

_LHF_(z_004DE01E) {
	c->eax = c->ebp - 0x168 - new_combo_size *2;
	c->ecx = c->ebp - 0x168 - new_combo_size;
	c->return_address = 0x004DE024;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_004DE035) {
	if (c->flags.ZF) {
		c->ecx = c->ebp - 0x168 - new_combo_size;
		c->return_address = 0x004DE03A;
		return NO_EXEC_DEFAULT;
	}
	else {
		c->return_address = 0x004DE014;
		return NO_EXEC_DEFAULT;
	}
}


_LHF_(z_004E27C3) {
	c->esp -= 0x10 + new_combo_size;
	return EXEC_DEFAULT;}

_LHF_(z_004E2930) {
	c->ecx = c->ebp - 0x78 - new_combo_size;
	return EXEC_DEFAULT;
}

_LHF_(z_004E29BA) {
	c->edx = c->ebp - 0x78 - new_combo_size + c->eax *4;
	c->ebx = 0;
	c->return_address = 0x004E29C0;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E294F) { //ok
	c->eax = c->edi * new_combo_size;
	c->edi = c->esi = 0;
	c->ebx = c->eax + c->ecx + 0x4;

	c->return_address = 0x004E295A;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E2D1B) { //ok
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*) 0x00660B6C;
	c->esi = 0;
	c->ebx = (long) EmeraldArtNewBTable;
	c->ecx = c->eax + c->edx + 0x4;
	c->return_address = 0x004E2D2E;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_004E2E43) {
	c->esp -= 4 + new_combo_size;
	return EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E2E8A) { //ok
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->esi = 0;
	c->ebx = (long)EmeraldArtNewBTable;
	c->ecx = c->eax + c->edx + 0x4;
	c->return_address = 0x004E2E9D;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_004E32F8) {
	c->esp -= 0x18 + new_combo_size;
	return EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E335C) { //ok
	c->eax = c->ebx * new_combo_size;
	c->edi = c->ebp - 0x7c - new_combo_size;// - 0x38;
	*(int*)(c->ebp - 0x14) = c->eax;
	c->esi = c->eax + c->ecx + 0x4;
	c->ecx = combo_artifact_bitfield_dwords_count; //= 5;
	c->return_address = 0x004E3371;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_004E3385) {
	c->Push(0);
	c->Push(c->eax);
	c->ecx = c->ebp - 0x7c - new_combo_size;// - 0x38;
	c->return_address = 0x004E338B;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_004E3394) {
	if (c->flags.ZF) {
		c->ecx = c->ebp - 0x7c - new_combo_size;// - 0x38;
		c->return_address = 0x004E3399;
		return NO_EXEC_DEFAULT;
	}
	else {
		c->return_address = 0x004E337E;
		return NO_EXEC_DEFAULT;
	}
}


_LHF_(z_SetComboSize_004E3988) {
	c->eax *= new_combo_size; c-> eax += c->edx;
	c->eax = *(int*) c->eax;
	c->return_address = 0x004E398E;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E39CE) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E39D4;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E3A74) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E3A7A;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E3AAD) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*) 0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E3AB8;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E3AF0) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E3AF6;
	return NO_EXEC_DEFAULT;

}

_LHF_(z_SetComboSize_004E3B2D) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E3B33;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E3CCE) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E3CD4;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E3D0E) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E3D14;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E3D47) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E3D52;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E3D8B) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E3D91;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E3DC9) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E3DCF;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E3F02) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E3F08;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E3FD0) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E3FD6;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4011) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E401C;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E405D) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E4063;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4160) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E4166;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4249) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E424F;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4284) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E428A;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E42BD) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E42C8;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4378) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E437E;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E43B4) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E43BA;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E445F) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E4465;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E44A0) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E44AB;
	return NO_EXEC_DEFAULT;

}

_LHF_(z_SetComboSize_004E44ED) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E44F3;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E471F) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E4725;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4760) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E476B;
	return NO_EXEC_DEFAULT;

}

_LHF_(z_SetComboSize_004E47AD) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E47B3;
	return NO_EXEC_DEFAULT;

}

_LHF_(z_SetComboSize_004E487B) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E4881;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4909) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E490F;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E49DB) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E49E1;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4A1C) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E4A27;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4A69) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E4A6F;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4CD1) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E4CD7;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4DA4) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E4DAA;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4F78) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E4F7E;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E4FB6) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E4FC1;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5055) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E505B;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E508F) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E509A;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E50D2) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E50D8;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5112) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E5118;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E532E) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E5334;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E54FB) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E5501;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5A2A) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E5A30;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5A70) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E5A7B;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5ABD) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E5AC3;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5B00) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E5B06;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5D45) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E5D4B;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5D7F) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E5D8A;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5DC0) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E5DC6;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5E2E) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E5E34;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5E6A) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E5E75;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5EAD) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E5EB3;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E5F0D) {
	c->eax = *(int*)(c->edx + c->eax * new_combo_size);
	c->return_address = 0x004E5F13;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E6425) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E642B;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E661C) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004E6627;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004E669F) {
	c->edx = *(int*)(c->ecx + c->eax * new_combo_size);
	c->return_address = 0x004E66A5;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_005F1D4D) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	*(int*)(c->ebp - 0x20) = 0x0;
	c->edx = *(int*)(c->eax + c->edx +0x4);
	c->return_address = 0x005F1D60;
	return NO_EXEC_DEFAULT;

}

_LHF_(z_SetComboSize_005F2D47) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->ecx = 0;
	c->esi = 5;
	c->edi = (c->eax + c->edx + 0x4);
	c->return_address = 0x005F2D5A;
	return NO_EXEC_DEFAULT;

}

// ----------------------------------------------------------- //

_LHF_(z_SetComboSize_00433943) {
	c->ecx = c->eax * new_combo_size;
	c->esi = 0;
	c->edi = (long) z_dword_00692E70;// 0x692E70; //TODO
	c->eax = c->edx + c->ecx + 0x4;
	c->return_address = 0x00433951;
	return NO_EXEC_DEFAULT;

}

_LHF_(z_SetComboSize_004D94A3) {
	c->edx = c->eax * new_combo_size;
	c->eax = *(int*)0x00660B6C;
	c->edx = *(int*)(c->eax + c->edx);
	c->return_address = 0x004D94AE;
	return NO_EXEC_DEFAULT;

}

_LHF_(z_SetComboSize_004D994C) { //ok
	c->eax = c->eax * new_combo_size;
	c->esi = c->edi = 0;
	c->edx = c->ecx + c->eax + 0x4;
	c->return_address = 0x004D9957;
	return NO_EXEC_DEFAULT;
}


/*
_LHF_(z_004D9F48) {
	c->esp -= 0xC + new_combo_size;
	return EXEC_DEFAULT;
}
*/

_LHF_(z_SetComboSize_004D9F6C) {
	c->eax *= new_combo_size;
	c->ecx = c->ebp - 0x14;
	*(int*)(c->ebp - 0x10) = -1;
	c->eax = *(int*)(c->edx +c->eax);
	c->return_address = 0x004D9F7C;
	return NO_EXEC_DEFAULT;
}



_LHF_(z_004DC0F3) {
	c->esp -= new_combo_size;
	return EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004DC102) {
	c->eax *= new_combo_size;
	c->Push(c->esi);
	c->Push(c->edi);
	c->edi = c->ebp - 0x20 - new_combo_size; // - 0x14;
	c->esi = c->ecx + c->eax + 0x4;
	c->ecx = combo_artifact_bitfield_dwords_count; //= 5;
	c->return_address = 0x004DC113;
	return NO_EXEC_DEFAULT;
}


_LHF_(z_004DC141) {
	c->eax = c->edx * 4 + c->ebp - 0x20 - new_combo_size; // - 0x14;
	c->edx = 1;
	c->return_address = 0x004DC14A;
	return NO_EXEC_DEFAULT;
}


// ----------------------------------------------------------- //

_LHF_(z_004DC388) {
	c->esp -= 8 + new_combo_size;
	return EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004DC44E) {
	c->ebx *= new_combo_size;
	c->ecx = combo_artifact_bitfield_dwords_count; //= 5;
	//c->return_address = 0x004DC459;
	c->edi = c->ebp - 0x50 - new_combo_size;
	c->return_address = 0x004DC45C;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_004DC477) {
	c->Push(0);
	c->Push(c->eax);
	c->ecx = c->ebp - 0x50 - new_combo_size;
	c->return_address = 0x004DC47D;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_004DC486) {
	if (c->flags.ZF) {
		c->ecx = c->ebp - 0x50 - new_combo_size;
		c->return_address = 0x004DC48B;
		return NO_EXEC_DEFAULT;
	}
	else {
		c->return_address = 0x004DC470;
		return NO_EXEC_DEFAULT;
	}
}

_LHF_(z_Hook_Load_GameMap_004C2215) {
	c->Push(0);
	c->ecx = c->ebp - 0x5c; //  - 0xA0 - new_combo_size * 2; // -0x5c
	c->return_address = 0x004C221A;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_Hook_Load_GameMap_004C2230) {
	c->Push(0);
	c->ecx = c->ebp - 0xA0 - new_combo_size ;  // -0x24
	c->return_address = 0x004C2235;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_Hook_Load_GameMap_004C2248) {
	c->esi = 0;
	c->edi = c->ebp - 0xA0 - new_combo_size;  // -0x24
	c->return_address = 0x004C224D;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_Hook_Load_GameMap_004C226E) {

	return EXEC_DEFAULT;
}

_LHF_(z_Hook_Load_GameMap_004C2284) {
	c->esi = c->ebp - 0xA0 - new_combo_size ;  // -0x24
	c->edi = c->ebp - 0x5c; // - 0xA0 - new_combo_size * 2; // -0x5c
	memset((int*)c->edi, 0, new_combo_size);
	c->return_address = 0x004C228A;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_Hook_Load_GameMap_004C228C) {
	c->ecx = *(int*)(c->ebp - 0x10);
	c->eax = c->ebp - 0x5c; // - 0xA0 - new_combo_size * 2; // -0x5c
	c->return_address = 0x004C2292;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_Hook_Load_GameMap_004C22F6) {
	c->Push(c->edx);
	c->Push(c->edi);
	c->ecx = c->ebp - 0x5c; // 0xA0 - new_combo_size * 2; // -0x5c
	c->return_address = 0x004C22FB;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_Hook_Load_GameMap_004C2363) {
	c->edi = c->ebp - 0xA0 - new_combo_size * 3; // -0x84
	c->eax = c->ebp - 0x5c; //  - 0xA0 - new_combo_size * 2; // -0x5c
	memset((int*)c->edi, 0, new_combo_size);
	c->return_address = 0x004C236C;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_Hook_Load_GameMap_004C2370) {
	c->edi = c->ebp - 0xA0 - new_combo_size * 3; // -0x84
	c->esi = 0;
	*(int*)(c->ebp - 0x3c) = c->eax;
	*(int*)(c->ebp - 0x38) = c->ecx;
	c->return_address = 0x004C237E;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_Hook_Load_GameMap_004C237E){
	c->ecx = c->ebp - 0xA0 - new_combo_size * 3; // -0x84
	c->return_address = 0x004C2384;
	return NO_EXEC_DEFAULT;
}

void Hook_Load_GameMap(void) {

	emerald->WriteWord(0x004C228A, 0x9090);
	emerald->WriteWord(0x004C236C, 0x9090);

	emerald->WriteDword(0x0048E868 + 2, NEW_ARTS_AMOUNT);
	
	memset( (void*) 0x004C224D, 0x90, 0x004C227F - 0x004C224D);
	//memset( (void*) 0x004C22E4, 0x90, 0x004C235B - 0x004C22E4);
	//memset( (void*) 0x004C2394, 0x90, 0x004C23E9 - 0x004C2394);
	//memset( (void*) 0x004C230C, 0x90, 0x004C235B - 0x004C230C);
	//memset( (void*) 0x004C23B4, 0x90, 0x004C23C5 - 0x004C23B4);
	

	// memset( (void*) 0x00487CB3, 0x90, 0x00487D1A - 0x00487CB3);
	// memset((void*)0x487CB5, 0x90, 6);

	return;
	emerald->WriteLoHook(0x004C2215, z_Hook_Load_GameMap_004C2215);
	emerald->WriteLoHook(0x004C2230, z_Hook_Load_GameMap_004C2230);
	emerald->WriteLoHook(0x004C2248, z_Hook_Load_GameMap_004C2248);
	emerald->WriteLoHook(0x004C226E, z_Hook_Load_GameMap_004C226E);
	emerald->WriteLoHook(0x004C2284, z_Hook_Load_GameMap_004C2284);
	emerald->WriteLoHook(0x004C228C, z_Hook_Load_GameMap_004C228C);
	emerald->WriteLoHook(0x004C22F6, z_Hook_Load_GameMap_004C22F6);
	emerald->WriteLoHook(0x004C2363, z_Hook_Load_GameMap_004C2363);
	emerald->WriteLoHook(0x004C2370, z_Hook_Load_GameMap_004C2370);
	emerald->WriteLoHook(0x004C237E, z_Hook_Load_GameMap_004C237E);
}

_LHF_(z_0044C518) {
	c->esp -= 4 + new_combo_size*2;
	return EXEC_DEFAULT;
}

_LHF_(z_0044C51E) {
	c -> eax = c->ebp - 0x5c - new_combo_size; // -0x24
	*(int*)(c->ebp - 0x10) = c->esp;
	c->return_address = 0x0044C524;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_0044C5E8) {
	c->edi = 1;
	c->eax = c->ebp + c->eax * 4 - 0x5c - new_combo_size *2 + 8 ;// -0x34
	c->return_address = 0x0044C5F1;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_0044C605) {
	c->ecx = combo_artifact_bitfield_dwords_count;
	c->esi = c->ebp - 0x5c - new_combo_size*2 + 8; // -0x34
	c->return_address = 0x0044C60D;
	return NO_EXEC_DEFAULT;
}


_LHF_(z_0044C611) {
	c->ecx = c->ebp - 0x28 - new_combo_size;
	c->edi = c->Pop();
	c->esi = c->Pop();
	c->return_address = 0x0044C616;
	return NO_EXEC_DEFAULT;
}

void Hook_Art_Set_ComboArtBitmask(void) {
	emerald->WriteLoHook(0x0044C518, z_0044C518);
	emerald->WriteLoHook(0x0044C51E, z_0044C51E);
	emerald->WriteLoHook(0x0044C5E8, z_0044C5E8);
	emerald->WriteLoHook(0x0044C605, z_0044C605);
	// emerald->WriteLoHook(0x0044C611, z_0044C611);

}


_LHF_(z_004DC1A3) {
	c->esp -= 0x10 + new_combo_size * 2;
	return EXEC_DEFAULT;
}

_LHF_(z_SetComboSize_004DC1B3) {
	c->eax *= new_combo_size;
	c->Push(c->edi);
	c->edi = c->ebp - 0x28 - 2 * new_combo_size + 8;//0x1c;
	c->esi = c->eax + c->ecx + 0x4;
	c->ecx = combo_artifact_bitfield_dwords_count; //= 5;
	c->return_address = 0x004DC1C6;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_004DC1F5) {
	c->eax = c->edx * 4 + c->ebp - 0x28 - 2 * new_combo_size + 8;
	c->edx = 1;
	c->return_address = 0x004DC1FE;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_004DC21A) {
	c->eax = 4;
	c->ecx = c->ebp - 0x28 - new_combo_size;
	c->return_address = 0x004DC222;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_004DC262) {
	if (c->eax & *(int*)(c->esi * 4 + c->ebp - 0x28 - 2 * new_combo_size + 8))
	{
		c->return_address = 0x004DC268;
		return NO_EXEC_DEFAULT;
	}
	else
	{
		c->return_address = 0x004DC21A;
		return NO_EXEC_DEFAULT;
	}
}

_LHF_(z_004DC284) {
	c->Push(c->edi);
	c->eax = c->ecx * 4 + c->ebp - 0x28 - 2 * new_combo_size + 8;
	c->return_address = 0x004DC289;
	return NO_EXEC_DEFAULT;
}

/*
char __stdcall z_004DC1A0(HiHook* h, HERO* Hero, int comboInD, int SlotInd ) {
	unsigned int* v15; _CArtSetup_ tmp1 = new_CArtSetup[comboInD]; v15 = tmp1.ArtNums;
	if (SlotInd != -1) {
		int v4 = Hero->IArt[SlotInd][0];
		v15[v4 >> 5] &= ~(1 << (v4 & 0x1F));
		RemoveArtFromHero(Hero, SlotInd);
	}

}
*/

char __stdcall BuildComboArtIfHasAllParts_4DC1A0(HiHook* h, HERO* Hero, int combo_index, int slot_index)
{
	DWORD required_artifacts_bitset[combo_artifact_bitfield_dwords_count]; 
	memcpy(required_artifacts_bitset, new_CArtSetup[combo_index].ArtNums, sizeof(required_artifacts_bitset));
	if(1){
		int bitset_length = combo_artifact_bitfield_dwords_count - 1;
		auto bitset_position = &required_artifacts_bitset[combo_artifact_bitfield_dwords_count - 1];
		do
		{
			if (*bitset_position)
			{
				for (int i = 0; i < 32; ++i) {
					if ((*bitset_position) & (1ll << i)) {
						int id = (bitset_length << 5) + i;
						if (!OldHasArtifact(Hero, id))
							return 0;
					}
				}
			}

			--bitset_length;
			--bitset_position;
		} while (bitset_length >= 0);
	}
	
	if (slot_index != -1)
	{
		int art_id = Hero->IArt[slot_index][0];
		if (art_id >= combo_artifact_bitfield_dwords_count*32)
			Exception_InvalidBitsetPosition_004346A0();
		required_artifacts_bitset[art_id >> 5] &= ~(1 << (art_id & 0x1F));
		RemoveArtFromHero(Hero, slot_index);
	}
	int num_art_slots = 19;
	DWORD* current_art = (DWORD*) &Hero->FreeAddSlots;
EmptySlot:
	int bitset_length = combo_artifact_bitfield_dwords_count - 1;
	auto bitset_position = &required_artifacts_bitset[combo_artifact_bitfield_dwords_count-1];

	do
	{
		if (*bitset_position)
		{
			-- --current_art;
			auto _art_id_ = *current_art;
			--num_art_slots;
			auto curr_art = current_art;
			if (_art_id_ != -1)
			{
				if (_art_id_ >= combo_artifact_bitfield_dwords_count * 32)
				{
					Exception_InvalidBitsetPosition_004346A0();
					current_art = curr_art;
				}
				if (((1 << (_art_id_ & 0x1F)) & required_artifacts_bitset[_art_id_ >> 5]) != 0)
				{
					auto art_id_ = *current_art;
					if (*current_art >= combo_artifact_bitfield_dwords_count * 32)
						Exception_InvalidBitsetPosition_004346A0();
					required_artifacts_bitset[art_id_ >> 5] &= ~(1 << (art_id_ & 0x1F));
					RemoveArtFromHero(Hero, num_art_slots);
					current_art = curr_art;
				}
			}
			goto EmptySlot;
		}
		--bitset_length;
		--bitset_position;
	} while (bitset_length >= 0);

	DWORD artifact[2];
	artifact[0] = new_CArtSetup[combo_index].Index;
	artifact[1] = -1;
	auto ret = AddArtToHero(Hero, (int*) artifact, -1);

	if (1 && !OldHasArtifact(Hero, *artifact))
		THISCALL_3(char,0x004E3200,Hero, (int*)artifact,-1);
	return ret;
}

void Hook_BuildComboArtIfHasAllParts(void) {
	emerald->WriteHiHook(0x004DC1A0, SPLICE_, EXTENDED_, THISCALL_, BuildComboArtIfHasAllParts_4DC1A0);
	// emerald->WriteHiHook(0x004DC1A0, SPLICE_, EXTENDED_, THISCALL_, z_004DC1A0);
	return;
	emerald->WriteLoHook(0x004DC1A3, z_004DC1A3);
	emerald->WriteLoHook(0x004DC1B3, z_SetComboSize_004DC1B3);
	emerald->WriteLoHook(0x004DC1F5, z_004DC1F5);
	emerald->WriteLoHook(0x004DC21A, z_004DC21A);
	emerald->WriteLoHook(0x004DC262, z_004DC262);
	emerald->WriteLoHook(0x004DC284, z_004DC284);
	//
}

void early_CArtSetup(void) {

	// 2021-05-17
	emerald->WriteDword(0x0043246A + 1, allowed_combo_parts);
	emerald->WriteDword(0x00432495 + 1, allowed_combo_parts);

	emerald->WriteDword(0x00432471 + 1, (long)z_dword_00692E68);
	emerald->WriteDword(0x0043249C + 1, (long)z_dword_00692E68);
	emerald->WriteDword(0x004338DB + 2, (long)z_dword_00692E68);
	emerald->WriteDword(0x00434129 + 2, (long)z_dword_00692E68);

	emerald->WriteLoHook(0x00433943, z_SetComboSize_00433943);
	emerald->WriteDword(0x00434123 + 2, (long)z_dword_00692E70);
	emerald->WriteDword(0x00434596 + 1, (long)z_dword_00692E70);

	emerald->WriteDword(0x00433957 + 2, (long)z_dword_00693770);
	emerald->WriteDword(0x004339B1 + 2, (long)z_dword_00693770);
	emerald->WriteDword(0x004345E4 + 2, (long)z_dword_00693770);

	// memcpy((void*) z_dword_00692E70, (int*) 0x00692E70, 144 * 16);

}

void patch_CArtSetup(void) {
	// memcpy(new_CArtSetup, default_CArtSetup, sizeof(default_CArtSetup));

	emerald->WriteByte(0x44C630,0xC3); // short circuit function

	emerald->WriteDword(0x660B6C, (long)new_CArtSetup);

	emerald->WriteDword(0x706839 + 1, (long)new_CArtSetup);
	emerald->WriteDword(0x706905 + 1, (long)new_CArtSetup);
	emerald->WriteDword(0x706BA2 + 1, (long)new_CArtSetup);

	emerald->WriteDword(0x706BDF + 2, (long)new_CArtSetup);
	emerald->WriteDword(0x716FCF + 2, (long)new_CArtSetup);
	emerald->WriteDword(0x7170FA + 2, (long)new_CArtSetup);
	emerald->WriteDword(0x73281A + 2, (long)new_CArtSetup);

	emerald->WriteDword(0x706900 + 1, sizeof(new_CArtSetup));
	emerald->WriteDword(0x706834 + 1, sizeof(new_CArtSetup));

	emerald->WriteByte(0x7327EE + 3, 127);
	emerald->WriteByte(0x717005 + 3, 127);

	emerald->WriteByte(0x4E33DC + 2, 127);
	emerald->WriteByte(0x4E34B1 + 2, 127);
	emerald->WriteByte(0x4E353D + 2, 127);
	emerald->WriteByte(0x4DC3C4 + 2, 127);
	emerald->WriteByte(0x4DC418 + 2, 127);
	emerald->WriteByte(0x4DC49E + 2, 127);

	//emerald->WriteDword(0x4E2A5C + 2, 160 * 32);
	//emerald->WriteDword(0x4E2967 + 2, 160 * 32);

	// -------------------------------------------------------------------------------------------------------------------//
	//bigger bitfield
	DWORD bytes_24_to_124[] = {
		0x00732817 + 2,
		0x007170F7 + 2,
		0x00716FCC + 2,
		0x00706BDC + 2,
		0x00706BD1 + 2,
		//
		// 0x0044CD87 + 2,

		// 0x004DC403 + 2,
		0x0
	};
	for (DWORD* i = bytes_24_to_124; *i; ++i)
		emerald->WriteByte(*i, new_combo_size);
	
	DWORD bytes_6_to_31[] = {
		0x00717100 + 1,
		 0x00706BE5 + 1,
		 0x00706C0A + 1,

		0x0
	};
	for (DWORD* i = bytes_6_to_31; *i; ++i)
		emerald->WriteByte(*i, combo_artifact_bitfield_dwords_count +1);
	
	DWORD dwords_0x300_to_0x3d84[] = {
		// 0x00706B9D + 1, // todo: HiHook it
		0x0
	};
	for (DWORD* i = dwords_0x300_to_0x3d84; *i; ++i)
		emerald->WriteDword(*i, 0x7c * 0x7F);

	DWORD bytes_5_to_30[] = {
		// 0x0048BBAA + 1,
		// 0x0048E8AA + 1,
		0x0044C524 + 1,
		0x0
	};
	for (DWORD* i = bytes_5_to_30; *i; ++i)
		emerald->WriteByte(*i, combo_artifact_bitfield_dwords_count);
	
	DWORD dwords_0x160_to_3C0[] = {
		0x48E5AD + 2,
		0x4CF67A + 2,
		0x4C2277 + 2,
		0x4DC319 + 2,
		0x4DC127 + 2,
		0x4DC1DA + 2,
		0x4DC240 + 2,
		0x4DC26A + 2,
		0x44C549 + 2,
		0x4DDFA4 + 2,
		0x44D1A8 + 2,
		0x717076 + 3,
		0x0
	};
	for (DWORD* i = dwords_0x160_to_3C0; *i; ++i)
		emerald->WriteDword(*i, combo_artifact_bitfield_dwords_count*32);

	//emerald->WriteDword(0x004DC1B3, 0x907cc06b); emerald->WriteWord(0x004DC1B7, 0x9090);
	//emerald->WriteDword(0x004DC44E, 0x907cdb6b); emerald->WriteWord(0x004DC452, 0x9090);
	emerald->WriteDword(0x007170DA + 2, (long)&BUC_Str.ArtNums);
	emerald->WriteDword(0x007170EE + 2, (long)&BUC_Str.Index);
	// emerald->WriteByte(0x00717100 + 1, 31); 
	// 0x00706BDC + 2 ommitted by purporse

	emerald->WriteDword(0x4E2A5C + 2, 32 * combo_artifact_bitfield_dwords_count * sizeof(ART_RECORD));
	emerald->WriteDword(0x4E2967 + 2, 32 * combo_artifact_bitfield_dwords_count * sizeof(ART_RECORD));


	//2021-06-18
	emerald->WriteDword(0x00709FDE + 1, NEW_ARTS_AMOUNT/* allowed_combo_parts */);

}

_LHF_(hook_00745CD0) {
	int arg = *(long*)(c->ebp-0x3E8);
	if (arg < NEW_ARTS_AMOUNT)
		c->return_address =  0x00745D09; // 0x00745CE5;
	else c->return_address = 0x00745B81;// 0x00745D5F;

	return NO_EXEC_DEFAULT;
}

_LHF_(hook_00745BC1) {
	long  art_num = *(long*)(c->ebp - 0x3E8);
	char* art_prop = (char*)(no_save.newbtable + art_num);
	long* cum_prop = (long*)(c->ebp - 0x3F8);

	for (int j = 0; j < 4; j++)
		cum_prop[j] -= art_prop[j];

	c->return_address = 0x00745B81;
	return NO_EXEC_DEFAULT;
}

void hook_CArtSetup(void) {

	// emerald->WriteLoHook(0x00745CD0, hook_00745CD0);
	emerald->WriteLoHook(0x00745BC1, hook_00745BC1);

	emerald->WriteHiHook(0x004D9DA0, SPLICE_, EXTENDED_, STDCALL_, Disassemble_ComboArt_004D9DA0);

	emerald->WriteHiHook(0x004E6730, SPLICE_, EXTENDED_, THISCALL_, HasAnyBitSet_004E6730);

	emerald->WriteHiHook(0x00706B7F, SPLICE_, EXTENDED_, STDCALL_, BuildUpNewComboArts_Hook);

	emerald->WriteHiHook(0x00716FE5, SPLICE_, EXTENDED_, STDCALL_, BuildUpCombo_Hook);

	Hook_Load_GameMap();
	Hook_Art_Set_ComboArtBitmask();
	Hook_BuildComboArtIfHasAllParts();

	emerald->WriteLoHook(0x004DC310, z_SetComboSize_004DC310);
	emerald->WriteLoHook(0x004DC635, z_SetComboSize_004DC635);
	emerald->WriteLoHook(0x004DC68F, z_SetComboSize_004DC68F);
	emerald->WriteLoHook(0x004DC6E3, z_SetComboSize_004DC6E3);
	emerald->WriteLoHook(0x004DC742, z_SetComboSize_004DC742);
	emerald->WriteLoHook(0x004DC79B, z_SetComboSize_004DC79B);
	emerald->WriteLoHook(0x004DCDD5, z_SetComboSize_004DCDD5);
	emerald->WriteLoHook(0x004DCE2F, z_SetComboSize_004DCE2F);
	emerald->WriteLoHook(0x004DCE83, z_SetComboSize_004DCE83);
	emerald->WriteLoHook(0x004DCEE2, z_SetComboSize_004DCEE2);

	emerald->WriteLoHook(0x004DD558, z_004DD558);
	emerald->WriteLoHook(0x004DCF3B, z_SetComboSize_004DCF3B);
	emerald->WriteLoHook(0x004DDF77, z_SetComboSize_004DDF77);
	
	emerald->WriteLoHook(0x004DDFF8, z_SetComboSize_004DDFF8);
	emerald->WriteLoHook(0x004DE01E, z_004DE01E);
	emerald->WriteLoHook(0x004DE035, z_004DE035);

	emerald->WriteLoHook(0x004E27C3, z_004E27C3);
	emerald->WriteLoHook(0x004E2930, z_004E2930);
	emerald->WriteLoHook(0x004E294F, z_SetComboSize_004E294F);
	emerald->WriteLoHook(0x004E29BA, z_004E29BA);
	
	emerald->WriteLoHook(0x004E2D1B, z_SetComboSize_004E2D1B);

	emerald->WriteLoHook(0x004E2E43, z_004E2E43);
	emerald->WriteLoHook(0x004E2E8A, z_SetComboSize_004E2E8A);
	
	emerald->WriteLoHook(0x004E32F8, z_004E32F8);
	emerald->WriteLoHook(0x004E335C, z_SetComboSize_004E335C);
	emerald->WriteLoHook(0x004E3385, z_004E3385);
	emerald->WriteLoHook(0x004E3394, z_004E3394);
	
	emerald->WriteLoHook(0x004E3988, z_SetComboSize_004E3988);
	emerald->WriteLoHook(0x004E39CE, z_SetComboSize_004E39CE);
	emerald->WriteLoHook(0x004E3A74, z_SetComboSize_004E3A74);
	emerald->WriteLoHook(0x004E3AAD, z_SetComboSize_004E3AAD);
	emerald->WriteLoHook(0x004E3AF0, z_SetComboSize_004E3AF0);
	emerald->WriteLoHook(0x004E3B2D, z_SetComboSize_004E3B2D);
	emerald->WriteLoHook(0x004E3CCE, z_SetComboSize_004E3CCE);
	emerald->WriteLoHook(0x004E3D0E, z_SetComboSize_004E3D0E);
	emerald->WriteLoHook(0x004E3D47, z_SetComboSize_004E3D47);
	emerald->WriteLoHook(0x004E3D8B, z_SetComboSize_004E3D8B);
	emerald->WriteLoHook(0x004E3DC9, z_SetComboSize_004E3DC9);
	emerald->WriteLoHook(0x004E3F02, z_SetComboSize_004E3F02);
	emerald->WriteLoHook(0x004E3FD0, z_SetComboSize_004E3FD0);
	emerald->WriteLoHook(0x004E4011, z_SetComboSize_004E4011);
	emerald->WriteLoHook(0x004E405D, z_SetComboSize_004E405D);
	emerald->WriteLoHook(0x004E4160, z_SetComboSize_004E4160);
	emerald->WriteLoHook(0x004E4249, z_SetComboSize_004E4249);
	emerald->WriteLoHook(0x004E4284, z_SetComboSize_004E4284);
	emerald->WriteLoHook(0x004E42BD, z_SetComboSize_004E42BD);
	emerald->WriteLoHook(0x004E4378, z_SetComboSize_004E4378);
	emerald->WriteLoHook(0x004E43B4, z_SetComboSize_004E43B4);
	emerald->WriteLoHook(0x004E445F, z_SetComboSize_004E445F);
	emerald->WriteLoHook(0x004E44A0, z_SetComboSize_004E44A0);
	emerald->WriteLoHook(0x004E44ED, z_SetComboSize_004E44ED);
	emerald->WriteLoHook(0x004E471F, z_SetComboSize_004E471F);
	emerald->WriteLoHook(0x004E4760, z_SetComboSize_004E4760);
	emerald->WriteLoHook(0x004E47AD, z_SetComboSize_004E47AD);
	emerald->WriteLoHook(0x004E487B, z_SetComboSize_004E487B);
	emerald->WriteLoHook(0x004E4909, z_SetComboSize_004E4909);
	emerald->WriteLoHook(0x004E49DB, z_SetComboSize_004E49DB);
	emerald->WriteLoHook(0x004E4A1C, z_SetComboSize_004E4A1C);
	emerald->WriteLoHook(0x004E4A69, z_SetComboSize_004E4A69);
	emerald->WriteLoHook(0x004E4CD1, z_SetComboSize_004E4CD1);
	emerald->WriteLoHook(0x004E4DA4, z_SetComboSize_004E4DA4);
	emerald->WriteLoHook(0x004E4F78, z_SetComboSize_004E4F78);
	emerald->WriteLoHook(0x004E4FB6, z_SetComboSize_004E4FB6);
	emerald->WriteLoHook(0x004E5055, z_SetComboSize_004E5055);
	emerald->WriteLoHook(0x004E508F, z_SetComboSize_004E508F);
	emerald->WriteLoHook(0x004E50D2, z_SetComboSize_004E50D2);
	emerald->WriteLoHook(0x004E5112, z_SetComboSize_004E5112);
	emerald->WriteLoHook(0x004E532E, z_SetComboSize_004E532E);
	emerald->WriteLoHook(0x004E54FB, z_SetComboSize_004E54FB);
	emerald->WriteLoHook(0x004E5A2A, z_SetComboSize_004E5A2A);
	emerald->WriteLoHook(0x004E5A70, z_SetComboSize_004E5A70);
	emerald->WriteLoHook(0x004E5ABD, z_SetComboSize_004E5ABD);
	emerald->WriteLoHook(0x004E5B00, z_SetComboSize_004E5B00);
	emerald->WriteLoHook(0x004E5D45, z_SetComboSize_004E5D45);
	emerald->WriteLoHook(0x004E5D7F, z_SetComboSize_004E5D7F);
	emerald->WriteLoHook(0x004E5DC0, z_SetComboSize_004E5DC0);
	emerald->WriteLoHook(0x004E5E2E, z_SetComboSize_004E5E2E);
	emerald->WriteLoHook(0x004E5E6A, z_SetComboSize_004E5E6A);
	emerald->WriteLoHook(0x004E5EAD, z_SetComboSize_004E5EAD);
	emerald->WriteLoHook(0x004E5F0D, z_SetComboSize_004E5F0D);
	emerald->WriteLoHook(0x004E6425, z_SetComboSize_004E6425);
	emerald->WriteLoHook(0x004E661C, z_SetComboSize_004E661C);
	emerald->WriteLoHook(0x004E669F, z_SetComboSize_004E669F);
	emerald->WriteLoHook(0x005F1D4D, z_SetComboSize_005F1D4D);
	emerald->WriteLoHook(0x005F2D47, z_SetComboSize_005F2D47);

	// ----------------------------------------------------------- //

	/*
	// 2021-05-17
	emerald->WriteDword(0x0043246A + 1, allowed_combo_parts);
	emerald->WriteDword(0x00432495 + 1, allowed_combo_parts);

	emerald->WriteDword(0x00432471 + 1, (long)z_dword_00692E68);
	emerald->WriteDword(0x0043249C + 1, (long)z_dword_00692E68);
	emerald->WriteDword(0x004338DB + 2, (long)z_dword_00692E68);
	emerald->WriteDword(0x00434129 + 2, (long)z_dword_00692E68);

	emerald->WriteLoHook(0x00433943, z_SetComboSize_00433943);
	emerald->WriteDword(0x00434123 + 2, (long) z_dword_00692E70 );
	emerald->WriteDword(0x00434596 + 1, (long) z_dword_00692E70);

	emerald->WriteDword(0x00433957 + 2, (long)z_dword_00693770);
	emerald->WriteDword(0x004339B1 + 2, (long)z_dword_00693770);
	emerald->WriteDword(0x004345E4 + 2, (long)z_dword_00693770);

	// memcpy((void*) z_dword_00692E70, (int*) 0x00692E70, 144 * 16);
	*/

	// ----------------------------------------------------------- //

	emerald->WriteLoHook(0x004D94A3, z_SetComboSize_004D94A3);
	emerald->WriteLoHook(0x004D994C, z_SetComboSize_004D994C);
	emerald->WriteLoHook(0x004D9F6C, z_SetComboSize_004D9F6C);

	emerald->WriteLoHook(0x004DC0F3, z_004DC0F3);
	emerald->WriteLoHook(0x004DC102, z_SetComboSize_004DC102);
	emerald->WriteLoHook(0x004DC141, z_004DC141);

	// ----------------------------------------------------------- //

	emerald->WriteLoHook(0x004DC388, z_004DC388);
	emerald->WriteLoHook(0x004DC44E, z_SetComboSize_004DC44E);
	emerald->WriteLoHook(0x004DC477, z_004DC477);
	emerald->WriteLoHook(0x004DC486, z_004DC486);

	// ----------------------------------------------------------- //


}

#endif // Disable_Emerald_Combos