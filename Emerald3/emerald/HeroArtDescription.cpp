#include "lib/patcher_x86.hpp"
#include "MyTypes.h"


extern GAMEDATA save;
extern GAMEDATA2 no_save;
extern PatcherInstance* emerald;

char heroArttDescBuf[4096] = "Debug";
int artID = -1; 
h3::H3Hero* currentHero = nullptr;

extern int __fastcall OldHasArtifact(HERO* hero, int art);
constexpr DWORD combo_artifact_bitfield_dwords_count = 30;
// Combo art
struct _CArtSetup_ {
	int   Index;
	//Dword ArtNums[5];
	//unsigned ArtNums[30];
	unsigned ArtNums[combo_artifact_bitfield_dwords_count];
};
extern _CArtSetup_ new_CArtSetup[128];

int next_art_in_combo(_CArtSetup_* combo, int index) {

	while (1)
	{
		if (index >= 32 * combo_artifact_bitfield_dwords_count)
			return -1;
		if ((combo->ArtNums[index / 32]) & (1 << (index % 32)))
			return index;
		++index;
	}
	
}

/*
void set_art_string() {
	h3::H3String str = heroArttDescBuf;
}
*/

h3::H3Hero* DlgHeroInfo_Hero() {
	return
		(h3::H3Hero*)
		*(int*)	(0x698B70);
}


h3::H3Hero* SwapHeroInfo_Hero(int side) {
	return
		(h3::H3Hero*) *(int*)
		(*(int*)(0x6A3D90) +64 + side * 4 );
}

bool current_hero_has_art(int art_num) {
	if(!currentHero)
		return false;
	return OldHasArtifact((HERO*)currentHero,art_num);
	/*
	for (auto i : currentHero->bodyArtifacts)
		if (i.GetId() == art_num)
			return true;

	return false;
	*/
}

void fix_heroArttDescBuf() {

	if (artID >= 0 && artID < 1000) {
		sprintf(heroArttDescBuf, "%s", no_save.newtable[artID].desc);
		if ( /* currentHero && */ no_save.newtable[artID].combonum >= 0) {
			strcat(heroArttDescBuf, "\n\n Combo Parts: \n");
			int elem = next_art_in_combo(new_CArtSetup + no_save.newtable[artID].combonum ,0);
			if (elem >= 0) {
				int i = 0;
				while (elem >= 0) {
					if (current_hero_has_art(elem)) {
						strcat(heroArttDescBuf, "{~LightGreen}");
					} else {
						strcat(heroArttDescBuf, "{~Grey}");
					}
					strcat(heroArttDescBuf, no_save.newtable[elem].name); 
					
					strcat(heroArttDescBuf, "{~}");
					/*
					if(i++ & 1) strcat(heroArttDescBuf, "{~} \n");
					else strcat(heroArttDescBuf, "{~}        ");
					*/

					elem = next_art_in_combo(new_CArtSetup + no_save.newtable[artID].combonum, elem+1);
					if( elem>=0 ) strcat(heroArttDescBuf, /*",  "*/ "\n");

				}
			}
			else
			{
				char debug[512]; sprintf(debug, "debug %d %d",
					no_save.newtable[artID].combonum,
					no_save.newtable[artID].partof);
				strcat(heroArttDescBuf, debug);
			}
		}
		else *heroArttDescBuf = 0;
		// artID = -1;
	}
	else *heroArttDescBuf = 0;
}

_LHF_(hook_4DE0BF) {
	// OnHeroDoll_Rmc
	artID = *(int*)(c->ebp - 28);
	currentHero = DlgHeroInfo_Hero();

	// fix_heroArttDescBuf();
	// c->edx = (int)heroArttDescBuf;
	return EXEC_DEFAULT;
}


_LHF_(hook_4DE37B) {
	// OnHeroBackPack_Rmc
	artID = *(int*)(c->ebp - 20);
	currentHero = DlgHeroInfo_Hero();


	return EXEC_DEFAULT;
}

_LHF_(hook_5AFA5F) {
	// OnSwapHeroDoll_Rmc
	artID = *(int*)(c->ebp - 8);
	// currentHero = (h3::H3Hero*)  c->edi;
	currentHero = SwapHeroInfo_Hero(*(int*)(c->ebp+8));

	return EXEC_DEFAULT;
}

_LHF_(hook_5AFE0E) {
	// OnSwapHeroBackPack_Rmc
	artID = *(int*)(c->ebp - 20);
	currentHero = (h3::H3Hero*) c->esi;

	return EXEC_DEFAULT;
}


_LHF_(hook_51F5CB) {
	// OnKingdomHeroDoll_Rmc
	artID = c->eax;
	currentHero = (h3::H3Hero*)c->esi;
	
	return EXEC_DEFAULT;
}


_LHF_(hook_51F711) {
	// OnKingdomHeroBackPack_Rmc
	artID = c->eax;
	currentHero = (h3::H3Hero*)c->esi;
	
	return EXEC_DEFAULT;
}

_LHF_(hook_004DB693) {
	// BuildUpArtifactDescription
	if (artID >= 0 && artID < 1000)
	{
		fix_heroArttDescBuf();
		if (*heroArttDescBuf) {
			c->esi = (int)heroArttDescBuf;
			artID = -1;
		}
	}
	return EXEC_DEFAULT;
}

int EnableCompoundArtifactHints = true;
void HeroArtDescHooks() {

	if (!EnableCompoundArtifactHints) return;

	emerald->WriteLoHook(0x4DE0BF, hook_4DE0BF);
	emerald->WriteLoHook(0x4DE37B, hook_4DE37B);
	
	emerald->WriteLoHook(0x5AFA5F, hook_5AFA5F);
	emerald->WriteLoHook(0x5AFE0E, hook_5AFE0E);

	emerald->WriteLoHook(0x51F5CB, hook_51F5CB);
	emerald->WriteLoHook(0x51F711, hook_51F711);

	emerald->WriteLoHook(0x004DB693, hook_004DB693);
}