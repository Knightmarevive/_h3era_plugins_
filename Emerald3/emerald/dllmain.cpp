// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.

#define Z_MAIN
// #include "..\__include\era.h"
#include "emerald.h"
#include "..\__include\era.h"

// #include "lib/H3API.hpp"
#include "../../__include__/H3API/single_header/H3API.hpp"
using namespace h3;

// using Era::ExecErmCmd;
// using Era::RegisterHandler;

extern void install_SSkill_bonus();

extern void __stdcall  Emerald(PEvent e);
//extern void __stdcall  UndoEmerald(PEvent e);

extern void __stdcall ReallocProhibitionTables(PEvent e);
extern void __stdcall LoadConfigs(PEvent e);
// extern void __stdcall blank(int first);
extern void CleanBegin(void);

extern GAMEDATA save;
extern GAMEDATA2 no_save;
Patcher * globalPatcher;
PatcherInstance *emerald;

unsigned long SaveLocker;

extern void DebugWindow(void);
extern void __stdcall DebugWindow(PEvent e);
//extern void __stdcall UndoProhibitionTables(PEvent e);


extern void __stdcall EmeraldRedo(PEvent e);
extern void __stdcall EmeraldUndo(PEvent e);
// extern void __stdcall EmeraldMove(PEvent e);
extern void __stdcall EmeraldStrings(PEvent e);
extern void __stdcall EmeraldStrings2(PEvent e);

extern void MonGiveSpell(HERO*); // ToDo
void do_cache_Creature2Artifact();

#ifndef Disable_Emerald_Combos
extern void patch_CArtSetup(void);
extern void hook_CArtSetup(void);
extern void early_CArtSetup(void);
#endif 

unsigned int InChat, InDialog, IsTidy, SpecialDialog, click_locked;
extern int SpeedUP;

extern char art_does_not_exist[2000];

#ifndef Disable_Emerald_duplicate_cpp
void ArtGiveSpell_Proc(HERO* hero);
void __stdcall ArtGiveSpell_Proc_Monster(HiHook* h, HERO* hero);
// _LHF_(negativeArtSlots);
// _LHF_(handleCreatureAsArt);
#endif

#ifndef Disable_Emerald_AI
void art_existence();
void art_AI_init(void);
extern "C" __declspec(dllexport) void art_AI_report(void);
#endif 

#ifndef Disable_Emerald_duplicate_cpp
extern int EquipmentChanged[1024];
void __stdcall Refresh_Spells(PEvent e) {
	if (SpeedUP) return;

	int &ref = *(int*)(0x887664 + 4 * sizeof(int));

	// ref = -1;// ErmV[4] = -1;
	// Era::ExecErmCmd("HE-1:N?v4");
	if(/* ErmV[4] */ ref >= 0)
	{
		HERO* hero = (HERO*)GetHeroRecord(/* ErmV[4] */ ref);
		if (EquipmentChanged[hero->Number]) {
			ArtGiveSpell_Proc(hero);
			EquipmentChanged[hero->Number] = 0;
		}
	}
}

void __stdcall Refresh_Spells_OW(PEvent e) {
	if (SpeedUP) return;

	// int& ref = *(int*)(0x887664 + 4 * sizeof(int));
	h3::H3Player* player = P_Main->GetPlayer();
	HERO* hero = (HERO*) player->GetActiveHero();
	if (!hero) return;
	if (EquipmentChanged[hero->Number]) {
		ArtGiveSpell_Proc(hero);
		EquipmentChanged[hero->Number] = 0;
	}
	// if(hero) ArtGiveSpell_Proc((HERO*)hero);

	return;

	/*
	//ref = -1;// ErmV[4] = -1;
	//Era::ExecErmCmd("OW:A-1/?v4");
	if ( ref >= 0)
	{
		HERO* hero = (HERO*)GetHeroRecord(ref);
		ArtGiveSpell_Proc(hero);
	}
	*/
}



void __stdcall Refresh_Spells2(PEvent e) {
	auto batman = P_CombatMgr;
	h3::H3Hero* att = batman->hero[0];
	h3::H3Hero* def = batman->hero[1];
	if (att && EquipmentChanged[att->id])
		{
			ArtGiveSpell_Proc((HERO*)att);
			EquipmentChanged[att->id] = 0;
		}
	if (def && EquipmentChanged[def->id]) {
		ArtGiveSpell_Proc((HERO*)def);
		EquipmentChanged[def->id] = 0;
	}
	return; 
	/*
	int& ref = *(int*)(0x887664 + 4 * sizeof(int));

	ref = -1;// ErmV[4] = -1;
	Era::ExecErmCmd("BA:H0/?v4");
	if (ref >= 0)
	{
		HERO* hero = (HERO*)GetHeroRecord( ref);
		ArtGiveSpell_Proc(hero);
	}
	ref = -1;// ErmV[4] = -1;
	Era::ExecErmCmd("BA:H1/?v4");
	if (ref >= 0)
	{
		HERO* hero = (HERO*)GetHeroRecord(ref);
		ArtGiveSpell_Proc(hero); 
	}
	*/
}

HERO* Refresh_Spells_HeroInteraction_left = nullptr;
HERO* Refresh_Spells_HeroInteraction_right = nullptr;
void __stdcall Refresh_Spells_HeroInteraction(PEvent e) {
	if (SpeedUP) return;

	// int& ref = *(int*)(0x887664 + 4 * sizeof(int));

	int ref = Era::x[1];// ErmV[4] = -1;
	//Era::ExecErmCmd("VRv4:Sx1");
	if (/* ErmV[4] */ ref >= 0)
	{
		HERO* hero = (HERO*)GetHeroRecord(/* ErmV[4] */ ref);
		if(EquipmentChanged[hero->Number])
		{
			ArtGiveSpell_Proc(hero);
			Refresh_Spells_HeroInteraction_left = hero;
			EquipmentChanged[hero->Number] = 0;
		}
	}
	ref = Era::x[2]; // -1;// ErmV[4] = -1;
	//Era::ExecErmCmd("VRv4:Sx2");
	if (/* ErmV[4] */ ref >= 0)
	{
		HERO* hero = (HERO*)GetHeroRecord(/* ErmV[4] */ ref);
		if (EquipmentChanged[hero->Number])
		{
			ArtGiveSpell_Proc(hero);
			Refresh_Spells_HeroInteraction_right = hero;
			EquipmentChanged[hero->Number] = 0;
		}
	}
}

void __stdcall Refresh_Spells_HeroInteraction_click(PEvent e) {
	if (SpeedUP) return;

	if(Refresh_Spells_HeroInteraction_left)
		ArtGiveSpell_Proc(Refresh_Spells_HeroInteraction_left);
	if (Refresh_Spells_HeroInteraction_right)
		ArtGiveSpell_Proc(Refresh_Spells_HeroInteraction_right);
}
#endif

/*
void __stdcall CheckDialog(PEvent e) {
	return;

	static TGameState state;
	*TGetGameState(&state);

	if ((state.CurrentDlgId != state.RootDlgId) ||
		(state.CurrentDlgId != 4205280))
	{
		/// *(ErmY + 1) = temp;
		InDialog = false;
		// Tidy(e);

	}
	else {
		InDialog = true;
		if (state.RootDlgId == 4205280)
			SpecialDialog = true;
		else SpecialDialog = false;
	}
}
*/

/*
void __stdcall Chat(PEvent e) {
	if (SaveLocker) return;

	int tmp = *(ErmY + 1);
	ExecErmCmd("SN:X?y1;");
	if ( *(ErmY + 1) == 0)ExecErmCmd("SN:W^InChat^/1;");
	if ( *(ErmY + 1) == 2)ExecErmCmd("SN:W^InChat^/0;");
	ExecErmCmd("SN:W^InChat^/?y1;");
		
	InChat= *(ErmY + 1);
	 *(ErmY + 1)=tmp;
}
*/

/*
void __stdcall Refresh(PEvent e) {
	//CheckDialog(e);
	//if (InDialog) return;
	
	//emerald->WriteDword(0x7324BD, NEW_ARTS_AMOUNT); //UN:A
	//blank(NON_BLANK_ARTS_AMOUNT);
	
	int UN_A = *(int*) 0x7324BD;
	if (UN_A != NEW_ARTS_AMOUNT )
		IsTidy=false;
	//else IsTidy = true; // _
}
*/


void __stdcall Tidy(PEvent e) {
	//CheckDialog(e);
	//if (InDialog) return;
	
	//if (click_locked) return;
	//Refresh(e);
	//if (IsTidy) return;
	//blank(NON_BLANK_ARTS_AMOUNT);

	/*
	EmeraldArtNewTable = save.newtable;
	EmeraldArtNewBTable = save.newbtable;
	Emerald(e);
	*/

	//ReallocProhibitionTables(e);
	//blank(NON_BLANK_ARTS_AMOUNT);
	//IsTidy = true;

	EmeraldRedo(e);
}





void __stdcall Clean(PEvent e) {
	//CheckDialog(e);
	//if (InDialog) return;
	
	//Refresh(e);
	//if (!IsTidy) return;
	
	
	//// Is it still needed? (should be not)
	//UndoProhibitionTables(e);
	EmeraldUndo(e);
	
	
	//IsTidy = false;
}


void __stdcall Click(PEvent e) {
	auto plr = h3::H3Main::Get()->GetPlayer();
	if (! plr->is_human)
		return;

	// auto her = *(HERO**)0x027F9970;
	// if(her)MonGiveSpell(her);

	// auto plr = h3::H3Main::Get()->GetPlayer();
	auto her = (HERO*) plr->GetActiveHero();
	if (her)MonGiveSpell(her);
}


/*
void __stdcall Click(PEvent e) {
	if (SaveLocker) return;

	if (click_locked) {
		//click_locked = false;
		ExecErmCmd("CM:R0;");
		//return;
	}

	//CheckDialog(e);
	//if(InDialog) return;

	int temp = *(ErmY + 1);

	ExecErmCmd("CM:I?y1;");
	//if (*(ErmY + 1) == 3) DebugWindow();

	if (*(ErmY + 1) != 10 ) {
		*(ErmY + 1) = temp;
		if ( (InDialog || click_locked) 
		// && (!save_state)
		) {
			click_locked = false;
			return;
		}

		Tidy(e);
		return;
	}else{

	*(ErmY + 1) = temp;
	Clean(e);
	return;
	}
}
*/

/*
void __stdcall Key(PEvent e) {
	if (SaveLocker) return;

	if (InChat) return;

	CheckDialog(e);
	if (InDialog) return;

	int temp = *(ErmY + 1);
	ExecErmCmd("SN:X?y1;");

	if ( *(ErmY + 1) != 76 && *(ErmY + 1) != 108 &&
		 *(ErmY + 1) != 83 && *(ErmY + 1) != 115 &&
		 *(ErmY + 1) != 69 && *(ErmY + 1) != 101 &&
		true)
	{

		*(ErmY + 1) = temp;
		Tidy(e);  return;
	}
	else {
		*(ErmY + 1) = temp;
		Clean(e); return;
	}

}
*/

void __stdcall InitData3 (PEvent e)
{

	static bool first_time = true;
	//MessageBoxA(0, "Emerald::InitData3", "Emerald::InitData3", 0);

	//EmeraldMove(e);

	//pause();
	//EmeraldMove(e);

	EmeraldArtNewTable = no_save.newtable;//save.newtable;//no_save.newtable;
	EmeraldArtNewBTable = no_save.newbtable;//save.newbtable;//no_save.newbtable;
	Emerald(e);

	if(!first_time) ReallocProhibitionTables(e);
	LoadConfigs(e);
	EmeraldStrings(e); // modified at 2017-12-12

	/*
   // fix getartbase and getartname wog functions
	emerald->WriteDword(0x0071498E, (int)EmeraldArtBase);
	emerald->WriteDword(0x00714FA8, (int)EmeraldArtBase);
	*/

	//Refresh_Artifact(e);
	//pause();


	//MessageBoxA(0, "Emerald::InitData3 done", "Emerald::InitData3 done", 0);

	/*
	memcpy(save.newtable, no_save.newtable, sizeof(ART_RECORD)*NEW_ARTS_AMOUNT);
	memcpy(save.newbtable, no_save.newbtable, sizeof(ART_BONUS)*NEW_ARTS_AMOUNT);


	EmeraldArtNewTable = save.newtable;
	EmeraldArtNewBTable = save.newbtable;
	Emerald(e);
	*/
	first_time = false;
}

/*
void __stdcall InitData2(PEvent e)
{
	int y2 = *(ErmY + 2);
	ExecErmCmd("SN:W^Emerald Enabled^/?y2;");
	if (*(ErmY + 2) == 1) return;
	ExecErmCmd("SN:W^Emerald Enabled^/1;");
	ReallocProhibitionTables(e);
	//Emerald(e);
	LoadConfigs(e);
	//EmeraldMove(e);
	EmeraldStrings(e);
		
	*(ErmY + 2) = y2;

}
*/

/*
void __stdcall InitData1(PEvent e)
{
	ReallocProhibitionTables(e);
	LoadConfigs(e);
}
*/


void __stdcall StoreData (PEvent e)
{

	//MessageBoxA(0, "Emerald::StoreData BUMP 01", "Emerald::StoreData BUMP", 0);

	/*
	////Trying to fix
	for (int i = 0; i < NEW_ARTS_AMOUNT; ++i) {
		//memcpy(&save.newtable[i], &no_save.newtable[i], sizeof(ART_RECORD));
		//memcpy(&save.newbtable[i], &no_save.newbtable[i], sizeof(ART_BONUS));

		if (save.newtable[i].name && save.artname[i] != save.newtable[i].name)
			strncpy(save.artname[i], save.newtable[i].name, char_table_size-1);
		if (save.artname[i][0] == 0 || !(save.newtable[i].name)) {
			strncpy(save.artname[i], "{Unnamed Artifact}", char_table_size - 1);
			save.newtable[i].name = save.artname[i];
		}
			
		if (save.newtable[i].desc && save.artdesc[i] != save.newtable[i].desc)
			strncpy(save.artdesc[i], save.newtable[i].desc, char_table_size-1);
		if (save.artdesc[i][0] == 0 || !(save.newtable[i].desc)) {
			strncpy(save.artdesc[i], "{Undescribed Artifact}", char_table_size - 1);
			save.newtable[i].desc = save.artdesc[i];
		}
	}
	*/
	
	 Era::WriteSavegameSection(sizeof(save), (void*)&save, PINSTANCE_MAIN);
	//WriteSavegameSection(NEW_ARTS_AMOUNT * 2, (void*)&save, PINSTANCE_MAIN);
	 click_locked = true;
	 //SaveLocker = true;

	 //MessageBoxA(0, "Emerald::StoreData BUMP 02", "Emerald::StoreData BUMP", 0);

}


void __stdcall RestoreData (PEvent e)
{
	// do_cache_Creature2Artifact();
	
	//EmeraldArtNewTable = no_save.newtable;
	//EmeraldArtNewBTable = no_save.newbtable;
	//Emerald(e);
	

	//EmeraldUndo(e);

	//MessageBoxA(0, "Emerald::RestoreData BUMP 01", "Emerald::RestoreData BUMP", 0);

	//ExecErmCmd("SN:W^InChat^/0;");
	Era::ReadSavegameSection(sizeof(save), (void*)&save, PINSTANCE_MAIN);
	//Refresh_Artifact(e);
	//ReadSavegameSection(NEW_ARTS_AMOUNT*2, (void*)&save, PINSTANCE_MAIN);
	
	/*
	////Trying to fix
	for (int i = 0; i < NEW_ARTS_AMOUNT; ++i) {
		//memcpy(&no_save.newbtable[i], &save.newbtable[i], sizeof(ART_BONUS));
		//memcpy(&no_save.newtable[i], &save.newtable[i],sizeof(ART_RECORD));
		//no_save.newtable[i].name = save.artname[i];
		//no_save.newtable[i].desc = save.artdesc[i];
		save.newtable[i].name = save.artname[i];
		save.newtable[i].desc = save.artdesc[i];
	}
	

	EmeraldArtNewTable = save.newtable;
	EmeraldArtNewBTable = save.newbtable;
	Emerald(e);
	*/
	EmeraldStrings2(e);

	//IsTidy = 2; // two steps
	click_locked = true;

	//MessageBoxA(0, "Emerald::RestoreData BUMP 02", "Emerald::RestoreData BUMP", 0);

	art_existence();
	
	art_AI_report();
}


void __stdcall Unlock(PEvent e) {

	// do_cache_Creature2Artifact();

	// MessageBoxA(0, "Emerald::Unlock", "Emerald::Unlock", 0);

	// ReallocProhibitionTables(e);
	SaveLocker = false;
	
	//Refresh(e);
	//IsTidy = false;

	//Tidy(e); //disabled on 2019-05-20
	
	// ExecErmCmd("SN:W^InChat^/0;");

	Era::ExecErmCmd("FU99001:P;");
}

void __stdcall Lock(PEvent e) {

	if (SpeedUP) return;

	// MessageBoxA(0, "Emerald::Lock", "Emerald::Lock", 0);

	Era::ExecErmCmd("FU99002:P;");

	SaveLocker = true;
	//Clean(e); //disabled on 2019-05-20
}


void __stdcall NewGame(PEvent e)
{
	/*
	EmeraldArtNewTable = no_save.newtable;
	EmeraldArtNewBTable = no_save.newbtable;
	Emerald(e);
	LoadConfigs(e);
	*/


	//memcpy(save.newtable, no_save.newtable, sizeof(ART_RECORD)*NEW_ARTS_AMOUNT);
	//memcpy(save.newbtable, no_save.newbtable, sizeof(ART_BONUS)*NEW_ARTS_AMOUNT);


	//EmeraldArtNewTable = save.newtable;
	//EmeraldArtNewBTable = save.newbtable;
	//Emerald(e);


	//memcpy(save.newtable, no_save.newtable, NEW_ARTS_AMOUNT * sizeof(ART_RECORD));
	//memcpy(save.newbtable, no_save.newbtable, NEW_ARTS_AMOUNT * sizeof(ART_BONUS));

	//InitData3(e);

	/*
	for (int i = 0; i < NEW_ARTS_AMOUNT; i++) {
		memcpy(&save.newbtable[i], &no_save.newbtable[i], sizeof(ART_BONUS));
		memcpy(&save.newtable[i], &no_save.newtable[i], sizeof(ART_RECORD));
	}

	Tidy(e);
	*/

	//EmeraldStrings2(e);
	Unlock(e);
}

void __stdcall AfterSaveGame(PEvent e) {

	//EmeraldArtNewTable = save.newtable;
	//EmeraldArtNewBTable = save.newbtable;
	//Emerald(e);
}

/*
void __stdcall NO_SAVE_NEWTABLES(PEvent e) {
	static bool first_time = true;
	if (first_time) {
		memcpy(no_save.newtable, save.newtable, NEW_ARTS_AMOUNT * sizeof(ART_RECORD));
		memcpy(no_save.newbtable, save.newbtable, NEW_ARTS_AMOUNT * sizeof(ART_BONUS));
		//MessageBoxA(0, "NO_SAVE_NEWTABLES", "emerald", MB_OK);
		first_time = false;
	}
}
*/

void __stdcall Z_OnAfterCreateWindow(PEvent e) {

	//MessageBoxA(0, "Emerald::Emerald OnAfterCreateWindow 0", "Emerald::Emerald OnAfterCreateWindow", 0);
	///// was:
	//InitData3(e);

	ReallocProhibitionTables(e);

	////2020-04-03
	//EmeraldArtNewTable = no_save.newtable;//save.newtable;//no_save.newtable;
	//EmeraldArtNewBTable = no_save.newbtable;//save.newbtable;//no_save.newbtable;
	
	//Emerald(e);

	//MessageBoxA(0, "Emerald::Emerald OnAfterCreateWindow 1", "Emerald::Emerald OnAfterCreateWindow", 0);

	LoadConfigs(e);
	//MessageBoxA(0, "Emerald::Emerald OnAfterCreateWindow 2", "Emerald::Emerald OnAfterCreateWindow", 0);
	
	////moved to Z_OnAfterWog
	//ReallocProhibitionTables(e);

	//MessageBoxA(0, "Emerald::Emerald OnAfterCreateWindow 3", "Emerald::Emerald OnAfterCreateWindow", 0);
	//LoadConfigs(e);
	//EmeraldStrings2(e); // modified at 2017-12-12

	//MessageBoxA(0, "Emerald::Emerald OnAfterCreateWindow 4", "Emerald::Emerald OnAfterCreateWindow", 0);

	//EmeraldUndo(e);

	// art_existence();
}


void __stdcall Block_Redirection(PEvent e)
{
	//	Era::RedirectMemoryBlock( );
	Era::RedirectMemoryBlock((void*)*(int*)0x4E2D26, sizeof(ART_BONUS) * OLD_ARTS_AMOUNT, no_save.newbtable);
	Era::RedirectMemoryBlock((void*)*(int*)0x660B68, sizeof(ART_RECORD) * OLD_ARTS_AMOUNT, no_save.newtable);
}

HMODULE Grandmaster_Magic = nullptr;
void __stdcall Z_OnAfterWog(PEvent e) {
	//MessageBoxA(0, "This is alpha version of Emerald. do not use.", "Emerald::Emerald OnAfterWog", 0);

	/*
	Grandmaster_Magic = GetModuleHandleA("Grandmaster_Magic.era");
	if (Grandmaster_Magic) Grandmaster_Magic_Update_Spells = (void(*) (void*))
		GetProcAddress(Grandmaster_Magic, "UpdateSpellsFromEmerald");
	*/

	//MessageBoxA(0, "Emerald::Emerald OnAfterWog 0", "Emerald::Emerald OnAfterWog", 0);
	///// was:
	//InitData3(e);

	//EmeraldArtNewTable = no_save.newtable;//save.newtable;//no_save.newtable;
	//EmeraldArtNewBTable = no_save.newbtable;//save.newbtable;//no_save.newbtable;

	Block_Redirection(e);

	/*
	for (int i = 0; i < NEW_ARTS_AMOUNT; ++i) {
		no_save.newtable[i].partof = -1;
		no_save.newtable[i].combonum = -1;
	}
	*/

	Emerald(e);
	ReallocProhibitionTables(e);


	//Lock(e);

	EmeraldStrings2(e);

#ifndef Disable_Emerald_Combos
	patch_CArtSetup();
	hook_CArtSetup(); //2021-05-17
#endif

	HMODULE Amethyst_Link = nullptr;
	if (!Amethyst_Link) Amethyst_Link = GetModuleHandleA("amethyst2_4.dll");
	if (!Amethyst_Link) Amethyst_Link = GetModuleHandleA("amethyst2_5.dll");
	if (Amethyst_Link) {
		isReceptive = (int (*)(int))
			GetProcAddress(Amethyst_Link, "isReceptive");
		Creature2Artifact = (int (*)(int))
			GetProcAddress(Amethyst_Link, "Creature2Artifact");
		Creature2Artifact_2 = (int (*)(int))
			GetProcAddress(Amethyst_Link, "Creature2Artifact_2");
	}

	do_cache_Creature2Artifact();
	// art_existence();

	/*
	h3::H3DLL HD_WoG = h3::H3DLL::H3DLL("HD_WoG.dll");
	int PatchAddress = HD_WoG.NeedleSearch<15>({ 0x6A, 0x00, 0x6A, 0x00, 0x68, 0xAE, 0x01, 0x00, 0x00, 0x68, 0xAB, 0x01, 0x00, 0x00 }, 5);
	if (PatchAddress) {
		emerald->WriteByte(PatchAddress, 0x33);
		emerald->WriteByte(PatchAddress + 4, 0xF4);
	}
	*/
}


void __stdcall Z_OnAfterErmInstructions(PEvent e) {
	// do_cache_Creature2Artifact();

	memset(save.SoulBound_Artifacts,0, STORED_HERO_AMOUNT *STORED_ARTS_AMOUNT *2);

	//MessageBoxA(0, "Emerald::Emerald Z_OnAfterErmInstructions", "Emerald::Emerald Z_OnAfterErmInstructions", 0);

	///// was:
	//NewGame(e);

	//Unlock(e);
	// art_existence();

	art_AI_report();
}

#ifndef Disable_Emerald_Events
void __stdcall OnNewDay(PEvent e);
void __stdcall OnBattleStart(PEvent e);
void __stdcall OnBattleEnd(PEvent e);
void __stdcall OnEquip(PEvent e);
void __stdcall OnUnequip(PEvent e);
#endif

//void __fastcall ArtGiveSpell_Proc_Monster(HERO* hero);
void __stdcall ArtGiveSpell_Proc_Monster(HiHook* h, HERO* hero);

void GlobalConfig();

bool load_GM_Magic() {
	if (SpeedUP) return false;
	if (Grandmaster_Magic) return true;

	Grandmaster_Magic = GetModuleHandleA("Grandmaster_Magic.era");
	if (Grandmaster_Magic) Grandmaster_Magic_Update_Spells = (void(*) (void*))
		GetProcAddress(Grandmaster_Magic, "UpdateSpellsFromEmerald");
	// else MessageBoxA(0, "Grandmaster_Magic is not loaded", "DEBUG", MB_OK);

	return Grandmaster_Magic;
}


void __stdcall Z_OnBeforeWog(PEvent e) {
	load_GM_Magic();
}


void HeroArtDescHooks();

int fix_bp_pos;
//void __stdcall Z_NeedleBackpack(PEvent e)
_LHF_(HooksInit)
{
	h3::H3DLL HD_WoG = h3::H3DLL::H3DLL("HD_WoG.dll");
	if(fix_bp_pos){
		int PatchAddress = HD_WoG.NeedleSearch<14>({ 0x6A, 0x00, 0x6A, 0x00, 0x68, 0xAE, 0x01, 0x00, 0x00, 0x68, 0xAB, 0x01, 0x00, 0x00 }, 5);
		if (PatchAddress) {
			emerald->WriteByte(PatchAddress, 0x33);
			emerald->WriteByte(PatchAddress + 5, 0xF4);

		}
	}

	if (true) {
		int PatchAddress = HD_WoG.NeedleSearch<>({0xF3,0xA5,0x83,0x7d,0xf4,0xff,0x0f,0x85,0xDD,0x01,0x00,0x00,0x83,0x7d,0xf8,0xff }, 6);
		if (PatchAddress) emerald->WriteHexPatch(PatchAddress, "E9 DE 01 00 00 90");
	}
	if (true) {
		int PatchAddress = HD_WoG.NeedleSearch<>({ 0xF3,0xA5,0x83,0xbd,0xDC,0xFD, 0xFF, 0xFF, 0xFF, 0x0F, 0x85, 0x14, 0x02, 0x00, 0x00 }, 9);
		if (PatchAddress) emerald->WriteHexPatch(PatchAddress, "E9 15 02 00 00 90");
	}

	if (true) {
		int PatchAddress = HD_WoG.NeedleSearch<>({ 0x0f, 0x85, 0xe6, 0x00, 0x00, 0x00,
			0xC7, 0x45 , 0x8C , 0x00 , 0x00 , 0x00 , 0x00 , 0xC7 , 0x45 , 0xAC , 0x00 , 0x00 , 0x00 , 0x00, 0xEB }, 0);
		if (PatchAddress) emerald->WriteHexPatch(PatchAddress, "E9 E7 00 00 00 90");
	}
	if (true) {
		int PatchAddress = HD_WoG.NeedleSearch<>({ 0x0f, 0x85, 0x14, 0x01, 0x00, 0x00,
			0xC7, 0x85 , 0x6C , 0xfd , 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 
			0xC7, 0x85 , 0x94 , 0xfd , 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00 }, 0);
		if (PatchAddress) emerald->WriteHexPatch(PatchAddress, "E9 15 01 00 00 90");
	}

	HeroArtDescHooks();

	return EXEC_DEFAULT;
}

inline void ErrorMessage(int a1, int a2, int a3) {
	CDECL_3(void, 0x00712333, a1, a2, a3);
}

BOOL __stdcall IsArtDisabled(HiHook* h, unsigned int a1)
{
	if (a1 < 144)
		return h3::H3Main::Get()->artifactsAllowed[a1] != 0
		|| save.allowed_artifacts[a1];
	else if (a1 < STORED_ARTS_AMOUNT)
		return save.allowed_artifacts[a1];

	/*
	if (a1 < 144)
		return h3::H3Main::Get()->artifactsAllowed[a1] != 0;
	else if (a1 < STORED_ARTS_AMOUNT)
		return no_save.ArtSetUpBack[a1].disable;
	*/

	ErrorMessage(4, 3540, (int)0x007917BC);
	return 0;
}
void __stdcall DisableArt(HiHook* h, unsigned int a1, char a2)
{
	if (a1 < STORED_ARTS_AMOUNT)
		save.allowed_artifacts[a1] = a2;
	else
		ErrorMessage(4, 3555, (int)007917E4);
	/*
	if (a1 < 144)
		h3::H3Main::Get()->artifactsAllowed[a1] = a2;
	else if (a1 < STORED_ARTS_AMOUNT)
		no_save.ArtSetUpBack[a1].disable = a2;
	else
		ErrorMessage(4, 3555, (int)007917E4);
	*/
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		
		static bool FirstTime = true;
		if(FirstTime)
		{
			//DebugWindow();
			//save.footer = 0;
			IsTidy = true; InChat = false; 
			InDialog = true; SpecialDialog = false;
			click_locked = true; SaveLocker = true;

			CleanBegin();

			//strcpy(save.footer, "here is emerald save.");
			//strcpy(save.header, "here is emerald save.");
			char emerald_save_marker[64] = "here is emerald save.";
			memcpy((void *)save.footer, emerald_save_marker, 64);
			memcpy((void *)save.header, emerald_save_marker, 64);
			//strcpy(save.header, "here is emerald save.");

			//strcpy(no_save.footer, "here is emerald not saved.");
			//strcpy(no_save.header, "here is emerald not saved.");
			char emerald_no_save_marker[64] = "here is emerald not saved.";
			memcpy((void *)no_save.footer, emerald_no_save_marker, 64);
			memcpy((void *)no_save.header, emerald_no_save_marker, 64);

			unsigned char _magic[22] = { 
				0x6A,0x01,0x6A,0x09,
				0x8D,0x4D,0xE4,0xB8,
				0xA0,0x67,0x4E,0x00,
				0xFF,0xD0,0xB8,0x9D,
				0x97,0x4D,0x00,0xFF,
				0xE0,0x90 
			}; //������ �����, �������������� �� ���������� ���������� ���������.
			memcpy((void*) no_save._magic, _magic, 22);

			no_save.enchanted_artifacts_count = 9;
			int enchanted_artifacts[] =
			{ 0x01,0x80,0x7B,0x7C,
				0x56,0x57,0x58,0x59,
				0x87 };	//������ ����������, ������ ����������. 
						//��� ���������� - ���������� ���� ��� � ����� ������ 
						//� ������ ��� ���������� ����� artspelltable
			memcpy(no_save.enchanted_artifacts, enchanted_artifacts, 4*9);

			memset(no_save.autosummon,-1, 4*9* STORED_ARTS_AMOUNT);


			EmeraldArtNewTable = no_save.newtable;//save.newtable;//no_save.newtable;
			EmeraldArtNewBTable = no_save.newbtable;//save.newbtable;//no_save.newbtable;

			FirstTime = false;
		}
		
		 
		globalPatcher = GetPatcher();
		emerald =  globalPatcher->CreateInstance(PINSTANCE_MAIN);

		emerald->WriteHiHook(0x00714EA7, SPLICE_, EXTENDED_, CDECL_, IsArtDisabled);
		emerald->WriteHiHook(0x00714F23, SPLICE_, EXTENDED_, CDECL_, DisableArt);
		for (auto& c : save.allowed_artifacts) c = 0;

		/*
		Grandmaster_Magic = GetModuleHandleA("Grandmaster_Magic.era");
		if (Grandmaster_Magic) Grandmaster_Magic_Update_Spells = (void(*) (void*))
			GetProcAddress(Grandmaster_Magic, "UpdateSpellsFromEmerald");
		else MessageBoxA(0,"Grandmaster_Magic is not loaded","DEBUG",MB_OK);
		*/

		GlobalConfig();

#ifndef Disable_Emerald_Combos
		early_CArtSetup();

		patch_CArtSetup();
		// hook_CArtSetup(); //2021-05-17
#endif

		// LoadConfigs(nullptr);

#ifndef Disable_Emerald_AI
		art_AI_init();
		// art_existence();
#endif 

		Era::ConnectEra();



		//Storing data
		//RegisterHandler(InitData2, "OnErmTimer 1");
		//RegisterHandler(InitData1, "OnBeforeErmInstructions");
		
		Era::RegisterHandler(StoreData, "OnSavegameWrite");
		Era::RegisterHandler(RestoreData, "OnSavegameRead");
		//RegisterHandler(InitData1, "OnAfterCreateWindow");

		//RegisterHandler(AfterSaveGame, "OnAfterSaveGame");

		//EmeraldArtNewTable = no_save.newtable;
		//EmeraldArtNewBTable = no_save.newbtable;


		////InitData3 fires too late

		////2020-04-02
		//RegisterHandler(InitData3, "OnAfterCreateWindow"); //original
		Era::RegisterHandler(Z_OnAfterCreateWindow, "OnAfterCreateWindow"); //original
		
		//RegisterHandler(InitData3, "OnBeforeWog"); 
		

		////2020-04-02
		//RegisterHandler(InitData3, "OnAfterWoG"); //changed at 2017-12-10
		Era::RegisterHandler(Z_OnAfterWog, "OnAfterWoG");

		Era::RegisterHandler(Z_OnBeforeWog, "OnBeforeWog");
												  
		//RegisterHandler(OnAfterWoG, "OnAfterWoG"); //original
		//RegisterHandler(InitData3, "OnAfterWoG"); //changed at 2017-12-10

		Era::RegisterHandler(Click, "OnAdventureMapLeftMouseClick");
		Era::RegisterHandler(Click, "OnHeroesMeetScreenMouseClick");
		Era::RegisterHandler(Click, "OnHeroScreenMouseClick");
		Era::RegisterHandler(Click, "OnTownMouseClick");
		//RegisterHandler(Key, "OnKeyPressed");
		//RegisterHandler(Chat, "OnChat");
		
		////Locking: why it can't be  disabled ?
		//// 2020-04-02
		Era::RegisterHandler(Lock, "OnBeforeSaveGame");
		Era::RegisterHandler(Unlock, "OnAfterSaveGame");
		Era::RegisterHandler(Unlock, "OnAfterLoadGame");
		Era::RegisterHandler(Unlock, "OnAfterErmInstructions");

		//RegisterHandler(Unlock, "OnAfterErmInstructions"); // disabled 2019-05-21
		
		////2020-04-02
		//RegisterHandler(NewGame, "OnAfterErmInstructions"); 
		Era::RegisterHandler(Z_OnAfterErmInstructions, "OnAfterErmInstructions");

		emerald->WriteLoHook(0x4EEAF2, HooksInit);
		//Era::RegisterHandler(Z_NeedleBackpack, "OnAfterErmInstructions");
		//Era::RegisterHandler(Z_NeedleBackpack, "OnAfterLoadGame");

#ifndef Disable_Emerald_Events
		Era::RegisterHandler(OnNewDay, "OnEveryDay");
		
		Era::RegisterHandler(OnBattleStart, "OnBeforeBattleUniversal");
		Era::RegisterHandler(OnBattleEnd, "OnAfterBattleUniversal");

		Era::RegisterHandler(OnEquip, "OnEquipArt");
		Era::RegisterHandler(OnUnequip, "OnUnequipArt");
#endif 

		//RegisterHandler(NO_SAVE_NEWTABLES, "OnAfterCreateWindow");

#ifndef Disable_Emerald_duplicate_cpp
		emerald->WriteHiHook(0x004D9840, SPLICE_, EXTENDED_, THISCALL_, ArtGiveSpell_Proc_Monster);

		Era::RegisterHandler(Refresh_Spells_OW, "OnBeforeAdventureMagic");


		Era::RegisterHandler(Refresh_Spells2, "OnBeforeBattleUniversal");
		Era::RegisterHandler(Refresh_Spells2, "OnAfterBattleUniversal");

		/* // removed on 2022-04-21 - to be reenabled and fixed later
		Era::RegisterHandler(Refresh_Spells2, "OnBeforeBattleUniversal");
		Era::RegisterHandler(Refresh_Spells2, "OnAfterBattleUniversal");

		Era::RegisterHandler(Refresh_Spells_OW, "OnBeforeAdventureMagic");
		Era::RegisterHandler(Refresh_Spells, "OnHeroScreenMouseClick");


		Era::RegisterHandler(Refresh_Spells_HeroInteraction, "OnBeforeHeroInteraction");
		Era::RegisterHandler(Refresh_Spells_HeroInteraction, "OnAfterHeroInteraction");

		Era::RegisterHandler(Refresh_Spells_HeroInteraction_click, "OnHeroesMeetScreenMouseClick");
		*/
#endif

		// emerald->WriteLoHook(0x004D9A3F, negativeArtSlots);
		// emerald->WriteLoHook(0x004D98A7, handleCreatureAsArt);



		// Era::RegisterHandler(Block_Redirection, "OnAfterWoG");


		install_SSkill_bonus();
	}
	return TRUE;
}

