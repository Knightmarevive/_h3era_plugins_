#define Z_ERA
#include "..\__include\era.h"



namespace Era {

    Z_ERA int* v = (int*)0x887664; // 1..10000
    Z_ERA TErmZVar* z = (TErmZVar*)0x9271E8; // 1..1000
    Z_ERA int* y = (int*)0xA48D7C; // 1..100
    Z_ERA int* x = (int*)0x91DA34; // 1..16
    Z_ERA bool* f = (bool*)0x91F2DF; // 1..1000
    Z_ERA float* e = (float*)0xA48F14; // 1..100


    Z_ERA TEventParams* EventParams = NULL;


    Z_ERA TApiHook              ApiHook = NULL;
    Z_ERA TClearAllIniCache     ClearAllIniCache = NULL;
    Z_ERA TClearIniCache        ClearIniCache = NULL;
    Z_ERA TExecErmCmd           ExecErmCmd = NULL;
    Z_ERA TExtractErm           ExtractErm = NULL;
    Z_ERA TFatalError           FatalError = NULL;
    Z_ERA TFireErmEvent         FireErmEvent = NULL;
    Z_ERA TFireEvent            FireEvent = NULL;
    Z_ERA TGetButtonID          GetButtonID = NULL;
    Z_ERA TGetEraVersion        GetEraVersion = NULL;
    Z_ERA TGetGameState         GetGameState = NULL;
    Z_ERA TGetRealAddr          GetRealAddr = NULL;
    Z_ERA TGlobalRedirectFile   GlobalRedirectFile = NULL;
    Z_ERA THook                 Hook = NULL;
    Z_ERA THookCode             HookCode = NULL;
    Z_ERA TLoadImageAsPcx16     LoadImageAsPcx16 = NULL;
    Z_ERA TLoadTxt              LoadTxt = NULL;
    Z_ERA TMemFree              MemFree = NULL;
    Z_ERA TNameColor            NameColor = NULL;
    Z_ERA TNotifyError          NotifyError = NULL;
    Z_ERA TPatchExists          PatchExists = NULL;
    Z_ERA TPluginExists         PluginExists = NULL;
    Z_ERA TReadSavegameSection  ReadSavegameSection = NULL;
    Z_ERA TReadStrFromIni       ReadStrFromIni = NULL;
    Z_ERA TRecallAPI            RecallAPI = NULL;
    Z_ERA TRedirectFile         RedirectFile = NULL;
    Z_ERA TRedirectMemoryBlock  RedirectMemoryBlock = NULL;
    Z_ERA TRegisterHandler      RegisterHandler = NULL;
    Z_ERA TReloadErm            ReloadErm = NULL;
    Z_ERA TReportPluginVersion  ReportPluginVersion = NULL;
    Z_ERA TRestoreEventParams   RestoreEventParams = NULL;
    Z_ERA TSaveEventParams      SaveEventParams = NULL;
    Z_ERA TSaveIni              SaveIni = NULL;
    Z_ERA TSplice               Splice = NULL;
    //TToStaticStr          ToStaticStr = NULL;
    Z_ERA TTr                   _tr = NULL;
    Z_ERA TWriteAtCode          WriteAtCode = NULL;
    Z_ERA TWriteSavegameSection WriteSavegameSection = NULL;
    Z_ERA TWriteStrToIni        WriteStrToIni = NULL;


    HINSTANCE hEra;
    HINSTANCE hAngel;

    /**
     * Returns translation for given complex key ('xxx.yyy.zzz') with substituted parameters.
     * Pass vector of (parameter name, parameter value) pairs to substitute named parameters.
     * Example: Mod\Lang\*.json file: { "eqs": { "greeting": "Hello, @name@" } }
     * Example: ShowMessage(tr("eqs.greeting", { "name", "igrik" }).c_str());
     *
     * @param  key    Key to get translation for.
     * @param  params Vector of (parameter name, parameter value pairs).
     * @return        Translation string.
     */
    std::string tr(const char* key, const std::vector<std::string> params = {}) {
        const int MAX_PARAMS = 64;
        const char* _params[MAX_PARAMS];
        int numParams = params.size() <= MAX_PARAMS ? params.size() : MAX_PARAMS;

        for (int i = 0; i < numParams; i++) {
            _params[i] = params[i].c_str();
        }

        char* buf = _tr(key, _params, numParams - 1);
        std::string result = buf;
        MemFree(buf);

        return result;
    }

    std::string IntToStr(int value) {
        char buf[64];
        sprintf(buf, "%d", value);

        return buf;
    }


    /**
     * Assigns new string value to buffer.
     * @param  Buf      Buffer to change contents of.
     * @param  NewValue New string value.
     * @param  BufSize  Maximal buffer size.
     * @return void
     */
    void SetPcharValue(char* Buf, const char* NewValue, int BufSize = -1) {
        if (BufSize < 0) {
            lstrcpyA(Buf, NewValue);
        }
        else if (BufSize > 0) {
            int NumBytesToCopy = lstrlenA(NewValue);
            NumBytesToCopy >= BufSize && (NumBytesToCopy = BufSize - 1);
            memcpy(Buf, NewValue, NumBytesToCopy);
            Buf[NumBytesToCopy] = 0;
        }
    }

    void ConnectEra()
    {
        hAngel = LoadLibraryA("angel.dll");
        EventParams = (TEventParams*)GetProcAddress(hAngel, "EventParams");
        RestoreEventParams = (TRestoreEventParams)GetProcAddress(hAngel, "RestoreEventParams");
        SaveEventParams = (TSaveEventParams)GetProcAddress(hAngel, "SaveEventParams");
        /***/
        hEra = LoadLibraryA("era.dll");
        _tr = (TTr)GetProcAddress(hEra, "tr");
        ApiHook = (TApiHook)GetProcAddress(hEra, "ApiHook");
        ClearAllIniCache = (TClearAllIniCache)GetProcAddress(hEra, "ClearAllIniCache");
        ClearIniCache = (TClearIniCache)GetProcAddress(hEra, "ClearIniCache");
        ExecErmCmd = (TExecErmCmd)GetProcAddress(hEra, "ExecErmCmd");
        ExtractErm = (TExtractErm)GetProcAddress(hEra, "ExtractErm");
        FatalError = (TFatalError)GetProcAddress(hEra, "FatalError");
        FireErmEvent = (TFireErmEvent)GetProcAddress(hEra, "FireErmEvent");
        FireEvent = (TFireEvent)GetProcAddress(hEra, "FireEvent");
        GetButtonID = (TGetButtonID)GetProcAddress(hEra, "GetButtonID");
        GetEraVersion = (TGetEraVersion)GetProcAddress(hEra, "GetVersion");
        GetGameState = (TGetGameState)GetProcAddress(hEra, "GetGameState");
        GetRealAddr = (TGetRealAddr)GetProcAddress(hEra, "GetRealAddr");
        GlobalRedirectFile = (TGlobalRedirectFile)GetProcAddress(hEra, "GlobalRedirectFile");
        Hook = (THook)GetProcAddress(hEra, "Hook");
        HookCode = (THookCode)GetProcAddress(hEra, "HookCode");
        LoadImageAsPcx16 = (TLoadImageAsPcx16)GetProcAddress(hEra, "LoadImageAsPcx16");
        LoadTxt = (TLoadTxt)GetProcAddress(hEra, "LoadTxt");
        MemFree = (TMemFree)GetProcAddress(hEra, "MemFree");
        NameColor = (TNameColor)GetProcAddress(hEra, "NameColor");
        NotifyError = (TNotifyError)GetProcAddress(hEra, "NotifyError");
        PatchExists = (TPatchExists)GetProcAddress(hEra, "PatchExists");
        PluginExists = (TPluginExists)GetProcAddress(hEra, "PluginExists");
        ReadSavegameSection = (TReadSavegameSection)GetProcAddress(hEra, "ReadSavegameSection");
        ReadStrFromIni = (TReadStrFromIni)GetProcAddress(hEra, "ReadStrFromIni");
        RecallAPI = (TRecallAPI)GetProcAddress(hEra, "RecallAPI");
        RedirectFile = (TRedirectFile)GetProcAddress(hEra, "RedirectFile");
        RedirectMemoryBlock = (TRedirectMemoryBlock)GetProcAddress(hEra, "RedirectMemoryBlock");
        RegisterHandler = (TRegisterHandler)GetProcAddress(hEra, "RegisterHandler");
        ReloadErm = (TReloadErm)GetProcAddress(hEra, "ReloadErm");
        ReportPluginVersion = (TReportPluginVersion)GetProcAddress(hEra, "ReportPluginVersion");
        SaveIni = (TSaveIni)GetProcAddress(hEra, "SaveIni");
        Splice = (TSplice)GetProcAddress(hEra, "Splice");
        //ToStaticStr = (TToStaticStr)GetProcAddress(hEra, "ToStaticStr");
        WriteAtCode = (TWriteAtCode)GetProcAddress(hEra, "WriteAtCode");
        WriteSavegameSection = (TWriteSavegameSection)GetProcAddress(hEra, "WriteSavegameSection");
        WriteStrToIni = (TWriteStrToIni)GetProcAddress(hEra, "WriteStrToIni");
    }
}