#pragma once
//������, ����� ��� ����� ������.

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
//#include "Tchar.h"
#include <io.h>
#include "..\__include\era.h"

#include "..\__include\heroes.h"
#include "..\__include\patcher_x86_commented.hpp"


inline bool FileExists(const char* chPath) {
	return(_access(chPath, 0) == 0);
}

#define OLD_ARTS_AMOUNT	171
#define NON_BLANK_ARTS_AMOUNT	161
#define NEW_ARTS_AMOUNT 1000
#define char_table_size 512
#define int_tuple_size 256

#define SPELLS_AMOUNT	128 //����� ������ ������?

#define PINSTANCE_MAIN "emerald3"

//Savegame
typedef struct
{
	char header[64];
	char    used_artifacts[NEW_ARTS_AMOUNT];// 0x4E224
	char allowed_artifacts[NEW_ARTS_AMOUNT];// 0x4E2B4
	char marker_1[8];
	//char unknown_art1     [NEW_ARTS_AMOUNT];// 0x4E344

	char artname[NEW_ARTS_AMOUNT][char_table_size];
	char artdesc[NEW_ARTS_AMOUNT][char_table_size];
	int ERM_Z_name[(NEW_ARTS_AMOUNT+2)*2];

	char footer[64]; // = "here is emerald.";
	
}GAMEDATA;

typedef struct
{
	char header[64];
// #include "Properties.h"

	 /* extern */  ART_RECORD newtable[NEW_ARTS_AMOUNT];
	 /* extern */  ART_BONUS newbtable[NEW_ARTS_AMOUNT];

	// char artname[NEW_ARTS_AMOUNT][char_table_size];
	// char artdesc[NEW_ARTS_AMOUNT][char_table_size];

	//extern char* arteventtable[NEW_ARTS_AMOUNT+1]; 
	 /* extern */  char* arteventtable[NEW_ARTS_AMOUNT + 1]/*[char_table_size]*/;
	 /* extern */ // char  arteventtable_txt[NEW_ARTS_AMOUNT + 1][char_table_size];

	 /* extern */  char  artspelltable[NEW_ARTS_AMOUNT];
	//New tables


	 /* extern */  int luck_bonuses[NEW_ARTS_AMOUNT];
	 /* extern */  int morale_bonuses[NEW_ARTS_AMOUNT];
	 /* extern */  int luck_bonuses_bp[NEW_ARTS_AMOUNT];
	 /* extern */  int morale_bonuses_bp[NEW_ARTS_AMOUNT];


	 /* extern */  int allow_fly[NEW_ARTS_AMOUNT];
	 /* extern */  int allow_water[NEW_ARTS_AMOUNT];

	 /* extern */  int spell_immunity[NEW_ARTS_AMOUNT][SPELLS_AMOUNT];

	 /* extern */  int autocast[NEW_ARTS_AMOUNT];



	 /* extern */  int crattack_bonuses[NEW_ARTS_AMOUNT];
	 /* extern */  int crdefence_bonuses[NEW_ARTS_AMOUNT];
	 /* extern */  int dmgmin_bonuses[NEW_ARTS_AMOUNT];
	 /* extern */  int dmgmax_bonuses[NEW_ARTS_AMOUNT];
	 /* extern */  int speed_bonuses[NEW_ARTS_AMOUNT];
	 /* extern */  int hp_bonuses[NEW_ARTS_AMOUNT];
	 /* extern */  int shots_bonuses[NEW_ARTS_AMOUNT];
	 /* extern */  int casts_bonuses[NEW_ARTS_AMOUNT];

	//bonuses


	//events
	 /* extern */  char erm_on_ae0[NEW_ARTS_AMOUNT][char_table_size];
	 /* extern */  char erm_on_ae1[NEW_ARTS_AMOUNT][char_table_size];

	 /* extern */  char erm_on_ba52[NEW_ARTS_AMOUNT][char_table_size];
	 /* extern */  char erm_on_ba53[NEW_ARTS_AMOUNT][char_table_size];
	 /* extern */  char erm_on_timer[NEW_ARTS_AMOUNT][char_table_size];

	 /* extern */  char erm_on_creature_param_init[NEW_ARTS_AMOUNT][char_table_size];
	 /* extern */  char erm_on_battlestart[NEW_ARTS_AMOUNT][char_table_size];

	 /*
	 int enchanted_artifacts[NEW_ARTS_AMOUNT];
	 int enchanted_artifacts_count;
	 */


	 int artspellswitch[256];      //����� ����� ��� ���� �������
	 unsigned char _magic[22] = { 0x6A,0x01,0x6A,0x09,
		 0x8D,0x4D,0xE4,0xB8,
		 0xA0,0x67,0x4E,0x00,
		 0xFF,0xD0,0xB8,0x9D,
		 0x97,0x4D,0x00,0xFF,
		 0xE0,0x90 }; //������ �����, �������������� �� ���������� ���������� ���������.
	 char new_cases[22 * 128];  //����� ��� ����� ������, ������ ��������� ����������


	 int enchanted_artifacts_count = 9;
	 int enchanted_artifacts[NEW_ARTS_AMOUNT] =
	 { 0x01,0x80,0x7B,0x7C,
		 0x56,0x57,0x58,0x59,
		 0x87 };	//������ ����������, ������ ����������. 
					//��� ���������� - ���������� ���� ��� � ����� ������ 
					//� ������ ��� ���������� ����� artspelltable

	char footer[64];
}GAMEDATA2;

/*
inline void init_GAMEDATA_footer(void) {
	strcpy(save.footer, "here is emerald.");
}
*/

extern GAMEDATA save;
extern GAMEDATA2 no_save;
static ART_RECORD* EmeraldArtBase = no_save.newtable;

extern Patcher * globalPatcher;
extern PatcherInstance * emerald;



extern "C" __declspec(dllexport) void CastArtifactSpell(int artifact, int spell, int duration);
extern "C" __declspec(dllexport)void ExecErmSequence(char* command);

inline void pause(void) {
	MessageBoxA(0, "Program Interuption" , "PAUSE" , 0);
}