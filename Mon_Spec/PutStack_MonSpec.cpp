// dllmain.cpp : Defines the entry point for the DLL application.
// #include "pch.h"
#pragma warning(disable : 4996)

// #include "../__include__/patcher_x86_commented.hpp"
// #include "patcher_x86_commented.hpp"

#define _H3API_PLUGINS_
#define _H3API_PATCHER_X86_
#include "../__include__/H3API/single_header/H3API.hpp"

#include "framework.h"
#include "era.h"

#define PINSTANCE_MAIN "Mon_Spec"

extern Patcher* globalPatcher;
extern PatcherInstance* Z_MonSpec;

/* extern */ BYTE* BPDummy;

BYTE* GetMPos(h3::H3CombatManager* Bm, int Side, int Pos)
{
	//#include "templ.h"
	__asm {
		//pusha //modified by majaczek

		mov    ebx, Side
		lea    eax, [8 * ebx]
		sub    eax, ebx
		mov    ebx, Pos
		lea    ecx, [ebx + 2 * eax]
		add    eax, ecx
		lea    ecx, [8 * eax]
		sub    ecx, eax
		lea    edx, [ecx + 2 * ecx]
		lea    eax, [eax + 8 * edx]
		mov    ebx, Bm
		lea    eax, [ebx + 8 * eax + 0x54CC]
		mov    BPDummy, eax

		//popa //modified by majaczek
	}
	//RETURN(BPDummy)
	return(BPDummy);
}
//-----------------------------------------------------------------------------------------------
static int PS_Type;
static int PS_Num;
static int PS_Pos;
static int PS_Side;
static h3::H3Hero* PS_hp;
static int PS_Placed;
static int PS_Stack;
static BYTE* Mstr;
BYTE* PutStack(h3::H3CombatManager* Bm, int Type, int Num, int Pos, int Side, h3::H3Hero* hp, int Placed, int Stack)
{
	//#include "templ.h"
	PS_Type = Type; PS_Num = Num; PS_Pos = Pos; PS_Side = Side; PS_hp = hp; PS_Placed = Placed; PS_Stack = Stack;
	Mstr = GetMPos(Bm, Side, Placed);
	__asm {
		//pusha //modified by majaczek

		push   PS_Stack;
		push   PS_Pos;
		push   PS_Placed;
		push   PS_Side;
		push   PS_hp;
		push   PS_Num;
		push   PS_Type;
		mov    ecx, Mstr
			mov    eax, 0x43D5D0
			call   eax
			mov    ecx, Mstr
			mov    eax, 0x43D710
			call   eax

			//popa //modified by majaczek
	}
	//RETURN(Mstr)
	return(Mstr);
}