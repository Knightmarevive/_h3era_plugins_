// dllmain.cpp : Defines the entry point for the DLL application.
// #include "pch.h"
#pragma warning(disable : 4996)

// #include "../__include__/patcher_x86_commented.hpp"
// #include "patcher_x86_commented.hpp"

#include "../Heroine/hero_limits.h"

#define _H3API_PLUGINS_
#define _H3API_PATCHER_X86_
#include "../__include__/H3API/single_header/H3API.hpp"

#include "framework.h"
#include "era.h"

#define PINSTANCE_MAIN "Mon_Spec"

Patcher* globalPatcher;
PatcherInstance* Z_MonSpec;

auto f_ConfigureHero = (void(*)(int, char*)) nullptr;
auto f_GetHeroesSpecialtyTable = (h3::H3HeroSpecialty * (*)()) nullptr;

auto f_mon_upgraded_in_town = (long(*)(long))nullptr;
auto f_set_custom_creature_upgrade = (void(*)(long))nullptr;

extern BYTE* PutStack(h3::H3CombatManager* Bm, int Type, int Num, int Pos, int Side, h3::H3Hero* hp, int Placed, int Stack);

auto f_Get_Spec44ptr = (h3::PH3LoadedPcx16(*)(int)) nullptr;
auto mon_bon_def = (h3::PH3LoadedDef) nullptr;
auto un44_def = (h3::PH3LoadedDef) nullptr;
auto monprt44_def = (h3::PH3LoadedDef) nullptr;


void Update_MonSpec(int id, h3::H3HeroSpecialty& spec, char* name = "Hero") {
    if (spec.type != 1 && spec.type != 4
        && spec.type != 6) return;
    long mon_id = spec.type == 6 ?
        spec.upgradeTo : spec.bonusId;
    long prev_mon_id = mon_id;

    auto cre_info = h3::H3CreatureInformation::Get();
    char* MonName = (char*)cre_info[mon_id].namePlural;

    strcpy((char*)spec.spFull, MonName);
    strcpy((char*)spec.spShort, MonName);

    sprintf((char*)spec.spDescr, "%s is Specialist of %s and allows to upgrade:\n%s",
        name, MonName, MonName);
    for (int i = 1; i < 4; ++i) {
        prev_mon_id = mon_id;
        mon_id = f_mon_upgraded_in_town(mon_id);
        if (prev_mon_id == mon_id || mon_id < 0) break;

        MonName = (char*)cre_info[mon_id].namePlural;
        strcat((char*)spec.spDescr, " -> ");
        strcat((char*)spec.spDescr, MonName);
    }

    auto spec44 = f_Get_Spec44ptr(id);
    auto frame = spec.type == 6 ?
        spec.upgradeTo : spec.bonusId;

    if (spec.bonusId >= monprt44_def->groups[0]->count - 2)
        mon_bon_def->DrawToPcx16(0, frame + 2, spec44, -7, -10);
    else monprt44_def->DrawToPcx16(0, frame + 2, spec44, 0, 0);
    
}

_LHF_(hook_0058692A) {
    static bool done = false;
    if(done) return EXEC_DEFAULT;

    un44_def = h3::H3LoadedDef::Load("un44.def");
    mon_bon_def = h3::H3LoadedDef::Load("TwCRPORT.def");
    monprt44_def = h3::H3LoadedDef::Load("monprt44.def");
    HMODULE h_Heroine = GetModuleHandleA("Heroine.era");
    HMODULE h_new_towns = GetModuleHandleA("new_towns.era");
    if (h_Heroine && h_new_towns) {
        f_ConfigureHero = (void(*)(int, char*))
            GetProcAddress(h_Heroine, "ConfigureHero");
        f_GetHeroesSpecialtyTable = (h3::H3HeroSpecialty*(*)())
            GetProcAddress(h_Heroine, "GetHeroesSpecialtyTable");
        f_mon_upgraded_in_town = (long(*)(long))
            GetProcAddress(h_new_towns, "mon_upgraded_in_town");
        f_set_custom_creature_upgrade = (void(*)(long))
            GetProcAddress(h_new_towns,"set_custom_creature_upgrade");
            
        f_Get_Spec44ptr = (h3::PH3LoadedPcx16(*)(int))
            GetProcAddress(h_Heroine, "Get_Spec44ptr");

        auto SpecTable = f_GetHeroesSpecialtyTable();
        for (int i = 0; i < MaxHeroCount; ++i) {
            if (SpecTable[i].type == 1 /* Monster */
                || SpecTable[i].type == 4
                || SpecTable[i].type == 6
                ) {
                Update_MonSpec(i, SpecTable[i]);
            }
        }
    }

    done = true;
    return EXEC_DEFAULT;
}
/*
void __stdcall OnHeroScreenMouseClick(Era::TEvent* e) {
    int* args = Era::x;

    //ExecErmCmd("CM:I?y11");
    
    return;
}
*/

_LHF_(hook_004DDBD5) {
    int slot_id = c->edi;
    auto myhero = *(h3::H3Hero**)0x00698B70;
    auto hero_id = myhero->id;
    auto slot_type = myhero->army.type[slot_id];
    auto my_spec = f_GetHeroesSpecialtyTable() + hero_id;
    if(my_spec->type!=1 && my_spec->type != 4
        && my_spec->type != 6
        ) return EXEC_DEFAULT;

    // long mon_id = my_spec->bonusId;

    long mon_id = my_spec->type == 6 ?
        my_spec->upgradeTo : my_spec->bonusId;

    long prev_mon_id = mon_id;
    for (int i = 0; i < 3; ++i) {
        prev_mon_id = mon_id;
        mon_id = f_mon_upgraded_in_town(mon_id);
        if (prev_mon_id == mon_id || mon_id < 0) break;

        if (prev_mon_id == slot_type) {
            f_set_custom_creature_upgrade(mon_id);
            return EXEC_DEFAULT;

        }
            
    }

    f_set_custom_creature_upgrade(-1);
    return EXEC_DEFAULT;
}

_LHF_(hook_0076C01A) {
    auto batman = *(h3::H3CombatManager**)(c->ebp + 0x08);
    auto &side = *(int*)(c->ebp + 0x0c);
    auto &Placed = *(int*)(c->ebp + 0x10);
    auto &her = batman->hero[side];
    if (!her) return EXEC_DEFAULT;

    auto my_spec = f_GetHeroesSpecialtyTable() + her->id;
    if (my_spec->type != 1 && my_spec->type != 4
        && my_spec->type != 6)
        return EXEC_DEFAULT;

    long mon_id = my_spec->type == 6 ?
        my_spec->upgradeTo : my_spec->bonusId;
    // long mon_id = my_spec->bonusId;
    long prev_mon_id = mon_id;
    for (int i = 0; i < 4; ++i) {
        prev_mon_id = mon_id;
        mon_id = f_mon_upgraded_in_town(mon_id);
        if (prev_mon_id == mon_id || mon_id < 0) break;
    }

    int growth = h3::H3CreatureInformation::Get()[prev_mon_id].grow;
    // int mon_count = sqrt(growth * sqrt(growth) * sqrt(her->level / 3));
    int mon_count = pow(growth, 1.3) * pow(her->level, 0.6) * 0.5;

    if (mon_count < 1) mon_count = 1;
    PutStack(batman, prev_mon_id, mon_count, side ? 97 : 89, side, her, Placed ,-1);
    ++ Placed; return EXEC_DEFAULT;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH: {
        globalPatcher = GetPatcher();
        Z_MonSpec = globalPatcher->CreateInstance(PINSTANCE_MAIN);
        Z_MonSpec->WriteLoHook(0x0058692A, hook_0058692A);

        Z_MonSpec->WriteLoHook(0x004DDBD5, hook_004DDBD5);
        // Era::RegisterHandler(OnHeroScreenMouseClick, "OnHeroScreenMouseClick");
    
        Z_MonSpec->WriteLoHook(0x0076C01A, hook_0076C01A);
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

