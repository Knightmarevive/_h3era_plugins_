#include <cstring>
#include <cstdio>
#include <cstdlib>
#include "Tchar.h"

#include <io.h>

#include "heroes.h"

char forced;

char Creature_ACAST_CUSTOM_Chance;
DWORD Creature_ACAST_CUSTOM_Spell;

char OverrideSpecialSpells;
char StackStep_field_type;

int strike_all_around_range;
char counterstrike_fury;
char counterstrike_twice;

char Wog_Spell_Immunites[16];

char isDragonResistant;
char isHeroic;

char NoGolemOverflow;

char isHellHydra;
char isLord_Table;

char Receptive_Table;
long poison_table;
long aging_table;
long paralyze_table;
long paralyze_chance;

char do_not_generate;

char isTeleporter;

DWORD CreatureSpellPowerMultiplier ;
DWORD CreatureSpellPowerDivider ;
DWORD CreatureSpellPowerAdder ;

DWORD CreatureMimicArtifact;
DWORD CreatureMimicArtifact2;

DWORD Necromancy_without_artifacts[16];
DWORD Necromancy_with_artifacts[16];

float shooting_resistance;
float melee_resistance;

int after_melee__spell;
int after_shoot__spell;
int after_defend_spell;
int after_wound__spell;
char after_action_spell_mastery;
int after_melee__spell2;
int after_shoot__spell2;
int after_defend_spell2;
int after_wound__spell2;

//// MOP Area begin 
char RangeRetaliation_Table;
char PreventiveCounterstrikeTable;

DWORD RegenerationHitPoints_Table ;
char  RegenerationChance_Table ;
WORD  ManaDrain_Table ;
char  SpellsCostDump_Table ;
char  SpellsCostLess_Table ;
char  Counterstrike_Table ;
char  AlwaysPositiveMorale_Table ;
char  AlwaysPositiveLuck_Table;
char  FireWall_Table;
char  ThreeHeadedAttack_Table;


char  DeathBlow_Table ;
char  Fear_Table ;
char  Fearless_Table ;
char  NoWallPenalty_Table ;
char  MagicAura_Table ;
char  StrikeAndReturn_Table ;
DWORD  Spells_Table ;
char  Hate_Table ;
char  JoustingBonus_Table ;
char  ImmunToJoustingBonus_Table ;
char  MagicChannel_Table ;
char  MagicMirror_Table ;
char  Sharpshooters_Table ;
char  ShootingAdjacent_Table ;
char  ReduceTargetDefense_Table ;
Word  Demonology_Table ;

char  ImposedSpells_Table[6];
//// Mop Area end

DWORD PersonalHate[1024]; 
char isDragonSlayer_Table;
char isFaerieDragon_Table;
char isAmmoCart_Table;
char isPassive_Table;
char isAimedCaster_Table;

char hasSantaGuards ;
//DWORD SantaGuardsType ;
//DWORD UpgradedSantaGuardsType ;
DWORD DalionsGuards;

float ghost_fraction;
char isGhost ;
char isRogue ;

char isEnchanter ;
char isSorceress ;

char isHellSteed ;
char isHellSteed2 ;
char isHellSteed3 ;

char MovesTwice_Table;

typedef enum
{
	ACAST_BIND,
	ACAST_BLIND,
	ACAST_DISEASE,
	ACAST_CURSE,
	ACAST_AGE,
	ACAST_STONE,
	ACAST_PARALIZE,
	ACAST_POIZON,
	ACAST_ACID,
	ACAST_DEFAULT
}
AFTERCAST_ABILITY;

char aftercast_abilities_table ;

//=============================================
typedef enum
{
	ATT_VAMPIRE,
	ATT_THUNDER,
	ATT_DEATHSTARE,
	ATT_DISPEL,
	ATT_DISRUPT,
	ATT_DEFAULT
}
ATTACK_ABILITY;

char attack_abilities_table ;
//=============================================
typedef enum
{
	RESIST_DWARF20,
	RESIST_DWARF40,
	RESIST_123LVL,
	RESIST_1234LVL,
	RESIST_MAGICIMMUNE,
	RESIST_ASAIR,
	RESIST_ASEARTH,
	RESIST_ASFIRE,
	RESIST_DEFAULT
}
MAGIC_RESISTANCE;

char magic_resistance_table ;
//=============================================

typedef enum
{
	VULN_HALF,
	VULN_QUATER,
	VULN_LIGHTING,
	VULN_SHOWER,
	VULN_ICE,
	VULN_FIRE,
	VULN_GOLD,
	VULN_DIAMOND,
	VULN_DEFAULT
}
MAGIC_VULNERABILITY;

char magic_vulnerability_table ;
//=============================================
char missiles_table ;
//=============================================

char spell_1_table ; //int  spell_1_table_ptr = (int)spell_1_table;
char spell_2_table ; //int  spell_2_table_ptr = (int)spell_2_table;
char spell_3_table ; //int  spell_3_table_ptr = (int)spell_3_table;
//=============================================

int skeltrans ;
//=============================================

int upgtable ;

char special_missiles_table ;
char missile_size_table;
long missile_anim_table;

float fire_shield_table ;
// float respawn_table ;
float respawn_table_chance;
float respawn_table_fraction;
float respawn_table_sure;

MONSTER_PROP new_monsters;

char  resource_type_table;
DWORD resource_amount_table;
DWORD resource_tax_table;

char isConstruct_Table;

long mana_regen_table;

void DefaultCreatureValues(void) {
	memset(&new_monsters,0,sizeof(MONSTER_PROP));
	new_monsters.town		= -1;
	upgtable				= -1;
	CreatureMimicArtifact	= 0;
	CreatureMimicArtifact2	= 0;

	strike_all_around_range = 0;
	counterstrike_fury		= 0;
	counterstrike_twice		= 0;

	memset(Wog_Spell_Immunites, 0, 16);

	resource_type_table = 8;
	resource_amount_table = 0;
	resource_tax_table = 0;

	isTeleporter = 0;

	//// MOP Area begin
	RangeRetaliation_Table = 0;
	PreventiveCounterstrikeTable=0;

	ReduceTargetDefense_Table = 0;
	Demonology_Table = 0;
	RegenerationHitPoints_Table=0;
	   RegenerationChance_Table=0;
	ManaDrain_Table=0;
	   SpellsCostDump_Table=0;
	   SpellsCostLess_Table=0;
	   Counterstrike_Table=1;
	   AlwaysPositiveMorale_Table=0;


	   DeathBlow_Table=0;
	   Fear_Table=0;
	   Fearless_Table=0;
	   NoWallPenalty_Table=0;
	   MagicAura_Table=0;
	   StrikeAndReturn_Table=0;
	   Spells_Table=0;
	   Hate_Table=-1;
	   JoustingBonus_Table=0;
	   ImmunToJoustingBonus_Table=0;
	   MagicChannel_Table=0;
	   MagicMirror_Table=0;
	   Sharpshooters_Table=0;
	   ShootingAdjacent_Table=0;
	   // ReduceTargetDefense_Table=0;

	   ImposedSpells_Table[0] = 0;
	   ImposedSpells_Table[1] = 0;
	   ImposedSpells_Table[2] = 0;
	   ImposedSpells_Table[3] = 0;
	   ImposedSpells_Table[4] = 0;
	   ImposedSpells_Table[5] = 0;
   //// MOP Area end

    isPassive_Table = 0;
	isAmmoCart_Table = 0;
	isDragonSlayer_Table = 0;
	isFaerieDragon_Table = 0;
	hasSantaGuards = 0;
	DalionsGuards = -1;
	isRogue = 0;
	isGhost = 0; ghost_fraction = 1.0;
	isEnchanter = 0;
	isSorceress = 0;
	isHellSteed = 0;
	isHellSteed2 = 0;
	isHellSteed3 = 0;
	AlwaysPositiveMorale_Table = 0;
	AlwaysPositiveLuck_Table = 0;
	FireWall_Table = 0;
	ThreeHeadedAttack_Table = 0;


	CreatureSpellPowerMultiplier = 1;
	CreatureSpellPowerDivider = 1;
	CreatureSpellPowerAdder = 0;

	aftercast_abilities_table = ACAST_DEFAULT;
	attack_abilities_table = ATT_DEFAULT;
	magic_resistance_table = RESIST_DEFAULT;
	magic_vulnerability_table = VULN_DEFAULT;

	spell_1_table = 9; spell_2_table = 8; spell_3_table = 3;

	skeltrans = 56; fire_shield_table = 0; respawn_table_sure = 0;
	respawn_table_chance = respawn_table_fraction = 0;
	
	special_missiles_table = 0; 
	missile_size_table = 0;
	missile_anim_table = 0;


	for (int i = 0; i < 1024; ++i) PersonalHate[i] = 0;

	isConstruct_Table = 0;
	isAimedCaster_Table = 0;
	MovesTwice_Table = 0;


	after_melee__spell = 0;
	after_shoot__spell = 0;
	after_defend_spell = 0;
	after_wound__spell = 0;

	after_action_spell_mastery = 2;

	after_melee__spell2 = 0;
	after_shoot__spell2 = 0;
	after_defend_spell2 = 0;
	after_wound__spell2 = 0;


	shooting_resistance = 0.0;
	melee_resistance	= 0.0;

	mana_regen_table =0;


	poison_table = 0;
	aging_table = 0;
	paralyze_table = 0;
	paralyze_chance = 0;

	Receptive_Table = 0;
	isHellHydra = 0;
	isLord_Table = 0;

	NoGolemOverflow = 0;

	isDragonResistant = 0;
	isHeroic = 0;
	StackStep_field_type = 0;
	OverrideSpecialSpells = 0;


	Creature_ACAST_CUSTOM_Chance = 100;
	Creature_ACAST_CUSTOM_Spell = 60;

	forced = 0;
	do_not_generate = 0;
}

void ChangeCreatureValues( char* buf) {
	DefaultCreatureValues();
	char* c; char pattern[256];


	c = strstr(buf, "ForceSaveAllCustomFields=");
	if (c != NULL) forced = atoi(c + strlen("ForceSaveAllCustomFields="));
	else forced = 0;

	c = strstr(buf, "DoNotGenerate=");
	if (c != NULL) do_not_generate = atoi(c + strlen("DoNotGenerate="));

	for (int i = 0; i < 1024; ++i) {
		sprintf(pattern, "Hate%04d=", i);
		c = strstr(buf, pattern);
		if (c != NULL) PersonalHate[i] = atoi(c + strlen(pattern));
	}

	c = strstr(buf, "ACAST_CUSTOM_Chance=");
	if (c != NULL) Creature_ACAST_CUSTOM_Chance = atoi(c + strlen("ACAST_CUSTOM_Chance="));
	c = strstr(buf, "ACAST_CUSTOM_Spell=");
	if (c != NULL) Creature_ACAST_CUSTOM_Spell = atoi(c + strlen("ACAST_CUSTOM_Spell="));

	c = strstr(buf, "OverrideSpecialSpells=");
	if (c != NULL) OverrideSpecialSpells = atoi(c + strlen("OverrideSpecialSpells="));

	c = strstr(buf, "StackStepFieldType=");
	if (c != NULL) StackStep_field_type = atoi(c + strlen("StackStepFieldType="));

	c = strstr(buf, "StrikeAllAroundRange=");
	if (c != NULL) strike_all_around_range = atoi(c + strlen("StrikeAllAroundRange="));

	c = strstr(buf, "CounterstrikeFury=");
	if (c != NULL) counterstrike_fury= atoi(c + strlen("CounterstrikeFury="));

	c = strstr(buf, "CounterstrikeTwice=");
	if (c != NULL) counterstrike_twice = atoi(c + strlen("CounterstrikeTwice="));

	c = strstr(buf, "Wog_Spell_Immunites=\"");
	if (c != NULL) {
		memset(Wog_Spell_Immunites, 0, 16);
		c += strlen("Wog_Spell_Immunites=\"");
		for (int i = 0; i < 16; ++i) {
			if (c[i] == '\"') break;
			Wog_Spell_Immunites[i] = c[i];
		}
	}

	c = strstr(buf, "isHeroic=");
	if (c != NULL) isHeroic = atoi(c + strlen("isHeroic="));

	c = strstr(buf, "isDragonResistant=");
	if (c != NULL) isDragonResistant = atoi(c + strlen("isDragonResistant="));

	c = strstr(buf, "NoGolemOverflow=");
	if (c != NULL) NoGolemOverflow = atoi(c + strlen("NoGolemOverflow="));


	c = strstr(buf, "isLord=");
	if (c != NULL) 	isLord_Table= atoi(c + strlen("isLord="));

	c = strstr(buf, "isHellHydra=");
	if (c != NULL) isHellHydra =
		atoi(c + strlen("isHellHydra="));


	c = strstr(buf, "Receptive=");
	if (c != NULL) Receptive_Table=
		atoi(c + strlen("Receptive="));

	c = strstr(buf, "Aging=");
	if (c != NULL) aging_table = atoi(c + strlen("Aging="));

	c = strstr(buf, "Poison=");
	if (c != NULL) poison_table = atoi(c + strlen("Poison="));


	c = strstr(buf, "Paralyze=");
	if (c != NULL) paralyze_table = atoi(c + strlen("Paralyze="));

	c = strstr(buf, "ParalyzeChance=");
	if (c != NULL) paralyze_chance = atoi(c + strlen("ParalyzeChance="));



	c = strstr(buf, "MeleeResistance=");
	if (c != NULL) melee_resistance		= atof(c + strlen("MeleeResistance="));

	c = strstr(buf, "ShootingResistance=");
	if (c != NULL) shooting_resistance	= atof(c + strlen("ShootingResistance="));

	c = strstr(buf, "isTeleporter=");
	if (c != NULL) isTeleporter = atoi(c + strlen("isTeleporter="));


	c = strstr(buf, "AfterMeleeSpell=");
	if (c != NULL) after_melee__spell = atoi(c + strlen("AfterMeleeSpell="));

	c = strstr(buf, "AfterShootSpell=");
	if (c != NULL) after_shoot__spell = atoi(c + strlen("AfterShootSpell="));

	c = strstr(buf, "AfterDefendSpell=");
	if (c != NULL) after_defend_spell = atoi(c + strlen("AfterDefendSpell="));

	c = strstr(buf, "AfterWoundSpell=");
	if (c != NULL) after_wound__spell = atoi(c + strlen("AfterWoundSpell="));

	c = strstr(buf, "AfterActionSpellMastery=");
	if (c != NULL) after_action_spell_mastery = atoi(c + strlen("AfterActionSpellMastery="));

	c = strstr(buf, "AfterMeleeSpell2=");
	if (c != NULL) after_melee__spell2 = atoi(c + strlen("AfterMeleeSpell2="));

	c = strstr(buf, "AfterShootSpell2=");
	if (c != NULL) after_shoot__spell2 = atoi(c + strlen("AfterShootSpell2="));

	c = strstr(buf, "AfterDefendSpell2=");
	if (c != NULL) after_defend_spell2 = atoi(c + strlen("AfterDefendSpell2="));

	c = strstr(buf, "AfterWoundSpell2=");
	if (c != NULL) after_wound__spell2 = atoi(c + strlen("AfterWoundSpell2="));




	c = strstr(buf, "ReduceTargetDefense=");
	if (c != NULL) ReduceTargetDefense_Table = atoi(c + strlen("ReduceTargetDefense="));

	c = strstr(buf, "ResGen_Amount=");
	if (c != NULL) resource_amount_table = atoi(c + strlen("ResGen_Amount="));

	c = strstr(buf, "ResGen_Type=");
	if (c != NULL) resource_type_table = atoi(c + strlen("ResGen_Type="));

	c = strstr(buf, "ResGen_Tax=");
	if (c != NULL) resource_tax_table = atoi(c + strlen("ResGen_Tax="));


	c = strstr(buf, "CreatureMimicArtifact=");
	if (c != NULL) CreatureMimicArtifact = atoi(c + strlen("CreatureMimicArtifact="));

	c = strstr(buf, "CreatureMimicArtifact2=");
	if (c != NULL) CreatureMimicArtifact2 = atoi(c + strlen("CreatureMimicArtifact2="));

	c = strstr(buf, "UpgradesTo=");
	if (c != NULL) upgtable = atoi(c + strlen("UpgradesTo="));

	//// MOP Area Begin

	c = strstr(buf, "RetaliateRanged=");
	if (c != NULL) RangeRetaliation_Table = atoi(c + strlen("RetaliateRanged="));

	c = strstr(buf, "PreventiveStrike=");
	if (c != NULL) PreventiveCounterstrikeTable = atoi(c + strlen("PreventiveStrike="));

	c = strstr(buf, "Demonology=");
	if (c != NULL) Demonology_Table = atoi(c + strlen("Demonology="));

	c = strstr(buf, "DeathBlow=");
	if (c != NULL) DeathBlow_Table  = atoi(c + strlen("DeathBlow="));
	c = strstr(buf, "Fear=");
	if (c != NULL) Fear_Table  = atoi(c + strlen("Fear="));
	c = strstr(buf, "Fearless=");
	if (c != NULL) Fearless_Table  = atoi(c + strlen("Fearless="));

	c = strstr(buf, "NoWallPenalty=");
	if (c != NULL) NoWallPenalty_Table  = atoi(c + strlen("NoWallPenalty="));
	c = strstr(buf, "Sharpshooters=");
	if (c != NULL) Sharpshooters_Table  = atoi(c + strlen("Sharpshooters="));
	c = strstr(buf, "ShootingAdjacent=");
	if (c != NULL) ShootingAdjacent_Table  = atoi(c + strlen("ShootingAdjacent="));


	c = strstr(buf, "MagicAura=");
	if (c != NULL) MagicAura_Table  = atoi(c + strlen("MagicAura="));
	c = strstr(buf, "MagicMirror=");
	if (c != NULL) MagicMirror_Table  = atoi(c + strlen("MagicMirror="));
	c = strstr(buf, "MagicChannel=");
	if (c != NULL) MagicChannel_Table  = atoi(c + strlen("MagicChannel="));


	c = strstr(buf, "StrikeAndReturn=");
	if (c != NULL) StrikeAndReturn_Table  = atoi(c + strlen("StrikeAndReturn="));
	c = strstr(buf, "JoustingBonus=");
	if (c != NULL) JoustingBonus_Table  = atoi(c + strlen("JoustingBonus="));
	c = strstr(buf, "JoustingImmune=");
	if (c != NULL) ImmunToJoustingBonus_Table  = atoi(c + strlen("JoustingImmune="));

	c = strstr(buf, "RegenerationChance=");
	if (c != NULL) RegenerationChance_Table  = atoi(c + strlen("RegenerationChance="));

	c = strstr(buf, "RegenerationHitPoints=");
	if (c != NULL) RegenerationHitPoints_Table  = atoi(c + strlen("RegenerationHitPoints="));


	c = strstr(buf, "Retaliations=");
	if (c != NULL) Counterstrike_Table  = atoi(c + strlen("Retaliations="));



	c = strstr(buf, "CastsSpell=");
	if (c != NULL)Spells_Table = atoi(c + strlen("CastsSpell="));

	c = strstr(buf, "ManaDrain=");
	if (c != NULL) ManaDrain_Table  = atoi(c + strlen("ManaDrain="));



	c = strstr(buf, "SpellsCostLess=");
	if (c != NULL) SpellsCostLess_Table  = atoi(c + strlen("SpellsCostLess="));

	c = strstr(buf, "SpellsCostDump=");
	if (c != NULL) SpellsCostDump_Table  = atoi(c + strlen("SpellsCostDump="));


	c = strstr(buf, "isCerberus=");
	if (c != NULL) ThreeHeadedAttack_Table		= atoi(c + strlen("isCerberus="));

	c = strstr(buf, "hasFirewall=");
	if (c != NULL) FireWall_Table				= atoi(c + strlen("hasFirewall="));

	c = strstr(buf, "AlwaysPositiveMorale=");
	if (c != NULL) AlwaysPositiveMorale_Table	= atoi(c + strlen("AlwaysPositiveMorale="));

	c = strstr(buf, "AlwaysPositiveLuck=");
	if (c != NULL) AlwaysPositiveLuck_Table		= atoi(c + strlen("AlwaysPositiveLuck="));




	c = strstr(buf, "ImposedSpell1Number=");
	if (c != NULL) ImposedSpells_Table[0] = atoi(c + strlen("ImposedSpell1Number="));
	c = strstr(buf, "ImposedSpell1Level=");
	if (c != NULL) ImposedSpells_Table[3] = atoi(c + strlen("ImposedSpell1Level="));

	c = strstr(buf, "ImposedSpell2Number=");
	if (c != NULL) ImposedSpells_Table[1] = atoi(c + strlen("ImposedSpell2Number="));
	c = strstr(buf, "ImposedSpell2Level=");
	if (c != NULL) ImposedSpells_Table[4] = atoi(c + strlen("ImposedSpell2Level="));

	c = strstr(buf, "ImposedSpell3Number=");
	if (c != NULL) ImposedSpells_Table[2] = atoi(c + strlen("ImposedSpell3Number="));
	c = strstr(buf, "ImposedSpell3Level=");
	if (c != NULL) ImposedSpells_Table[5] = atoi(c + strlen("ImposedSpell3Level="));
	//// MOP Area end



	c = strstr(buf, "CreatureSpellPowerMultiplier=");
	if (c != NULL) CreatureSpellPowerMultiplier  = atoi(c + strlen("CreatureSpellPowerMultiplier="));

	c = strstr(buf, "CreatureSpellPowerDivider=");
	if (c != NULL) CreatureSpellPowerDivider  = atoi(c + strlen("CreatureSpellPowerDivider="));

	c = strstr(buf, "CreatureSpellPowerAdder=");
	if (c != NULL) CreatureSpellPowerAdder  = atoi(c + strlen("CreatureSpellPowerAdder="));

	c = strstr(buf, "hasSantaGuards=");
	if (c != NULL) hasSantaGuards  = atoi(c + strlen("hasSantaGuards="));
	c = strstr(buf, "DalionsGuards=");
	if (c != NULL) DalionsGuards  = atoi(c + strlen("DalionsGuards="));


	c = strstr(buf, "isRogue=");
	if (c != NULL) isRogue  = atoi(c + strlen("isRogue="));

	c = strstr(buf, "isGhost=");
	if (c != NULL) isGhost  = atoi(c + strlen("isGhost="));
	c = strstr(buf, "GhostFraction=");
	if (c != NULL) ghost_fraction= atof(c + strlen("GhostFraction="));

	c = strstr(buf, "isEnchanter=");
	if (c != NULL) isEnchanter  = atoi(c + strlen("isEnchanter="));

	c = strstr(buf, "isSorceress=");
	if (c != NULL) isSorceress  = atoi(c + strlen("isSorceress="));

	c = strstr(buf, "isFaerie=");
	if (c != NULL) isFaerieDragon_Table = atoi(c + strlen("isFaerie="));

	c = strstr(buf, "isDragonSlayer=");
	if (c != NULL) isDragonSlayer_Table = atoi(c + strlen("isDragonSlayer="));

	c = strstr(buf, "isPassive=");
	if (c != NULL) isPassive_Table = atoi(c + strlen("isPassive="));
	c = strstr(buf, "isAmmoCart=");
	if (c != NULL) isAmmoCart_Table = atoi(c + strlen("isAmmoCart="));

	c = strstr(buf, "isAimedCaster=");
	if (c != NULL) isAimedCaster_Table= atoi(c + strlen("isAimedCaster="));

	c = strstr(buf, "isHellSteed=");
	if (c != NULL) isHellSteed  = atoi(c + strlen("isHellSteed="));
	c = strstr(buf, "isHellSteed2=");
	if (c != NULL) isHellSteed2  = atoi(c + strlen("isHellSteed2="));
	c = strstr(buf, "isHellSteed3=");
	if (c != NULL) isHellSteed3  = atoi(c + strlen("isHellSteed3="));

	//end of majaczek
	c = strstr(buf, "Level=");
	if (c != NULL)
		new_monsters.level = atoi(c + strlen("Level="));

	c = strstr(buf, "Flags=");
	if (c != NULL)
		new_monsters.flags = atoi(c + strlen("Flags="));


	c = strstr(buf, "Town=");
	if (c != NULL)
		new_monsters.town = atoi(c + strlen("Town="));
	//			
	///*
	//Singular	Plural	Wood	Mercury	Ore	Sulfur	Crystal	Gems	Gold	Fight Value	AI Value	Growth	Horde Growth	
	//Hit Points	Speed	Attack	Defense	Low	High	
	//Shots	Spells	Low	High	Ability Text	Attributes */
	//
	c = strstr(buf, "Adv.high=");
	if (c != NULL) new_monsters.adv_high =
		atoi(c + strlen("Adv.high="));
	//			else new_monsters.adv_high = 0;
	//
	c = strstr(buf, "Adv.low=");
	if (c != NULL) new_monsters.adv_low =
		atoi(c + strlen("Adv.low="));
	//			else new_monsters.i_AdvLow = 0;
	//
	c = strstr(buf, "AI_value=");
	if (c != NULL) new_monsters.ai_value =
		atoi(c + strlen("AI_value="));
	//
	c = strstr(buf, "Attack=");
	if (c != NULL) new_monsters.attack =
		atoi(c + strlen("Attack="));
	//
	c = strstr(buf, "CostCrystal=");
	if (c != NULL) new_monsters.cost_crystal =
		atoi(c + strlen("CostCrystal="));
	//
	c = strstr(buf, "CostGems=");
	if (c != NULL) new_monsters.cost_gems =
		atoi(c + strlen("CostGems="));
	//
	c = strstr(buf, "CostGold=");
	if (c != NULL) new_monsters.cost_gold =
		atoi(c + strlen("CostGold="));
	//
	c = strstr(buf, "CostMercury=");
	if (c != NULL) new_monsters.cost_mercury =
		atoi(c + strlen("CostMercury="));
	//
	c = strstr(buf, "CostOre=");
	if (c != NULL) new_monsters.cost_ore =
		atoi(c + strlen("CostOre="));
	//
	c = strstr(buf, "CostSulfur=");
	if (c != NULL) new_monsters.cost_sulfur =
		atoi(c + strlen("CostSulfur="));
	//
	c = strstr(buf, "CostWood=");
	if (c != NULL) new_monsters.cost_wood =
		atoi(c + strlen("CostWood="));
	//
	c = strstr(buf, "DamageHigh=");
	if (c != NULL) new_monsters.damage_high =
		atoi(c + strlen("DamageHigh="));
	//
	c = strstr(buf, "DamageLow=");
	if (c != NULL) new_monsters.damage_low =
		atoi(c + strlen("DamageLow="));
	//
	c = strstr(buf, "Defence=");
	if (c != NULL) new_monsters.defence =
		atoi(c + strlen("Defence="));
	//
	c = strstr(buf, "FightValue=");
	if (c != NULL) new_monsters.fight_value =
		atoi(c + strlen("FightValue="));
	//
	c = strstr(buf, "Growth=");
	if (c != NULL) new_monsters.growth =
		atoi(c + strlen("Growth="));
	//
	c = strstr(buf, "HP=");
	if (c != NULL) new_monsters.hp =
		atoi(c + strlen("HP="));
	//
	c = strstr(buf, "Horde=");
	if (c != NULL) new_monsters.horde_growth =
		atoi(c + strlen("Horde="));
	//
	c = strstr(buf, "Shots=");
	if (c != NULL) new_monsters.shots =
		atoi(c + strlen("Shots="));
	//
	c = strstr(buf, "Speed=");
	if (c != NULL) new_monsters.speed =
		atoi(c + strlen("Speed="));
	//
	c = strstr(buf, "Spells=");
	if (c != NULL) new_monsters.spells =
		atoi(c + strlen("Spells="));
	//
	///*Time between fidgets	Walk Animation Time	Attack Animation Time	
	//Flight Animation Distance	Upper-right Missile Offset		
	//Right Missile Offset		Lower-right Missile Offset		Missile Frame Angles												
	//Troop Count Location Offset	Attack Climax Frame*/
	//
	///*
	//			c = strstr(buf,"TBF=" );
	//			if (c!=NULL) new_monsters.i_Spells=
	//						atoi(c+strlen("TBF=" ));
	//*/


	c = strstr(buf, "Spell effect=");
	if (c != NULL) aftercast_abilities_table  =
		atoi(c + strlen("Spell effect="));

	c = strstr(buf, "Attack effect=");
	if (c != NULL) attack_abilities_table  =
		atoi(c + strlen("Attack effect="));

	c = strstr(buf, "Resistance effect=");
	if (c != NULL) magic_resistance_table  =
		atoi(c + strlen("Resistance effect="));

	c = strstr(buf, "Vulnerability effect="); // majaczek
	if (c != NULL) magic_vulnerability_table  =
		atoi(c + strlen("Vulnerability effect="));




	c = strstr(buf, "Spell 1=");
	if (c != NULL) spell_1_table  =
		atoi(c + strlen("Spell 1="));


	c = strstr(buf, "Spell 2=");
	if (c != NULL) spell_2_table  =
		atoi(c + strlen("Spell 2="));


	c = strstr(buf, "Spell 3=");
	if (c != NULL) spell_3_table  =
		atoi(c + strlen("Spell 3="));

	c = strstr(buf, "Sktransformer=");
	if (c != NULL) skeltrans  =
		atoi(c + strlen("Sktransformer="));



	c = strstr(buf, "Shot type=");
	if (c != NULL) special_missiles_table  =
		atoi(c + strlen("Shot type="));

	c = strstr(buf, "Shot size=");
	if (c != NULL) missile_size_table =
		atoi(c + strlen("Shot size="));

	c = strstr(buf, "Shot anim=");
	if (c != NULL) missile_anim_table =
		atoi(c + strlen("Shot anim="));




	c = strstr(buf, "Fire shield=");
	if (c != NULL) fire_shield_table  =
		(float)atof(c + strlen("Fire shield="));


	// deprecated but left for backwards compatibility
	c = strstr(buf, "Self-resurrection=");
	if (c != NULL) respawn_table_fraction = respawn_table_chance = respawn_table_sure =
		(float)atof(c + strlen("Self-resurrection="));

	c = strstr(buf, "rebirth chance=");
	if (c != NULL) 	respawn_table_chance =
		(float)atof(c + strlen("rebirth chance="));

	c = strstr(buf, "rebirth fraction=");
	if (c != NULL) 	respawn_table_fraction=
		(float)atof(c + strlen("rebirth fraction="));

	c = strstr(buf, "rebirth sure=");
	if (c != NULL) 	respawn_table_sure =
		(float)atof(c + strlen("rebirth sure="));


	c = strstr(buf, "isConstruct=");
	if (c != NULL) isConstruct_Table =
		atoi(c + strlen("isConstruct="));


	c = strstr(buf, "MovesTwice=");
	if (c != NULL) MovesTwice_Table=
		atoi(c + strlen("MovesTwice="));

	c = strstr(buf, "ManaRegen=");
	if (c != NULL) mana_regen_table=
		atoi(c + strlen("ManaRegen="));
}

bool writeCreatureValues(char* filename) {

	if (auto fdesc = fopen(filename, "w"))
	{
		if (forced) fprintf(fdesc, "ForceSaveAllCustomFields=1\n\n");


		if (forced || do_not_generate) fprintf(fdesc, "DoNotGenerate=%d\n", (int)do_not_generate);

		for(int i=0; i<1024; ++i) if (PersonalHate[i])
			fprintf(fdesc, "Hate%04d=%d\n", i, PersonalHate[i]);

		if (Wog_Spell_Immunites[0]) {
			fprintf(fdesc, "Wog_Spell_Immunites=\"%s\"\n",Wog_Spell_Immunites);
		}

		if (forced || isTeleporter) fprintf(fdesc, "isTeleporter=%d\n", (int)isTeleporter);


		if (forced || Creature_ACAST_CUSTOM_Chance) fprintf(fdesc, "ACAST_CUSTOM_Chance=%d\n", Creature_ACAST_CUSTOM_Chance);
		if (forced || Creature_ACAST_CUSTOM_Spell) fprintf(fdesc, "ACAST_CUSTOM_Spell=%d\n", Creature_ACAST_CUSTOM_Spell);


		if (forced || shooting_resistance)	fprintf(fdesc, "ShootingResistance=%f\n",	shooting_resistance);
		if (forced || melee_resistance)		fprintf(fdesc, "MeleeResistance=%f\n",		melee_resistance);

		if (forced || after_melee__spell) fprintf(fdesc, "AfterMeleeSpell=%d\n",  after_melee__spell);
		if (forced || after_shoot__spell) fprintf(fdesc, "AfterShootSpell=%d\n",  after_shoot__spell);
		if (forced || after_defend_spell) fprintf(fdesc, "AfterDefendSpell=%d\n", after_defend_spell);
		if (forced || after_wound__spell) fprintf(fdesc, "AfterWoundSpell=%d\n",  after_wound__spell);
		if (forced || after_action_spell_mastery != 2) fprintf(fdesc, "AfterActionSpellMastery=%d\n", (int)after_action_spell_mastery);
		if (forced || after_melee__spell2) fprintf(fdesc, "AfterMeleeSpell2=%d\n", after_melee__spell2);
		if (forced || after_shoot__spell2) fprintf(fdesc, "AfterShootSpell2=%d\n", after_shoot__spell2);
		if (forced || after_defend_spell2) fprintf(fdesc, "AfterDefendSpell2=%d\n", after_defend_spell2);
		if (forced || after_wound__spell2) fprintf(fdesc, "AfterWoundSpell2=%d\n", after_wound__spell2);



		if (forced || ReduceTargetDefense_Table)	fprintf(fdesc, "ReduceTargetDefense=%d\n", ReduceTargetDefense_Table);

		if (forced || resource_tax_table)		fprintf(fdesc, "ResGen_Tax=%d\n", resource_tax_table);
		if (forced || resource_type_table<8)  fprintf(fdesc, "ResGen_Type=%d\n", resource_type_table);
		if (forced || resource_amount_table)  fprintf(fdesc, "ResGen_Amount=%d\n", resource_amount_table);

		if (forced || new_monsters.flags)     fprintf(fdesc, "Flags=%d\n", new_monsters.flags);
		/*if (new_monsters.level)*/ fprintf(fdesc, "Level=%d\n", new_monsters.level);
		if (forced || new_monsters.town!=-1)  fprintf(fdesc, "Town=%d\n",  new_monsters.town);
		if (forced || upgtable >=0 )			fprintf(fdesc, "UpgradesTo=%d\n", upgtable);

		if (forced || aftercast_abilities_table != 9) fprintf(fdesc, "Spell effect=%d\n", aftercast_abilities_table);
		if (forced || attack_abilities_table    != 5) fprintf(fdesc, "Attack effect=%d\n", attack_abilities_table);
		if (forced || magic_resistance_table    != 8) fprintf(fdesc, "Resistance effect=%d\n", magic_resistance_table);
		if (forced || magic_vulnerability_table != 8) fprintf(fdesc, "Vulnerability effect=%d\n", magic_vulnerability_table);

		if (forced || spell_1_table !=9) fprintf(fdesc, "Spell 1=%d\n", spell_1_table);
		if (forced || spell_2_table !=8) fprintf(fdesc, "Spell 2=%d\n", spell_2_table);
		if (forced || spell_3_table !=3) fprintf(fdesc, "Spell 3=%d\n", spell_3_table);

		if (forced || CreatureSpellPowerMultiplier != 1)	fprintf(fdesc, "CreatureSpellPowerMultiplier=%d\n", CreatureSpellPowerMultiplier);
		if (forced || CreatureSpellPowerDivider    != 1)	fprintf(fdesc, "CreatureSpellPowerDivider=%d\n", CreatureSpellPowerDivider);
		if (forced || CreatureSpellPowerAdder      != 0)	fprintf(fdesc, "CreatureSpellPowerAdder=%d\n", CreatureSpellPowerAdder);



		if (forced || ImposedSpells_Table[0])		fprintf(fdesc, "ImposedSpell1Number=%d\nImposedSpell1Level=%d\n", ImposedSpells_Table[0], ImposedSpells_Table[3]);
		if (forced || ImposedSpells_Table[1])		fprintf(fdesc, "ImposedSpell2Number=%d\nImposedSpell2Level=%d\n", ImposedSpells_Table[1], ImposedSpells_Table[4]);
		if (forced || ImposedSpells_Table[2])		fprintf(fdesc, "ImposedSpell3Number=%d\nImposedSpell3Level=%d\n", ImposedSpells_Table[2], ImposedSpells_Table[5]);


		if (forced || RegenerationHitPoints_Table)	fprintf(fdesc, "RegenerationHitPoints=%d\n", RegenerationHitPoints_Table);
		if (forced || RegenerationChance_Table)		fprintf(fdesc, "RegenerationChance=%d\n",	 RegenerationChance_Table);
		if (forced || ManaDrain_Table)				fprintf(fdesc, "ManaDrain=%d\n",			 ManaDrain_Table);
		if (forced || DeathBlow_Table)				fprintf(fdesc, "DeathBlow=%d\n",			 DeathBlow_Table);
		if (forced || SpellsCostLess_Table)			fprintf(fdesc, "SpellsCostLess=%d\n",		 SpellsCostLess_Table);
		if (forced || SpellsCostDump_Table)			fprintf(fdesc, "SpellsCostDump=%d\n",		 SpellsCostDump_Table);
		if (forced || Counterstrike_Table != 1)		fprintf(fdesc, "Retaliations=%d\n",			 Counterstrike_Table);
		if (forced || Spells_Table)					fprintf(fdesc, "CastsSpell=%d\n",			 Spells_Table);
		if (forced || Demonology_Table)				fprintf(fdesc, "Demonology=%d\n",			 Demonology_Table);

		if (forced || RangeRetaliation_Table)			fprintf(fdesc, "RetaliateRanged=%d\n",(int)RangeRetaliation_Table);
		if (forced || PreventiveCounterstrikeTable)	fprintf(fdesc, "PreventiveStrike=%d\n",(int)PreventiveCounterstrikeTable);

		if (forced || Fear_Table)						fprintf(fdesc, "Fear=%d\n",(int)Fear_Table);
		if (forced || Fearless_Table)					fprintf(fdesc, "Fearless=%d\n",(int)Fearless_Table);
		if (forced || NoWallPenalty_Table)			fprintf(fdesc, "NoWallPenalty=%d\n",(int)NoWallPenalty_Table);
		if (forced || Sharpshooters_Table)			fprintf(fdesc, "Sharpshooters=%d\n",(int)Sharpshooters_Table);
		if (forced || ShootingAdjacent_Table)			fprintf(fdesc, "ShootingAdjacent=%d\n",(int)ShootingAdjacent_Table);
		if (forced || StrikeAndReturn_Table)			fprintf(fdesc, "StrikeAndReturn=%d\n",(int)StrikeAndReturn_Table);
		if (forced || JoustingBonus_Table)			fprintf(fdesc, "JoustingBonus=%d\n",(int)JoustingBonus_Table);
		if (forced || ImmunToJoustingBonus_Table)		fprintf(fdesc, "JoustingImmune=%d\n",(int)ImmunToJoustingBonus_Table);
		if (forced || MagicAura_Table)				fprintf(fdesc, "MagicAura=%d\n",(int)MagicAura_Table);
		if (forced || MagicMirror_Table)				fprintf(fdesc, "MagicMirror=%d\n",(int)MagicMirror_Table);
		if (forced || MagicChannel_Table)				fprintf(fdesc, "MagicChannel=%d\n",(int)MagicChannel_Table);


		if (forced || isAimedCaster_Table)			fprintf(fdesc, "isAimedCaster=%d\n", (int)isAimedCaster_Table);
		if (forced || isPassive_Table)				fprintf(fdesc, "isPassive=%d\n", (int)isPassive_Table);
		if (forced || isAmmoCart_Table)				fprintf(fdesc, "isAmmoCart=%d\n", (int)isAmmoCart_Table);
		if (forced || isFaerieDragon_Table)			fprintf(fdesc, "isFaerie=%d\n",(int)isFaerieDragon_Table);
		if (forced || isDragonSlayer_Table)			fprintf(fdesc, "isDragonSlayer=%d\n",(int)isDragonSlayer_Table);
		if (forced || hasSantaGuards)					fprintf(fdesc, "hasSantaGuards=%d\n",(int)hasSantaGuards);
		if (forced || DalionsGuards >=0)				fprintf(fdesc, "DalionsGuards=%d\n", DalionsGuards);
		if (forced || isRogue)       					fprintf(fdesc, "isRogue=%d\n",(int)isRogue);
		if (forced || isGhost)        				fprintf(fdesc, "isGhost=%d\n",(int)isGhost);
		if (forced || isEnchanter)    				fprintf(fdesc, "isEnchanter=%d\n",(int)isEnchanter);
		if (forced || isSorceress)    				fprintf(fdesc, "isSorceress=%d\n",(int)isSorceress);

		if (isHellSteed)					fprintf(fdesc, "isHellSteed=%d\n",(int)isHellSteed);
		if (isHellSteed2)					fprintf(fdesc, "isHellSteed2=%d\n",(int)isHellSteed2);
		if (isHellSteed3)					fprintf(fdesc, "isHellSteed3=%d\n",(int)isHellSteed3);


		if (forced || FireWall_Table)					fprintf(fdesc, "hasFirewall=%d\n",(int)FireWall_Table);
		if (forced || AlwaysPositiveMorale_Table)		fprintf(fdesc, "AlwaysPositiveMorale=%d\n",(int)AlwaysPositiveMorale_Table);
		if (forced || AlwaysPositiveLuck_Table)		fprintf(fdesc, "AlwaysPositiveLuck=%d\n",(int)AlwaysPositiveLuck_Table);
		if (forced || ThreeHeadedAttack_Table)		fprintf(fdesc, "isCerberus=%d\n",(int)ThreeHeadedAttack_Table);

		if (forced || skeltrans != 56)			fprintf(fdesc, "Sktransformer=%d\n", skeltrans);
		if (forced || special_missiles_table)	fprintf(fdesc, "Shot type=%d\n", special_missiles_table);
		if (forced || missile_size_table)		fprintf(fdesc, "Shot size=%d\n", missile_size_table);
		if (forced || missile_anim_table)		fprintf(fdesc, "Shot anim=%d\n", missile_anim_table);

		if (forced || fire_shield_table != 0.0) fprintf(fdesc, "Fire shield=%f\n", fire_shield_table);
		// if (forced || respawn_table     != 0.0) fprintf(fdesc, "Self-resurrection=%f\n", respawn_table);
		if (forced || respawn_table_chance != 0.0)		fprintf(fdesc, "rebirth chance=%f\n", respawn_table_chance);
		if (forced || respawn_table_fraction != 0.0)	fprintf(fdesc, "rebirth fraction=%f\n", respawn_table_fraction);
		if (forced || respawn_table_sure != 0.0)		fprintf(fdesc, "rebirth sure=%f\n", respawn_table_sure);


		if (forced || CreatureMimicArtifact)	fprintf(fdesc, "CreatureMimicArtifact=%d\n" , CreatureMimicArtifact);
		if (forced || CreatureMimicArtifact2)	fprintf(fdesc, "CreatureMimicArtifact2=%d\n", CreatureMimicArtifact2);

		if (forced || mana_regen_table)			fprintf(fdesc, "ManaRegen=%d\n", mana_regen_table);

		if (forced || isConstruct_Table)		fprintf(fdesc, "isConstruct=%d\n", isConstruct_Table);
		if (forced || MovesTwice_Table)			fprintf(fdesc, "MovesTwice=%d\n", (int) MovesTwice_Table);


		if (forced || poison_table )			fprintf(fdesc, "Poison=%d\n", poison_table);
		if (forced || aging_table )				fprintf(fdesc, "Aging=%d\n", aging_table);
		if (forced || paralyze_table )			fprintf(fdesc, "Paralyze=%d\n", paralyze_table);
		if (forced || paralyze_chance)			fprintf(fdesc, "ParalyzeChance=%d\n", paralyze_chance);

		if (forced || Receptive_Table)			fprintf(fdesc, "Receptive=%d\n", (int) Receptive_Table);
		if (forced || isHellHydra)				fprintf(fdesc, "isHellHydra=%d\n", (int) isHellHydra);
		if (forced || isLord_Table)				fprintf(fdesc, "isLord=%d\n", (int)isLord_Table);

		if (forced || ghost_fraction != 1.0)	fprintf(fdesc, "GhostFraction=%f\n", ghost_fraction);

		if (forced || NoGolemOverflow)			fprintf(fdesc, "NoGolemOverflow=%d\n", (int)NoGolemOverflow);
		if (forced || isDragonResistant)		fprintf(fdesc, "isDragonResistant=%d\n", (int)isDragonResistant);
		if (forced || isHeroic)					fprintf(fdesc, "isHeroic=%d\n", (int)isHeroic);
		if (forced || counterstrike_fury)		fprintf(fdesc, "CounterstrikeFury=%d\n", (int)counterstrike_fury);
		if (forced || counterstrike_twice)		fprintf(fdesc, "CounterstrikeTwice=%d\n", (int)counterstrike_twice);

		if (forced || strike_all_around_range)	fprintf(fdesc, "StrikeAllAroundRange=%d\n", (int) strike_all_around_range);
		if (forced || StackStep_field_type)		fprintf(fdesc, "StackStepFieldType=%d\n",	(int)StackStep_field_type);
		if (forced || OverrideSpecialSpells)	fprintf(fdesc, "OverrideSpecialSpells=%d\n", (int)OverrideSpecialSpells);

		fclose(fdesc);
		return true;
	}
	else return false;
}