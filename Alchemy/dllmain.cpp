// dllmain.cpp : Defines the entry point for the DLL application.

#pragma warning(disable : 4996)
typedef struct IUnknown IUnknown;
#include "pch.h"

#define _H3API_PLUGINS_
#define _H3API_MESSAGES_
// #define _H3API_EXCEPTION
#define _H3API_PATCHER_X86_
// #include "../__include__/patcher_x86_commented.hpp"
#include "../__include__/H3API/single_header/H3API.hpp"
#include "../__include__/era.h"
// #include <cstdio>

void (*ChangeCreatureTable_ptr)(int,char*) = nullptr;

inline int z_MsgBox(const char* Mes, int MType, int PosX, int PosY, int Type1, int SType1, int Type2, int SType2, int Par, int Time2Show, int Type3, int SType3) {
    FASTCALL_12(int, 0x004F6C00, Mes, MType, PosX, PosY, Type1, SType1, Type2, SType2, Par, Time2Show, Type3, SType3);

    int IDummy;
    __asm {
        mov eax, 0x6992D0
        mov ecx, [eax]
        mov eax, [ecx + 0x38]
        mov IDummy, eax
    }
    return IDummy;
}
void z_shout(const char* Mes) {
    z_MsgBox((char*)Mes, 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
}
void z_whisper(const char* Mes) {
    z_MsgBox((char*)Mes, 4, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
}

void ParseInt(char* buf, const char* name, int* result)
{
    char* c = strstr(buf, name);
    if (c != NULL)
        *result = atoi(c + strlen(name));
}

#define char_table_size 4096
bool ParseStr(char* buf, const char* name, char res[char_table_size])
{

    char* c = strstr(buf, name);
    int l = strlen(name);
    if (c != NULL)
    {
        char* cend = strstr(c + l, "\"");
        if (cend != NULL) {
            memset(res, 0, cend - c + 1);

            for (int i = 0, j = 0; j < (cend - c) - l; i++, j++)
            {
                if (c[l + j] != '\\')
                {
                    res[i] = c[l + j];
                }
                else
                {
                    if (c[l + j + 1] == 'r')
                    {
                        res[i] = '\r';

                        j++;
                        continue;
                    }
                    else if (c[l + j + 1] == 'n')
                    {
                        res[i] = '\n';
                        j++;
                        continue;
                    }
                    else
                    {
                        res[i] = c[l + j];

                    }
                }
            }

            res[cend - c - l] = 0;

        }
        return true;
    }
    else return false;
}

void ExecErmString(char* str) {
    if (!str) return;
    char* here = str;
    char buf[4096] = "";
    while (*here) {
        if (*here == '!') {
            here++;
            if (*here == '!') {
                here++; int index = 0;
                while (*here && *here != ';' && index < 4096) {
                    buf[index] = *here;
                    here++; index++;
                }
                buf[index] = ';';
                buf[index + 1] = 0;
                if (*here) Era::ExecErmCmd(buf);
                else Era::ExecErmCmd("IF:M^ ExecErmString \n\n Syntax Error \n\n missing semicolon ^;");
            }
        }
        here++;
    }
}

#define MAX_REAGENTS   64
#define MAX_POTIONS    64
#define MAX_HEROES   1024
#define LIMIT_POWER   100

char is_art_alchemical[1024] = {};
int get_reagent_ID(h3::H3Artifact& art);
int get_potion_ID(h3::H3Artifact& art);

struct AlchemyBag {
    short potions  [MAX_POTIONS]  [LIMIT_POWER];
    short reagents [MAX_REAGENTS] [LIMIT_POWER];
    bool AddArtifact(h3::H3Artifact& art);
    long Calc_Potions_of_type(int pot_id) {
        int answer = 0;
        if (pot_id>=MAX_POTIONS) return 0;
        for (auto count : potions[pot_id])
            answer += count;
        return answer;
    }
    long Calc_Reagents_of_type(int pot_id) {
        int answer = 0;
        if (pot_id >= MAX_REAGENTS) return 0;
        for (auto count : reagents[pot_id])
            answer += count;
        return answer;
    }
    long HasArtifact(h3::H3Artifact &art) {
        if (art.subtype >= LIMIT_POWER) return false;
        switch (is_art_alchemical[art.id]) {
        case 0: return false;
        case 1: return reagents[get_reagent_ID(art)][art.subtype];
        case 2: return potions[get_potion_ID(art)][art.subtype];
        default: return false;
        }
    }
    bool RemoveArtifact(h3::H3Artifact& art) {
        if (!HasArtifact(art)) return false;
        switch (is_art_alchemical[art.id]) {
        case 0: return false;
        case 1: --(reagents[get_reagent_ID(art)][art.subtype]); return true;
        case 2: --(potions[get_potion_ID(art)][art.subtype]); return true;
        default: return false;
        }
    }
    bool RemoveArtifact(h3::H3Artifact& art1, h3::H3Artifact& art2) {
        if (!HasArtifact(art1)) return false;
        if (!HasArtifact(art2)) return false;
        RemoveArtifact(art1); RemoveArtifact(art2);
        return true;
    }
    void clean() {
        for (auto& i : potions) for (auto& j : i) j = 0;
        for (auto& i : reagents) for (auto& j : i) j = 0;
    }

    long get_fitting_bottle(int min_power);
    bool CollectReagent(int rea_id, int bonus);
};

AlchemyBag BagPerHero[MAX_HEROES];
short mixing_table[MAX_POTIONS][MAX_POTIONS] = {};

struct ReagentInfo {
    char color[4]; int art_id; int base_power; char name[256]; int terrain;
};

ReagentInfo ReagentInfos[MAX_REAGENTS] = {
    {"B",617,1, "Root"},
    {"R",618,1, "Red Berries"},
    {"X",619,1, "Shroom"},
    {"Y",620,1, "Flower"},

    {"R",632,5,  "Fire Berries"},
    {"R",621,10, "Enchanted Rose"},
    {"R",635,15, "Wolven Eye"},
    {"R",636,20, "Lesser Ruby"},
    {"R",622,25, "Greater Ruby"},
    {"R",623,50, "Dragon Eye"},

    {"B",631,5,  "Phirma Root"},
    {"B",637,10, "Meterorite"},
    {"B",624,15, "Lapis Lazuli"},
    {"B",639,20, "Belladonna"},
    {"B",638,25, "Wisp Heart"},
    {"B",625,35, "Greater Saphire"},
    {"B",626,50, "Magic Mushroom"},

    {"Y",634,5, "Poppy"},
    {"Y",640,10, "Thorn"},
    {"Y",641,15, "Sulfur"},
    {"Y",627,20, "Pyrite"},
    {"Y",642,25, "Lesser Topaz"},
    {"Y",628,50, "Greater Topaz"},

    {"X",633,5, "Grey Mushroom"},
    {"X",643,10, "Onyx"},
    {"X",644,15, "Mercury"},
    {"X",645,20, "Lavastone"},
    {"X",629,25, "Vulcano Ashes"},
    {"X",630,50, "Philosopher's Stone"},
};
int get_reagent_ID(h3::H3Artifact& art) {
    if (is_art_alchemical[art.id] != 1) return -1;
    auto nfo = ReagentInfos;
    for (int i = 0; nfo[i].color; ++i) {
        if (art.id == nfo[i].art_id) {
            return i;
        }
    }
    return -1;
}
bool AlchemyBag::CollectReagent(int rea_id, int bonus) {
    if (rea_id < 0 || rea_id >= MAX_REAGENTS) return false;
    auto &rea_nfo = ReagentInfos[rea_id];
    if (rea_nfo.color[0] == 0) return false;
    h3::H3Artifact ret; 
    ret.id = rea_nfo.art_id;
    ret.subtype = rea_nfo.base_power + bonus;
    return AddArtifact(ret);
}

struct PotionInfo {
    char color[12]; int art_id; char name[256];
    int flags; char desc[char_table_size];
    enum {
        IN_ADVENTURE = 1, IN_BATTLE = 2, IN_BOTH = 3,
    };
    char* Amethyst_Modifier; 
    char* hero_script;
    char* unit_script;
};
PotionInfo PotionInfos[MAX_POTIONS] = {
    {"E",647,"Empty Bottle"},
    {"B",648,"Mana Potion (Blue)", PotionInfo::IN_BOTH},
    {"R",649,"Life Potion (Red)", PotionInfo::IN_BATTLE},
    {"Y",650,"Stamina Potion (Yellow)", PotionInfo::IN_ADVENTURE},

    {"X",652,"Catalyst (Grey)"},
    {"P",654,"Potion (Purple)"},
    {"G",655,"Potion (Green)"},
    {"O",656,"Potion (Orange)"},

    {"RO",657, "Layered Potion (Red+Orange)"},
    {"RP",658, "Layered Potion (Red+Purple)"},
    {"RG",659, "Layered Potion (Red+Green)"},
    {"BO",660, "Layered Potion (Blue+Orange)"},
    {"BP",661, "Layered Potion (Blue+Purple)"},
    {"BG",662, "Layered Potion (Blue+Green)"},
    {"YO",663, "Layered Potion (Yellow+Orange)"},
    {"YP",664, "Layered Potion (Yellow+Purple)"},
    {"YG",665, "Layered Potion (Yellow+Green)"},
    {"OP",666, "Layered Potion (Orange+Purple)"},
    {"GP",667, "Layered Potion (Green+Purple)"},
    {"GO",668, "Layered Potion (Green+Orange)"},
};


long AlchemyBag::get_fitting_bottle(int min_power) {
    h3::H3Artifact art = {PotionInfos->art_id, -1};
    for (int i = min_power; i < LIMIT_POWER; ++i) {
        auto b = potions[0][i]; 
        if (b > 0) return i;
    }
    return -1;
}

int get_potion_ID(h3::H3Artifact& art) {
    if (is_art_alchemical[art.id] != 2) return - 1;
    auto nfo = PotionInfos;
    for (int i = 0; nfo[i].color; ++i) {
        if (art.id == nfo[i].art_id) {
            return i;
        }
    }
    return -1;
}

char* get_potion_description(h3::H3Artifact& art) {
    static char answer[4096]; // = "test alchemy";
    int id = get_potion_ID(art);
    char pow[16]; itoa(art.subtype, pow, 10);
    if (id < 0) return nullptr;

    strcpy(answer, PotionInfos[id].name);
    strcat(answer, "\n ---------------- \n");
    strcat(answer, "Power: "); strcat(answer, pow);
    strcat(answer, "\n ---------------- \n");
    char* desc = PotionInfos[id].desc;
    if (desc) strcat(answer, desc);

    return answer;
}

char* get_reagent_description(h3::H3Artifact& art) {
    static char answer[4096]; // = "test alchemy";
    int id = get_reagent_ID(art);
    char pow[16]; itoa(art.subtype, pow, 10);
    if (id < 0) return nullptr;

    strcpy(answer, ReagentInfos[id].name);
    strcat(answer, "\n ---------------- \n");
    strcat(answer, "Power: "); strcat(answer, pow);
    strcat(answer, "\n ---------------- \n");

    return answer;
}

char* get_custom_description(h3::H3Artifact& art) {
    switch (is_art_alchemical[art.id]) {
        case 0: return nullptr;
        case 1: return get_reagent_description(art);
        case 2: return get_potion_description(art);

        default: return nullptr;
    }
}

int get_complexity(int potion_ID) {
    PotionInfo& pot = PotionInfos[potion_ID];
    if (pot.color[0] == 0) return -1;
    if (pot.color[1] == 0) {
        switch (pot.color[0])
        {
        case 'E':
        case 'B':
        case 'R':
        case 'Y':
            return 0;
        case 'X':
        case 'P':
        case 'G':
        case 'O':
            return 1;
        default:
            break;
        }
    }
    if (pot.color[2] == 0) return 2;

    return 9;
}

void alter_mixing_table(int src1, int src2, int result) {
    if (src1 < 0 || src2 < 0) return;
    if (src1 >= MAX_POTIONS || src2 >= MAX_POTIONS) return;
    if (result < 0 || result >= MAX_POTIONS) return;
    mixing_table[src1][src2] = mixing_table[src2][src1] = result;
}



bool AlchemyBag::AddArtifact(h3::H3Artifact& art)
{
    if (is_art_alchemical[art.id] == 0)
        return false;

    if(art.subtype >= 0 && art.subtype < LIMIT_POWER)
    {
        if (is_art_alchemical[art.id] == 2)
        {
            int pot_id = get_potion_ID(art);
            if (pot_id >= 0) {
                ++(this->potions[pot_id][art.subtype]);
                return true;
            }
        }

        if (is_art_alchemical[art.id] == 1)
        {
            int rea_id = get_reagent_ID(art);
            if (rea_id >= 0) {
                ++(this->reagents[rea_id][art.subtype]);
                return true;
            }
        }
    }

    return false;
}

extern "C" __declspec(dllexport) bool put_in_alchemy_bag(h3::H3Hero * her, h3::H3Artifact * art) {
    if (!her) return false;
    return BagPerHero[her->id].AddArtifact(*art);
}


struct ReagentDlg : public h3::H3Dlg {
    h3::H3Hero* myHero; static constexpr int Empty = 145; h3::H3Artifact answer;
    ReagentDlg(h3::H3Hero* her, char _color_) :H3Dlg(780, 576), myHero(her), answer(-1, 0) {
        int i = 0; int j = 0; int m = 0; int n = 0;
        for (int z = 0; z < 9000; ++z) {
            if (j >= 12) break;
            if (m >= MAX_REAGENTS) break;

            auto count = BagPerHero[her->id].reagents[m][n];
            char color = ReagentInfos[m].color[0];
            if (count && (color == _color_)) {
                char cnt[8]; itoa(count, cnt, 10);
                CreateDef(30 + i * 40, 30 + j * 40, 10000 + z, "artifact.def", ReagentInfos[m].art_id);
                CreateText(30 + i * 40, 54 + j * 40, 32, 16, cnt, "medfont.fnt", h3::eTextColor::GREEN, 20000 + z);

                ++i; if (i >= 18) { i = 0; ++j; }
            }

            ++n; if (n >= LIMIT_POWER) { n = 0; ++m; }
        }

    }
    BOOL DialogProc(h3::H3Msg& msg) override;
};

BOOL ReagentDlg::DialogProc(h3::H3Msg& msg)
{

    if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
        auto dlg = this->GetH3DlgItem(msg.itemId);
        int pos = msg.itemId - 10000;

        if(pos>=0 && pos <= MAX_REAGENTS * LIMIT_POWER)
        {
            int art_id = ReagentInfos[pos / LIMIT_POWER].art_id;
            int power = pos % LIMIT_POWER;

            // int art_id = PotionInfos[potion_id].art_id;
            h3::H3Artifact art = { art_id, power };

            answer = art; this->Stop();
        }
    }
    return 0;
}

struct AlchemyDlg : public h3::H3Dlg {
    h3::H3Hero* myHero; static constexpr int Empty = 145; 
    const int isBattle; h3::H3Artifact answer;
    AlchemyDlg(h3::H3Hero* her, int _isBattle_) 
        :H3Dlg(480, 480), myHero(her), isBattle(_isBattle_), answer(-1,0) {

            int i = 0; int j = 0; 
            for (int z = 0; z < MAX_POTIONS; ++z) {
                auto count = BagPerHero[her->id].Calc_Potions_of_type(z);
                if (count) {
                    char cnt[8]; itoa(count, cnt, 10);
                    CreateDef(76 + i * 40, 76 + j * 40, 3000 + z, "artifact.def", PotionInfos[z].art_id);
                    CreateText(76 + i * 40, 76 + j * 40, 32, 16, cnt, "medfont.fnt", h3::eTextColor::GREEN, 6000 + z);
                }
                else {
                    CreateDef(76 + i * 40, 76 + j * 40, 9000 + z, "artifact.def", Empty);
                }
                ++i; if (i >= 8) { i = 0; ++j; }
            }
        }
    BOOL DialogProc(h3::H3Msg& msg) override;
};

struct AlchemyDlgDetail : public h3::H3Dlg {
    h3::H3Hero* myHero; static constexpr int Empty = 145; 
    const int isBattle; const int potion_id;
    h3::H3Artifact answer;
    AlchemyDlgDetail(h3::H3Hero* her, int _isBattle_, int _potion_id_)
        :H3Dlg(512, 512), myHero(her), isBattle(_isBattle_), potion_id(_potion_id_), answer(-1,0) {

        int i = 0; int j = 0; 
        int art_id = PotionInfos[potion_id].art_id;
        for (int z = 0; z < LIMIT_POWER; ++z) {
            auto count = BagPerHero[her->id].potions[potion_id][z];

            if (count) {
                char cnt[8]; itoa(count, cnt, 10);
                CreateDef(56 + i * 40, 56 + j * 40, 3000 + z, "artifact.def", art_id);
                CreateText(56 + i * 40, 56 + j * 40, 32, 16, cnt, "medfont.fnt", h3::eTextColor::GREEN, 6000 + z);
            }
            else {
                CreateDef(56 + i * 40, 56 + j * 40, 9000 + z, "artifact.def", Empty);
            }

            ++i; if (i >= 10) { i = 0; ++j; }
        }
    }
    BOOL DialogProc(h3::H3Msg& msg) override;
};

BOOL AlchemyDlgDetail::DialogProc(h3::H3Msg& msg) {

    if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
        auto dlg = this->GetH3DlgItem(msg.itemId);
        int pos = msg.itemId - 3000;

        int art_id = PotionInfos[potion_id].art_id;
        h3::H3Artifact art = { art_id, pos };

        answer = art; this->Stop();
    }

    if (msg.subtype == h3::eMsgSubtype::RBUTTON_DOWN) {
        if (msg.itemId >= 3000 && msg.itemId < 4000) {
            auto dlg = this->GetH3DlgItem(msg.itemId);
            int pos = msg.itemId - 3000;

            int art_id = PotionInfos[potion_id].art_id;
            h3::H3Artifact art = {art_id, pos};
            z_whisper(get_potion_description(art));
        }
    }

    return 0;
}

BOOL AlchemyDlg::DialogProc(h3::H3Msg& msg)
{
    if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
        if (msg.itemId >= 3000 && msg.itemId < 4000) {
            int pos = msg.itemId - 3000;
            if(isBattle<0 || (PotionInfos[pos].flags & 
                (isBattle ? PotionInfo::IN_BATTLE : PotionInfo::IN_ADVENTURE)))
            {
                AlchemyDlgDetail wnd(myHero, isBattle, pos);

                wnd.CreateOKButton();
                wnd.Start();

                if (wnd.answer != h3::H3Artifact{ -1,0 }) {
                    this->answer = wnd.answer;
                    Stop();
                }
            }
            else {
                z_shout(isBattle ?  "this potion type is useless in battle"
                                  : "this potion type is useless in adventure");
            }
        }
    }


    if (msg.subtype == h3::eMsgSubtype::RBUTTON_DOWN) {
        if (msg.itemId >= 3000 && msg.itemId < 4000) {
            auto dlg = this->GetH3DlgItem(msg.itemId);
            int pos = msg.itemId - 3000;
            // auto& art = myHero->backpackArtifacts[pos];
            // int id = get_potion_ID(art);
            z_whisper(PotionInfos[pos].name);
        }
    }

    return 0;
}

extern "C" __declspec(dllexport) void CallAlchemyWindow(h3::H3Hero * her, bool isBattle) {
    AlchemyDlg wnd(her, isBattle);

    wnd.CreateOKButton();
    wnd.Start();
    auto& art = wnd.answer;
    long potion_ID = get_potion_ID(art);
    if (potion_ID < 0) return;
    auto& nfo = PotionInfos[potion_ID];

    if (isBattle && ChangeCreatureTable_ptr && nfo.Amethyst_Modifier) {
        auto batman = h3::H3CombatManager::Get();
        int stack_ID = (batman->currentMonSide ? 21 : 0) + batman->currentMonIndex;
        ChangeCreatureTable_ptr(-1 - stack_ID, nfo.Amethyst_Modifier);
    }

    if (isBattle && nfo.unit_script) {
        auto batman = h3::H3CombatManager::Get();
        int stack_ID = (batman->currentMonSide ? 21 : 0) + batman->currentMonIndex;
        Era::v[2] = stack_ID; Era::v[3] = art.subtype;
        ExecErmString(nfo.unit_script); 
    }

    if (nfo.hero_script) {
        Era::v[2] = her->id; Era::v[3] = art.subtype;
        ExecErmString(nfo.hero_script);
    }

    BagPerHero[her->id].RemoveArtifact(art);
}

struct BottleShop : public h3::H3Dlg {
    h3::H3Hero* myHero; static constexpr int Empty = 145;
    BottleShop(h3::H3Hero* her) :H3Dlg(320, 320), myHero(her) {
        int frame = PotionInfos[0].art_id;
        int i = 0; int j = 0;

        for (int z = 5; z < 99; z+=5) {

            char txt[8]; itoa(z, txt, 10);
            CreateDef(36 + i * 48, 36 + j * 48, 3000 + z, "artifact.def", frame);
            CreateText(36 + i * 48, 60 + j * 48, 32,16, txt, "medfont.fnt", h3::eTextColor::GOLD, -1);

            ++i; if (i >= 5) { i = 0; ++j; }
        }
        CreateDef(36 + i * 48, 36 + j * 48, 3000 + 99, "artifact.def", frame);
        CreateText(36 + i * 48, 60 + j * 48, 32, 16, "99", "medfont.fnt", h3::eTextColor::GOLD, -1);

    }
    BOOL DialogProc(h3::H3Msg& msg) override;
};
BOOL BottleShop::DialogProc(h3::H3Msg& msg) {
    if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
        if (msg.itemId >= 3000 && msg.itemId < 4000) {
            int pos = msg.itemId - 3000;
            int price = pos * 50;
            int& gold = h3::H3Main::Get()->players[myHero->owner].playerResources.gold;
            if (gold < price) z_shout("Not Enough Gold");
            else if (z_MsgBox("Do You Wish to Buy the Bottle",2,-1,-1,8,PotionInfos->art_id, 6, price, -1, 0, -1, 0)) {
                h3::H3Artifact art{PotionInfos->art_id, pos};
                gold -= price; put_in_alchemy_bag(myHero, &art);
            }
        }
    }
    return 0;
}

struct PrepareAlchemy : public h3::H3Dlg {
    h3::H3Hero* myHero; static constexpr int Empty = 144;
    h3::H3Artifact left, right, result;
    PrepareAlchemy(h3::H3Hero* her) :H3Dlg(256, 256), myHero(her), 
        left(Empty, -1), right(Empty, -1), result(Empty, -1)
    {
        for (int i = 1; i < 5; ++i) {
            int frame = PotionInfos[i].art_id;
            CreateDef(36 + (i-1) * 48, 36 /* + j * 48 */, 3000 + i, "artifact.def", frame);
        }

        CreateDef(36 + 0 * 48, 60 + 1 * 48, 4000, "artifact.def", Empty); // left
        CreateDef(36 + 1 * 48, 60 + 1 * 48, 4001, "artifact.def", Empty); // right
        CreateDef(36 + 3 * 48, 60 + 1 * 48, 5000, "artifact.def", Empty); // result

        CreateDef(36 + 3 * 48, 36  + 3 * 48 , 9000, "artifact.def", PotionInfos->art_id);
    }

    BOOL DialogProc(h3::H3Msg& msg) override;
};

BOOL PrepareAlchemy::DialogProc(h3::H3Msg& msg)
{
    if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
        if (msg.itemId > 3000 && msg.itemId < 4000) {
            char color = PotionInfos[msg.itemId - 3000].color[0];
            ReagentDlg wnd(myHero, color);

            wnd.CreateOKButton();
            wnd.Start();
            
            auto& reagent = wnd.answer; auto& bag = BagPerHero[myHero->id];
            h3::H3Artifact bottle = {PotionInfos->art_id, 
                bag.get_fitting_bottle(reagent.subtype)};
            if (bag.RemoveArtifact(reagent, bottle)) {
                h3::H3Artifact potion = {-1,0};
                potion.subtype = reagent.subtype + 0;
                int rea_id = get_reagent_ID(reagent);
                char color = ReagentInfos[rea_id].color[0];

                for (int i = 1; i < 5; ++i)
                    if (PotionInfos[i].color[0] == color)
                        potion.id = PotionInfos[i].art_id;

                bag.AddArtifact(potion);
            }
            else {
                if (bottle.subtype < 0) z_shout("You do not have a fitting bottle.");
            }
        }
        else if (msg.itemId == 4000 || msg.itemId == 4001) {
            int pos = msg.itemId - 4000;
            AlchemyDlg wnd(myHero, -1);
            
            wnd.CreateOKButton();
            wnd.Start();

            if (wnd.answer.id >= 0)
            {
                // pos ? right : left = wnd.answer;
                if (pos) right = wnd.answer;
                else left = wnd.answer;
                GetDef(msg.itemId)->SetFrame(wnd.answer.id);

                if (left.id >= 0 && right.id >= 0 && left.id != right.id)
                {
                    int left_pid = get_potion_ID(left); 
                    int right_pid = get_potion_ID(right);
                    int result_pid = mixing_table[left_pid] [right_pid];
                    if (result_pid >= 0) {
                        result.id = PotionInfos[result_pid].art_id;
                        result.subtype = (left.subtype < right.subtype) ?
                            left.subtype : right.subtype;
                        GetDef(5000)->SetFrame(result.id);
                    }
                    else if (PotionInfos[left_pid].color[0] == 'X'
                        || PotionInfos[right_pid].color[0] == 'X'
                        ) {
                        bool is_right_catalyst = (PotionInfos[right_pid].color[0] == 'X');
                        int catalyst_power = is_right_catalyst ? right.subtype : left.subtype;
                        int other_power = is_right_catalyst ? left.subtype : right.subtype;
                        if (other_power >= catalyst_power) goto failed;

                        result.id = is_right_catalyst ? left.id : right.id;
                        result.subtype = catalyst_power;
                        GetDef(5000)->SetFrame(result.id);
                    }
                    else {
                        failed:
                        result = {-1,0};
                        GetDef(5000)->SetFrame(Empty);
                    }

                    bool miss_left = !BagPerHero[myHero->id].HasArtifact(left);
                    bool miss_right = !BagPerHero[myHero->id].HasArtifact(right);

                    if (miss_left) {
                        left = { -1,0 };
                        GetDef(4000)->SetFrame(Empty);
                    }

                    if (miss_right) {
                        right = { -1,0 };
                        GetDef(4001)->SetFrame(Empty);
                    }

                    if( miss_left || miss_right) {
                        result = { -1,0 };
                        GetDef(5000)->SetFrame(Empty);
                    }
                }
                Redraw();
            }

        }
        else if (msg.itemId == 5000) {
            if (BagPerHero[myHero->id].RemoveArtifact(left, right)) {
                BagPerHero[myHero->id].AddArtifact(result);

                bool miss_left = !BagPerHero[myHero->id].HasArtifact(left);
                bool miss_right = !BagPerHero[myHero->id].HasArtifact(right);

                if (miss_left) {
                    left = { -1,0 };
                    GetDef(4000)->SetFrame(Empty);
                }

                if (miss_right) {
                    right = { -1,0 };
                    GetDef(4001)->SetFrame(Empty);
                }

                if (miss_left || miss_right) {
                    result = { -1,0 };
                    GetDef(5000)->SetFrame(Empty);
                }

            }
            else {
                result = { -1,0 };
                GetDef(5000)->SetFrame(Empty);
                Redraw();
            }

            Redraw();
        }
        else if (msg.itemId == 9000) {
            auto GAME = h3::H3Game::Get();
            h3::H3Town *in_town = nullptr;
            auto h = myHero;
            for (auto &t : GAME->towns) {
                if (t.x == h->x && t.y == h->y && t.z == h->z)
                {
                    in_town = &t; break;
                }
            }
            if (in_town) {
                BottleShop wnd(h);

                wnd.CreateOKButton();
                wnd.Start();
            }
            else z_shout("You can buy empty bottles only in towns.");
        }
    }
    return 0;
}

extern "C" __declspec(dllexport) void CallPrepareAlchemy(h3::H3Hero * her, bool isBattle) {
    PrepareAlchemy wnd(her);

    wnd.CreateOKButton();
    wnd.Start();
}

char* read_text_file(char* fname) {
    FILE* fdesc; char* buf;
    if (fdesc = fopen(fname, "r"))
    {
        //----------
        fseek(fdesc, 0, SEEK_END);
        int fdesc_size = ftell(fdesc);
        rewind(fdesc);
        //----------
        buf = (char*)malloc(fdesc_size + 1);
        fread(buf, 1, fdesc_size, fdesc);
        buf[fdesc_size] = 0;
        fclose(fdesc);

        return buf;
        //potion.Amethyst_Modifier = buf;
        //buf = nullptr;
    }
    else return nullptr;
}

void __stdcall InitAlchemy(Era::TEvent* e) {
    memset(mixing_table, -1, sizeof(mixing_table));
    auto alt = alter_mixing_table; auto nfo = PotionInfos;
    alt(1, 2, 5); alt(1, 3, 6); alt(2, 3, 7);

    for (int it = 0; it < MAX_REAGENTS; ++it) {
        if (!*ReagentInfos[it].color) break;
        auto art_id = ReagentInfos[it].art_id;
        is_art_alchemical[art_id] = 1;
        P_Artifacts[art_id].name = ReagentInfos[it].name;
    }

    for (int it = 0; it < MAX_POTIONS; ++it) {
        if (get_complexity(it) < 0) break;
        auto art_id = PotionInfos[it].art_id;
        is_art_alchemical[art_id] = 2;
        P_Artifacts[art_id].name = PotionInfos[it].name;
        if (get_complexity(it) < 2) continue;
        for (int j = 0; j < it; ++j)
            for (int k = 0; k < it; ++k) {
                char test1[32] = {};
                sprintf_s(test1, "%s%s", nfo[j].color, nfo[k].color);
                //strcpy(nfo[j].color, test1); 
                //strcat(test1, nfo[k].color);

                char test2[32] = {};
                sprintf_s(test2, "%s%s", nfo[k].color, nfo[j].color);
                //strcpy(nfo[k].color, test2); 
                //strcat(test2, nfo[j].color);

                if (strcmp(test1, nfo[it].color) == 0
                    || strcmp(test2, nfo[it].color) == 0)
                    alt(j, k, it);
            }
    }

    HMODULE Amethyst_Link = nullptr;
    if (!Amethyst_Link) Amethyst_Link = GetModuleHandleA("amethyst2_4.dll");
    if (!Amethyst_Link) Amethyst_Link = GetModuleHandleA("amethyst2_5.dll");
    if (Amethyst_Link) {
        ChangeCreatureTable_ptr = (void(*)(int, char*)) GetProcAddress(Amethyst_Link, "ChangeCreatureTable");
        for (auto& potion : PotionInfos) {
            if (potion.color[0] == 0) break;

            char fname[256]; //char* buf, fname[256]; FILE* fdesc;
            sprintf(fname, "Data\\potions\\%s_amethyst.cfg", potion.color);
            potion.Amethyst_Modifier = read_text_file(fname);
            if (potion.Amethyst_Modifier) potion.flags |= PotionInfo::IN_BATTLE;
        }
    }
    for (auto& potion : PotionInfos) {
        char fname[256]; if (potion.color[0] == 0) break;

        sprintf(fname, "Data\\potions\\%s_hero_script.txt", potion.color);
        potion.hero_script = read_text_file(fname);
        if (potion.hero_script) potion.flags |= PotionInfo::IN_ADVENTURE;

        sprintf(fname, "Data\\potions\\%s_unit_script.txt", potion.color);
        potion.unit_script = read_text_file(fname);
        if (potion.unit_script) potion.flags |= PotionInfo::IN_BATTLE;

        sprintf(fname, "Data\\potions\\%s_properties.txt", potion.color);
        char* properties = read_text_file(fname);
        if (properties) {
            ParseInt(properties, "Flags=", &potion.flags);
            // ParseInt(properties, "Terrain=", &potion.terrain);
            char tmp[char_table_size];
            if(ParseStr(properties, "Name=\"", tmp)) strcpy(potion.name,tmp);
            free(properties);
        }
    }
    char fname[256] = "Data\\potions\\Reagents.txt";
    char* reagent_properties = read_text_file(fname);
    if (reagent_properties) {
        for (auto &rea: ReagentInfos) {
            if (rea.color[0] == 0) break;
            char property[64]; 
            sprintf(property, "%s_%d_terrain=", rea.color, rea.base_power);
            ParseInt(reagent_properties, property, &rea.terrain);
        }
        free(reagent_properties);
    }
}

void __stdcall StoreData(Era::TEvent* e)
{
    Era::WriteSavegameSection(sizeof(BagPerHero), &BagPerHero, "Z_AlchemyBag");
}
void __stdcall RestoreData(Era::TEvent* e)
{
    Era::ReadSavegameSection(sizeof(BagPerHero), &BagPerHero, "Z_AlchemyBag");
}

_LHF_(hook_004DB67F) {
    int art_id = c->eax;
    if (!is_art_alchemical[art_id])
        return EXEC_DEFAULT;

    h3::H3Artifact art = { c->eax, *(int*)(c->ecx+4)};

    c->ecx = (int) (void*) P_ArtifactSetup;
    c->ebx = *(int*)(c->ebp + 8);
    c->edx = *(int*)(c->ebp + 8 + 3);
    c->Push(c->edi);

    // char custom_description[512] = "test alchemy";

    // c->esi = (int) custom_description;
    c->esi = (int)get_custom_description(art);
    c->return_address = 0x004DB693;
    return SKIP_DEFAULT;
}


void __stdcall Clean(Era::TEvent* e) {
    for (auto& i : BagPerHero) i.clean();
}


Patcher* globalPatcher;
PatcherInstance* Z_Alchemy;
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH: {
        globalPatcher = GetPatcher();
        Z_Alchemy = globalPatcher->CreateInstance((char*)"Z_BackPack");
        Era::ConnectEra();

        Era::RegisterHandler(StoreData, "OnSavegameWrite");
        Era::RegisterHandler(RestoreData, "OnSavegameRead");
        Era::RegisterHandler(InitAlchemy, "OnAfterCreateWindow");

        Z_Alchemy->WriteLoHook(0x004DB67F, hook_004DB67F);

        Era::RegisterHandler(Clean, "OnAfterErmInstructions");

    } break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

