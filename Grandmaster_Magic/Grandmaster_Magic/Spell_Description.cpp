#include "patcher_x86_commented.hpp"
#include "extern.h"
extern int AppliedSpells_Efects[];
bool isAppliedSpell(int spell);
#include"_extended_spell_info_.h"

_LHF_(hook_0059BE11) {
	//if (c->ecx > 3) c->ecx = 3;
	//return EXEC_DEFAULT;

	// c->edx = (int) Z_Spells[*(int*)(c->ebp+0xC)].description[*(int*)(c->ebp-10)];
	c->edx = (int)Z_Spells[*(int*)(c->ebp + 0xC)].description[c->ecx];

	c->ecx = -1;
	c->return_address = 0x0059BE18;
	return NO_EXEC_DEFAULT;
}

char Mastery_short[][16] = {
	"Non", "Bas", "Adv", "Exp", "M", "GM", "UGM", "ZZZ"
};
char* Mastery_short_ptr[] = {
	Mastery_short[0],
	Mastery_short[1],
	Mastery_short[2],
	Mastery_short[3],
	Mastery_short[4],
	Mastery_short[5],
	Mastery_short[6],
	Mastery_short[7],
};


char level_text[][16] = { "ZERO lvl", "1st lvl", "2nd lvl", "3rd lvl", "4th lvl", "5th lvl", "6th lvl", "7th lvl", "8th lvl", "9th lvl" };
char* level_text_ptr[] = { level_text[0], level_text[1], level_text[2], level_text[3], level_text[4], level_text[5],
							level_text[6], level_text[7], level_text[8], level_text[9]};

void fix_spell_description(void) {
	Z_Grandmaster_Magic->WriteLoHook(0x0059BE11, hook_0059BE11);
	Z_Grandmaster_Magic->WriteDword(0x0059D105+3, (int)Mastery_short_ptr);


	Z_Grandmaster_Magic->WriteDword(0x0059D1B2 + 3, (long)level_text_ptr);
	Z_Grandmaster_Magic->WriteDword(0x0059D0DC + 3, (long)level_text_ptr);
	Z_Grandmaster_Magic->WriteDword(0x0059BF2A + 3, (long)level_text_ptr);
	// Z_Grandmaster_Magic->WriteDword(0x0059D0DC + 3, (long)level_text_ptr);
}