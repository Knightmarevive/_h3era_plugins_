#include "patcher_x86_commented.hpp"
#include "extern.h"


int death_ripple_effect[6] = { 40,80,120,160,200,240 };
int destroy_undead_effect[6] = { 40,80,120,160,200,240 };
int Armageddon_effect[6] = { 80,160,240,320,400,480 };


_LHF_(z_hook_0x005A50C8) {
	if (c->eax) return EXEC_DEFAULT;

	c->return_address = 0x005A50E7;
	return NO_EXEC_DEFAULT;
}

_LHF_(z_hook_005A5237) {
	c->ecx = *(int*)(c->eax + 0x124);
	c->edx = *(int*)(c->eax - 0x4);
	
	if (c->ecx) c->return_address = 0x005A5240;
	else		c->return_address = 0x005A5258;

	return NO_EXEC_DEFAULT;
}

void fix_MassDamageSpells(void) {
	Z_Grandmaster_Magic->WriteDword(0x005A1043 + 3, int(Z_Spells[24].base_value));//int(death_ripple_effect));
	Z_Grandmaster_Magic->WriteWord(0x005A1043 + 1, 0xb50c);

	Z_Grandmaster_Magic->WriteDword(0x005A1105 + 3, int(Z_Spells[24].base_value));//int(death_ripple_effect));
	Z_Grandmaster_Magic->WriteWord(0x005A1105 + 1, 0xb534);
	if (set_max_magic_proficiency) set_max_magic_proficiency(24, 5);


	Z_Grandmaster_Magic->WriteDword(0x005A1240 + 3, int(Z_Spells[25].base_value));//int(destroy_undead_effect));
	Z_Grandmaster_Magic->WriteWord(0x005A1240 + 1, 0xb50c);

	Z_Grandmaster_Magic->WriteDword(0x005A1329 + 3, int(Z_Spells[25].base_value));//int(destroy_undead_effect));
	Z_Grandmaster_Magic->WriteWord(0x005A1329 + 1, 0xb534);
	if (set_max_magic_proficiency) set_max_magic_proficiency(25, 5);


	Z_Grandmaster_Magic->WriteDword(0x005A4F91 + 3, int(Z_Spells[26].base_value));//int(Armageddon_effect));
	Z_Grandmaster_Magic->WriteWord(0x005A4F91 + 1, 0x9d0c);

	Z_Grandmaster_Magic->WriteDword(0x005A550C + 3, int(Z_Spells[26].base_value));//int(Armageddon_effect));
	Z_Grandmaster_Magic->WriteWord(0x005A550C + 1, 0x8d04);

	Z_Grandmaster_Magic->WriteLoHook(0x005A50C8, z_hook_0x005A50C8);
	Z_Grandmaster_Magic->WriteLoHook(0x005A5237, z_hook_005A5237);

	if (set_max_magic_proficiency) set_max_magic_proficiency(26, 5);
}