#pragma once

constexpr DWORD combo_artifact_bitfield_dwords_count = 30;
/*
// Combo art
struct _CArtSetup_ {
	int   Index;
	//Dword ArtNums[5];
	//unsigned ArtNums[30];
	h3::H3Bitfield ArtNums[combo_artifact_bitfield_dwords_count];
};

constexpr size_t z_combo_size = sizeof(_CArtSetup_);
constexpr DWORD new_combo_size = 124;

inline bool Z_ComboArtInfo(int comboArtIndex, int iArt) {
	_CArtSetup_* combos =(_CArtSetup_*) *(int*) 0x660B6C;
	return combos[comboArtIndex].ArtNums->GetState(iArt);
}
constexpr DWORD ARTIFACTS_NUM(combo_artifact_bitfield_dwords_count << 5);
inline HMODULE Emerald_Linkage(void) {
	return  GetModuleHandleA("emerald3_3.era");
}
*/


extern HMODULE Emerald_Link;
extern void (*Combo_Add_Spells)(void*, int);

/*
struct SpellBitset {
	h3::H3Bitfield data[5];
	inline const char test(int spell) {
		return data->GetState(spell);
	}
	inline const char operator[](int spell) {
		return data->GetState(spell);
	}
	inline const int Size() {
		if (true)
			return 140;
		else return 70;
	}
	inline const int size() {
		if (true)
			return 140;
		else return 70;
	}
	inline void set(INT32 position, BOOL state) {
		data->SetState(position, state);
	}
	inline void set(INT32 position) {
		data->Set(position);
	}

};

struct _SpellBitset70_: public SpellBitset {
	inline const int Size() {
		return 70;
	}
	inline const int size() {
		return 70;
	}
};
*/