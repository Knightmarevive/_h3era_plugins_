#pragma once

namespace Era {
	typedef void(__stdcall* TExecErmCmd) (const char* CmdStr);
	extern TExecErmCmd           ExecErmCmd;
	void ConnectEra();
	extern int* v;
}