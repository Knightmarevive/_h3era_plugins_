#include "patcher_x86_commented.hpp"
#include "extern.h"

long DimensionDoorRepeats[6] = {2,3,4,5,6,7};
_LHF_(hook_0041D2C2) {
	if (c->ecx < /* DimensionDoorRepeats[c->eax]*/
		Z_Spells[8].special[c->eax])
			c->return_address = 0x0041D323;
	else
			c->return_address = 0x0041D2C8;

	return NO_EXEC_DEFAULT;
}

long TownPortalMovementCost[6] = {350,300,250,200,150,100};
_LHF_(hook_0041D552) {
	c->edx = *(int*)(c->esi + 0x4d);

	// c->eax = TownPortalMovementCost[c->eax];
	c->eax = Z_Spells[9].special[c->eax];

	c->return_address = 0x0041D559;
	return NO_EXEC_DEFAULT;
}

int dword_006778AC[] = {140,140,120,100,90,80,70,60,50};
void fix_Adventure_Spells(void) {
	Z_Grandmaster_Magic->WriteLoHook(0x0041D2C2, hook_0041D2C2);
	if(set_max_magic_proficiency) set_max_magic_proficiency(8, 5);

	Z_Grandmaster_Magic->WriteLoHook(0x0041D552, hook_0041D552);
	if (set_max_magic_proficiency) set_max_magic_proficiency(9, 5);

	Z_Grandmaster_Magic->WriteDword(0x004B1539 + 3, (long)dword_006778AC);
	Z_Grandmaster_Magic->WriteDword(0x004B1555 + 3, (long)dword_006778AC);
	Z_Grandmaster_Magic->WriteDword(0x004B1570 + 3, (long)dword_006778AC);
}