#include "patcher_x86_commented.hpp"
// #include "__include/era.h"
#include "__include/heroes.h"
#include "lib\H3API.hpp"

extern PatcherInstance* Z_Grandmaster_Magic;
_LHF_(hook_0043A3B8) {
	int fun_address = *(int*)(c->ebp - 0x14);
	if (fun_address) return EXEC_DEFAULT;
	c->esi = c->eax = 0; c->Pop();
	c->return_address = 0x0043A3BD;
	return NO_EXEC_DEFAULT;
}

void GM_Magic_Fixes() {
	Z_Grandmaster_Magic->WriteLoHook(0x0043A3B8, hook_0043A3B8);
}