#include "patcher_x86_commented.hpp"
#include "extern.h"
#include "__include/heroes.h"

extern char (*_get_hero_SS_)(HERO*, int);

#include "Hero_Specialties.h"
H3HeroSpecialty& P_HeroSpecialty(int id)
{
	
	return (reinterpret_cast<H3HeroSpecialty*>
		(*(int*)0x00679C80))[id];

	return (*reinterpret_cast<H3HeroSpecialty**>
		(*(int*)(0x4B8AF1 + 1)))[id];
}

int SS_Astra = 5, SS_Life = 9, SS_Dark = 12, SS_Sorcery = 25, SS_Aether = 30;
float sorcery_mult =		0.05 * 2.0;
double default_spell_mult = 0.05 * 4.0;
double astral_mult =		0.05 * 3.0;
double life_mult =			0.05 * 3.0;
double dark_mult =			0.05 * 2.5;
/*
_LHF_(hook_004E5B49) {
	int SS_Specialty = *(int*)(c->eax + 4);
	if (SS_Specialty == SS_Sorcery) {
		c->return_address = 0x004E5B4F;
		return NO_EXEC_DEFAULT;
	}
	short Hero_Level = *(short*)(c->esi + 0x55);
	short Spell = *(short*)(c->ebp + 0x8);
	if (_get_hero_SS_) {
		if (SS_Specialty == SS_Astra && (
			Spell == SPELL_LIGHTBOLT ||
			Spell == SPELL_CHAINLIGHT ||
			Spell == SPELL_METEORSHOWER ||
			Spell == SPELL_ARMAGEDDON
			)) {
			float ret = Hero_Level * astral_mult + 1.0;
			*(float*)(c->ebp - 0x4) = ret;
		}
		if (SS_Specialty == SS_Life && Spell == SPELL_DESTROYUNDEAD) {
			float ret = Hero_Level * life_mult + 1.0;
			*(float*)(c->ebp - 0x4) = ret;

		}
		if (SS_Specialty == SS_Dark && Spell == SPELL_DEATHWAVE) {
			float ret = Hero_Level * dark_mult + 1.0;
			*(float*)(c->ebp - 0x4) = ret;

		}
	}
	c->return_address = 0x004E5B71;
	return NO_EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_004E5B71) {
	float ret = *(float*)(c->ebp - 0x4);
	short Spell = *(short*)(c->ebp + 0x8);
	HERO* hero = (HERO*) c->esi;
	
	if (_get_hero_SS_) {
		// char AstroLevel = *(char*)(c->esi + 0xc9 +SS_Astra);
		char AstroLevel = _get_hero_SS_(hero, SS_Astra);
		if (Spell == SPELL_LIGHTBOLT ||
			Spell == SPELL_CHAINLIGHT ||
			Spell == SPELL_METEORSHOWER ||
			Spell == SPELL_ARMAGEDDON
			) {
			// ret *= 1.0 + (AstroLevel * 0.10);
			ret *= 1.0 + get_SS_table_zf(SS_Astra, AstroLevel , 0);
		}

		//char LifeLevel = *(char*)(c->esi + 0xc9 + SS_Life);
		char LifeLevel = _get_hero_SS_(hero, SS_Life);
		if (Spell == SPELL_DESTROYUNDEAD) {
			ret *= 1.0 + (LifeLevel * 0.20);
		}


		//char DarkLevel = *(char*)(c->esi + 0xc9 + SS_Dark);
		char DarkLevel = _get_hero_SS_(hero, SS_Dark);
		if (Spell == SPELL_DEATHWAVE) {
			ret *= 1.0 + (DarkLevel * 0.15);
		}
	}
	*(float*)(c->ebp - 0x4) = ret;
	return EXEC_DEFAULT;
}
*/

long long __stdcall hook_Hero_GetSorceryEffect(HiHook* h, HERO* hero, int spell, signed int damage, void* stack) {
	long long ret = CALL_4(int, __thiscall, h->GetDefaultFunc(), hero, spell, damage, stack);
	auto SS_Specialty = P_HeroSpecialty(hero->Number);
	if (_get_hero_SS_) {
		if (SS_Specialty.type == H3HeroSpecialty::ST_skill &&
			SS_Specialty.bonusID == SS_Astra && (
			spell == SPELL_LIGHTBOLT ||
			spell == SPELL_CHAINLIGHT ||
			spell == SPELL_METEORSHOWER ||
			spell == SPELL_ARMAGEDDON
			)) {
			// ret *= 1.0 + (AstroLevel * 0.10);
			ret *= 1.0 + get_SS_table_zf(SS_Astra, _get_hero_SS_(hero,SS_Astra), 0);
		}
		if (SS_Specialty.type == H3HeroSpecialty::ST_skill &&
			SS_Specialty.bonusID == SS_Life && spell == SPELL_DESTROYUNDEAD) {
			// ret *= hero->ExpLevel * life_mult + 1.0;
			ret *= 1.0 + get_SS_table_zf(SS_Life, _get_hero_SS_(hero, SS_Life), 0);

		}
		if (SS_Specialty.type == H3HeroSpecialty::ST_skill &&
			SS_Specialty.bonusID == SS_Dark && spell == SPELL_DEATHWAVE) {
			// ret *= hero->ExpLevel * dark_mult + 1.0;

			ret *= 1.0 + get_SS_table_zf(SS_Dark, _get_hero_SS_(hero, SS_Dark), 0);
		}


		char AstroLevel = _get_hero_SS_(hero, SS_Astra);
		if (spell == SPELL_LIGHTBOLT ||
			spell == SPELL_CHAINLIGHT ||
			spell == SPELL_METEORSHOWER ||
			spell == SPELL_ARMAGEDDON
			) {
			// ret *= 1.0 + (AstroLevel * 0.10);
			ret *= 1.0 + get_SS_table_zf(SS_Astra, AstroLevel,0);
		}

		//char LifeLevel = *(char*)(c->esi + 0xc9 + SS_Life);
		char LifeLevel = _get_hero_SS_(hero, SS_Life);
		if (spell == SPELL_DESTROYUNDEAD) {
			//ret *= 1.0 + (LifeLevel * 0.20);

			ret *= 1.0 + get_SS_table_zf(SS_Life, LifeLevel, 0);
		}


		//char DarkLevel = *(char*)(c->esi + 0xc9 + SS_Dark);
		char DarkLevel = _get_hero_SS_(hero, SS_Dark);
		if (spell == SPELL_DEATHWAVE) {
			// ret *= 1.0 + (DarkLevel * 0.15);
			ret *= 1.0 + get_SS_table_zf(SS_Dark, DarkLevel, 0);
		}

	}
	return ret;
}


int __stdcall hook_HeroSpellSpecialityEffect(HiHook* h, HERO* hero, int spell, int monLevel, signed int effect) {
	int bonus = CALL_4(int, __thiscall, h->GetDefaultFunc(),hero,spell,monLevel,effect);
	auto SS_Specialty = P_HeroSpecialty(hero->Number); double z_bonus = bonus;
	
	if (_get_hero_SS_) {
		char LifeLevel = _get_hero_SS_(hero, SS_Life);// (hero->SSkill[SS_Life]);
		char DarkLevel = _get_hero_SS_(hero, SS_Dark);// (hero->SSkill[SS_Dark]);
		long HeroLevel = hero->ExpLevel;

		if(SS_Specialty.type == H3HeroSpecialty::ST_skill){
			
			if (spell == SPELL_CURE || spell == SPELL_RESSURRECT || spell == SPELL_SACRIFICE)
				if (SS_Specialty.bonusID == SS_Life) z_bonus *= 1.0 + (HeroLevel * life_mult);
				//return ceil((effect + bonus) * (LifeLevel * 0.25));

			if (spell == SPELL_ANIMATEDEAD && SS_Specialty.bonusID == SS_Dark)
				z_bonus *= 1.0 + (HeroLevel * dark_mult);
				//return ceil((effect + bonus) * (DarkLevel * 0.25));
		}
		if (spell == SPELL_CURE || spell == SPELL_RESSURRECT || spell == SPELL_SACRIFICE)
			z_bonus *= 1.0 + get_SS_table_zf(SS_Life, LifeLevel, 0);
			// return ceil((effect + bonus) * (LifeLevel * 0.25));

		if (spell == SPELL_ANIMATEDEAD)
			z_bonus *= 1.0 + get_SS_table_zf(SS_Dark, DarkLevel, 0);
			// return ceil((effect + bonus) * (DarkLevel * 0.25));
	}
	return (int)z_bonus;
}

bool fix_magic_specialties_applied = false;
void fix_magic_specialties(void) {
	if (fix_magic_specialties_applied) return;

	Z_Grandmaster_Magic->WriteDword(0x004E5B5F + 2, (int)&sorcery_mult);
	Z_Grandmaster_Magic->WriteDword(0x004E631D + 2, (int)&default_spell_mult);
	//Z_Grandmaster_Magic->WriteLoHook(0x004E5B49, hook_004E5B49);
	//Z_Grandmaster_Magic->WriteLoHook(0x004E5B71, hook_004E5B71);
	Z_Grandmaster_Magic->WriteHiHook(0x004E6260, SPLICE_, EXTENDED_, THISCALL_, hook_HeroSpellSpecialityEffect);
	Z_Grandmaster_Magic->WriteHiHook(0x004E59D0, SPLICE_, EXTENDED_, THISCALL_, hook_Hero_GetSorceryEffect);

	fix_magic_specialties_applied = true;
}