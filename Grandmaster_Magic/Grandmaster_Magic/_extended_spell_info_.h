#pragma once
#include<Windows.h>
#define char_table_size 4096
struct _extended_spell_info_ {
	UINT32 mana_cost[8];
	UINT32 base_value[8];
	UINT32 chance_to_get[36];
	char description[8][char_table_size];
	UINT32 special[64];
	UINT32 zflag;
	UINT32 target;
	static void clean();
	static void read();
	static void hook();
	char command[512];
	enum {
		is_disabled = 0x80000000,
		is_summon = 1,
		is_buff = 2,
		is_debuff = 4,
		is_timed = 6,
		has_dependency = 8,
		is_combo = 16,
		is_runic = 32,
		has_amethyst_command = 64,
		has_ERM_command = 128,
		has_damage = 256,
		has_flight_animation = 512,
		is_healing = 1024,
		is_ressurrecting = 2048,
		does_overflow = 4096,
		dispel_like_blind = 8192,
	};

	enum class target {
		CasterOnly = 1,
		DragonOnly = 2,
		nonDragonOnly = 4,
		livingOnly = 8,
		undeadOnly = 16,
		constructOnly =32,

	};
	/*
	inline INT32 GetBaseEffect(INT32 level, INT32 spellPower)
	{
		return base_value[level] + spellPower * sp_effect;
	}
	*/
};

extern _extended_spell_info_ Z_Spells[256];