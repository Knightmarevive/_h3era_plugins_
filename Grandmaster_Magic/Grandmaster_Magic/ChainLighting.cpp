#include "patcher_x86_commented.hpp"
#include "extern.h"

int chain_lighting_parts[6] = { 4,4,5,5,6,7 };
int chain_lighting_effect[6] = {90,180,270,360,450,540};
_LHF_(hook_005A669D) {
    c->ebx = 0;
    long old_edi = c->edi;
    if (c->edi > 7) c->edi = 7;
    c->ecx = c->eax + 0x88 * 19 + 0x30;
    c->eax = c->eax + c->edi * 4 + 0x88 * 19 + 0x34;
    c->edi = old_edi; c->return_address = 0x005A66AC;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_005A669D_) {
    if (c->edi > 7) c->edi = 7;
    return EXEC_DEFAULT;
}

void fix_chain_lighting(void) {

    Z_Grandmaster_Magic->WriteDword(0x005A66C6 + 3, (int) (Z_Spells[19].special));
    Z_Grandmaster_Magic->WriteDword(0x005A68CE + 3, (int) (Z_Spells[19].special));
    
    Z_Grandmaster_Magic->WriteWord(0x005A66A5 + 1, 0xbd04);
    Z_Grandmaster_Magic->WriteDword(0x005A66A5 + 3, (int) (Z_Spells[19].base_value));
    if (set_max_magic_proficiency) set_max_magic_proficiency(19, 7);

    return;
    Z_Grandmaster_Magic->WriteDword(0x005A66C6 + 3, (int)chain_lighting_parts);
    Z_Grandmaster_Magic->WriteDword(0x005A68CE + 3, (int)chain_lighting_parts);
    // Z_Grandmaster_Magic->WriteLoHook(0x005A669D, hook_005A669D_);
    Z_Grandmaster_Magic->WriteWord(0x005A66A5 + 1, 0xbd04);
    Z_Grandmaster_Magic->WriteDword(0x005A66A5 + 3, (int)chain_lighting_effect);
    if (set_max_magic_proficiency) set_max_magic_proficiency(19, 5);
}
