// dllmain.cpp : Defines the entry point for the DLL application.
// #include "pch.h"
#include "patcher_x86_commented.hpp"
#include "__include/era.h"
#include "__include/heroes.h"
#include "lib\H3API.hpp"
#include "_extended_spell_info_.h"

#define PINSTANCE_MAIN "Z_Grandmaster_Magic"

Patcher* globalPatcher;
PatcherInstance* Z_Grandmaster_Magic;

HMODULE More_SS_Levels_Link = nullptr;
int (*get_real_magic_proficiency)(HERO*, int, int, int);
void (*set_max_magic_proficiency)(int, int);
int  (*get_max_magic_proficiency)(int);
char (*_get_hero_SS_)(HERO*, int);

int   (*get_SS_table_zi)(int skill, int level, int type);
float (*get_SS_table_zf)(int skill, int level, int type);

HMODULE Emerald_Link = nullptr;
void (*Combo_Add_Spells)(void*, int);

void fix_chain_lighting(void);
void fix_summonClone(void);
void fix_AppliedSpells_Efects(void);
void fix_MassDamageSpells(void);
void fix_spell_description(void);
void fix_healing_spells(void);
void fix_Adventure_Spells(void);
void fix_magic_specialties(void);

void activate_more_spells_cpp(void);

void load_More_SS_Levels_Link(void) {
    if (!More_SS_Levels_Link) More_SS_Levels_Link = GetModuleHandleA("More_SS_Levels.era");
    if (More_SS_Levels_Link) {
        get_real_magic_proficiency = (int(*)(HERO*, int, int, int))
            GetProcAddress(More_SS_Levels_Link, "get_real_magic_proficiency");
        set_max_magic_proficiency = (void(*)(int, int))
            GetProcAddress(More_SS_Levels_Link, "set_max_magic_proficiency");
        get_max_magic_proficiency = (int(*)(int))
            GetProcAddress(More_SS_Levels_Link, "get_max_magic_proficiency");
        _get_hero_SS_ = (char(*)(HERO*, int))
            GetProcAddress(More_SS_Levels_Link, "_get_hero_SS_");

        get_SS_table_zf = (float(*)(int, int, int))
            GetProcAddress(More_SS_Levels_Link, "get_SS_table_zf");
        get_SS_table_zi = (int(*)(int, int, int))
            GetProcAddress(More_SS_Levels_Link, "get_SS_table_zi");
    }
}

extern void fix_Quicksand(void);
void DumpAllSpells();
#define SPL_FIRST_NEW 81
#define o_Spell h3::H3Internal::Spell()
static char null_text[] = "NULL SPELL";
static char default_sound[] = "default.wav";

bool Z_load_late_applied = false;
void __stdcall Z_load_late(Era::TEvent* e) {

    if (Z_load_late_applied) return;
    fix_Quicksand();
    // load_More_SS_Levels_Link();
    fix_chain_lighting();
    fix_summonClone();
    fix_AppliedSpells_Efects();
    fix_MassDamageSpells();
    fix_healing_spells();
    fix_Adventure_Spells();
    fix_magic_specialties();
    fix_spell_description();
    // _extended_spell_info_::read();
    Z_load_late_applied = true;

    for (int i = SPL_FIRST_NEW; i < 200; ++i) {
        if (!o_Spell[i].name) o_Spell[i].name = null_text;
        if (!o_Spell[i].shortName) o_Spell[i].shortName = null_text;
        if (!o_Spell[i].description[0]) o_Spell[i].description[0] = null_text;
        if (!o_Spell[i].description[1]) o_Spell[i].description[1] = null_text;
        if (!o_Spell[i].description[2]) o_Spell[i].description[2] = null_text;
        if (!o_Spell[i].description[3]) o_Spell[i].description[3] = null_text;
        if (!o_Spell[i].soundName) o_Spell[i].soundName = default_sound;
        if (!*(long*)&o_Spell[i].flags)
        {
            o_Spell[i].flags.map_spell = 1;
            o_Spell[i].flags.battlefield_spell = 1;
        }
        if (!*(long*)&o_Spell[i].school) o_Spell[i].school = h3::H3Spell::eSchool(15);
        if (!o_Spell[i].level) o_Spell[i].level = 1;
    }
    //DumpAllSpells();
    _extended_spell_info_::clean();
}

void __stdcall Z_load_after_wog(Era::TEvent* e) {
    load_More_SS_Levels_Link();

    if(!Emerald_Link) Emerald_Link = GetModuleHandleA("emerald3_3.era");
    if (Emerald_Link) Combo_Add_Spells = (void(*)(void*, int))
        GetProcAddress(Emerald_Link,"Combo_Add_Spells");
    
    
    /*
    for (int j = 0; j < 27; ++j) for(int i = 0;i<70;++i)
        Z_Spells[i].chance_to_get[j] = o_Spell[i].chance_to_get[j%9];
     */

    activate_more_spells_cpp();
    _extended_spell_info_::hook();

}

void __stdcall Z_load_Config(Era::TEvent* e)
{
    _extended_spell_info_::read(); 
}


void __stdcall Z_Debug(Era::TEvent* e)
{
    DumpAllSpells();
}

extern int ERM_ZSpellDesc[200][8];
void __stdcall StoreData(Era::TEvent* e)
{
    Era::WriteSavegameSection(sizeof(ERM_ZSpellDesc), (void*)&(ERM_ZSpellDesc[0][0]), PINSTANCE_MAIN "ERM_ZSpellDesc");
    Era::WriteSavegameSection(sizeof(Z_Spells), (void*)&Z_Spells, PINSTANCE_MAIN "_Z_Spells_v2");
    // Era::WriteSavegameSection(sizeof(h3::H3Spell) * 200, o_Spell, PINSTANCE_MAIN "_o_Spell");
}


void __stdcall RestoreData(Era::TEvent* e)
{
    // Era::ReadSavegameSection(sizeof(h3::H3Spell) * 200, o_Spell, PINSTANCE_MAIN "_o_Spell");
    Era::ReadSavegameSection(sizeof(ERM_ZSpellDesc), (void*)&(ERM_ZSpellDesc[0][0]), PINSTANCE_MAIN "ERM_ZSpellDesc");
    Era::ReadSavegameSection(sizeof(Z_Spells), (void*)&Z_Spells, PINSTANCE_MAIN "_Z_Spells_v2");

    _extended_spell_info_::read();
}


void __stdcall OnAfterScriptsReload(Era::TEvent* e)
{
    _extended_spell_info_::read();
}

void GM_Magic_Fixes();
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {

        globalPatcher = GetPatcher();
        Z_Grandmaster_Magic = globalPatcher->CreateInstance(PINSTANCE_MAIN);

        

        Era::ConnectEra();
        Era::RegisterHandler(Z_load_late, "OnGameEnter");
        Era::RegisterHandler(Z_load_after_wog, "OnAfterWoG");
        // Era::RegisterHandler(Z_load_OnAfterCreateWindow,"OnAfterCreateWindow");
        Era::RegisterHandler(Z_load_Config, "OnAfterLoadMedia");
        Era::RegisterHandler(Z_load_Config, "OnBeforeErmInstructions");
        Era::RegisterHandler(Z_Debug, "OnGenerateDebugInfo");
        Era::RegisterHandler(OnAfterScriptsReload, "OnAfterScriptsReload");

        Era::RegisterHandler(StoreData, "OnSavegameWrite");
        Era::RegisterHandler(RestoreData, "OnSavegameRead");



        GM_Magic_Fixes();

        break;
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

