#define _CRT_SECURE_NO_WARNINGS
#include <bitset>

// #include"lib/H3API.hpp"
#include"../../__include__/H3API/single_header/H3API.hpp"

#include"_extended_spell_info_.h"

#include "patcher_x86_commented.hpp"
// #include "__include/era.h"
#include "__include/heroes.h"

#include "__include/era_lite.h"

void ExecErmSequence(char* _command)
{
    using namespace Era;
    if (!ExecErmCmd) ConnectEra();
    if ((!_command) || (!*_command)) return;

    char command[512];
    memset(command, 0, 512);
    strcpy(command, _command);

    char* p;

    p = strtok(command, "!");
    ExecErmCmd(p);
    do {
        p = strtok(NULL, "!");
        if (p) ExecErmCmd(p);
    } while (p);
}

extern HMODULE More_SS_Levels_Link;

// #include "Elementals.h"
extern Patcher* globalPatcher;
extern PatcherInstance* Z_Grandmaster_Magic;

#define ORIG_ADVSPELLS_NUM 10
#define ORIG_SPELLS_NUM 70
//#define ARTIFACTS_NUM 144

#define SPELLS_MAX 140
#define SPELLS_WITH_DURATION 162
#define SPELLS_MAX_HERO 140
#define SPELLS_MAX_ALL  200
#define ANIMS_MAX  0x7f
#define HEROES_NUM 156

#define SPELL_MEMORIZED 1
#define SPELL_TEMPORARY 2

/*
#define ANIMS_NUM 89
#define ANIM_FEAR 83
#define ANIM_DEATH_CLOUD 84
#define ANIM_DEATH_BLOW 85
#define ANIM_TOUGHNESS 86
#define ANIM_BEHEMOTHS_CLAWS 87
#define ANIM_INCINERATION 88
*/

// #define SPELLS_NUM 82


// spells
#define SPL_SUMMON_BOAT  0
#define SPL_SCUTTLE_BOAT 1
#define SPL_VISIONS   2
#define SPL_VIEW_EARTH  3
#define SPL_DISGUISE  4
#define SPL_VIEW_AIR  5
#define SPL_FLY    6
#define SPL_WATER_WALK  7
#define SPL_DIMENSION_DOOR 8
#define SPL_TOWN_PORTAL  9

#define SPL_QUICKSAND  10
#define SPL_LAND_MINE  11
#define SPL_FORCE_FIELD  12
#define SPL_FIRE_WALL  13
#define SPL_EARTHQUAKE  14
#define SPL_MAGIC_ARROW  15
#define SPL_ICE_BOLT  16
#define SPL_LIGHTNING_BOLT 17
#define SPL_IMPLOSION  18
#define SPL_CHAIN_LIGHTNING 19
#define SPL_FROST_RING  20
#define SPL_FIREBALL  21
#define SPL_INFERNO   22
#define SPL_METEOR_SHOWER 23
#define SPL_DEATH_RIPPLE 24
#define SPL_DESTROY_UNDEAD 25
#define SPL_ARMAGEDDON  26
#define SPL_SHIELD   27
#define SPL_AIR_SHIELD  28
#define SPL_FIRE_SHIELD  29
#define SPL_PROTECTION_FROM_AIR  30
#define SPL_PROTECTION_FROM_FIRE 31
#define SPL_PROTECTION_FROM_WATER 32
#define SPL_PROTECTION_FROM_EARTH 33
#define SPL_ANTI_MAGIC  34
#define SPL_DISPEL   35
#define SPL_MAGIC_MIRROR 36
#define SPL_CURE   37
#define SPL_RESURRECTION 38
#define SPL_ANIMATE_DEAD 39
#define SPL_SACRIFICE  40
#define SPL_BLESS   41
#define SPL_CURSE   42
#define SPL_BLOODLUST  43
#define SPL_PRECISION  44
#define SPL_WEAKNESS  45
#define SPL_STONE_SKIN  46
#define SPL_DISRUPTING_RAY 47
#define SPL_PRAYER   48
#define SPL_MIRTH   49
#define SPL_SORROW   50
#define SPL_FORTUNE   51
#define SPL_MISFORTUNE  52
#define SPL_HASTE   53
#define SPL_SLOW   54
#define SPL_SLAYER   55
#define SPL_FRENZY   56
#define SPL_TITANS_LIGHTNING_BOLT 57
#define SPL_COUNTERSTRIKE 58
#define SPL_BERSERK   59
#define SPL_HYPNOTIZE  60
#define SPL_FORGETFULNESS 61
#define SPL_BLIND   62
#define SPL_TELEPORT  63
#define SPL_REMOVE_OBSTACLE 64
#define SPL_CLONE   65

#define SPL_FIRE_ELEMENTAL 66
#define SPL_EARTH_ELEMENTAL 67
#define SPL_WATER_ELEMENTAL 68
#define SPL_AIR_ELEMENTAL 69


#define SPL_DISEASE 73
#define SPL_AGE 75
#define SPL_DISPEL_HELPFUL_SPELLS 78
#define SPL_ACID_BREATH 80
#define SPL_FIRST_NEW 81

#define SPL_GHOST 81


#define CID_AIR_ELEMENTAL 112
#define CID_EARTH_ELEMENTAL 113
#define CID_FIRE_ELEMENTAL 114
#define CID_WATER_ELEMENTAL 115


#include "EmeraldSupport.h"
#define AID_SPELL_SCROLL 1

#define CeilDiv(x, y) ((x - 1) / y + 1)

typedef std::bitset<ORIG_SPELLS_NUM> _SpellBitset70_;
typedef std::bitset<SPELLS_MAX_HERO> _SpellBitset_;

#define _PI Z_Grandmaster_Magic


// typedef h3::H3Army _Army_;
typedef h3::H3Town _Town_;
typedef h3::H3Hero _Hero_;
typedef h3::H3Spell _Spell_;
typedef h3::H3Main _GameMgr_;
typedef h3::H3CreatureInformation _CreatureInfo_;
typedef h3::H3Army ArmyGroup;
typedef h3::H3CombatManager _BattleMgr_;
typedef h3::H3AdventureManager _AdvMgr_;


struct _BookSpell_
{
    int id;
    int school_flags;
    int school_level;
};

#define o_ComboArtInfo (*(_ComboArtInfo_**)0x660B6C)
#define o_MagicAnim ((_MagicAnim_*)0x641E18)


// _Spell_ *Spell = (_Spell_*) 0x007BD2C0;
// _Spell_ Spell[SPELLS_MAX];
// _MagicAnim_ SpellAnim[ANIMS_MAX];



#define o_GameMgr h3::H3Internal::Main()
#define o_TownMgr h3::H3Internal::TownManager()
#define o_Spell h3::H3Spell::Get()
#define o_pCreatureInfo h3::H3CreatureInformation::Get()
#define o_ArtInfo h3::H3ArtifactSetup::Get()
#define o_ComboArtInfo h3::H3Internal::CombinationArtifacts()
#define o_BattleMgr h3::H3CombatManager::Get()

#define ID_NONE -1
#define NOALIGN __declspec(align(1))


bool check_HI_Spell(void* H, int spell);

// #include "Elementals.h"

NOALIGN struct _Struct_
{
    template <typename _type_>
    inline _type_& Field(int offset) { return *(_type_*)((_ptr_)this + offset); }

    template <typename _type_>
    inline _type_* PField(int offset) { return (_type_*)((_ptr_)this + offset); }

    inline _ptr_    Offset(int offset) { return ((_ptr_)this + offset); }
};


struct Game : public _GameMgr_, public _Struct_
{
    /*
    inline int getCurrentDay()
    {
        return curr_day_ix + (curr_week_ix - 1) * 7 + (curr_month_ix - 1) * 28;
    }
    */
    
    inline bool SpellDisabled(int SpellID)
    {
        if (SpellID < SPELLS_MAX_HERO)
            return PField<char>(4)[SpellID];
        else return false;
    }
    
    
    inline void DisableSpell(int SpellID, bool Disabled = true)
    {
        if(SpellID<SPELLS_MAX_HERO)
            PField<char>(4)[SpellID] = Disabled;
    }
    
    inline void OpenArea(int x, int y, int z, int player, int radius, int a1)
    {
        CALL_7(void, __thiscall, 0x49CDD0, this, x, y, z, player, radius, a1);
    }
    void SetSpellsAvailability();
    int FillShrine(int shrineFlags);
};

#define pGame (*(Game**)0x699538)
void DisableSpell(int SpellID, bool Disabled = true) {
    pGame->DisableSpell(SpellID,Disabled);
}

struct _spell_effect_ {
    float f; int i;
};

struct Army : public h3::H3CombatMonster//_BattleStack_
{
    static _spell_effect_ ArmyState[2][21][SPELLS_MAX_ALL];

    inline _spell_effect_* GetSpellEffect(int spellID) {
        return &ArmyState[side][sideIndex][spellID];
    }

    inline int get_active_spell_duration(int spellID) {
        
        return this->activeSpellDuration[spellID];
    }

    inline int GetEffectiveAttackAgainst(Army* enemy, bool isRangedAttack)
    {
        return CALL_3(int, __thiscall, 0x442130, this, enemy, isRangedAttack);
    }
    inline void CancelIndividualSpell(int SpellID)
    {
        CALL_2(void, __thiscall, 0x444230, this, SpellID);
    }
    inline int current_health()
    {
        return numberAlive * baseHP - healthLost;
    }
    inline int full_health()
    {
        return numberAlive * baseHP;
    }
    inline int fightValue()
    {
        return info.fightValue * numberAlive;
    }
    inline int GetOrientation()
    {
        return secondHexOrientation;
    }
    inline int get_total_combat_value(int a1, int a2)
    {
        return CALL_3(int, __thiscall, 0x442B80, this, a1, a2);
    }
    bool CanUnitAttack(Army* enemy);
    int GetEffectiveDefenseAgainst(Army* enemy, bool needFrenzyMod);
    double GetDefenseModifier(Army* defender, bool isRangedAttack);
};
_spell_effect_ Army::ArmyState[2][21][SPELLS_MAX_ALL];

struct Hero : public _Hero_
{
    inline int GetSpellSchoolLevel(int SpellID, int landModifier)
    {
        return CALL_3(int, __thiscall, 0x4E52F0, this, SpellID, landModifier);
    }
    inline int GetManaCost(int SpellID, const ArmyGroup* armyGroup, int landModifier)
    {
        return CALL_4(int, __thiscall, 0x4E54B0, this, SpellID, armyGroup, landModifier);
    }
    inline void SpendMana(int mana)
    {
        CALL_2(void, __thiscall, 0x4D9540, this, mana);
    }
    void AddSpell(int SpellID);
    void UpdateSpellsFromArtifacts();
    int getBestMagicSchoolForSpell(int SpellID);

    inline int GetLandModifierUnder() { return CALL_1(int, __thiscall, 0x4E5210, this); }

    inline bool HasSpell(int Spell) {
        if (Spell < SPELLS_MAX_HERO)
            return ((_Hero_*)this) ->HasSpell(Spell);
        else return check_HI_Spell(this, Spell);
    }

    inline char get_rune_mastery() {
        int z_skill = secSkill[8];
        if (!More_SS_Levels_Link) return z_skill;
        int z_mastery[16] = { 0,1,1,2,2,2,3,3,3,3,4,4,4,4,4,5 };
        return z_mastery[z_skill];
    }

};


struct CombatManager : public _BattleMgr_, public _Struct_
{
    /*
   inline Army* GetActiveStack()
   {
	  return (Army*)&stack[unk_side][current_stack_ix];
   }
   */
   inline bool hasObstaclePenalty(Army* stack, int hex, int foeHex)
   {
	  return CALL_4(bool, __thiscall, 0x4670F0, this, stack, hex, foeHex);
   }
   inline bool hasDistancePenalty(Army* stack, Army* enemy)
   {
	  return CALL_3(bool, __thiscall, 0x4671E0, this, stack, enemy);
   }
   inline void CastAreaSpell(int a1, int SpellID, int a3, int a4)
   {
	  CALL_5(void, __thiscall, 0x5A4C80, this, a1, SpellID, a3, a4);
   }
   inline void PlayAnimation(int animId, Army* stack, int time = 100, bool needHitAnim = false)
   {
	  CALL_5(void, __thiscall, 0x4963C0, this, animId, stack, time, needHitAnim);
   }
   inline bool MoatPresent()
   {
	  return Field<bool>(0x53A8);
   }
   inline bool InMoat(int hex, int wallIndex)
   {
	  return CALL_3(bool, __thiscall, 0x4699A0, this, hex, wallIndex);
   }
   inline int GetAction()
   {
	  return Field<int>(0x3C);
   }
   bool CanAISummonCreature(int SpellID, int side);

   inline UINT32* current_resource_amount() {
       UINT32 ret[8]; int side = currentActiveSide;
       int owner = heroOwner[side];
       INT32* res = (INT32*) &pGame->players[owner].playerResources;
       for (int i = 0; i < 7; ++i) 
        { ret[i] = res[i] < 0 ? 0 : res[i]; }
       ret[7] = ((int*)(0x27F9A00))[owner]; 
       if ((INT32) ret[7] <0) ret[7] = 0;
       return ret;
   }

   inline bool have_enough_resources(UINT32* arg) {
       UINT32* current = current_resource_amount();
       for (int i = 0; i < 8; ++i) {
           if (current[i] < arg[i])
               return false;
       }
       return true;
   }

   inline void pay_for_spell(UINT32* arg) {
       UINT32 ret[8]; int side = currentActiveSide;
       int owner = heroOwner[side];
       INT32* res = (INT32*)&pGame->players[owner].playerResources;
       for (int i = 0; i < 7; ++i) 
         { res[i] -= arg[i]; }
       ((int*)(0x27F9A00))[owner] -= arg[7];
   }

};


struct AdventureManager : public _AdvMgr_
{
    inline void RedrawMiniMap(int a1, int a2, int a3, int a4, int a5)
    {
        CALL_6(void, __thiscall, 0x4136F0, this, a1, a2, a3, a4, a5);
    }
    inline void SimulateMouseOver(int x, int y)
    {
        CALL_3(void, __thiscall, 0x40E2C0, this, x, y);
    }
};

struct WindowManager : public _Struct_
{
};

struct MouseManager : public _Struct_
{
    inline void SetMouseCursor(int frame, int type)
    {
        CALL_3(void, __thiscall, 0x50CEA0, this, frame, type);
    }
    inline void TurnMouseOn(bool on = true)
    {
        CALL_2(void, __thiscall, 0x50D7B0, this, on);
    }
};

void GetMouseCoordinates(int& x, int& y)
{
    CALL_2(void, __stdcall, 0x50D700, &x, &y);
}

#define pAdventureManager (*(AdventureManager**)0x6992B8)
#define pCombatManager (*(CombatManager**)0x699420)
#define pWindowManager (*(WindowManager**)0x6992D0)
#define pMouseManager (*(MouseManager**)0x6992B0)

#include "Elementals.h"

struct _active_spells_data_ {
    long activeSpellLevel;
};
_active_spells_data_ active_spells_data[SPELLS_WITH_DURATION];

// These are not exactly optional tables, so don't remove them
char spellIndirectTableA[SPELLS_MAX_ALL - ORIG_ADVSPELLS_NUM] =
{
    // Spells (starting from Quicksand #10)
     0,  1,  2,  2,  3,  4,  4,  4,  4,  4,
     5,  5,  5,  5,  4,  4,  4,  4,  4,  4,
     4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
     6,  4,  4,  4,  4,  4,  4,  4,  4,  4,
     4,  4,  4,  4,  4,  4,  4,  4,  4,  5,
     4,  4,  4,  7,  8,  9, 10, 10, 10, 10,
     // Special Abilities (starting from Stone Gaze #71)
      4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
      4,
      //// New Spells (starting from Fear #81)
      // 4,  5,  4,  4, 10, 10, 10,  4,  4,  4,
      // 4,  5,  4
};

char spellIndirectTableB[SPELLS_MAX_ALL - ORIG_ADVSPELLS_NUM] =
{
    // Spells (starting from Quicksand #10)
     0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
    10, 11, 12, 13, 14, 15, 16, 17, 17, 17,
    17, 17, 17, 17, 17, 18, 17, 19, 20, 20,
    21, 17, 17, 22, 17, 17, 17, 23, 17, 17,
    17, 17, 17, 17, 17, 17, 17,  7, 17, 24,
    17, 17, 17, 25, 26, 27, 28, 29, 30, 31,
    // Special Abilities (starting from Stone Gaze #71)
    32, 33, 34, 17, 17, 34, 37, 37, 35, 37,
    36,
    //// New Spells (starting from Fear #81)
    // 17, 11, 17, 17, 38, 39, 40, 17, 17, 17,
    // 17, 11, 17
};

char spellIndirectTableC[SPELLS_MAX_ALL - SPL_SHIELD] =
{
    // Spells (starting from Shield #27)
     0,  1,  2,  3,  4,  5,  6,  7, 32,  8,
    32, 32, 32, 32,  9, 10, 11, 12, 13, 14,
    32, 15, 16, 17, 18, 19, 20, 21, 22, 23,
    32, 24, 25, 26, 27, 28, 32, 32, 32, 32,
    32, 32, 32,
    // Special Abilities (starting from Stone Gaze #71)
    32, 29, 32, 30, 32, 31, 32, 32, 32, 32,
    32,
    // New Spells (starting from Fear #81)
    // 32, 32, 32, 32, 32, 32, 32, 32, 32, 32,
    // 32, 32, 32
};

char spellIndirectTableD[SPELLS_MAX_ALL - SPL_WEAKNESS] =
{
    // Spells (starting from Weakness #45)
     0,  1,  9,  2,  9,  9,  9,  9,  3,  4,
     9,  9,  9,  9,  9,  5,  9,  9,  9,  9,
     9,  9,  9,  9,  9,
     // Special Abilities (starting from Stone Gaze #71)
     9,  9,  6,  7,  9,  8,  9,  9,  9,  9,
     9,
     //  // New Spells (starting from Fear #81)
     //  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,
     //  9,  9,  9
};

char spellIndirectTableE[SPELLS_MAX_ALL - SPL_LIGHTNING_BOLT] =
{
    // Spells (starting from Lightning Bolt #17)
     0, 16,  0, 16, 16, 16, 16,  1,  2, 16,
    16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    16,  3,  4, 16,  5,  6, 16,  7, 16, 16,
    16, 16,  8,  8,  9,  9, 16, 16,  9, 16,
    16, 16, 10, 11, 12, 13, 16, 16, 16, 16,
    16, 16, 16,
    // Special Abilities (starting from Stone Gaze #71)
    14, 15, 16, 16, 16, 16, 16, 16, 16, 16,
    16,
    // New Spells (starting from Fear #81)
    // 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
    // 16, 16, 16
};

char spellIndirectTableF[SPELLS_MAX_ALL - SPL_EARTHQUAKE] =
{
    // Spells (starting from Earthquake #14)
     0,  9,  9,  9,  9,  1,  2,  2,  2,  2,
     3,  3,  3,  9,  9,  9,  9,  9,  9,  9,
     9,  4,  9,  9,  5,  5,  6,  9,  9,  9,
     9,  9,  9,  9,  9,  9,  9,  9,  9,  9,
     9,  9,  9,  9,  9,  9,  9,  9,  9,  7,
     9,  9,  8,  8,  8,  8,
     // Special Abilities (starting from Stone Gaze #71)
      9,  9,  9,  9,  9,  9,  9,  9,  9,  9,
      9,
      // // New Spells (starting from Fear #81)
      // 9,  2,  9,  9,  8,  8,  8,  9,  9,  9,
      // 9,  2,  9
};


char shrineSpells[SPELLS_MAX_HERO];


int creatureSpell[] =
{
   SPELL_STONE, SPELL_ROOTS, SPELL_PARALIZE, SPELL_DEATHCLOUD, SPELL_THUNERBOLT,
   SPELL_DISPELBEN, SPELL_DEATHSTARE, SPELL_ACIDBREATH
};



const char* oldSpellName[] =
{
   "Summon Boat", "Scuttle Boat", "Visions", "View Earth", "Disguise",
   "View Air", "Fly", "Water Walk", "Dimension Door", "Town Portal",
   "Quicksand", "Land Mine", "Force Field", "Fire Wall", "Earthquake",
   "Magic Arrow", "Ice Bolt", "Lightning Bolt", "Implosion", "Chain Lightning",
   "Frost Ring", "Fireball", "Inferno", "Meteor Shower", "Death Ripple",
   "Destroy Undead", "Armageddon", "Shield", "Air Shield", "Fire Shield",
   "Protection from Air", "Protection from Fire", "Protection from Water", "Protection from Earth", "Anti-Magic",
   "Dispel", "Magic Mirror", "Cure", "Resurrection", "Animate Dead",
   "Sacrifice", "Bless", "Curse", "Bloodlust", "Precision",
   "Weakness", "Stone Skin", "Disrupting Ray", "Prayer", "Mirth",
   "Sorrow", "Fortune", "Misfortune", "Haste", "Slow",
   "Slayer", "Frenzy", "Titan's Lightning Bolt", "Counterstrike", "Berserk",
   "Hypnotize", "Forgetfulness", "Blind", "Teleport", "Remove Obstacle",
   "Clone", "Sunmmon Fire Elemental", "Summon Earth Elemental", "Summon Water Elemental", "Summon Air Elemental"
};


void fillSpell(const int id)
{
    int Earth, Water, Fire, Air;
    // TODO much TODO
}


void fillSpell2(const int Spell_ID)
{
    int Earth, Water, Fire, Air;
    // TODO much TODO
}


void initHeroAdvInfoEx()
{
    for (int i = 0; i < HEROES_NUM; ++i)
    {
        //heroAdvInfoEx[i].mobilityCastCount = 0;
        //heroAdvInfoEx[i].eyeOfTheMagiCastCount = 0;
    }
}


int __stdcall afterInit(LoHook* h, HookContext* c)
{
    /*
    // Spell Table
    for (int i = SPL_SUMMON_BOAT; i <= SPL_ACID_BREATH; ++i)
        Spell[i] = o_Spell[i];
    */

    /*
    for (std::size_t i = 0; i < sizeof(newSpell) / sizeof(int); ++i)
        fillSpell(i);
    */

    for (std::size_t i = SPL_FIRST_NEW; i < SPELLS_MAX_ALL; ++i)
        fillSpell2(i);




    // Adventure Spells
    initHeroAdvInfoEx();


    return EXEC_DEFAULT;
}


void Game::SetSpellsAvailability()
{
    char TextBuffer[64];

    // Old spells
    for (int iSpell = SPL_SUMMON_BOAT; iSpell <= SPL_AIR_ELEMENTAL; ++iSpell)
    {
        /*
        int key = GetPrivateProfileIntA("Enabled Spells", TextBuffer, 1, iniPath);
        sprintf(TextBuffer, "<%s>", oldSpellName[iSpell]);
        if (key == 0)
            DisableSpell(iSpell);
        if (key == -1)
            DisableSpell(iSpell, false);
        */
    }

    // Creature spells
    for (std::size_t i = 0; i < sizeof(creatureSpell) / sizeof(int); ++i)
        DisableSpell(creatureSpell[i]);

    /*
    // New spells
    for (std::size_t i = 0; i < sizeof(newSpell) / sizeof(int); ++i)
        DisableSpell(newSpell[i], GetPrivateProfileIntA(newSpellName[i], "<Enabled>", 1, iniPath) != 1);
    */
}

void Hero::AddSpell(int SpellID)
{
    // spell[SpellID] = SPELL_MEMORIZED;
    //   learned_spell[SpellID] = SPELL_MEMORIZED;
    learnedSpells[SpellID] = SPELL_MEMORIZED;

}

void __fastcall AddSpell(Hero* hero, int unused_edx, int SpellID)
{
    hero->AddSpell(SpellID);
}


// Spells which may appear
_SpellBitset_ mayAppearSpellsEx;
int __stdcall expandMayAppearSpells(LoHook* h, HookContext* c)
{
    int mayAppearSpellsAddr;
    int outputBitsetAddr;
    int returnAddr;

    switch (h->GetAddress())
    {
    case 0x5BE518:
        mayAppearSpellsAddr = c->edi + 0xD4;
        outputBitsetAddr = (int)&c->ecx;
        returnAddr = 0x5BE51E;
        break;
    case 0x5BE52D:
        mayAppearSpellsAddr = c->edi + 0xD4;
        outputBitsetAddr = (int)&c->eax;
        returnAddr = 0x5BE533;
        break;
    default:
        mayAppearSpellsAddr = c->esi + 0xD4;
        outputBitsetAddr = (int)&c->ebx;
        returnAddr = 0x5BEA2A;
    }

    // UINT32 *check_me = (UINT32*) mayAppearSpellsAddr;
    _SpellBitset70_* mayAppearSpells = (_SpellBitset70_*)mayAppearSpellsAddr;
    /*
    if (!check_me[0] && !check_me[1] && !check_me[2]){
        for (int iSpell = 0; iSpell < ORIG_SPELLS_NUM; ++iSpell)
            mayAppearSpells->set(iSpell);
    }
    */
    for (int iSpell = 0; iSpell < ORIG_SPELLS_NUM; ++iSpell)
        mayAppearSpellsEx.set(iSpell, mayAppearSpells->test(iSpell));

    for (int iSpell = ORIG_SPELLS_NUM; iSpell < SPELLS_MAX_HERO; ++iSpell)
        if (pGame->disabledSpells[iSpell])
            mayAppearSpellsEx.set(iSpell);

    *(int*)outputBitsetAddr = (int)&mayAppearSpellsEx;
    c->return_address = returnAddr;
    return NO_EXEC_DEFAULT;
}


// Add here *new* spells which must appear in all towns for testing purposes
int mustAppearSpellsList[] = { ID_NONE };

bool mustSpellAppearInTowns(int SpellID)
{
    bool mustAppear = false;

    for (std::size_t i = 0; i < sizeof(mustAppearSpellsList) / sizeof(int); ++i)
    {
        if (SpellID == mustAppearSpellsList[i])
        {
            mustAppear = true;
            break;
        }
    }

    return mustAppear;
}

// Spells which must appear
_SpellBitset_ mustAppearSpellsEx;
int __stdcall expandMustAppearSpells(LoHook* h, HookContext* c)
{
    _SpellBitset70_* mustAppearSpells = (_SpellBitset70_*)c->eax;
    int SpellID = c->esi;

    for (int iSpell = 0; iSpell < ORIG_SPELLS_NUM; ++iSpell)
        mustAppearSpellsEx.set(iSpell, mustAppearSpells->test(iSpell));

    for (int iSpell = ORIG_SPELLS_NUM; iSpell < /*SPELLS_NUM*/ SPELLS_MAX_HERO; ++iSpell)
        if (!pGame->SpellDisabled(iSpell) && mustSpellAppearInTowns(iSpell))
            mustAppearSpellsEx.set(iSpell);

    c->return_address = mustAppearSpellsEx.test(SpellID) ? 0x5BEB3B : 0x5BEB1D;
    return NO_EXEC_DEFAULT;
}
// ===============================================================


int __stdcall initSpells(LoHook* h, HookContext* c)
{
    pGame->SetSpellsAvailability();
    memcpy(&shrineSpells, &pGame->disabledShrines, sizeof(shrineSpells) / sizeof(char));

    c->return_address = 0x4C2641;
    return NO_EXEC_DEFAULT;
}


int Game::FillShrine(int shrineFlags)
{
    int availSpellsNum = 0;

    for (int i = 0; i < /*SPELLS_NUM*/ SPELLS_MAX_HERO; ++i)
    {
        int lvl = o_Spell[i].level - 1;
        if (lvl < 5 && ((1 << lvl) & shrineFlags) && o_Spell[i].school && !shrineSpells[i])
            ++availSpellsNum;
    }

    int SpellID = 0;
    int chosenSpell = 0;

    if (availSpellsNum)
    {
        // int rndSpell = Randint(0, availSpellsNum - 1);
        int rndSpell = CALL_0(unsigned int,__cdecl,0x0061842C)% availSpellsNum;

        for (int i = 0; i < /*SPELLS_NUM*/ SPELLS_MAX_HERO; ++i)
        {
            int lvl = o_Spell[i].level - 1;
            if (lvl < 5 && ((1 << lvl) & shrineFlags) && o_Spell[i].school && !shrineSpells[i])
            {
                if (SpellID == rndSpell) break;
                ++SpellID;
            }
            ++chosenSpell;
        }

        shrineSpells[chosenSpell] = true;
    }
    else
    {
        SpellID = 0;

        for (int i = 0; i < /*SPELLS_NUM*/ SPELLS_MAX_HERO; ++i)
        {
            int lvl = o_Spell[i].level - 1;
            if (lvl < 5 && ((1 << lvl) & shrineFlags) && o_Spell[i].school && !disabledShrines[i])
            {
                shrineSpells[i] = false;
                ++SpellID;
            }
        }

        chosenSpell = SpellID ? FillShrine(shrineFlags) : ID_NONE;
    }

    return chosenSpell;
}


int __fastcall FillShrine(Game* game, int unused_edx, int shrineFlags)
{
    return game->FillShrine(shrineFlags);
}


int __stdcall applySpell(LoHook* h, HookContext* c)
{
    int SpellID = *(int*)(c->ebp + 8);
    Hero* hero = *(Hero**)(c->ebp + 0x14);
    Army* stack = (Army*)c->esi;
    int schoolLevel = c->eax;
    // int spellSpecialtyEffect = 0;
    int vEffect = c->edi;

    int SpellPower = *(int*)(c->ebp + 12);

    if (SpellID < ORIG_ADVSPELLS_NUM) {
        // h3::F_MessageBox("Tried to Apply Adventure spell in combat");
        c->return_address = 0x444D5C;
        return NO_EXEC_DEFAULT;
    }




    if (Z_Spells[SpellID].zflag & _extended_spell_info_::is_combo) {
        volatile int pos = o_BattleMgr->actionTarget; // stack->position;
        // if (pos < 0) h3::H3Messagebox("Debug: target_pos < 0");
        // h3::H3Messagebox("Debug: apply combo spell");
        // Era::ExecErmCmd("IF:M^Debug: apply combo spell^;");
        for (int i = 0; i < 8; ++i) {
            if (Z_Spells[SpellID].special[i + 16] < SPELLS_MAX_ALL) {
                if (hero && FALSE) stack->ApplySpell(Z_Spells[SpellID].special[i + 16], SpellPower, schoolLevel, hero);
                else P_CombatMgr->CastSpell(Z_Spells[SpellID].special[i + 16], pos, hero ? 0 : 1, pos, schoolLevel, SpellPower);
            }
        }
        // return EXEC_DEFAULT;
        P_CombatMgr->action = h3::eCombatAction::NOTHING;
        
        c->return_address = 0x444D5C;
        return NO_EXEC_DEFAULT;
    }

    if (SpellID >= SPELLS_WITH_DURATION) {
        h3::H3Messagebox("Tried to Apply spell which can't have duration.");
    }


    if (Z_Spells[SpellID].zflag == 0 || (Z_Spells[SpellID].zflag & _extended_spell_info_::is_disabled))
        return EXEC_DEFAULT;

    if(!(Z_Spells[SpellID].zflag & _extended_spell_info_::is_timed)) return EXEC_DEFAULT;

    /*
    switch (SpellID)
    {

    default:
        return EXEC_DEFAULT;
    }
    */
    if ((Z_Spells[SpellID].zflag & _extended_spell_info_::has_damage)
        && (Z_Spells[SpellID].zflag & _extended_spell_info_::is_debuff) ) {
        int base_dmg = Z_Spells[SpellID].base_value[schoolLevel] + o_Spell[SpellID].spEffect * SpellPower; // hero->primarySkill[2];
        int dmg = CALL_7(int, __thiscall, 0x005A7BF0, (void*) P_CombatMgr, base_dmg, SpellID, hero, /*DHp*/ nullptr, stack, 1);

        (stack->GetSpellEffect(SpellID)->i) = dmg;
        /*
        int pos2 = CALL_2(int, __thiscall, 0x00443DB0, targetmon, dmg); *(int*)(c->ebp + 14) = pos2;
        CALL_3(char, __thiscall, 0x00468570, P_CombatMgr, ((int*)&(o_Spell[SpellID]))[2], 1);
        if (!CALL_1(char, __fastcall, 0x0046A080, P_CombatMgr)) // battle visible
        {
            CALL_6(void, __thiscall, 0x00469670, P_CombatMgr, o_Spell[SpellID].name, 1, dmg, targetmon, pos2); // log
        }
        CALL_1(char*, __thiscall, 0x00469020, P_CombatMgr); //phoenix
        */

        c->return_address = 0x444D5C;
        return NO_EXEC_DEFAULT;
    }


    if (SpellID > 80 && !(Z_Spells[SpellID].zflag & _extended_spell_info_::is_timed)) {
        c->eax = 0;
        c->return_address = 0x004446AD;
        return NO_EXEC_DEFAULT;
    }

    c->return_address = 0x444D5C;
    return NO_EXEC_DEFAULT;
}


int __stdcall resetSpell(LoHook* h, HookContext* c)
{
    int SpellID = *(int*)(c->ebp + 8);
    Army* stack = (Army*)c->esi;

    
    switch (SpellID)
    {
    case SPELL_WEAKNESS:
    case SPELL_PRAYER:
    case SPELL_HASTE:
    case SPELL_SLOW:
    case SPELL_HYPNOTIZE:
    case SPELL_BLIND:
    case SPELL_ROOTS:
    case SPELL_DECAY:
    case SPELL_AGE:
        int id = SpellID - 0x2d;
        int opt = *(char*)(0x004445EC+id);
        c->return_address = ((int*)0x004445C4)[opt];
        return SKIP_DEFAULT;
    }
    
    if (Z_Spells[SpellID].zflag == 0 || (Z_Spells[SpellID].zflag & _extended_spell_info_::is_disabled))
        return EXEC_DEFAULT;

    /*
    switch (SpellID)
    {


    default:
        return EXEC_DEFAULT;
    }
    */


    c->return_address = 0x4444E1;
    return SKIP_DEFAULT;
}


int __stdcall creatureCast(LoHook* h, HookContext* c)
{
    int SpellID = c->eax;
    int hex_ix = *(int*)(c->esi - 0xB4);

    switch (SpellID)
    {
 
    default:
        return EXEC_DEFAULT;
    }

    c->return_address = 0x468CE7;
    return NO_EXEC_DEFAULT;
}


int __stdcall cureNewSpells(LoHook* h, HookContext* c)
{
    Army* stack = (Army*)c->esi;

    //CALL_2(void, __thiscall, 0x444230, stack, SPL_FEAR);
    for (int spell = SPL_FIRST_NEW; spell < SPELLS_WITH_DURATION; ++spell)
        if (Z_Spells[spell].zflag & _extended_spell_info_::is_debuff)
            CALL_2(void, __thiscall, 0x444230, stack, spell);

    return EXEC_DEFAULT;
}


int __stdcall getStackMagicVulnerability(LoHook* h, HookContext* c)
{
    _CreatureInfo_* creature = (_CreatureInfo_*)c->edi;
    int CreatureID = *(int*)(c->ebp - 8);
    int SpellID = *(int*)(c->ebp - 0x10);

    bool immune = false; bool _default_ = true;

    if(Z_Spells[SpellID].zflag & _extended_spell_info_::is_runic)
        if(!pCombatManager->have_enough_resources(Z_Spells[SpellID].special+8))
        {
            immune = true; _default_ = false;
        }

    /*
    switch (SpellID)
    {

    default:
        return EXEC_DEFAULT;
    }
    */
    if (_default_) return EXEC_DEFAULT;

    c->return_address = immune ? 0x44A3E8 : 0x44A416;
    return NO_EXEC_DEFAULT;
}


// class type_AI_spellcaster
struct type_AI_spellcaster : _Struct_
{
    _ptr_ func;
    Hero* hero;
    Hero* enemyHero;
    // ...
};



// struct type_enchant_data
struct type_enchant_data : _Struct_
{
    int SpellID;
    int a2; // School Level, but not always
    int a3; // Spell Power
    int a4; // Spell Power
    int a5;
};



int __stdcall get_enchantment_function(HiHook* h, type_AI_spellcaster* caster, int SpellID)
{

    if (SpellID > 80) {
        return 0;
    }

    /*
    switch (SpellID)
    {

    default:
        return CALL_2(int, __thiscall, h->GetDefaultFunc(), caster, SpellID);
    }
    */
    return CALL_2(int, __thiscall, h->GetDefaultFunc(), caster, SpellID);

}


bool CombatManager::CanAISummonCreature(int SpellID, int side)
{
    if (heroMonCount[side] >= 20)
        return false;

    return true;
}


bool __fastcall CanAISummonCreature(CombatManager* combatMgr, int unused_edx, int SpellID, int side)
{
    return combatMgr->CanAISummonCreature(SpellID, side);
}

int __fastcall getCreatureTypeToSummon(int SpellID)
{

    return spell2creature(o_BattleMgr,SpellID);

    int CreatureID;

    switch (SpellID)
    {
    case SPL_FIRE_ELEMENTAL:
        CreatureID = CID_FIRE_ELEMENTAL;
        break;
    case SPL_EARTH_ELEMENTAL:
        CreatureID = CID_EARTH_ELEMENTAL;
        break;
    case SPL_WATER_ELEMENTAL:
        CreatureID = CID_WATER_ELEMENTAL;
        break;
    case SPL_AIR_ELEMENTAL:
        CreatureID = CID_AIR_ELEMENTAL;
        break;
        /*
    case SPL_SPRITE:
        CreatureID = CID_SPRITE;
        break;
    case SPL_MAGIC_ELEMENTAL:
        CreatureID = CID_MAGIC_ELEMENTAL;
        break;
    case SPL_FIREBIRD:
        CreatureID = CID_FIREBIRD;
        break;
        */
    default:
        CreatureID = ID_NONE;
    }

    return CreatureID;
}


double getSummoningEffect(int SpellID, int schoolLevel)
{
    double effect = 0.0;
    double elem_AI_Value_Avg = (o_pCreatureInfo[CID_AIR_ELEMENTAL].aiValue + o_pCreatureInfo[CID_EARTH_ELEMENTAL].aiValue +
        o_pCreatureInfo[CID_FIRE_ELEMENTAL].aiValue + o_pCreatureInfo[CID_WATER_ELEMENTAL].aiValue) / 4.0;

    switch (SpellID)
    {
    case SPL_FIRE_ELEMENTAL:
    case SPL_EARTH_ELEMENTAL:
    case SPL_WATER_ELEMENTAL:
    case SPL_AIR_ELEMENTAL:
        effect = 1;
        break;
/*
    case SPL_SPRITE:
        effect = 0.8 * elem_AI_Value_Avg / o_pCreatureInfo[CID_SPRITE].AI_value;
        break;
    case SPL_MAGIC_ELEMENTAL:
        effect = 1.2 * elem_AI_Value_Avg / o_pCreatureInfo[CID_MAGIC_ELEMENTAL].AI_value;
        break;
    case SPL_FIREBIRD:
        effect = 1.2 * elem_AI_Value_Avg / o_pCreatureInfo[CID_FIREBIRD].AI_value;
        break;
 */
    case SPL_GHOST:
        effect = 1;
        break;  
    }

    // if (schoolLevel > 3) schoolLevel = 3;
    if (schoolLevel > 7) schoolLevel = 7;
    effect *= Z_Spells[SpellID].base_value[schoolLevel];

    return effect;
}


int __stdcall battleAISpellWeighting(LoHook* h, HookContext* c)
{
    int SpellID = c->edi;
    int schoolLevel = *(int*)(c->ebx + 4);
    int spellPower = *(int*)(c->ebx + 8);
    int side = *(int*)(c->esi + 0xC);

    bool _default_ = true;
    /*
    switch (SpellID)
    {
    case SPL_FIRE_ELEMENTAL:
    case SPL_EARTH_ELEMENTAL:
    case SPL_WATER_ELEMENTAL:
    case SPL_AIR_ELEMENTAL:
    case SPL_GHOST:
        //
    case SPL_SPRITE:
    case SPL_MAGIC_ELEMENTAL:
    case SPL_FIREBIRD:
        */
    if (spell2creature(pCombatManager, SpellID)>=0) {
        if (!*(bool*)(c->esi + 0x1C) && pCombatManager->CanAISummonCreature(SpellID, side))
        {
            *(bool*)(c->ebx + 0x20) = true;

            int creaturesNum = (int)(getSummoningEffect(SpellID, schoolLevel) * spellPower);
            if (creaturesNum < 1) creaturesNum = 1;

            int k = *(bool*)(c->esi + 0x28) ? 1000 : creaturesNum;
            *(int*)(c->ebx + 0x1C) = k * o_pCreatureInfo[getCreatureTypeToSummon(SpellID)].aiValue;
        }
        _default_ = false;
    }
        /*
        break;
    default:
        return EXEC_DEFAULT;
    }
    */
    if (_default_) return EXEC_DEFAULT;

    c->return_address = 0x43B9BF;
    return NO_EXEC_DEFAULT;
}

// ===============================================================
// ---- Summon Sprite, Summon Magic Elemental, Summon Firebird ---
// ---------------------------------------------------------------
int __stdcall SummonCreatures(LoHook* h, HookContext* c)
{
    int SpellID = *(int*)(c->ebp + 8);
    auto bm = (h3::H3CombatManager*) c->ebx;
    int CreatureID = spell2creature(bm,SpellID);

    if(CreatureID == -1)
        return EXEC_DEFAULT;
    /*
    switch (SpellID)
    {

    case SPL_FIRE_ELEMENTAL:
    case SPL_EARTH_ELEMENTAL:
    case SPL_WATER_ELEMENTAL:
    case SPL_AIR_ELEMENTAL:
        break;

    case SPL_GHOST:
        break;
        /*
    case SPL_SPRITE:
        CreatureID = CID_SPRITE;
        break;
    case SPL_MAGIC_ELEMENTAL:
        CreatureID = CID_MAGIC_ELEMENTAL;
        break;
    case SPL_FIREBIRD:
        CreatureID = CID_FIREBIRD;
        break;
        
    default:
        return EXEC_DEFAULT;
    }
    */

    CALL_5(void, __thiscall, 0x5A7390, pCombatManager, SpellID, CreatureID, *(int*)(c->ebp + 0x1C), c->esi);

    c->return_address = 0x5A2368;
    return NO_EXEC_DEFAULT;
}

int __stdcall skipNonElementals(LoHook* h, HookContext* c)
{
    c->return_address = 0x5A74DD;
    return NO_EXEC_DEFAULT;

    int CreatureID = *(int*)(c->ebp + 0xC);

    switch (CreatureID)
    {
        /*
    case CID_SPRITE:
    case CID_FIREBIRD:
        c->return_address = 0x5A74DD;
        return NO_EXEC_DEFAULT;
        */
    default:
        return EXEC_DEFAULT;
    }
}


int spriteSpellParams[] = { 1000, 1000, 1500, 2000 };
int magicElementalSpellParams[] = { 100, 100, 150, 200 };
int firebirdSpellParams[] = { 50, 50, 75, 100 };

int __stdcall setSummonedCreaturesNumber(LoHook* h, HookContext* c)
{
    int CreatureID = *(int*)(c->ebp + 0xC);
    int spellPower = *(int*)(c->ebp + 0x10);
    int schoolLevel = *(int*)(c->ebp + 0x14);

    switch (CreatureID)
    {
        /*
    case CID_SPRITE:
        c->esi = spriteSpellParams[schoolLevel];
        break;
    case CID_MAGIC_ELEMENTAL:
        c->esi = magicElementalSpellParams[schoolLevel];
        break;
    case CID_FIREBIRD:
        c->esi = firebirdSpellParams[schoolLevel];
        break;
        */
    default:
        return EXEC_DEFAULT;
    }

    c->esi = c->esi * spellPower / 100;
    if (c->esi < 1) c->esi = 1;

    c->ecx = c->ebx;
    c->return_address = 0x5A7526;
    return NO_EXEC_DEFAULT;
}

int __stdcall checkSummonedCreaturesType(LoHook* h, HookContext* c)
{
    c->return_address = 0x59F8B7;
    int SpellID = c->ebx;
    _BattleMgr_* bm = (_BattleMgr_*) c->esi;

    c->eax = spell2creature(bm, SpellID);

    return NO_EXEC_DEFAULT;
}
// ===============================================================


void __stdcall setMouseCursor(HiHook* h, MouseManager* mouseManager, int frame, int type)
{

    /*
    if (eyeOfTheMagiCast && frame == 41 && type == 1)
    {
        frame = 0;
        type = 3;
    }
    */

    CALL_3(void, __thiscall, h->GetDefaultFunc(), mouseManager, frame, type);
}


int __stdcall castAdventureSpell(LoHook* h, HookContext* c)
{
    int SpellID = *(int*)(c->ebp + 8);
    Hero* hero = (Hero*)c->esi;

    int landModifier = hero->GetLandModifierUnder();
    int schoolLevel = hero->GetSpellSchoolLevel(SpellID, landModifier);

    switch (SpellID)
    {


    default:
        return EXEC_DEFAULT;
    }

    c->return_address = 0x41C67B;
    return NO_EXEC_DEFAULT;
}


int __stdcall loadHeroAdvInfoEx(LoHook* h, HookContext* c)
{
    Hero* hero = (Hero*)c->edi;

    if (hero)
    {
        /*
        CALL_3(void, __thiscall, *(int*)(*(int*)c->esi + 4), c->esi, c->ebp + 8, 4);
        heroAdvInfoEx[hero->id].mobilityCastCount = *(int*)(c->ebp + 8);
        CALL_3(void, __thiscall, *(int*)(*(int*)c->esi + 4), c->esi, c->ebp + 8, 4);
        heroAdvInfoEx[hero->id].eyeOfTheMagiCastCount = *(int*)(c->ebp + 8);
        */
    }

    return EXEC_DEFAULT;
}


int __stdcall saveHeroAdvInfoEx(LoHook* h, HookContext* c)
{
    Hero* hero = (Hero*)c->edi;

    if (hero)
    {

/*
        *(int*)(c->ebp + 8) = heroAdvInfoEx[hero->id].mobilityCastCount;
        CALL_3(void, __thiscall, *(int*)(*(int*)c->esi + 8), c->esi, c->ebp + 8, 4);
        *(int*)(c->ebp + 8) = heroAdvInfoEx[hero->id].eyeOfTheMagiCastCount;
        CALL_3(void, __thiscall, *(int*)(*(int*)c->esi + 8), c->esi, c->ebp + 8, 4);
 */
    
    }

    return EXEC_DEFAULT;
}
// ===============================================================


#include <bitset>
typedef std::bitset<140> _SpellBitset_;
void Hero::UpdateSpellsFromArtifacts()
{
    if (!this) return;
    if (int(this)==0xbaadf00d) return;

    for (int iSpell = 0; iSpell < SPELLS_MAX_HERO; ++iSpell)
        if (learnedSpells[iSpell] > SPELL_MEMORIZED)
            learnedSpells[iSpell] = 0;

    for (int iSlot = 0; iSlot < 19; ++iSlot)
    {
        if (bodyArtifacts[iSlot].id != ID_NONE)
        {
            if (bodyArtifacts[iSlot].id == AID_SPELL_SCROLL)
            {
                if (bodyArtifacts[iSlot].subtype < SPELLS_MAX_HERO && !learnedSpells[bodyArtifacts[iSlot].subtype])
                    learnedSpells[bodyArtifacts[iSlot].subtype] = SPELL_TEMPORARY;
            }
            else if (o_ArtInfo[bodyArtifacts[iSlot].id].newSpell)
            {
                _SpellBitset_ spells;
                CALL_2(int, __fastcall, 0x4D95C0, &spells, bodyArtifacts[iSlot].id);

                for (std::size_t iSpell = 0; iSpell < spells.size(); ++iSpell)
                {
                    if (spells[iSpell] && !learnedSpells[iSpell])
                        learnedSpells[iSpell] = SPELL_TEMPORARY;
                }

                int comboArtIndex = o_ArtInfo[bodyArtifacts[iSlot].id].comboID;
                if (Emerald_Link) Combo_Add_Spells(this,comboArtIndex);
                else if (comboArtIndex != -1)
                {

                    //// TODO
                    /*
                    for (int iArt = 0; iArt < 144; ++iArt)
                    {
                        //if (o_ComboArtInfo[comboArtIndex].parts[iArt] && o_ArtInfo[iArt].newSpell)
                        //if (Z_ComboArtInfo(comboArtIndex,iArt) && o_ArtInfo[iArt].newSpell)
                        if (o_ComboArtInfo[comboArtIndex].artifacts->GetState(iArt) && o_ArtInfo[iArt].newSpell)
                        {
                            _SpellBitset_ spells;
                            CALL_2(int, __fastcall, 0x4D95C0, &spells, iArt);

                            for (std::size_t iSpell = 0; iSpell < spells.size(); ++iSpell)
                            {
                                if (spells[iSpell] && !learned_spell[iSpell])
                                    learned_spell[iSpell] = SPELL_TEMPORARY;
                            }
                        }
                    }
                    */
                }
                
            }
        }
    }
    //TODO
}

void __fastcall UpdateSpellsFromArtifacts(void* hero)
{
    if(hero)    ((Hero*) hero)->UpdateSpellsFromArtifacts();
}
/*
void __stdcall UpdateSpellsFromArtifacts(HiHook* h, Hero* hero) {
    // if(Emerald_Linkage) THISCALL_1(int, h->GetDefaultFunc(), hero);
    hero->UpdateSpellsFromArtifacts();
}
*/

extern "C" __declspec(dllexport) void UpdateSpellsFromEmerald(void* hero)
{
    if (hero)    ((Hero*)hero)->UpdateSpellsFromArtifacts();
}

_SpellBitset_ RMGDisabledSpells;
int __stdcall RMGDisableSpells(LoHook* h, HookContext* c)
{
    for (int iSpell = 0; iSpell < ORIG_SPELLS_NUM; ++iSpell)
        pGame->DisableSpell(iSpell,false);
    _extended_spell_info_::read();
    _extended_spell_info_::clean();

    for (int iSpell = 0; iSpell < SPELLS_MAX_HERO; ++iSpell)
        RMGDisabledSpells.set(iSpell, pGame->SpellDisabled(iSpell));

    CALL_3(void, __thiscall, *(int*)(*(int*)c->ebx + 8), c->ebx, &RMGDisabledSpells, CeilDiv(ORIG_SPELLS_NUM/*SPELLS_MAX_HERO*/, 8));

    c->return_address = 0x54AF1E;
    return NO_EXEC_DEFAULT;
}


int __stdcall RMGDisableSpellsInScrollsA(LoHook* h, HookContext* c)
{
    int SpellID = (c->eax - (int)&o_Spell->school) / sizeof(_Spell_);

    if (pGame->SpellDisabled(SpellID))
    {
        c->return_address = 0x5353EE;
        return NO_EXEC_DEFAULT;
    }

    return EXEC_DEFAULT;
}


int __stdcall RMGDisableSpellsInScrollsB(LoHook* h, HookContext* c)
{
    int SpellID = (c->eax - (int)&o_Spell->school) / sizeof(_Spell_);

    if (pGame->SpellDisabled(SpellID))
    {
        c->return_address = 0x535423;
        return NO_EXEC_DEFAULT;
    }

    return EXEC_DEFAULT;
}

_LHF_(hook_005A7516) {
    // c->esi = (int)Z_Spells[*(int*)(c->ebp + 8)].base_value; // [*(int*)(c->ebp + 14)] ;
    c->esi = Z_Spells[*(int*)(c->ebp + 8)].base_value[c->edx];
    c->return_address = 0x005A7520;
    return NO_EXEC_DEFAULT;
}


static inline void cmp(FlagsRegister& flags,long a,long b) {
    if (a < b)      { flags.ZF = 0; flags.CF = 1; }
    if (a > b)      { flags.ZF = 0; flags.CF = 0; }
    if (a == b)     { flags.ZF = 1; flags.CF = 0; }
}

/*
_LHF_(hook_cmp_edi_70) {
    cmp(c->flags, c->edi, SPELLS_MAX_HERO);
    return EXEC_DEFAULT;
}

_LHF_(hook_cmp_esi_70) {
    cmp(c->flags, c->esi, SPELLS_MAX_HERO);
    return EXEC_DEFAULT;
}


_LHF_(hook_cmp_edx_70) {
    cmp(c->flags, c->edx, SPELLS_MAX_HERO);
    return EXEC_DEFAULT;
}

_LHF_(hook_cmp_eax_70) {
    cmp(c->flags, c->eax, SPELLS_MAX_HERO);
    return EXEC_DEFAULT;
}


_LHF_(hook_cmp_edi_81) {
    cmp(c->flags, c->edi, SPELLS_MAX_ALL);
    return EXEC_DEFAULT;
}

_LHF_(hook_cmp_esi_81) {
    cmp(c->flags, c->esi, SPELLS_MAX_ALL);
    return EXEC_DEFAULT;
}

_LHF_(hook_cmp_eax_81) {
    cmp(c->flags, c->eax, SPELLS_MAX_ALL);
    return EXEC_DEFAULT;
}
*/

_LHF_(hook_cmp_edi_70_0x402900) {
    if (c->edi < SPELLS_MAX_HERO)
        c->return_address = 0x004028F7;
    else
        c->return_address = 0x00402905;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_0048A349) {
    if (c->esi < SPELLS_MAX_HERO)
        c->return_address = 0x0048A332;
    else
        c->return_address = 0x0048A34E;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_00535429) {
    if (c->esi < SPELLS_MAX_HERO)
        c->return_address = 0x00535408;
    else
        c->return_address = 0x0053542E;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_0050244F) {
    if(c->edi < SPELLS_MAX_HERO)
        c->return_address = 0x005023D9;
    else
        c->return_address = 0x00502454;
    return NO_EXEC_DEFAULT;

}
_LHF_(hook_0059EFDA) {
    if (c->eax > (SPELLS_MAX_ALL - 1 - ORIG_ADVSPELLS_NUM) || c->eax < 0)
        c->return_address = 0x0059F96C;
    else c->return_address = 0x0059EFE0;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_005A064C) {
    if (c->eax > (SPELLS_MAX_ALL - 1 - ORIG_ADVSPELLS_NUM) || c->eax < 0)
        c->return_address = 0x005A2368;
    else
        c->return_address = 0x005A0655;
    return NO_EXEC_DEFAULT;
}

/*
_LHF_(hook_00444263) {
    cmp(c->flags, c->eax, SPELLS_MAX_ALL - 1 - SPL_WEAKNESS);
    // *(int*)(c->esi + 0x194) = c->edx;
    return EXEC_DEFAULT;
}
*/
_LHF_(hook_00444263) {
    if (c->edx < 0) c->edx = 0;
    int spell_id = *(int*)(c->ebp + 8);

    ((h3::H3CombatCreature*)(c->esi))->activeSpellNumber = c->edx;
    ((h3::H3CombatCreature*)(c->esi))->activeSpellDuration[spell_id] = 0;

    if ( c->eax > SPELLS_MAX_ALL - 1 - SPL_WEAKNESS || c->eax<0 ) {
        c->return_address = 0x004444E1;
    }
    else {
        c->return_address = 0x00444276;
    }
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0044A25A) {
    //cmp(c->flags, c->eax, SPELLS_MAX_ALL - 1 - SPL_LIGHTNING_BOLT);
    //return EXEC_DEFAULT;
    
    if (c->eax > SPELLS_MAX_ALL - 1 - /*SPL_EARTHQUAKE*/ SPL_LIGHTNING_BOLT || c->eax < 0)
        c->return_address = 0x0044A416;
    else c->return_address = 0x0044A260;
    return NO_EXEC_DEFAULT;

}
_LHF_(hook_0043B789) {
    //cmp(c->flags, c->eax, SPELLS_MAX_ALL - 1 - SPL_EARTHQUAKE);
    if (c->eax > SPELLS_MAX_ALL - 1 - SPL_EARTHQUAKE || c->eax < 0)
        c->return_address = 0x0043B984;
    else c->return_address = 0x0043B78F;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_cmp_edi_70_0x502403) {
    if (c->edi < SPELLS_MAX_HERO)
        c->return_address = 0x00502410;
    else
        c->return_address = 0x00502408;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edi_70_0x41FC8F) {
    if (c->edi < SPELLS_MAX_HERO)
        c->return_address = 0x0041FBD9;
    else
        c->return_address = 0x0041FC98;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edx_70_0x427082) {
    if (c->edx < SPELLS_MAX_HERO)
        c->return_address = 0x00427033;
    else
        c->return_address = 0x00427088;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edi_70_0x471C55) {
    if (c->edi < SPELLS_MAX_HERO)
        c->return_address = 0x00471C4C;
    else
        c->return_address = 0x00471C5A;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edi_70_0x4864AD) {
    ++(c->edi);
    if (c->edi < SPELLS_MAX_HERO)
        c->return_address = 0x0048649A;
    else
        c->return_address = 0x004864B3;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_eax_70_0x4A2741) {
    *(int*)(c->ebp - 0x14) = c->eax;
    if (c->eax < SPELLS_MAX_HERO)
        c->return_address = 0x004A269D;
    else
        c->return_address = 0x004A274D;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_esi_70_0x4F5089) {
    if (c->esi < SPELLS_MAX_HERO)
        c->return_address = 0x004F5080;
    else
        c->return_address = 0x004F508E;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_eax_70_0x4C170B) {
    *(int*)(c->ebp - 0x10) = c->eax;
    if (c->eax < SPELLS_MAX_HERO)
        c->return_address = 0x004C16C7;
    else
        c->return_address = 0x004C1713;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_eax_70_0x5012B5) {
    *(int*)(c->ebp + 0x0C) = c->eax;
    if (c->eax < SPELLS_MAX_HERO)
        c->return_address = 0x0050127A;
    else
        c->return_address = 0x005012BD;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edi_70_0x5BEA6E) {
    if (c->edi < SPELLS_MAX_HERO)
        c->return_address = 0x005BEA2D;
    else
        c->return_address = 0x005BEA73;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_esi_70_0x5BEAAB) {
    if (c->esi < SPELLS_MAX_HERO)
        c->return_address = 0x005BEAB8;
    else
        c->return_address = 0x005BEAB0;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_esi_70_0x5BEB46) {
    *(int*)(c->edx + 4 * c->eax) = c->esi;
    if (c->esi < SPELLS_MAX_HERO)
        c->return_address = 0x005BEB56;
    else
        c->return_address = 0x005BEB4E;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_esi_70_0x5BEB6F) {
    *(int*)c->eax = c->ecx;
    if (c->esi < SPELLS_MAX_HERO)
        c->return_address = 0x005BEC40;
    else
        c->return_address = 0x005BEB7A;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_esi_70_0x5BEBB0) {
    if (c->esi < SPELLS_MAX_HERO)
        c->return_address = 0x005BEBC2;
    else
        c->return_address = 0x005BEBB5;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_esi_70_0x5BEC19) {
    *(int*)(c->edx + 4 * c->eax) = c->esi;
    if (c->esi < SPELLS_MAX_HERO)
        c->return_address = 0x005BEC29;
    else
        c->return_address = 0x005BEC21;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edi_70_0x5024CD) {
    if (c->edi >= SPELLS_MAX_HERO)
        c->return_address = 0x005025DD;
    else
        c->return_address = 0x005024D6;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edi_70_0x502513) {
    if (c->edi < SPELLS_MAX_HERO)
        c->return_address = 0x005024A3;
    else
        c->return_address = 0x00502518;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edi_70_0x502E60) {
    if (c->edi >= SPELLS_MAX_HERO)
        c->return_address = 0x00502F24;
    else
        c->return_address = 0x00502E69;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edi_70_0x502EB5) {
    if (c->edi < SPELLS_MAX_HERO)
        c->return_address = 0x00502E30;
    else
        c->return_address = 0x00502EBE;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edi_81_0x443F61) {
    if (c->edi < SPELLS_WITH_DURATION)
        c->return_address = 0x00443F50;
    else
        c->return_address = 0x00443F66;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edi_81_0x446F01) {
    if (c->edi < SPELLS_WITH_DURATION)
        c->return_address = 0x00446EE0;
    else
        c->return_address = 0x00446F06;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_esi_81_0x5A1905) {
    if (c->esi < SPELLS_WITH_DURATION)
        c->return_address = 0x005A18F7;
    else
        c->return_address = 0x005A190A;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_edi_81_0x5A1993) {
    if (c->edi < SPELLS_WITH_DURATION)
        c->return_address = 0x005A1985;
    else
        c->return_address = 0x005A1998;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_eax_81_0x5A84C6) {
    if (c->eax < SPELLS_WITH_DURATION)
        c->return_address = 0x005A84BD;
    else
        c->return_address = 0x005A84CB;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_eax_81_0x5A852D) {
    if (c->eax < SPELLS_WITH_DURATION)
        c->return_address = 0x005A8519;
    else
        c->return_address = 0x005A8532;
    return NO_EXEC_DEFAULT;
}


_LHF_(hook_00745988) {
    if ((*(int*)(c->ebp - 0x54)) < SPELLS_MAX_ALL)
        c->return_address = 0x745993;
    else
        c->return_address = 0x74598E;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_cmp_edi_70_0x004E67AA) {
    if (c->edi < SPELLS_MAX_HERO) {
        c->return_address = 0x004E67B4;
    }
    else
    {
        c->return_address = 0x004E67AF;
    }
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_cmp_esi_70_0x4CEC4D) {
    if (c->esi < SPELLS_MAX_HERO) {
        c->return_address = 0x004CEC59;
    }
    else
    {
        c->return_address = 0x004CEC52;
    }
    return NO_EXEC_DEFAULT;

}

typedef h3::H3CombatMonster _BattleStack_;
int __stdcall castBattleSpell(LoHook* h, HookContext* c)
{
    int SpellID = c->edx;
    Army* stack = (Army*)c->ebx;
    int spell_power = *(int*)(c->ebp+0x1c);
    Hero* H = (Hero*) *(int*)(c->ebp - 0x14);

    int skillLevel = *(int*)(c->ebp + 0x18);
    int pos2 = *(int*)(c->ebp + 0x14);
    int casterKind = *(int*)(c->ebp + 0x10);
    int pos = *(int*)(c->ebp + 0x0C);
    // return EXEC_DEFAULT;
    Hero* DHp = (Hero*)*(int*)(c->ebp - 0x3c);

    int spel_lvl = c->esi;

    h3::H3CombatMonster *targetmon = (h3::H3CombatMonster*) c->edi;

    static void (*ChangeCreatureTable)(int, char*) = nullptr;
    if (!ChangeCreatureTable) {
        HMODULE Amethyst = 0;
        Amethyst = GetModuleHandleA("amethyst2_4.era");
        if (!Amethyst) Amethyst = GetModuleHandleA("amethyst2_4.dll");
        if (!Amethyst) Amethyst = GetModuleHandleA("amethyst2_5.era");
        if (!Amethyst) Amethyst = GetModuleHandleA("amethyst2_5.dll");

        if (Amethyst) {
            ChangeCreatureTable = (void(*)(int, char*)) GetProcAddress(Amethyst, "ChangeCreatureTable");
        }
    }

    static char* (*get_def_name)(int) = nullptr;
    if (!get_def_name) {
        HMODULE more_anims = 0;
        more_anims = GetModuleHandleA("more_anims.era");
        if(more_anims) get_def_name = (char*(*)(int)) GetProcAddress(more_anims, "get_def_name");
    }

    if(Z_Spells[SpellID].zflag == 0 || (Z_Spells[SpellID].zflag & _extended_spell_info_::is_disabled))
        return EXEC_DEFAULT;

    if ((Z_Spells[SpellID].zflag & _extended_spell_info_::has_amethyst_command)
        && ChangeCreatureTable && targetmon) {
        int target_stack = targetmon->Index();
        ChangeCreatureTable(-1-target_stack, Z_Spells[SpellID].command);
    }

    if ((Z_Spells[SpellID].zflag & _extended_spell_info_::has_ERM_command)
        && ExecErmSequence && Era::v && targetmon) {

        Era::v[2] = targetmon->Index();
        Era::v[3] = spel_lvl;// skillLevel;
        // Era::v[4] = Z_Spells[SpellID].GetBaseEffect(spel_lvl, H->primarySkill[2]);
        Era::v[4] = Z_Spells[SpellID].base_value[spel_lvl] + o_Spell[SpellID].spEffect * spell_power;// H->primarySkill[2];

        Era::v[10] = 0;
        ExecErmSequence(Z_Spells[SpellID].command);
        if (Era::v[10]) {
            P_CombatMgr->heroCasted[P_CombatMgr->currentActiveSide] = false;
            P_CombatMgr->action = h3::eCombatAction::CANCEL;
            CALL_2(void, __thiscall, 0x004D9540, H, -*(int*)(c->ebp - 0x68));

            c->return_address = 0x005A024F;
            return NO_EXEC_DEFAULT;
        }
    }

    if (Z_Spells[SpellID].zflag & _extended_spell_info_::is_runic) {
        pCombatManager->pay_for_spell(Z_Spells[SpellID].special+8);
    }

    if (Z_Spells[SpellID].zflag & _extended_spell_info_::is_combo) {
        // h3::H3Messagebox("Combo");
        // if(pos<0) h3::H3Messagebox("Combo Spell - pos<0");
        for (int i = 0; i < 8; ++i) {
            if (Z_Spells[SpellID].special[i + 16] < SPELLS_MAX_ALL)
                P_CombatMgr->CastSpell(Z_Spells[SpellID].special[i + 16],
                    pos, casterKind, pos2, casterKind ? 2 : skillLevel, spell_power);

        }
        // return EXEC_DEFAULT;
        P_CombatMgr->action = h3::eCombatAction::NOTHING;
        c->return_address = 0x005A024F;// 0x005A2368;
        return NO_EXEC_DEFAULT;
    }

    if (Z_Spells[SpellID].zflag & _extended_spell_info_::is_timed) {
        c->return_address = 0x005A138F;
        return NO_EXEC_DEFAULT;
    }
    else if (Z_Spells[SpellID].zflag & _extended_spell_info_::has_damage) {
        if ((Z_Spells[SpellID].zflag & _extended_spell_info_::has_flight_animation) 
           && get_def_name &&!CALL_1(char, __fastcall, 0x0046A080, (void*) P_CombatMgr))
        {
            int y = CALL_1(int, __thiscall, 0x00446350, targetmon);
            int x = CALL_1(int, __thiscall, 0x00446380, targetmon);
            char* anims[5]; 
            for (int i = 0; i < 5;++i) 
                anims[i] = get_def_name(i+ ((int*)&(o_Spell[SpellID]))[2]);
            CALL_8(void, __thiscall,0x00467990, (void*) P_CombatMgr, *(int*)(c->ebp-30)
                ,spel_lvl,x,y,5,0x00642210,anims);
        }

        if(!(Z_Spells[SpellID].zflag & _extended_spell_info_::has_flight_animation))
            CALL_5(int, __thiscall, 0x004963C0, (void*) P_CombatMgr, ((int*)(o_Spell+SpellID))[2],targetmon,100,0);
        int base_dmg = Z_Spells[SpellID].base_value[spel_lvl] + o_Spell[SpellID].spEffect * spell_power;//  H->primarySkill[2];
        int dmg = CALL_7(int, __thiscall, 0x005A7BF0, (void*) P_CombatMgr, base_dmg, SpellID, H, DHp, targetmon, 1);
        
        if(targetmon) // prevents some crashes
        {
            int pos2 = CALL_2(int, __thiscall, 0x00443DB0, targetmon, dmg); *(int*)(c->ebp + 14) = pos2;
            CALL_3(char, __thiscall, 0x00468570, (void*)P_CombatMgr, ((int*)&(o_Spell[SpellID]))[2], 1);
            if (!CALL_1(char, __fastcall, 0x0046A080, (void*)P_CombatMgr)) // battle visible
            {
                CALL_6(void, __thiscall, 0x00469670, (void*)P_CombatMgr, o_Spell[SpellID].name, 1, dmg, targetmon, pos2); // log
            }
            CALL_1(char*, __thiscall, 0x00469020, (void*)P_CombatMgr); //phoenix
        }

        c->return_address = 0x5A2368;
        return NO_EXEC_DEFAULT;
    }
    else if (Z_Spells[SpellID].zflag & (_extended_spell_info_::is_ressurrecting| _extended_spell_info_::is_healing)
        && !(Z_Spells[SpellID].zflag & _extended_spell_info_::is_buff)) {
        int base_heal = Z_Spells[SpellID].base_value[spel_lvl] + o_Spell[SpellID].spEffect * spell_power;// H->primarySkill[2];
        if (Z_Spells[SpellID].zflag & _extended_spell_info_::is_healing) {
            targetmon->healthLost -= base_heal;
            if (targetmon->healthLost < 0) {
                base_heal = -targetmon->healthLost;
                targetmon->healthLost = 0;
            }
        }
        if (base_heal>0 && (Z_Spells[SpellID].zflag & _extended_spell_info_::is_ressurrecting)) {
            int base_ressurrect = base_heal / targetmon->baseHP;
            if (base_ressurrect > 0 && targetmon->numberAlive <= 0) {
                CALL_1(_BattleStack_*, __thiscall, 0x0043E790, targetmon);
                CALL_3(char*, __thiscall, 0x004683A0, (void*) P_CombatMgr,targetmon, targetmon->position);
                CALL_3(int, __stdcall, 0x005A7630,&P_CombatMgr->squares[targetmon->position],targetmon->side,targetmon->sideIndex);
                if (targetmon->info.doubleWide) {
                    int v17 = CALL_1(int,__thiscall,0x004463C0,targetmon);
                    CALL_3(int, __stdcall, 0x005A7630, &P_CombatMgr->squares[v17], targetmon->side, targetmon->sideIndex);
                }
                if (targetmon->secondHexOrientation != 1 - targetmon->side)
                    CALL_2(char,__thiscall,0x00446440,targetmon,0);
                if (targetmon->animation == 5) {
                    targetmon->animation = 2;
                    targetmon->animationFrame = 0;
                }
                targetmon->info.cannotMove = 0;
            }

            if((Z_Spells[SpellID].zflag & _extended_spell_info_::does_overflow) || (targetmon->numberAtStart > targetmon->numberAlive))
            {
                // targetmon->
                targetmon->numberAlive += base_ressurrect;
                if (targetmon->numberAlive > targetmon->numberAtStart && !(Z_Spells[SpellID].zflag & _extended_spell_info_::does_overflow)) {
                    targetmon->numberAlive = targetmon->numberAtStart;
                }
                else {
                    targetmon->numberAtStart = targetmon->numberAlive;
                }
            }
        }
    }

    /*
    switch (SpellID)
    {
    case SPL_DEATH_CLOUD_NEW:
    case SPL_INCINERATION:
        pCombatManager->CastAreaSpell(*(int*)(c->ebp + 0xC), SpellID, c->esi, *(int*)(c->ebp + 0x1C));
        break;
    case SPL_EXPLOSION:
        pCombatManager->PlayAnimation(o_Spell[SPL_EXPLOSION].animation_ix, stack, 10);
        c->return_address = 0x5A0E2A;
        return NO_EXEC_DEFAULT;
    default:
        return EXEC_DEFAULT;
    }
    */
    
    /*
    if (stack && (Z_Spells[SpellID].zflag & _extended_spell_info_::is_timed)) {
        //CALL_5(stack,spell_power,c->esi,long(H));
        pCombatManager->PlayAnimation(((long*)(o_Spell+SpellID))[2], stack);
        stack->activeSpellsDuration[SpellID] = spell_power;
        c->return_address = 0x5A0E2A;
        return NO_EXEC_DEFAULT;

    }
    */

    c->return_address = 0x5A2368;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004446EB) {
    //cmp(c->flags, c->ecx, SPELLS_NUM - 1 - SPL_SHIELD);
    //return EXEC_DEFAULT;

    *(int*)(c->ebp + 0x10) = c->edi;
    c->edx = 2;

    if (c->ecx > SPELLS_MAX_ALL - 1 - SPL_SHIELD) {
        c->return_address = 0x00444D5C;
    }
    else
    {
        c->return_address = 0x004446F9;
    }

    return NO_EXEC_DEFAULT;
}

_LHF_(hook_BM_G) {
    c->edx = (int)&active_spells_data[c->eax].activeSpellLevel;
    c->return_address = 0x0075F367;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_004446A6) {
    active_spells_data[c->ebx].activeSpellLevel = c->eax;
    c->return_address = 0x004446AD;
    return NO_EXEC_DEFAULT;
}


bool check_HI_Spell(void* me,int spell) {
    Hero* H = (Hero*)me;
    if (spell >= SPELLS_MAX_ALL) return false;
    /*
    if (spell == 199)
        return true;
    */
    if (Z_Spells[spell].zflag & _extended_spell_info_::has_dependency) {
        for (int i = 0; i < 8; ++i) {
            if (Z_Spells[spell].special[i+16] < SPELLS_MAX_ALL)
                if (!H->HasSpell(Z_Spells[spell].special[i+16]))
                    return false;
        }
        return true;
    }
    if (Z_Spells[spell].zflag & _extended_spell_info_::is_runic) {
        return (H->get_rune_mastery() >= o_Spell[spell].level);
    }


    return false;
}

_LHF_(hook_0059CDC5) {
    Hero* H = (Hero*)c->ecx; int* v2 = (int*) c->esi;

    int &v53 = *(int*)(c->ebp - 0x44);
    int &v55 = *(int*)(c->ebp - 0x3c);
    int &v57 = *(int*)(c->ebp - 0x34);
    int &v58 = *(int*)(c->ebp - 0x30);

    for (int spell = SPELLS_MAX_HERO; spell < SPELLS_MAX_ALL; ++spell) {
        if (check_HI_Spell(H,spell) &&
            (o_Spell[spell].school & *(v2 + 28))
            && ((*(long*)(&(o_Spell[spell].flags))) & *(v2 + 29))
            ) {
            int v12 = CALL_2(int, __thiscall, 0x004E5430,H, o_Spell[spell].school);
            int v13 = *(v2 + 28);
            if (v13 >= 15)
                v13 = v12;
            int v14 = *(v2 + 27);
            v58 = v13;
            int v15 = *(v2 + 25);
            v57 = spell;
            

            CALL_4(unsigned int, __thiscall, 0x0054D4E0, &v53, v55, 1u, &v57);
        }
    }


    return EXEC_DEFAULT;
}

_LHF_(hook_0073382D) {
    if ((*(int*)(c->ebp - 0x44)) < SPELLS_MAX_HERO)
        c->return_address = 0x00733853;
    else
        c->return_address = 0x00733833;

    return NO_EXEC_DEFAULT;
}

_LHF_(spellChosen_0059EFD4) {
    int spell = c->ebx; int mastery = c->eax; 
    CombatManager* bm = (CombatManager*)c->esi;
    // if (spell < SPL_FIRST_NEW) return EXEC_DEFAULT;
    if (Z_Spells[spell].zflag == 0 || (Z_Spells[spell].zflag & _extended_spell_info_::is_disabled))
        return EXEC_DEFAULT;
    if (Z_Spells[spell].zflag & _extended_spell_info_::is_runic) {
        if(bm->have_enough_resources(Z_Spells[spell].special+8)) return EXEC_DEFAULT;
        // b_MsgBox("You have not enough resources !!!", 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
        FASTCALL_12(VOID, 0x4F6C00, (char*)"You have not enough resources !!!", 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
        c->return_address = 0x0059F96C;
        return NO_EXEC_DEFAULT;
    }


    return EXEC_DEFAULT;
}

const char* BattleMgr_CreatureMagicResist_text = nullptr;
double __stdcall BattleMgr_CreatureMagicResist_hook(HiHook* H, _BattleMgr_* ecx, int spell, int side, h3::H3CombatMonster* targetMon, int a5, int a6, int noHero) {
    BattleMgr_CreatureMagicResist_text = nullptr;
    static int(*isCreatureConstruct)(int) = nullptr;
    if (!isCreatureConstruct) {
        HMODULE Amethyst_Link = nullptr;
        if (!Amethyst_Link) Amethyst_Link = GetModuleHandleA("amethyst2_4.dll");
        if (!Amethyst_Link) Amethyst_Link = GetModuleHandleA("amethyst2_5.dll");
        if (Amethyst_Link) {
            isCreatureConstruct = (int(*)(int)) GetProcAddress(Amethyst_Link, "isCreatureConstruct");
        }
    }

    if (Z_Spells[spell].target) {
        // h3::H3CombatMonster* stackTarget = CALL_6(h3::H3CombatMonster*, __thiscall, 0x005A3C60, ecx, spell, ecx->currentActiveSide, ecx->actionTarget, 1, 0);
        h3::H3CombatMonster* &stackTarget = targetMon;
        if (!stackTarget) {
        fail:
            return 0.0;
        }
        if ((Z_Spells[spell].target & (UINT32)_extended_spell_info_::target::CasterOnly) && !stackTarget->info.spellCharges) {
            //FASTCALL_12(VOID, 0x4F6C00, (char*)"You can cast it only on Casters !!!", 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
            BattleMgr_CreatureMagicResist_text = "You can cast it only on Casters !!!";  goto fail;
        }
        if ((Z_Spells[spell].target & (UINT32)_extended_spell_info_::target::DragonOnly) && !stackTarget->info.dragon) {
            //FASTCALL_12(VOID, 0x4F6C00, (char*)"You can cast it only on Dragons !!!", 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
            BattleMgr_CreatureMagicResist_text = "You can cast it only on Dragons !!!";  goto fail;
            goto fail;
        }
        if ((Z_Spells[spell].target & (UINT32)_extended_spell_info_::target::nonDragonOnly) && stackTarget->info.dragon) {
            //FASTCALL_12(VOID, 0x4F6C00, (char*)"You can't cast it on Dragons !!!", 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
            BattleMgr_CreatureMagicResist_text = "You can't cast it on Dragons !!!";  goto fail;
            goto fail;
        }
        if ((Z_Spells[spell].target & (UINT32)_extended_spell_info_::target::livingOnly) && !stackTarget->info.alive) {
            //FASTCALL_12(VOID, 0x4F6C00, (char*)"You can cast it only on Dragons !!!", 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
            BattleMgr_CreatureMagicResist_text = "You can cast it only on living troops !!!";  goto fail;
            goto fail;
        }
        if ((Z_Spells[spell].target & (UINT32)_extended_spell_info_::target::undeadOnly) && !stackTarget->info.undead) {
            //FASTCALL_12(VOID, 0x4F6C00, (char*)"You can cast it only on Dragons !!!", 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
            BattleMgr_CreatureMagicResist_text = "You can cast it only on undead troops !!!";  goto fail;
            goto fail;
        }

        if ((Z_Spells[spell].target & (UINT32)_extended_spell_info_::target::constructOnly) && !isCreatureConstruct(stackTarget->type)) {
            //FASTCALL_12(VOID, 0x4F6C00, (char*)"You can cast it only on Dragons !!!", 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
            BattleMgr_CreatureMagicResist_text = "You can cast it only on Constructs !!!";  goto fail;
            goto fail;
        }
    }

    
    if (targetMon->numberAlive == 0 && (Z_Spells[spell].zflag & _extended_spell_info_::is_ressurrecting))
        return 1.0;
    
    return CALL_7(double, __thiscall, H->GetDefaultFunc(), ecx, spell,side,targetMon,a5,a6,noHero);
}

int  __stdcall BattleMgr_CanUseSpell_hook(HiHook* H, _BattleMgr_* ecx, int spell, int side, _BattleStack_* targetMon, int a5, int a6) {
    if (targetMon->numberAlive <= 0 && (Z_Spells[spell].zflag & _extended_spell_info_::is_ressurrecting))
        return true;

    return CALL_6(int, __thiscall, H->GetDefaultFunc(), ecx, spell, side, targetMon, a5, a6);
}
_BattleStack_* __stdcall CombatMan_005A3C60_GetSpellTargetMonsterAtPos_hook(HiHook* H, _BattleMgr_* ecx, int a2, int a3, signed int a4, char a5, int a6) {
    if (a4 < 0 || a4 >= 187)
        return nullptr;
    if (Z_Spells[a2].zflag & _extended_spell_info_::is_ressurrecting) {
        int ptr = int(ecx) + 112 * a4; int v6 = ptr + 452; int v9 = *(int*)(v6 + 0x1c)-1;
        if (v9>=0) for (int i = v9; i >=0;--i) {
            char v10 = *(char*)(v6 + 0x20 + i); _BattleStack_* v11 = &ecx->stacks[v10][*(char*)(v6 + 0x2e + i)];
            if (v10==a3) return v11;
        }
    }

    return CALL_6(_BattleStack_*, __thiscall, H->GetDefaultFunc(), ecx, a2, a3, a4, a5, a6);
}

_LHF_(z_hook_005A2FA8) {
    if (BattleMgr_CreatureMagicResist_text)
        c->eax = (int) BattleMgr_CreatureMagicResist_text;
    return EXEC_DEFAULT;
}

int __stdcall newRoundSpellSettings(LoHook* h, HookContext* c)
{
    Army* stack = (Army*)c->esi;

    for (int SpellID = 81; SpellID < SPELLS_WITH_DURATION; ++SpellID)
    {
        _spell_effect_* SE = stack->GetSpellEffect(SpellID);

        if((Z_Spells[SpellID].zflag & _extended_spell_info_::has_damage)
            && (Z_Spells[SpellID].zflag & _extended_spell_info_::is_debuff)
            && SE->i >0 && stack->get_active_spell_duration(SpellID) > 0)
        {
            int dmg = SE->i;

            if (!(Z_Spells[SpellID].zflag & _extended_spell_info_::has_flight_animation))
                CALL_5(int, __thiscall, 0x004963C0, (void*) P_CombatMgr, ((int*)(o_Spell + SpellID))[2], stack, 100, 0);

            int pos2 = CALL_2(int, __thiscall, 0x00443DB0, stack, dmg); // *(int*)(c->ebp + 14) = pos2;
            CALL_3(char, __thiscall, 0x00468570, (void*) P_CombatMgr, ((int*)&(o_Spell[SpellID]))[2], 1);
            if (!CALL_1(char, __fastcall, 0x0046A080, (void*) P_CombatMgr)) // battle visible
            {
                CALL_6(void, __thiscall, 0x00469670, (void*) P_CombatMgr, o_Spell[SpellID].name, 1, dmg, stack, pos2); // log
            }
            CALL_1(char*, __thiscall, 0x00469020, (void*) P_CombatMgr); //phoenix
        }
    }
    return EXEC_DEFAULT;
}

static char null_text[] = "NULL SPELL";
char default_sound[] = "default.wav";
void activate_more_spells_cpp(void) {
    static bool applied = false;

    if (!applied) {

        _PI->WriteHiHook(0x005A3C60, SPLICE_, EXTENDED_, THISCALL_, CombatMan_005A3C60_GetSpellTargetMonsterAtPos_hook);
        // _PI->WriteHiHook(0x005A3F90, SPLICE_, EXTENDED_, THISCALL_,BattleMgr_CanUseSpell_hook);
        _PI->WriteHiHook(0x005A83A0, SPLICE_, EXTENDED_, THISCALL_, BattleMgr_CreatureMagicResist_hook);
        _PI->WriteLoHook(0x005A2FA8, z_hook_005A2FA8);

        // Moving and Expanding Spell Table
        _PI->WriteLoHook(0x4EE1C1, afterInit);

        // nwcthereisnospoon
        _PI->WriteLoHook(0x402900, hook_cmp_edi_70_0x402900); // _PI->WriteByte(0x402902, SPELLS_NUM);

        // "(Already learned)" Text
        _PI->WriteDword(0x40D97C, 0x3EA);

        // AI
        _PI->WriteDword(0x41FBDF, 0x3EA);
        _PI->WriteLoHook(0x41FC8F,hook_cmp_edi_70_0x41FC8F); //_PI->WriteByte(0x41FC91, SPELLS_NUM); _PI->WriteByte(0x41FC93, 0x82);
        _PI->WriteDword(0x425C72, 0x3EA);
        _PI->WriteDword(0x425E98, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteDword(0x427039, 0x3EA);
        _PI->WriteDword(0x427044, 0x3EA);
        _PI->WriteLoHook(0x427082, hook_cmp_edx_70_0x427082); // _PI->WriteByte(0x427085, SPELLS_MAX_HERO); _PI->WriteByte(0x427086, 0x72);
        _PI->WriteDword(0x4329C1, 0x3EA);
        _PI->WriteDword(0x432A43, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteDword(0x432CDE, 0x3EA);
        _PI->WriteDword(0x432D1E, SPELLS_MAX_HERO * sizeof(_Spell_));

        // ===============================================================
        // ----------------------- Adventure AI --------------------------
        // ---------------------------------------------------------------
        _PI->WriteWord(0x430AE3, 0x868A);  _PI->WriteDword(0x430AE5, 0x3EA + SPL_DIMENSION_DOOR);
        _PI->WriteWord(0x4E57CA, 0x868A);  _PI->WriteDword(0x4E57CC, 0x3EA + SPL_SUMMON_BOAT);
        _PI->WriteWord(0x56B344, 0x868A);  _PI->WriteDword(0x56B346, 0x3EA + SPL_TOWN_PORTAL);
        _PI->WriteWord(0x56B7E8, 0x868A);  _PI->WriteDword(0x56B7EA, 0x3EA + SPL_DIMENSION_DOOR);
        _PI->WriteWord(0x56B93D, 0x868A);  _PI->WriteDword(0x56B93F, 0x3EA + SPL_FLY);
        _PI->WriteWord(0x56B998, 0x868A);  _PI->WriteDword(0x56B99A, 0x3EA + SPL_WATER_WALK);
        // ===============================================================

        // ===============================================================
        // ------------------------ Battle AI ----------------------------
        // ---------------------------------------------------------------
        _PI->WriteDword(0x4397E6, SPELLS_MAX_ALL * sizeof(_Spell_));
        _PI->WriteDword(0x43C6F2, SPELLS_MAX_HERO * sizeof(_Spell_));
        // Master Genie AI Spell Weighting
        _PI->WriteDword(0x43C21B, SPELLS_MAX_HERO * sizeof(_Spell_));
        // AI Quick Battle
        _PI->WriteDword(0x433026, 0x3EA);
        _PI->WriteDword(0x433719, 0x3EA);
        _PI->WriteDword(0x43940C, 0x3EA);
        _PI->WriteDword(0x43C561, 0x3EA);
        // AI Spell Scrolls?
        _PI->WriteDword(0x52A97A, 0x3EA);
        _PI->WriteDword(0x52A9B6, SPELLS_MAX_HERO * sizeof(_Spell_));
        // ===============================================================

        // Can cast?
        _PI->WriteDword(0x447551, SPELLS_MAX_HERO * sizeof(_Spell_));

        _PI->WriteDword(0x447C7D, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteDword(0x447CC8, SPELLS_MAX_HERO * sizeof(_Spell_));

        // Cheats in battle
        _PI->WriteLoHook(0x471C55, hook_cmp_edi_70_0x471C55); // _PI->WriteByte(0x471C57, SPELLS_MAX_HERO);  _PI->WriteByte(0x471C58, 0x72);

        // Clear Hero.spell[140] (optional)
        _PI->WriteDword(0x48647A, 0x23);
        _PI->WriteJmp(0x486485, 0x486498);

        _PI->WriteLoHook(0x4864AD, hook_cmp_edi_70_0x4864AD); //_PI->WriteByte(0x4864B0, SPELLS_MAX_HERO); _PI->WriteByte(0x4864B1, 0x72);

        // Scholar Secondary Skill
        _PI->WriteLoHook(0x4A2741, hook_cmp_eax_70_0x4A2741); // _PI->WriteByte(0x4A2743, SPELLS_MAX_HERO); _PI->WriteByte(0x4A2748, 0x82);

        // Load Game
        _PI->WriteLoHook(0x0048A349, hook_0048A349);// _PI->WriteByte(0x48A34B, SPELLS_MAX_HERO); _PI->WriteByte(0x48A34C, 0x72);

        // _PI->WriteByte(0x4E67AD, 0xEB);// _PI->WriteByte(0x4E67AC, SPELLS_MAX_HERO); _PI->WriteByte(0x4E67AD, 0x72);
        _PI->WriteLoHook(0x004E67AA, hook_cmp_edi_70_0x004E67AA);

        // Cheat Menu?
        _PI->WriteLoHook(0x4F5089, hook_cmp_esi_70_0x4F5089);  // _PI->WriteByte(0x4F508B, SPELLS_MAX_HERO); _PI->WriteByte(0x4F508C, 0x72);
        _PI->WriteDword(0x4F50CE, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteDword(0x4F5114, SPELLS_MAX_HERO * sizeof(_Spell_));

        // Init spells
        _PI->WriteLoHook(0x4C2625, initSpells);


        // Shrine spells
        _PI->WriteDword(0x4C92C5, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteDword(0x4C9347, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteDword(0x4C93C0, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteHiHook(0x4C9260, SPLICE_, DIRECT_, THISCALL_, FillShrine);


        // Pyramids
        _PI->WriteLoHook(0x4C170B, hook_cmp_eax_70_0x4C170B);  // _PI->WriteByte(0x4C170D, SPELLS_MAX_HERO); _PI->WriteByte(0x4C1711, 0x72);

        // _PI->WriteByte(0x4CEC50, 0xEB);// _PI->WriteByte(0x4CEC4F, SPELLS_MAX_HERO);  _PI->WriteByte(0x4CEC50, 0x72);
        _PI->WriteLoHook(0x4CEC4D, hook_cmp_esi_70_0x4CEC4D);

        // Clear Hero.spell[140] (optional)
        _PI->WriteDword(0x4D8F2E, 0x23);
        _PI->WriteJmp(0x4D8F36, 0x4D8F49);

        // Tome of Air Magic
        _PI->WriteDword(0x4D962D, SPELLS_MAX_HERO * sizeof(_Spell_));
        // Tome of Fire Magic
        _PI->WriteDword(0x4D9681, SPELLS_MAX_HERO * sizeof(_Spell_));
        // Tome of Water Magic
        _PI->WriteDword(0x4D96D6, SPELLS_MAX_HERO * sizeof(_Spell_));
        // Tome of Earth Magic
        _PI->WriteDword(0x4D972E, SPELLS_MAX_HERO * sizeof(_Spell_));
        // Spellbinder's Hat
        _PI->WriteDword(0x4D9771, SPELLS_MAX_HERO * sizeof(_Spell_));

        // Scholars
        _PI->WriteLoHook(0x5012B5, hook_cmp_eax_70_0x5012B5); // _PI->WriteByte(0x5012B7, SPELLS_MAX_HERO); _PI->WriteByte(0x5012BB, 0x72);

        _PI->WriteDword(0x527ACB, 0x3EA);
        _PI->WriteDword(0x527B08, SPELLS_MAX_HERO * sizeof(_Spell_));

        // RMG
        _PI->WriteDword(0x534C4B, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteLoHook(0x54AE0F, RMGDisableSpells);

        // RMG Spell Scrolls
        _PI->WriteDword(0x5353D5, SPELLS_MAX_HERO);
        _PI->WriteLoHook(0x535429, hook_00535429); // _PI->WriteByte(0x53542B, SPELLS_MAX_HERO); _PI->WriteByte(0x53542C, 0x72);
        _PI->WriteLoHook(0x5353E8, RMGDisableSpellsInScrollsA);
        _PI->WriteLoHook(0x535417, RMGDisableSpellsInScrollsB);

        // Spell Book
        _PI->WriteDword(0x59CCDD, SPELLS_MAX_ALL * sizeof(_BookSpell_));
        _PI->WriteDword(0x59CD36, SPELLS_MAX_ALL * sizeof(_BookSpell_));
        _PI->WriteDword(0x59CD5E, 0x3EA); _PI->WriteLoHook(0x0059CDC5, hook_0059CDC5);
        _PI->WriteDword(0x59CDBF, SPELLS_MAX_HERO * sizeof(_Spell_));

        // Add Spell
        _PI->WriteHiHook(0x4D95A0, SPLICE_, DIRECT_, THISCALL_, AddSpell);

        // Cast Spell
        _PI->WriteDword(0x5A1AD6, SPELLS_MAX_HERO * sizeof(_Spell_));

        // ===============================================================
        // -------------------------- Mage Guild -------------------------
        // ---------------------------------------------------------------
        _PI->WriteByte(0x5BEA2D, 0xEB); // _PI->WriteByte(0x5BEA2C, SPELLS_MAX_HERO);
        _PI->WriteLoHook(0x5BEA6E, hook_cmp_edi_70_0x5BEA6E);// _PI->WriteByte(0x5BEA70, SPELLS_MAX_HERO);
        _PI->WriteLoHook(0x5BEAAB, hook_cmp_esi_70_0x5BEAAB);// _PI->WriteByte(0x5BEAAD, SPELLS_MAX_HERO);
        _PI->WriteDword(0x5BEAFE, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteDword(0x5BEB2C, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteLoHook(0x5BEB46, hook_cmp_esi_70_0x5BEB46);// _PI->WriteByte(0x5BEB48, SPELLS_MAX_HERO);
        _PI->WriteLoHook(0x5BEB6F, hook_cmp_esi_70_0x5BEB6F);// _PI->WriteByte(0x5BEB71, SPELLS_MAX_HERO);
        _PI->WriteLoHook(0x5BEBB0, hook_cmp_esi_70_0x5BEBB0);// _PI->WriteByte(0x5BEBB2, SPELLS_MAX_HERO);
        _PI->WriteDword(0x5BEC05, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteLoHook(0x5BEC19, hook_cmp_esi_70_0x5BEC19);// _PI->WriteByte(0x5BEC1B, SPELLS_MAX_HERO);

        // Town setup
        _PI->WriteByte(0x5BEA05, 0x30); // Now we have the equivalent of std::bitset<SPELLS_MAX> on the stack
        _PI->WriteDword(0x5BEA12, 4); // Let's initialize it with zeroes

        int newBitsetOffset[] = { 0x5BEA67, 0x5BEAB2, 0x5BEACF, 0x5BEB50, 0x5BEB68, 0x5BEBB7, 0x5BEBD6, 0x5BEC23, 0x5BEC3B };
        for (std::size_t i = 0; i < sizeof(newBitsetOffset) / sizeof(int); ++i)
            _PI->WriteByte(newBitsetOffset[i], -0x30);

        _PI->WriteLoHook(0x5BEA24, expandMayAppearSpells);
        _PI->WriteLoHook(0x5BE518, expandMayAppearSpells); // Aurora Borealis
        _PI->WriteLoHook(0x5BE52D, expandMayAppearSpells); // Aurora Borealis
        _PI->WriteLoHook(0x5BEB15, expandMustAppearSpells);
        // ===============================================================

        // Conflux Grail
        _PI->WriteDword(0x5BE512, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteDword(0x5BE56E, SPELLS_MAX_HERO * sizeof(_Spell_));
        _PI->WriteDword(0x5D7464, SPELLS_MAX_HERO);

        // ===============================================================
        // ------------------ New Hero.spell[140] field ------------------
        // ---------------------------------------------------------------
        _PI->WriteCodePatch(0x4D8B72, (char*)"%n", 8);
        _PI->WriteCodePatch(0x4D8F7F, (char*)"%n", 8);
        // ===============================================================

        // ===============================================================
        // ------------- New Game.disabled_spells[140] field -------------
        // ---------------------------------------------------------------
        _PI->WriteByte(0x4C16ED, 4); // Pyramids
        _PI->WriteByte(0x4C254C, 4); // Titan's Lightning Bolt
        _PI->WriteByte(0x4C25F1, 4); // Titan's Lightning Bolt
        _PI->WriteByte(0x501297, 4); // Scholars
        _PI->WriteByte(0x5BEA55, 4); // Mage Guild
        _PI->WriteByte(0x7162FB, 4); // ERM
        // ===============================================================

        _PI->WriteDword(0x52AE1C, 0x3EA);

        // ===============================================================
        // -------------------------- Battle -----------------------------
        // ---------------------------------------------------------------
        // _PI->WriteByte(0x4963E9, ANIMS_NUM);
        // _PI->WriteByte(0x4965BD, ANIMS_NUM);
        _PI->WriteLoHook(0x502403, hook_cmp_edi_70_0x502403);// _PI->WriteByte(0x502405, SPELLS_MAX_HERO);
        _PI->WriteLoHook(0x50244F, hook_0050244F);// _PI->WriteByte(0x502451, SPELLS_MAX_HERO);
        _PI->WriteLoHook(0x5024CD, hook_cmp_edi_70_0x5024CD); // _PI->WriteByte(0x5024CF, SPELLS_MAX_HERO);
        _PI->WriteLoHook(0x502513, hook_cmp_edi_70_0x502513); //_PI->WriteByte(0x502515, SPELLS_MAX_HERO);
        _PI->WriteLoHook(0x502E60, hook_cmp_edi_70_0x502E60); //_PI->WriteByte(0x502E62, SPELLS_MAX_HERO);
        _PI->WriteLoHook(0x502EB5, hook_cmp_edi_70_0x502EB5); //_PI->WriteByte(0x502EB7, SPELLS_NUM);
     
        _PI->WriteLoHook(0x59EFDA, hook_0059EFDA);// _PI->WriteByte(0x59EFD9, SPELLS_NUM - 1 - ORIG_ADVSPELLS_NUM);
        _PI->WriteDword(0x59EFE4, (int)&spellIndirectTableA); 
        memset(spellIndirectTableA + SPL_FIRST_NEW - ORIG_ADVSPELLS_NUM,4, SPELLS_MAX_ALL - SPL_FIRST_NEW);

        _PI->WriteLoHook(0x5A064C, hook_005A064C);// _PI->WriteByte(0x5A064E, SPELLS_NUM - 1 - ORIG_ADVSPELLS_NUM);
        memset(spellIndirectTableB + SPL_FIRST_NEW - ORIG_ADVSPELLS_NUM, 17 , SPELLS_MAX_ALL - SPL_FIRST_NEW);
        _PI->WriteDword(0x5A0659, (int)&spellIndirectTableB);
        
        memset(spellIndirectTableC + SPL_FIRST_NEW - SPL_SHIELD, 32, SPELLS_MAX_ALL - SPL_FIRST_NEW );
        _PI->WriteLoHook(0x004446EB, hook_004446EB);
        // _PI->WriteByte(0x4446EA, SPELLS_NUM - 1 - SPL_SHIELD);
        _PI->WriteDword(0x4446FD, (int)&spellIndirectTableC);

        memset(spellIndirectTableD + SPL_FIRST_NEW - SPL_WEAKNESS, 9, SPELLS_MAX_ALL - SPL_FIRST_NEW);
        _PI->WriteLoHook(0x444263, hook_00444263);// _PI->WriteByte(0x444262, SPELLS_NUM - 1 - SPL_WEAKNESS);
        _PI->WriteDword(0x44427A, (int)&spellIndirectTableD);

        memset(spellIndirectTableE + SPL_FIRST_NEW - SPL_LIGHTNING_BOLT, 16 , SPELLS_MAX_ALL - SPL_FIRST_NEW);
        _PI->WriteLoHook(0x44A25A, hook_0044A25A);// _PI->WriteByte(0x44A259, SPELLS_NUM - 1 - SPL_LIGHTNING_BOLT);
        _PI->WriteDword(0x44A264, (int)&spellIndirectTableE);
        
        memset(spellIndirectTableF + SPL_FIRST_NEW - SPL_EARTHQUAKE, 9, SPELLS_MAX_ALL - SPL_FIRST_NEW );
        _PI->WriteLoHook(0x43B789, hook_0043B789);// _PI->WriteByte(0x43B788, SPELLS_NUM - 1 - SPL_EARTHQUAKE);
        _PI->WriteDword(0x43B793, (int)&spellIndirectTableF);
        
        _PI->WriteHiHook(0x43B2E0, SPLICE_, EXTENDED_, THISCALL_, get_enchantment_function);
        _PI->WriteLoHook(0x43B797, battleAISpellWeighting);
        // ===============================================================

        // ===============================================================
        // ----- New CombatManager.active_spell_duration[162] field ------
        // ---------------------------------------------------------------
        _PI->WriteDword(0x43787A, 162 );
        _PI->WriteCodePatch(0x437880, (char*)"%n", 19);
        _PI->WriteDword(0x43D314, 162 );
        _PI->WriteDword(0x43E3DF, SPELLS_WITH_DURATION);
        _PI->WriteLoHook(0x443F61, hook_cmp_edi_81_0x443F61); // _PI->WriteByte(0x443F63, SPELLS_NUM);
        _PI->WriteLoHook(0x446F01, hook_cmp_edi_81_0x446F01); // _PI->WriteByte(0x446F03, SPELLS_NUM);
        _PI->WriteLoHook(0x5A1905, hook_cmp_esi_81_0x5A1905); //_PI->WriteByte(0x5A1907, SPELLS_NUM);
        _PI->WriteLoHook(0x5A1993, hook_cmp_edi_81_0x5A1993);  // _PI->WriteByte(0x5A1995, SPELLS_NUM);
        _PI->WriteLoHook(0x5A84C6, hook_cmp_eax_81_0x5A84C6); // _PI->WriteByte(0x5A84C8, SPELLS_NUM);
        _PI->WriteLoHook(0x5A852D, hook_cmp_eax_81_0x5A852D); // _PI->WriteByte(0x5A852F, SPELLS_NUM);
        _PI->WriteCodePatch(0x4446D2, (char*)"%n", 7);
        _PI->WriteJmp(0x444694, 0x4450C2);
        // ===============================================================

        // Fear, Poison, Disease, Age
        _PI->WriteLoHook(0x444701, applySpell);
        _PI->WriteLoHook(0x44427E, resetSpell);
        //// Poison every round, no retaliations for Fear at Expert
        _PI->WriteLoHook(0x446F11, newRoundSpellSettings);
        // Creature's cast
        _PI->WriteLoHook(0x468CCF, creatureCast);
        // Zombie's Disease on Magic Plains
        //_PI->WriteLoHook(0x5A01E5, zombieDiseaseOnMagicPlains);
        //// Allow to dispel Poison
        //_PI->WriteHexPatch(0x5A18FA, "90 90");
        //_PI->WriteHexPatch(0x5A1988, "90 90");

        // Allow to cure new spells
        _PI->WriteLoHook(0x4462FD, cureNewSpells);

        //// Death Cloud, Incineration
        _PI->WriteLoHook(0x5A065D, castBattleSpell);

        //// Death Blow
        //_PI->WriteLoHook(0x4435A9, DeathBlow);

        //// Drain Life
        //_PI->WriteLoHook(0x440903, DrainLife);
        //_PI->WriteLoHook(0x44096F, calcDrainLifeHeal);

        // Summon Sprite, Summon Magic Elemental, Summon Firebird
        _PI->WriteLoHook(0x59F889, checkSummonedCreaturesType);
        _PI->WriteLoHook(0x5A065D, SummonCreatures);
        _PI->WriteLoHook(0x5A74D6, skipNonElementals);
        _PI->WriteLoHook(0x5A7516, setSummonedCreaturesNumber);
        _PI->WriteHiHook(0x5A96B0, SPLICE_, DIRECT_, THISCALL_, CanAISummonCreature);
        _PI->WriteHiHook(0x5A9670, SPLICE_, DIRECT_, FASTCALL_, getCreatureTypeToSummon);

        // Adventure Spells
        _PI->WriteLoHook(0x41C663, castAdventureSpell);
        //_PI->WriteLoHook(0x4915B7, eyeOfTheMagi);
        _PI->WriteHiHook(0x50CEA0, SPLICE_, EXTENDED_, THISCALL_, setMouseCursor);

        // Stack's Magic Vulnerability
        _PI->WriteLoHook(0x44A268, getStackMagicVulnerability);

        
        // Artifacts (incl. Spell Scrolls)
        if (!Emerald_Link)
            _PI->WriteHiHook(0x4D9840, SPLICE_, DIRECT_, THISCALL_, UpdateSpellsFromArtifacts);
        

        //_PI->WriteHiHook(0x4425A0, SPLICE_, DIRECT_, THISCALL_, CanUnitAttack);
        //_PI->WriteHiHook(0x4C7CA0, SPLICE_, EXTENDED_, THISCALL_, beforeNewDayStart);
        //_PI->WriteHiHook(0x4C2110, SPLICE_, EXTENDED_, THISCALL_, beforeNewGameStart);

        // ===============================================================
        // ---------------------------- Fear -----------------------------
        // ---------------------------------------------------------------
        // Set cursor for human players
        //_PI->WriteHiHook(0x475DC0, SPLICE_, EXTENDED_, THISCALL_, setCursorForFearSpell);
        // Not allowing to attack under Fear
        //_PI->WriteLoHook(0x422088, skipMeleeAttackUnderFear);
        //_PI->WriteLoHook(0x41F25C, skipShootingUnderFear);
        // Getting defense modifier (common for Blind, Paralyze, Fear)
        //_PI->WriteHiHook(0x4422B0, SPLICE_, DIRECT_, THISCALL_, GetEffectiveDefenseAgainst);
        //_PI->WriteHiHook(0x4438B0, SPLICE_, DIRECT_, THISCALL_, GetDefenseModifier);
        // ===============================================================

        // ===============================================================
        // ---------------------- Save & Load Game -----------------------
        // ---------------------------------------------------------------
        _PI->WriteLoHook(0x4D7D44, loadHeroAdvInfoEx);
        _PI->WriteLoHook(0x4D83B3, saveHeroAdvInfoEx);

        /*
        _PI->WriteDword(0x41814E, (int&)gmnExt); // .GM%d
        _PI->WriteDword(0x418139, (int&)cgmExt); // .CGM
        _PI->WriteDword(0x4BEC71, (int&)gmnStr); // %s.GM%d
        _PI->WriteDword(0x4BEC27, (int&)cgmStr); // CGM
        _PI->WriteDword(0x4BEC3B, (int&)tgmStr); // TGM
        _PI->WriteDword(0x6834AC, (int&)gmnMsk); // *.gm?
        _PI->WriteDword(0x6834B0, (int&)cgmMsk); // *.cgm
        _PI->WriteDword(0x6834B8, (int&)tgmMsk); // *.tgm
        */
        // ===============================================================

        /*
        // LOD
        _PI->WriteDword(0x69E5B8, 3);
        _PI->WriteDword(0x69E5B8 + 4, (int)&lodType);
        _PI->WriteLoHook(0x55A57D, loadLod);

        // SND
        _PI->WriteLoHook(0x598979, loadSnd);
        */

        // _PI->WriteLoHook(0x4ADF71, initSpellMods);

        /*
        int speedModJmpAddr[] = { 0x441CE3, 0x441E06, 0x44857E, 0x4485BC, 0x4485FC, 0x44867A, 0x448A05 };
        for (std::size_t i = 0; i < sizeof(speedModJmpAddr) / sizeof(int); ++i)
            _PI->WriteHexPatch(speedModJmpAddr[i], "90 90");
        */

        // fix for erm
        Z_Grandmaster_Magic->WriteDword(0x007459A7 + 3, 0x3EA);
        // Z_Grandmaster_Magic->WriteByte(0x00745988 + 3, 140);
        // Z_Grandmaster_Magic->WriteByte(0x0074598C + 0, 0x76);
        Z_Grandmaster_Magic->WriteLoHook(0x00745988, hook_00745988);
        Z_Grandmaster_Magic->WriteDword(0x007459C9 + 3, 0x3EA);
        Z_Grandmaster_Magic->WriteByte(0x007459DD + 6, 2);
        elementals_setup();

        Z_Grandmaster_Magic->WriteLoHook(0x005A7516,hook_005A7516);

        Z_Grandmaster_Magic->WriteLoHook(0x0075F360, hook_BM_G);
        Z_Grandmaster_Magic->WriteLoHook(0x004446A6,hook_004446A6);

        Z_Grandmaster_Magic->WriteLoHook(0x0059EFD4, spellChosen_0059EFD4);
        /*
        _extended_spell_info_::read();
        for (int i = SPL_FIRST_NEW; i < SPELLS_NUM;++i) {
            if (!o_Spell[i].name) o_Spell[i].name = null_text;
            if (!o_Spell[i].shortName) o_Spell[i].shortName = null_text;
            if (!o_Spell[i].description[0]) o_Spell[i].description[0] = null_text;
            if (!o_Spell[i].description[1]) o_Spell[i].description[1] = null_text;
            if (!o_Spell[i].description[2]) o_Spell[i].description[2] = null_text;
            if (!o_Spell[i].description[3]) o_Spell[i].description[3] = null_text;
            if (!o_Spell[i].soundName) o_Spell[i].soundName = default_sound;
            if (! *(long*) &o_Spell[i].flags) 
            {
                o_Spell[i].flags.map_spell = 1;
                o_Spell[i].flags.battlefield_spell = 1;
            }
            if (!*(long*)&o_Spell[i].school) o_Spell[i].school = h3::H3Spell::eSchool(15);
            if (!o_Spell[i].level) o_Spell[i].level = 1;
        }
        _extended_spell_info_::read();
        */

        Z_Grandmaster_Magic->WriteDword(0x004C2445+1,0x0000C7F6); //force original function (0x004CEC40)
        Z_Grandmaster_Magic->WriteLoHook(0x0073382D, hook_0073382D);
    }

    applied = true;
}
