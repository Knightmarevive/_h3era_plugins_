#include "pch.h"
// #include "era.h"
#include "patcher_x86.hpp"
#include <cstdio>

#define PINSTANCE_MAIN "majaczek_bug_fixes"
#define PEvent Era::TEvent*

extern Patcher* globalPatcher;
extern PatcherInstance* PI;

extern bool KnightmareKingdoms;

// #include "call_convention.hpp"

//#include "H3API/lib/H3API.hpp"
#include "../../__include__/H3API/single_header/H3API.hpp"
#include "heroes.h"

#define PERMSIZE 256000
BYTE  PostERM_buffer[PERMSIZE];

void install_PostERM_buffer() {

    //PI->WriteDword(0x0073E1C3 + 2, (int)PostERM_buffer);
    //PI->WriteDword(0x0073E1CD + 1, (int)PostERM_buffer);
    // // PI->WriteDword(0x00749A0D + 1, (int)PostERM_buffer);
    // PI->WriteDword(0x00740A47 + 1, (int)PostERM_buffer);

    PI->WriteDword(0x00A4AB0C, (int)PostERM_buffer);
    PI->WriteDword(0x0073E1D2 + 2, PERMSIZE);
}

_LHF_(hook_0074A0CA) {
    if(*(int*)0x00A4AB0C != (int)PostERM_buffer)
        install_PostERM_buffer();

    return EXEC_DEFAULT;
    if (!c->edx || c->edx == 0x91F6C8) {
        install_PostERM_buffer();

        /*
        int& PERM_v32 = *(int*)(c->ebp - 0x36C);
        int var_k = *(int*)(c->ebp - 0x364);
        install_PostERM_buffer();

        // PERM_v32 = (int)0x0091F6C8;
        // c->edx = var_k + PERM_v32;
       
        PERM_v32 = (int)PostERM_buffer;
        c->edx = var_k + (int)PostERM_buffer;
        */ 
    }

    return EXEC_DEFAULT;
}
void H3HeapFix_Apply() {
	// // PI->WriteDword(0x0073E1D2 + 2, PERMSIZE);
    // PI->WriteLoHook(0x0074A0CA, hook_0074A0CA);
}