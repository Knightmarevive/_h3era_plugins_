#pragma warning(disable : 4996)

#define _H3API_PLUGINS_
#define _H3API_PATCHER_X86_
#include "../__include__/H3API/single_header/H3API.hpp"

#include "framework.h"
#include "era.h"

#define PINSTANCE_MAIN "Heroine"

extern Patcher* globalPatcher;
extern PatcherInstance* Z_Heroine;

#include "hero_limits.h"

#include "NPC.h"
extern "C" __declspec(dllexport) 
    void HeroInfo_Dlg_Show(int color, int hero_id, int RMB);
namespace Heroine {
    extern h3::H3HeroSpecialty HSpecBack[MaxHeroCount];
    extern h3::H3HeroInfo HeroInfoBack[MaxHeroCount];
    extern h3::H3HeroInfo HeroInfo[MaxHeroCount];
    extern BOOL8 isHeroEnabled[MaxHeroCount];

    /** @brief [21620] */
    extern h3::H3Hero heroes[MaxHeroCount];
    /** @brief [4DF18] */
    extern INT8 heroOwner[MaxHeroCount];
    /** @brief [4DFB4] */
    extern h3::H3Bitfield heroMayBeHiredBy[MaxHeroCount];
    
    extern struct _HeroCombatDef_ {
        char* def_name;
        DWORD cast_x, cast_y, cast_cadre;
    } CombatHeroDef[MaxHeroClassCount];

    extern struct _HeroType_ {
        DWORD Belong2Town;
        char* TypeName;
        DWORD Agression;
        char PSkillStart[4];
        char ProbPSkillToLvl9[4];
        char ProbPSkillAfterLvl10[4];
        char ProbSSkill[28];
        char ProbInTown[9];
        char field_3D[3];
    } HeroTypes[MaxHeroClassCount];
    char HeroType_ProbInTown[MaxHeroClassCount][MaxHeroFactionCount];

    extern struct _PicsBack_ {
        BYTE* HPSLoaded;
        BYTE* HPLLoaded;
    } PicsBack[MaxHeroCount];
}

signed int __stdcall GameMgr_GetRandomHero_hook(
    HiHook* h,
    h3::H3Game* ptr,
    unsigned int player,
    signed int a3,
    char a4,
    signed int HeroType) {
    int v6; // esi
    char* v7; // eax
    int v8; // edx
    DWORD* v9; // ecx
    int* p_Type; // ebx
    int* HeroMayBeHiredBy; // edi
    unsigned int v12; // eax
    int* v13; // eax
    int v14; // ecx
    int v15; // edx
    int v16; // esi
    int i; // eax
    signed int v18; // ecx
    int v19; // ebx
    Heroine::_HeroType_* v20; // eax
    int v21; // edx
    DWORD* v22; // ecx
    Heroine::_HeroType_* v23; // eax
    int v24; // edx
    DWORD* v25; // ecx
    int v26; // ebx
    int* v27; // eax
    int v28; // edx
    int v29; // ecx
    int v30; // edi
    int v31; // eax
    DWORD* v32; // ecx
    int* v33; // edi
    signed int v34; // esi
    unsigned int v35; // eax
    int v37; // [esp+0h] [ebp-A8h]
    int v38; // [esp+4h] [ebp-A4h]
    int v39; // [esp+8h] [ebp-A0h]
    int High[MaxHeroClassCount]; // [esp+Ch] [ebp-9Ch] BYREF
    DWORD v41[MaxHeroClassCount]; // [esp+54h] [ebp-54h] BYREF
    int v42; // [esp+94h] [ebp-14h]
    int v43; // [esp+98h] [ebp-10h]
    int v44; // [esp+9Ch] [ebp-Ch]
    h3::H3Game* v45; // [esp+A0h] [ebp-8h]
    int v46; // [esp+A4h] [ebp-4h]
    int* a4a; // [esp+B8h] [ebp+10h]
    int HeroTypea; // [esp+BCh] [ebp+14h]


    v6 = 0;
    v45 = ptr;
    v44 = 0;
    if ((player & 0x80000000) != 0)
        v46 = -1;
    else
        v46 = ptr->playersInfo.townType[player];
    memset(High, 0, sizeof(High));

    char Probablities[MaxHeroFactionCount] = {};
    // memcpy(Probablities, &Heroine::HeroTypes->ProbInTown[v46], 9);
    memcpy(Probablities, Heroine::HeroType_ProbInTown[v46],
        MaxHeroFactionCount);

    v9 = v41;
    // v7 = &HeroTypesTablePo->ProbInTown[v46];
    // v7 = &Heroine::HeroTypes->ProbInTown[v46];
    v7 = Probablities;
    v8 = MaxHeroClassCount;
    do
    {
        *v9 = *v7;
        v7 += 64;
        ++v9;
        --v8;
    } while (v8);
    p_Type = &Heroine::heroes->hero_class;// &ptr->Heroes[0].Type;
    HeroMayBeHiredBy = (int*)Heroine::heroMayBeHiredBy; // v45->HeroMayBeHiredBy;
    do
    {
        //if (v45->HeroOwner[v6] == -1)
        if (Heroine::heroOwner[v6] == -1)
        {
            v12 = player;
            if (player == -1)
                goto LABEL_12;
            if (player >= 8)
            {
                h3::H3Messagebox::Show("Random Hero is Bugged");
                // Player_ID_Error(v37, v38, v39);
                v12 = player;
            }
            if (((1 << (v12 & 0x1F)) & HeroMayBeHiredBy[v12 >> 5]) != 0)
            {
            LABEL_12:
                if (Heroine::isHeroEnabled[v6]) {
                    v13 = &High[*p_Type];
                    v14 = *v13 + 1;
                    ++v44;
                    *v13 = v14;
                }
            }
        }
        ++v6;
        ++HeroMayBeHiredBy;
        p_Type = (int*)((int)p_Type + 1170);
    } while (v6 < MaxHeroCount);
    v15 = v44;
    v16 = 0;
    if (!v44)
        return -1;
    for (i = 0; i < MaxHeroClassCount; ++i)
    {
        if (!High[i])
            v41[i] = 0;
    }
    v18 = HeroType;
    int* LodType2LoadFromPo = *(int**)0x0069928C;
    if (h3::H3Game::Get()->mapKind >= 2 && *LodType2LoadFromPo == 2)
    {
        v19 = v46;
        if (v46 != 8 && High[16] + High[17] < v15)
        {
            if (HeroType != 16)
                v42 = 0;
            if (HeroType != 17)
                v43 = 0;
        }
    }
    else
    {
        v19 = v46;
    }
    if (a3 < MaxHeroClassCount && High[a3] < v15)
        v41[a3] = 0;
    if (a4)
    {
        v22 = v41;
        v20 = Heroine::HeroTypes;// HeroTypesTablePo;
        v21 = MaxHeroClassCount;
        do
        {
            if (v20->Belong2Town == v19)
                v16 += *v22;
            ++v20;
            ++v22;
            --v21;
        } while (v21);
        if (v16 > 0)
        {
            v25 = v41;
            v23 = Heroine::HeroTypes; // HeroTypesTablePo;
            v24 = MaxHeroClassCount;
            do
            {
                if (v23->Belong2Town != v19)
                    *v25 = 0;
                ++v23;
                ++v25;
                --v24;
            } while (v24);
        }
        v18 = HeroType;
    }
    if (v18 == MaxHeroClassCount || !v41[v18])
    {
        v28 = 0;
        v27 = (int*)v41;
        v29 = MaxHeroClassCount;
        do
        {
            v30 = *v27++;
            v28 += v30;
            --v29;
        } while (v29);
        v31 = FASTCALL_2(int, 0x0050C7C0, 1, v28);// Random(1, v28);
        v26 = 0;
        v32 = v41;
        do
        {
            v31 -= *v32;
            if (v31 <= 0)
                break;
            ++v26;
            ++v32;
        } while (v26 < MaxHeroClassCount);
    }
    else
    {
        v26 = v18;
    }
    HeroTypea = FASTCALL_2(int, 0x0050C7C0, 1, High[v26]);//Random(1, High[v26]);
    v34 = 0;
    v33 = (int*)Heroine::heroMayBeHiredBy; // v45->HeroMayBeHiredBy;

    //for (a4a = &v45->Heroes[0].Type; ; a4a = (a4a + 1170))
    for (a4a = &Heroine::heroes[0].hero_class; ; a4a = (int*)((int)a4a + 1170))
    {
        // if (v45->HeroOwner[v34] == -1)
        if (Heroine::heroOwner[v34] == -1 && Heroine::isHeroEnabled[v34])
        {
            v35 = player;
            if (player == -1)
                goto LABEL_56;
            if (player >= 8)
            {
                h3::H3Messagebox::Show("Random Hero is Bugged");
                // Player_ID_Error(v37, v38, v39);
                v35 = player;
            }
            if (((1 << (v35 & 0x1F)) & v33[v35 >> 5]) != 0)
            {
            LABEL_56:
                if (*a4a == v26 && !--HeroTypea)
                    break;
            }
        }
        ++v34;
        ++v33;
        if (v34 >= MaxHeroCount)
            return -1;
    }
    return v34;
}


int  __stdcall GameMgr_GetRandomHeroSimple_hook(HiHook* h,
    h3::H3Game* ptr, int town_type, unsigned int a3, int a4) {
    int v20 = town_type * 2; int v21 = v20 + 1;
    auto p_Type = &Heroine::heroes->hero_class;
    auto HeroMayBeHiredBy = (int*)Heroine::heroMayBeHiredBy;
    auto v4 = ptr; auto v18 = ptr; auto v5 = 0;
    unsigned int v8; int v9; int towna = 0;
    int v17[MaxHeroCount]; int* v19 = v17;
    int* v10; int v11 = 0, v12 = 0;

    do
    {
        if (Heroine::heroOwner[v5] == -1)
        {
            v8 = a3;
            if (a3 >= 8)
            {
                h3::H3Messagebox::Show("Random Hero is Bugged");
                // Player_ID_Error(v14, v15, v16);
                v8 = a3;
            }
            if (((1 << (v8 & 0x1F)) & HeroMayBeHiredBy[v8 >> 5]) != 0
                && (*p_Type == v20 || *p_Type == v21) && Heroine::isHeroEnabled[v5])
            {
                // v9 = (int) v19;
                *v19 = v5;
                ++towna;
                ++v19; // v19 = (char*)(v9 + 4);
            }
            v4 = v18;
        }
        ++v5;
        ++HeroMayBeHiredBy;
        p_Type = (INT32*)((int)p_Type + 1170);
    } while (v5 < MaxHeroCount);

    if (!towna)
    {
        v11 = 0;
        v19 = v17;
        v10 = (int*)Heroine::heroMayBeHiredBy;
        do
        {
            if (Heroine::heroOwner[v11] == -1)
            {
                if (a3 >= 8)
                {
                    h3::H3Messagebox::Show("Random Hero is Bugged");
                    /// Player_ID_Error(v14, v15, v16);
                    v4 = v18;
                }
                if (((1 << (a3 & 0x1F)) & v10[a3 >> 5]) != 0)
                {
                    // v12 = (int) v19;
                    *v19 = v11;
                    ++towna;
                    ++v19; // v19 = (char*) (v12 + 4);
                }
            }
            ++v11;
            ++v10;
        } while (v11 < MaxHeroCount);
    }

    // return *&v17[4 * Random(1, towna) - 4];
    return v17[FASTCALL_2(int, 0x0050C7C0, 0, towna - 1)];
}

int __stdcall ChangeTavernHeroes_hook(HiHook* h, 
    h3::H3Game* ptr, unsigned int player)
{
    h3::H3Player* v2; // edx
    int v3; // ebx
    h3::H3Game* v4; // edi
    unsigned int v5; // esi
    int RandomHero; // eax
    h3::H3Hero* RandomHero_ptr; // eax
    int v7; // ecx
    signed int v8; // eax
    __int16 CurWeek; // cx
    int i; // esi
    h3::H3Artifact* v11; // ebx
    h3::H3Hero* v12; // edi
    unsigned char v14; // al
    int v15; // ebx
    long double IntelligencePower; // fst7
    h3::H3Game* v17; // ecx
    int SlotToAdd; // [esp+Ch] [ebp-18h] BYREF
    int Mod; // [esp+10h] [ebp-14h]
    int v21; // [esp+14h] [ebp-10h]
    h3::H3Player* v22; // [esp+18h] [ebp-Ch]
    int thisa; // [esp+1Ch] [ebp-8h]
    int BackPackSlot; // [esp+20h] [ebp-4h]

    v5 = player;
    v4 = ptr;
    thisa = (int) ptr;
    v2 = &ptr->players[player];
    v3 = 0;
    v22 = v2;
    SlotToAdd = -1;
    Mod = -1;
    v21 = 0;
    while (1)
    {
        RandomHero = (&v2->tavernHeroL)[v3];// v2->Heroes[v3 + 8];
        if (RandomHero < 0)
        {
            v7 = *(&v2->ownerID + 11 - v3);
            // v8 = v7 >= 0 ? v4->Heroes[v7].Type : 18;
            v8 = v7 >= 0 ? Heroine::heroes[v7].hero_class : MaxHeroClassCount;
            if (v4->inTutorial && (CurWeek = v4->date.week, CurWeek <= 2u))
            {
                if (CurWeek == 1)
                {
                    for (i = 0; i < MaxHeroCount; ++i)
                    {
                        if (i == -1)
                            RandomHero_ptr = 0;
                        else
                            // RandomHero = &o_GameMgr->Heroes[i];
                            RandomHero_ptr = &Heroine::heroes[i];
                        if (RandomHero_ptr->hero_class == 1 && 
                            Heroine::heroOwner[i] == -1)
                            break;
                    }
                }
                else if (v3)
                {
                    for (i = 0; i < MaxHeroCount; ++i)
                    {
                        if (i == -1)
                            RandomHero_ptr = 0;
                        else
                            // RandomHero = &o_GameMgr->Heroes[i];
                            RandomHero_ptr = &Heroine::heroes[i];
                        if (RandomHero_ptr->hero_class == 7 && 
                            Heroine::heroOwner[i] == -1)
                            break;
                    }
                }
                else
                {
                    for (i = 0; i < MaxHeroCount; ++i)
                    {
                        if (i == -1)
                            RandomHero_ptr = 0;
                        else
                            // RandomHero = &o_GameMgr->Heroes[i];
                            RandomHero_ptr = &Heroine::heroes[i];
                        if (RandomHero_ptr->hero_class== 6 && 
                            Heroine::heroOwner[i] == -1)
                            break;
                    }
                }
            }
            else
            {
                RandomHero = GameMgr_GetRandomHero_hook(nullptr, v4, v5, v8, v3 == 0, MaxHeroClassCount);
                v2 = v22;
                i = RandomHero;
            }
            (&v2->tavernHeroL)[v3] = i;// v2->Heroes[v3 + 8] = i;
            if (i != -1)
            {
                // v4->HeroOwner[i] = 64;
                Heroine::heroOwner[i] = 64;
                BackPackSlot = 63;
                // v12 = &v4->Heroes[i];
                v12 = &Heroine::heroes[i];
                v11 = &v12->backpackArtifacts[63];
                do
                {
                    SlotToAdd = v11->id;
                    Mod = v11->subtype;
                    
                    /*
                    if (SlotToAdd != -1 && Hero_ArtAdd_OnDoll(v12, &SlotToAdd, 0xFFFFFFFF))
                        Hero_ArtRemove_inBackpack(v12, BackPackSlot);
                    */
                    if (SlotToAdd != -1 && THISCALL_3(char,0x004E2C70,v12, &SlotToAdd, 0xFFFFFFFF))
                        THISCALL_2(char, 0x004E2FC0, v12, BackPackSlot);

                    --v11;
                } while (BackPackSlot-- != 0);
                v14 = (UINT8) v12->primarySkill[3];
                if (v14 <= 250)
                {
                    v15 = v14;
                    /*
                    if (v14 <= 0)
                        v15 = 1;
                     */
                }
                else
                {
                    v15 = 250;
                }
                // IntelligencePower = Hero_GetIntelligencePower(v12);
                IntelligencePower = THISCALL_1(float , 0x004E4B20, v12);
                *&BackPackSlot = (10 * v15);
                v17 = (h3::H3Game*) thisa;
                v12->spellPoints = (IntelligencePower * *&BackPackSlot);
                
                // LOBYTE(RandomHero) = GameMgr_Reset_Hero_Army(v17, i, 0, 0);
                /* RandomHero = */ THISCALL_4(char, 0x004C93F0, v17, i, 0, 0);
                
                v3 = v21;
                v4 = (h3::H3Game*) thisa;
                v2 = v22;
            }
        }
        v21 = ++v3;
        if (v3 >= 2)
            break;
        v5 = player;
    }
    return RandomHero;
}

int __stdcall Tavern_Generate_One_New_Hero_hook( HiHook* h,
    h3::H3Main* MainStructure, int color_ID, int Left_or_Right_HeroInTavern)
{
    int v3; // eax
    int Type; // ecx
    h3::H3Player* v5; // ebx
    signed int RandomHero; // eax
    int v8; // esi
    // char v9; // al
    h3::H3Hero* hero_ptr;
    int v10; // ebx
    // char* player; // [esp+14h] [ebp+8h]
    float Left_or_Right_HeroInTaverna; // [esp+18h] [ebp+Ch]

    Type = MaxHeroClassCount;
    v5 = &MainStructure->players[color_ID];
    // v3 = *(&v5->Number + 11 - Left_or_Right_HeroInTavern);
    v3 = (&v5->tavernHeroR) [-Left_or_Right_HeroInTavern];

    if (v3 != -1)
        Type = Heroine::heroes[v3].hero_class;
    RandomHero = GameMgr_GetRandomHero_hook(nullptr, MainStructure, color_ID, Type, 0, MaxHeroClassCount);
    v8 = RandomHero;
    
    //v5->Heroes[Left_or_Right_HeroInTavern + 8] = RandomHero;
    (&v5->tavernHeroL)[Left_or_Right_HeroInTavern] = RandomHero;

    if (RandomHero != -1)
    {
        // MainStructure->HeroOwner[RandomHero] = 64;
        Heroine::heroOwner[RandomHero] = 64;

        // player = &MainStructure->field_0[1170 * RandomHero];
        //v9 = player[137881];
        hero_ptr = &Heroine::heroes[RandomHero];

        if (hero_ptr->primarySkill[3] <= 250)
        {
            v10 = hero_ptr->primarySkill[3];
            if (hero_ptr->primarySkill[3] <= 0)
                v10 = 1;
        }
        else
        {
            v10 = 250;
        }
        // Left_or_Right_HeroInTaverna = (10 * v10);
        
        // *(player + 68380) = (Hero_GetIntelligencePower((player + HeroesOffset)) * Left_or_Right_HeroInTaverna);
        hero_ptr->spellPoints = (THISCALL_1(float, 0x004E4B20, hero_ptr));
        
        // LOBYTE(RandomHero) = GameMgr_Reset_Hero_Army(MainStructure, v8, 0, 1);
        THISCALL_4(char, 0x004C93F0, MainStructure, v8, 0, 1);

    }
    return RandomHero;
}

int heroStartList[8][MaxHeroesPerTown]; // Castle town has 19 possible heroes, plus the random -1 value.
// h3::H3ScenarioPlayer* current_player = nullptr;
int current_player = 0;
int current_hero[8] = {};

struct _StartingHeroesList_
{
    _byte_ f_0[0x20];
    int currentHeroPosition; // 0x20
    _byte_ f_24[4];
    int heroCount; // 0x28
    int heroesList[16]; // 0x2C~...
};

extern bool campaignHeroSelectable;

int __stdcall FillNewHeroList(LoHook* h, HookContext* c)
{
    // auto v_player = *(h3::H3ScenarioPlayer**) (c->ebp - 0x18);
    auto z_player = *(int*)(c->ebp - 0x0c);
   
    _StartingHeroesList_* heroes = (_StartingHeroesList_*)c->eax;
    if(heroes->heroCount == 0) memset(&heroStartList[z_player][0], -1, sizeof(heroStartList) / 8);

    if (   !Heroine::isHeroEnabled[c->edx]
//        || (!campaignHeroSelectable && (Heroine::HeroInfo[c->edx].campaignHero || Heroine::HeroInfo[c->edx].expansionHero))
           || (!campaignHeroSelectable && (c->edx>=144 && c->edx<=155))
        )
    {
        c->edx = *(int*)(c->eax + 0x28);
        c->return_address = 0x00583DA0;
        return NO_EXEC_DEFAULT;
    }

    if (heroes->heroCount < 16)
        heroes->heroesList[heroes->heroCount] = c->edx;

    heroStartList[z_player][heroes->heroCount] = c->edx; // store the hero id to our list
    return NO_EXEC_DEFAULT;
}

int __stdcall ReadFromNewHeroListPortrait(LoHook* h, HookContext* c)
{
    int &a2 = *(int*)(c->ebp + 8);
    int heroPosition = c->eax; // int z_player = ;
    c->edx = heroStartList[a2][heroPosition];
    *(int*)(c->ebp + 8) = c->edx; // overwritten instruction
    return NO_EXEC_DEFAULT;
}
int __stdcall ReadFromNewHeroListName(LoHook* h, HookContext* c)
{
    int& a2 = *(int*)(c->ebp + 8);
    int heroPosition = c->ecx;
    int heroId = heroStartList[a2][heroPosition];
    if (heroId != -1)
        c->eax = heroId;
    return EXEC_DEFAULT;
}
int __stdcall ReadFromNewHeroListGenerate(LoHook* h, HookContext* c)
{
    int heroPosition = *(int*)(c->eax - 0x50);
    c->ecx = heroStartList[c->esi][heroPosition];
    return EXEC_DEFAULT;
}
int __stdcall ReadFromNewHeroListRightClick(LoHook* h, HookContext* c)
{
    auto dlg = *(h3::H3Dlg**)(c->ebp - 0x74);
    // auto tmp = *(int*)(0x64+(int)dlg);
    auto v19 = (h3::H3ScenarioPlayer*)c->eax;

    int heroPosition = c->ecx;
    int heroId = heroStartList[v19->player2][heroPosition];
    c->edi = heroId;
    return EXEC_DEFAULT;
}

struct ChooseHero_Dlg : public h3::H3Dlg {
    int plr = -1; int choice = -1;

    ChooseHero_Dlg(int _plr) : H3Dlg(720, 480, -1, -1, 0, 0),
        plr(_plr), choice(-1)
    {
        AddBackground(0, 0, widthDlg, heightDlg, true, false, plr, false);

        int j = 0;
        for (int i = 0; i < MaxHeroesPerTown; ++i) {
            if (heroStartList[plr][i] >= 0) {
                CreatePcx(32 + 86 * (j % 8), 32 + 97 * (j / 8), 3000 + i,
                    Heroine::HeroInfo[heroStartList[plr][i]].largePortrait);
                LPCSTR text = Heroine::HeroInfo[heroStartList[plr][i]].name;
                CreateText(32 + 86 * (j % 8), 96 + 97 * (j / 8), 
                   64, 32, text,"MedFont.fnt", TxtHeroColour, 4000 + i );
            }
            ++j;
        }
    }

    BOOL DialogProc(h3::H3Msg& msg) override;
};

BOOL ChooseHero_Dlg::DialogProc(h3::H3Msg& msg) {
    if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
        // auto target = msg.GetDlg()->ItemAtPosition(msg);
        if (msg.itemId >= 3000 && msg.itemId < 4000) {
            //int hero_id = heroStartList[msg.itemId - 3000];
            //auto start_hero = (int*) 0x0069FB80;
            //start_hero[current_player] = msg.itemId - 3000;
            choice = msg.itemId - 3000;

            auto z_parent = (ChooseHero_Dlg*)msg.parentDlg;
            z_parent->Stop();
        }
    }

    if (msg.subtype == h3::eMsgSubtype::RBUTTON_DOWN) {
        // auto target = msg.GetDlg()->ItemAtPosition(msg);
        if (msg.itemId >= 3000 && msg.itemId < 4000) {
            {
                int hero_id = heroStartList[plr][msg.itemId - 3000];
                HeroInfo_Dlg_Show(plr, hero_id, 1);
            }
        }
    }
    return 0;
}


#define NETMSG_ID 0x5CE5E1EC
struct H3SelectScenario_packet {
    int player_id;
    int town;
    int hero;
};

h3::H3ScenarioPlayer* find_player_with_id(int player_id) {
    auto dlg = h3::H3SelectScenarioDialog::Get();
    char slot[8] = {}; int humans = 0;
    auto plr = dlg->mapPlayersHuman;
    auto end1 = dlg->mapPlayersHuman + 8;
    auto end2 = dlg->mapPlayersHuman + 16;
    while (plr != end1 && plr->player2 >= 0) {
        if (plr->player2 == player_id)
            return plr;
        slot[plr->player2] = 1;
        ++plr; ++humans;
    }

    int i = 0; int j = 0; plr = end1;
    while (plr != end2) {
        if (slot[i]) { ++i; continue; }
        if (j == player_id) return plr;
        ++plr; ++i; ++j;
    }

    return nullptr;

}

_LHF_(hook_00588AC3) {
	if (c->eax != NETMSG_ID)
		return EXEC_DEFAULT;

	auto dlg = h3::H3SelectScenarioDialog::Get();
	if (dlg) {
		auto packet = (h3::H3NetworkData<H3SelectScenario_packet>*) c->edi;

        /*
		auto plr = dlg->mapPlayersHuman;
		auto end = dlg->mapPlayersHuman + 16;
		while (plr->player2 != packet->data.player_id
			&& plr != end) ++plr;

		if (plr == end)
			return EXEC_DEFAULT;
        */
        auto plr = find_player_with_id(packet->data.player_id);
        if (!plr) return EXEC_DEFAULT;

		plr->gameVersion = packet->data.hero; 
		plr->town = packet->data.town;

		if(packet->data.hero<0)
			THISCALL_1(void, 0x00583B60, dlg); // FillStartingHeroesList

		if (dlg->_f_37F) THISCALL_4(void, 0x0058D7E0, dlg, packet->data.player_id, 1, -1);
		else THISCALL_3(void, 0x005857D0, dlg, -1, 1);
	}

	c->return_address = 0x00589305;
	return SKIP_DEFAULT;
}


_LHF_(hook_0058692A) {
    auto me = (h3::H3Msg*)c->esi;
    auto dlg = me ? (h3::H3SelectScenarioDialog*) me->GetDlg() : nullptr;
    // auto master = (h3::H3Dlg*) c->ebx;

    int a2 = *(int*)(c->ebp + 0x08);
    int a3 = *(int*)(c->ebp + 0x0c);
    char a4 = *(char*)(c->ebp + 0x10);

    if (c->ecx >= 0x6C && c->ecx <= 0x73) {
        current_player = c->ecx - 0x6C;
    }
    if (c->ecx >= 0x74 && c->ecx <= 0x7B) {
        current_player = c->ecx - 0x74;
    }
    if (c->ecx >= 0x7C && c->ecx <= 0x83) {
        current_player = c->ecx - 0x7C;
    }
    if (c->ecx >= 0x84 && c->ecx <= 0x8B) {
        current_player = c->ecx - 0x84;
    }
    if (c->ecx >= 0x9c && c->ecx <= 0xa2) {
        current_player = c->ecx - 0x9c;
        // memset(heroStartList, -1, sizeof(heroStartList));
    }
    if (c->ecx >= 0x0107 && c->ecx <= 0x010e) {
        // current_player = c->ecx - 0x0107;
    }

    if (c->ecx >= 0xFF && c->ecx <= 0x106) {
        int player_id = c->ecx - 0xff;
        current_player = player_id;
        // h3::H3Messagebox::Show("Hero Clicked");
        if (true) {
            /*
            auto plr = (h3::H3ScenarioPlayer*)(0x1064 + (char*)dlg);
            auto end = dlg->mapPlayersHuman + 16;
            while (plr->player2 != player_id
                && plr != end) ++plr;
            if (plr == end)
                return EXEC_DEFAULT;
            */
            
            auto plr = find_player_with_id(player_id);
            if (!plr) return EXEC_DEFAULT;

            if (plr->town < 0) return EXEC_DEFAULT;

            ChooseHero_Dlg window(player_id);
            window.CreateOKButton(); window.Start();
            if (window.choice >= 0
                && heroStartList[player_id][current_hero[player_id]]
                != heroStartList[player_id][window.choice]) {
                for (int i = 0; i < 16; ++i)
                    if (heroStartList[dlg->mapPlayersHuman[i].player2]
                        [dlg->mapPlayersHuman[i].gameVersion]
                        == heroStartList[player_id][window.choice]) {

                        h3::H3Messagebox("Hero in use.");
                        return EXEC_DEFAULT;
                    }
            }
            int result = current_hero[player_id] = window.choice;


            plr->gameVersion = result;
            dlg->mapInfo.playerAttributes[player_id].mainHeroId =
                heroStartList[player_id][result];
            // THISCALL_3(void, 0x0058B1F0, dlg, -1, plr);
            // THISCALL_3(void, 0x0058B1F0, dlg, 0, plr);

            // dlg->RedrawItem(0x0107 + player_id);
            auto wnd_mgr = h3::H3WindowManager::Get();

            auto flag_item = dlg->GetH3DlgItem(0x0107 + player_id);
            auto my_item = dlg->GetH3DlgItem(0x016A + player_id);

            // THISCALL_1(void, 0x005FEDC0, flag_item);
            // FASTCALL_1(void, 0x005FEEF0, flag_item); // flag_item->

            // FASTCALL_1(void, 0x00456620, flag_item);

            // h3::H3Msg flag_msg(*me);  flag_msg.itemId = (0x0107 + player_id);


            if (h3::H3Network::Multiplayer()) {
                H3SelectScenario_packet packet = { player_id, plr->town, plr->gameVersion };
                auto msg = h3::H3NetworkData<H3SelectScenario_packet>(-1, NETMSG_ID, packet);

                FASTCALL_4(INT32, 0x5549E0, &msg, 127, false, 1);// data.SendData(0);
            }

            if (dlg->_f_37F)
            {
                /*
                auto pcx2 = (h3::H3LoadedPcx*) Heroine::PicsBack
                    [heroStartList[player_id][result]].HPSLoaded;
                pcx2->DrawToPcx16(wnd_mgr->screenPcx16,
                    my_item->GetAbsoluteX(), my_item->GetAbsoluteY(),
                        false);

                char* result_name = result < 0 ? "Random" : Heroine::HeroInfo[heroStartList[player_id][result]].name;

                auto col = wnd_mgr->screenPcx16->GetPixel(my_item->GetAbsoluteX() - 7, my_item->GetAbsoluteY() + 36);
                wnd_mgr->screenPcx16->FillRectangle(my_item->GetAbsoluteX() - 11, my_item->GetAbsoluteY() + 34,
                    my_item->GetWidth() + 22, 10, col);

                wnd_mgr->screenPcx16->TextDraw(P_TinyFont, result_name,
                    my_item->GetAbsoluteX() - 11, my_item->GetAbsoluteY() + 33,
                    my_item->GetWidth() + 22, 12);
                */
                THISCALL_4(void, 0x0058D7E0, dlg, player_id, 1, -1);
            }
            else {
                THISCALL_3(void, 0x005857D0, dlg, -1, 1);
            }

            wnd_mgr->H3Redraw(my_item->GetAbsoluteX(), my_item->GetAbsoluteY(), 64, 64);

            /*
            flag_msg.command = h3::NH3Messages::eMessageCommand::ITEM_COMMAND;
            flag_msg.subtype = h3::NH3Messages::eMessageMsgSubtype::REPAINT;
            THISCALL_2(signed, 0x005FEAB0, flag_item, &flag_msg);
            */
            /*
            THISCALL_3(char, 0x005FFCA0, dlg, 
                flag_item->GetAbsoluteX() + 2,
                flag_item->GetAbsoluteY() + 2);
            */
            // h3::H3Main::Get()->mapInfo.playerAttributes[player_id].mainHeroId
            // THISCALL_3(char, 0x005857D0, dlg, -1, 1);
            /*
            int v81 = *(int*)(0x370 + (int)dlg);
            int a2 = *(int*)(c->ebp + 8);
            auto v8 = *(int*)(a2 + 8);
            int v83 = v8 - 142;

            THISCALL_3(char, 0x005857D0, dlg, v81 + v83, 1);
            */
            
            /*
            h3::H3Msg z_msg;
            z_msg.SetCommand(h3::eMsgCommand::MOUSE_OVER,
                h3::eMsgSubtype::NONE, plr_flag_id,
                h3::eMsgFlag::NONE, 0, 0, 0, (INT32) dlg);
            THISCALL_2(VOID, 0x5FF3A0, dlg, &z_msg);
            */

            // THISCALL_4(int, 0x0058692A, dlg);
        }
    }

    return EXEC_DEFAULT;
}

_LHF_(hook_0058260B) {
    int player_color = *(int*)(c->ebp + 8) - 362;
    auto Game = h3::H3Game::Get();
    // auto v7 = Game->playersInfo.heroMaybe[player_color];
    int hero_id = THISCALL_2(signed int, 0x0058D3B0, c->ecx, player_color);
    // int hero_id = heroStartList[player_color][i];
    if (hero_id < 0) return EXEC_DEFAULT;

    HeroInfo_Dlg_Show(player_color, hero_id, 1);

    c->return_address = 0x00582D52;
    return SKIP_DEFAULT;
}

_LHF_(hook_00583C52) {
    // memset(&heroStartList[current_player][0], -1, sizeof(heroStartList) / 8);

    int town_id = c->eax;
    *(int*)(c->ebp - 4) = 2 * town_id;
    c->ebx = 2 * town_id + 1;

    c->return_address = 0x00583CD7;
    return SKIP_DEFAULT;
}

/*
_LHF_(hook_0058B25A) {
    // auto& something = *(int*)(c->edi + 4 * c->edx + 0x70);
    if (-2 == *(int*)(c->ebp+8)) {
        c->ecx = current_hero;

        c->return_address = 0x0058B363; //  0x0058B263;
        return SKIP_DEFAULT;

        return EXEC_DEFAULT;
    }

    return EXEC_DEFAULT;
}
*/

int __stdcall Y_NewScenarioDlg_Proc_hook(HiHook* hook, h3::H3SelectScenarioDialog* this_, h3::H3Msg* msg) {
    if (msg->itemId >= 0x8e && msg->itemId <= 0x9f)
        return THISCALL_2(int, hook->GetDefaultFunc(), this_, msg);

    if (msg->itemId > 0x6b && msg->itemId <= 0x010e + 0x6b
        // && msg->command == h3::eMsgCommand::MOUSE_BUTTON
        )
        return THISCALL_2(int, hook->GetOriginalFunc(), this_, msg);

    return THISCALL_2(int, hook->GetDefaultFunc(), this_, msg);
}

/*
_LHF_(hook_00588469) {
    int msg_type    = *(int*)(c->ebx + 0);
    int msg_subtype = *(int*)(c->ebx + 4);
    if (msg_subtype == 13 || msg_subtype == 14)
        return SKIP_DEFAULT;

    c->return_address = 0x0058858A; 
    return SKIP_DEFAULT;
}
*/

extern bool newHeroSelectionDialog;

void InstallStartingHeroes() {

    memset(heroStartList, -1, sizeof(heroStartList));

    Z_Heroine->WriteByte(0x583DA5 + 2, MaxHeroesPerTown); // extend available hero list to 19 heroes instead of 16
    Z_Heroine->WriteLoHook(0x583D95, FillNewHeroList); // custom hero list is filled instead of original one
    Z_Heroine->WriteLoHook(0x58D454, ReadFromNewHeroListPortrait);	// set new portrait correctly
    Z_Heroine->WriteLoHook(0x58D5DE, ReadFromNewHeroListName);		// set new name correctly
    // hook would be earlier but not possible without large overwrite
    Z_Heroine->WriteLoHook(0x58C97A, ReadFromNewHeroListGenerate);	// select the hero from the new list when generating map
    Z_Heroine->WriteLoHook(0x582853, ReadFromNewHeroListRightClick);	// gives correct hero id when right-clicking to get starting information

    Z_Heroine->WriteLoHook(0x00588AC3, hook_00588AC3);
    Z_Heroine->WriteLoHook(0x00583C52, hook_00583C52);
    // Z_Heroine->WriteLoHook(0x0058B25A, hook_0058B25A);


    Z_Heroine->WriteHiHook(0x587FD0, SPLICE_, SAFE_, THISCALL_, Y_NewScenarioDlg_Proc_hook);
    // Z_Heroine->WriteLoHook(0x00588469, hook_00588469);

    if (newHeroSelectionDialog)
    {
        Z_Heroine->WriteLoHook(0x0058692A, hook_0058692A);
        Z_Heroine->WriteLoHook(0x0058260B, hook_0058260B);
    }
}