// dllmain.cpp : Defines the entry point for the DLL application.
// #include "pch.h"
#pragma warning(disable : 4996)

// #include "../__include__/patcher_x86_commented.hpp"
// #include "patcher_x86_commented.hpp"

#define _H3API_PLUGINS_
#define _H3API_PATCHER_X86_
#include "../__include__/H3API/single_header/H3API.hpp"

#include "framework.h"
#include "era.h"

#define PINSTANCE_MAIN "Heroine"

Patcher* globalPatcher;
PatcherInstance* Z_Heroine;

char* hlp_Unnamed = "Unnamed";

#include "hero_limits.h"

#include "NPC.h"


namespace Heroine {

    /** @brief [A4] to read data from h3m file*/
    h3::H3SetupHero heroSetup[MaxHeroCount];

    /** @brief [21620] */
    h3::H3Hero heroes[MaxHeroCount];
    /** @brief [4DF18] */
    INT8 heroOwner[MaxHeroCount];
    /** @brief [4DFB4] */
    h3::H3Bitfield heroMayBeHiredBy[MaxHeroCount];

    h3::H3HeroSpecialty heroSpecialty[MaxHeroCount], 
        HSpecBack[MaxHeroCount];

    struct _HSpecNames_ {
        int PicNum; 
        int Var[3];   
    } HSpecNames[MaxHeroCount];

    int   ERM_ZERO, ERMVarH[MaxHeroCount][200]; // 00A4AB10

    h3::H3HeroInfo HeroInfo[MaxHeroCount]; /// 0067DCE8

    NPC NPCs[MaxHeroCount], NPCsBack[MaxHeroCount];
    // 0x28620C0 , 0x286D520

    static struct _ERM_Hero_ {
        WORD  HintVar;     // ���������� ��������� 0-���,1...
        char  Disabled[8]; // 1-dis 0-en �� �������
    } ERM_Hero[MaxHeroCount];

    struct _PicsBack_ {
        BYTE* HPSLoaded;
        BYTE* HPLLoaded;
    } PicsBack[MaxHeroCount] = {};

    struct _HeroBios_ { // HeroBios.txt 5B9A06 -> 6A6740 (6A673C) (4*N) N=A3h(N+7)
        char* HBios;
    } HBios_unk, HBiosTable[MaxHeroCount];

    h3::H3HeroInfo HTableBack[MaxHeroCount];
    h3::H3HeroInfo HeroInfoBack[MaxHeroCount];

    // BYTE  PostERM_buffer[PERMSIZE];
    int old_erm_buffer = 0;

    char ERMVarHMacro[MaxHeroCount][200][8];

    BOOL8 isHeroEnabled[MaxHeroCount];

    struct _HeroCombatDef_ {
        char* def_name;
        DWORD cast_x, cast_y, cast_cadre;
    } CombatHeroDef[MaxHeroClassCount];

    struct _HeroType_ {
        DWORD Belong2Town;
        char* TypeName;
        DWORD Agression;
        char PSkillStart[4];
        char ProbPSkillToLvl9[4];
        char ProbPSkillAfterLvl10[4];
        char ProbSSkill[28];
        char ProbInTown[9];
        char field_3D[3];
    } HeroTypes[MaxHeroClassCount];

    // Adventure Manager + 0x10c
    h3::H3LoadedDef* heroDef[MaxHeroClassCount];

    // auto helper = sizeof(_HeroType_);
}


_LHF_(hook_005684B4) {
    h3::H3Hero* hero = (h3::H3Hero*)c->ebx;
    c->eax = hero->id;
    return SKIP_DEFAULT;
}

_LHF_(hook_004077B0) {
    c->ebx = (int) Heroine::heroDef;
    return SKIP_DEFAULT;
}

_LHF_(hook_004077BF) {
    if (c->ecx) return EXEC_DEFAULT;

    c->return_address = 0x004077C7;
    return SKIP_DEFAULT;
}

_LHF_(hook_004072E8) {
    static char Name[16];
    sprintf(Name,"AH%02d_.def", c->esi);
    c->ecx = (int) Name;
    return SKIP_DEFAULT;
}

_LHF_(hook_004072F7) {
    Heroine::heroDef[c->esi] = (h3::H3LoadedDef*) c->eax;
    return SKIP_DEFAULT;
}
_LHF_(hook_00407308) {
    if (c->esi < MaxHeroClassCount)
        c->return_address = 0x004072E8;
    return SKIP_DEFAULT;
}

_LHF_(hook_00410183) {
    c->eax = (int)Heroine::heroDef[c->ecx];
    return SKIP_DEFAULT;
}
_LHF_(hook_004101FF) {
    c->ecx = (int)Heroine::heroDef[c->eax];
    return SKIP_DEFAULT;
}

_LHF_(hook_005F7C1C) {
    c->eax = (int)Heroine::heroDef[c->eax];
    return SKIP_DEFAULT;
}



_LHF_(hook_0046C786) {
    h3::H3Hero* hero = (h3::H3Hero*)c->esi;
    c->eax = hero->id;
    return EXEC_DEFAULT;
}

_LHF_(hook_00485043) {
    h3::H3Hero* hero = (h3::H3Hero*)c->esi;
    c->eax = hero->id;
    return SKIP_DEFAULT;
}

_LHF_(hook_00451906) {
    h3::H3Hero* hero = (h3::H3Hero*)c->ebx;
    c->edx = hero->id;
    return EXEC_DEFAULT;
}

_LHF_(hook_005AEB8B) {
    h3::H3Hero* hero = (h3::H3Hero*)c->edx;
    c->eax = hero->id;
    return SKIP_DEFAULT;
}

_LHF_(hook_004700AA) {
    h3::H3Hero* hero = (h3::H3Hero*)c->edi;
    c->edx = hero->id;
    return EXEC_DEFAULT;
}

_LHF_(hook_00470315) {
    h3::H3Hero* hero = (h3::H3Hero*)c->esi;
    c->edx = hero->id;
    return EXEC_DEFAULT;
}

_LHF_(hook_005D08A0) {
    h3::H3Hero* hero = (h3::H3Hero*)c->edx;
    c->ecx = hero->id;
    return EXEC_DEFAULT;
}

_LHF_(hook_004F90AB) {
    h3::H3Hero* hero = (h3::H3Hero*)c->edi;
    c->ecx = hero->id;
    return EXEC_DEFAULT;
}

_LHF_(hook_004E7880) {
    h3::H3Hero* hero = (h3::H3Hero*)c->ecx;
    c->edx = hero->id;
    return EXEC_DEFAULT;
}

_LHF_(hook_005DE518) {
    h3::H3Hero* hero = (h3::H3Hero*)c->eax;
    c->edx = hero->id;
    c->edi = c->edx;
    return SKIP_DEFAULT;
}

_LHF_(hook_0052F117) {
    h3::H3Hero* hero = (h3::H3Hero*)c->ebx;
    c->edx = hero->id;
    return EXEC_DEFAULT;
}

_LHF_(hook_0051C93E) {
    h3::H3Hero* hero = (h3::H3Hero*)c->esi;
    c->edx = hero->id;
    return EXEC_DEFAULT;
}

_LHF_(hook_0051CBE7) {
    h3::H3Hero* hero = (h3::H3Hero*)c->esi;
    c->edx = hero->id;
    return EXEC_DEFAULT;
}

_LHF_(hook_0051D73D) {
    h3::H3Hero* hero = (h3::H3Hero*)c->ecx;
    c->edx = hero->id;
    return EXEC_DEFAULT;
}

_LHF_(hook_005C72B0) {
    h3::H3Hero* hero = (h3::H3Hero*)c->edx;
    c->ecx = hero->id;
    return SKIP_DEFAULT;
}

_LHF_(hook_005C73AB) {
    h3::H3Hero* hero = (h3::H3Hero*)c->edi;
    c->edx = hero->id;
    c->Push(c->ecx);
    c->Push(c->edx);
    return SKIP_DEFAULT;
}

_LHF_(hook_005D8798) {
    h3::H3Hero* hero = (h3::H3Hero*)c->edi;
    c->eax = hero->id;
    return SKIP_DEFAULT;
}

_LHF_(hook_004E18CC) {
    h3::H3Hero* hero = (h3::H3Hero*)c->eax;
    c->edx = hero->id;
    return SKIP_DEFAULT;
}

void InstallHeroDef(void) {
    
    // Z_Heroine->WriteLoHook(0x0046C786, hook_0046C786); // BattleMgr_Dlg_HeroPreview_Prepare
    Z_Heroine->WriteLoHook(0x00485043, hook_00485043); // CampScenarioStartOptionsPlayer_GetImgName
    Z_Heroine->WriteLoHook(0x00451906, hook_00451906); // DlgPanel_BuildHeroStatsPanel
    Z_Heroine->WriteLoHook(0x005AEB8B, hook_005AEB8B); // DlgSwapHero_Create
    Z_Heroine->WriteLoHook(0x004700AA, hook_004700AA); // Dlg_BattleResults_Create
    Z_Heroine->WriteLoHook(0x00470315, hook_00470315); // Dlg_BattleResults_Create
    Z_Heroine->WriteLoHook(0x005D08A0, hook_005D08A0); // Dlg_HarrisonMoveCreatures
    Z_Heroine->WriteLoHook(0x004F90AB, hook_004F90AB); // Dlg_HeroLvlUp
    Z_Heroine->WriteLoHook(0x004E7880, hook_004E7880); // Dlg_HillFortBuild
    Z_Heroine->WriteLoHook(0x005DE518, hook_005DE518); // Dlg_PlayerRankAdjustItems
    Z_Heroine->WriteLoHook(0x0052F117, hook_0052F117); // Dlg_RightClick_Hero_Prepare
    Z_Heroine->WriteLoHook(0x0051C93E, hook_0051C93E); // KingdomOverview_Prepare
    Z_Heroine->WriteLoHook(0x0051CBE7, hook_0051CBE7); // KingdomOverview_Prepare
    Z_Heroine->WriteLoHook(0x0051D73D, hook_0051D73D); // KingdomOverview_Prepare
    Z_Heroine->WriteLoHook(0x005C72B0, hook_005C72B0); // Town_CreateNewGarriBars
    Z_Heroine->WriteLoHook(0x005C73AB, hook_005C73AB); // Town_CreateNewGarriBars
    // Z_Heroine->WriteLoHook(0x005D8798, hook_005D8798); // Town_Dlg_Taverna_Show
    // Z_Heroine->WriteLoHook(0x004E18CC, hook_004E18CC); // sub_004E1870
    

    Z_Heroine->WriteDword(0x00463077 + 2, 0x0 + (int)Heroine::CombatHeroDef);
    Z_Heroine->WriteDword(0x005A040D + 2, 0x4 + (int)Heroine::CombatHeroDef);
    Z_Heroine->WriteDword(0x005A04BD + 2, 0x4 + (int)Heroine::CombatHeroDef);
    Z_Heroine->WriteDword(0x005A04C9 + 2, 0x8 + (int)Heroine::CombatHeroDef);
    Z_Heroine->WriteDword(0x005A051B + 2, 0xC + (int)Heroine::CombatHeroDef);
    Z_Heroine->WriteDword(0x005A057D + 2, 0xC + (int)Heroine::CombatHeroDef);
    Z_Heroine->WriteDword(0x005A241C + 2, 0xC + (int)Heroine::CombatHeroDef);

    
    Z_Heroine->WriteLoHook(0x005684B4, hook_005684B4);
    Z_Heroine->WriteLoHook(0x004077B0, hook_004077B0);
    Z_Heroine->WriteLoHook(0x004077BF, hook_004077BF);
    Z_Heroine->WriteDword(0x004077B6 + 3, MaxHeroClassCount);

    Z_Heroine->WriteLoHook(0x004072E8, hook_004072E8);
    Z_Heroine->WriteLoHook(0x00407308, hook_00407308);

    Z_Heroine->WriteLoHook(0x004072F7, hook_004072F7);
    Z_Heroine->WriteLoHook(0x00410183, hook_00410183);
    Z_Heroine->WriteLoHook(0x004101FF, hook_004101FF);
    Z_Heroine->WriteLoHook(0x00410623, hook_00410183);
    Z_Heroine->WriteLoHook(0x0041069F, hook_004101FF);

    Z_Heroine->WriteLoHook(0x0047F58A, hook_004101FF);
    Z_Heroine->WriteLoHook(0x0047F443, hook_004101FF);
    Z_Heroine->WriteLoHook(0x0047F876, hook_004101FF);

    Z_Heroine->WriteLoHook(0x005F7C1C, hook_005F7C1C);
    Z_Heroine->WriteLoHook(0x005F7C92, hook_004101FF);

    Z_Heroine->WriteLoHook(0x005F801C, hook_005F7C1C);
    Z_Heroine->WriteLoHook(0x005F8092, hook_004101FF);
}

#define old_heroes_ptr ((int)(h3::H3Main::Get()->heroes))

h3::H3HeroInfo* old_HeroInfo_ptr = (h3::H3HeroInfo*)0x00679DD0;
h3::H3HeroInfo* old_HeroInfo_wog_ptr = (h3::H3HeroInfo*)0x007C44C0;
int* old_heroname_wog = (int*) 0x7C7FB8;

void set_string(char** a, char* b, int n);

extern void InstallHeroesSpecialtyGfx();

/*
inline h3::H3Hero* get_old_heroes_table() {
    return h3::H3Main::Get()->heroes;
}
*/

void RefreshPicsBack(int id, bool force = 0);

void RefUp_PicsBack(int id) {
    // return;

    auto large = (h3::H3LoadedPcx*)Heroine::PicsBack[id].HPLLoaded;
    auto small = (h3::H3LoadedPcx*)Heroine::PicsBack[id].HPSLoaded;

    if (large) large->IncreaseReferences();
    if (small) small->IncreaseReferences();
}

void install_PicsBack() {
    static bool once = true;

    /*
    if (once) memcpy(Heroine::PicsBack, (void*)0x027FE1A0,
        sizeof(Heroine::_PicsBack_) * OldHeroCount);
    */
    Z_Heroine->WriteDword(0x00753CE7 + 3, (int)Heroine::PicsBack);
    Z_Heroine->WriteDword(0x00753D07 + 3, (int)Heroine::PicsBack + 4);
    Z_Heroine->WriteDword(0x007541B6 + 3, (int)Heroine::PicsBack);
    Z_Heroine->WriteDword(0x00753D4B + 3, 0);
    // Z_Heroine->WriteDword(0x028077CC + 0, 0);

    if (true && once) {
        for (int i = 0; i < MaxHeroCount; ++i) {
            RefreshPicsBack(i);
            RefUp_PicsBack(i);
        }
    }

    once = false;
}

void RefreshPicsBack(int id, bool force) {

    // install_PicsBack();
    
    // CDECL_0(DWORD*, 0x753B98);
    
    // return;

    if( (!Heroine::PicsBack[id].HPLLoaded || force) &&  
        Heroine::HeroInfo[id].largePortrait)
            Heroine::PicsBack[id].HPLLoaded =  
                FASTCALL_1(BYTE*, 0x55AA10, Heroine::HeroInfo[id].largePortrait);
    if( (!Heroine::PicsBack[id].HPSLoaded || force) && 
        Heroine::HeroInfo[id].smallPortrait)
            Heroine::PicsBack[id].HPSLoaded = 
                FASTCALL_1(BYTE*, 0x55AA10, Heroine::HeroInfo[id].smallPortrait);
}

extern "C" __declspec(dllexport) NPC* GetCommander(int i = 0) {
    return &Heroine::NPCs[i];
}

extern "C" __declspec(dllexport) char** GetBiography(int i=0) {
    return &Heroine::HBiosTable[i].HBios;
}

extern "C" __declspec(dllexport) void* GetERMVarHTable() {
    return Heroine::ERMVarH;
}

extern "C" __declspec(dllexport) h3::H3Hero * GetHeroesTable() {
    return Heroine::heroes;
}
extern "C" __declspec(dllexport) h3::H3HeroSpecialty * GetHeroesSpecialtyTable() {
    return Heroine::heroSpecialty;
}
extern "C" __declspec(dllexport) Heroine::_HSpecNames_ * GetHeroesSpecNames() {
    return Heroine::HSpecNames;
}
extern "C" __declspec(dllexport) h3::H3HeroInfo * GetHeroesInfoTable() {
    return Heroine::HeroInfo;
}
extern "C" __declspec(dllexport) INT8 GetOwner(int ID) {
    return Heroine::heroOwner[ID];
}
extern "C" __declspec(dllexport) void SetOwner(int ID, INT8 value) {
    Heroine::heroOwner[ID] = value;
}

/*
_LHF_(_add_eax_HeroesOffset_) {
    c->eax += (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}
*/

void configure_all_heroes();
void default_hero(int id);

void install_PostERM_buffer() {
    // Heroine::ERM_ZERO = Heroine::old_erm_buffer;
    return;
    Z_Heroine->WriteDword(0x00A4AB0C, Heroine::old_erm_buffer);
    return;

    // Z_Heroine->WriteDword(0x0073E1C3 + 6, (int)Heroine::PostERM_buffer);
    // Z_Heroine->WriteDword(0x00A4AB0C + 0, (int)&Heroine::PostERM_buffer);
    Z_Heroine->WriteDword(0x0073E1D2 + 2, PERMSIZE);
    // CDECL_0(int, 0x73E1AA);
}

void install_Biographies() {
    static bool once = true;

    // return;

    // if (once) memcpy(Heroine::HBiosTable, (void*)0x006A6740, 4 * OldHeroCount);
    Z_Heroine->WriteDword(0x007466D1 + 3, (int)Heroine::HBiosTable);
    Z_Heroine->WriteDword(0x00746717 + 3, (int)Heroine::HBiosTable);
    Z_Heroine->WriteDword(0x005B9A18 + 2, (int)&Heroine::HBiosTable[-1].HBios);
    Z_Heroine->WriteDword(0x005641A2 + 2, (int)&Heroine::HBiosTable[-1].HBios);
    Z_Heroine->WriteDword(0x004D92B7 + 3, (int)Heroine::HBiosTable);
    Z_Heroine->WriteDword(0x004DD975 + 3, (int)Heroine::HBiosTable);

    Z_Heroine->WriteDword(0x005B9A0E + 2, 4 * OldHeroPortraitsCount);

    Z_Heroine->WriteDword(0x006A673C + 0, (int)Heroine::HBiosTable);

    CDECL_0(char, 0x005B99F0);

    once = false;
}

void install_MISC() {
    static bool once = true;

    Z_Heroine->WriteDword(0x007348F8 + 3, (int)Heroine::ERMVarHMacro);
    Z_Heroine->WriteDword(0x0074DA2A + 2, (int)Heroine::ERMVarHMacro);
    Z_Heroine->WriteDword(0x0074DA81 + 2, (int)Heroine::ERMVarHMacro);
    Z_Heroine->WriteDword(0x00750F92 + 1, sizeof(Heroine::ERMVarHMacro));
    Z_Heroine->WriteDword(0x0075175B + 1, sizeof(Heroine::ERMVarHMacro));

    Z_Heroine->WriteDword(0x007510E1 + 1, (int)Heroine::ERM_Hero);
    Z_Heroine->WriteDword(0x007518AA + 1, (int)Heroine::ERM_Hero);
    Z_Heroine->WriteDword(0x007529DA + 3, (int)Heroine::ERM_Hero);
    Z_Heroine->WriteDword(0x007374F7 + 1, (int)Heroine::ERM_Hero);
    Z_Heroine->WriteDword(0x0074DDCC + 3, (int)Heroine::ERM_Hero);
    Z_Heroine->WriteDword(0x007510DC + 1, sizeof(Heroine::ERM_Hero));
    Z_Heroine->WriteDword(0x007518A5 + 1, sizeof(Heroine::ERM_Hero));

    Z_Heroine->WriteDword(0x00752CDC + 2, (int)Heroine::HTableBack);
    Z_Heroine->WriteDword(0x00752D11 + 2, (int)Heroine::HTableBack);
    Z_Heroine->WriteDword(0x00752D02 + 3, sizeof(Heroine::HTableBack));
    Z_Heroine->WriteDword(0x00752CC7 + 3, sizeof(Heroine::HTableBack));


    Z_Heroine->WriteDword(0x007538F8 + 1, sizeof(Heroine::HeroInfoBack));
    Z_Heroine->WriteDword(0x00753D1C + 1, sizeof(Heroine::HeroInfoBack));
    Z_Heroine->WriteDword(0x00753C9D + 1, sizeof(Heroine::HeroInfoBack));
    Z_Heroine->WriteDword(0x00753D25 + 1, (int)Heroine::HeroInfoBack);
    Z_Heroine->WriteDword(0x00753CA2 + 1, (int)Heroine::HeroInfoBack);

    Z_Heroine->WriteDword(0x00753A3D + 2, (int)Heroine::HeroInfoBack + 0x30);
    Z_Heroine->WriteDword(0x00753B82 + 2, (int)Heroine::HeroInfoBack + 0x30);
    Z_Heroine->WriteDword(0x00753A24 + 2, (int)Heroine::HeroInfoBack + 0x34);
    Z_Heroine->WriteDword(0x00753B69 + 2, (int)Heroine::HeroInfoBack + 0x34);

    once = false;
}

void Install_heroCount() {
     // return;

    int addr_sod[] = {
        0x4BB1EC + 2,  0x4BB252 + 2,  0x4BB36F + 2,
        0x4BB500 + 2,  0x4BD1CB + 3,  0x4BE51D + 1,
        0x4BE597 + 3,  0x4BEF9A + 2,  0x4BF61E + 2,
        0x4BF8D8 + 2,  0x4BFBA1 + 2,
        0x4C81DE + 2,  0x4C8222 + 2,  0x4C825F + 2,
        0x4C89E5 + 1,  0x4CDE53 + 3, 

        0x00583DAE + 2, 0x004CDE2F + 1,
        0x004CDC2A + 1, 0x004CE4F4 + 1, 0x004CE57A + 1,

        0x004BD088 + 1, 0x004BD1CB + 3,

        0x004BD144 + 1,

        0x004BE570 + 1, 0x004BE565 + 1, 0x004BEF62 + 1,

        0x004BD139 + 1,
    };
    int addr_wog[] = {
        0x0070E044 +3, 0x0070E119+3, 0x007116AF+3, 0x0071171E+3, 0x00711791+3, 
        /*0x00714096,*/ 0x0071774A+2, 0x00717BCC+2, 0x007180AF+1, 0x00719098+1, 
        0x00719E4D+3, 0x0071A1E5+3, 0x007247D6+3, 0x007252C9+3, 0x007252D2+3, 
        0x00725522+3, 0x0072590D+3, 0x00726024+3, /* 0x0072735F + 1, 0x007274D5 + 1, */
        /*0x00727582+7, 0x00727995, 0x007279EA, 0x00728273, */ 0x0072BB0C+3, 
        0x00731410+3, 0x0073358E+3, 0x007373D9+3, 0x00737A86+3, 0x00737ADE+3, 
        0x00737B4C+3, 0x0073C46C+3, 0x0073C4AD+3, 0x0073C67C+3, 0x0073CEB0+3, 
        0x007476C2+3, 0x0074AE5B+3, 0x0074AEF0+3, 0x0074AF84+3, 0x0075034C+3, 
        0x00751525+3, /*0x00751C51+3,*/ 0x00752310+3, 0x0075237C+3, 0x00752843+3, 
        0x007529CB+3, 0x00752F58+3, /*0x00753337,*/ 0x0075393C+3, 0x007539D2+3, 
        0x00753AE9+3, 0x00753B11+3, 0x00753BCE+3, 0x00753CC5+3, /* 0x00753D4B + 3, */
        0x00753F93+6, 0x007543E4+6, 0x0075471C+3, 0x00755AD1+3, 0x00755C25+3, 
        0x00757C8D+3, 0x00757F90+3, 0x007581D2+3, 0x007582DB+3, 0x00758A84+3, 
        0x00758D1E+3, 0x0075D551+3, /*0x0075E37F, 0x0075E5F3, 0x0075E86D, */
        /* 0x007606D6,*/ 0x00760AD3+3, 0x00760DF8+3, 0x00761F4E+3, 0x00763DB4+3, 
        0x00765C7B+3, 0x00766617+3, 0x0076B4E0+3, 0x0076B573+3, 0x0076B608+3, 
        0x0076B68C+3, /* 0x0076E1D4+3, */ 0x0076F210 + 3, 0x0076F27A + 3, 0x0076F358+3, 
        0x0076F3FC+3, 0x0076F598+3, 0x0076F710+3, 0x0076F84C+3, 0x0076FF8F+3, 
        0x0076FFC0+3, 0x0076FFF1+3, 0x007701D6+3, 0x007702B6+3, 0x00770320+3, 
        0x00770400+3, 0x007707F3+3, 0x00770B51+3, /* 0x007715A5,*/ 0x00771BEB+3,
        /* 0x00774C28,*/ 0x00779C01+3, /* 0x00782591, */0x00784488+1, 0x007844A8+1,
    };
    for (int i : addr_sod) { 
        int tmp = *(int*)i;
        if (tmp != 0x9c && tmp != MaxHeroCount) {

            continue;
        }
        Z_Heroine->WriteDword(i, (int)MaxHeroCount); 
    }
    for (int i : addr_wog) {
        int tmp = *(int*)i;
        if (tmp != 0x9c && tmp != MaxHeroCount) {

            continue;
        }
        Z_Heroine->WriteDword(i, (int)MaxHeroCount);
    }

    // Z_Heroine->WriteByte(0x004D925F + 3, MaxHeroCount);
    // Z_Heroine->WriteByte(0x004DD91D + 3, MaxHeroCount);

    // unused: 0x004CAADB
    // unused: // 4C2CCF 4CAADB

    Z_Heroine->WriteDword(0x007519A6 + 3, MaxHeroCount); // 164
}

void Install_heroNPC() {
    constexpr size_t NPC_Size = sizeof(NPC);
    int addr_NPCs[] = { 0x0076B395+2, 0x0076B502+2, 0x0076B52A+2, 0x0076B588+2, 0x0076B5B1+2,
        0x0076B61A+2, 0x0076B644+2, 0x0076B6A7+2, 0x0076BF68+2, 0x0076C020+2, 0x0076C7E3+2,
        0x0076C8F6+2, 0x0076CB87+2, 0x0076CD35+2, 0x0076CF2F+1, 0x0076D1C0+2, 0x0076D575+2, 
        0x0076D622+1, 0x0076D8CC+2, 0x0076D96F+1, 0x0076DA2F+1, 0x0076DAD0+1, 0x0076DB9A+1, 
        0x0076DCC0+1, 0x0076DD41+2, 0x0076DE0A+2, 0x0076E4E4+2, 0x0076E63A+2, 0x0076E7A3+2, 
        0x0076E894+2, 0x0076EC15+2, 0x0076EF3C+2, 0x0076F14C+2, 0x0076F2B2+1,

        0x0076F2BC+3, 0x0076F36D+2, 0x00770811+1, 0x0077085E+2, 0x00770906+2, 
        0x00770ABD+1, 0x00770B09+1, 0x00770B75+2, 0x00770B9B+2, 
        0x00770CBA+1, 0x00784492+1, 
    };
    for (int i : addr_NPCs) {
        int tmp = *(int*)i;
        if (tmp != 0x28620C0) {

            continue;
        }Z_Heroine->WriteDword(i, (int)Heroine::NPCs);
    }
    int addr_NPCsBack[] = { 0x00770820 + 2, 0x00770B04 + 1, 0x007844B2 + 1 };
    for (int i : addr_NPCsBack) Z_Heroine->WriteDword(i, (int)Heroine::NPCsBack);

    //---------------------

    Z_Heroine->WriteDword(0x00770AB8 + 1, sizeof(Heroine::NPCs));
    Z_Heroine->WriteDword(0x00770CB5 + 1, sizeof(Heroine::NPCs));
    Z_Heroine->WriteDword(0x00770AFF + 1, sizeof(Heroine::NPCs));

    int addr_NPC_28620C4[] = { 0x0076B597 + 2 , 0x0076B5C0 + 2 , 0x0076B62D + 2 , 0x0076B657 + 2 , 
        0x0076B6C2 + 2 , 0x0076C7FE + 2 , 0x0076C910 + 2 , 0x0076F411 + 2 , 0x007708BD + 2 , };
    for (int i : addr_NPC_28620C4) Z_Heroine->WriteDword(i, (int)Heroine::NPCs + 4);

    Z_Heroine->WriteDword(0x00770B66 + 2, (int)Heroine::NPCs + 8);

    int addr_NPC_28620D4[] = { 0x0076C745 +2, 0x0076C774+2, 0x0076C857+2, 0x0076C887+2 };
    for (int i : addr_NPC_28620D4) Z_Heroine->WriteDword(i, (int)Heroine::NPCs + 0x14);

    Z_Heroine->WriteDword(0x0076F5B0 + 2, (int)Heroine::NPCs + 0x18);
    Z_Heroine->WriteDword(0x0076F5C4 + 2, (int)Heroine::NPCs + 0x18);

    Z_Heroine->WriteDword(0x0076F728 + 3, (int)Heroine::NPCs + 0x1C);

    Z_Heroine->WriteDword(0x0076F864 + 3, (int)Heroine::NPCs + 0x28);

    int addr_NPC_2862116[] = {
        0x0076C7A8 + 4, 0x0076C7C6 + 4, 0x0076C832 + 4, 
        0x0076C8BB + 4, 0x0076C8D9 + 4, 0x0076C944 + 4,
    };
    for (int i : addr_NPC_2862116) Z_Heroine->WriteDword(i, (int)Heroine::NPCs + 0x56);

    int addr_NPC_28621D4[] = { 0x0076B459 + 2, 0x0076B498 + 2, 0x0076FFA4+2 };
    for (int i : addr_NPC_28621D4) Z_Heroine->WriteDword(i, (int)Heroine::NPCs + 0x114);

    Z_Heroine->WriteDword(0x0076FFD5 + 2, (int)Heroine::NPCs + 0x118);

    Z_Heroine->WriteDword(0x00770009 + 2, (int)Heroine::NPCs + 0x11C);

    int addr_NPC_28621E0[] = { 0x007701EB + 2, 0x007702C8 + 2,
        0x007702DA + 2, 0x007702E9 + 2, 0x007702FB + 2 };
    for (int i : addr_NPC_28621E0) Z_Heroine->WriteDword(i, (int)Heroine::NPCs + 0x120);

    int addr_NPC_28621E4[] = { 0x00770335 + 2, 0x00770412 + 2,
        0x00770424 + 2, 0x00770433 + 2, 0x00770445 + 2 };
    for (int i : addr_NPC_28621E4) Z_Heroine->WriteDword(i, (int)Heroine::NPCs + 0x124);


}
void Install_heroSetup() {
    static bool once = true;

    // return; // do we need it at all ?

    if(once)
        memcpy(Heroine::heroSetup, h3::H3Main::Get()->heroSetup,
            OldHeroCount * sizeof(h3::H3SetupHero));
     // return;

    int addr_1[] = {
        0x0062C920, 0x0062CAE0
    };

    int addr_2[] = { /* 0x004BEF54, */
        0x004CDC2F, 0x004CE57F, // do we need it ?
        // 0x00749A20, 
    };
    int addr_3[] = { 0x00485C55, 0x00485CAD, 0x004C27A3, 0x005029D6
    };
    for (int i : addr_1) Z_Heroine->WriteDword(i + 1, (int)Heroine::heroSetup - (int)h3::H3Main::Get());
    for (int i : addr_2) Z_Heroine->WriteDword(i + 2, (int)Heroine::heroSetup - (int)h3::H3Main::Get());
    for (int i : addr_3) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroSetup - (int)h3::H3Main::Get());

    /*
    for (int i = 0; i < OldHeroCount; ++i) {
        Heroine::heroSetup[i].id = i;
    }
    */

    Z_Heroine->WriteDword(0x00485C49 + 4, (int)Heroine::heroSetup + 0x301 - (int)h3::H3Main::Get());
    Z_Heroine->WriteDword(0x004CAAE2 + 2, (int)Heroine::heroSetup + 0x301 - (int)h3::H3Main::Get());

    once = false;
}

void Install_HeroInfo() {
    static bool once = true;
    
    if (once) {
        memcpy((void*)Heroine::HeroInfo, old_HeroInfo_wog_ptr,
            sizeof(h3::H3HeroInfo) * OldHeroPortraitsCount);
        memcpy((void*)Heroine::HeroInfoBack, old_HeroInfo_wog_ptr,
            sizeof(h3::H3HeroInfo) * OldHeroPortraitsCount);
    }
    // Z_Heroine->WriteDword(0x00712646 + 1, (int)Heroine::HeroInfo);

    Z_Heroine->WriteDword(0x004E68A6 + 1, (int)Heroine::HeroInfo->creatureAmount);
    Z_Heroine->WriteDword(0x004E69A6 + 2, (int)Heroine::HeroInfo[OldHeroPortraitsCount].creatureAmount);

    Z_Heroine->WriteDword(0x007511B3 + 1, (int)Heroine::HeroInfo);
    Z_Heroine->WriteDword(0x007519B9 + 1, (int)Heroine::HeroInfo);
    Z_Heroine->WriteDword(0x00752CD6 + 2, (int)Heroine::HeroInfo);
    Z_Heroine->WriteDword(0x00752D17 + 2, (int)Heroine::HeroInfo);

    /*
    for (int i = 0; i < OldHeroPortraitsCount; ++i) {
        auto& at = Heroine::HeroInfo[i].armyType;
        if (*at < 0) h3::H3Messagebox("boo");
        auto& acn = Heroine::HeroInfo[i].creatureAmount;
        auto& aco = old_HeroInfo_ptr[i].creatureAmount;
        for (int j = 0; j < 3; ++j) {
            acn[j].lowAmount  = 1;
            acn[j].highAmount = 1;
            aco[j].lowAmount = 1;
            aco[j].highAmount = 1;
        }
    }
    */

    // CDECL_0(char, 0x004E6850); // Load_Hotraits_TXT
    
    Z_Heroine->WriteDword(0x007511AE + 1, sizeof(Heroine::HeroInfo));
    
    Z_Heroine->WriteDword(0x0067DCE8, (int)Heroine::HeroInfo);

    Z_Heroine->WriteDword(0x00746E58 + 3, (int)Heroine::HeroInfo->armyType);
    Z_Heroine->WriteDword(0x00746E82 + 3, (int)Heroine::HeroInfo->creatureAmount);
    Z_Heroine->WriteDword(0x00746EAC + 3, (int)Heroine::HeroInfo->creatureAmount + 4);

    once = false;
}
void Install_HeroInfo_late() {
    // CDECL_0(char, 0x004E69D0); // Load_Hctraits_TXT

    return;
    int ii = 0;
    for (auto& i : Heroine::HeroInfo) {
        // if(!i.name) i.name = hlp_Unnamed;
        if (!i.name) i.name = (ii < OldHeroCount) ?
            ((char*)old_heroname_wog[ii]) : hlp_Unnamed;
        ++ii;
    }
}

char __stdcall Load_Hotraits_TXT_hook(HiHook* h) {
    
    auto ret = CDECL_0(char, h->GetDefaultFunc());
    // Install_HeroInfo();

    // Install_heroSetup();

    return ret;
}


void Install_heroMayBeHiredBy()
{
    /** offset [4DFB4] */
    Z_Heroine->WriteDword(0x00715646 + 1, (int)Heroine::heroMayBeHiredBy - (int)h3::H3Main::Get());
    {
        int addr[] = { 0x004BD1C5, /*0x004BE591,*/  0x004BD19D, 0x004CDE4D, /* 0x004CE07A, */ 0x004BB30D, 0x004BB4A2, 0x004BB189, 0x004BB206, /* 0x004BEF5C,*/ 0x00577A55};
        for (int i : addr) Z_Heroine->WriteDword(i + 2, (int)Heroine::heroMayBeHiredBy - (int)h3::H3Main::Get());
    }
    {
        int addr[] = { 0x00486D48, 0x004A3CE3, 0x004868F8, 0x004D7BCC, 0x004C4B84 };
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroMayBeHiredBy - (int)h3::H3Main::Get());
    }
}

void Install_heroOwner()
{
    /** offset [4DF18] */
    {
        int addr[] = { 0x004DA489, 0x004A3B35, 0x004CAB67, 0x004CABB9, 0x004F1A79, 0x0048752B, 0x004BB4AE, 0x004C81CF, 0x004C8217, 0x004C8254, 0x004BB18F, 0x004BB20C, 0x004BB316, 0x00583CE8 };
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroOwner - (int)h3::H3Main::Get());
    }
    {
        int addr[] = { /* 0x004BE55F, 0x00577A4F,*/ 0x004BD133, /* 0x004BD165, 0x004CE040, */ 0x00577A5D, 0x0058C0A5, 0x00580A7C, 0x005838C7, 0x00583A23, /*0x004BEF43,*/ 0x0058360A};
        for (int i : addr) Z_Heroine->WriteDword(i + 2, (int)Heroine::heroOwner - (int)h3::H3Main::Get());
    }
    {
        int addr[] = { 0x004868E1, 0x00502FD8, 0x00485F3D, 0x004DA50F, 0x00486D31, 0x004A3CD2, 
            0x004D7BBC, 0x004DA4BF, 0x004BFC43, /* 0x004C9E51, */ 0x004F1A83, 0x004BFDB7, 0x00502FC3, 
            0x004DA411, 0x004DA492, 0x00485F44, 0x004C4B2B, 0x004C8711, 0x00485FE4, 0x004C4B35, 
            0x004C828E, 0x004C83C9, 0x00412F18,  };
        for (int i : addr) 
            Z_Heroine->WriteDword(i + 3, (int)Heroine::heroOwner - (int)h3::H3Main::Get());
    }
    Z_Heroine->WriteDword(0x0056AF3E + 4, (int)Heroine::heroOwner - (int)h3::H3Main::Get());
    Z_Heroine->WriteDword(0x0071560D + 1, (int)Heroine::heroOwner - (int)h3::H3Main::Get());
}

void Install_ERM() {
    // return; // debug

    static bool done = false;
    if (done) return;
    const int diff = (int)Heroine::ERMVarH - (int) &Heroine::ERM_ZERO;
    if (diff != 4) MessageBoxA(0, "Critical Error", 
        "Heroine Data Integrity", 0);

    Z_Heroine->WriteDword(0x0072BB4B + 3, (int)(void*)Heroine::ERMVarH);
    Z_Heroine->WriteDword(0x0072BB94 + 3, (int)(void*)Heroine::ERMVarH);
    Z_Heroine->WriteDword(0x00736521 + 3, (int)(void*)Heroine::ERMVarH);
    Z_Heroine->WriteDword(0x00750F72 + 1, (int)(void*)Heroine::ERMVarH);
    Z_Heroine->WriteDword(0x0075173B + 1, (int)(void*)Heroine::ERMVarH);
    Z_Heroine->WriteDword(0x00752874 + 3, (int)(void*)Heroine::ERMVarH);
    Z_Heroine->WriteDword(0x007528DB + 3, (int)(void*)Heroine::ERMVarH);
    Z_Heroine->WriteDword(0x007684C5 + 3, (int)(void*)Heroine::ERMVarH);
    Z_Heroine->WriteDword(0x0076858C + 3, (int)(void*)Heroine::ERMVarH);

    Z_Heroine->WriteDword(0x00750F6D + 1, sizeof(Heroine::ERMVarH));
    Z_Heroine->WriteDword(0x00751736 + 1, sizeof(Heroine::ERMVarH));

    Z_Heroine->WriteDword(0x0072D628 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0072D6EA + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0072E410 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0072EB1D + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0072E94A + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0072DDBE + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0072DFB3 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0072E157 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0072E5BD + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0072EDF1 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0072EF9E + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0073D6E0 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0073D88F + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0073ECC7 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0073EEC3 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0073F0F2 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0073F1A3 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x00740F2F + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x0074107C + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x00741197 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x00741268 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x00741AA9 + 3, (int)(void*)Heroine::ERMVarH - 4);
    Z_Heroine->WriteDword(0x00741B63 + 3, (int)(void*)Heroine::ERMVarH - 4);
    // Z_Heroine->WriteDword(0x0073E1C3 + 2, (int)&Heroine::ERM_ZERO);
    // Z_Heroine->WriteDword(0x0073E1CD + 1, (int)&Heroine::ERM_ZERO);
    // // Z_Heroine->WriteDword(0x00749A0D + 1, (int)&Heroine::ERM_ZERO);
    // Z_Heroine->WriteDword(0x00740A47 + 1, (int)&Heroine::ERM_ZERO);

    // Heroine::old_erm_buffer = *(int*)0x00A4AB0C;
    // Z_Heroine->WriteDword((int)&Heroine::ERM_ZERO, Heroine::old_erm_buffer);

    // Z_Heroine->WriteDword(0x027F9988 + 0, (int)(void*)Heroine::ERMVarH); // ???

    int rebased_ID = 100000000;

    Z_Heroine->WriteDword(0x0074AFC2 + 2, rebased_ID + 1);
    Z_Heroine->WriteDword(0x0074D925 + 1, rebased_ID + 1);
    Z_Heroine->WriteDword(0x0074D90D + 6, rebased_ID + 0);

    Z_Heroine->WriteDword(0x0074AF2E + 1, rebased_ID + 2001);
    Z_Heroine->WriteDword(0x0074D83B + 1, rebased_ID + 2001);
    Z_Heroine->WriteDword(0x0074D826 + 6, rebased_ID + 2000);

    Z_Heroine->WriteDword(0x0074B054 + 2, rebased_ID + 4040);
    Z_Heroine->WriteDword(0x0074CF20 + 2, rebased_ID + 4040);

    Z_Heroine->WriteDword(0x0074AE99 + 2, rebased_ID + 6000);
    Z_Heroine->WriteDword(0x0074D74A + 1, rebased_ID + 6000);
    Z_Heroine->WriteDword(0x0074D4E2 + 2, rebased_ID + 6000);

    done = true;
}

void InstallHeroesSod()
{
    static bool once = true;
    /*
    memcpy(Heroine::heroSpecialty, (void*)*(int*)0x00679C80,
        OldHeroCount * sizeof(h3::H3HeroSpecialty));
    */
    // Z_Heroine->WriteDword(0x00679C80, (int)Heroine::heroSpecialty);

    // auto H3Main = h3::H3Main::Get();
    /*
    memcpy(Heroine::heroSetup, H3Main->heroSetup,
        OldHeroCount * sizeof(h3::H3SetupHero));
    */

    if(once)
        memcpy(Heroine::heroes, h3::H3Main::Get()->heroes,
            OldHeroCount * sizeof(h3::H3Hero));
 
    // memcpy(Heroine::heroOwner, H3Main->heroOwner, OldHeroCount);
    /*
    memcpy(Heroine::heroMayBeHiredBy, H3Main->heroMayBeHiredBy,
        OldHeroCount * sizeof(h3::H3Bitfield));
    */

    // Heroine::heroes begin

    Z_Heroine->WriteDword(0x004C89DF + 2, (int)Heroine::heroes + 0x105 - (int)h3::H3Main::Get());


    // 2025-03-18
    Z_Heroine->WriteDword(0x005BE681 + 4, 0x22 + (int)Heroine::heroes - (int)h3::H3Main::Get());


    //Z_Heroine->WriteLoHook(0x0062C9C3, _add_eax_HeroesOffset_);
    Z_Heroine->WriteDword(0x0062C9C3 + 1, (int)Heroine::heroes - (int)h3::H3Main::Get());
    //Z_Heroine->WriteLoHook(0x0062CB5F, _add_eax_HeroesOffset_);
    Z_Heroine->WriteDword(0x0062CB5F + 1, (int)Heroine::heroes - (int)h3::H3Main::Get());


    //Z_Heroine->WriteLoHook(0x004192D4, _lea_eax__eax_ecx_HeroesOffset_);
    Z_Heroine->WriteDword(0x004192D4 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    //Z_Heroine->WriteLoHook(0x00453552, _lea_eax__eax_ecx_HeroesOffset_);
    Z_Heroine->WriteDword(0x00453552 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    
    {
        int addr[] = {0x4F26A6,0x557096,0x419182,0x4817CA,0x4F313A,0x005267F5,
            0x005BF5EE, 0x005C69E5, 0x005C6A12, 0x005D8480, 0x005D8AAB, 0x005F7835 };
        // for(int i: addr) Z_Heroine->WriteLoHook(i, _lea_eax__eax_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    //Z_Heroine->WriteLoHook(0x00513CE7, _lea_eax__ebx_eax_HeroesOffset_);
    Z_Heroine->WriteDword(0x00513CE7 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    //Z_Heroine->WriteLoHook(0x004BFDA7, _lea_eax__ebx_eax_HeroesOffset_);
    Z_Heroine->WriteDword(0x004BFDA7 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    //Z_Heroine->WriteLoHook(0x005BFD42, _lea_eax__ebx_edx_HeroesOffset_);
    Z_Heroine->WriteDword(0x005BFD42 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = {0x0048197E, 0x005D4516, 0x00407FFA, 0x00408EA2, 0x00409261,
            0x00409A8A, 0x00409B7C, 0x0040A9C9, 0x0040DF86, 0x00412CE0, 0x0042B13F,
            0x0043145A, 0x004317EB, 0x004331A3, 0x004806D2, 0x00483F8B, 0x004896AC,
            0x004C81C2, 0x004C820A, 0x004C8247, 0x004DE57C, 0x004E18C5, 0x004FD243,
            0x004FD2A5, 0x004FD2F3, 0x0051FAAD, 0x0052189C, 0x005218D4, 0x00526C00,
            0x00526F42, 0x0056E478, 0x005721A8, 0x005BE40F, 0x005BE689, 0x005BF5A7,
            0x005C188B, 0x005C7EED, 0x005C9BED, 0x005D33DD, 0x005D84F3, 0x005D8862,
            0x005DE50D, 0x005F1F68
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_eax__ecx_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = {0x00525741, 0x0040E9A4, 0x00429807, 0x0042986C, 0x004315CA,
            0x0048971B, 0x0049E03F
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_eax__ecx_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x004B262B, 0x004C8178, 0x004FD4A1, 0x0050609B, 0x005C76A1,
            0x005C76C1
        };
        //for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_eax__edi_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    /*
    Z_Heroine->WriteLoHook(0x004C839F, _lea_eax__edi_ecx_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x0051FB29, _lea_eax__edi_edx_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x0040E4AD, _lea_eax__edx_eax_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x0042EFB6, _lea_eax__edx_eax_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x005DEF1D, _lea_eax__edx_eax_HeroesOffset_);
    */
    {
        int addr[] = { 0x004C839F, 0x0051FB29, 0x0040E4AD, 0x0042EFB6, 0x005DEF1D
        };
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }


    {
        int addr[] = {0x005D4499, 0x004168D0, 0x00418E4C, 0x0041943D, 0x00431EE9,
            0x004814CC, 0x0049A2EA, 0x0049B0DD, 0x0052297F, 0x00525950, 0x00525E87,
            0x0052A8AD, 0x005C7E0A, 0x005D7DA9
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_eax__edx_ecx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    
    // Z_Heroine->WriteLoHook(0x004BEF6E, _lea_eax__esi_HeroesOffset_);
    // Z_Heroine->WriteDword(0x004BEF6E + 2, (int)Heroine::heroes - (int)h3::H3Main::Get());

    /*
    Z_Heroine->WriteLoHook(0x004FD37E, _lea_eax__esi_eax_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x00513AC9, _lea_eax__esi_eax_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x005D31E6, _lea_eax__esi_eax_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x005D3238, _lea_eax__esi_eax_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x0049D446, _lea_eax__esi_ecx_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x00483C89, _lea_eax__esi_edx_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x0049D4AC, _lea_eax__esi_edx_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x005D3209, _lea_eax__esi_edx_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x004F4A11, _lea_ebx__eax_ecx_HeroesOffset_);
    */
    {
        int addr[] = {  0x004FD37E, 0x00513AC9,0x005D31E6, 0x005D3238,
            0x0049D446, 0x00483C89, 0x0049D4AC, 0x005D3209, 0x004F4A11
        };
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x004AAE0C, 0x004035DF, 0x004AAF6D, 0x004AAFFB,
            0x004C8830, 0x00582654, 0x00481AAC
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_ebx__eax_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x0040A7F4, 0x004518D1, 0x0047F4B9 };
        //for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_ebx__ecx_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x004165BA, 0x004167DF, 0x00416FAB, 0x0052B36F };
        //for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_ebx__ecx_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    // Z_Heroine->WriteLoHook(0x005060EC, _lea_ebx__edi_ecx_HeroesOffset_);
    Z_Heroine->WriteDword(0x005060EC + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = { 0x0042BB00, 0x00418DE1, 0x0041CA56, 0x004BC922,
            0x0052B5D7, 0x00582866
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_ebx__edx_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    /*
    Z_Heroine->WriteLoHook(0x0052B48F, _lea_ebx__edx_ecx_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x005683A6, _lea_ebx__edx_ecx_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x004860E8, _lea_ebx__esi_eax_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x004C83DA, _lea_ecx__eax_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x005BE9A6, _lea_ecx__eax_ecx_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x004E7715, _lea_ecx__eax_ecx_HeroesOffset_);
    */
    {
        int addr[] = { 0x0052B48F, 0x005683A6, 0x004860E8, 0x005BE9A6, 0x004E7715
        };
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }
    Z_Heroine->WriteDword(0x004C83DA + 2, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = { 0x005AEEFB, 0x005AEF23, 0x004C676F, 0x00432048,
            0x004DE61A, 0x005BE4A8, 0x005D843F, 0x0052210B, 0x00409097,
            0x004812E5, 0x005D853E, 0x00487652, 0x005D2238
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_ecx__eax_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    // Z_Heroine->WriteLoHook(0x004C94BD, _lea_ecx__ebx_HeroesOffset_);
    Z_Heroine->WriteDword(0x004C94BD + 2, (int)Heroine::heroes - (int)h3::H3Main::Get());
    //Z_Heroine->WriteLoHook(0x004C94E3, _lea_ecx__ebx_HeroesOffset_);
    Z_Heroine->WriteDword(0x004C94E3 + 2, (int)Heroine::heroes - (int)h3::H3Main::Get());

    // Z_Heroine->WriteLoHook(0x005BFE0F, _lea_ecx__ebx_eax_HeroesOffset_);
    Z_Heroine->WriteDword(0x005BFE0F + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    //Z_Heroine->WriteLoHook(0x005EA3AE, _lea_ecx__ebx_eax_HeroesOffset_);
    Z_Heroine->WriteDword(0x005EA3AE + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    //Z_Heroine->WriteLoHook(0x004BFF35, _lea_ecx__ebx_ecx_HeroesOffset_);
    Z_Heroine->WriteDword(0x004BFF35 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    //Z_Heroine->WriteLoHook(0x005BFD69, _lea_ecx__ebx_ecx_HeroesOffset_);
    Z_Heroine->WriteDword(0x005BFD69 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = { 0x004316EA, 0x004534D6, 0x004F1A4B, 0x00480FFA,
            0x004BA99F, 0x0051F687, 0x005222BD, 0x004199F5, 0x0051FA10,
            0x0052163E, 0x005D80C3, 0x0040625B, 0x00406284, 0x00527E84
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_ecx__ecx_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x00431633, 0x0049E088, 0x004E86D8, 0x005BE7EF, 0x005DF460 };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_ecx__ecx_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }
    
    /*
    Z_Heroine->WriteLoHook(0x004CAC54, _lea_ecx__edi_eax_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x005EA231, _lea_ecx__edi_eax_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x004E8ABF, _lea_ecx__edi_ecx_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x004C9EA5, _lea_ecx__edi_edx_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x005BE87B, _lea_ecx__edx_eax_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x004062BB, _lea_ecx__edx_eax_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x0056A896, _lea_ecx__edx_eax_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x004062A6, _lea_ecx__edx_eax_HeroesOffset_);
    */
    {
        int addr[] = { 0x004CAC54, 0x005EA231, 0x004E8ABF, 0x004C9EA5,
            0x005BE87B, 0x004062BB, 0x0056A896, 0x004062A6
        };
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x00433B02, 0x005F1D0B, 0x0047F11D, 0x0047FB87,
            0x004801BA, 0x004803CA, 0x00480644, 0x004BAA05, 0x004F318F,
            0x00522AD7, 0x005C173D, 0x005D89D6, 0x004080EC, 0x0040F10B,
            0x00453193, 0x00480B0A, 0x0040AF7C, 0x0040AFF5, 0x0048190E
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_ecx__edx_ecx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x0040FCA8, 0x00483C46, 0x004E87BC, 0x005BE7BE,
            0x004C7767
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_ecx__esi_ecx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    // Z_Heroine->WriteLoHook(0x004C76D6, _lea_ecx__esi_edx_HeroesOffset_);
    Z_Heroine->WriteDword(0x004C76D6 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    // Z_Heroine->WriteLoHook(0x005F1B9D, _lea_edi__eax_ecx_HeroesOffset_);
    Z_Heroine->WriteDword(0x005F1B9D + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = { 0x0040315B, 0x005C7373, 0x005CE9C1  };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_edi__eax_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    // Z_Heroine->WriteLoHook(0x004BE501, _lea_edi__ebx_HeroesOffset_);
    // Z_Heroine->WriteDword(0x004BE501 + 2, (int)Heroine::heroes - (int)h3::H3Main::Get());

    // Z_Heroine->WriteLoHook(0x004C709C, _lea_edi__ebx_ecx_HeroesOffset_);
    Z_Heroine->WriteDword(0x004C709C + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = { 0x004B9CCB, 0x00408BFA,0x00408E2B, 0x0047F1DB,
            0x005CE99D, 0x005F1DE1
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_edi__ecx_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x0040E0F4, 0x0047F69A, 0x005D8773 };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_edi__ecx_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    // Z_Heroine->WriteLoHook(0x0056E9A4, _lea_edi__edi_eax_HeroesOffset_);
    Z_Heroine->WriteDword(0x0056E9A4 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = { 0x004C82A5, 0x004E8AD9, 0x005DE4AB };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_edi__edi_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x0040EC31, 0x0048181C, 0x0056E579, 0x005722A9, 0x005BE89B };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_edi__edx_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x00417B74, 0x0040E607, 0x0041763E, 0x004AA735, 0x005C7253 };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_edi__edx_ecx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    Z_Heroine->WriteDword(0x0040E5FB + 4, 2 + ((int)Heroine::heroes - (int)h3::H3Main::Get()));

    // Z_Heroine->WriteLoHook(0x004BD0AB, _lea_edi__esi_HeroesOffset_);
    // Z_Heroine->WriteDword(0x004BD0AB + 2, (int)Heroine::heroes - (int)h3::H3Main::Get());

    // Z_Heroine->WriteLoHook(0x004E87D6, _lea_edi__esi_edx_HeroesOffset_);
    Z_Heroine->WriteDword(0x004E87D6 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = { 0x004BA939, 0x00526526 };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_edx__eax_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    // Z_Heroine->WriteLoHook(0x005C7291, _lea_edx__ebx_edx_HeroesOffset_);
    Z_Heroine->WriteDword(0x005C7291 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = { 0x00413784, 0x004E1AD7, 0x0051D5B6, 0x00572471 };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_edx__ecx_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    //Z_Heroine->WriteLoHook(0x005EA4A8, _lea_edx__edi_ecx_HeroesOffset_);
    Z_Heroine->WriteDword(0x005EA4A8 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    //Z_Heroine->WriteLoHook(0x0052B1A4, _lea_edx__edi_edx_HeroesOffset_);
    Z_Heroine->WriteDword(0x0052B1A4 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = { 0x004BA8C6, 0x005C17D2, 0x0040E583 };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_edx__edx_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    // Z_Heroine->WriteLoHook(0x004AAD33, _lea_edx__edx_ecx_HeroesOffset_);
    Z_Heroine->WriteDword(0x004AAD33 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    // Z_Heroine->WriteLoHook(0x004E7FB5, _lea_edx__edx_ecx_HeroesOffset_);
    Z_Heroine->WriteDword(0x004E7FB5 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = { 0x0041C6B2, 0x0041C862, 0x0040247D, 0x0040B126, 0x0041C4EE,
            0x004BA767, 0x0051F3C0, 0x00521741, 0x005D3513, 0x005D7E84
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_esi__eax_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    // Z_Heroine->WriteLoHook(0x004BFB47, _lea_esi__ebx_HeroesOffset_);
    Z_Heroine->WriteDword(0x004BFB47 + 2, (int)Heroine::heroes - (int)h3::H3Main::Get());
    
    {
        int addr[] = { 0x004C0044, 0x004C7160 };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_esi__ebx_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    /*
    Z_Heroine->WriteLoHook(0x004C8036, _lea_esi__ebx_ecx_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x004C70EF, _lea_esi__ebx_edx_HeroesOffset_);
    Z_Heroine->WriteLoHook(0x004C809E, _lea_esi__ebx_edx_HeroesOffset_);
    */
    {
        int addr[] = { 0x004C8036, 0x004C70EF, 0x004C809E };
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x0041C943, 0x00569DB0, 0x0040BBD4, 0x00414555, 0x0041652D,
            0x0041D034, 0x0042F6F7, 0x0047FF58, 0x0051CBB3, 0x005217A9, 0x005257FB,
            0x00526FC0, 0x0052A1C9, 0x005D1D62, 0x005D34EF, 0x005DF3DF
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_esi__ecx_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    {
        int addr[] = { 0x00486BB0, 0x0041D259, 0x0041D544, 0x004A3CAC,
            0x0052137B, 0x005F78DA, 0x005F7CDA
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_esi__ecx_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    /*
    Z_Heroine->WriteLoHook(0x00407B19, _lea_esi__edi_eax_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x004CAB9E, _lea_esi__edi_ecx_HeroesOffset_);

    Z_Heroine->WriteLoHook(0x005270E7, _lea_esi__edx_eax_HeroesOffset_);
    */
    {
        int addr[] = { 0x00407B19, 0x004CAB9E, 0x005270E7 };
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }
    
    {
        int addr[] = { 0x0041C7A4, 0x0040FDBA, 0x0041024A, 0x0041711D, 0x0041C63C,
            0x00428175, 0x0048124A, 0x004A249F, 0x004BA7BB, 0x0051C90D
        };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_esi__edx_ecx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    // Z_Heroine->WriteLoHook(0x0042DEE8, _lea_esi__esi_eax_HeroesOffset_);
    Z_Heroine->WriteDword(0x0042DEE8 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    // Z_Heroine->WriteLoHook(0x004F53B3, _lea_esi__esi_ecx_HeroesOffset_);
    Z_Heroine->WriteDword(0x004F53B3 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    {
        int addr[] = { 0x00483CCE, 0x004F540C, 0x004F5481 };
        // for (int i : addr) Z_Heroine->WriteLoHook(i, _lea_esi__esi_edx_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }

    //Z_Heroine->WriteLoHook(0x00417B7B, _mov_cx__edx_ecx_HeroesOffset_);
    //Z_Heroine->WriteLoHook(0x004199ED, _mov_dx__ecx_eax_HeroesOffset_);
    Z_Heroine->WriteDword(0x00417B7B + 4, (int)Heroine::heroes - (int)h3::H3Main::Get());
    Z_Heroine->WriteDword(0x004199ED + 4, (int)Heroine::heroes - (int)h3::H3Main::Get());

    // added missing ones

    // Z_Heroine->WriteLoHook(0x004CDE34, _lea_eax__esi_HeroesOffset_);
    Z_Heroine->WriteDword(0x004CDE34 + 2, (int)Heroine::heroes - (int)h3::H3Main::Get());

    //Z_Heroine->WriteLoHook(0x004CE4F9, _lea_edx__esi_HeroesOffset_);
    Z_Heroine->WriteDword(0x004CE4F9 + 2, (int)Heroine::heroes - (int)h3::H3Main::Get());

    // ----------------

    Z_Heroine->WriteDword(0x004C941D + 2, (int)Heroine::heroes + 0x91 - (int)h3::H3Main::Get());
    Z_Heroine->WriteDword(0x0058D579 + 3, (int)Heroine::heroes + 0x23 - (int)h3::H3Main::Get());
    Z_Heroine->WriteDword(0x0058D697 + 3, (int)Heroine::heroes + 0x23 - (int)h3::H3Main::Get());

    
    {
        int addr[] = { 0x004BB183, 0x004BB305, 0x004BB49C };
        for (int i : addr) Z_Heroine->WriteDword(i + 2, 
            (int)Heroine::heroes + 0x30 - (int)h3::H3Main::Get());
    }

    // Heroine::heroes end
    once = false;
}

void InstallHeroesWoG() {
    
    // Heroine::heroes wog area
    {
        int addr[] = { 0x62C9C3, 0x62CB5F, 0x70AE86, 0x70D275, 0x7116C8, 0x754702 };
        //// for (int i : addr) Z_Heroine->WriteLoHook(i, _add_eax_HeroesOffset_);
        for (int i : addr) Z_Heroine->WriteDword(i + 1, (int)Heroine::heroes - (int)h3::H3Main::Get());
    }
    
    Z_Heroine->WriteDword(0x749A2E + 2, (int)Heroine::heroes - (int)h3::H3Main::Get());

    // Z_Heroine->WriteLoHook(0x71647E, _lea_esi__eax_edx_HeroesOffset_);
    Z_Heroine->WriteDword(0x71647E + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    // Z_Heroine->WriteLoHook(0x7571B0, _lea_esi__ecx_eax_HeroesOffset_);
    Z_Heroine->WriteDword(0x7571B0 + 3, (int)Heroine::heroes - (int)h3::H3Main::Get());

    
    
}

void Install_HeroNames() {
    static char unk[] = "Unknown";
    int ii = 0;

    /*
    for (int i = OldHeroCount; i < MaxHeroCount; ++i) {
        auto name= &Heroine::HeroInfo[i].name;
        if (!*name) *name = unk;
    }
    return; // code below doesn't do anything visible
    
    int ii = 0;
    for (auto& i : Heroine::heroes) {
        strcpy(i.name, (ii < OldHeroCount) ?
            ((char*)old_heroname_wog[ii]) : hlp_Unnamed);
        ++ii;
    }
    */

    ii = 0;
    for (auto& i : Heroine::HeroInfo) {
        // if(!i.name) i.name = hlp_Unnamed;
        auto str = (ii < OldHeroCount) ?
            ((char*)old_heroname_wog[ii]) : hlp_Unnamed;
        set_string(const_cast<char**>(&i.name), str, 16);

        /*
        if (!i.name) i.name = (ii < OldHeroCount) ?
            ((char*)old_heroname_wog[ii]) : hlp_Unnamed;
        */
        ++ii;
    }

    ii = 0;
    for (auto& i :
        // h3::H3Main::Get()->heroSetup
        Heroine::heroSetup
        ) {

        auto str = (ii < OldHeroCount) ?
            ((char*)old_heroname_wog[ii]) : hlp_Unnamed;
        // set_string(const_cast<char**>(&i.name), str, 12 + 1);
        strcpy(i.name, str);

        /*
        strcpy(i.name, (ii < OldHeroCount) ?
            ((char*)old_heroname_wog[ii]) : hlp_Unnamed);
        */
        
        ++ii;
    }

}

void Install_HeroSpec() {
    static bool once = true;

    if (once) memcpy(Heroine::heroSpecialty,
        *(void**)0x00679C80, // (void*)0x00678420,
        OldHeroCount * sizeof(h3::H3HeroSpecialty));
    Z_Heroine->WriteDword(0x00679C80 + 0, (int)Heroine::heroSpecialty);
    Z_Heroine->WriteDword(0x00751536 + 2, (int)Heroine::heroSpecialty);
    // Z_Heroine->WriteDword(0x678420, sizeof());

    Z_Heroine->WriteDword(0x00752345 + 1, sizeof(Heroine::HSpecNames));
    Z_Heroine->WriteDword(0x0075155A + 1, sizeof(Heroine::HSpecNames));

    Z_Heroine->WriteDword(0x007335FB + 2, (int)Heroine::HSpecNames);
    Z_Heroine->WriteDword(0x00733630 + 2, (int)Heroine::HSpecNames);
    Z_Heroine->WriteDword(0x00750378 + 2, (int)Heroine::HSpecNames);
    Z_Heroine->WriteDword(0x0075039F + 2, (int)Heroine::HSpecNames);

    Z_Heroine->WriteDword(0x0075155F + 1, (int)Heroine::HSpecNames);
    Z_Heroine->WriteDword(0x0075234A + 1, (int)Heroine::HSpecNames);
    Z_Heroine->WriteDword(0x00752F67 + 2, (int)Heroine::HSpecNames);

    Z_Heroine->WriteDword(0x00733644 + 3, (int)Heroine::HSpecNames+4);
    Z_Heroine->WriteDword(0x00733677 + 3, (int)Heroine::HSpecNames+4);
    Z_Heroine->WriteDword(0x007523AE + 3, (int)Heroine::HSpecNames+4);
    Z_Heroine->WriteDword(0x00752F92 + 3, (int)Heroine::HSpecNames+4);

    // 0x0091DA78
    Z_Heroine->WriteDword(0x00750E62 + 2, (int)Heroine::HSpecBack);
    Z_Heroine->WriteDword(0x00750E2D + 2, (int)Heroine::HSpecBack);
    Z_Heroine->WriteDword(0x00750E53 + 3, sizeof(Heroine::HSpecBack));
    Z_Heroine->WriteDword(0x00750E18 + 3, sizeof(Heroine::HSpecBack));

    Z_Heroine->WriteDword(0x00733765 + 2, (int)&Heroine::HSpecBack->spShort);
    Z_Heroine->WriteDword(0x0073377F + 2, (int)&Heroine::HSpecBack->spFull);
    Z_Heroine->WriteDword(0x00733799 + 2, (int)&Heroine::HSpecBack->spDescr);

    once = false;
}

char* __stdcall Hero_GetClassName_hook(HiHook* h, h3::H3Hero* hero) {
    static char nobody[16] = "Nobody";
    static char unknown[16] = "Unknown";

    if (hero->hero_class < 0 || hero->hero_class >= MaxHeroClassCount)
        return nobody;
    char* class_name = Heroine::HeroTypes[hero->hero_class].TypeName;
    if (class_name) return class_name;
    else return unknown;
}

namespace WoG {
    struct VarNum {
        signed     Num : 32; // ����, ����������  ��� �����
// #define VN_MAXVAL 0x7FFFFFFF// 2147483647
// #define VN_MINVAL ((int)0x80000000)//-2147483647
        unsigned   Type : 4; // ��� �����
        // 0=�����, 1=����, 2=f...t, 3=v1...1000, 4=w1...100, 5=x1...100, 6=y1...100
        // 7=z1...500,8=e1...e100
        // ******* 8=Scope v1...1000, 9=Scope z1...z500
        unsigned   IType : 4;
        // indexed
        // 0=���, 1=����, 2=f...t, 3=v1...1000, 4=w1...100, 5=x1...100, 6=y1...100
        // ******* 8=Scope v1...1000 float
        unsigned   Check : 3; // ��� ��������
        // 0=nothing, 1?, 2=, 3<>, 4>, 5<, 6>=, 7<=
      
    };
    struct _Mes_ {
        char* s;
        long  l;
    };
    struct Mes {
        long  i;    // ������ ��� �������� �������
        _Mes_ m;
        //  VarNum Efl [16][2];
        //  VarNum Ofl [16][2];      // �� 16 ������ ����������� |
        VarNum Efl[2][16][2];      // �� 16 ������ ����������� & � |
        VarNum VarI[16];
        char  c[16];
        char  f[16];
        int   n[16];
    };
}
__declspec(naked) int __cdecl ApplyERMArgument_Bridge(void* dp, char size, WoG::Mes* mp, char ind) {
    __asm {
        push ebp
        mov ebp, esp
        sub esp, 0x2C

        push 0x00741963
        ret
    }
}
int  __stdcall ApplyERMArgument_hook(HiHook* h, void* dp, char size, WoG::Mes* mp, char ind) {
    // int addr = h->GetDefaultFunc();
    // return CDECL_4(int, addr, dp, size, mp, ind);

    int v, vf, ivf, p, fl, ret, vi, chk;
    chk = mp->VarI[ind].Check;
    if (chk != 0) {
        vf = mp->VarI[ind].Type;
        ivf = mp->VarI[ind].IType;

        if( (vf & 0xf) == 4 || (ivf & 0xf) == 4)
            return CDECL_4(int, ApplyERMArgument_Bridge, dp, size, mp, ind);
    }
    
    int addr = h->GetDefaultFunc();
    return CDECL_4(int,addr,dp,size,mp,ind);
}

int __stdcall GetNum_hook(HiHook* h, WoG::Mes* ms, int ind, int immed) {
    char* s = ms->m.s; long l = ms->m.l; long i = ms->i;
    WoG::VarNum* mvi = &ms->VarI[ind];
    if(mvi->Type == 4)
        return CDECL_3(int, h->GetOriginalFunc(), ms, ind, immed);

    int addr = h->GetDefaultFunc();
    return CDECL_3(int, addr, ms, ind, immed);
}

void install_ERM_Hooks() {
    // return; // debug

    h3::H3DLL era_dll = h3::H3DLL::H3DLL("era.dll");

    if(true)
    {
        UINT8 pattern[4] = { 0x10, 0xAB, 0xA4,0x00 };
        int PatchAddress = era_dll.NeedleSearchData(pattern, 4);
        while (PatchAddress) {
            Z_Heroine->WriteDword(PatchAddress, (int)Heroine::ERMVarH);
            PatchAddress = era_dll.NeedleSearchData(pattern, 4);
        }
    }

    if (false)
    {
        UINT8 pattern[4] = { 0x0C, 0xAB, 0xA4,0x00 };
        int PatchAddress = era_dll.NeedleSearchData(pattern, 4);
        while (PatchAddress) {
            Z_Heroine->WriteDword(PatchAddress, (int)Heroine::ERM_ZERO);
            PatchAddress = era_dll.NeedleSearchData(pattern, 4);
        }
    }

    // Z_Heroine->WriteHiHook(0x0074195D, SPLICE_, SAFE_, CDECL_, ApplyERMArgument_hook);
    // Z_Heroine->WriteHiHook(0x0073E970, SPLICE_, SAFE_, CDECL_, GetNum_hook);
}

void __stdcall LateLoad(Era::TEvent* Event) {
    static bool once = true;

    // Z_Heroine->WriteHexPatch(0x00587FD0, "558BEC83EC08");

    install_PicsBack();

    Heroine::old_erm_buffer = *(int*)0x00A4AB0C;

    Install_HeroInfo();
    Install_HeroInfo_late();

    // install_MISC();
    Install_HeroSpec();

    // Z_Heroine->WriteDword(0x678420, sizeof());

   
    // Install_heroCount();

    // configure_all_heroes();

    // configure_all_heroes();
    // InstallHeroesWoG();

    /*
    InstallHeroesSod();

    InstallHeroesWoG();
    
    Install_heroMayBeHiredBy();
    Install_heroOwner();

    // Install_HeroInfo();

    Install_heroSetup();
    */

    /*
    memcpy((void*)Heroine::HeroInfo, old_HeroInfo_ptr,
        sizeof(h3::H3HeroInfo) * OldHeroPortraitsCount);
    */

    /*
    Z_Heroine->WriteDword(0x0067DCE8, (int)Heroine::HeroInfo);

    Z_Heroine->WriteDword(0x00679C80, (int)Heroine::heroSpecialty);
    */

    Z_Heroine->WriteHiHook(0x004D91E0, SPLICE_, EXTENDED_, THISCALL_, Hero_GetClassName_hook);
    // InstallHeroesSpecialtyGfx();

    once = false;
}

_LHF_(hook_004EE15B) {
    // install_PostERM_buffer();
    install_ERM_Hooks();
    // Z_Heroine->WriteHexPatch(0x0074195D, "55 8B EC 83 EC 2C");

    Install_heroCount();

    InstallHeroesSod();

    InstallHeroesWoG();

    Install_ERM();

    Install_heroMayBeHiredBy();
    Install_heroOwner();

    Install_heroCount();

    Install_heroSetup();

    // configure_all_heroes();

    install_PostERM_buffer();
    return EXEC_DEFAULT;
}

_LHF_(hook_004FBA1C) {

    Install_HeroNames();

    // configure_all_heroes();

    return EXEC_DEFAULT;
}

_LHF_(hook_004EE010) {

    // Install_HeroInfo();

    Install_heroSetup();

    // configure_all_heroes();

    return EXEC_DEFAULT;
}

/*
_LHF_(hook_004EE357) {
    // disabled

    InstallHeroesSod();

    InstallHeroesWoG();

    Install_ERM();

    Install_heroMayBeHiredBy();
    Install_heroOwner();

    // Install_HeroInfo();

    // Install_heroSetup();

    // configure_all_heroes();

    return EXEC_DEFAULT;
}
*/

void __stdcall EarlyLoad(Era::TEvent* Event) {
    // Install_HeroInfo();
    // InstallHeroesSod();
    /*
    memcpy((void*)Heroine::HeroInfo, (void*)0x00679DD0,
        sizeof(h3::H3HeroInfo) * OldHeroCount);

    memcpy(Heroine::heroSpecialty, (void*)0x00678420,
        OldHeroCount * sizeof(h3::H3HeroSpecialty));
        */


}

_LHF_(hook_0070F9EC) {
    void* a1 = *(void**)(c->ebp + 8);
    if ((int) a1 > 1024) return EXEC_DEFAULT;

    h3::H3Messagebox("Warning: Wrong Call to 0x0070F9E3");

    c->eax = 0; c->return_address = 0x0070FB5C;
    return SKIP_DEFAULT;
}

_LHF_(hook_004D8AB6) {
    static char zdefault[13] = "default";
    char** hero_name_out = (char**)(c->esi + c->ecx + 0x40);

    if (1024 > (int) *hero_name_out) {
        // Install_HeroInfo();
        Install_HeroNames();
    }

    if (1024 > (int) *hero_name_out) {
        c->Push(0xD);
        c->eax = c->ebx + 0x23;
        c->edx = (int) zdefault;
        c->return_address = 0x004D8ABF;
        return SKIP_DEFAULT;
    }

    return EXEC_DEFAULT;
}

_LHF_(hook_configure) {
    install_MISC();

    Install_ERM();

    install_Biographies();

    // Z_Heroine->WriteDword(0x00679C80, (int)Heroine::heroSpecialty);
    configure_all_heroes();
    Install_heroCount();

    install_PostERM_buffer();

    // Z_Heroine->WriteHexPatch(0x00587FD0, "558BEC83EC08");
    // Z_Heroine->WriteHiHook(0x004D91E0, SPLICE_, EXTENDED_, THISCALL_, Hero_GetClassName_hook);

    InstallHeroesSpecialtyGfx();

    return EXEC_DEFAULT;
}

_LHF_(hook_0074A0CA) {

    if (false && !c->edx) {
        int &PERM_v32 = *(int*)(c->ebp - 0x36C);
        int var_k = *(int*)(c->ebp - 0x364 );
        // install_PostERM_buffer();

        PERM_v32 = (int) 0x0091F6C8;
        c->edx = var_k + PERM_v32;
        /*
        PERM_v32 = (int)Heroine::PostERM_buffer;
        c->edx = var_k + (int) Heroine::PostERM_buffer;
        */
    }

    return EXEC_DEFAULT;
}

_LHF_(hook_004CDC20) {
    static bool once = true;

    if (once) {
        once = false;
        return EXEC_DEFAULT;
    }
    else {
        c->return_address = 0x004CDC43;
        return SKIP_DEFAULT;
    }
}
_LHF_(hook_004CE575) {
    static bool once = true;

    if (false && once) {
        once = false;
        return EXEC_DEFAULT;
    }
    else {
        c->return_address = 0x004CE593;
        return SKIP_DEFAULT;
    }
}

_LHF_(hook_004CDE25) {
    static bool once = true;

    if (once) {
        once = false;
        return EXEC_DEFAULT;
    }
    else {
        memset(Heroine::heroMayBeHiredBy,0,sizeof(Heroine::heroMayBeHiredBy));
        c->return_address = 0x004CDE6E; // 0x004CDE49;
        return SKIP_DEFAULT;
    }
}
_LHF_(hook_004CE4EF) {
    static bool once = true;

    if (false && once) {
        once = false;
        return EXEC_DEFAULT;
    }
    else {
        c->return_address = 0x004CE50E;
        return SKIP_DEFAULT;
    }
}

_LHF_(hook_004CE040) {
    c->edi = (int) Heroine::heroOwner;
    return SKIP_DEFAULT;
}
_LHF_(hook_004CE07A) {
    c->edi = (int) Heroine::heroOwner;
    return SKIP_DEFAULT;
}

_LHF_(hook_00577A4F) {

    c->return_address = 0x00577A71;
    return SKIP_DEFAULT;
}


// Hero_Reset
_LHF_(hook_004D899B) {
    return EXEC_DEFAULT;

    short heroID = *(short*)(c->ebp+8);
    h3::H3Hero* hero = (h3::H3Hero*) c->ecx;
    if (heroID == 0) {
        Install_heroCount();
        Install_HeroInfo();

        configure_all_heroes();
    }

    auto& nfo = Heroine::HeroInfo[heroID];
    if (!nfo.name) {
        // Install_heroCount();
        // Install_HeroInfo();
        // set_string(const_cast<char**>(& nfo.name), "Unknown", 12 + 1);
        // configure_all_heroes();
        strcpy(Heroine::heroes[heroID].name, "Buggy");
        set_string(const_cast<char**>(&nfo.name), "Buggy", 16);
    }
    // Heroine::heroes[heroID].name

    return EXEC_DEFAULT;

    Install_heroCount();
    // Install_HeroInfo();
    return EXEC_DEFAULT;
}
_LHF_(hook_004D8A89) {
    int heroID = *(int*)(c->ebp + 8);
    if (heroID >= OldHeroCount) {
        // *(char*)(c->ebx + 0x34) = OldHeroCount;
        *(char*)(c->ebx + 0x34) = (heroID < NewHeroPortraitsCount) ? heroID : OldHeroCount;
        *(int*)(c->ebx + 0x1A) = heroID;
        *(int*)(c->ebx + 0x1E) = heroID;
    }

    return EXEC_DEFAULT;
}

_LHF_(hook_00749A19) {
    *(int*)(c->ebp - 0x0368) = (int) Heroine::heroSetup;
    *(int*)(c->ebp - 0x0004) = (int) Heroine::heroes;

    c->return_address = 0x00749A37;
    return SKIP_DEFAULT;
}

_LHF_(hook_00769B41) {
    auto me = (NPC*) c->ecx;
    if (me->Number >= OldHeroCount) {
        c->return_address = 0x00769B57;
        c->eax = (int) hlp_Unnamed;
        return SKIP_DEFAULT;
    }
    else return EXEC_DEFAULT;
}

_LHF_(hook_004BEF5C) {
    c->edi = (int) Heroine::heroMayBeHiredBy;
    return SKIP_DEFAULT;
}
_LHF_(hook_004BEF54) {
    c->ebx = (int)Heroine::heroSetup;
    return SKIP_DEFAULT;
}
_LHF_(hook_004BEF6E) {
    c->eax = (int)Heroine::heroes;
    return SKIP_DEFAULT;
}
_LHF_(hook_004BEF43) {
    c->edi = (int)Heroine::heroOwner;
    memset((void*)c->edi, 0xFFu, MaxHeroCount);
    return SKIP_DEFAULT;
}

_LHF_(hook_004BCFA6) {
    return EXEC_DEFAULT;

    c->eax = (int) Heroine::heroMayBeHiredBy;
    *(int*)(c->ebp - 0x05CC) = c->eax;
    return EXEC_DEFAULT;
}


_LHF_(hook_004BD0AB) {
    c->edi = (int) Heroine::heroes;
    return SKIP_DEFAULT;
}
_LHF_(hook_004BE501) {
    c->edi = (int)Heroine::heroes;
    return SKIP_DEFAULT;
}
_LHF_(hook_00749A13) {
    c->ecx = Heroine::ERM_ZERO;

    if (!c->ecx) {
        // CDECL_0(int, 0x73E1AA);
        // c->ecx = *(int*)0x00A4AB0C;

        // c->ecx = 0x0091F6C8;

        install_PostERM_buffer();
        // c->ecx = Heroine::old_erm_buffer;
        c->ecx = Heroine::ERM_ZERO;

        // c->ecx = (int) Heroine::PostERM_buffer;
    }
    return EXEC_DEFAULT;
}

_LHF_(hook_00407432) {
    auto& plr_ptr = *(h3::H3Player**)0x0069CCFC;
    if (!plr_ptr) {
        auto Game = h3::H3Game::Get();
        plr_ptr = &Game->players[Game->GetPlayerID()];
    }

    return EXEC_DEFAULT;
}

_LHF_(hook_004BE55F) {
    c->eax = (int)Heroine::heroOwner;
    return SKIP_DEFAULT;
}
_LHF_(hook_004BE591) {
    c->ecx = (int)Heroine::heroMayBeHiredBy;
    return SKIP_DEFAULT;
}


_LHF_(hook_00704FF2) {
    install_PostERM_buffer();
    return EXEC_DEFAULT;
}

_LHF_(hook_004BD165) {
    c->eax = (int)Heroine::heroOwner;
    return SKIP_DEFAULT;
}


_LHF_(hook_004E1EE1) {
    auto Hero = (h3::H3Hero*) c->ecx;
    if (Hero->id >= OldHeroCount) c->eax = Hero->id;
    return EXEC_DEFAULT;
}

void  __stdcall PrepareSpecWoG_hook(HiHook* h, int WoG) {
    static bool done = false;


    // if (done) return;

    if (false) {
        // configure_all_heroes();

        // CDECL_0(DWORD*, 0x753B98);
        
        if (!Heroine::HeroInfo[MaxHeroCount - 1].smallPortrait)
            return;

        for (int i = OldHeroCount; i < MaxHeroCount; ++i) {
            RefreshPicsBack(i);
            RefUp_PicsBack(i);
        }


        done = true;
    }

    // CDECL_1(void, h->GetDefaultFunc(), WoG);
    // Z_Heroine->WriteDword(0x28077CC, 1);
}

/*
_LHF_(hook_004D5B23) {
    configure_all_heroes();
    return EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_0058D9B0) {
    c->ecx = (int) Heroine::PicsBack[c->eax].HPSLoaded;
    return SKIP_DEFAULT;
}

_LHF_(hook_0058D98D) {

    c->return_address = 0x0058D9C0;
    return SKIP_DEFAULT;
}
*/

_LHF_(hook_0058DEFD) {
    //int plr_ID = c->edi;
    auto dlgpo = c->esi;
    auto &pcx = *(h3::H3LoadedPcx**)(dlgpo + 0xc8);

    c->Pop(); c->eax = (int) THISCALL_2(char*, 0x0058D4C0, c->ecx, c->edi);

    /*
    if (!pcx) {
        pcx = h3::H3LoadedPcx::Load("HPS136Wi.pcx");
    }
    */
    int dy = (h3::H3GameHeight::Get() - 600) >> 1;
    int dx = (h3::H3GameWidth::Get() - 800) >> 1;
    if (dx) dx += 15;

    auto var_8  = *(int*)(c->ebp - 0x08);
    auto var_14 = *(int*)(c->ebp - 0x14);
    // auto pcx2 = h3::H3LoadedPcx::Load("HPS136Wi.pcx");
    auto pcx2 = (h3::H3LoadedPcx*)Heroine::PicsBack[var_14].HPSLoaded;
    //auto pcx2 = h3::H3LoadedPcx::Load(Heroine::HeroInfoBack[var_14].smallPortrait);
    auto wnd_mgr = h3::H3WindowManager::Get();
    if(pcx2) pcx2->DrawToPcx16( wnd_mgr->screenPcx16,
      dx + 0xFC , dy + var_8 + 0x82 ,0);
    /*
    if (c->eax < 1024) {
        h3::H3Hero* her = (h3::H3Hero*) c->edi;
        c->eax = (int) Heroine::PicsBack[her->picture].HPLLoaded;
    }
    */

    *(int*)(c->ebp + 0x10) = c->eax;
    c->return_address = 0x0058DF41;
    return SKIP_DEFAULT;
}

_LHF_(hook_004C9E51) {
    Heroine::heroOwner[c->esi] = c->BL();
    return SKIP_DEFAULT;
}

extern signed int __stdcall GameMgr_GetRandomHero_hook(
    HiHook* h, h3::H3Game* ptr, unsigned int player,
    signed int a3, char a4, signed int HeroType);

extern int  __stdcall GameMgr_GetRandomHeroSimple_hook(HiHook* h,
    h3::H3Game* ptr, int town_type, unsigned int a3, int a4);

extern int __stdcall ChangeTavernHeroes_hook(HiHook* h,
    h3::H3Game* ptr, unsigned int player);

extern int __stdcall Tavern_Generate_One_New_Hero_hook(HiHook* h,
    h3::H3Main* MainStructure, int color_ID, int Left_or_Right_HeroInTavern);


extern void InstallStartingHeroes();

_LHF_(hook_004E1DD9) {
    c->esi = (int) Heroine::HeroTypes;
    return SKIP_DEFAULT;
}

void LoadGlobalConfig(void);

void __stdcall CloseCastleGate_70A146(HiHook* h, signed int a1, unsigned __int8* a2) {
    return;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH: {

        globalPatcher = GetPatcher();
        Z_Heroine= globalPatcher->CreateInstance(PINSTANCE_MAIN);
        Era::ConnectEra(hModule,PINSTANCE_MAIN);


        Z_Heroine->WriteHiHook(0x70A146, SPLICE_, EXTENDED_, CDECL_, CloseCastleGate_70A146);

        LoadGlobalConfig();

        install_MISC();

        Z_Heroine->WriteLoHook(0x004E1DD9, hook_004E1DD9);

        Z_Heroine->WriteLoHook(0x004E1EE1, hook_004E1EE1);

        Z_Heroine->WriteLoHook(0x004BD165, hook_004BD165);

        // Z_Heroine->WriteLoHook(0x00704FF2, hook_00704FF2);

        Z_Heroine->WriteLoHook(0x004BE55F, hook_004BE55F);
        Z_Heroine->WriteLoHook(0x004BE591, hook_004BE591);

        // Z_Heroine->WriteDword(0x00A4AB0C,  0x0091F6C8);
        Z_Heroine->WriteLoHook(0x00407432, hook_00407432);
        // Z_Heroine->WriteLoHook(0x00749A13, hook_00749A13);

        Z_Heroine->WriteLoHook(0x004BD0AB, hook_004BD0AB);
        Z_Heroine->WriteLoHook(0x004BE501, hook_004BE501);

        Z_Heroine->WriteLoHook(0x004BCFA6, hook_004BCFA6);

        Z_Heroine->WriteLoHook(0x004BEF6E, hook_004BEF6E);
        Z_Heroine->WriteLoHook(0x004BEF5C, hook_004BEF5C);
        Z_Heroine->WriteLoHook(0x004BEF54, hook_004BEF54);
        Z_Heroine->WriteLoHook(0x004BEF43, hook_004BEF43);

        Z_Heroine->WriteLoHook(0x00769B41, hook_00769B41);

        Z_Heroine->WriteLoHook(0x00749A19, hook_00749A19);

        Z_Heroine->WriteLoHook(0x004D899B, hook_004D899B);

        Z_Heroine->WriteLoHook(0x00577A4F, hook_00577A4F);

        Z_Heroine->WriteLoHook(0x004CE07A, hook_004CE07A);
        Z_Heroine->WriteLoHook(0x004CE040, hook_004CE040);

        Z_Heroine->WriteLoHook(0x004CDE25, hook_004CDE25);
        Z_Heroine->WriteLoHook(0x004CE4EF, hook_004CE4EF);

        Z_Heroine->WriteLoHook(0x004CDC20, hook_004CDC20);
        Z_Heroine->WriteLoHook(0x004CE575, hook_004CE575);

        Z_Heroine->WriteLoHook(0x004D8AB6, hook_004D8AB6);
        Z_Heroine->WriteLoHook(0x0074A0CA, hook_0074A0CA);

        // Install_HeroInfo();
        // Z_Heroine->WriteHiHook(0x004E6850, SPLICE_, EXTENDED_, CDECL_,Load_Hotraits_TXT_hook);
        // Z_Heroine->WriteLoHook(0x004EE357, hook_004EE357);
        Z_Heroine->WriteLoHook(0x004EE010, hook_004EE010);
        Z_Heroine->WriteLoHook(0x004FBA1C, hook_004FBA1C);

        Z_Heroine->WriteLoHook(0x0070F9EC, hook_0070F9EC);

        Z_Heroine->WriteLoHook(0x004EE15B, hook_004EE15B);

        // Z_Heroine->WriteLoHook(0x004BEF37, hook_configure);
        // Z_Heroine->WriteLoHook(0x00486B4C, hook_configure);
        // Z_Heroine->WriteLoHook(0x004BFB3B, hook_configure);
        // Z_Heroine->WriteLoHook(0x004BCA84, hook_configure);

        // Era::ConnectEra();

        // InstallHeroesSod();
        Era::RegisterHandler(EarlyLoad, "OnBeforeWog");

        // Era::RegisterHandler(LateLoad, "OnAfterCreateWindow");
        // Era::RegisterHandler(LateLoad, "OnAfterWog");
        Era::RegisterHandler(LateLoad, "OnAfterLoadMedia");

        // install_MISC();

        Install_heroNPC();

        install_PicsBack();

        InstallStartingHeroes();
        Install_ERM();


        Z_Heroine->WriteHiHook(0x753C70, SPLICE_, EXTENDED_, CDECL_, PrepareSpecWoG_hook);
        Z_Heroine->WriteLoHook(0x004D5B23, hook_configure /* hook_004D5B23 */);
        Z_Heroine->WriteLoHook(0x00486DCB, hook_configure);
        Z_Heroine->WriteLoHook(0x004D8A89, hook_004D8A89);
        Z_Heroine->WriteLoHook(0x0058DEFD, hook_0058DEFD);

     // Z_Heroine->WriteLoHook(0x0058D9B0, hook_0058D9B0);
     // Z_Heroine->WriteLoHook(0x0058D98D, hook_0058D98D);

        Z_Heroine->WriteDword(0x0057C7D0 + 2, 0);
        // Z_Heroine->WriteDword(0x00583EA6 + 1, 0);
        Z_Heroine->WriteHexPatch(0x00583EAB, "9090909090909090909090");
        Z_Heroine->WriteHiHook(0x004BB0C0, SPLICE_, EXTENDED_, THISCALL_, GameMgr_GetRandomHeroSimple_hook);
        Z_Heroine->WriteHiHook(0x004BB2A0, SPLICE_, EXTENDED_, THISCALL_, GameMgr_GetRandomHero_hook);
        Z_Heroine->WriteHiHook(0x004C8110, SPLICE_, EXTENDED_, THISCALL_, ChangeTavernHeroes_hook);
        Z_Heroine->WriteHiHook(0x004C8360, SPLICE_, EXTENDED_, THISCALL_, Tavern_Generate_One_New_Hero_hook);
        // Z_Heroine->WriteHiHook(0x004D91E0, SPLICE_, EXTENDED_, THISCALL_, Hero_GetClassName_hook);
        Z_Heroine->WriteLoHook(0x004C9E51, hook_004C9E51);
        /*
        memcpy((void*)Heroine::HeroTypes, (void*) 0x0067D868, 
            16 * sizeof(Heroine::_HeroType_) ); 
        */ for (int i = 0; i < MaxHeroClassCount; ++i) 
                Heroine::HeroTypes[i].Belong2Town = i / 2;
        Z_Heroine->WriteDword(0x0069928C, (int) Heroine::HeroTypes);
        Z_Heroine->WriteDword(0x004E6A20 + 3, 8 + (int)Heroine::HeroTypes);
        Z_Heroine->WriteDword(0x004E6B90 + 1, (int)Heroine::HeroTypes + 
            OldHeroClassCount * sizeof(Heroine::_HeroType_));
        Z_Heroine->WriteDword(0x0067DCEC, (int)Heroine::HeroTypes);
        // Z_Heroine->WriteDword(0x004D9210 + 2, (int) Heroine::HeroTypes);
        // Z_Heroine->WriteByte(0x004C8343 + 2, 2);
        // CDECL_0(char, 0x0004E69D0);
        // CDECL_0(int, 0x73E1AA);

        // install_Biographies();

        /*
        memcpy(Heroine::heroSpecialty, (void*)0x00678420,
            OldHeroCount * sizeof(h3::H3HeroSpecialty));
        Z_Heroine->WriteDword(0x00679C80, (int)Heroine::heroSpecialty);
        */

        /*
        for(int i=0;i<MaxHeroCount;++i)
            default_hero(i);
        */

        // Install_heroCount();

        // InstallHeroesSpecialtyGfx();

        InstallHeroDef();

        break;
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

