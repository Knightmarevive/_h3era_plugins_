#pragma warning(disable : 4996)

#define _H3API_PLUGINS_
#define _H3API_PATCHER_X86_
#include "../__include__/H3API/single_header/H3API.hpp"

#include "framework.h"
#include "era.h"

#define PINSTANCE_MAIN "Heroine"

extern Patcher* globalPatcher;
extern PatcherInstance* Z_Heroine;

#include "hero_limits.h"

#include "NPC.h"


namespace Heroine {
	h3::PH3LoadedPcx16 spec_pcx44[MaxHeroCount];
};
// h3::PH3LoadedDef zn44 = nullptr;

extern "C" __declspec(dllexport) h3::PH3LoadedPcx16 Get_Spec44ptr(int hero_id) {
	if (hero_id < 0 || hero_id >= MaxHeroCount) return nullptr;
	return Heroine::spec_pcx44[hero_id];
}

_LHF_(hook_004E0D6E) {
	// c->eax = (int) h3::H3DlgDef::Create(18, 180, 44, 44, 118, "zn44.def", 0, 0, 0, 0);
	c->eax = (int)h3::H3DlgPcx16::Create(18, 180, 44, 44, 118, "default.pcx");
	c->return_address = 0x004E0DA6;
	return SKIP_DEFAULT;
}

_LHF_(hook_004E1F14) {
	auto dlg = (h3::H3Dlg*) c->ebx;
	int hero_id = *(int*)(c->edx+0x1a);
	// auto it = dlg->GetH3DlgItem(118);
	// auto wnd_mgr = h3::H3WindowManager::Get();
	
	auto it = dlg->GetPcx16(118);
	it->SetPcx(Heroine::spec_pcx44[hero_id]);

	/*
	Heroine::spec_pcx44[hero_id]->DrawToPcx16(
		it->GetAbsoluteX(), it->GetAbsoluteY(),
		false, wnd_mgr->screenPcx16
	);
	wnd_mgr->H3Redraw(it->GetAbsoluteX(), it->GetAbsoluteY(), 44, 44);
	*/
	c->return_address = 0x004E1F2F;
	return SKIP_DEFAULT;
}

void InstallHeroesSpecialtyGfx() {

	static bool done = false;
	if (done) return;

	char tmp[65536];
	for (int i = 0; i < MaxHeroCount; ++i) {
		sprintf(tmp, "zn44%04d.pcx", i);
		Heroine::spec_pcx44[i] = h3::H3LoadedPcx16::Create(tmp,44,44);
	}
	auto un44 = h3::H3LoadedDef::Load("un44.def");
	/*
	static char zn44name[16] = "zn44.def";
	zn44 = h3::H3ObjectAllocator<h3::H3LoadedDef>().allocate();;
	zn44->Init(zn44name, 0x47 ,44,44);
	zn44->AddDefGroup(0, 4096);
	zn44->palette565 = h3::H3ObjectAllocator<h3::H3Palette565>().allocate();
	zn44->palette888 = h3::H3ObjectAllocator<h3::H3Palette888>().allocate();
	*/
	for (int i = 0; i < OldHeroCount; ++i) {
		// zn44->AddFrameFromDef("un44.def",i);
		un44->DrawToPcx16(0,i, Heroine::spec_pcx44[i],0,0);
	}
	// auto empty= h3::H3LoadedPcx::Init("empty_44.pcx",44,44);
	for (int i = OldHeroCount; i < MaxHeroCount; ++i) {
		// zn44->AddFrameFromDef("un44.def", 0);
		// zn44->AddFrameToGroup(0, zn44->GetGroupFrame(0,0));

//		un44->DrawToPcx16(0, i, Heroine::spec_pcx44[HSpecNames->PicNum], 0, 0);

		if (un44->groups[0][0].count>i)
			un44->DrawToPcx16(0, i, Heroine::spec_pcx44[i], 0, 0);
		else
			un44->DrawToPcx16(0, 0, Heroine::spec_pcx44[i], 0, 0);
	}
	/*
	for (int i = OldHeroCount; i < un44->groups[0][0].count; ++i) {
		zn44->AddFrameFromDef("un44.def", i);
	}
	*/
	/*
	//zn44->IncreaseReferences();
	zn44->AddToResourceManager();
	zn44->IncreaseReferences();
	// Z_Heroine->WriteByte(0x00679D90, 'z');
	// Z_Heroine->WriteDword(0x004E0D89 + 1, (int) zn44name);
	*/
	Z_Heroine->WriteLoHook(0x004E0D6E, hook_004E0D6E);
	Z_Heroine->WriteLoHook(0x004E1F14, hook_004E1F14);

	done = true;
}