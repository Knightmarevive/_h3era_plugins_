#pragma warning(disable : 4996)

#define _H3API_PLUGINS_
#define _H3API_PATCHER_X86_
#include "../__include__/H3API/single_header/H3API.hpp"

#include "framework.h"
#include "era.h"

#define PINSTANCE_MAIN "Heroine"

extern Patcher* globalPatcher;
extern PatcherInstance* Z_Heroine;

#include "hero_limits.h"

#include "NPC.h"


namespace Heroine {
	extern h3::PH3LoadedPcx16 spec_pcx44[MaxHeroCount];
	extern struct _PicsBack_ {
		BYTE* HPSLoaded;
		BYTE* HPLLoaded;
	} PicsBack[MaxHeroCount];
	extern h3::H3HeroInfo HeroInfo[MaxHeroCount];
	extern struct _HeroBios_ { 
		char* HBios;
	} HBios_unk, HBiosTable[MaxHeroCount];
	extern h3::H3HeroSpecialty heroSpecialty[MaxHeroCount];
};

struct HeroInfo_Dlg : public h3::H3Dlg {
	int plr = -1; int hero_id = -1;
	HeroInfo_Dlg(int _plr, int _heroid) : H3Dlg(720, 480, -1, -1, 0, 0),
		plr(_plr), hero_id(_heroid)
	{
		AddBackground(0, 0, widthDlg, heightDlg, true, false, plr, false);

		CreatePcx(32 , 32 , 101, Heroine::HeroInfo[hero_id].largePortrait);
		LPCSTR text = Heroine::HeroInfo[hero_id].name;
		CreateText(16, 96, 96, 32, text, "MedFont.fnt", TxtHeroColour, 102);

		text = Heroine::HBiosTable[hero_id].HBios;
		CreateText(96, 32, 512, 64, text, "MedFont.fnt", TxtHeroColour, 103);

		CreatePcx16(40, 144, 44, 44, 104, "default.pcx")->
			SetPcx(Heroine::spec_pcx44[hero_id]);

		text = Heroine::heroSpecialty[hero_id].spFull;
		CreateText(16, 200, 96, 32, text, "MedFont.fnt", TxtHeroColour, 105);

		text = Heroine::heroSpecialty[hero_id].spDescr;
		CreateText(96, 128, 512, 64, text, "MedFont.fnt", TxtHeroColour, 106);

		auto& nfo = Heroine::HeroInfo[hero_id];
		CreateDef(32,  256, 107, "Twcrport.def", nfo.armyType[0] + 2);
		CreateDef(128, 256, 108, "Twcrport.def", nfo.armyType[1] + 2);
		CreateDef(224, 256, 109, "Twcrport.def", nfo.armyType[2] + 2);

		char buf[512]; auto mons = h3::H3CreatureInformation::Get();
		sprintf(buf, "%s\n%d - %d", mons[nfo.armyType[0]].namePlural,
			nfo.creatureAmount[0].lowAmount, nfo.creatureAmount[0].highAmount);
		CreateText(16, 320, 96, 64, buf, "MedFont.fnt", TxtHeroColour, 110);
		sprintf(buf, "%s\n%d - %d", mons[nfo.armyType[1]].namePlural,
			nfo.creatureAmount[1].lowAmount, nfo.creatureAmount[1].highAmount);
		CreateText(16 + 96, 320, 96, 64, buf, "MedFont.fnt", TxtHeroColour, 111);
		sprintf(buf, "%s\n%d - %d", mons[nfo.armyType[2]].namePlural,
			nfo.creatureAmount[2].lowAmount, nfo.creatureAmount[2].highAmount);
		CreateText(16 + 192, 320, 96, 64, buf, "MedFont.fnt", TxtHeroColour, 112);

		HMODULE h_More_SS = GetModuleHandleA("more_SS_levels.era");
		if (h_More_SS) {
			auto f_get_SS_Name = (char*(*)(int)) 
				GetProcAddress(h_More_SS, "get_SS_Name");
			CreateDef(24 + 96 * 3, 240, 113, "ZECSK82.def",
				16 * nfo.sskills[0].type + nfo.sskills[0].level + 16);
			CreateDef(24 + 96 * 4, 240, 114, "ZECSK82.def", 
				16 * nfo.sskills[1].type + nfo.sskills[1].level + 16);
			sprintf(buf, "%s\nlevel %d", 
				f_get_SS_Name(nfo.sskills[0].type),
				nfo.sskills[0].level);
			CreateText(16 + 96 * 3, 320, 96, 64, buf, "MedFont.fnt", TxtHeroColour, 115);
			sprintf(buf, "%s\nlevel %d",
				f_get_SS_Name(nfo.sskills[1].type),
				nfo.sskills[1].level);
			CreateText(16 + 96 * 4, 320, 96, 64, buf, "MedFont.fnt", TxtHeroColour, 116);
		}
		else {
			CreateDef(24 + 96 * 3, 240, 113, "SECSK82.def",
				3 * nfo.sskills[0].type + nfo.sskills[0].level + 2);
			CreateDef(24 + 96 * 4, 240, 114, "SECSK82.def",
				3 * nfo.sskills[1].type + nfo.sskills[1].level + 2);
		}

		if (nfo.startingSpell >= 0) {
			CreateDef(32 + 96 * 5, 256, 117, "SpellBon.def", nfo.startingSpell);
			text = h3::H3Spell::Get()[nfo.startingSpell].name;
			CreateText(16 + 96 * 5, 320, 96, 64, text, "MedFont.fnt", TxtHeroColour, 118);
		}
	}

	BOOL DialogProc(h3::H3Msg& msg) override;
};


BOOL HeroInfo_Dlg::DialogProc(h3::H3Msg& msg) {
	if (msg.command == 512 
		// && msg.subtype != 0xE
		)
	{
		auto z_parent = (HeroInfo_Dlg*)msg.parentDlg;
		z_parent->Stop();
	}

	return 0;
}

extern "C" __declspec(dllexport) void HeroInfo_Dlg_Show(int color, int hero_id, int RMB) {

	HeroInfo_Dlg window(color, hero_id);
	if(RMB)	window.RMB_Show(); 
	else window.Start();
}