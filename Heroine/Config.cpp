#pragma warning(disable : 4996)

#define _H3API_PLUGINS_
#define _H3API_PATCHER_X86_
#include "../__include__/H3API/single_header/H3API.hpp"

#include "framework.h"
#include "era.h"

#define PINSTANCE_MAIN "Heroine"

extern Patcher* globalPatcher;
extern PatcherInstance* Z_Heroine;

#include "hero_limits.h"

#include "NPC.h"

namespace Heroine {
	extern h3::H3HeroSpecialty HSpecBack[MaxHeroCount];
	extern h3::H3HeroInfo HeroInfoBack[MaxHeroCount];
	extern h3::H3HeroInfo HeroInfo[MaxHeroCount];
	extern struct _HSpecNames_ {
		int PicNum;
		int Var[3];
	} HSpecNames[MaxHeroCount];

	extern BOOL8 isHeroEnabled[MaxHeroCount];

	extern struct _HeroCombatDef_ {
		char* def_name;
		DWORD cast_x, cast_y, cast_cadre;
	} CombatHeroDef[MaxHeroClassCount];

	extern struct _HeroType_ {
		DWORD Belong2Town;
		char* TypeName;
		DWORD Agression;
		char PSkillStart[4];
		char ProbPSkillToLvl9[4];
		char ProbPSkillAfterLvl10[4];
		char ProbSSkill[28];
		char ProbInTown[9];
		char field_3D[3];
	} HeroTypes[MaxHeroClassCount];
	extern char HeroType_ProbInTown[MaxHeroClassCount][MaxHeroFactionCount];

	
	// Adventure Manager + 0x10c
	extern h3::H3LoadedDef* heroDef[MaxHeroClassCount];
}

extern "C" __declspec(dllexport) h3::H3Hero * GetHeroesTable();
extern "C" __declspec(dllexport) h3::H3HeroSpecialty * GetHeroesSpecialtyTable();
extern "C" __declspec(dllexport) h3::H3HeroInfo * GetHeroesInfoTable();
extern "C" __declspec(dllexport) Heroine::_HSpecNames_ * GetHeroesSpecNames();
extern "C" __declspec(dllexport) NPC * GetCommander(int);


#define char_table_size 4096
#define int_tuple_size 256

bool newHeroSelectionDialog = true;
bool campaignHeroSelectable = true;

void ParseArray(char* buf, char* name, int* cortage)
{

}


void ParseFloat(char* buf, const char* name, float* result)
{
	char* c = strstr(buf, name);
	if (c != NULL)
		*result = atof(c + strlen(name));
}

bool ParseInt(char* buf, char* name, int* result)
{
	char* c = strstr(buf, name);
	if (c != NULL) {
		*result = atoi(c + strlen(name));
		return true;
	}
	else return false;
}

void ParseByte(char* buf, const char* name, char* result)
{
	char* c = strstr(buf, name);
	if (c != NULL)
		*result = (char)atoi(c + strlen(name));
}


bool ParseStr(char* buf, const char* name, /*char* &target */ char res[char_table_size])
{
	// char res[char_table_size];
	char* c = strstr(buf, name);
	int l = strlen(name);
	if (c != NULL)
	{
		char* cend = strstr(c + l, "\"");
		if (cend != NULL) //���� ��, ����� ������ �����������
		{
			//*res = (char*)malloc(cend-c+1);

			//memset(*res,0,cend-c+1);
			memset(res, 0, cend - c + 1);

			for (int i = 0, j = 0; j < (cend - c) - l; i++, j++)
			{
				if (c[l + j] != '\\')
				{
					//(*res)[i] = c[l+j];
					res[i] = c[l + j];
				}
				else
				{
					if (c[l + j + 1] == 'r')
					{
						//(*res)[i] = '\r';
						res[i] = '\r';

						j++;
						continue;
					}
					else if (c[l + j + 1] == 'n')
					{
						//(*res)[i] = '\n';
						res[i] = '\n';
						j++;
						continue;
					}
					else
					{
						//(*res)[i] = c[l+j];
						res[i] = c[l + j];

					}
				}
			}

			//strncpy(*res,c+l,cend-1-c);

			//(*res)[cend-c-l] = 0;

			res[cend - c - l] = 0;

			//res[cend - c - l] = 13; //end of WoG erm
			//res[cend - c - l +1] = 0;

			//sprintf(*res,*res);

			/*
			if (strcmp(target, res) != 0 ) {
				strcpy(target, res);
			}
			*/
		}
		return true;
	}
	else return false;
}
void ParseStr2(char* buf, const char* name, char*& target_1, char*& target_2
	/* char res[char_table_size]*/, char* target_static, int& target_int) {

	char res[char_table_size];
	if (ParseStr(buf, name, res)) {
		target_1 = target_2 = target_static;
		strcpy(target_static, res);
		target_int = 0;
	}

	/*
	if (target && target!= target2 && !*target)
		strcpy(target2, target);
	ParseStr(buf, name, target2);
	target = target2;
	target3 = 0;
	*/

	/*
	char res[char_table_size];
	ParseStr(buf, name, res);
	if (!target || strcmp(target, res) != 0) {
		//char* tmp = new char[char_table_size];
		//target = tmp;
		target = target2;
		strcpy(target, res);
	}
	*/
}


int ParseTuple(char* buf, const char* name, /* int** tuple */ int tuple[int_tuple_size])
{
	int len = 0;
	//char *tmp = (char*)malloc(strlen(buf));
	static char tmp[32768];
	char* c = strstr(buf, name);
	if (c != NULL)
	{
		strncpy(tmp, c + strlen(name), int_tuple_size);
		c = strpbrk(tmp, "\r\n\0}");
		if (c != NULL)
		{
			tmp[c - tmp] = 0;

			char* p = strtok(tmp, ",");

			do
			{
				if (p)
				{
					len++;
					// *tuple = (int*)realloc(*tuple,len*sizeof(int));
					// (*tuple)[len-1]=atoi(p);
					tuple[len - 1] = atoi(p);
				}
				p = strtok(NULL, ", ");
			} while (p);
		}
	}
	//free(tmp);
	return len;
}

// ------------------------------------------------------------------ //

struct _HeroBios_ { char* HBios; };
extern "C" __declspec(dllexport) char** GetBiography(int);
extern void RefreshPicsBack(int id, bool force = 0);
extern void RefUp_PicsBack(int id);
extern void InstallHeroesSpecialtyGfx();

void set_string(char**a, char* b, int n) {
	static std::vector<std::pair< char**, char* >> applied = {};

	for (auto& i : applied) {
		if (a == i.first) {
			*a = i.second;
			strcpy(*a, b);
			return;
		}
	}

	*a = CDECL_1(char*, 0x00617492, n);
	applied.push_back(std::make_pair(a, *a));
	strcpy(*a, b);
	return;

	// if (!*a) *a = CDECL_1(char*,0x00617492,n);// new char[n];
	// strcpy(*a, b);
}

void refresh_string(char** a,  int n) {
	char b[4096] = {};
	strcpy(b, *a);
	set_string(a, b, n);
}

void default_hero(int id) {
	auto  zNPC = GetCommander(id);
	auto& hero = GetHeroesTable()[id];
	auto& heroInfo = GetHeroesInfoTable()[id];
	auto& heroSpec = GetHeroesSpecialtyTable()[id];
	auto HeroBio = GetBiography(id);
	// static char* Unknown = "Unknown";
	char Unknown[char_table_size] = {};
	sprintf(Unknown, "Hero #%d", id);

	Heroine::isHeroEnabled[id] = (id < OldHeroCount);

	if (!*HeroBio) {
		set_string(const_cast<char**>(HeroBio), Unknown, char_table_size);
	}

	if (id < OldHeroCount) {
		refresh_string(const_cast<char**>(&heroSpec.spShort), char_table_size);
		refresh_string(const_cast<char**>(&heroSpec.spFull),  char_table_size);
		refresh_string(const_cast<char**>(&heroSpec.spDescr), char_table_size);

	}
	if(id>= OldHeroCount)
	{
		strcpy(hero.name, Unknown);
		hero.experience = 1;
		hero.id = id; int* Number = (&hero.id) + 1; *Number = id;
		// hero.picture = id < OldHeroPortraitsCount ? id : OldHeroPortraitsCount;
		hero.picture = id < NewHeroPortraitsCount ? id : NewHeroPortraitsCount;
		hero.owner = -1;

		zNPC->Number = id;
		THISCALL_1(void,0x00769AB4,zNPC);// *zNPC = {};
		// strcpy(zNPC->Name, "Unknown");
	}

	if (id >= OldHeroCount)
	{
		
		heroInfo.hasSpellbook = 1; heroInfo.startingSpell = -1;
		heroInfo.campaignHero = 1; heroInfo.heroClass = 0; 
		heroInfo.roeHero = 0; heroInfo.expansionHero = 0;
		heroInfo.sskills[0] = h3::H3SecondarySkill();
		heroInfo.sskills[1] = h3::H3SecondarySkill();
		heroInfo.armyType[0] = heroInfo.armyType[1] = heroInfo.armyType[2] = 139;
		heroInfo.creatureAmount[0].lowAmount = heroInfo.creatureAmount[0].highAmount = 1;
		heroInfo.creatureAmount[1].lowAmount = heroInfo.creatureAmount[1].highAmount = 1;
		heroInfo.creatureAmount[2].lowAmount = heroInfo.creatureAmount[2].highAmount = 1;
		heroInfo.sskills[0] = { h3::eSecondary::WISDOM,		h3::eSecSkillLevel::BASIC };
		heroInfo.sskills[1] = { h3::eSecondary::LOGISTICS,	h3::eSecSkillLevel::BASIC };
		memcpy(&Heroine::HeroInfoBack[id], &heroInfo, sizeof(heroInfo));

		set_string(const_cast<char**>(&heroInfo.name), Unknown, 16);
		set_string(const_cast<char**>(&heroInfo.largePortrait), "HPL136Wi.pcx", 16);
		set_string(const_cast<char**>(&heroInfo.smallPortrait), "HPS136Wi.pcx", 16);
		set_string(const_cast<char**>(&Heroine::HeroInfoBack[id].name), Unknown, 16);
		set_string(const_cast<char**>(&Heroine::HeroInfoBack[id].largePortrait), "HPL136Wi.pcx", 16);
		set_string(const_cast<char**>(&Heroine::HeroInfoBack[id].smallPortrait), "HPS136Wi.pcx", 16);

		// RefreshPicsBack(id);
		// RefUp_PicsBack(id);
	}

	if (id >= OldHeroCount)
	{
		heroSpec.type = h3::eHeroSpecialty(8);
		heroSpec.bonusId = 1;
		set_string(const_cast<char**>(&heroSpec.spShort), Unknown, char_table_size);
		set_string(const_cast<char**>(&heroSpec.spFull), Unknown, char_table_size);
		set_string(const_cast<char**>(&heroSpec.spDescr), Unknown, char_table_size);
	}


	RefreshPicsBack(id);
}

void LoadGlobalConfig(void) {
	char* buf, * c; // fname[256];
	FILE* fdesc; int answer = 0;
	if (fdesc = fopen("Data\\heroine.cfg", "r"))
	{

		//----------
		fseek(fdesc, 0, SEEK_END);
		int fdesc_size = ftell(fdesc);
		rewind(fdesc);
		//----------
		buf = (char*)malloc(fdesc_size + 1);
		fread(buf, 1, fdesc_size, fdesc);
		buf[fdesc_size] = 0;
		fclose(fdesc);

		if (ParseInt(buf, (char*)"disable_newHeroSelectionDialog=", &answer))
			newHeroSelectionDialog = !answer;

		if (ParseInt(buf, (char*)"campaignHeroSelectable=", &answer))
			campaignHeroSelectable = answer;
		
		free(buf);
	}
}

extern "C" __declspec(dllexport) void ConfigureHero(int id, char* cfg) {
	auto& hero = GetHeroesTable()[id];
	auto& heroInfo = GetHeroesInfoTable()[id];
	auto& heroSpec = GetHeroesSpecialtyTable()[id];
	auto& heroSpecNames = GetHeroesSpecNames()[id];
	auto HeroBio = GetBiography(id);

	Heroine::isHeroEnabled[id] = true;

	static char str[char_table_size];

	if (ParseStr(cfg, "Name=\"", str)) {
		strcpy(hero.name, str);
		set_string(const_cast<char**>(&heroInfo.name), str, 16);
	}

	if (ParseStr(cfg, "largePortrait=\"", str)) {
		set_string(const_cast<char**>(&heroInfo.largePortrait), str, 16);
		set_string(const_cast<char**>(&Heroine::HeroInfoBack[id].largePortrait), str, 16);
	}
	if (ParseStr(cfg, "smallPortrait=\"", str)) {
		set_string(const_cast<char**>(&heroInfo.smallPortrait), str, 16);
		set_string(const_cast<char**>(&Heroine::HeroInfoBack[id].smallPortrait), str, 16);
	}
		
	ParseInt(cfg, "isFemale=", &heroInfo.isFemale);
	ParseInt(cfg, "race=", (int*)&heroInfo.race);
	ParseInt(cfg, "Class=", (int*)&heroInfo.heroClass);
	ParseInt(cfg, "Class=", (int*)&hero.hero_class);
	ParseByte(cfg, "hasSpellbook=", &heroInfo.hasSpellbook);
	ParseInt(cfg, "startingSpell=", &heroInfo.startingSpell);
	ParseInt(cfg, "army0type=", &heroInfo.armyType[0]);
	ParseInt(cfg, "army1type=", &heroInfo.armyType[1]);
	ParseInt(cfg, "army2type=", &heroInfo.armyType[2]);
	ParseInt(cfg, "army0low=", &heroInfo.creatureAmount[0].lowAmount);
	ParseInt(cfg, "army0high=", &heroInfo.creatureAmount[0].highAmount);
	ParseInt(cfg, "army1low=", &heroInfo.creatureAmount[1].lowAmount);
	ParseInt(cfg, "army1high=", &heroInfo.creatureAmount[1].highAmount);
	ParseInt(cfg, "army2low=", &heroInfo.creatureAmount[2].lowAmount);
	ParseInt(cfg, "army2high=", &heroInfo.creatureAmount[2].highAmount);
	ParseByte(cfg, "roeHero=", &heroInfo.roeHero);
	ParseByte(cfg, "expansionHero=", &heroInfo.expansionHero);
	ParseByte(cfg, "campaignHero=", &heroInfo.campaignHero);
	ParseInt(cfg, "skill0type=", (int*)&heroInfo.sskills[0].type);
	ParseInt(cfg, "skill1type=", (int*)&heroInfo.sskills[1].type);
	ParseInt(cfg, "skill0level=", (int*)&heroInfo.sskills[0].level);
	ParseInt(cfg, "skill1level=", (int*)&heroInfo.sskills[1].level);

	if (ParseStr(cfg, "specShort=\"", str))
		set_string(const_cast<char**>(&heroSpec.spShort), str, char_table_size);
	if (ParseStr(cfg, "specFull=\"", str))
		set_string(const_cast<char**>(&heroSpec.spFull), str, char_table_size);
	if (ParseStr(cfg, "specDescr=\"", str))
		set_string(const_cast<char**>(&heroSpec.spDescr), str, char_table_size);

	ParseInt(cfg, "specPicNum=", (int*)&heroSpecNames.PicNum);
	ParseInt(cfg, "specType=", (int*)&heroSpec.type);
	ParseInt(cfg, "specBonusID=", (int*)&heroSpec.bonusId);
	ParseInt(cfg, "attackBonus=", (int*)&heroSpec.attackBonus);
	ParseInt(cfg, "defenseBonus=", (int*)&heroSpec.defenseBonus);
	ParseInt(cfg, "damageBonus=", (int*)&heroSpec.damageBonus);
	ParseInt(cfg, "upgrade2=", (int*)&heroSpec.upgrade2);
	ParseInt(cfg, "upgradeTo=", (int*)&heroSpec.upgradeTo);

	if(*HeroBio) ParseStr(cfg, "Biography=\"", *HeroBio);

}

extern "C" __declspec(dllexport) void ConfigureHeroClasses(int id, char* cfg) {
	char buf[char_table_size]; char str[char_table_size];
	for (int j = 0; j < MaxHeroFactionCount; ++j) {
		sprintf(buf, "ProbInTown%04d=",j);
		ParseByte(cfg, buf, &Heroine::HeroType_ProbInTown[id][j]);
	}
	if (ParseStr(cfg, "Name=\"", str)) 
		set_string(const_cast<char**>(&Heroine::HeroTypes[id].TypeName),str, char_table_size);
	ParseInt(cfg, "Aggression=", (int*) &Heroine::HeroTypes[id].Agression);
	for (int j = 0; j < 4; ++j) {
		sprintf(buf, "PSkillStart_%04d=", j); 
		ParseByte(cfg, buf, &Heroine::HeroTypes[id].PSkillStart[j]);
		sprintf(buf, "ProbPSkillToLvl9_%04d=", j); 
		ParseByte(cfg, buf, &Heroine::HeroTypes[id].ProbPSkillToLvl9[j]);
		sprintf(buf, "ProbPSkillAfterLvl10_%04d=", j); 
		ParseByte(cfg, buf, &Heroine::HeroTypes[id].ProbPSkillAfterLvl10[j]);
	}
	ParseInt(cfg, "Belong2Town=", (int*)&Heroine::HeroTypes[id].Belong2Town);

	/*
	if (id >= 18 && Heroine::heroDef[id] == nullptr) {
		sprintf(str, "AH%d_.def",id);
		Heroine::heroDef[id] = h3::H3LoadedDef::Load(str);
	}
	*/
}

void configure_all_heroes() {
	// static char buf[65535];
	static char fname[1024];
	static bool done = false;

	static h3::H3HeroInfo S_HeroInfo[MaxHeroCount];
	if (done) {
		memcpy(Heroine::HeroInfoBack, S_HeroInfo, sizeof(S_HeroInfo));
		memcpy(Heroine::HeroInfo, S_HeroInfo, sizeof(S_HeroInfo));

		return;
	}

	for (int hc = 0; hc < MaxHeroClassCount; ++hc) for (int j = 0; j<9; ++j)
		Heroine::HeroType_ProbInTown[hc][j] = Heroine::HeroTypes[hc].ProbInTown[j];

	auto old_combat_def = (Heroine::_HeroCombatDef_*)0x0063BD40;
	for (int hc = 0; hc < MaxHeroClassCount; ++hc) {
		memcpy(&Heroine::CombatHeroDef[hc],&old_combat_def[hc%18], sizeof(Heroine::_HeroCombatDef_));
		set_string(const_cast<char**>(&Heroine::CombatHeroDef[hc].def_name), old_combat_def[hc % 18].def_name, 16);
	}

	for (int hc = 0; hc < MaxHeroClassCount; ++hc) {
		FILE* fdesc = nullptr;
		sprintf(fname, "Data\\Hero_Classes\\%u.cfg", hc);

		if (fdesc = fopen(fname, "r"))
		{


			//----------
			fseek(fdesc, 0, SEEK_END);
			int fdesc_size = ftell(fdesc);
			rewind(fdesc);
			//----------
			char* buf = (char*)malloc(fdesc_size + 1);
			memset(buf, 0, fdesc_size + 1);
			fread(buf, 1, fdesc_size, fdesc);
			buf[fdesc_size] = 0;
			fclose(fdesc);

			ConfigureHeroClasses(hc, buf);

			free(buf);
		}
	}

	for (int id = 0; id < MaxHeroCount; ++id) {
		default_hero(id);

		/*
		auto& hero = GetHeroesTable()[id];
		auto& heroInfo = GetHeroesInfoTable()[id];
		auto& heroSpec = GetHeroesSpecialtyTable()[id];
		*/

		FILE* fdesc = nullptr;
		sprintf(fname, "Data\\Heroes\\%u.cfg", id);

		if (fdesc = fopen(fname, "r"))
		{


			//----------
			fseek(fdesc, 0, SEEK_END);
			int fdesc_size = ftell(fdesc);
			rewind(fdesc);
			//----------
			char* buf = (char*)malloc(fdesc_size + 1);
			memset(buf, 0, fdesc_size + 1);
			fread(buf, 1, fdesc_size, fdesc);
			buf[fdesc_size] = 0;
			fclose(fdesc);

			ConfigureHero(id, buf);

			free(buf);
		}
		static long refresh_once = 2;
		if (true /* && refresh_once */) {
			RefreshPicsBack(id, true);
			//--refresh_once;
			// refresh_once = false;
		}
	}

	Z_Heroine->WriteDword(0x028077CC + 0, 0);
	// CDECL_1(int, 0x00753C70 , 1);

	if (!done && S_HeroInfo->name) {
		memcpy(S_HeroInfo, Heroine::HeroInfoBack, sizeof(S_HeroInfo));

		done = true;
	}
}