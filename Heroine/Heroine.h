#pragma once

/*/

_LHF_(_add_eax_HeroesOffset_) {
    c->eax += (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_eax__eax_ecx_HeroesOffset_) {
    c->eax += c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_eax__eax_edx_HeroesOffset_) {
    c->eax += c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_eax__ebx_eax_HeroesOffset_) {
    c->eax = c->ebx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}


_LHF_(_lea_eax__ebx_edx_HeroesOffset_) {
    c->eax = c->ebx + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_eax__ecx_eax_HeroesOffset_) {
    c->eax = c->ecx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}


_LHF_(_lea_eax__ecx_edx_HeroesOffset_) {
    c->eax = c->ecx + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}


_LHF_(_lea_eax__edi_eax_HeroesOffset_) {
    c->eax = c->edi + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_eax__edi_ecx_HeroesOffset_) {
    c->eax = c->edi + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_eax__edi_edx_HeroesOffset_) {
    c->eax = c->edi + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}


_LHF_(_lea_eax__edx_eax_HeroesOffset_) {
    c->eax = c->edx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_eax__edx_ecx_HeroesOffset_) {
    c->eax = c->edx + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_eax__esi_HeroesOffset_) {
    // c->eax = c->esi + (int)Heroine::heroes - (int)h3::H3Main::Get();
    c->eax = (int)Heroine::heroes;
    return SKIP_DEFAULT;
}

_LHF_(_lea_eax__esi_eax_HeroesOffset_) {
    c->eax = c->esi + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}


_LHF_(_lea_eax__esi_ecx_HeroesOffset_) {
    c->eax = c->esi + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_eax__esi_edx_HeroesOffset_) {
    c->eax = c->esi + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}


_LHF_(_lea_ebx__eax_ecx_HeroesOffset_) {
    c->ebx = c->eax + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ebx__eax_edx_HeroesOffset_) {
    c->ebx = c->eax + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ebx__ecx_eax_HeroesOffset_) {
    c->ebx = c->ecx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ebx__ecx_edx_HeroesOffset_) {
    c->ebx = c->ecx + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ebx__edi_ecx_HeroesOffset_) {
    c->ebx = c->edi + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ebx__edx_eax_HeroesOffset_) {
    c->ebx = c->edx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ebx__edx_ecx_HeroesOffset_) {
    c->ebx = c->edx + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ebx__esi_eax_HeroesOffset_) {
    c->ebx = c->esi + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}


_LHF_(_lea_ecx__eax_HeroesOffset_) {
    c->ecx = c->eax + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__eax_ecx_HeroesOffset_) {
    c->ecx = c->eax + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__eax_edx_HeroesOffset_) {
    c->ecx = c->eax + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__ebx_HeroesOffset_) {
    c->ecx = c->ebx + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__ebx_eax_HeroesOffset_) {
    c->ecx = c->ebx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__ebx_ecx_HeroesOffset_) {
    c->ecx = c->ebx + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__ecx_eax_HeroesOffset_) {
    c->ecx = c->ecx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__ecx_edx_HeroesOffset_) {
    c->ecx = c->ecx + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__edi_eax_HeroesOffset_) {
    c->ecx = c->edi + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__edi_ecx_HeroesOffset_) {
    c->ecx = c->edi + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__edi_edx_HeroesOffset_) {
    c->ecx = c->edi + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__edx_eax_HeroesOffset_) {
    c->ecx = c->edx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__edx_ecx_HeroesOffset_) {
    c->ecx = c->edx + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__esi_ecx_HeroesOffset_) {
    c->ecx = c->esi + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_ecx__esi_edx_HeroesOffset_) {
    c->ecx = c->esi + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edi__eax_ecx_HeroesOffset_) {
    c->edi = c->eax + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edi__eax_edx_HeroesOffset_) {
    c->edi = c->eax + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edi__ebx_HeroesOffset_) {
    c->edi = c->ebx + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edi__ebx_ecx_HeroesOffset_) {
    c->edi = c->ebx + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}


_LHF_(_lea_edi__ecx_eax_HeroesOffset_) {
    c->edi = c->ecx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edi__ecx_edx_HeroesOffset_) {
    c->edi = c->ecx + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edi__edi_eax_HeroesOffset_) {
    c->edi = c->edi + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edi__edi_edx_HeroesOffset_) {
    c->edi = c->edi + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edi__edx_eax_HeroesOffset_) {
    c->edi = c->edx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edi__edx_ecx_HeroesOffset_) {
    c->edi = c->edx + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edi__esi_HeroesOffset_) {
    c->edi = c->esi + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edi__esi_edx_HeroesOffset_) {
    c->edi = c->esi + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edx__eax_edx_HeroesOffset_) {
    c->edx = c->eax + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edx__ebx_edx_HeroesOffset_) {
    c->edx = c->ebx + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edx__ecx_eax_HeroesOffset_) {
    c->edx = c->ecx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edx__edi_ecx_HeroesOffset_) {
    c->edx = c->edi + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edx__edi_edx_HeroesOffset_) {
    c->edx = c->edi + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edx__edx_eax_HeroesOffset_) {
    c->edx = c->edx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_edx__edx_ecx_HeroesOffset_) {
    c->edx = c->edx + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__eax_edx_HeroesOffset_) {
    c->esi = c->eax + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__ebx_HeroesOffset_) {
    c->esi = c->ebx + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__ebx_eax_HeroesOffset_) {
    c->esi = c->ebx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__ebx_ecx_HeroesOffset_) {
    c->esi = c->ebx + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__ebx_edx_HeroesOffset_) {
    c->esi = c->ebx + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__ecx_eax_HeroesOffset_) {
    c->esi = c->ecx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__ecx_edx_HeroesOffset_) {
    c->esi = c->ecx + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__edi_eax_HeroesOffset_) {
    c->esi = c->edi + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__edi_ecx_HeroesOffset_) {
    c->esi = c->edi + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__edx_eax_HeroesOffset_) {
    c->esi = c->edx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__edx_ecx_HeroesOffset_) {
    c->esi = c->edx + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__esi_eax_HeroesOffset_) {
    c->esi = c->esi + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__esi_ecx_HeroesOffset_) {
    c->esi = c->esi + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_lea_esi__esi_edx_HeroesOffset_) {
    c->esi = c->esi + c->edx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

_LHF_(_mov_cx__edx_ecx_HeroesOffset_) {
    c->CX() = *(short*)(c->edx + c->ecx * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get());
    return SKIP_DEFAULT;
}

_LHF_(_mov_dx__ecx_eax_HeroesOffset_) {
    c->DX() = *(short*)(c->ecx + c->eax * 2 + (int)Heroine::heroes - (int)h3::H3Main::Get());
    return SKIP_DEFAULT;
}


_LHF_(_lea_edx__esi_HeroesOffset_) {
    c->edx = c->esi + (int)Heroine::heroes - (int)h3::H3Main::Get();
    return SKIP_DEFAULT;
}

*/