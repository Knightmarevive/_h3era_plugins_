#pragma once


#define MaxHeroCount 256
#define MaxHeroClassCount 128
#define MaxHeroFactionCount (MaxHeroClassCount/2)
#define OldHeroCount 156
#define OldHeroClassCount 18
#define OldHeroPortraitsCount 164
#define NewHeroPortraitsCount 240
#define MaxHeroesPerTown 32


#define PERMSIZE 256000
#define TxtHeroColour 1