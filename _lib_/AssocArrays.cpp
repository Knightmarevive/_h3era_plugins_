
#include <vcl.h>
#pragma hdrstop

#include "AssocArrays.h"

  /***/


#include <System.hpp>


__fastcall TAssocArray::TAssocArray( THashFunc HashFunc,
            /*n*/ TKeyPreprocessFunc KeyPreprocessFunc, bool OwnsItems, bool ItemsAreObjects, Utils TItemGuardProc::ItemGuardProc,
  /*IN*/  /*n*/ Utils TItemGuard::& ItemGuard )
 : fRoot(NULL),
   fOwnsItems(false),
   fItemsAreObjects(false),
   fItemCount(0),
   fNodeCount(0),
   fIterCurrItem(NULL),
   fIterNodeInd(0),
   fLocked(false)
{
  /*!*/
  Assert( HashFunc != NULL );
  /*!*/
  Assert( &ItemGuardProc != NULL );
  this->fHashFunc = HashFunc;
  this->fKeyPreprocessFunc = KeyPreprocessFunc;
  this->fOwnsItems = OwnsItems;
  this->fItemsAreObjects = ItemsAreObjects;
  this->fItemGuardProc = ItemGuardProc;
  this->fItemGuard = ItemGuard;
  this->fItemCount = 0;
  this->fNodeCount = 0;
  ItemGuard = NULL;
} // .constructor TAssocArray.Create



__fastcall TAssocArray::~TAssocArray( )
{
  this->Clear();
  SysUtils.FreeAndNil( this->fItemGuard );
} // .destructor TAssocArray.Destroy



PAssocArrayItem __fastcall TAssocArray::CloneItem( PAssocArrayItem Item ) /*O*/
{
  PAssocArrayItem result = NULL;
  /*!*/
  Assert( Item != NULL );
  /*!*/
  Assert( this->IsValidValue( Item->Value ) );
  result = new TAssocArrayItem;
  result->Key = Item->Key;
  if ( Item->NextItem != NULL )
  {
    result->NextItem = this->CloneItem( Item->NextItem );
  } // .if
  else
  {
    result->NextItem = NULL;
  } // .else
  if ( ( Item->Value == NULL ) || ( ! this->OwnsItems ) )
  {
    result->Value = Item->Value;
  } // .if
  else
  {
    /*!*/
    Assert( this->ItemsAreObjects );
    /*!*/
    Assert( dynamic_cast< Utils.TCloneable >( ((TObject*) Item->Value ) ) );
    result->Value = Utils.TCloneable( Item->Value ).Clone;
  } // .else
  return result;
} // .function TAssocArray.CloneItem



PAssocArrayNode __fastcall TAssocArray::CloneNode(/*n*/ PAssocArrayNode Node ) /*On*/
{
  PAssocArrayNode result = NULL;
  if ( Node == NULL )
  {
    result = NULL;
  } // .if
  else
  {
    result = new TAssocArrayNode;
    result->Hash = Node->Hash;
    result->Item = this->CloneItem( Node->Item );
    result->ChildNodes[LEFT_CHILD] = this->CloneNode( Node->ChildNodes[LEFT_CHILD] );
    result->ChildNodes[RIGHT_CHILD] = this->CloneNode( Node->ChildNodes[RIGHT_CHILD] );
  } // .else
  return result;
} // .function TAssocArray.CloneNode



void __fastcall TAssocArray::Assign( Utils TCloneable::Source )
{


/*U*/
  TAssocArray* SrcArr = NULL;
  /*!*/
  Assert( ! this->Locked );
  /*!*/
  Assert( Source != NULL );
  SrcArr = ( TAssocArray* ) Source;
  // * * * * * //
  if ( this != Source )
  {
    this->Clear();
    this->fHashFunc = SrcArr->HashFunc;
    this->fKeyPreprocessFunc = SrcArr->KeyPreprocessFunc;
    this->fOwnsItems = SrcArr->OwnsItems;
    this->fItemsAreObjects = SrcArr->ItemsAreObjects;
    this->fItemGuardProc = SrcArr->ItemGuardProc;
    this->fItemGuard = SrcArr->fItemGuard.Clone;
    this->fItemCount = SrcArr->ItemCount;
    this->fNodeCount = SrcArr->NodeCount;
    this->fRoot = this->CloneNode( SrcArr->fRoot );
  } // .if
} // .procedure TAssocArray.Assign



void __fastcall TAssocArray::FreeItemValue( PAssocArrayItem Item )
{
  /*!*/
  Assert( Item != NULL );
  if ( this->OwnsItems )
  {
    if ( this->ItemsAreObjects )
    {
      delete ((TObject*) Item->Value );
    } // .if
    else
    {
      FreeMem( Item->Value );
    } // .else
  } // .if
  Item->Value = NULL;
} // .procedure TAssocArray.FreeItemValue



void __fastcall TAssocArray::RemoveNode(/*n*/ PAssocArrayNode ParentNode, PAssocArrayNode ItemNode )
{


/*U*/
  PAssocArrayNode RightClosestNodeParent = NULL;
/*U*/
  PAssocArrayNode RightClosestNode = NULL;
  bool ItemNodeIsRoot = false;
  TChildNodeSide ItemNodeSide = false;
  /*!*/
  Assert( ItemNode != NULL );
  RightClosestNodeParent = NULL;
  RightClosestNode = NULL;
  ItemNodeSide = false;
  // * * * * * //
  ItemNodeIsRoot = ParentNode == NULL;
  if ( this->NodeCount == 1 )
  {
    /*!*/
    Assert( ItemNodeIsRoot );
    /*!*/
    Assert( ItemNode == this->fRoot );
    delete this->fRoot;
    this->fRoot = NULL;
  } // .if
  else
  {
    if ( ! ItemNodeIsRoot )
    {
      ItemNodeSide = ItemNode->Hash >= ParentNode->Hash;
    } // .if
    
    /* N
      - -
    */
    if ( ( ItemNode->ChildNodes[LEFT_CHILD] == NULL ) && ( ItemNode->ChildNodes[RIGHT_CHILD] == NULL ) )
    {
      ParentNode->ChildNodes[ItemNodeSide] = NULL;
      delete ItemNode;
      ItemNode = NULL;
    } // .if
    /* N
      - R
    */
    else
      if ( ItemNode->ChildNodes[LEFT_CHILD] == NULL )
      {
        if ( ItemNodeIsRoot )
        {
          this->fRoot = ItemNode->ChildNodes[RIGHT_CHILD];
        } // .if
        else
        {
          ParentNode->ChildNodes[ItemNodeSide] = ItemNode->ChildNodes[RIGHT_CHILD];
        } // .else
        delete ItemNode;
        ItemNode = NULL;
      } // .ELSEIF
    /* N
      L -
    */
      else
        if ( ItemNode->ChildNodes[RIGHT_CHILD] == NULL )
        {
          if ( ItemNodeIsRoot )
          {
            this->fRoot = ItemNode->ChildNodes[LEFT_CHILD];
          } // .if
          else
          {
            ParentNode->ChildNodes[ItemNodeSide] = ItemNode->ChildNodes[LEFT_CHILD];
          } // .else
          delete ItemNode;
          ItemNode = NULL;
        } // .ELSEIF
    /* N
      L R
    */
        else
        {
          RightClosestNodeParent = ItemNode;
          RightClosestNode = ItemNode->ChildNodes[RIGHT_CHILD];
          while ( RightClosestNode->ChildNodes[LEFT_CHILD] != NULL )
          {
            RightClosestNodeParent = RightClosestNode;
            RightClosestNode = RightClosestNode->ChildNodes[LEFT_CHILD];
          } // .while
          ItemNode->Item = RightClosestNode->Item;
          RightClosestNode->Item = NULL;
          ItemNode->Hash = RightClosestNode->Hash;
          this->RemoveNode( RightClosestNodeParent, RightClosestNode );
        } // .else
  } // .else
} // .procedure TAssocArray.RemoveNode



void __fastcall TAssocArray::RemoveItem(
  /*n*/ PAssocArrayNode ParentNode, PAssocArrayNode ItemNode,
  /*n*/ PAssocArrayItem ParentItem, PAssocArrayItem Item )
{
  /*!*/
  Assert( ItemNode != NULL );
  /*!*/
  Assert( Item != NULL );
  this->FreeItemValue( Item );
  if ( ( ItemNode->Item == Item ) && ( Item->NextItem == NULL ) )
  {
    this->RemoveNode( ParentNode, ItemNode );
    /* RemoveNode is recursive procedure not affecting the counter */
    this->fNodeCount--;
  } // .if
  else
  {
    if ( ItemNode->Item == Item )
    {
      ItemNode->Item = Item->NextItem;
    } // .if
    else
    {
      /*!*/
      Assert( ParentItem != NULL );
      ParentItem->NextItem = Item->NextItem;
    } // .else
  } // .else
  delete Item;
  Item = NULL;
  this->fItemCount--;
} // .procedure TAssocArray.RemoveItem



void __fastcall TAssocArray::FreeNode(/*IN*/ /*n*/ PAssocArrayNode& Node )
{


/*U*/
  PAssocArrayItem Item = NULL;
/*U*/
  PAssocArrayItem NextItem = NULL;
  Item = NULL;
  NextItem = NULL;
  // * * * * * //
  if ( Node != NULL )
  {
    Item = Node->Item;
    while ( Item != NULL )
    {
      NextItem = Item->NextItem;
      this->FreeItemValue( Item );
      delete Item;
      Item = NULL;
      Item = NextItem;
    } // .while
    this->FreeNode( Node->ChildNodes[LEFT_CHILD] );
    this->FreeNode( Node->ChildNodes[RIGHT_CHILD] );
    delete Node;
    Node = NULL;
  } // .if
} // .procedure TAssocArray.FreeNode



void __fastcall TAssocArray::Clear( )
{
  /*!*/
  Assert( ! this->Locked );
  this->FreeNode( this->fRoot );
  this->fItemCount = 0;
  this->fNodeCount = 0;
} // .procedure TAssocArray.Clear



String __fastcall TAssocArray::GetPreprocessedKey( const String Key )
{
  String result;
  if ( this->KeyPreprocessFunc == NULL )
  {
    result = Key;
  } // .if
  else
  {
    result = this->KeyPreprocessFunc( Key );
  } // .else
  return result;
} // .function TAssocArray.GetPreprocessedKey



bool __fastcall TAssocArray::IsValidValue(/*n*/ void* Value )
{
  bool result = false;
  result = this->ItemGuardProc( Value, this->ItemsAreObjects, Utils.TItemGuard( this->fItemGuard ) );
  return result;
} // .function TAssocArray.IsValidValue 



int __fastcall TAssocArray::CalcCritDepth( )
{
  int result = 0;
  result = Alg.IntLog2( this->NodeCount + 1 ) << 1;
  return result;
} // .function TAssocArray.CalcCritDepth



int __fastcall AssocArrayCompareNodes( int A, int B )
{
  int result = 0;
  if ( ((PAssocArrayNode) A )->Hash > ((PAssocArrayNode) B )->Hash )
  {
    result = + 1;
  } // .if
  else
    if ( ((PAssocArrayNode) A )->Hash < ((PAssocArrayNode) B )->Hash )
    {
      result = - 1;
    } // .ELSEIF
    else
    {
      result = 0;
    } // .else
  return result;
} // .function AssocArrayCompareNodes



void __fastcall TAssocArray::ConvertToLinearNodeArray( TLinearNodeArray& Res )
{
  int LeftInd = 0;
  int RightInd = 0;
  int RightCheckInd = 0;
  int NumNotProcessedNodes = 0;
/*U*/
  PAssocArrayNode CurrNode = NULL;
  int i = 0;
  Res.NodeArray.Length = this->NodeCount;
  Res.NodeCount = this->NodeCount;
  Res.ItemCount = this->ItemCount;
  if ( this->NodeCount > 0 )
  {
    CurrNode = this->fRoot;
    LeftInd = 0;
    Res.NodeArray[LeftInd] = CurrNode;
    RightInd = this->NodeCount;
    RightCheckInd = RightInd - 1;
    NumNotProcessedNodes = this->NodeCount - 1;
    while ( NumNotProcessedNodes > 0 )
    {
      if ( CurrNode->ChildNodes[RIGHT_CHILD] != NULL )
      {
        RightInd--;
        Res.NodeArray[RightInd] = CurrNode->ChildNodes[RIGHT_CHILD];
        NumNotProcessedNodes--;
      } // .if
      if ( CurrNode->ChildNodes[LEFT_CHILD] != NULL )
      {
        CurrNode = CurrNode->ChildNodes[LEFT_CHILD];
        LeftInd++;
        Res.NodeArray[LeftInd] = CurrNode;
        NumNotProcessedNodes--;
      } // .if
      else
      {
        CurrNode = Res.NodeArray[RightCheckInd];
        RightCheckInd--;
      } // .else
    } // .while
    for ( int 

!!! cut off in the demo version !!!