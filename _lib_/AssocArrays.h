#ifndef AssocArraysH
#define AssocArraysH
/*
DESCRIPTION:  Associative array implementation
AUTHOR:       Alexander Shostak (aka Berserker aka EtherniDee aka BerSoft)
*/

/*
The implementation uses binary tree and (in case of array with string keys) user provided hash function to store and retrieve data.
Tree is automatically rebalanced when critical search depth is met which is equal to 2X of balanced tree height.
Rebalancing is done by converting tree to linear node array and inserting nodes in empty tree.
*/

/***/


#include <System.hpp>  /***/

#include <sysutils.hpp>
#include "Utils.h"
#include "Alg.h"
#include "Crypto.h"



class TAssocArray;
struct TAssocArrayItem;
struct TAssocArrayNode;
struct TLinearNodeArray;
struct TLinearObjNodeArray;
class TObjArray;
struct TObjArrayNode;


const bool LEFT_CHILD = false;
const bool RIGHT_CHILD = true;
const int NO_KEY_PREPROCESS_FUNC = NULL;


typedef bool TChildNodeSide;
typedef TAssocArrayItem* PAssocArrayItem;

struct TAssocArrayItem {
  String Key;
    /*OUn*/
  void* Value;
    /*On*/
  PAssocArrayItem NextItem;
}; // .record TAssocArrayItem



typedef TAssocArrayNode* PAssocArrayNode;

struct TAssocArrayNode {
  int Hash;
    /*O*/
  PAssocArrayItem Item;
  PAssocArrayNode ChildNodes [ 2/*# range LEFT_CHILD..RIGHT_CHILD*/ ];
}; // .record TAssocArrayNode



typedef int __fastcall ( * THashFunc )( const String );
typedef String __fastcall ( * TKeyPreprocessFunc )( const String );
typedef DynamicArray< PAssocArrayNode > TNodeArray;

struct TLinearNodeArray {
  TNodeArray NodeArray; // Nodes are sorted by hash
  int NodeCount;
  int ItemCount;
}; // .record TLinearNodeArray



class TAssocArray: public Utils::TCloneable
    /***/ {
  typedef Utils::TCloneable inherited;
  friend class TObjArray;
  protected: /***/
      /*On*/
  PAssocArrayNode fRoot;
  THashFunc fHashFunc;
      /*n*/
  TKeyPreprocessFunc fKeyPreprocessFunc;
  bool fOwnsItems;
  bool fItemsAreObjects;
  Utils.TItemGuardProc fItemGuardProc;
      /*On*/
  Utils.TItemGuard fItemGuard;
  int fItemCount;
  int fNodeCount;
  DynamicArray< PAssocArrayNode > fIterNodes;
      /*U*/
  PAssocArrayItem fIterCurrItem;
  int fIterNodeInd;
  bool fLocked;
  PAssocArrayItem __fastcall CloneItem( PAssocArrayItem Item ); /*O*/
  PAssocArrayNode __fastcall CloneNode(/*n*/ PAssocArrayNode Node ); /*On*/
  void __fastcall FreeItemValue( PAssocArrayItem Item );
  void __fastcall FreeNode(/*IN*/ /*n*/ PAssocArrayNode& Node );
  void __fastcall RemoveNode(/*n*/ PAssocArrayNode ParentNode, PAssocArrayNode ItemNode );
  void __fastcall RemoveItem(
        /*n*/ PAssocArrayNode ParentNode, PAssocArrayNode ItemNode,
        /*n*/ PAssocArrayItem ParentItem, PAssocArrayItem Item );
      
      /*
        All nodes are placed in NodeArray and disconnected from each other.
        Original binary tree is emptied. Nodes are sorted by hash.
      */
  void __fastcall ConvertToLinearNodeArray( TLinearNodeArray& Res );
  bool __fastcall FindItem( int Hash, const String Key, /*ni*/ PAssocArrayNode& ParentNode, /*ni*/ PAssocArrayNode& ItemNode, /*ni*/ PAssocArrayItem& ParentItem, /*ni*/ PAssocArrayItem& Item );

    /***/
  public: /***/
  __fastcall TAssocArray( THashFunc HashFunc,
                  /*n*/ TKeyPreprocessFunc KeyPreprocessFunc, bool OwnsItems, bool ItemsAreObjects, Utils TItemGuardProc::ItemGuardProc,
        /*IN*/  /*n*/ Utils TItemGuard::& ItemGuard );
  __fastcall virtual ~TAssocArray( );
  virtual void __fastcall Assign( Utils TCloneable::Source );
  void __fastcall Clear( );
  String __fastcall GetPreprocessedKey( const String Key );
  bool __fastcall IsValidValue(/*n*/ void* Value );
  int __fastcall CalcCritDepth( );
  void __fastcall Rebuild( );
  void* __fastcall GetValue( String Key ); /*n*/
  bool __fastcall GetExistingValue( String Key, /*Un*/ void*& Res );
  void __fastcall SetValue( String Key, /*OUn*/ void* NewValue );
  bool __fastcall DeleteItem( String Key );
      
      /* Returns value with specified key and NILify it in the array */
  bool __fastcall TakeValue( String Key, /*OUn*/ void*& Value );
      
      /* Returns old value */
  bool __fastcall ReplaceValue( String Key,
            /*OUn*/ void* NewValue, /*OUn*/ void*& OldValue );
  void __fastcall BeginIterate( );
  bool __fastcall IterateNext( String& Key, /*Un*/ void*& Value );
  void __fastcall EndIterate( );
  __property THashFunc HashFunc = { read = fHashFunc };
  __property TKeyPreprocessFunc KeyPreprocessFunc = { read = fKeyPreprocessFunc };
  __property bool OwnsItems = { read = fOwnsItems };
  __property bool ItemsAreObjects = { read = fItemsAreObjects };
  __property int ItemCount = { read = fItemCount };
  __property Utils TItemGuardProc::ItemGuardProc = { read = fItemGuardProc };
  __property int NodeCount = { read = fNodeCount };
  __property bool Locked = { read = fLocked };
  __property void* Items [ String Key ] = { read = /*n*/ GetValue, write = /*OUn*/ SetValue /*# default */ };
  void * operator [ ] ( String Index ) { return Items [ Index ] ; }
  private: // originally nested
  void __fastcall InsertNode( PAssocArrayNode InsNode );
  void __fastcall InsertNodeRange( int MinInd, int MaxInd, TNodeArray& NodeArray );
}; // .class TAssocArray



typedef TObjArrayNode* PObjArrayNode;

struct TObjArrayNode {
  int Hash;  // Hash is encoded {U} Key: pointer
    /*OUn*/
  void* Value;
  PObjArrayNode ChildNodes [ 2/*# range LEFT_CHILD..RIGHT_CHILD*/ ];
}; // .record TObjArrayItem



typedef DynamicArray< PObjArrayNode > TObjNodeArray;

struct TLinearObjNodeArray {
  TObjNodeArray NodeArray;  // Nodes are sorted by hash
  int NodeCount;
}; // .record TLinearObjNodeArray



class TObjArray: public Utils::TCloneable
    /***/ {
  typedef Utils::TCloneable inherited;
  friend class TAssocArray;
  protected: /***/
      /*On*/
  PObjArrayNode fRoot;
  bool fOwnsItems;
  bool fItemsAreObjects;
  Utils.TItemGuardProc fItemGuardProc;
      /*On*/
  Utils.TItemGuard fItemGuard;
  int fNodeCount;
  DynamicArray< PObjArrayNode > fIterNodes;
  int fIterNodeInd;
  bool fLocked;
  void* __fastcall HashToKey( int Hash ); /*n*/
  int __fastcall KeyToHash( void* Key /*n*/ );
  void __fastcall FreeNodeValue( PObjArrayNode Node );
  void __fastcall FreeNode(/*IN*/ /*n*/ PObjArrayNode& Node );
  void __fastcall RemoveNode(/*n*/ PObjArrayNode ParentNode, PObjArrayNode Node );
  PObjArrayNode __fastcall CloneNode( PObjArrayNode Node ); /*O*/
      
      /*
        All nodes are placed in NodeArray and disconnected from each other.
        Original binary tree is emptied. Nodes are sorted by hash.
      */
  void __fastcall ConvertToLinearNodeArray( TLinearObjNodeArray& Res );
  bool __fastcall FindItem(
            /*n*/ void* Key, /*ni*/ PObjArrayNode& ParentNode, /*ni*/ PObjArrayNode& ItemNode );

    /***/
  public: /***/
  __fastcall TObjArray( bool OwnsItems, bool ItemsAreObjects, Utils TItemGuardProc::ItemGuardProc,
        /*IN*/  /*n*/ Utils TItemGuard::& ItemGuard );
  __fastcall virtual ~TObjArray( );
  virtual void __fastcall Assign( Utils TCloneable::Source );
  void __fastcall Clear( );
  bool __fastcall IsValidValue(/*n*/ void* Value );
  int __fastcall CalcCritDepth( );
  void __fastcall Rebuild( );
  void* __fastcall GetValue(/*n*/ void* Key ); /*n*/
  bool __fastcall GetExistingValue(/*n*/ void* Key, /*Un*/ void*& Res );
  void __fastcall SetValue(/*n*/ void* Key, /*OUn*/ void* NewValue );
  bool __fastcall DeleteItem(/*n*/ void* Key );
      
      /* Returns value with specified key and NILify it in the array */
  bool __fastcall TakeValue(/*n*/ void* Key, /*OUn*/ void*& Value );
      
      /*Returns old value*/
  bool __fastcall ReplaceValue(
            /*n*/ void* Key,
            /*OUn*/ void* NewValue, /*OUn*/ void*& OldValue );
  void __fastcall BeginIterate( );
  bool __fastcall IterateNext( /*Un*/ void*& Key, /*Un*/ void*& Value );
  void __fastcall EndIterate( );
  __property bool OwnsItems = { read = fOwnsItems };
  __property bool ItemsAreObjects = { read = fItemsAreObjects };
  __property int ItemCount = { read = fNodeCount };
  __property Utils TItemGuardProc::ItemGuardProc = { read = fItemGuardProc };
  __property int NodeCount = { read = fNodeCount };
  __property bool Locked = { read = fLocked };/*n*/  /*OUn*/
  __property void* Items [ void* Key ] = { read = GetValue, write = SetValue /*# default */ };
  void * operator [ ] ( void* Index ) { return Items [ Index ] ; }
  private: // originally nested
  void __fastcall InsertNode( PObjArrayNode InsNode );
  void __fastcall InsertNodeRange( int MinInd, int MaxInd, TObjNodeArray& NodeArray );
}; // .class TObjArray



TAssocArray* __fastcall NewAssocArr( THashFunc HashFunc,
  /*n*/ TKeyPreprocessFunc KeyPreprocessFunc, bool OwnsItems, bool ItemsAreObjects, TClass ItemType, bool AllowNIL );
TAssocArray* __fastcall NewSimpleAssocArr( THashFunc HashFunc,
  /*n*/ TKeyPreprocessFunc KeyPreprocessFunc );
TAssocArray* __fastcall NewStrictAssocArr(/*n*/ TClass TypeGuard, bool OwnsItems = true );
TObjArray* __fastcall NewObjArr( bool OwnsItems, bool ItemsAreObjects, TClass ItemType, bool AllowNIL );
TObjArray* __fastcall NewSimpleObjArr( );
TObjArray* __fastcall NewStrictObjArr(/*n*/ TClass TypeGuard );


/***/

#endif //  AssocArraysH