#include "MyTypes.h"
// #include "H3DlgItem.hpp"

#include "../__include__/H3API/single_header/H3API.hpp"
// #include "patcher_x86.hpp"
#include <cstdio>
#include "Storage.h"

extern PatcherInstance* Zecondary_Skills;
extern int  max_SS_level;

char lvl_0[512] = "None";		char lvl_0_abb[512] = "No";
char lvl_1[512] = "Level 1";	char lvl_1_abb[512] = "LV 1";
char lvl_2[512] = "Level 2";	char lvl_2_abb[512] = "LV 2";
char lvl_3[512] = "Level 3";	char lvl_3_abb[512] = "LV 3";
char lvl_4[512] = "Level 4";	char lvl_4_abb[512] = "LV 4";
char lvl_5[512] = "Level 5";	char lvl_5_abb[512] = "LV 5";
char lvl_6[512] = "Level 6";	char lvl_6_abb[512] = "LV 6";
char lvl_7[512] = "Level 7";	char lvl_7_abb[512] = "LV 7";
char lvl_8[512] = "Level 8";	char lvl_8_abb[512] = "LV 8";
char lvl_9[512] = "Level 9";	char lvl_9_abb[512] = "LV 9";
char lvl_A[512] = "Level 10";	char lvl_A_abb[512] = "LV 10";
char lvl_B[512] = "Level 11";	char lvl_B_abb[512] = "LV 11";
char lvl_C[512] = "Level 12";	char lvl_C_abb[512] = "LV 12";
char lvl_D[512] = "Level 13";	char lvl_D_abb[512] = "LV 13";
char lvl_E[512] = "Level 14";	char lvl_E_abb[512] = "LV 14";
char lvl_F[512] = "Level 15";	char lvl_F_abb[512] = "LV 15";

// H3SecondarySkillInfoNew SS_Descriptions[28];

char* Skill_Levels[] = {
	lvl_0, lvl_1,lvl_2, lvl_3,lvl_4, lvl_5, lvl_6, lvl_7,lvl_8, lvl_9, lvl_A, lvl_B, lvl_C, lvl_D, lvl_E, lvl_F
};


char* Skill_Levels_abb[] = {
	lvl_0_abb, lvl_1_abb,lvl_2_abb, lvl_3_abb,lvl_4_abb, lvl_5_abb, lvl_6_abb, lvl_7_abb,lvl_8_abb, lvl_9_abb, lvl_A_abb, lvl_B_abb, lvl_C_abb, lvl_D_abb, lvl_E_abb, lvl_F_abb
};

H3SecondarySkillInfoNew znewSkillsDescriptions[_skill_limit_ + 1] = {};
H3SecondarySkillInfoNew* newSkillsDescriptions = znewSkillsDescriptions + 1;


extern "C" __declspec(dllexport) H3SecondarySkillInfoNew* get_H3SecondarySkillInfoNew(int id) {
	return newSkillsDescriptions + id;
}

H3SecondarySkillInfo* oldSkillsDescriptions = (H3SecondarySkillInfo*)  0x00698d88;

_SSDesc_ znewErmSkillsDescriptions[32] = {};
_SSDesc_ *newErmSkillsDescriptions = znewErmSkillsDescriptions + 1;

replace_dword SS_text_dwords[] = {

	//2020-04-24
	{0x00733181 + 3, (unsigned int)(newErmSkillsDescriptions + 1)},
	{0x007331B4 + 3, (unsigned int)(newErmSkillsDescriptions + 1)},
	{0x007514CE + 1, (unsigned int)(newErmSkillsDescriptions + 1)},
	{0x00752045 + 1, (unsigned int)(newErmSkillsDescriptions + 1)},
	{0x007520A6 + 3, (unsigned int)(newErmSkillsDescriptions + 1)},
	{0x00752EE2 + 3, (unsigned int)(newErmSkillsDescriptions + 1)},

	{0x005f0182 + 2, (unsigned int)(Skill_Levels + 1)}, // mov ecx, dword ptr ds:[0x006A75D8]
	{0x005f1390 + 1, (unsigned int)(Skill_Levels + 1)}, // mov eax, dword ptr ds:[0x006A75D8]
	{0x005f0d04 + 2, (unsigned int)(Skill_Levels + 1)}, // mov edx, dword ptr ds:[0x006A75D8]
	// {0x005efe11 + 2, (unsigned int)(Skill_Levels + 1)}, // mov ecx, dword ptr ds:[0x006A75D8]
	{0x005f0fed + 2, (unsigned int)(Skill_Levels + 1)}, // mov ecx, dword ptr ds:[0x006A75D8]

	{0x004dadc3 + 3, (unsigned int)(Skill_Levels + 0)},
	{0x004daddc + 3, (unsigned int)(Skill_Levels + 0)},
	{0x004F5D1B + 3, (unsigned int)(Skill_Levels + 1)},
	{0x004F9304 + 3, (unsigned int)(Skill_Levels + 0)},
	{0x004F933E + 3, (unsigned int)(Skill_Levels + 0)},
	{0x004F96E0 + 3, (unsigned int)(Skill_Levels + 1)},

	/*
	{0x075EED00 + 0, (unsigned int)(Skill_Levels + 1)},
	{0x075EEF00 + 0, (unsigned int)(Skill_Levels + 1)},
	{0x075EF100 + 0, (unsigned int)(Skill_Levels + 1)},
	{0x075Ef200 + 0, (unsigned int)(Skill_Levels + 1)},
	{0x075Ef300 + 0, (unsigned int)(Skill_Levels + 1)},
	*/

	//// 0x006A75D4
	{0x004847B1 + 3, (unsigned int)(Skill_Levels + 0)}, // mov edx, dword ptr ds:[ecx*4+0x6A75D4]
	{0x004DBE9A + 3, (unsigned int)(Skill_Levels + 0)}, // mov eax, dword ptr ds:[edx*4+0x6A75D4]
	{0x004E225A + 3, (unsigned int)(Skill_Levels + 0)}, // mov edi, dword ptr ds:[ecx*4+0x6A75D4]
	{0x005215BD + 3, (unsigned int)(Skill_Levels + 0)}, // mov eax, dword ptr ds:[edx*4+0x6A75D4]
	{0x005B0EB1 + 3, (unsigned int)(Skill_Levels + 0)}, // mov ecx, dword ptr ds:[eax*4+0x6A75D4]
	{0x005B9785 + 2, (unsigned int)(Skill_Levels + 0)}, // mov dword ptr ds:[ecx+0x6A75D4], edx

	/*
	{0x0040DCDA + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x00484797 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004A7DA9 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004A7DDE + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004A7E34 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004DAD9E + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004DBE7D + 2,(unsigned int)(newSkillsDescriptions + 0)},
	// {0x004DE7E8 + 2,(unsigned int)(&newSkillsDescriptions + 0)}, //missing
	{0x004E21F9 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F1536 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F5D85 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F9323 + 1,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F95A9 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F9623 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F96E7 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F9816 + 1,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004FA074 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004FA0BA + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x0051F53B + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005215A0 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005B0367 + 1,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005B0E62 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005B0E91 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005EF874 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005EFD89 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005F0CFB + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005F0FCD + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005F136D + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x00714B1F + 2,(unsigned int)(newSkillsDescriptions + 0)},
	*/

	{0x67DCF0,(unsigned int)(newSkillsDescriptions + 0)},

	{0,0}
};




replace_byte SS_text_bytes[] = {
	{0x0040DCE0 + 2, 0x6},
	{0x004847A1 + 2, 0x6},
	{0x004A7DB5 + 2, 0x6},
	{0x004A7DE9 + 2, 0x6},
	{0x004A7E3C + 2, 0x6},
	// {0x004DADA6 + 2, 0x6}, // heroLevelUp 
	{0x004DBE85 + 2, 0x6},
	//{0x???????? + 2, 0x6},
	{0x004E2201 + 2, 0x6},
	// {} // missing
	// {0x004F5D94 + 2, 0x6},
	// {0x004F932B + 2, 0x6}, // heroLevelUp
	// {} // missing
	// {} // missing
	{0x004F96DD + 2, 0x6},
	// {} // missing
	// {} // missing
	// {} // missing
	// {} // missing
	{0x005215A8 + 2, 0x6},
	// {} // missing
	{0x005B0E6A + 2, 0x6},
	{0x005B0E99 + 2, 0x6},
	// {} // missing
	{0x005EFD93 + 2, 0x6},
	{0x005F0CF5 + 2, 0x6},
	{0x005F0FE1 + 2, 0x6},
	{0x005F1383 + 2, 0x6},
	{0x00714B2A + 2, 0x6},

	{0x00714B2D + 3, 0xC0},


	
	{0x0073317B + 2, 0x06}, // 2020-04-24 added
	{0x007331AB + 2, 0x06},
	{0x007520A0 + 2, 0x06},
	{0x00752EDC + 2, 0x06},
	
	{0x004F1541 + 3, 0x00},

	{0,0}

};

int ret_4F1521 = 0x004F1521;
__declspec(naked) void patch_4F1507(void) {
	_asm {
		push 0
		xor ecx, ecx
		lea edx, [esi - 0x10]
		jmp ret_4F1521
	}

}

//=================================================================================================//

char university_levels[1024] = {};
int university_price[16] = { 2000, 3500, 5500, 7500, 10000,
							13500, 17000, 21000, 25000, 30000,
							35000, 40000, 45000, 50000, 65000, 77700 };
int max_university_skill_level = 5; int max_university_skill_level_default[2] = { 5,5 };
h3::H3Hero* UniversityHero = nullptr;
int* UniversitySkills = nullptr;
#include"z_msgbox.hpp"

inline void MixedPos(int* x, int* y, int* l, int pos)
{
	if (pos >= 0x04000000) *l = 1; else *l = 0;
	*x = pos & 0x000003FF;
	*y = (pos >> 16) & 0x03FF;

}

h3::H3Dlg* __stdcall Dlg_University_hook(HiHook* hook, h3::H3Dlg* ecx0, void* keys, int z_this, int source) {
	UniversitySkills = (int*)(z_this);
	max_university_skill_level = max_university_skill_level_default[source];


	if (!source) {
		// auto her = *(h3::H3Hero**)0x027F9970;
		auto her = (h3::H3Hero*) keys;
		int x = her->x; int y = her->y; int z = her->z;
		auto mip = h3::H3AdventureManager::Get()->GetMapItem(x, y, z);
		// auto mip = h3::H3AdventureManager::Get()->GetMapItem();
		auto uni = &mip->university;
		if (mip->objectType == 104 && university_levels[uni->id])
			max_university_skill_level = university_levels[uni->id];
	}

	Zecondary_Skills->WriteByte(0x0068C680, 'z');
	auto fun = hook->GetDefaultFunc();
	auto ret = THISCALL_4(h3::H3Dlg*, fun, ecx0, keys, z_this, source);

	h3::H3Hero* me = *(h3::H3Hero**)(int(ret) + 0x60);
	UniversityHero = me;

	/*
	for (int d = 0; d < 0xff; ++d) {
		auto ptr = ret->GetDef(d);
		if (!ptr) continue;
		z_shout(ptr->GetDef()->GetName());
	}
	*/

	/*
	for (int i = 0; i < 4; ++i)
	{
		int skill_id = ((int*)(z_this))[i];
		int skill_LV = get_hero_SS(me->id, skill_id);
		int frame = skill_id << 4 + skill_LV;
		// ret->GetDef(0x70 + 3 * i)->SetFrame(frame);
		// ecx0->GetDef(0x78 +i)->SetFrame(frame);
		auto item = ret->GetH3DlgItem(0x68 + 3 * i);
		if(item)((h3::H3DlgDef*)item)->SetFrame(frame);
	}
	*/

	/*
	auto uni = (h3::H3MapitemUniversity*) GetUniMapItem(me->mixedPosition);
	if (!source && university_levels[uni->id]) {
		max_university_skill_level = university_levels[uni->id];
	}
	*/

	// ret->GetDef();

	return ret;
}

char __stdcall  sub_005EF860_hook(HiHook* hook, int _this_, char a2, char a3) {
	char result;

	if (!a2)
		return 0;

	int skill_id = *(int*)(_this_ + 76);
	int _dlg_ = *(int*)(_this_ + 4);
	h3::H3Hero* me = *(h3::H3Hero**)(_dlg_ + 0x60);
	// UniversityHero = me;
	// char skill_LV = *(char*)(_this_ + 72);
	char skill_LV = get_hero_SS(me->id, skill_id);
	int next_level = skill_LV + (skill_LV < max_university_skill_level);
	auto frame = 16 + 16 * skill_id + next_level;
	auto z_dlg = (h3::H3Dlg*)_dlg_;
	int i = 0; while (UniversitySkills[i] != skill_id) ++i;

	if (a3)
	{
		// int next_level = skill_LV + (skill_LV < 15);
		LPCSTR text = znewSkillsDescriptions[skill_id + 1].description[next_level - 1];
		//b_MsgBox(*(16 * *(this + 76) + o_SSText + 4), 4, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
		// z_shout(znewSkillsDescriptions[skill_id + 1].description[skill_LV]);
		// auto frame = 16 + 16 * skill_id + next_level;
		FASTCALL_12(void*, 0x004F6C00, text, 4, -1, -1, 20, frame + _Frame_Shift_, -1, 0, -1, 0, -1, 0);
		result = 1;
	}
	else
	{
		if (z_dlg->GetDef(0x69 + 3 * i)->GetFrame() == 2)
		{
			z_shout("You Cannot Learn this Skill.");
		}
		else
			if (skill_LV < max_university_skill_level) {
				int current_price = university_price[skill_LV];
				// int &player_gold = h3::H3Main::Get()->players[o_ActivePlayerID].playerResources.gold;
				int& player_gold = h3::H3Main::Get()->players[me->owner].playerResources.gold;

				FASTCALL_12(void*, 0x004F6C00, "Do You want to Learn ?",
					2, -1, -1, 20, frame + _Frame_Shift_, 6, current_price, -1, 0, -1, 0);
				auto ans = msg_answer();
				if (ans) {
					if (player_gold < current_price) {
						z_shout("Not enough gold.");
					}
					else {
						player_gold -= current_price;
						set_hero_SS(me->id, skill_id, next_level);
						// auto z_dlg = (h3::H3Dlg*)_dlg_;
						int show_level = next_level + (next_level < max_university_skill_level);
						// int i = 0; while (UniversitySkills[i] != skill_id) ++i;
						int state = next_level < max_university_skill_level;
						// z_dlg->GetDef(0x6b + 3 * i)->SetFrame(frame + (next_level < max_university_skill_level));


						auto item = z_dlg->GetH3DlgItem(0x68 + 3 * i);
						if (item) {
							auto it = ((h3::H3DlgDef*)item);
							it->SetFrame(frame + (next_level < max_university_skill_level));
							if (!state) it->SetHint("You cannot learn more about this skill here.");
						}
						z_dlg->GetDef(0x69 + 3 * i)->SetFrame(state);
						z_dlg->GetDef(0x6a + 3 * i)->SetFrame(state);
						z_dlg->GetText(10000 + i)->SetText(Skill_Levels[show_level]);
						z_dlg->Redraw();
					}

					// z_shout("ans");
				}

				/*
				// Zecondary_Skills->WriteDword(0x005F0CF0 + 1, university_price[skill_LV]);
				Zecondary_Skills->WriteDword(0x005F10E8 + 6, university_price[skill_LV]);
				THISCALL_2(void, 0x005F0E90, *(int*)(_this_ + 4), skill_id);
				*/
			}
			else z_shout("You cannot learn more about this skill here.");
		// todo
		result = 1;
	}
	return result;
}

_LHF_(hook_005F0BBB) {
	c->esi = (int)"You cannot learn more about this skill here.";
	c->return_address = 0x005F0BC1;
	return SKIP_DEFAULT;
}

_LHF_(hook_005F0ED1) {
	char skill_LV = c->ecx & 0xff;


	if (skill_LV < max_university_skill_level) {
		c->return_address = 0x005F0F24;
		return SKIP_DEFAULT;
	}
	else {
		c->ecx = *(int*)(0x006A5DC4);
		c->return_address = 0x005F0EDB;
		return SKIP_DEFAULT;
	}

}

_LHF_(hook_005F0BA9) {
	char skill_LV = c->edx & 0xff;
	if (skill_LV < max_university_skill_level) {
		c->return_address = 0x005F0C0C;
		return SKIP_DEFAULT;
	}
	else {
		c->ecx = *(int*)(0x006A5DC4);
		c->return_address = 0x005F0BB1;
		return SKIP_DEFAULT;
	}
}

_LHF_(hook_005EFE11) {
	static int i = 0;
	auto hero = *(h3::H3Hero**)(*(int*)(c->ebp - 0x18) + 96);
	int skill_id = *(int*)(c->ebp - 0x24);
	int skill_LV = get_hero_SS(hero->id, skill_id);
	int skill_next = skill_LV + (skill_LV < max_university_skill_level);
	c->ecx = (int)Skill_Levels[skill_next];

	c->Push(8); c->Push(0); c->Push(1);
	c->Push(10000 + i);

	i++; if (i >= 4) i = 0;

	c->return_address = 0x005EFE1F;
	return SKIP_DEFAULT;
}

_LHF_(hook_0073B62D) {
	if (c->AL() != 'L') return EXEC_DEFAULT;

	if (*(int*)(c->ebp + 0x0C) == 1)
	{
		auto uni = (h3::H3MapitemUniversity*)
			*(int*)(c->ebp - 4);
		char& before = university_levels[uni->id];
		if (!before) before = max_university_skill_level_default[0];
		CDECL_4(int,   0x0074195D, &before, 1, *(int*)(c->ebp + 0x14), 0);

		c->return_address = 0x0073B6BA;
		c->eax = 1; return SKIP_DEFAULT;
	}
	else {
		z_shout("\"!!UR:L\"-wrong syntax.");
		c->return_address = 0x0073B6BA;
		c->eax = 0; return SKIP_DEFAULT;
	}
}

_LHF_(hook_005EFBF8) {
	c->eax <<= 4;
	c->edx = *((int*)(0x00643B90 + c->edx));

	c->return_address = 0x005EFC01;
	return SKIP_DEFAULT;
}

_LHF_(hook_005EFC08) {
	int skill_id = c->eax;

	auto hero = *(h3::H3Hero**)(*(int*)(c->ebp - 0x18) + 96);
	int skill_LV = get_hero_SS(hero->id, skill_id);
	int skill_next = skill_LV + (skill_LV < max_university_skill_level);
	auto frame = 16 + 16 * skill_id + skill_next;

	c->Push(frame);
	++(c->edx);
	c->Push(0x0068C680);
	c->Push(c->ecx);

	c->return_address = 0x005EFC10;
	return SKIP_DEFAULT;
}
_LHF_(hook_005F0D7E) {
	int skill_id = c->eax;
	int skill_LV = get_hero_SS(UniversityHero->id, skill_id);
	int skill_next = skill_LV + (skill_LV < max_university_skill_level);
	auto frame = 16 + (skill_id << 4) + skill_next;
	++(c->eax);		c->Push(frame);
	c->return_address = 0x005F0D83;
	return SKIP_DEFAULT;
}

int tmp_005F0CF0 = 0;
_LHF_(hook_005F0CF0) {
	int skill_id = c->eax;
	int Skill_LV = get_hero_SS(UniversityHero->id, skill_id);
	tmp_005F0CF0 = Skill_LV;
	c->Push(university_price[Skill_LV]);
	c->return_address = 0x005F0CF5;
	return SKIP_DEFAULT;
}

_LHF_(hook_005F0D04) {
	int Skill_LV = tmp_005F0CF0;
	c->edx = (int)(Skill_Levels[Skill_LV + 1]);
	c->return_address = 0x005F0D0A;
	return SKIP_DEFAULT;
}

//=================================================================================================//

char witch_hut_levels[1024] = {};
int max_witch_hut_skill_level = 5; int max_witch_hut_skill_level_default = 5;

int witch_hut_price[16] = { 0,		2000,  3500,  5500, 7500,
							10000, 13500, 17000, 21000, 25000,
							30000, 35000, 40000, 45000, 50000, 65000 };

int get_wh_id(int x, int y, int z) {
	int X, Y, Z; int i = 0;
	if (CDECL_6(int, 0x0072F539, 113, -1, 1, &X, &Y, &Z))
		return 1023;
	while (X != x || Y != y || Z != z) {
		if (CDECL_6(int, 0x0072F67B, 113, -1, &X, &Y, &Z, -1))
			return 1023;
		++i; if (i > 1023) return 1023;
	}

	return i;
}

int get_wh_id(h3::H3MapItem* mip) {
	int X, Y, Z; int i = 0; // h3::H3MapItem* MIP = nullptr;
	if (CDECL_6(int, 0x0072F539, 113, -1, 1, &X, &Y, &Z))
		return 1023;
	auto MIP = h3::H3AdventureManager::Get()->GetMapItem(X, Y, Z);
	while (MIP != mip) {
		if (CDECL_6(int, 0x0072F67B, 113, -1, &X, &Y, &Z, -1))
			return 1023;
		++i; if (i > 1023) return 1023;
		MIP = h3::H3AdventureManager::Get()->GetMapItem(X, Y, Z);
	}

	return i;
}

_LHF_(hook_0073AD70) {
	if (c->DL() != 'L') return EXEC_DEFAULT;

	if (*(int*)(c->ebp + 0x0C) == 1)
	{
		auto mp = (h3::H3MapItem*)
			*(int*)(c->ebp - 4);

		/*
		auto her = *(h3::H3Hero**)0x027F9970;
		int x = her->x; int y = her->y; int z = her->z;
		*/
		/*
		int x = 0; int y = 0; int z = 0;
		int ID = get_wh_id(x, y, z);
		*/

		int ID = get_wh_id(mp);
		char& before = witch_hut_levels[ID];

		if (!before) before = max_witch_hut_skill_level_default;
		CDECL_4(int,   0x0074195D, &before, 1, *(int*)(c->ebp + 0x14), 0);

		c->return_address = 0x0073ADE0;
		c->eax = 1; return SKIP_DEFAULT;
	}
	else {

		z_shout("\"!!WH:L\"-wrong syntax.");
		c->return_address = 0x0073ADE0;
		c->eax = 0; return SKIP_DEFAULT;
	}

	return SKIP_DEFAULT;
}

_LHF_(hook_004A7D93) {
	auto her = (h3::H3Hero*)c->ebx;
	c->eax = get_hero_SS(her->id, c->esi);
	int skill_id = c->esi; int skill_LV = c->eax;
	max_witch_hut_skill_level = max_witch_hut_skill_level_default;

	int x = her->x; int y = her->y; int z = her->z;
	// auto mip = h3::H3AdventureManager::Get()->GetMapItem(x, y, z);

	int ID = get_wh_id(x, y, z);
	int custom_level = witch_hut_levels[ID];
	if (custom_level) max_witch_hut_skill_level = custom_level;

	auto is_human = h3::H3Main::Get()->players[her->owner].is_human;


	if (c->eax == 0) {
		auto frame = 16 + 16 * skill_id + skill_LV + 1;
		if (is_human)
			FASTCALL_12(void*, 0x004F6C00, "Do You want to Learn ?",
				2, -1, -1, 20, frame + _Frame_Shift_, -1, 0, -1, 0, -1, 0);
		auto ans = is_human ? msg_answer() : true;
		if (ans) {
			int next_level = 1; skill_LV = 1;
			set_hero_SS(her->id, skill_id, next_level);
			goto label;
		}

		c->return_address = 0x004A7E8F;
		c->eax = 0;  return SKIP_DEFAULT;
	}
	else if (c->eax < max_witch_hut_skill_level) {
	label:
		bool ans = true; while (ans && skill_LV < max_witch_hut_skill_level)
		{
			int current_price = university_price[skill_LV];
			int& player_gold = h3::H3Main::Get()->players[her->owner].playerResources.gold;

			int next_level = skill_LV + (skill_LV < max_witch_hut_skill_level);
			auto frame = 16 + 16 * skill_id + next_level;

			if (is_human)
				FASTCALL_12(void*, 0x004F6C00, "Do You want to Learn ?",
					2, -1, -1, 20, frame + _Frame_Shift_, 6, current_price, -1, 0, -1, 0);
			ans = is_human ? msg_answer() : true;
			if (ans) {
				if (player_gold < current_price) {
					ans = false;
					if (is_human) z_shout("Not enough gold.");
				}
				else {
					player_gold -= current_price;
					++skill_LV;
					set_hero_SS(her->id, skill_id, next_level);
				}
			}
			// else break;
		}
		c->return_address = 0x004A7E8F;
		c->eax = 0;  return SKIP_DEFAULT;
	}
	else {
		c->return_address = 0x004A7D9E;
		return SKIP_DEFAULT;
	}
}

//=================================================================================================//

_LHF_(hook_004A4B1B) {
	c->Push(0); c->Push(-1);
	int Skill_ID = c->edi;
	auto her = (h3::H3Hero*)c->ebx;
	int skill_LV = get_hero_SS(her->id, Skill_ID);

	int frame = (Skill_ID << 4) + skill_LV
		+ 16 + (skill_LV < max_SS_level);
	c->Push(frame + _Frame_Shift_);
	c->return_address = 0x004A4B20;
	return SKIP_DEFAULT;
}


void SS_text_clean(void) {
	char buf[4096];
	
	znewSkillsDescriptions->name = new char[4096];
	strcpy((char*)znewSkillsDescriptions->name, "Unknown skill");
	for (int j = 0; j < 15; j++) {
		char* zdesc = new char[4096];
		//sprintf(buf, "Unknown Skill description level %i", j + 1);
		//strcpy(zdesc, buf);
		strcpy(zdesc, "Unknown skill");
		znewSkillsDescriptions->description[j] = zdesc;
	}

	H3SecondarySkillInfo *old = oldSkillsDescriptions;
	for (int i = 0; i < /*_skill_limit_*/ _SOD_SS_Count_; i++) {
		// sprintf(buf, "Skill %i name", i);
		char* zname = new char[4096];
		// strcpy(zname,buf);
		// newSkillsDescriptions[i].name = zname;
		newSkillsDescriptions[i].name = zname;
		if (old[i].name)
			strcpy(zname, old[i].name);
		else sprintf(zname, "Skill %i",i);
		for (int j = 0; j < 3; j++) {

			char* zdesc = new char[4096];
			newSkillsDescriptions[i].description[j] = zdesc;
			if(old[i].description[j])
				strcpy(zdesc , old[i].description[j]);
			else
				sprintf(zdesc, "Skill %i description level %i", i, j + 1);
		}
		for (int j = 3; j < 15; j++) {
			char* zdesc = new char[4096];
			sprintf(buf, "Skill %i description level %i", i, j+1);
			strcpy(zdesc, buf);
			newSkillsDescriptions[i].description[j] = zdesc;
		}

	}

	for (int i = _SOD_SS_Count_; i < _skill_limit_; i++) {
		
		if (!newSkillsDescriptions[i].name) {
			char* zname = new char[4096];
			sprintf(zname, "Skill %i", i);
			newSkillsDescriptions[i].name = zname;
		}
		for (int j = 0; j < 15; j++) {
			if (!newSkillsDescriptions[i].description[j]) {
				char* zdesc = new char[4096];
				sprintf(buf, "Skill %i description level %i", i, j + 1);
				strcpy(zdesc, buf);
				newSkillsDescriptions[i].description[j] = zdesc;
			}
		}
	}

	

	/// University
	Zecondary_Skills->WriteLoHook(0x005F0BBB, hook_005F0BBB);
	Zecondary_Skills->WriteHiHook(0x005EF860, SPLICE_, SAFE_, THISCALL_, sub_005EF860_hook);
	Zecondary_Skills->WriteHiHook(0x005EF8D0, SPLICE_, SAFE_, THISCALL_, Dlg_University_hook);
	Zecondary_Skills->WriteLoHook(0x005F0ED1, hook_005F0ED1);
	Zecondary_Skills->WriteLoHook(0x005F0BA9, hook_005F0BA9);
	// Zecondary_Skills->WriteLoHook(0x005F0CF0, hook_005F0CF0);
	Zecondary_Skills->WriteLoHook(0x005EFE11, hook_005EFE11);
	Zecondary_Skills->WriteLoHook(0x0073B62D, hook_0073B62D);
	// Zecondary_Skills->WriteLoHook(0x005EFBF8, hook_005EFBF8);
	// Zecondary_Skills->WriteByte(0x0068C680, 'z');
	// Zecondary_Skills->WriteWord(0x005EFBF8, 0x9090);
	// Zecondary_Skills->WriteByte(0x005EFBF8+2, 0x90);
	// Zecondary_Skills->WriteLoHook(0x005EFC08, hook_005EFC08);
	// Zecondary_Skills->WriteHiHook(0x005F0B60, SPLICE_, EXTENDED_, THISCALL_, University_sub_005F0B60_hook);
	Zecondary_Skills->WriteLoHook(0x005F0D7E, hook_005F0D7E);
	Zecondary_Skills->WriteLoHook(0x005F0CF0, hook_005F0CF0);
	// Zecondary_Skills->WriteLoHook(0x005F0D04, hook_005F0D04);
	// Zecondary_Skills->WriteLoHook(0x005F0BA0, hook_005F0BA0);


	/// Witch Hut
	Zecondary_Skills->WriteLoHook(0x0073AD70, hook_0073AD70);
	Zecondary_Skills->WriteLoHook(0x004A7D93, hook_004A7D93);


	// Scholar
	Zecondary_Skills->WriteLoHook(0x004A4B1B, hook_004A4B1B);


}

extern "C" __declspec(dllexport) void set_SS_Description(int skill, int level, const char* text) {
	if (!newSkillsDescriptions[skill].description[level - 1])
		newSkillsDescriptions[skill].description[level - 1] = new char[4096];
	strcpy( (char*) newSkillsDescriptions[skill].description[level - 1] , text);
}

extern "C" __declspec(dllexport) void set_SS_Name(int skill, const char* text) {
	if (!newSkillsDescriptions[skill].name)
		newSkillsDescriptions[skill].name = new char[4096];
	strcpy((char*)newSkillsDescriptions[skill].name, text);
}

extern "C" __declspec(dllexport) const char* get_SS_Name(int skill) {
	static char z_default[] = "Nothing";
	if (skill < 0) return z_default;
	return newSkillsDescriptions[skill].name;
}

extern "C" __declspec(dllexport) void set_SS_level_text(int level, const char* text) {
	strcpy((char*) Skill_Levels[level], text);
}


bool ParseStr(char* buf, char* name, /*char* &target */ char res[char_table_size]);
extern "C" __declspec(dllexport) void ApplyDescriptions(int skillID, char* config_str) {
	char name[512] = "Skill_Name=\""; char buf[4096];
	if(ParseStr(config_str, name, buf))
		set_SS_Name(skillID,buf);

	for (int i = 1; i <= 15 ; ++i) {
		sprintf(name, "Level_%i_Description=\"", i);
		if (ParseStr(config_str, name, buf))
			set_SS_Description(skillID,i,buf);
	}
}