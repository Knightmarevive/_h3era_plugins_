#include "MyTypes.h"
// #include "patcher_x86.hpp"
// #include "H3DlgItem.hpp"

// #include "../__include__/H3API/single_header/H3API.hpp"
#include "heroes.h"

#include "Storage.h"
#include <cstdio>

/*
struct H3SecondarySkillInfoNew
{
	LPCSTR name;
	LPCSTR description[15];
};
*/



char  __stdcall get_hero_SS(HERO * H, int skilllnum);

extern PatcherInstance* Zecondary_Skills;
extern char* Skill_Levels[];
extern H3SecondarySkillInfoNew *newSkillsDescriptions; // [_skill_limit_ + 1] ;
extern int max_SS_level; extern char* Skill_Levels[];

replace_byte level_up_bytes[] = {

	{0x004DAE16 + 2, 0x04}, 
	{0x004DAE1D + 2, 0x06},

	//{0x004F92F7 + 1, 0x10},
	//{0x004F931B + 1, 0x10},

	{0x004DADA6 + 2, 0x06},
	{0x004DADCD + 2, 0x06},

	{0x004DAE16 + 2, 0x06},
	{0x004DAE1D + 0, 0x90},
	{0x004DAE1D + 1, 0x90},
	{0x004DAE1D + 2, 0x90},

	//{0x004F932E + 3, 0xC0},
	//{0x004F9332 + 3, 0xC0},
	//{0x004F932B + 2, 0x06},

	{0x004F92F7 + 0, 0x8b},
	{0x004F92F7 + 1, 0xd0},
	{0x004F92F7 + 2, 0x83},
	{0x004F92F7 + 3, 0xE2},
	{0x004F92F7 + 4, 0x0F},
	{0x004F92F7 + 5, 0x90},

	{0x004F92FD + 0, 0x90},
	{0x004F92FD + 1, 0x90},

	//{0x004F92E6 + 0, 0x90},
	//{0x004F92E6 + 1, 0x90},

	//{0x004F9328 + 0, 0x90},
	//{0x004F9328 + 1, 0x90},
	//{0x004F9328 + 2, 0x90},

	//{0x004F92F1 + 2, 0x06},



	{0x004F931B + 0, 0x8b},
	{0x004F931B + 1, 0xd0},
	{0x004F931B + 2, 0x83},
	{0x004F931B + 3, 0xE2},
	{0x004F931B + 4, 0x0F},
	{0x004F931B + 5, 0x90},

	{0x004F9321 + 0, 0x90},
	{0x004F9321 + 1, 0x90},


	{0x004F96D5 + 0, 0x8b},
	{0x004F96D5 + 1, 0xd0},
	{0x004F96D5 + 2, 0x83},
	{0x004F96D5 + 3, 0xE2},
	{0x004F96D5 + 4, 0x0F},
	{0x004F96D5 + 5, 0x90},

	{0x004F96DB + 0, 0x90},
	{0x004F96DB + 1, 0x90},
	

	{0x0,0x0}
};

int ret_4DAE38 = 0x004DAE38;
__declspec(naked) void hero_level_smallpatch_1(void) {
	//	004DAE30 | 8D447F 03 | lea eax, dword ptr ds : [edi + edi * 2 + 0x3] |
	//	004DAE34 | 8D5476 03 | lea edx, dword ptr ds : [esi + esi * 2 + 0x3] |
	_asm {
		//lea eax, dword ptr ds : [edi + edi * 2 + 0x3]
		mov eax,edi
		inc eax
		shl eax, 4
		inc eax

		//lea edx, dword ptr ds : [esi + esi * 2 + 0x3]
		mov edx, esi
		inc edx
		shl edx, 4
		inc edx

		jmp ret_4DAE38
	}
}


int ret_4DAD33 = 0x004DAD33;
_declspec(naked) void hero_level_smallpatch_2(void) {
	//	004DAD2D | 8D4C7F 03 | lea ecx,dword ptr ds:[edi+edi*2+0x3] 
	_asm {
		mov ecx, edi
		inc ecx
		shl ecx, 4
		inc ecx

		add eax, ecx

		jmp ret_4DAD33
	}

}

/*
int ret_4F9323 = 0x004F9323;
_declspec(naked) void hero_level_smallpatch_4(void) {

// 004F930E | F7EB | imul ebx |
// 004F9310 | 8BC2 | mov eax, edx |
// 004F9312 | C1E8 1F | shr eax, 0x1F |
// 004F9315 | 03D0 | add edx, eax |
// 004F9317 | 8BC3 | mov eax, ebx |
// 004F9319 | 8BFA | mov edi, edx |
// 004F931B | BB 10000000 | mov ebx, 0x10 |
// 004F9320 | 99 | cdq |
// 004F9321 | F7FB | idiv ebx |

	_asm {
		//mov  eax, ebx
		//mov  edi, [ebp+0x14]
		mov edi, 0x20
		 // mov ebx, eax

		 //and eax, 0xfffffff0
		 //and edi, 0xfffffff0

		 shr ebx, 4
		 shr edi, 4


		 shl ebx, 6

		jmp ret_4F9323
	}

}
*/

/*
// hook at 0x4f92e1
_LHF_(hero_level_smallhook_4) {

	c->Push( (int) newSkillsDescriptions[(c->ebx >> 4) - 1].name);
	c->Push( (int) Skill_Levels[c->ebx & 0xf]);
	c->Push( (int) newSkillsDescriptions[(c->edi >> 4) - 1].name);
	c->Push( (int) Skill_Levels[c->edi & 0xf]);


	c->ref_local_n(0x18 / 4) = int(&Skill_Levels[c->ebx & 0xf]);
	c->ref_arg_n(2) =(int) &Skill_Levels[c->edi & 0xf];

	// c->edi &= (int)&Skill_Levels[c->edi & 0xf];

	c->return_address = 0x4f934b;
	return NO_EXEC_DEFAULT;
}
*/

/*
// hook at 0x4f92e1
_LHF_(hero_level_smallhook_4b) {

	const char* LN = newSkillsDescriptions[(c->ebx >> 4) - 1].name;
	const char* LL = Skill_Levels[c->ebx & 0xf];
	const char* RN = newSkillsDescriptions[(c->edi >> 4) - 1].name;
	const char* RL = Skill_Levels[c->edi & 0xf];

	c->eax = sprintf((char*)0x697428, " Test Level Up ");

	c->return_address = 0x004F9356;
	return NO_EXEC_DEFAULT;
}
*/


_LHF_(hero_level_smallhook_5) {

	//c->eax = *(int*)(c->edx + 0);
	//c->edx = *(int*)c->ecx;
	//c->return_address = 0x004F95B8;
	
	c->edi = 0x10 + ((ref_arg_n(c,3) & 0xfffffff0) << 2) -0x40;
	return EXEC_DEFAULT;
}

_LHF_(hero_level_smallhook_6) {

	c->eax = 0x10 + ((ref_arg_n(c,4) & 0xfffffff0) << 2) - 0x40;
	return EXEC_DEFAULT;

}

_LHF_(hero_level_smallhook_7) {

	//// TODO
	//c->edi = 0x10 + ((c->ref_arg_n(3) & 0xfffffff0) << 2) - 0x40;
	c->edi = 0x10 + c->edi - 0x40;

	return EXEC_DEFAULT;
}



_LHF_(hero_level_smallhook_8) {
	//c->eax = 

	c->ecx = ((int*) (*(int*)0x67DCF0)) [c->edi -0x10];

	c->Push(-1); c->Push(0);
	c->Push(-1); c->Push(0);
	c->Push(-1); c->Push(-1); c->Push(-1);

	c->return_address = 0x004FA07E;
	return NO_EXEC_DEFAULT;
}


_LHF_(hero_level_smallhook_9) {
	//c->eax = 

	c->ecx = ((int*)(*(int*)0x67DCF0))[c->edi - 0x10];

	c->Push(-1); c->Push(0);
	c->Push(-1); c->Push(0);
	c->Push(-1); c->Push(-1); c->Push(-1);

	c->return_address = 0x004FA0C4;
	return NO_EXEC_DEFAULT;
}

int skill; int level;
extern int meet_hero_left; 
extern int meet_hero_right;
_LHF_(hero_level_smallhook_A) {
	//c->eax = 
	//int ptr = *(int*) (c->ebp - 0x0C);
	/*int */ skill = c->edx;
	int side  = c->eax;
	int heronum = side?meet_hero_right:meet_hero_left;
	HERO* hero = (HERO*)GetHeroRecord(heronum);
	//int ptr = c->eax*4;
	//short level = *(short*) (c->ebx+ptr+0x40);
	
	//short level = *(short*) (*(int*)(c->ebx + (c->eax *4) + 0x40)) ;
	/* int */ level = hero->SSkill[skill];

	//c->edi = (int) (((char**)0x67DCF0)[ (c->edx << 4) + level ]);
	c->edi = ((int*)(*(int*)(0x67DCF0)))[(skill << 4) + level]; // this line works

	c->Push(0); c->Push(-1); 
	c->Push(0); c->Push(-1); 
	c->Push(0); c->Push(-1);

	c->return_address = 0x005B036F;
	return NO_EXEC_DEFAULT;
}


HERO* current_hero = 0;
extern int left_hero_SS[10];
extern int right_hero_SS[10];

_LHF_(hero_level_smallhook_B) {
	//H3BaseDlg* dialog = (H3BaseDlg*)*(int*)(c->ebx + 0x38);
	//int focus = dialog->focusedItemId;
	int frame = 16 + (skill << 4) + level;
	//if (focus >= 200 && focus <= 207) frame = left_hero_SS[focus - 200 + 1];
	//if (focus >= 208 && focus <= 215) frame = left_hero_SS[focus - 208 + 1];
	*(int*)(c->esp + 0xc) = frame + _Frame_Shift_;
	
	return EXEC_DEFAULT;
}


/*
_LHF_(hero_level_smallhook_D) {
	//c->eax = 

	//c->edi = c->ebx;

	//c->edi = 0x10 + ((c->ref_arg_n(3) & 0xfffffff0) << 2); // -0x40;
	c->edx = c->ref_arg_n(3) & 0x0f;
	c->edi = 0x0;

	c->return_address = 0x004F96E0;
	return NO_EXEC_DEFAULT;
}
*/

/*
_LHF_(hero_level_smallhook_E) {

	c->Push(10);
	c->Push(0);
	c->Push(0);
	c->Push(0);

	c->Push(0x0);

	c->return_address = 0x004F97DB;
	return NO_EXEC_DEFAULT;
}
*/

/*
_LHF_(hero_level_smallhook_G) {
	//H3BaseDlg* dialog = (H3BaseDlg*)*(int*)(c->ebx + 0x38);
	//int focus = dialog->focusedItemId;
	int frame = 16 + (skill << 4) + level;
	//if (focus >= 200 && focus <= 207) frame = left_hero_SS[focus - 200 + 1];
	//if (focus >= 208 && focus <= 215) frame = left_hero_SS[focus - 208 + 1];
	*(int*)(c->esp + 0xc) = frame + _Frame_Shift_;

	return EXEC_DEFAULT;
}
*/


short hero_level_multiple_level_up_last_level;
//int level_UP_esi; int level_UP_edi;
HookContext hero_level_multiple_level_up_prepare_context;
_LHF_(hero_level_multiple_level_up_prepare) {

	*(unsigned char*)(c->esi + c->ebx + 0x476) = c->eax & 0xff;
	hero_level_multiple_level_up_prepare_context = *c;
	
	c->return_address = 0x004DAB6E;
	return NO_EXEC_DEFAULT;
}

HookContext hero_level_multiple_level_up_restoration_context;
int Level_UP_counter = 0;
extern int max_level_UP_Counter;
_LHF_(hero_level_multiple_level_up) {
	/*
	if (!Level_UP_counter) {
		level_UP_edi = c->edi;
		level_UP_esi = c->esi;
	}
	else {
		c->edi = level_UP_edi;
		c->esi = level_UP_esi;
	}
	*/

	//--(*(short*)(c->ebx + 0x55));
	//--(*(int*)(c->ebp - 0x10));
	if (!Level_UP_counter) {
		hero_level_multiple_level_up_restoration_context = *c;
		//hero_level_multiple_level_up_last_level = *(int*)(c->ebp - 0x10);
	}
	else {
		//h3::H3Hero* hero = (h3::H3Hero*) current_hero;
		//CALL_1(int,__thiscall, 0x0050C7B0,214013 * hero->level + 156823 * hero->levelSeed + 154079 + Level_UP_counter * 144);

		*c = hero_level_multiple_level_up_restoration_context;
		// *(int*)(c->ebp - 0x10) = hero_level_multiple_level_up_last_level;
	}

	(++Level_UP_counter) %= max_level_UP_Counter;



	if (Level_UP_counter) {
		///c->edx = c->ecx;
		//c->return_address = 0x004DAAD8;// 0x004DAAD5;


		//c->edi = level_UP_edi;
		//c->esi = level_UP_esi;

		// c->edi = c->ebx;
		
		*c = hero_level_multiple_level_up_prepare_context;

		c->return_address = 0x004DAB6E;
		return NO_EXEC_DEFAULT;
	}
	else {

		// c->edi = level_UP_edi;
		// c->esi = level_UP_esi;

		// (((HERO*)c->edi)->ExpLevel) -= max_level_UP_Counter;
		// ++(((HERO*)0x0067DCEC)->ExpLevel) -= max_level_UP_Counter;

		// *c = hero_level_multiple_level_up_restoration_context;

		/*
		c->edx = *(int*)  (c->ebp - 0x10);
		c->ecx = *(short*)(c->ebx + 0x55);

		c->eax = c->ecx;
		*/


		// hero_level_multiple_level_up_last_level++;
		/*
		if (c->ecx > c->edx) {
			//++ (*(short*)(c->ebx + 0x55));
			++((*(int*)(c->ebp - 0x10)));

			*c = hero_level_multiple_level_up_prepare_context;

			c->return_address = 0x004DAB6E;
			return NO_EXEC_DEFAULT;
		}
		*/

		//c->eax &= 0xffffff00;
		//c->eax += c->ecx & 0xff;
		//c->eax = c->ecx;
		int tmp = *(int*)(c->ebp - 0x10);//c->eax & 0xffff;
		*c = hero_level_multiple_level_up_restoration_context;
		c->eax = tmp + 1;// (c->ebp - 0x10);

		//c->edx = *(int*)(c->ebp - 0x10) + 1;
		++(*(int*)(c->ebp - 0x10));
		c->return_address = 0x004DAF10;  //0x004DAF18;// 0x004DAF10;
		return NO_EXEC_DEFAULT;
	}
	return EXEC_DEFAULT;
}

/*
_LHF_(hero_level_multiple_level_up_1) {
	
	return EXEC_DEFAULT;
}
_LHF_(hero_level_multiple_level_up_2) {

	return EXEC_DEFAULT;
}
_LHF_(hero_level_multiple_level_up_3) {

	return EXEC_DEFAULT;
}
*/

_LHF_(hero_level_multiple_level_up_simple) {
	if (max_level_UP_Counter == 1) return EXEC_DEFAULT;

	(++Level_UP_counter) %= max_level_UP_Counter;
	if (Level_UP_counter) {
		--(*(int*)(c->ebx + 0x55));
		--(*(unsigned char*)(c->ebx + 0x0476 + *(int*)(c->ebp-0x14)));
	}
	return EXEC_DEFAULT;
}



_LHF_(Hook_004DB143) {
	HERO* H = (HERO*)(char*) c->edi;
	c->BL() = get_hero_SS(H, c->eax);
	c->return_address = 0x004DB14A;
	return NO_EXEC_DEFAULT;
}


_LHF_(Hook_004DB198) {
	HERO* H = (HERO*)(char*)c->edi;
	c->DL() = get_hero_SS(H, c->eax);
	c->return_address = 0x004DB19F;
	return NO_EXEC_DEFAULT;
}

/*
char buf_004f9328[512] = " Test Level UP 004f9328 ";
_LHF_(Hook_004f9328) {

	//c->eax = (int)buf_004f9328;

	strcpy((char*)0x00697428, buf_004f9328);

	c->return_address = 0x004F9356;
	return NO_EXEC_DEFAULT;
}
*/

/*
_LHF_(Hook_004F9026) {

	c->return_address = 0x004F9418;
	return NO_EXEC_DEFAULT;
}
*/

/*
char buf_004f92d0[1024] = " Test Level UP 004f92d0 ";
_LHF_(Hook_0x4f92d0) {
	c->eax = sprintf((char*)0x00697428, buf_004f92d0);

	c->return_address = 0x004F9356;
	return NO_EXEC_DEFAULT;

}
*/

_LHF_(Hook_004DADB5) {
	HERO* H = (HERO*)c->ebx;
	c->edx = get_hero_SS(H, c->esi);
	c->return_address = 0x004DADBD;
	return NO_EXEC_DEFAULT;
}


_LHF_(Hook_004DADD3) {
	HERO* H = (HERO*)c->ebx;
	c->edx = get_hero_SS(H, c->edi);
	c->return_address = 0x004DADDB;
	return NO_EXEC_DEFAULT;
}




_LHF_(Hook_004DAE3E) {
	HERO* H = (HERO*)c->ebx;
	c->eax = get_hero_SS(H, c->esi);
	c->return_address = 0x004DAE46;
	return NO_EXEC_DEFAULT;
}


_LHF_(Hook_004DAE28) {
	HERO* H = (HERO*)c->ebx;
	c->ecx = get_hero_SS(H, c->edi);
	c->return_address = 0x004DAE30;
	return NO_EXEC_DEFAULT;
}



_LHF_(Hook_004DAD22) {
	HERO* H = (HERO*)c->ebx;
	c->eax = get_hero_SS(H, c->edi);
	c->return_address = 0x004DAD2A;
	return NO_EXEC_DEFAULT;
}

char level_up_text[512] = "Your hero may learn new Secondary Skill";
_LHF_(Hook_level_up_text) {
	c->ecx = (int) level_up_text;
	return EXEC_DEFAULT;
}

/*
int add_hero_button(int hero, int but, int x, int y, int ID) {
	int RET = 0;
	_asm {
		push 0x2
		push 0x0
		push 0x0
		push 0x1
		push 0x0
		push but // 0x660240
		push ID
		push 0x1E
		push 0x40
		push y // 0x19D
		push x // 0x128
		mov ecx, hero
		mov eax, 0x455BD0
		call eax
		mov RET, eax
	}
	return RET;
}

int z_new(int size) {
	int ret = 0; int arg = size;
	_asm {
		mov eax, arg
		push eax
		mov eax, 0x00617492
		call eax
		add esp, 4
		mov ret, eax
	}
	return ret;
}

_LHF_(CancelButton) {
	int EAX = c->eax;
	int ECX = c->ecx;

	
	// int RET = add_hero_button(EAX, 0x660240, 0x128,0x19D, 0x7802 );
	// // z_new(0x68);
	// RET = add_hero_button(z_new(0x68), 0x00660234, 0x20, 0x19D, 0x7800);
	// // add_hero_button(z_new(0x68), 0x00660234, 0x20, 0x19D, 0x7800);
	

	// add_hero_button(z_new(0x68), 0x00660234, 0x20, 0x19D, 0x7800);
	int RET = add_hero_button(EAX, 0x660240, 0x128, 0x19D, 0x7802);

	c->eax = RET;
	// *(int*)(c->ebp + 0x0C) = RET;
	c->return_address = 0x004F98D7; // 0x004F98E3;
	return NO_EXEC_DEFAULT;
}
*/

int SSkill1 = 0; int SSkill2 = 0;
int SSkill3_type = 0; int SSkill3_frame = 0;
int SSkill4_type = 0; int SSkill4_frame = 0;
bool first_action = false;
bool auto_lvlup = false;

_LHF_(Hook_Get_current_hero) {
	// at 0x004F9068
	SSkill1 = *(int*)(c->ebp + 0x10);
	SSkill2 = *(int*)(c->ebp + 0x14);
	SSkill3_type = SSkill4_type = -1;
	current_hero = (HERO*) c->edi;
	return EXEC_DEFAULT;
}

void __declspec(naked) slot3_preview() {
	__asm {
		push 0x0
		push 0xFFFFFFFF
		push 0x0
		mov edi, SSkill3_frame
		push 0x004FA04B
		ret
	}
}

void __declspec(naked) slot4_preview() {
	__asm {
		push 0x0
		push 0xFFFFFFFF
		push 0x0
		mov edi, SSkill4_frame
		push 0x004FA04B
		ret
	}
}

int get_new_skill(int slot, int min, int max) {
	int ret = -1; int tries = -1; again:
	
	ret = FASTCALL_4(int, 0x004DAF70, current_hero, min, max, -1);

	if (ret + 1 == SSkill1 >> 4 || ret + 1 == SSkill2 >> 4) {
		if (++tries < 64)
		{
			goto again;
		}
		else return -1;
	}
	if (slot > 3 && ret == SSkill3_type) {
		if (++tries < 64)
		{
			goto again;
		}
		else return -1;
	}
	return ret;
}

int off_004FA240[8] = {
	0x004FA03D, 0x004FA080, 0x004FA03D, 0x004FA080,
	int(slot3_preview), int(slot3_preview),
	int(slot4_preview), int(slot4_preview)
};

_LHF_(Hook_add_More_buttons) { // at 0x004F92D0
	// SSkill3 = SSkill4 = -1;
	// if(SSkill1<0 || SSkill2 < 0) return EXEC_DEFAULT;
	int color = ((h3::H3BasePalette565*)*(int*)0x006AAD18)->color[45];

	// h3::H3Dlg* DLG = (h3::H3Dlg*)*(int*)(c->ebp - 0x14);
	// h3::H3Dlg* DLG = (h3::H3Dlg*)*(int*)(c->edi);
	h3::H3Dlg* DLG = (h3::H3Dlg*)*(int*) 0x00699684;
	char SSkill_text[512];
	// DLG->firstItem;

	// HERO* hero = (HERO*) *(int*)(c->ebp+8);	
	// HERO* hero = (HERO*) *(int*)(c->edx);

	DLG->AddItem(h3::H3DlgDefButton::Create(24,413,7777,(LPCSTR)"icancel.def",0,1,0,0), false);


	if (SSkill1 < 0 || SSkill2 < 0) return EXEC_DEFAULT;

	SSkill3_type = get_new_skill(3,1, max_SS_level) ; // CALL_4(int, __fastcall, 0x004DAF70, current_hero, 1,15, -1);
	if (SSkill3_type >=0) {
		int sskil3_level = get_hero_SS(current_hero, SSkill3_type) + 1;
		int sskil3_frame = 16 + sskil3_level + (SSkill3_type << 4);
		SSkill3_frame = sskil3_frame;

		sprintf_s(SSkill_text, "%s\n%s", Skill_Levels[sskil3_level], newSkillsDescriptions[SSkill3_type].name);
		DLG->AddItem(h3::H3DlgFrame::Create(30, 325, 47, 46, 2024, color), false);
		DLG->AddItem(h3::H3DlgDef::Create(32, 326, 44, 44, 2014, (LPCSTR)(const char*)0x006601D0, sskil3_frame), false);
		DLG->AddItem(h3::H3DlgText::Create(10, 375, 87, 40, SSkill_text, (LPCSTR)(const char*)0x0065F2F8, 1, 2015), false);

		SSkill4_type = get_new_skill(4, 0, max_SS_level); // CALL_4(int, __fastcall, 0x004DAF70, current_hero, 0, 15, -1);
		if (SSkill4_type >= 0) {
			int sskil4_level = get_hero_SS(current_hero, SSkill4_type) + 1;
			int sskil4_frame = 16 + sskil4_level + (SSkill4_type << 4);
			SSkill4_frame = sskil4_frame;

			sprintf_s(SSkill_text, "%s\n%s", Skill_Levels[sskil4_level], newSkillsDescriptions[SSkill4_type].name);
			DLG->AddItem(h3::H3DlgFrame::Create(310, 325, 47, 46, 2026, color), false);
			DLG->AddItem(h3::H3DlgDef::Create(312, 326, 44, 44, 2016, (LPCSTR)(const char*)0x006601D0, sskil4_frame), false);
			DLG->AddItem(h3::H3DlgText::Create(290, 375, 87, 40, SSkill_text, (LPCSTR)(const char*)0x0065F2F8, 1, 2017), false);
		}
	}
	first_action = true;

	return EXEC_DEFAULT;
}

_LHF_(hook_reset_dialogue) {
	// at 0x004F9E4F
	h3::H3Dlg* DLG = (h3::H3Dlg*)*(int*)0x00699684;

	if (first_action) {
		if (SSkill3_type >= 0) DLG->GetH3DlgItem(2024)->SendCommand(6, 4);
		if (SSkill4_type >= 0) DLG->GetH3DlgItem(2026)->SendCommand(6, 4);
		first_action = false;
	}

	if (auto_lvlup) {
		Zecondary_Skills->WriteDword((long)DLG + 0x68, 2010);
		// c->eax = 30722;
		int* timeClick = (int*)0x006977D4;
		*timeClick = CDECL_0(int, 0x004F8970) + 25;

		c->return_address = 0x004F9D79;
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}

_LHF_(Hook_Handle_More_Buttons) { // at 0x004FA0DD
	//	if (SSkill1 < 0 || SSkill2 < 0) return EXEC_DEFAULT;
	h3::H3Dlg* DLG = (h3::H3Dlg*)*(int*)0x00699684;

	/*
	if (first_action) {
		DLG->GetH3DlgItem(2024)->SendCommand(6, 4);
		DLG->GetH3DlgItem(2026)->SendCommand(6, 4);
		first_action = false;
	}
	*/

	if (c->eax == 7777 || auto_lvlup) {
		auto_lvlup = true;
		Zecondary_Skills->WriteDword((long)DLG + 0x68, 2010);
		
		

		c->eax = 30722;
		c->return_address = 0x004FA195;
		return NO_EXEC_DEFAULT;
	}

	if (SSkill1 < 0 || SSkill2 < 0) return EXEC_DEFAULT;

	if (c->eax == 2016 || c->eax == 2017 || c->eax == 2026)
	{
		Zecondary_Skills->WriteDword(0x0067FA84, 2016);

		DLG->GetH3DlgItem(2012)->SendCommand(6, 4);
		DLG->GetH3DlgItem(2013)->SendCommand(6, 4);
		DLG->GetH3DlgItem(2024)->SendCommand(6, 4);
		DLG->GetH3DlgItem(2026)->SendCommand(5, 4);
		// DLG->GetH3DlgItem(2015)->SendCommand(4, 4);
		// Zecondary_Skills->WriteDword(0x0067FA84,);

		c->Push(0x7802);
		Zecondary_Skills->WriteDword((long)DLG + 0x68, 2016);
		c->return_address = 0x004FA20A; // 0x004FA1F8;

		return NO_EXEC_DEFAULT;
	}
	if(c->eax == 2014 || c->eax == 2015 || c->eax == 2024)
	{
		Zecondary_Skills->WriteDword(0x0067FA84, 2014);

		DLG->GetH3DlgItem(2012)->SendCommand(6, 4);
		DLG->GetH3DlgItem(2013)->SendCommand(6, 4);
		if(SSkill4_type >= 0) DLG->GetH3DlgItem(2026)->SendCommand(6, 4);
		DLG->GetH3DlgItem(2024)->SendCommand(5, 4);
		// DLG->GetH3DlgItem(2015)->SendCommand(4, 4);
		// Zecondary_Skills->WriteDword(0x0067FA84,);

		c->Push(0x7802);
		Zecondary_Skills->WriteDword((long)DLG + 0x68, 2014);
		c->return_address = 0x004FA20A; // 0x004FA1F8;

		return NO_EXEC_DEFAULT;
	}
	if (c->eax == 2010 || c->eax == 2012 || c->eax == 2005)
	{
		Zecondary_Skills->WriteDword(0x0067FA84, 2010);

		if (SSkill3_type >= 0) DLG->GetH3DlgItem(2024)->SendCommand(6, 4);
		DLG->GetH3DlgItem(2013)->SendCommand(6, 4);
		if (SSkill4_type >= 0) DLG->GetH3DlgItem(2026)->SendCommand(6, 4);
		DLG->GetH3DlgItem(2012)->SendCommand(5, 4);
		// DLG->GetH3DlgItem(2015)->SendCommand(4, 4);

		c->Push(0x7802);
		Zecondary_Skills->WriteDword((long)DLG + 0x68, 2010);
		c->return_address = 0x004FA20A; // 0x004FA1F8;
		return NO_EXEC_DEFAULT;
	}
	if (c->eax == 2011 || c->eax == 2013 || c->eax == 2006)
	{

		Zecondary_Skills->WriteDword(0x0067FA84, 2011);

		if (SSkill4_type >= 0) DLG->GetH3DlgItem(2026)->SendCommand(6, 4);
		if (SSkill3_type >= 0) DLG->GetH3DlgItem(2024)->SendCommand(6, 4);
		DLG->GetH3DlgItem(2012)->SendCommand(6, 4);
		DLG->GetH3DlgItem(2013)->SendCommand(5, 4);
		// DLG->GetH3DlgItem(2015)->SendCommand(4, 4);

		c->Push(0x7802); 
		Zecondary_Skills->WriteDword((long)DLG + 0x68, 2011);
		c->return_address = 0x004FA20A; // 0x004FA1F8;
		return NO_EXEC_DEFAULT;
	}
	// DLG->SendCommandToItem(4,2014,7);
	return EXEC_DEFAULT;
}

_LHF_(Hook_Dlg_Result) { // at 0x004DAEE9
	int result = c->eax;
	if (result == 2014) {
		c->edi = SSkill3_type; // SS number
		c->return_address = 0x004DAEFC;
		return NO_EXEC_DEFAULT;
	}
	if (result == 2016) {
		c->edi = SSkill4_type; // SS number
		c->return_address = 0x004DAEFC;
		return NO_EXEC_DEFAULT;
	}
	return EXEC_DEFAULT;
}

_LHF_(hook_more_random) {
	c->ecx += Level_UP_counter * 144;
	return EXEC_DEFAULT;
}

int __stdcall HeroLvlUp_Proc_Hook(HiHook* h, int cmdpo) {

	int ret = STDCALL_1(int, h->GetDefaultFunc(),cmdpo);

	if (auto_lvlup) return 1;

	return ret;
}

_LHF_(hook_004DA9AE) {
	auto_lvlup = false;
	return EXEC_DEFAULT;
}

_LHF_(hook_004DAC91) {
	if (auto_lvlup) {
		c->return_address = 0x004DACA0;
		return NO_EXEC_DEFAULT;
	}
	else return EXEC_DEFAULT;
}

extern double qword_levelup;
void installHeroLevelUp(void) {


	for (int i = 0; level_up_bytes[i].address; i++)
		Zecondary_Skills->WriteByte(level_up_bytes[i].address, level_up_bytes[i].new_byte);



	Zecondary_Skills->WriteJmp(0x004DAE30, (int)hero_level_smallpatch_1);
	Zecondary_Skills->WriteJmp(0x004DAD2D, (int)hero_level_smallpatch_2);

	
	//////// some of them are bad
	/////Zecondary_Skills->WriteJmp(0x004F930E, (int)hero_level_smallpatch_4);
	//Zecondary_Skills->WriteLoHook(0x4f92e1, hero_level_smallhook_4);
	//Zecondary_Skills->WriteLoHook(0x4f92e1, hero_level_smallhook_4b);
	Zecondary_Skills->WriteLoHook(0x4F95B2, hero_level_smallhook_5);
	Zecondary_Skills->WriteLoHook(0x4F962C, hero_level_smallhook_6);
	Zecondary_Skills->WriteLoHook(0x4F96ED, hero_level_smallhook_7);
	

	Zecondary_Skills->WriteLoHook(0x4FA04B, hero_level_smallhook_8);
	Zecondary_Skills->WriteLoHook(0x4FA094, hero_level_smallhook_9);
	Zecondary_Skills->WriteLoHook(0x5B034C, hero_level_smallhook_A);
	Zecondary_Skills->WriteLoHook(0x5B085A, hero_level_smallhook_B);

	//Zecondary_Skills->WriteLoHook(0x4F96C3, hero_level_smallhook_D);
	//Zecondary_Skills->WriteLoHook(0x4F97D2, hero_level_smallhook_E);

	// Zecondary_Skills->WriteLoHook(0x004DAB67, hero_level_multiple_level_up_prepare);
	// Zecondary_Skills->WriteLoHook(0x004DAF06, hero_level_multiple_level_up);

	Zecondary_Skills->WriteLoHook(0x004DAF06, hero_level_multiple_level_up_simple);

	// Zecondary_Skills->WriteLoHook(0x004DACC6 + 5, hero_level_multiple_level_up_1);
	// Zecondary_Skills->WriteLoHook(0x004DAD3D + 5, hero_level_multiple_level_up_2);
	// Zecondary_Skills->WriteLoHook(0x004DAE51 + 5, hero_level_multiple_level_up_3);

	Zecondary_Skills->WriteLoHook(0x004DB143, Hook_004DB143);
	Zecondary_Skills->WriteLoHook(0x004DB198, Hook_004DB198);

	// Zecondary_Skills->WriteLoHook(0x004f9328, Hook_004f9328);
	// Zecondary_Skills->WriteDword(0x004F937D + 1, buf_004f9328);

	// Zecondary_Skills->WriteLoHook(0x004F9026, Hook_004F9026);

	// Zecondary_Skills->WriteLoHook(0x4f92d0, Hook_0x4f92d0);

	// ---------------------------------------------------------------------

	Zecondary_Skills->WriteLoHook(0x004DADB5, Hook_004DADB5);
	Zecondary_Skills->WriteLoHook(0x004DADD3, Hook_004DADD3);

	Zecondary_Skills->WriteLoHook(0x004DAE28, Hook_004DAE28);
	Zecondary_Skills->WriteLoHook(0x004DAE3E, Hook_004DAE3E);

	Zecondary_Skills->WriteLoHook(0x004DAD22, Hook_004DAD22);

	// ---------------------------------------------------------------------

	/*
	Zecondary_Skills->WriteDword(0x004DA9F4 + 2, (int)&qword_levelup);
	Zecondary_Skills->WriteDword(0x004DAA18 + 2, (int)&qword_levelup);
	Zecondary_Skills->WriteDword(0x004E37F8 + 2, (int)&qword_levelup);
	Zecondary_Skills->WriteDword(0x004E381C + 2, (int)&qword_levelup);
	*/
	*(double*)0x0063AC20 = qword_levelup;
	
	// ---------------------------------------------------------------------

	Zecondary_Skills->WriteLoHook(0x004F96C3, Hook_level_up_text);
	Zecondary_Skills->WriteLoHook(0x004F92E1, Hook_level_up_text);

	// Zecondary_Skills->WriteLoHook(0x004F98AE, CancelButton);
	
	Zecondary_Skills->WriteLoHook(0x004F92D0, Hook_add_More_buttons);
	// Zecondary_Skills->WriteHiHook(0x004F9E10, SPLICE_, EXTENDED_, STDCALL_, HeroLvlUp_Proc_Hook);
	Zecondary_Skills->WriteLoHook(0x004FA0DD, Hook_Handle_More_Buttons);
	Zecondary_Skills->WriteLoHook(0x004DAEE9, Hook_Dlg_Result);
	Zecondary_Skills->WriteLoHook(0x004F9D31, hook_reset_dialogue);
	Zecondary_Skills->WriteLoHook(0x004F9068, Hook_Get_current_hero);
	Zecondary_Skills->WriteLoHook(0x004DAAA0, hook_more_random);
	Zecondary_Skills->WriteDword(0x004FA036+3, (int) &off_004FA240[0]);
	Zecondary_Skills->WriteByte(0x004FA02D+2,7);
	Zecondary_Skills->WriteByte(0x004F8F2E + 1, 32);

	Zecondary_Skills->WriteLoHook(0x004DA9AE, hook_004DA9AE);
	Zecondary_Skills->WriteLoHook(0x004DAC91, hook_004DAC91);

}