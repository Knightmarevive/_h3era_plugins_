// // #include "patcher_x86.hpp"
// #include "patcher_x86.hpp"
#include "MyTypes.h"
#include "heroes.h"
#include "HeroSpec.h"

char  __stdcall get_hero_SS(HERO* H, int skilllnum);

extern SSP_Table SecondarySkillsPowerInternal;
extern PatcherInstance* Zecondary_Skills;

char max_proficiency_per_spell[256] = {};
char last_spell = -1;
// int  SS_Aether = 30;
constexpr int schools_count = 7;
int all_schools_flag = 0x7F;
int	 SS_New[schools_count-4] = { 30, 31, 32 };

// 004E52F8
_LHF_(last_spell_1) {
	last_spell = c->edi;
	return EXEC_DEFAULT;
}

// 004E54B8
_LHF_(last_spell_2) {
	last_spell = c->esi;
	return EXEC_DEFAULT;
}


_LHF_(last_spell_3) {
	last_spell = c->edx;
	return EXEC_DEFAULT;
}

/*
_LHF_(last_spell_4) {
	last_spell = c->edx / 17;
	return EXEC_DEFAULT;
}
*/

/*
int __stdcall last_spell_7(HiHook* h, HERO* Hp, int arg2, int GroundModifier, int MagicScool) {
	last_spell = arg2 / 17;
	return CALL_4(int, __fastcall, h->GetDefaultFunc(), Hp, arg2, GroundModifier, MagicScool);
}
*/

bool is_hero_spec_of_spell(long HeroID, long Spell) {
	// auto& spec = P_HeroSpecialty( ((HERO*)Hero)->Number );
	auto& spec = ZP_HeroSpecialty(HeroID);
	if (spec.type == spec.ST_spell
		&& spec.bonusID == Spell)
		return true;

	return false;
}

extern int  max_magic_mastery;

int __stdcall magic_004E53B8(HiHook* H, HERO* hero, int elements) {
	int ret = 0; int tmp; int max = 7;

	if (elements & 1) {
		tmp = SecondarySkillsPowerInternal[15][hero->SSkill[15]].i;
		tmp += THISCALL_2(long, 0x4D9460, hero, 79);
		if (tmp > ret) ret = tmp;
	}

	if (elements & 2) {
		tmp = SecondarySkillsPowerInternal[14][hero->SSkill[14]].i;
		tmp += THISCALL_2(long,  0x4D9460, hero, 81);
		if (tmp > ret) ret = tmp;
	}

	if (elements & 8) {
		tmp = SecondarySkillsPowerInternal[17][hero->SSkill[17]].i;
		tmp += THISCALL_2(long,  0x4D9460, hero, 80);
		if (tmp > ret) ret = tmp;
	}

	if (elements & 4) {
		tmp = SecondarySkillsPowerInternal[16][hero->SSkill[16]].i;
		tmp += THISCALL_2(long,  0x4D9460, hero, 82);
		if (tmp > ret) ret = tmp;
	}

	for(int S=4;S<7;++S)
	if (elements & (1<<S)) {
		tmp = SecondarySkillsPowerInternal[SS_New[S - 4]][get_hero_SS(hero, SS_New[S - 4])].i;
		tmp += THISCALL_2(long,  0x4D9460, hero, 93);
		if (tmp > ret) ret = tmp;
	}

	if (last_spell >= 0) ret += is_hero_spec_of_spell(hero->Number, last_spell);

	if (ret > max) ret = max;

	return ret;
}
/*
_LHF_(magic_1) {
	c->eax = 0;	int ret = 0; int tmp = 0; int max = max_magic_mastery; // were max=3
	unsigned char elements = (c->ebx & 0xff); 
	// if(last_spell>=0) max = max_proficiency_per_spell[last_spell];

	if (elements & 1) {
		tmp = SecondarySkillsPowerInternal[15][*(unsigned char*)(c->ecx + 0xd8)].i;
		tmp += CALL_2(long,__thiscall, 0x4D9460, c->ecx, 79);
		if (tmp > ret) ret = tmp;
	}

	if (elements & 2) {
		tmp = SecondarySkillsPowerInternal[14][*(unsigned char*)(c->ecx + 0xd7)].i;
		tmp += THISCALL_2(long,  0x4D9460, c->ecx, 81);
		if (tmp > ret) ret = tmp;
	}

	if (elements & 8) {
		tmp = SecondarySkillsPowerInternal[17][*(unsigned char*)(c->ecx + 0xda)].i;
		tmp += THISCALL_2(long,  0x4D9460, c->ecx, 80);
		if (tmp > ret) ret = tmp;
	}

	if (elements & 4) {
		tmp = SecondarySkillsPowerInternal[16][*(unsigned char*)(c->ecx + 0xd9)].i;
		tmp += THISCALL_2(long,  0x4D9460, c->ecx, 82);
		if (tmp > ret) ret = tmp;
	}

	if (elements & 16) {
		tmp = SecondarySkillsPowerInternal[SS_Aether][get_hero_SS((HERO*)c->ecx, SS_Aether)].i;
		tmp += THISCALL_2(long,  0x4D9460, c->ecx, 93);
		if (tmp > ret) ret = tmp;
	}
	if (last_spell >= 0) ret += is_hero_spec_of_spell(*(long*)(c->ecx + 0x1A),last_spell);

	if (ret > max) ret = max;

	// last_spell = -1;
	c->eax = c->ecx  = c->edx= ret;
	c->return_address = 0x004E5402;
	return NO_EXEC_DEFAULT;
}
*/

extern "C" __declspec(dllexport) int get_max_magic_proficiency(int spell) {
	return max_magic_mastery;
	return max_proficiency_per_spell[spell];
}
extern "C" __declspec(dllexport) void set_max_magic_proficiency(int spell, int value) {
	max_proficiency_per_spell[spell] = value;
}

extern "C" __declspec(dllexport) int get_real_magic_proficiency(HERO* h, int spell, int elements, int terrain) {

	// return CALL_4(int,__fastcall,0x004E5370,h,spell*17,terrain,elements);
	// disabled... why ?

	int ret = 0; int tmp = 0; int max = 7; // 5;

	// if (terrain == 1) return max;

	// terrain = 0; // debug

	if (elements & 1) {
		tmp = SecondarySkillsPowerInternal[15][*(unsigned char*)( ((char*)(h)) + 0xd8)].i;
		tmp += THISCALL_2(long,  0x4D9460, h, 79);
		if ((terrain == 9) || (terrain == 1)) tmp = max;
		if (tmp > ret) ret = tmp;
	}

	if (elements & 2) {
		tmp = SecondarySkillsPowerInternal[14][*(unsigned char*)(((char*)(h)) + 0xd7)].i;
		tmp += THISCALL_2(long,  0x4D9460, h, 81);
		if ((terrain == 7) || (terrain == 1)) tmp = max;
		if (tmp > ret) ret = tmp;
	}

	if (elements & 8) {
		tmp = SecondarySkillsPowerInternal[17][*(unsigned char*)(((char*)(h)) + 0xda)].i;
		tmp += THISCALL_2(long,  0x4D9460, h, 80);
		if ((terrain == 8) || (terrain == 1)) tmp = max;
		if (tmp > ret) ret = tmp;
	}

	if (elements & 4) {
		tmp = SecondarySkillsPowerInternal[16][*(unsigned char*)(((char*)(h)) + 0xd9)].i;
		tmp += THISCALL_2(long,  0x4D9460, h, 82);
		if ((terrain == 6) || (terrain == 1)) tmp = max;
		if (tmp > ret) ret = tmp;
	}

	for (int S = 4; S < schools_count; ++S)
	if (elements & (1<<S)) {
		tmp = SecondarySkillsPowerInternal[SS_New[S-4]][get_hero_SS(h, SS_New[S - 4])].i;
		tmp += THISCALL_2(long,  0x4D9460, h, 93);
		if (terrain == 1) tmp = max;
		if (tmp > ret) ret = tmp;
	}

	if (last_spell >= 0) ret += is_hero_spec_of_spell(h->Number, last_spell);

	if (ret > max) ret = max;

	return ret;
}

int __stdcall magic_004E5370(HiHook *H, HERO* hero, int spell, int elements, int terrain) {
	last_spell = spell / 17; // if (terrain <= 0 || terrain >= 0x7F) terrain = 0x7f;
	return get_real_magic_proficiency(hero, spell, elements, terrain);
}

int __stdcall magic_004E5430(HiHook* H, HERO* hero, signed int elements) {
	int ret = 0, school_lvl = -1;
	char* he = (char*)hero;

	if ((elements & 1) && SecondarySkillsPowerInternal[15][*(he + 216)].i > school_lvl)
	{
		school_lvl = SecondarySkillsPowerInternal[15] [*(he + 216)].i;
		ret = 1;
	}
	if ((elements & 2) && SecondarySkillsPowerInternal[14] [*(he + 215)].i > school_lvl)
	{
		school_lvl = SecondarySkillsPowerInternal[14] [*(he + 215)].i;
		ret = 2;
	}
	if ((elements & 8) && SecondarySkillsPowerInternal[17][*(he + 218)].i > school_lvl)
	{
		school_lvl = SecondarySkillsPowerInternal[17][*(he + 218)].i;
		ret = 8;
	}
	if ((elements & 4) && SecondarySkillsPowerInternal[16][*(he + 217)].i > school_lvl)
	{
		school_lvl = SecondarySkillsPowerInternal[16] [*(he + 217)].i;
		ret = 4;
	}
	for (int S = 4; S < schools_count; ++S)
	if ((elements & (1 << S)) && SecondarySkillsPowerInternal[SS_New[S - 4]][get_hero_SS(hero, SS_New[S - 4])].i > school_lvl) {
		school_lvl = SecondarySkillsPowerInternal[SS_New[S - 4]][get_hero_SS(hero, SS_New[S - 4])].i;
		ret = (1 << S);
	}

	return ret;
}
/*
_LHF_(magic_004E5378) {
	c->eax = get_real_magic_proficiency((HERO*)c->ecx, 0, *(int*)(c->ebp+0x8) , *(int*)(c->ebp + 0xC));
	c->return_address = 0x004E5402;  return NO_EXEC_DEFAULT;
}
*/

_LHF_(wisdom_427070) {
	c->eax = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->edi + 0xd0)].i;
	c->return_address = 0x0042707A;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_42BED4) {
	c->eax = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->esi + 0xd0)].i;
	c->return_address = 0x0042BEDE;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_42BFB5) {
	c->ebx = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->ebx + 0xd0)].i;
	c->return_address = 0x0042BFBF;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_469C10) {
	c->edx = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->ecx + 0xd0)].i -2;
	c->return_address = 0x00469C17;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_4A0203) {
	c->eax = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->edi + 0xd0)].i;
	c->return_address = 0x004A020D;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_4A2657) {
	c->eax = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->edi + 0xd0)].i;
	c->return_address = 0x004A2661;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_4A267C) {
	c->eax = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->ebx + 0xd0)].i;
	c->return_address = 0x004A2686;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_4A408A) {
	c->eax = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->edi + 0xd0)].i -2;
	c->return_address = 0x004A4091;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_4A4A68) {
	c->edx = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->ebx + 0xd0)].i - 2;
	c->return_address = 0x004A4A6F;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_4A5459) {
	c->edx = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->edi + 0xd0)].i - 2;
	c->return_address = 0x004A5460;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_52B763) {
	c->ecx = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->esi + 0xd0)].i;
	c->return_address = 0x0052B76D;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_527F92) {
	c->ecx = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->esi + 0xd0)].i - 2;
	c->return_address = 0x00527F99;
	return NO_EXEC_DEFAULT;

}
_LHF_(wisdom_529DBE) {
	c->ecx = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->esi + 0xd0)].i - 2;
	c->return_address = 0x00529DC5;
	return NO_EXEC_DEFAULT;

}

_LHF_(wisdom_52B7D2) {
	c->ecx = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->esi + 0xd0)].i - 2;
	c->return_address = 0x0052B7D9;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_574220) {
	c->eax = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->edi + 0xd0)].i - 2;
	c->return_address = 0x00574227;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_5BE576) {
	c->ecx = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->ebx + 0xd0)].i;
	c->return_address = 0x005BE580;
	return NO_EXEC_DEFAULT;
}

_LHF_(wisdom_5BE5DA) {
	c->edx = SecondarySkillsPowerInternal[7][*(unsigned char*)(c->ecx + 0xd0)].i;
	c->return_address = 0x005BE5E4;
	return NO_EXEC_DEFAULT;
}

_LHF_(scholar_4A284F) {

	c->edx += 304 + 1024;

	c->Push(c->eax);
	c->Push(c->ecx);

	c->return_address = 0x004A2854;
	return NO_EXEC_DEFAULT;
}

_LHF_(scholar_4A25F7) {

	c->esi = SecondarySkillsPowerInternal[18][c->esi].i;

	return EXEC_DEFAULT;
}

char* SpLev_00641DA4[schools_count] = {
	(char*)"SPLEVA.def",
	(char*)"SPLEVF.def",
	(char*)"SPLEVW.def",
	(char*)"SPLEVE.def",
	(char*)"SPLEVX.def",
	(char*)"SPLEVY.def",
	(char*)"SPLEVZ.def",
};

void patch_magic_early(void) {
	if (!*max_proficiency_per_spell) memset(max_proficiency_per_spell, 3, sizeof(max_proficiency_per_spell));
}

_LHF_(magic_0059CD87) {
	if (c->ecx >= 15) {
		// c->ecx = 127;
		c->ecx = c->eax;
	}

	c->return_address = 0x0059CD8E;
	return NO_EXEC_DEFAULT;
}

void patch_magic(void) {
	Zecondary_Skills->WriteByte(0x0059CEBD,0x8D);
	Zecondary_Skills->WriteDword(0x0059D86E, all_schools_flag);
	Zecondary_Skills->WriteDword(0x0059D5AA, all_schools_flag);
	Zecondary_Skills->WriteDword(0x0059D5E8, all_schools_flag);
	Zecondary_Skills->WriteDword(0x0059D653, all_schools_flag);
	Zecondary_Skills->WriteDword(0x0059D008 + 3, (int) SpLev_00641DA4);
	//Zecondary_Skills->WriteWord(0x0059CD8A, 0x9090);
	Zecondary_Skills->WriteByte(0x0059CFF8 + 2, schools_count);
	Zecondary_Skills->WriteLoHook(0x0059CD87, magic_0059CD87);
	//Zecondary_Skills->WriteLoHook(0x004E5378, magic_004E5378);
	Zecondary_Skills->WriteHiHook(0x004E5370, SPLICE_, EXTENDED_, FASTCALL_, magic_004E5370);
	Zecondary_Skills->WriteHiHook(0x004E53B8, SPLICE_, EXTENDED_, THISCALL_, magic_004E53B8);
	Zecondary_Skills->WriteHiHook(0x004E5430, SPLICE_, EXTENDED_, THISCALL_, magic_004E5430);
	// Zecondary_Skills->WriteLoHook(0x004E53B8, magic_1);
	// Zecondary_Skills->WriteLoHook(0x004E52F8, last_spell_1);
	// Zecondary_Skills->WriteLoHook(0x004E54B8, last_spell_2);
	// Zecondary_Skills->WriteLoHook(0x004E57E2, last_spell_3);
	// Zecondary_Skills->WriteLoHook(0x004E5373, last_spell_4);
	// Zecondary_Skills->WriteHiHook(0x004E5370, SPLICE_, EXTENDED_, FASTCALL_, last_spell_7);
	
	Zecondary_Skills->WriteDword(0x004E53AE+1,7);

	Zecondary_Skills->WriteLoHook(0x00427070, wisdom_427070);
	Zecondary_Skills->WriteLoHook(0x0042BED4, wisdom_42BED4);
	Zecondary_Skills->WriteLoHook(0x0042BFB5, wisdom_42BFB5);
	Zecondary_Skills->WriteLoHook(0x00469C10, wisdom_469C10);
	Zecondary_Skills->WriteLoHook(0x004A0203, wisdom_4A0203);
	Zecondary_Skills->WriteLoHook(0x004A2657, wisdom_4A2657);
	Zecondary_Skills->WriteLoHook(0x004A267C, wisdom_4A267C);
	Zecondary_Skills->WriteLoHook(0x004A408A, wisdom_4A408A);
	Zecondary_Skills->WriteLoHook(0x004A4A68, wisdom_4A4A68);
	Zecondary_Skills->WriteLoHook(0x004A5459, wisdom_4A5459);
	Zecondary_Skills->WriteLoHook(0x0052B763, wisdom_52B763);
	Zecondary_Skills->WriteLoHook(0x0052B7D2, wisdom_52B7D2);
	Zecondary_Skills->WriteLoHook(0x00574220, wisdom_574220);
	Zecondary_Skills->WriteLoHook(0x005BE576, wisdom_5BE576);
	Zecondary_Skills->WriteLoHook(0x005BE5DA, wisdom_5BE5DA);

	Zecondary_Skills->WriteLoHook(0x00527F92, wisdom_527F92);
	Zecondary_Skills->WriteLoHook(0x00529DBE, wisdom_529DBE);

	Zecondary_Skills->WriteLoHook(0x004A284F, scholar_4A284F);
	Zecondary_Skills->WriteByte(0x004A265E + 2, 0x00);
	Zecondary_Skills->WriteByte(0x004A2683 + 2, 0x00);
	Zecondary_Skills->WriteLoHook(0x004A25F7, scholar_4A25F7);

}