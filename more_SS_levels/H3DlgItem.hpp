#pragma once
// #include "patcher_x86.hpp"

// #define _H3API_

// typedef void H3BaseDlg;


typedef unsigned long h3func;
typedef INT8 h3unk;
typedef INT8 BOOL8;
typedef INT32 H3Dlg_proc;
typedef void H3LoadedPCX16;
typedef char H3Vector_Blank[16];



struct H3SecondarySkill
{
	INT type;
	INT level; // 0 ~ 3
};

struct H3SecondarySkillInfo
{
	LPCSTR name;
	LPCSTR description[3];
};


struct H3SecondarySkillInfoNew
{
	LPCSTR name;
	LPCSTR description[15];
};

struct H3DlgItem;
struct H3Dlg;

// typedef INT32(__stdcall* H3Dlg_proc)(H3Dlg*, H3Msg*);
// typedef INT32(__fastcall* H3DlgButton_proc)(H3MsgCustom*);
// typedef VOID(__fastcall* H3DlgScrollbar_proc)(INT32, H3Dlg*);

struct H3DlgVTable
{
public: // copied from vTable at 0x63A6A8
	h3func     destroyDlg;
	h3func     showDlg;
	h3func     hideDlg;
	h3func     callProcessAction;
	h3func     _nullsub;
	h3func     redrawDlg;
	h3func     runDlg;
	h3func     initDlgItems;
	h3func     activateDlg;
	H3Dlg_proc dlgProc;
	h3func     mouseMove;
	h3func     rightClick;
	h3func     clickRet;
	h3func     _nullsub3;
	h3func     closeDlg;

	// _H3API_ H3DlgVTable();
};

struct H3BaseDlg
{
	// protected:
		// * +0
	H3DlgVTable* vtable;
	// * +4
	INT32 zOrder;
	// * +8
	H3Dlg* nextDialog;
	// * +C
	H3Dlg* lastDialog;
	// * +10
	INT32 flags;
	// * +14
	INT32 state;
	// * +18
	INT32 xDlg;
	// * +1C
	INT32 yDlg;
	// * +20
	INT32 widthDlg;
	// * +24
	INT32 heightDlg;
	// * +28
	H3DlgItem* lastItem;
	// * +2C
	H3DlgItem* firstItem;
	// * +30
	H3Vector_Blank/* H3Vector<H3DlgItem*> */dlgItems;
	// * +40
	INT32 focusedItemId;
	// * +44
	H3LoadedPCX16* pcx16;
	// * +48
	INT32 deactivatesCount;
	h3unk _f_4C[28]; // normal dialog size is 0x68 total	
};


struct H3DlgItemVTable // 0x63BA24
{
	h3func deref;
	h3func init;
	h3func processCommand; // used to redraw
	h3func _null1;
	h3func draw;
	h3func getHeight;
	h3func getWidth;
	h3func showHint;
	h3func drawShadow;
	h3func setEnabled;
	h3func getFocus;
	h3func loseFocus;
	h3func _null5;
	h3func setText;
	////////////////////////////////////////////////
	// not all H3DlgItems have the following:
	////////////////////////////////////////////////
	h3func setFocus; // H3DlgEdit has these // ScrollbarText has 3
	h3func processKey;
	h3func isDisallowedKey; // +40 disallowed key for H3DlgEdit, e.g. save game field 0x57D2F0
	h3func loadPcx16;
	h3func pcx16FromBuffer;
};

struct H3DlgItem // size 0x30
{
	enum eVTables
	{
		VT_DLGITEM = 0x63BA24,
		VT_DLGFRAME = 0x63BA5C,
		VT_DLGPCX = 0x63BA94,
		VT_DLGPCX16 = 0x63BACC,
		VT_DLGDEFBUTTON = 0x63BB54,
		VT_DLGCAPTIONBUTTON = 0x63BB88,
		VT_DLGCUSTOMBUTTOM = 0x63BBBC,

		VT_EDITTEXT2 = 0x63EBF4,

		VT_DLGDEF = 0x63EC48,
		VT_DLGSCROLLBAR = 0x641D60,

		//VT_DEFScrollsmt     = 0x642CD8,

		VT_DLGSCROLLTEXT = 0x642D1C,
		VT_DLGEDIT = 0x642D50,
		VT_DLGTEXT = 0x642DC0,
		VT_DLGTEXTPCX = 0x642DF8,
	};

	// protected:
		// * +0
	H3DlgItemVTable* vTable;
	// * +4
	H3BaseDlg* parent;
	// * +8
	H3DlgItem* previousDlgItem;
	// * +C
	H3DlgItem* nextDlgItem;
	// * +10
	UINT16 id;
	// * +12
	UINT16 zOrder;
	// * +14
	UINT16 flags;
	// * +16
	UINT16 state;
	// * +18
	INT16 xPos;
	// * +1A
	INT16 yPos;
	// * +1C
	UINT16 widthItem;
	// * +1E
	UINT16 heightItem;
	// * +20
	LPCSTR hint;
	// * +24
	LPCSTR rightClickHint;
	// * +28
	BOOL8 hintsAreAllocated;
	h3unk _f_29[3];
	// * +2C
	INT32 deactivatesCount;

	// * private function, use SetText() of respective items
	// * if it's not there, it's not available!
};

struct H3DlgDef : public H3DlgItem // size 0x48
{ 
	// * +30
	struct H3LoadedDEF* loadedDef;
	// * +34
	INT32 defFrame;
	// * +38
	INT32 defFrameOnClick;
	// * +3C
	INT32 mirror;
	h3unk _f_40[4];
	// * +44
	INT16 closeDialog;
	h3unk _f_46[2];
};