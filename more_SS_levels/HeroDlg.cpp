#include "pch.h"
// #include "patcher_x86.hpp"
#include "MyTypes.h"
#include <string>

// #include "era.h"
#include "../__include__/H3API/single_header/H3API.hpp"
char txt_ps[4][16] = { "Att", "Def", "Pow", "Know" };

extern PatcherInstance* Zecondary_Skills;

_LHF_(FinishHeroDlg_Hook) {
	// h3::H3Dlg* dlgHero = (h3::H3Dlg*) c->edi;// *(int*)(c->ebp-0x14);
	h3::H3Dlg* dlgHero = (h3::H3Dlg*) c->edi;// *(int*)(c->ebp-0x14);

	int dx = 48; int beg = 16; int beg2 = (dx - 44) / 2 + beg;
	for (int i = 0; i < 4; i++) {
		dlgHero->GetH3DlgItem(50 + i)->SetX(beg + i * dx);

		dlgHero->GetH3DlgItem(46 + i)->SetX(beg2 + i * dx);
		dlgHero->GetH3DlgItem(46 + i)->SetWidth(dx);

		// ((h3::H3DlgText*)dlgHero->GetH3DlgItem(103 + i))->SetText(txt_ps[i]);
		dlgHero->GetH3DlgItem(103 + i)->SetX(beg + i * dx);
		dlgHero->GetH3DlgItem(103 + i)->SetWidth(dx);
		
		// dlgHero->Redraw();
	}

	// return EXEC_DEFAULT;

	dlgHero->GetH3DlgItem(116)->SetX(beg2+dx*4);
	dlgHero->GetH3DlgItem(116)->SetY(111);

	dlgHero->GetH3DlgItem(117)->SetX(beg2 + dx * 5);
	dlgHero->GetH3DlgItem(117)->SetY(111);
	
	//dlgHero->AddItem(dlgHero->CreateText(beg + dx * 4,158, dx,18,"0","smalfont.fnt",1,1116),false);
	//dlgHero->AddItem(dlgHero->CreateText(beg + dx * 5, 158, dx, 18, "0", "smalfont.fnt", 1, 1117), false);

	dlgHero->CreateText(beg + dx * 4,158, dx,18,"unk","smalfont.fnt",1,1116);
	dlgHero->CreateText(beg + dx * 5, 158, dx, 18, "unk", "smalfont.fnt", 1, 1117);


	return EXEC_DEFAULT;
}

int BuildPicturePSkillLuckMoral_HiHook(HiHook* h, h3::H3Hero* hero) {
	//_asm{pusha}
	int ret = THISCALL_1(int, h->GetDefaultFunc(), hero);
	//_asm {popa}

	return ret;
}
_LHF_(BuildPicturePSkillLuckMoral_hook) {
	h3::H3Hero* hero = (h3::H3Hero*) *(int*) 0x00698B70;
	h3::H3Dlg* dlgHero = (h3::H3Dlg*)*(int*) 0x00698AC8;

	char moraletext[16] = "M"; sprintf_s(moraletext,"%d", (int) hero->GetMoraleBonus(0,0,1) );
	char lucktext[16] = "L";   sprintf_s(lucktext, "%d",  (int) hero->GetLuckBonus(0, 0, 1) );

	if(dlgHero->GetH3DlgItem(1116))		((h3::H3DlgText*)dlgHero->GetH3DlgItem(1116))->SetText(moraletext);
	if (dlgHero->GetH3DlgItem(1117))	((h3::H3DlgText*)dlgHero->GetH3DlgItem(1117))->SetText(lucktext);

	return EXEC_DEFAULT;
}

void HeroDlg_Patch() {
	Zecondary_Skills->WriteLoHook(0x004E15C2, FinishHeroDlg_Hook);
	Zecondary_Skills->WriteLoHook(0x004E194B, BuildPicturePSkillLuckMoral_hook);
	// Zecondary_Skills->WriteHiHook(0x004E1940,THISCALL_, BuildPicturePSkillLuckMoral_HiHook);
	// Zecondary_Skills->WriteDword();
}