#include "MyTypes.h"
// #include "patcher_x86.hpp"

#pragma warning(disable : 4996)
#include "../__include__/H3API/single_header/H3API.hpp"

#include "era_lite.h"

extern PatcherInstance* Zecondary_Skills;

SSP_Table SecondarySkillsPowerInternal = {};
SSP_Table SecondarySkillsPowerVariables[SS_variable_types_count] = {};
// int Max_SSkill_Level[32] = {};
extern int max_SS_level;

unsigned char ballistic_chances[16][8] = {
	{5, 10 , 25 , 50 , 1, 10 , 60 , 30 },

	{7, 15 , 30 , 60 , 1, 0 , 50 , 50 },
	{7, 15 , 30 , 60 , 1, 0 , 50 , 50 },

	{7, 15 , 30 , 60 , 2, 0 , 50 , 50 },
	{7, 15 , 30 , 60 , 2, 0 , 50 , 50 },
	{7, 15 , 30 , 60 , 2, 0 , 50 , 50 },

	{10, 20 , 40 , 75 , 2, 0 , 0 , 100 },
	{10, 20 , 40 , 75 , 2, 0 , 0 , 100 },
	{10, 20 , 40 , 75 , 2, 0 , 0 , 100 },
	{10, 20 , 40 , 75 , 2, 0 , 0 , 100 },

	{15, 27 , 50 , 80 , 3, 0 , 0 , 100 },
	{15, 27 , 50 , 80 , 3, 0 , 0 , 100 },
	{15, 27 , 50 , 80 , 3, 0 , 0 , 100 },
	{15, 27 , 50 , 80 , 3, 0 , 0 , 100 },

	{20, 32 , 50 , 100 , 5, 0 , 0 , 100 }
};

#include "heroes.h"
int   (*check_SSI_bonus)(HERO*, long) = nullptr;
float (*check_SSF_bonus)(HERO*, long) = nullptr;
int   (*check_SSI_variable)(HERO*, long, int) = nullptr;
float (*check_SSF_variable)(HERO*, long, int) = nullptr;

void debugme() {
	constexpr int a = sizeof(SecondarySkillsPowerVariables);
	constexpr int b = sizeof(SecondarySkillsPowerVariables[0]);
	constexpr int c = sizeof(SecondarySkillsPowerVariables[0][0]);
	constexpr int d = sizeof(SecondarySkillsPowerVariables[0][0][0]);
}

extern "C" __declspec(dllexport) int get_Hero_SSI_with_bonuses(HERO* hero, long skill) {
	switch (skill) {
	case 8:
		return THISCALL_1(int, 0x004E41B0, hero);
	case 3:
		return THISCALL_1(int, 0x004E42E0, hero);
	case 13:
		return THISCALL_1(int, 0x004E4600, hero);
	default:
		return SecondarySkillsPowerInternal[skill][get_hero_SS(hero->Number, skill)].i
			+ (check_SSI_bonus ? check_SSI_bonus(hero, skill) : 0);
	}
}

extern "C" __declspec(dllexport) float get_Hero_SSF_with_bonuses(HERO * hero, long skill) {
	switch (skill) {
	case 1:
		return THISCALL_1(long double,   0x004E43D0, hero);
	case 22:
		return THISCALL_1(long double,   0x004E4520, hero);
	case 23:
		return THISCALL_1(long double,   0x004E4580, hero);
	case 11:
		return THISCALL_1(long double,   0x004E4690, hero);
	case 4:
		return THISCALL_1(long double,   0x004E47F0, hero);
	case 26:
		return THISCALL_1(long double,   0x004E4950, hero);
	case 21:
		return THISCALL_1(long double,   0x004E4AB0, hero);
	case 27:
		return THISCALL_1(long double,   0x004E4B90, hero);
	case 12:
		return THISCALL_2(long double,   0x004E4B90, hero, 0);


	default:
		return SecondarySkillsPowerInternal[skill][get_hero_SS(hero->Number, skill)].f
			+ (check_SSF_bonus ? check_SSF_bonus(hero, skill) : 0.0);
	}
}

extern "C" __declspec(dllexport) int get_Hero_zSSI_with_bonuses(HERO * hero, long skill, int type = 0) {
	return SecondarySkillsPowerVariables[type][skill][get_hero_SS(hero->Number, skill)].i
		+ (check_SSI_variable ? check_SSI_variable(hero, skill, type) : 0);
}

extern "C" __declspec(dllexport) float get_Hero_zSSF_with_bonuses(HERO * hero, long skill, int type = 0) {
	return SecondarySkillsPowerVariables[type][skill][get_hero_SS(hero->Number, skill)].f
		+ (check_SSF_variable ? check_SSF_variable(hero, skill, type) : 0.0);
}

_LHF_(hook_00746EE3) {
	int choice = c->edx;
	int Num = *(int*)(c->ebp - 0x10);
	switch(choice) {
	case 'Q':
		if (Num != 1) {
			CDECL_3(void, 0x00712333,1,7400,"\"HE:Q\"-wrong syntax");
			c->return_address = 0x007496D9; return NO_EXEC_DEFAULT;
		}
		else {
			int skill = *(int*)(c->ebp - 0x54); HERO* hero = (HERO*)*(int*)(c->ebp - 0x380);
	
			Era::e[2] = get_Hero_SSF_with_bonuses(hero, skill);
			Era::v[2] = get_Hero_SSI_with_bonuses(hero, skill);

			Era::e[3] = get_Hero_zSSF_with_bonuses(hero, skill);
			Era::v[3] = get_Hero_zSSI_with_bonuses(hero, skill);

			c->return_address = 0x00746F00; return NO_EXEC_DEFAULT;
		}
		

	default: return EXEC_DEFAULT;
	}
}

#include<sstream>
void read_ballistic_chances(char* file_name) {
	if (auto fdesc = fopen(file_name, "r"))
	{


		//----------
		fseek(fdesc, 0, SEEK_END);
		int fdesc_size = ftell(fdesc);
		rewind(fdesc);
		//----------
		char* buf = (char*)malloc(fdesc_size + 1);
		memset(buf, 0, fdesc_size + 1);
		fread(buf, 1, fdesc_size, fdesc);
		//buf[fdesc_size]=0;

		std::stringstream str(buf); int tmp;

		for (int i = 0; i < 16; ++i) for (int j = 0; j < 8; ++j) {
			str >> tmp; if (tmp < 0 || tmp>100) {
				char buf[512]; sprintf(buf, "Wrong Ballistic Config \n\n Position %i %i \n Value %i \n Game may not work properly", i, j, tmp);
				MessageBoxA(nullptr, buf, "more_SS_levels.era", MB_ICONWARNING);
			}
			else ballistic_chances[i][j] = tmp;
		}

		fclose(fdesc); free(buf);
	}
}

replace_dword SSP_relocation[] = {
	{0x00679C84 + 0, (unsigned int) &(ballistic_chances[0][0]) },

	{0x004E39F3 + 3, (unsigned int)SecondarySkillsPowerInternal[9]},  // 0x63E998
	{0x004E3C48 + 3, (unsigned int)SecondarySkillsPowerInternal[6]},  // 0x63E9A8
	{0x004E3F56 + 3, (unsigned int)SecondarySkillsPowerInternal[12]}, // 0x63E9B8
	{0x004E41C5 + 3, (unsigned int)SecondarySkillsPowerInternal[8]},  // 0x63E9C8
	{0x004E42F5 + 3, (unsigned int)SecondarySkillsPowerInternal[3]},  // 0x63E9D8

	{0x004E43E4 + 3, (unsigned int)SecondarySkillsPowerInternal[1]},  // 0x63E9E8
	{0x004E4531 + 3, (unsigned int)SecondarySkillsPowerInternal[22]},
	{0x004E4591 + 3, (unsigned int)SecondarySkillsPowerInternal[23]},
	{0x004E461B + 3, (unsigned int)SecondarySkillsPowerInternal[13]},
	{0x004E46A4 + 3, (unsigned int)SecondarySkillsPowerInternal[11]},
	{0x004E4804 + 3, (unsigned int)SecondarySkillsPowerInternal[4]},
	{0x004E4964 + 3, (unsigned int)SecondarySkillsPowerInternal[26]},
	{0x004E4AC1 + 3, (unsigned int)SecondarySkillsPowerInternal[21]},
	{0x004E4EE6 + 3, (unsigned int)SecondarySkillsPowerInternal[2]},
	{0x004E5B29 + 3, (unsigned int)SecondarySkillsPowerInternal[25]},

	{0x004D8C1D + 3, (unsigned int)SecondarySkillsPowerInternal[24]},
	{0x004D9146 + 3, (unsigned int)SecondarySkillsPowerInternal[24]},
	{0x004DA55E + 3, (unsigned int)SecondarySkillsPowerInternal[24]},
	{0x004DDA94 + 3, (unsigned int)SecondarySkillsPowerInternal[24]},
	{0x004E20B4 + 3, (unsigned int)SecondarySkillsPowerInternal[24]},
	{0x004E4B31 + 3, (unsigned int)SecondarySkillsPowerInternal[24]},

	{0x004E4BA1 + 3, (unsigned int)SecondarySkillsPowerInternal[27]},

	//{0x004E62AC + 3, (int)SecondarySkillsPowerInternal[??]}, // 0x63EAA8
	//{0x???????? + 3, (int)SecondarySkillsPowerInternal[??]},
	//{0x???????? + 3, (int)SecondarySkillsPowerInternal[??]},
	//{0x???????? + 3, (int)SecondarySkillsPowerInternal[??]},

	//{0x004DE9C0 + 2, (int)SecondarySkillsPowerInternal[??]}, // 0x63EAE8
	//{0x004E17E1 + 2, (int)SecondarySkillsPowerInternal[??]}, // 0x63EAE8

	//{0x???????? + 3, (int)SecondarySkillsPowerInternal[??]}, // 0x63EAF8
	//{0x???????? + 3, (int)SecondarySkillsPowerInternal[??]}, // 0x63EB08



	{0x0,0x0}
};



extern "C" __declspec(dllexport) void set_SS_table_i(int skill, int level, int value) {
	SecondarySkillsPowerInternal[skill][level].i = value;
}

extern "C" __declspec(dllexport) void set_SS_table_f(int skill, int level, float value) {
	SecondarySkillsPowerInternal[skill][level].f = value;
}

extern "C" __declspec(dllexport) int  get_SS_table_i(int skill, int level) {
	return SecondarySkillsPowerInternal[skill][level].i;
}

extern "C" __declspec(dllexport) float get_SS_table_f(int skill, int level) {
	return SecondarySkillsPowerInternal[skill][level].f;
}

extern "C" __declspec(dllexport) int get_SS_table_f_(int skill, int level) {
	return SecondarySkillsPowerInternal[skill][level].f *10000;
}



extern "C" __declspec(dllexport) void set_SS_table_zi(int skill, int level, int value, int type = 0) {
	SecondarySkillsPowerVariables[type][skill][level].i = value;
}

extern "C" __declspec(dllexport) void set_SS_table_zf(int skill, int level, float value, int type = 0) {
	SecondarySkillsPowerVariables[type][skill][level].f = value;
}


extern "C" __declspec(dllexport) int  get_SS_table_zi(int skill, int level, int type = 0) {
	return SecondarySkillsPowerVariables[type][skill][level].i;
}

extern "C" __declspec(dllexport) float get_SS_table_zf(int skill, int level, int type = 0) {
	return SecondarySkillsPowerVariables[type][skill][level].f;
}

extern "C" __declspec(dllexport) int get_SS_table_zf_(int skill, int level, int type = 0) {
	return SecondarySkillsPowerVariables[type][skill][level].f * 10000;
}



_LHF_(Check_SSkill_Level_1) {

	c->edi = *(int*)(c->ebp + 8);
	c->ecx = c->ebx & 0xff;
	int limit = max_SS_level; // Max_SSkill_Level[c->eax];
	//------------------------------------------

	/*
	if (c->ecx < limit && c->ecx>1) {
		c->edi = limit;
		*(int*)(c->ebp + 8) = limit;
	}
	*/

	c->edi = limit;
	// *(int*)(c->ebp + 8) = limit;

	c->return_address = 0x004DB0B1;
	return NO_EXEC_DEFAULT;
}

_LHF_(Check_SSkill_Level_2) {

	c->ecx = c->edx & 0xff;
	int limit = max_SS_level; // Max_SSkill_Level[c->eax];
	//------------------------------------------

	c->edi = limit;
	// *(int*)(c->ebp + 8) = limit;

	//------------------------------------------

	if (c->ecx >= c->edi) {

		c->return_address = 0x004DB131;
		return NO_EXEC_DEFAULT;
	}

	c->return_address = 0x004DB10A;
	return NO_EXEC_DEFAULT;
}

_LHF_(Check_SSkill_Level_3) {

	c->edx = *(int*)(c->ebp+8);
	c->ecx = c->ebx & 0xff; 
	int limit = max_SS_level; // Max_SSkill_Level[c->eax];
	//------------------------------------------

	c->edx = limit;
	// *(int*)(c->ebp + 8) = limit;
	/*
	if (c->edx >= 3 && (c->ecx) >= 3); {
		c->edx = limit;
	}
	*/
	c->return_address = 0x004DB152;
	return NO_EXEC_DEFAULT;
}

extern int Diplo_Divider;
extern int Diplo_Shift;

/*
// 0x004A7484
_LHF_(diplo_divide) {
	c->ebx = c->ebx & 0xffff0000 + ( *(char*)(c->edi+0xcd) ) / 5;

	c->return_address = 0x004A748C;
	return NO_EXEC_DEFAULT;
}
*/
// 0x004E47FF
_LHF_(diplo_divide_4E47FF) {
	c->eax = (c->eax & 0xffffff00) | (((c->eax & 0xff) + Diplo_Shift) / Diplo_Divider);
	if (c->eax & 0x80) c->eax &= 0xffffff00;
	return EXEC_DEFAULT;
}

_LHF_(diplo_004A7484) {
	char& skill_level = *(char*)(c->edi + 0xCD);
	c->ebx = 0xffff & ((skill_level + Diplo_Shift)/ Diplo_Divider);
	c->return_address = 0x004A748C;
	return NO_EXEC_DEFAULT;
}

/*
// 0x4E6050
_LHF_(diplo_divide_4E6050) {
	//c->eax = c->eax & 0xffffff00 + (c->eax & 0xff) / 5;
	return EXEC_DEFAULT;
}
// 0x0x4E6080
_LHF_(diplo_divide_4E6080) {
	//c->eax = c->eax & 0xffffff00 + (c->eax & 0xff) / 5;
	return EXEC_DEFAULT;
}
*/
// 0x04171C4
_LHF_(diplo_4171BD) {
	char& skill_level = *(char*)(c->esi + 0xCD);
	//c->eax = c->eax & 0xffffff00 + (c->eax & 0xff) / 5;
	c->edx = (skill_level + Diplo_Shift) / Diplo_Divider;
	// if (c->edx < 0) c->edx = 0;
	c->return_address = 0x04171C4;
	return NO_EXEC_DEFAULT;
}

_LHF_(Tactics_0046280C_hook) {
	c->edi = SecondarySkillsPowerInternal[19][*(char*)(c->eax + 0xDC)].i;
	c->return_address = 0x00462813;
	return NO_EXEC_DEFAULT;
}
_LHF_(Tactics_00462821_hook) {
	c->ecx = SecondarySkillsPowerInternal[19][*(char*)(c->eax + 0xDC)].i;
	c->return_address = 0x00462828;
	return NO_EXEC_DEFAULT;
}

#include<cstdio>
void ParseInt(char* buf, char* name, int* result);
void ParseFloat(char* buf, char* name, float* result);

extern "C" __declspec(dllexport) void ApplySkillPower(int skillID, char* config_str) {
	char name[512];
	for (int i = 0; i <= 15; ++i) {
		sprintf(name, "Level_%i_Power_Internal_int=", i);
		ParseInt(config_str, name, &SecondarySkillsPowerInternal[skillID][i].i);
		sprintf(name, "Level_%i_Power_Internal_float=", i);
		ParseFloat(config_str, name, &SecondarySkillsPowerInternal[skillID][i].f);

		/// compatibility
		sprintf(name, "Level_%i_Power_Custom_int=", i);
		ParseInt(config_str, name, &SecondarySkillsPowerVariables[0][skillID][i].i);
		sprintf(name, "Level_%i_Power_Custom_float=", i);
		ParseFloat(config_str, name, &SecondarySkillsPowerVariables[0][skillID][i].f);

		for (int z = 0; z < SS_variable_types_count; ++z) {
			sprintf(name, "Level_%i_Power_Custom_%i_int=", i, z);
			ParseInt(config_str, name, &SecondarySkillsPowerVariables[z][skillID][i].i);
			sprintf(name, "Level_%i_Power_Custom_%i_float=", i, z);
			ParseFloat(config_str, name, &SecondarySkillsPowerVariables[z][skillID][i].f);
		}
	}
}

extern int  SS_Multiple_Build;
extern "C" __declspec(dllexport) int get_town_bonus_build_count(h3::H3Town *town) {
	if (SS_Multiple_Build < 0) return 0; int ret = 0;

	static h3::H3Hero* (__cdecl *GetAzylumHero)(h3::H3Town*) = nullptr ;
	if (!GetAzylumHero) {
		HMODULE mod = GetModuleHandle(L"new_towns.era");
		if (mod) GetAzylumHero = 
			(h3::H3Hero * (__cdecl *)(h3::H3Town*))
			GetProcAddress(mod, "get_azylum_hero");
	}
	
	if (GetAzylumHero) {
		auto hero_azylum = GetAzylumHero(town);
		if (hero_azylum) ret += get_Hero_zSSI_with_bonuses((HERO*)hero_azylum, SS_Multiple_Build, 0);
	}

	auto hero_up = town->GetGarrisonHero();
	if(hero_up)	ret += get_Hero_zSSI_with_bonuses((HERO*) hero_up , SS_Multiple_Build, 0);



	return ret;
}

float necro_amplifier	= 0.025;
float necro_soul_prison	= 0.050;

// #define SSL(z,a) Max_SSkill_Level[z]=a

#define SSI set_SS_table_i
#define SSF set_SS_table_f

#define SSIA(z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p) SSI(z,0,a);SSI(z,1,b);SSI(z,2,c); SSI(z,3,d); SSI(z,4,e); SSI(z,5,f);SSI(z,6,g); SSI(z,7,h); SSI(z, 8, i); SSI(z, 9, j); SSI(z, 10, k); SSI(z, 11, l); SSI(z, 12, m); SSI(z, 13, n); SSI(z, 14, o); SSI(z, 15, p);
#define SSFA(z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p) SSF(z,0,a);SSF(z,1,b);SSF(z,2,c); SSF(z,3,d); SSF(z,4,e); SSF(z,5,f);SSF(z,6,g); SSF(z,7,h); SSF(z, 8, i); SSF(z, 9, j); SSF(z, 10, k); SSF(z, 11, l); SSF(z, 12, m); SSF(z, 13, n); SSF(z, 14, o); SSF(z, 15, p);


#define SSIZ set_SS_table_zi
#define SSFZ set_SS_table_zf

#define SSIAZ(z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p) SSIZ(z,0,a);SSIZ(z,1,b);SSIZ(z,2,c); SSIZ(z,3,d); SSIZ(z,4,e); SSIZ(z,5,f);SSIZ(z,6,g); SSIZ(z,7,h); SSIZ(z, 8, i); SSIZ(z, 9, j); SSIZ(z, 10, k); SSIZ(z, 11, l); SSIZ(z, 12, m); SSIZ(z, 13, n); SSIZ(z, 14, o); SSIZ(z, 15, p);
#define SSFAZ(z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p) SSFZ(z,0,a);SSFZ(z,1,b);SSFZ(z,2,c); SSFZ(z,3,d); SSFZ(z,4,e); SSFZ(z,5,f);SSFZ(z,6,g); SSFZ(z,7,h); SSFZ(z, 8, i); SSFZ(z, 9, j); SSFZ(z, 10, k); SSFZ(z, 11, l); SSFZ(z, 12, m); SSFZ(z, 13, n); SSFZ(z, 14, o); SSFZ(z, 15, p);

void fill_SSP_Tables(void) {

	SSIA(9, 0, 1, 1,  2,  2,  3,  3,  4,   4,  5,  5,  6,  6,  7,  7,  7); // LUCK
	SSIA(6, 0, 1, 1,  1,  2,  2,  2,  3,   3,  3,  4,  4,  4,  5,  5,  5); // Leadership
	SSIA(8, 2, 4, 6,  8, 10, 12, 14, 16,  18, 20, 22, 24, 26, 28, 30, 32); // Mysticism
	SSIA(3, 5, 7, 9, 11, 13, 15, 17, 19,  21, 23, 25, 27, 29, 31, 33, 35); // Scouting

	SSIA(13, 25, 125, 250, 500, 1000, 2000, 4000, 7000, 13000, 24000, 33000, 50000, 75000, 80000, 88000, 100000); // Estates

	SSFA(12, 0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.10, 1.20, 1.30, 1.40, 1.50); // Necro
	SSFA( 1, 0.00, 0.10, 0.25, 0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00, 3.25, 3.50); // archery
	SSFA(22, 0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.10, 1.20, 1.30, 1.40, 1.50); // ofence
	SSFA(23, 0.00, 0.04, 0.08, 0.12, 0.16, 0.20, 0.24, 0.28, 0.32, 0.36, 0.40, 0.44, 0.48, 0.52, 0.56, 0.60); // armorer

	SSFA(11, 0.0, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0); //Eagle Eye
	SSFA( 4, 0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0) // Diplomacy
	SSFA(26, 0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.10, 1.20, 1.30, 1.40, 1.50); // resistance
	SSFA(21, 0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.10, 1.20, 1.30, 1.40, 1.50); // Learning
	SSFA( 2, 0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.10, 1.20, 1.30, 1.40, 1.50); // Logistics
	SSFA(25, 0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.10, 1.20, 1.30, 1.40, 1.50); // Sorcery
	SSFA(24, 0.00, 0.25, 0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00, 3.25, 3.50, 3.75); // Inteligence

	// custom internals
	SSIA(14, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3); // Fire  Magic
	SSIA(15, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3); // Air   Magic
	SSIA(16, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3); // Water Magic
	SSIA(17, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3); // Earth Magic
	SSIA( 7, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 5); // Wisdom
	SSIA(18, 0, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 5); // Scholar
	SSIA( 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3); // Pathfinding
	SSIA( 5, 1500, 2250, 3000, 3750, 4500, 5250, 6000, 6750, 7500, 8250, 9000, 9750, 10500, 11250, 12000, 12750); // Navigation
	SSFA(27, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0); // first aid
	// // missing:
	// ballistic
	// artilery

	SSIA(19, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5); // TODO: Tactics

	for (int i = 0; SSP_relocation[i].address; i++)
		Zecondary_Skills->WriteDword(SSP_relocation[i].address, SSP_relocation[i].new_dword );

	// Zecondary_Skills->WriteLoHook(0x004DB0AB, Check_SSkill_Level_1);
	// Zecondary_Skills->WriteLoHook(0x004DB103, Check_SSkill_Level_2);
	// Zecondary_Skills->WriteLoHook(0x004DB14D, Check_SSkill_Level_3);

	// Zecondary_Skills->WriteLoHook(0x004A7484, diplo_divide);
	// Zecondary_Skills->WriteLoHook(0x004171C4, diplo_divide_4171C4);
	// Zecondary_Skills->WriteLoHook(0x004E47FF, diplo_divide_4E47FF);
	Zecondary_Skills->WriteLoHook(0x004A7484, diplo_004A7484);
	Zecondary_Skills->WriteLoHook(0x004171BD, diplo_4171BD);

	Zecondary_Skills->WriteLoHook(0x0046280C, Tactics_0046280C_hook);
	Zecondary_Skills->WriteLoHook(0x00462821, Tactics_00462821_hook);

	// for (int i = 0; i < 32; i++) Max_SSkill_Level[i] = 3;
	/*
	SSL(9, 15);  SSL(6, 15); SSL(8, 15); SSL(3, 15); SSL(13, 15);
	SSL(12, 15); SSL(22, 15); SSL(23, 15); SSL(11, 7); SSL(4, 5);
	SSL(26, 15); SSL(21, 15); SSL(2, 15); SSL(25, 15); SSL(24, 15);
	*/

	
	Zecondary_Skills->WriteByte(0x004DABAF + 1, max_SS_level);
	Zecondary_Skills->WriteByte(0x004DABCD + 1, max_SS_level);
	Zecondary_Skills->WriteByte(0x004DAC10 + 1, max_SS_level);
	// Zecondary_Skills->WriteByte(0x????? + 1, 0xf);

	Zecondary_Skills->WriteByte(0x004E258E + 2, max_SS_level);
	Zecondary_Skills->WriteByte(0x004E2593 + 2, max_SS_level);

	Zecondary_Skills->WriteDword(0x004E4100, (int) &necro_amplifier);
	Zecondary_Skills->WriteDword(0x004E411F, (int) &necro_soul_prison);
	
	HMODULE SS_Bonus_Agents = 0; SS_Bonus_Agents = GetModuleHandleA("SS_Bonus_Agents.dll");
	if (SS_Bonus_Agents) {
		check_SSI_bonus = (int   (*)(HERO*, long)) GetProcAddress(SS_Bonus_Agents, "check_SSI_bonus");
		check_SSF_bonus = (float (*)(HERO*, long)) GetProcAddress(SS_Bonus_Agents, "check_SSF_bonus");

		check_SSI_variable = (int   (*)(HERO*, long, int)) GetProcAddress(SS_Bonus_Agents, "check_SSI_variable");
		check_SSF_variable = (float (*)(HERO*, long, int)) GetProcAddress(SS_Bonus_Agents, "check_SSF_variable");
	}
}


extern "C" __declspec(dllexport) void fill_SSP_Tables_Knightmare_Kingdoms(void) {

	SSFAZ(9, 0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.10, 1.20, 1.30, 1.40, 1.50); // Life Magic
	SSIA(9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0); // LUCK/Life Magic

	SSIA(6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0); // Leadership/Inspiration
	SSIA(8, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32); // Mysticism
	SSIA(3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35); // Scouting

	SSIA(13, 0, 300, 500, 700, 900, 1200, 1500, 1800, 2100, 2500, 2900, 3300, 3700, 4200, 4700, 5200); // Estates
	SSIAZ(13, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15); // Estates resource

	SSFA(12, 0.00, 0.03, 0.06, 0.09, 0.12, 0.15, 0.18, 0.21, 0.24, 0.27, 0.30, 0.33, 0.36, 0.39, 0.42, 0.45); // Necro
	SSFAZ(12, 0.00, 0.03, 0.06, 0.09, 0.12, 0.15, 0.18, 0.21, 0.24, 0.27, 0.30, 0.33, 0.36, 0.39, 0.42, 0.45); // Dark

	SSFA(1 , 0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.10, 1.20, 1.30, 1.40, 1.50); // archery
	SSFA(22, 0.00, 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75); // ofence
	SSFA(23, 0.00, 0.04, 0.08, 0.12, 0.16, 0.20, 0.24, 0.28, 0.32, 0.36, 0.40, 0.44, 0.48, 0.52, 0.56, 0.60); // armorer

	SSFA(11, 0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0); // Eagle Eye/Scavenging
	SSFA(4,  0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.0, 1.0, 1.0, 1.0, 1.0) // Diplomacy/Nobility
	SSFA(26, 0.00, 0.04, 0.08, 0.12, 0.16, 0.20, 0.24, 0.28, 0.32, 0.36, 0.40, 0.44, 0.48, 0.52, 0.56, 0.60); // resistance
	SSFA(21, 0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.10, 1.20, 1.30, 1.40, 1.50); // Learning
	SSIAZ(21, 0, 300, 500, 700, 900, 1200, 1500, 1800, 2100, 2500, 2900, 3300, 3700, 4200, 4700, 5200); // Learning XP
	SSFA(2,  0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00, 1.10, 1.20, 1.30, 1.40, 1.50); // Logistics
	SSFA(25, 0.00, 0.03, 0.06, 0.09, 0.12, 0.15, 0.18, 0.21, 0.24, 0.27, 0.30, 0.33, 0.36, 0.39, 0.42, 0.45); // Sorcery
	SSFA(24, 0.00, 0.13, 0.26, 0.39, 0.52, 0.65, 0.78, 0.91, 1.04, 1.17, 1.30, 1.43, 1.56, 1.69, 1.82, 1.95); // Inteligence

	// custom internals
	SSIA(14, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5); // Fire  Magic
	SSIA(15, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5); // Air   Magic
	SSIA(16, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5); // Water Magic
	SSIA(17, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5); // Earth Magic
	SSIA(7,  0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5); // Wisdom
	SSIA(18, 0, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 5); // Scholar/Monasticism
	SSIA(0,  0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3); // Pathfinding
	SSIAZ(10, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5); // Engineering -> more builds per town
	SSIA(5, 1500, 2250, 3000, 3750, 4500, 5250, 6000, 6750, 7500, 8250, 9000, 9750, 10500, 11250, 12000, 12750); // Navigation
	SSFAZ(5, 0.00, 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75); // Astromancy
	SSFA(27, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0); // first aid/Machinery
	// // missing:
	// ballistic
	// artilery

	SSIA(19, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5); // TODO: Tactics


	SSIA(30, 0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5); // Magic 30
}