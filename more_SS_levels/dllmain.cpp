// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
// #include "patcher_x86.hpp"
#include "MyTypes.h"

#include "era.h"
//#include "H3DlgItem.hpp"
//#include "H3Manager.hpp"

using namespace Era;

#define PINSTANCE_MAIN "more_SS_levels"

Patcher* globalPatcher;
PatcherInstance* Zecondary_Skills;

void fill_SSP_Tables(void);

void show_HERO_SS_icons(void);
void show_HERO_SS_icons_original(void);

void hero_meet_original(void);
void hero_meet(void);

void  fill_show_meeting_hero_SS(void);

void hero_level_A_original(void);
void hero_level_A_patched(void);

void hero_level_B_original(void);
void hero_level_B_patched(void);
_LHF_(hero_level_B2_Hooked);

void SS_text_clean(void);
replace_byte  SS_text_bytes[];
replace_dword SS_text_dwords[];
extern int meet_hero_left;
extern int meet_hero_right;
extern int current_hero;

void hero_meet_recalc_frames(void);
void HERO_meet_DLG_change_frames(void);

// void fill_show_current_hero_SS(void);
void HERO_Screen_Preset(void);
void asm_HERO_Screen_Preset(void);
int __stdcall hook_HERO_Screen_Preset(LoHook* h, HookContext* c);
// int __stdcall hook_HERO_Screen_Refresh(LoHook* h, HookContext* c);
int __stdcall hook_HERO_zecsk82a(LoHook* h, HookContext* c);
int __stdcall hook_HERO_zecsk82b(LoHook* h, HookContext* c);

void calc_pre_msgbox(void);
void calc_in_msgbox(void);
void patch_4F1507(void);

using namespace h3;

extern H3BaseDlg* meeting_HERO_DLG;

_LHF_(hero_meet_recalc_frames);
void installHeroLevelUp(void);
void patch_magic(void);
void patch_magic_early(void);
void patch_movement(void);
void patch_machines(void);


_LHF_(Hook_004E1CC6);

int  __stdcall Hero_SSkillAdd_Hook(HiHook* h, HERO* me, int skill, char val);
int  __stdcall Hero_sub_004E24C0_hook(HiHook* h, HERO* me, int skill, char level);
int  __stdcall Hero_sub_004E2610_hook(HiHook* h, HERO* me, int a2);
char __stdcall Hero_sub_7643DE_Hook(HiHook* h);

bool master_config(char* file_name);
extern int SS_Limit_Per_Hero;
extern int current_SS_count;


void Show_HERO_SS_Icons_Install();
void WoG_Area_install();

void link_amethyst(void)
{
	//HMODULE Amethyst = LoadLibraryA("Mods\\Knightmare Kingdoms\\eraplugins\\amethyst2.3.dll");
	HMODULE Amethyst = 0;
	//DWORD flags = 0; // GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS;
	//::GetModuleHandleEx(flags, reinterpret_cast<LPCTSTR>("Mods\\Knightmare Kingdoms\\eraplugins\\amethyst2.3.dll"), &Amethyst);
	Amethyst = GetModuleHandleA("amethyst2_4.era");
	if (!Amethyst) Amethyst = GetModuleHandleA("amethyst2_4.dll");
	if (!Amethyst) Amethyst = GetModuleHandleA("amethyst2_5.era");
	if (!Amethyst) Amethyst = GetModuleHandleA("amethyst2_5.dll");

	if (Amethyst) {
		auto InitNecromancy15 = (void(*)(void)) GetProcAddress(Amethyst, "InitNecromancy15");
		if (InitNecromancy15) InitNecromancy15();
		else { MessageBoxA(0, "detected Amethyst, but it's outdated", "more_SS_levels", MB_OK); }
	}
	//else { MessageBoxA(0, "couldn't link to Amethyst", "Knightmare  War Machines", MB_OK); }
}

void __stdcall Z_OnPreHeroScreen(TEvent* e) {
	current_hero = x[1];
	// fill_show_current_hero_SS();

	// void HERO_Screen_Preset(void);

}

void __stdcall Z_OnBeforeHeroInteraction(TEvent* e) {
	// ExecErmCmd("");
	// RedirectFile("","");
	//meet_hero_left  = x[1];
	//meet_hero_right = x[2];
	//fill_show_meeting_hero_SS();
	//hero_meet_recalc_frames();
}

void __stdcall Z_OnAfterWog(TEvent* e) {

}
void __stdcall Z_OnGameEnter(TEvent* e) {

}

void Z_Replace_SS_text_dwords(void) {
	SS_text_clean();

	for (int i = 0; SS_text_dwords[i].address; i++)
		Zecondary_Skills->WriteDword(SS_text_dwords[i].address, SS_text_dwords[i].new_dword);

	for (int i = 0; SS_text_bytes[i].address; i++)
		Zecondary_Skills->WriteByte(SS_text_bytes[i].address, SS_text_bytes[i].new_byte);
}

void patch_SS_Limit_Per_Hero() {

	int limit = SS_Limit_Per_Hero;
	Zecondary_Skills->WriteByte(0x004A7DD0, limit);
	Zecondary_Skills->WriteByte(0x004DAFCE, limit);
	Zecondary_Skills->WriteByte(0x004E256A, limit);
	Zecondary_Skills->WriteByte(0x0057417F, limit);
	Zecondary_Skills->WriteByte(0x005F0C87, limit);
	Zecondary_Skills->WriteByte(0x005F0F50, limit);
	Zecondary_Skills->WriteByte(0x00744AB4, limit);

}


void __stdcall Z_Replace_SS_text_dwords(TEvent* e) {
	Z_Replace_SS_text_dwords();
}


void install_hero_classes();

void __stdcall Z_OnAfterCreateWindow(TEvent* e) {


	replace_byte patch[] = {
		{0x006600f8, 'z'}, // zecsk82.def
		{0x006817d0, 'z'}, // zecsk32.def 
		{0x006601d0, 'z'}, // zecskill.def
		{0x00677280, 'z'}, // zSkilBon.def
		
		
		{0x0, 0x0}
	};
	
	for (int i = 0; patch[i].address; i++)
		Zecondary_Skills->WriteByte(patch[i].address, patch[i].new_byte);

	/**
	for (int i = 0; SS_text_dwords[i].address; i++)
		Zecondary_Skills->WriteDword(SS_text_dwords[i].address, SS_text_dwords[i].new_dword);
	*/
	//Zecondary_Skills->CreateHiHook(0x004DF7E0,SPLICE_,EXTENDED_,STDCALL_,hook_HERO_Screen_Preset);
	// Zecondary_Skills->WriteLoHook (0x004DF7d0, hook_HERO_Screen_Preset);
	// Zecondary_Skills->WriteLoHook(0x00754C10, hook_HERO_Screen_Refresh);
	// Zecondary_Skills->WriteJmp(0x004df7f0, (int) asm_HERO_Screen_Preset);

	//// 2021-04-01
	//Zecondary_Skills->WriteJmp(0x004df7f8, (int) show_HERO_SS_icons);
	//Zecondary_Skills->WriteJmp(0x004df800, (int)show_HERO_SS_icons_original);

	// Zecondary_Skills->WriteLoHook(0x004F5D22, hook_HERO_zecsk82a);
	// Zecondary_Skills->WriteLoHook(0x004F5DAE, hook_HERO_zecsk82b);

	/*
	for (int i = 0x004DE7E5; i < 0x004DE843; i++)
		Zecondary_Skills->WriteByte(i, 0x90);
	*/
	
	//Zecondary_Skills->WriteJmp(0x004DE7E5, (int)calc_pre_msgbox);
	
	
	for (int i = 0x004F5D0C; i < 0x004F5DFA; i++)
		Zecondary_Skills->WriteByte(i, 0x90);
	Zecondary_Skills->WriteJmp(0x004F5D0C, (int)calc_in_msgbox);
	

	for (int i = 0x005AE072; i < 0x005ae505; i++)
		Zecondary_Skills->WriteByte(i, 0x90);
	Zecondary_Skills->WriteJmp(0x005AE072, (int)hero_meet);

	// Zecondary_Skills->WriteJmp(0x004DADA4, (int)hero_level_A_patched);
	// Zecondary_Skills->WriteJmp(0x004F92F7, (int)hero_level_B_patched);

	// Zecondary_Skills->WriteLoHook(0x005AED38, hero_meet_recalc_frames);
	 Zecondary_Skills->WriteLoHook(0x005AEAFF, hero_meet_recalc_frames);
	// Zecondary_Skills->WriteLoHook(0x004F92F7, hero_level_B2_Hooked);

	 Zecondary_Skills->WriteJmp(0x004F1507, (int) patch_4F1507);
	 Zecondary_Skills->WriteLoHook(0x004E1CC6, Hook_004E1CC6);

	 installHeroLevelUp();
	 fill_SSP_Tables();
	 patch_magic();
	 patch_movement();
	 patch_machines();
	 patch_SS_Limit_Per_Hero();
	 // patch_ERM();
	 Show_HERO_SS_Icons_Install();
	 WoG_Area_install();
	 install_hero_classes();

	 Zecondary_Skills->WriteHiHook(0x4E2540, SPLICE_, EXTENDED_, THISCALL_, Hero_SSkillAdd_Hook);
	 Zecondary_Skills->WriteHiHook(0x4E24C0, SPLICE_, EXTENDED_, THISCALL_, Hero_sub_004E24C0_hook);
	 Zecondary_Skills->WriteHiHook(0x4E2610, SPLICE_, EXTENDED_, THISCALL_, Hero_sub_004E2610_hook);
	 Zecondary_Skills->WriteHiHook(0x7643DE, SPLICE_, EXTENDED_, THISCALL_, Hero_sub_7643DE_Hook);

	 link_amethyst();
}

#include<fstream>
_LHF_(debug_004b38af) {
	auto ofs = std::ofstream("debug_004b38af_esi.txt", std::ofstream::app);
	ofs << std::endl << c->esi<< std::flush;
	return EXEC_DEFAULT;
}

extern char witch_hut_levels[1024];
extern char university_levels[1024];

void __stdcall StoreData(TEvent* e)
{
	WriteSavegameSection(1024, (void*)witch_hut_levels, "Z_witch_hut_levels");
	WriteSavegameSection(1024, (void*)university_levels, "Z_university_levels");
	WriteSavegameSection(hero_SS_ext_size,			(void*)hero_SS_ext,		"Z_hero_SS_ext");
	WriteSavegameSection(sizeof(hero_SSShow_new),	(void*)hero_SSShow_new,	"Z_hero_SSShow_new");
}


void __stdcall RestoreData(TEvent* e)
{
	ReadSavegameSection(1024, (void*)witch_hut_levels, "Z_witch_hut_levels");
	ReadSavegameSection(1024, (void*)university_levels, "Z_university_levels");
	ReadSavegameSection(hero_SS_ext_size,		 (void*)hero_SS_ext,		"Z_hero_SS_ext");
	ReadSavegameSection(sizeof(hero_SSShow_new), (void*)hero_SSShow_new,	"Z_hero_SSShow_new");
}

_LHF_(FinishHeroDlg_Hook); void HeroDlg_Patch();
void Skills_Config(); void read_ballistic_chances(char* file_name); 
_LHF_(Net_Copy_Hero_Hook); _LHF_(Net_Copy_Hero_Hook2); _LHF_(hook_00746EE3);
extern "C" __declspec(dllexport) BOOL APIENTRY DllMain(HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
	if (reason == DLL_PROCESS_ATTACH)
	{
		char config_name[] = "Data\\More_SS_Levels.txt";
		if (!master_config(config_name)) { 
			MessageBoxA(0, "Cannot init Master Config.", "More_SS_Levels.era", MB_ICONERROR);
			return false;
		}
		Skills_Config(); read_ballistic_chances( (char*) "Data\\Ballistics15.txt");
		// SS_text_clean();
		globalPatcher = GetPatcher();
		Zecondary_Skills = globalPatcher->CreateInstance((char*)PINSTANCE_MAIN);
		patch_magic_early();

		//// don't work, may need to be fixed for network game
		//Zecondary_Skills->WriteLoHook(0x004D8775, Net_Copy_Hero_Hook);		
		//Zecondary_Skills->WriteLoHook(0x004D8775, Net_Copy_Hero_Hook2);
		
		//	Show_HERO_SS_Icons_Install();
		ConnectEra();
		Zecondary_Skills->WriteLoHook(0x00746EE3, hook_00746EE3);
		HeroDlg_Patch();

		//Zecondary_Skills->WriteLoHook(0x004b38af,debug_004b38af);

		// Z_Replace_SS_text_dwords();

		/*
		for (int i = 0; SS_text_dwords[i].address; i++)
			Zecondary_Skills->WriteDword(SS_text_dwords[i].address, SS_text_dwords[i].new_dword);
		*/

		////code here
		//RegisterHandler(InitializeHandler2, "OnAfterLoadGame");
		//RegisterHandler(InitializeHandler2, "OnAfterErmInstructions");
		//ApiHook((void*)Hook_Initialize1, HOOKTYPE_CALL, (void*)0x00402340);
		//ApiHook((void*)Hook_Initialize2, HOOKTYPE_JUMP, (void*)0x00402450);


		RegisterHandler(Z_OnAfterCreateWindow, (char*)"OnAfterCreateWindow");

		RegisterHandler(Z_OnAfterWog,  (char*)"OnAfterWoG");
		RegisterHandler(Z_OnGameEnter, (char*)"OnGameEnter");
		RegisterHandler(Z_OnPreHeroScreen, (char*)"OnPreHeroScreen");
		RegisterHandler(Z_OnBeforeHeroInteraction, (char*) "OnBeforeHeroInteraction");
		
		RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnAfterCreateWindow");
		//RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnAfterLoadGame");
		//RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnAfterErmInstructions");
		//RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnAfterErmInstructions");
		// RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnBeforeWoG");
		// RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnAfterWoG");

		// RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnAfterWoG");

		// RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnBeforeWoG");

		//ApiHook((void*)Hook_Blank, HOOKTYPE_CALL, (void*)0x00402340);


		RegisterHandler(StoreData, "OnSavegameWrite");
		RegisterHandler(RestoreData, "OnSavegameRead");
	}
	return TRUE;
};

