
#include <windows.h>
#include <cstdio>
#include <stdlib.h>
//#include "Tchar.h"
#include <io.h>

#include "Storage.h"

#define char_table_size 512
#define int_tuple_size 256

void ParseDouble(char* buf, char* name, double* result)
{
	char* c = strstr(buf, name);
	if (c != NULL)
		*result = atof(c + strlen(name));
}

void ParseFloat(char* buf, char* name, float* result)
{
	char* c = strstr(buf, name);
	if (c != NULL)
		*result = atof(c + strlen(name));
}

void ParseInt(char* buf, char* name, int* result)
{
	char* c = strstr(buf, name);
	if (c != NULL)
		*result = atoi(c + strlen(name));
}

void ParseByte(char* buf, char* name, char* result)
{
	char* c = strstr(buf, name);
	if (c != NULL)
		*result = (char)atoi(c + strlen(name));
}


bool ParseStr(char* buf, char* name, /*char* &target */ char res[char_table_size])
{
	// char res[char_table_size];
	char* c = strstr(buf, name);
	int l = strlen(name);
	if (c != NULL)
	{
		char* cend = strstr(c + l, "\"");
		if (cend != NULL) //���� ��, ����� ������ �����������
		{
			//*res = (char*)malloc(cend-c+1);

			//memset(*res,0,cend-c+1);
			memset(res, 0, cend - c + 1);

			for (int i = 0, j = 0; j < (cend - c) - l; i++, j++)
			{
				if (c[l + j] != '\\')
				{
					//(*res)[i] = c[l+j];
					res[i] = c[l + j];
				}
				else
				{
					if (c[l + j + 1] == 'r')
					{
						//(*res)[i] = '\r';
						res[i] = '\r';

						j++;
						continue;
					}
					else if (c[l + j + 1] == 'n')
					{
						//(*res)[i] = '\n';
						res[i] = '\n';
						j++;
						continue;
					}
					else
					{
						//(*res)[i] = c[l+j];
						res[i] = c[l + j];

					}
				}
			}

			//strncpy(*res,c+l,cend-1-c);

			//(*res)[cend-c-l] = 0;

			res[cend - c - l] = 0;

			//res[cend - c - l] = 13; //end of WoG erm
			//res[cend - c - l +1] = 0;

			//sprintf(*res,*res);

			/*
			if (strcmp(target, res) != 0 ) {
				strcpy(target, res);
			}
			*/
		}
		return true;
	}
	else return false;
}
void ParseStr2(char* buf, char* name, char*& target_1, char*& target_2
	/* char res[char_table_size]*/, char* target_static, int& target_int) {

	char res[char_table_size];
	if (ParseStr(buf, name, res)) {
		target_1 = target_2 = target_static;
		strcpy(target_static, res);
		target_int = 0;
	}

	/*
	if (target && target!= target2 && !*target)
		strcpy(target2, target);
	ParseStr(buf, name, target2);
	target = target2;
	target3 = 0;
	*/

	/*
	char res[char_table_size];
	ParseStr(buf, name, res);
	if (!target || strcmp(target, res) != 0) {
		//char* tmp = new char[char_table_size];
		//target = tmp;
		target = target2;
		strcpy(target, res);
	}
	*/
}


int ParseTuple(char* buf, char* name, /* int** tuple */ int tuple[int_tuple_size])
{
	int len = 0;
	//char *tmp = (char*)malloc(strlen(buf));
	static char tmp[32768];
	char* c = strstr(buf, name);
	if (c != NULL)
	{
		strncpy(tmp, c + strlen(name), int_tuple_size);
		c = strpbrk(tmp, "\r\n\0}");
		if (c != NULL)
		{
			tmp[c - tmp] = 0;

			char* p = strtok(tmp, ",");

			do
			{
				if (p)
				{
					len++;
					// *tuple = (int*)realloc(*tuple,len*sizeof(int));
					// (*tuple)[len-1]=atoi(p);
					tuple[len - 1] = atoi(p);
				}
				p = strtok(NULL, ", ");
			} while (p);
		}
	}
	//free(tmp);
	return len;
}

int  max_magic_mastery = 7;
int  max_SS_level = 15;
int  max_level_UP_Counter = 3;
int  Diplo_Divider = 4;
int  Diplo_Shift = 1;
//   int SOD_SS_Count = 28;
int  current_SS_count = _SOD_SS_Count_;
int  SS_Limit_Per_Hero = _SOD_SS_Count_;
int  SS_0_3_to_SS_0_15[4] = {0, 5, 10, 15};
double qword_levelup = 1.2;
int  SS_Multiple_Build = -1;

bool master_config(char* file_name) {

	if (auto fdesc = fopen(file_name, "r"))
	{


		//----------
		fseek(fdesc, 0, SEEK_END);
		int fdesc_size = ftell(fdesc);
		rewind(fdesc);
		//----------
		char* buf = (char*)malloc(fdesc_size + 1);
		memset(buf, 0, fdesc_size + 1);
		fread(buf, 1, fdesc_size, fdesc);
		//buf[fdesc_size]=0;

		ParseDouble(buf, const_cast<char*>("LevelUpMultiplier="), &qword_levelup);


		ParseInt(buf, const_cast<char*>("max_magic_mastery="), &max_magic_mastery);

		ParseInt(buf, const_cast<char*>("Max_SS_Level="),	&max_SS_level);
		ParseInt(buf, const_cast<char*>("SS_Per_Level="),	&max_level_UP_Counter);
		ParseInt(buf, const_cast<char*>("SS_Per_Hero="),	&SS_Limit_Per_Hero);
		ParseInt(buf, const_cast<char*>("SS_Count="),		&current_SS_count);
		ParseInt(buf, const_cast<char*>("Diplo_Divider="),	&Diplo_Divider);
		ParseInt(buf, const_cast<char*>("Diplo_Shift="),	&Diplo_Shift);

		ParseInt(buf, const_cast<char*>("SS_Novice="),	 SS_0_3_to_SS_0_15 + 0);
		ParseInt(buf, const_cast<char*>("SS_Basic="),	 SS_0_3_to_SS_0_15 + 1);
		ParseInt(buf, const_cast<char*>("SS_Advanced="), SS_0_3_to_SS_0_15 + 2);
		ParseInt(buf, const_cast<char*>("SS_Expert="),	 SS_0_3_to_SS_0_15 + 3);

		ParseInt(buf, const_cast<char*>("SS_Multiple_Build="), &SS_Multiple_Build);

		fclose(fdesc);

		if (Diplo_Divider == 0) return false;
		if (max_SS_level < 3 || max_SS_level > 15) return false;
		if (current_SS_count < _SOD_SS_Count_) return false;


		//ConfigureArt(target, buf);

		//free(buf);

		return true;
	}
	else return false;

}


extern "C" __declspec(dllexport) void ApplyHeroClass(int skillID, char* config_str);
extern "C" __declspec(dllexport) void ApplyDescriptions(int skillID, char* config_str);
extern "C" __declspec(dllexport) void ApplySkillPower(int skillID, char* config_str);

void Skills_Config() {
	char path[512];
	for (int i = 0; i < current_SS_count; ++i) {
		sprintf(path, "Data\\Secondary_Skills\\%i.cfg", i);
		if (auto fdesc = fopen(path, "r")) {
			fseek(fdesc, 0, SEEK_END);
			int fdesc_size = ftell(fdesc);
			rewind(fdesc);
			//----------
			char* buf = (char*)malloc(fdesc_size + 1);
			memset(buf, 0, fdesc_size + 1);
			fread(buf, 1, fdesc_size, fdesc);

			ApplyHeroClass(i, buf);
			ApplyDescriptions(i, buf);
			ApplySkillPower(i, buf);

			free(buf);
			fclose(fdesc);
		}

	}
}

