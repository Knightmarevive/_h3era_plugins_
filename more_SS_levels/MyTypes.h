#pragma once
// #include"heroes.h"

// typedef ART_RECORD _ArtSetUp_;

#define char_table_size 512
#define int_tuple_size 256
#define SS_variable_types_count 8

#include "Storage.h"

#define _H3API_PLUGINS_
#define _H3API_PATCHER_X86_
#include "../__include__/H3API/single_header/H3API.hpp"

struct replace_byte {
	unsigned int address;
	unsigned char new_byte;
};

struct replace_dword {
	unsigned int address;
	unsigned int new_dword;
};


union SSP_field {
	float f;
	int i;
};

typedef SSP_field SSP_Line[16];
typedef SSP_Line SSP_Table[_skill_limit_];


struct _SSDesc_ { // 0x698C34
	char* Name[16]; // ���,�����,����
};


struct H3SecondarySkillInfoNew
{
	LPCSTR name;
	LPCSTR description[15];
};


struct H3SecondarySkillInfo
{
	LPCSTR name;
	LPCSTR description[3];
};

// typedef h3::H3Hero HERO;
#include "heroes.h"


inline int& ref_arg_n(HookContext *c, int n)
{
	return *reinterpret_cast<int*>(c->ebp + 4 + 4 * n);
}

// #include "call_convention.hpp"