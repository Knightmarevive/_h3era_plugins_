#pragma once
#define _hero_limit_		1024
#define _hero_class_limit_	128    
#define _skill_limit_		128
#define _SOD_SS_Count_		28
#define _Frame_Shift_		0x400

extern char hero_SS_ext			[_hero_limit_][_skill_limit_ - _SOD_SS_Count_];
constexpr long hero_SS_ext_size = _hero_limit_ * (_skill_limit_ - _SOD_SS_Count_);
extern char hero_SSShow_new		[_hero_limit_][_skill_limit_];
extern int Storage_current_hero;

// #include "heroes.h"

int count_SSShow(int heronum);
void fix_hero_SSShow(int heronum);

char  __stdcall get_hero_SS(int heronum, int skilllnum);
void  __stdcall set_hero_SS(int heronum, int skilllnum, int value);

//char  __stdcall get_hero_SS(HERO* H, int skilllnum);
//void  __stdcall set_hero_SS(HERO* H, int skilllnum, int value);

//char*  __stdcall ptr_hero_SS(int heronum, int skilllnum);
//char*  __stdcall ptr_hero_SS(HERO* H, int skilllnum);