#include "heroes.h"

int meet_hero_left = -1; int meet_hero_right = -1;

typedef void(*voidfun)(void);
voidfun hero_meet_ret = (voidfun)0x005ae505;
voidfun fun_54C900 = (voidfun) 0x0054C900;
// voidfun fun_4EA800 = (voidfun) 0x004EA800; // <h3era hd.create_dlg_item>
extern voidfun fun_4EA800; // <h3era hd.create_dlg_item>
//voidfun fun_617492 = (voidfun) 0x00617492;
extern voidfun fun_617492;

void hero_meet_original(void);
char*   ptr_6817D0 = (char*)   0x006817D0; // <h3era hd.zecsk32>
//const char* void_def = "void.def";
extern const char* void_def;

extern int left_hero_SS[10] = { 0 };
extern int right_hero_SS[10] = { 0 };


// extern int left_hero_SS_frame[10]  = { 0 };
// extern int right_hero_SS_frame[10] = { 0 };

/*
void hero_meet_recalc_frames(void) {

	for (int i = 1; i < 9; i++)
	{
		left_hero_SS_frame[i] = 0;
		right_hero_SS_frame[i] = 0;

		if (meet_hero_left >= 0  && left_hero_SS[i]) 
			left_hero_SS_frame[i]= ((HERO*) GetHeroRecord(meet_hero_left))->SSkill[left_hero_SS[i]];

		if (meet_hero_right >= 0 && right_hero_SS[i])
			right_hero_SS_frame[i] = ((HERO*)GetHeroRecord(meet_hero_right))->SSkill[right_hero_SS[i]];
	}
	
}
*/

///// original begin = 0x005AE072
__declspec(naked) void hero_meet(void) {
	_asm {
		je label_5AE095
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push ptr_6817D0 //<h3era hd.zecsk32>
		push 0x4C8
		push 0x20
		push 0x20
		push 0x57
		push 0x1E
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE097
		label_5AE095 :
		xor eax, eax
			label_5AE097 :
		lea edx, dword ptr ss : [ebp + 0x8]
			mov ecx, esi
			push edx
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp + 0x8] , eax
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xA1
			je label_5AE0DE
			push 0x10
			push ebx
			push ebx
			push ebx

			
			push ebx // left_hero_SS[1]  // push ebx
			
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4C9
			push 0x20
			push 0x20
			push 0x57
			push 0x42
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE0E0
			label_5AE0DE :
		xor eax, eax
			label_5AE0E0 :
		mov dword ptr ss : [ebp + 0x8] , eax
			lea eax, dword ptr ss : [ebp + 0x8]
			push eax
			mov ecx, esi
			mov byte ptr ss : [ebp - 0x4] , bl
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xA2
			je label_5AE127
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4CA
			push 0x20
			push 0x20
			push 0x57
			push 0x66
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE129
			label_5AE127 :
		xor eax, eax
			label_5AE129 :
		lea ecx, dword ptr ss : [ebp + 0x8]
			mov byte ptr ss : [ebp - 0x4] , bl
			push ecx
			mov ecx, esi
			mov dword ptr ss : [ebp + 0x8] , eax
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xA3
			je label_5AE173
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4CB
			push 0x20
			push 0x20
			push 0x57
			push 0x8A
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE175
			label_5AE173 :
		xor eax, eax
			label_5AE175 :
		lea edx, dword ptr ss : [ebp + 0x8]
			mov ecx, esi
			push edx
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp + 0x8] , eax
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xA4
			je label_5AE1BF
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4CC
			push 0x20
			push 0x20
			push 0x57
			push 0xAE
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE1C1
			label_5AE1BF :
		xor eax, eax
			label_5AE1C1 :
		mov dword ptr ss : [ebp + 0x8] , eax
			lea eax, dword ptr ss : [ebp + 0x8]
			push eax
			mov ecx, esi
			mov byte ptr ss : [ebp - 0x4] , bl
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xA5
			je label_5AE20B
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4CD
			push 0x20
			push 0x20
			push 0x57
			push 0xD2
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE20D
			label_5AE20B :
		xor eax, eax
			label_5AE20D :
		lea ecx, dword ptr ss : [ebp + 0x8]
			mov byte ptr ss : [ebp - 0x4] , bl
			push ecx
			mov ecx, esi
			mov dword ptr ss : [ebp + 0x8] , eax
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xA6
			je label_5AE257
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4CE
			push 0x20
			push 0x20
			push 0x57
			push 0xF6
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE259
			label_5AE257 :
		xor eax, eax
			label_5AE259 :
		lea edx, dword ptr ss : [ebp + 0x8]
			mov ecx, esi
			push edx
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp + 0x8] , eax
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xA7
			je label_5AE2A3
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4CF
			push 0x20
			push 0x20
			push 0x57
			push 0x11A
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE2A5
			label_5AE2A3 :
		xor eax, eax
			label_5AE2A5 :
		mov dword ptr ss : [ebp + 0x8] , eax
			lea eax, dword ptr ss : [ebp + 0x8]
			push eax
			mov ecx, esi
			mov byte ptr ss : [ebp - 0x4] , bl
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xA8
			je label_5AE2EF
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4D0
			push 0x20
			push 0x20
			push 0x57
			push 0x1E4
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE2F1
			label_5AE2EF :
		xor eax, eax
			label_5AE2F1 :
		lea ecx, dword ptr ss : [ebp + 0x8]
			mov byte ptr ss : [ebp - 0x4] , bl
			push ecx
			mov ecx, esi
			mov dword ptr ss : [ebp + 0x8] , eax
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xA9
			je label_5AE33B
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4D1
			push 0x20
			push 0x20
			push 0x57
			push 0x208
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE33D
			label_5AE33B :
		xor eax, eax
			label_5AE33D :
		lea edx, dword ptr ss : [ebp + 0x8]
			mov ecx, esi
			push edx
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp + 0x8] , eax
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xAA
			je label_5AE387
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4D2
			push 0x20
			push 0x20
			push 0x57
			push 0x22C
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE389
			label_5AE387 :
		xor eax, eax
			label_5AE389 :
		mov dword ptr ss : [ebp + 0x8] , eax
			lea eax, dword ptr ss : [ebp + 0x8]
			push eax
			mov ecx, esi
			mov byte ptr ss : [ebp - 0x4] , bl
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xAB
			je label_5AE3D3
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4D3
			push 0x20
			push 0x20
			push 0x57
			push 0x250
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE3D5
			label_5AE3D3 :
		xor eax, eax
			label_5AE3D5 :
		lea ecx, dword ptr ss : [ebp + 0x8]
			mov byte ptr ss : [ebp - 0x4] , bl
			push ecx
			mov ecx, esi
			mov dword ptr ss : [ebp + 0x8] , eax
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xAC
			je label_5AE41F
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4D4
			push 0x20
			push 0x20
			push 0x57
			push 0x274
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE421
			label_5AE41F :
		xor eax, eax
			label_5AE421 :
		lea edx, dword ptr ss : [ebp + 0x8]
			mov ecx, esi
			push edx
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp + 0x8] , eax
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xAD
			je label_5AE46B
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4D5
			push 0x20
			push 0x20
			push 0x57
			push 0x298
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE46D
			label_5AE46B :
		xor eax, eax
			label_5AE46D :
		mov dword ptr ss : [ebp + 0x8] , eax
			lea eax, dword ptr ss : [ebp + 0x8]
			push eax
			mov ecx, esi
			mov byte ptr ss : [ebp - 0x4] , bl
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xAE
			je label_5AE4B7
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4D6
			push 0x20
			push 0x20
			push 0x57
			push 0x2BC
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE4B9
			label_5AE4B7 :
		xor eax, eax
			label_5AE4B9 :
		lea ecx, dword ptr ss : [ebp + 0x8]
			mov byte ptr ss : [ebp - 0x4] , bl
			push ecx
			mov ecx, esi
			mov dword ptr ss : [ebp + 0x8] , eax
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xAF
			je label_5AE503
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push  ptr_6817D0 //<h3era hd.zecsk32>
			push 0x4D7
			push 0x20
			push 0x20
			push 0x57
			push 0x2E0
			mov ecx, eax
			call fun_4EA800 // <h3era hd.create_dlg_item>
			jmp label_5AE505
			label_5AE503 :
		xor eax, eax
			label_5AE505 :


			mov dword ptr ss : [ebp + 0x8] , eax
			lea eax, dword ptr ss : [ebp + 0x8]
			push eax
			mov ecx, esi
			mov byte ptr ss : [ebp - 0x4] , bl
			call fun_54C900
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x20] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0xAE


		jmp hero_meet_original
	}
}

// #define ptr_6817D0 void_def

///// original begin = 0x005AE072
__declspec(naked) void hero_meet_original(void) {
	_asm {
		je label_5AE095
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push ptr_6817D0 //<h3era hd.zecsk32>
		push 0xC8
		push 0x20
		push 0x20
		push 0x57
		push 0x1E
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE097
		label_5AE095:
		xor eax, eax
		label_5AE097:
		lea edx, dword ptr ss : [ebp + 0x8]
		mov ecx, esi
		push edx
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp + 0x8] , eax
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xA1
		je label_5AE0DE
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xC9
		push 0x20
		push 0x20
		push 0x57
		push 0x42
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE0E0
			label_5AE0DE:
		xor eax, eax
			label_5AE0E0:
		mov dword ptr ss : [ebp + 0x8] , eax
		lea eax, dword ptr ss : [ebp + 0x8]
		push eax
		mov ecx, esi
		mov byte ptr ss : [ebp - 0x4] , bl
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xA2
		je label_5AE127
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xCA
		push 0x20
		push 0x20
		push 0x57
		push 0x66
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE129
			label_5AE127:
		xor eax, eax
			label_5AE129:
		lea ecx, dword ptr ss : [ebp + 0x8]
		mov byte ptr ss : [ebp - 0x4] , bl
		push ecx
		mov ecx, esi
		mov dword ptr ss : [ebp + 0x8] , eax
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xA3
		je label_5AE173
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xCB
		push 0x20
		push 0x20
		push 0x57
		push 0x8A
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE175
			label_5AE173:
		xor eax, eax
			label_5AE175:
		lea edx, dword ptr ss : [ebp + 0x8]
		mov ecx, esi
		push edx
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp + 0x8] , eax
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xA4
		je label_5AE1BF
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xCC
		push 0x20
		push 0x20
		push 0x57
		push 0xAE
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE1C1
			label_5AE1BF:
		xor eax, eax
			label_5AE1C1:
		mov dword ptr ss : [ebp + 0x8] , eax
		lea eax, dword ptr ss : [ebp + 0x8]
		push eax
		mov ecx, esi
		mov byte ptr ss : [ebp - 0x4] , bl
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xA5
		je label_5AE20B
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xCD
		push 0x20
		push 0x20
		push 0x57
		push 0xD2
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE20D
			label_5AE20B:
		xor eax, eax
			label_5AE20D:
		lea ecx, dword ptr ss : [ebp + 0x8]
		mov byte ptr ss : [ebp - 0x4] , bl
		push ecx
		mov ecx, esi
		mov dword ptr ss : [ebp + 0x8] , eax
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xA6
		je label_5AE257
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xCE
		push 0x20
		push 0x20
		push 0x57
		push 0xF6
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE259
			label_5AE257:
		xor eax, eax
			label_5AE259:
		lea edx, dword ptr ss : [ebp + 0x8]
		mov ecx, esi
		push edx
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp + 0x8] , eax
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xA7
		je label_5AE2A3
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xCF
		push 0x20
		push 0x20
		push 0x57
		push 0x11A
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE2A5
			label_5AE2A3:
		xor eax, eax
			label_5AE2A5:
		mov dword ptr ss : [ebp + 0x8] , eax
		lea eax, dword ptr ss : [ebp + 0x8]
		push eax
		mov ecx, esi
		mov byte ptr ss : [ebp - 0x4] , bl
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xA8
		je label_5AE2EF
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xD0
		push 0x20
		push 0x20
		push 0x57
		push 0x1E4
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE2F1
			label_5AE2EF:
		xor eax, eax
			label_5AE2F1:
		lea ecx, dword ptr ss : [ebp + 0x8]
		mov byte ptr ss : [ebp - 0x4] , bl
		push ecx
		mov ecx, esi
		mov dword ptr ss : [ebp + 0x8] , eax
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xA9
		je label_5AE33B
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xD1
		push 0x20
		push 0x20
		push 0x57
		push 0x208
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE33D
			label_5AE33B:
		xor eax, eax
			label_5AE33D:
		lea edx, dword ptr ss : [ebp + 0x8]
		mov ecx, esi
		push edx
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp + 0x8] , eax
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xAA
		je label_5AE387
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xD2
		push 0x20
		push 0x20
		push 0x57
		push 0x22C
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE389
			label_5AE387:
		xor eax, eax
			label_5AE389:
		mov dword ptr ss : [ebp + 0x8] , eax
		lea eax, dword ptr ss : [ebp + 0x8]
		push eax
		mov ecx, esi
		mov byte ptr ss : [ebp - 0x4] , bl
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xAB
		je label_5AE3D3
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xD3
		push 0x20
		push 0x20
		push 0x57
		push 0x250
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE3D5
			label_5AE3D3:
		xor eax, eax
			label_5AE3D5:
		lea ecx, dword ptr ss : [ebp + 0x8]
		mov byte ptr ss : [ebp - 0x4] , bl
		push ecx
		mov ecx, esi
		mov dword ptr ss : [ebp + 0x8] , eax
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xAC
		je label_5AE41F
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xD4
		push 0x20
		push 0x20
		push 0x57
		push 0x274
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE421
			label_5AE41F:
		xor eax, eax
			label_5AE421:
		lea edx, dword ptr ss : [ebp + 0x8]
		mov ecx, esi
		push edx
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp + 0x8] , eax
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xAD
		je label_5AE46B
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xD5
		push 0x20
		push 0x20
		push 0x57
		push 0x298
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE46D
			label_5AE46B:
		xor eax, eax
			label_5AE46D:
		mov dword ptr ss : [ebp + 0x8] , eax
		lea eax, dword ptr ss : [ebp + 0x8]
		push eax
		mov ecx, esi
		mov byte ptr ss : [ebp - 0x4] , bl
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xAE
		je label_5AE4B7
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xD6
		push 0x20
		push 0x20
		push 0x57
		push 0x2BC
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE4B9
			label_5AE4B7:
		xor eax, eax
			label_5AE4B9:
		lea ecx, dword ptr ss : [ebp + 0x8]
		mov byte ptr ss : [ebp - 0x4] , bl
		push ecx
		mov ecx, esi
		mov dword ptr ss : [ebp + 0x8] , eax
		call fun_54C900
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x20] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0xAF
		je label_5AE503
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push  ptr_6817D0 //<h3era hd.zecsk32>
		push 0xD7
		push 0x20
		push 0x20
		push 0x57
		push 0x2E0
		mov ecx, eax
		call fun_4EA800 // <h3era hd.create_dlg_item>
		jmp label_5AE505
			label_5AE503:
		xor eax, eax
			label_5AE505:

		jmp hero_meet_ret
	}
}