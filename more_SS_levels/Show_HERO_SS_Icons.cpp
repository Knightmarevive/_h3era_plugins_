// #include "patcher_x86.hpp"
// #include "H3DlgItem.hpp"
#include "MyTypes.h"

extern PatcherInstance* Zecondary_Skills;

//#include "heroes.h"
#include "MyTypes.h"

typedef void(*voidfun)(void);

voidfun fun_5FE2D0 = (voidfun) 0x5FE2D0;
voidfun fun_617492 = (voidfun) 0x617492;
voidfun fun_4EA800 = (voidfun) 0x4EA800;
voidfun ret_4df800 = (voidfun) 0x004df800;
voidfun ret_4dfa36 = (voidfun) 0x004dfa36;
char* Zecskill_def = (char*)0x006601d0;
const char* void_def = "void.def";

void HERO_Screen_Calc(void);
extern int show_hero_SS[10];
HERO* z_current_HERO;

//char* show_hero_SS_address = (char*) show_hero_SS;

///// modified code
__declspec(naked) void show_HERO_SS_icons(void) {
	_asm {

		//call show_HERO_SS_icons_original
		//lea ecx, dword ptr ss : [ebp - 0x20] // missing line

		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push ecx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
		 
		nop
		nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x2B
		// je label_4DF7FF
 
		//mov ebx, 1
		//call HERO_Screen_Calc
		// mov ebx, [show_hero_SS+1*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x44F
		push 0x2C
		push 0x2C
		push 0x114
		push 0x12
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF801

			label_4DF7FF:
		xor eax, eax

			label_4DF801:
		lea edx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push edx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			nop 
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x2C
		// je label_4DF851

			 
			nop


			// mov ebx, [show_hero_SS+2*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x450
		push 0x2C
		push 0x2C
		push 0x114
		push 0xA1
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF853

			label_4DF851:
		xor eax, eax

			label_4DF853:
		lea ecx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push ecx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x2D
		// je label_4DF8A0

			
			nop

			// mov ebx, [show_hero_SS+3*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x451
		push 0x2C
		push 0x2C
		push 0x144
		push 0x12
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF8A2

			label_4DF8A0:
		xor eax, eax

			label_4DF8A2:
		lea edx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push edx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x2E
		// je label_4DF8F2

			nop

			// mov ebx, [show_hero_SS+4*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x452
		push 0x2C
		push 0x2C
		push 0x144
		push 0xA1
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF8F4

			label_4DF8F2:
		xor eax, eax

			label_4DF8F4:
		lea ecx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push ecx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			nop
			

			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x2F
		// je label_4DF941
		
			nop

			// mov ebx, [show_hero_SS+5*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x453
		push 0x2C
		push 0x2C
		push 0x174
		push 0x12
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF943

			label_4DF941:
		xor eax, eax

			label_4DF943:
		lea edx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push edx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x30
		// je label_4DF993

			
			nop

			// mov ebx, [show_hero_SS+6*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x454
		push 0x2C
		push 0x2C
		push 0x174
		push 0xA1
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF995

			label_4DF993:
		xor eax, eax

			label_4DF995:
		lea ecx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push ecx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
		
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x31
		// je label_4DF9E2
		

			// mov ebx, [show_hero_SS+7*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x455
		push 0x2C
		push 0x2C
		push 0x1A4
		push 0x12
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF9E4

			label_4DF9E2:
		xor eax, eax

			label_4DF9E4:
		lea edx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push edx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			 
			nop
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x32
		// je label_4DFA34

			
			nop
			nop

			// mov ebx, [show_hero_SS+8*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x456
		push 0x2C
		push 0x2C
		push 0x1A4
		push 0xA1
		mov ecx, eax
		call fun_4EA800
		jmp label_4dfa36

			label_4DFA34:
		xor eax, eax

			label_4dfa36:
		nop

			mov ebx, 0;

		nop

		jmp ret_4df800
	}
}

#define Zecskill_def void_def

///// original code
__declspec(naked) void show_HERO_SS_icons_original(void) {
	_asm {

		lea ecx, dword ptr ss : [ebp - 0x20] // missing line
		
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push ecx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0x2B
		je label_4DF7FF
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push Zecskill_def
		push 0x4F
		push 0x2C
		push 0x2C
		push 0x114
		push 0x12
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF801

		label_4DF7FF :
		xor eax, eax

			label_4DF801 :
		lea edx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push edx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x2C
			je label_4DF851
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x50
			push 0x2C
			push 0x2C
			push 0x114
			push 0xA1
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF853

			label_4DF851 :
		xor eax, eax

			label_4DF853 :
		lea ecx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push ecx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x2D
			je label_4DF8A0
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x51
			push 0x2C
			push 0x2C
			push 0x144
			push 0x12
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF8A2

			label_4DF8A0 :
		xor eax, eax

			label_4DF8A2 :
		lea edx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push edx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x2E
			je label_4DF8F2
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x52
			push 0x2C
			push 0x2C
			push 0x144
			push 0xA1
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF8F4

			label_4DF8F2 :
		xor eax, eax

			label_4DF8F4 :
		lea ecx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push ecx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x2F
			je label_4DF941
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x53
			push 0x2C
			push 0x2C
			push 0x174
			push 0x12
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF943

			label_4DF941 :
		xor eax, eax

			label_4DF943 :
		lea edx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push edx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x30
			je label_4DF993
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x54
			push 0x2C
			push 0x2C
			push 0x174
			push 0xA1
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF995

			label_4DF993 :
		xor eax, eax

			label_4DF995 :
		lea ecx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push ecx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x31
			je label_4DF9E2
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x55
			push 0x2C
			push 0x2C
			push 0x1A4
			push 0x12
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF9E4

			label_4DF9E2 :
		xor eax, eax

			label_4DF9E4 :
		lea edx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push edx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x32
			je label_4DFA34
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x56
			push 0x2C
			push 0x2C
			push 0x1A4
			push 0xA1
			mov ecx, eax
			call fun_4EA800
			jmp label_4dfa36

			label_4DFA34 :
		xor eax, eax

			label_4dfa36 :

		//last line is modified

		jmp ret_4dfa36
	}
}


struct _DlgDef1_ {
	char unk[0x48];
};

struct _DlgText_ {
	char unk[0x50];
};

struct _Dlg_ {
	int VTable;
	int ZOrder;
	int NextDialog;
	int LastDialog;

	char unk[0x68-0x10];
};

struct _DlgItem_ {
	char unk[0x30];
};

char  aSecskill_def[] = "Zecskill.def";
char  aSmalfont_fnt[] = "smalfont.fnt";
auto  wog_operator_new = (int (*) (int) ) 0x00617492;
auto  DlgItem_BuildTextItem = (_DlgText_*(*__thiscall)(_DlgText_*, int, int, int, int,char*,int,int,int,int,int,int) ) 0x005BC6A0;
auto  DlgDef_BuildAndLoadDef =(_DlgDef1_*(*__thiscall)(_DlgDef1_*, int , int , int , int , int , char* , int , int , int , int , int)) 0x004EA800;
auto  AddItemToList = (int(*__thiscall) (int*,void**,unsigned int,void**) )0x005FE2D0;

#define Z_LOBYTE(w) *(char*)((int*)&w)

/*
// modified part of Dlg_HeroInfo 
_Dlg_* add_hero_SS_to_hero_DLG(_Dlg_* ecx0, int a2, int asdfg, _DlgDef1_* v96){
	INT32 ItemsFirst; 
	int* Item; 
	_Dlg_* DLG; 


	_Dlg_* dlgHero; // edi
	_Dlg_* Dlg; // esi

	// _DlgDef1_* v96; 
	_DlgDef1_* v97; 
	_DlgDef1_* v98; 
	_DlgDef1_* v99; 

	_DlgDef1_* v100; // eax
	_DlgDef1_* v101; // eax
	_DlgDef1_* v102; // eax
	_DlgDef1_* v103; // eax
	_DlgDef1_* v104; // eax
	_DlgDef1_* v105; // eax
	_DlgDef1_* v106; // eax
	_DlgDef1_* v107; // eax
	_DlgDef1_* v108; // eax
	_DlgDef1_* v109; // eax
	_DlgDef1_* v110; // eax
	_DlgDef1_* v111; // eax
	_DlgDef1_* v112; // eax
	_DlgText_* v113; // eax
	_DlgText_* v114; // eax
	_DlgText_* v115; // eax
	_DlgText_* v116; // eax
	_DlgText_* v117; // eax
	_DlgText_* v118; // eax
	_DlgText_* v119; // eax
	_DlgText_* v120; // eax
	_DlgText_* v121; // eax
	_DlgText_* v122; // eax
	_DlgText_* v123; // eax
	_DlgText_* v124; // eax
	_DlgText_* v125; // eax
	_DlgText_* v126; // eax
	_DlgText_* v127; // eax
	_DlgText_* v128; // eax
	_DlgText_* v129; // eax
	_DlgText_* v130; // eax
	_DlgText_* v131; // eax
	_DlgText_* v132; // eax
	_DlgText_* v133; // eax
	_DlgText_* v134; // eax
	_DlgText_* v135; // eax
	_DlgText_* v136; // eax
	_DlgText_* v137; // eax
	_DlgText_* v138; // eax
	_DlgText_* v139; // eax
	_DlgText_* v140; // eax
	_DlgText_* v141; // eax
	_DlgText_* v142; // eax
	_DlgText_* v143; // eax
	_DlgText_* v144; // eax


	_DlgItem_** j; // esi
	int item; // [esp+0h] [ebp-20h]
	char* v292; // [esp+4h] [ebp-1Ch]
	int i; // [esp+8h] [ebp-18h]
	_Dlg_* dlg; // [esp+Ch] [ebp-14h]
	int v295; // [esp+10h] [ebp-10h]
	int v296; // [esp+1Ch] [ebp-4h]

	DLG = ecx0;
	dlg = ecx0;


	Dlg = (_Dlg_*)((char*)dlg + 48);

	item = (int)v96;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v97 = (_DlgDef1_*)wog_operator_new(72);
  v292 = (char*)v97;
  Z_LOBYTE(v296) = 43;
  if (v97)
	v98 = DlgDef_BuildAndLoadDef(v97, 18, 276, 44, 44, 79, aSecskill_def, 0, 0, 0, 0, 16);
  else
	v98 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v98;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v99 = (_DlgDef1_*)wog_operator_new(72);
  v292 = (char*)v99;
  Z_LOBYTE(v296) = 44;
  if (v99)
	v100 = DlgDef_BuildAndLoadDef(v99, 161, 276, 44, 44, 80, aSecskill_def, 0, 0, 0, 0, 16);
  else
	v100 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v100;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v101 = (_DlgDef1_*)wog_operator_new(72);
  v292 = (char*)v101;
  Z_LOBYTE(v296) = 45;
  if (v101)
	v102 = DlgDef_BuildAndLoadDef(v101, 18, 324, 44, 44, 81, aSecskill_def, 0, 0, 0, 0, 16);
  else
	v102 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v102;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v103 = (_DlgDef1_*)wog_operator_new(72);
  v292 = (char*)v103;
  Z_LOBYTE(v296) = 46;
  if (v103)
	v104 = DlgDef_BuildAndLoadDef(v103, 161, 324, 44, 44, 82, aSecskill_def, 0, 0, 0, 0, 16);
  else
	v104 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v104;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v105 = (_DlgDef1_*)wog_operator_new(72);
  v292 = (char*)v105;
  Z_LOBYTE(v296) = 47;
  if (v105)
	v106 = DlgDef_BuildAndLoadDef(v105, 18, 372, 44, 44, 83, aSecskill_def, 0, 0, 0, 0, 16);
  else
	v106 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v106;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v107 = (_DlgDef1_*)wog_operator_new(72);
  v292 = (char*)v107;
  Z_LOBYTE(v296) = 48;
  if (v107)
	v108 = DlgDef_BuildAndLoadDef(v107, 161, 372, 44, 44, 84, aSecskill_def, 0, 0, 0, 0, 16);
  else
	v108 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v108;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v109 = (_DlgDef1_*)wog_operator_new(72);
  v292 = (char*)v109;
  Z_LOBYTE(v296) = 49;
  if (v109)
	v110 = DlgDef_BuildAndLoadDef(v109, 18, 420, 44, 44, 85, aSecskill_def, 0, 0, 0, 0, 16);
  else
	v110 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v110;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v111 = (_DlgDef1_*)wog_operator_new(72);
  v292 = (char*)v111;
  Z_LOBYTE(v296) = 50;
  if (v111)
	v112 = DlgDef_BuildAndLoadDef(v111, 161, 420, 44, 44, 86, aSecskill_def, 0, 0, 0, 0, 16);
  else
	v112 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v112;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v113 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v113;
  Z_LOBYTE(v296) = 51;
  if (v113)
	v114 = DlgItem_BuildTextItem(v113, 68, 300, 90, 18, 0, (int)aSmalfont_fnt, 1, 87, 0, 0, 8);
  else
	v114 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v114;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v115 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v115;
  Z_LOBYTE(v296) = 52;
  if (v115)
	v116 = DlgItem_BuildTextItem(v115, 211, 300, 90, 18, 0, (int)aSmalfont_fnt, 1, 88, 0, 0, 8);
  else
	v116 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v116;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v117 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v117;
  Z_LOBYTE(v296) = 53;
  if (v117)
	v118 = DlgItem_BuildTextItem(v117, 68, 347, 90, 18, 0, (int)aSmalfont_fnt, 1, 89, 0, 0, 8);
  else
	v118 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v118;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v119 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v119;
  Z_LOBYTE(v296) = 54;
  if (v119)
	v120 = DlgItem_BuildTextItem(v119, 211, 347, 90, 18, 0, (int)aSmalfont_fnt, 1, 90, 0, 0, 8);
  else
	v120 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v120;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v121 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v121;
  Z_LOBYTE(v296) = 55;
  if (v121)
	v122 = DlgItem_BuildTextItem(v121, 68, 395, 90, 18, 0, (int)aSmalfont_fnt, 1, 91, 0, 0, 8);
  else
	v122 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v122;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v123 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v123;
  Z_LOBYTE(v296) = 56;
  if (v123)
	v124 = DlgItem_BuildTextItem(v123, 211, 395, 90, 18, 0, (int)aSmalfont_fnt, 1, 92, 0, 0, 8);
  else
	v124 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v124;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v125 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v125;
  Z_LOBYTE(v296) = 57;
  if (v125)
	v126 = DlgItem_BuildTextItem(v125, 68, 443, 90, 18, 0, (int)aSmalfont_fnt, 1, 93, 0, 0, 8);
  else
	v126 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v126;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v127 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v127;
  Z_LOBYTE(v296) = 58;
  if (v127)
	v128 = DlgItem_BuildTextItem(v127, 211, 443, 90, 18, 0, (int)aSmalfont_fnt, 1, 94, 0, 0, 8);
  else
	v128 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v128;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v129 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v129;
  Z_LOBYTE(v296) = 59;
  if (v129)
	v130 = DlgItem_BuildTextItem(v129, 68, 280, 90, 18, 0, (int)aSmalfont_fnt, 1, 95, 0, 0, 8);
  else
	v130 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v130;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v131 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v131;
  Z_LOBYTE(v296) = 60;
  if (v131)
	v132 = DlgItem_BuildTextItem(v131, 211, 280, 90, 18, 0, (int)aSmalfont_fnt, 1, 96, 0, 0, 8);
  else
	v132 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v132;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v133 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v133;
  Z_LOBYTE(v296) = 61;
  if (v133)
	v134 = DlgItem_BuildTextItem(v133, 68, 328, 90, 18, 0, (int)aSmalfont_fnt, 1, 97, 0, 0, 8);
  else
	v134 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v134;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v135 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v135;
  Z_LOBYTE(v296) = 62;
  if (v135)
	v136 = DlgItem_BuildTextItem(v135, 211, 328, 90, 18, 0, (int)aSmalfont_fnt, 1, 98, 0, 0, 8);
  else
	v136 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v136;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v137 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v137;
  Z_LOBYTE(v296) = 63;
  if (v137)
	v138 = DlgItem_BuildTextItem(v137, 68, 376, 90, 18, 0, (int)aSmalfont_fnt, 1, 99, 0, 0, 8);
  else
	v138 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v138;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v139 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v139;
  Z_LOBYTE(v296) = 64;
  if (v139)
	v140 = DlgItem_BuildTextItem(v139, 211, 376, 90, 18, 0, (int)aSmalfont_fnt, 1, 100, 0, 0, 8);
  else
	v140 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v140;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v141 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v141;
  Z_LOBYTE(v296) = 65;
  if (v141)
	v142 = DlgItem_BuildTextItem(v141, 68, 424, 90, 18, 0, (int)aSmalfont_fnt, 1, 101, 0, 0, 8);
  else
	v142 = 0;
  Z_LOBYTE(v296) = 0;
  item = (int)v142;
  AddItemToList(&Dlg->VTable, (void**)Dlg->NextDialog, 1u, (void**)&item);
  v143 = (_DlgText_*)wog_operator_new(80);
  v292 = (char*)v143;
  Z_LOBYTE(v296) = 66;
  if (v143)
	v144 = DlgItem_BuildTextItem(v143, 211, 424, 90, 18, 0, (int)aSmalfont_fnt, 1, 102, 0, 0, 8);
  else
	v144 = 0;

}
*/

#include "heroes.h"
#include "Storage.h"
extern int  current_SS_count;
char* __stdcall ptr_hero_SS(HERO* H, int skilllnum);
char  __stdcall get_hero_SS(HERO* H, int skilllnum);

_LHF_(Hook_004E21DC) {
	int skill_level = get_hero_SS((HERO*)(char*)c->eax, c->esi);
	
	c->eax = skill_level;
	c->edx = c -> esi * 16 + 16 + skill_level;

	c->return_address = 0x004E21EB;
	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_004E2250) {
	int skill_level = get_hero_SS((HERO*)(char*)c->edx, c->eax);

	c->ecx = skill_level;

	c->return_address = 0x004E2258;
	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_004E21B3) {
	auto H = (HERO*)(char*)c->eax;
	c->edx = hero_SSShow_new[H->Number][c->esi];

	c->return_address = 0x004E21BA;
	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_004DBE69) {
	auto H = (HERO*)(char*)c->esi;
	c->edx = hero_SSShow_new[H->Number][c->eax];

	c->return_address = 0x004DBE70;
	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_004DE7C5) {
	HERO* H = (HERO*)(char*)c->esi;
	z_current_HERO = H;

	if(H)	c->eax = hero_SSShow_new[H->Number][c->edx];
	else    c->eax = hero_SSShow_new[z_current_HERO->Number][c->edx];

	c->return_address = 0x004DE7CC;
	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_004D8A68) {
	auto H = (HERO*)(char*)c->ebx;
	/*
	for (int i = 0; i < _skill_limit_) {
		hero_SSShow_new[H->Number][i] = 0;
	}
	*/

	memset(hero_SSShow_new[H->Number], 0, _skill_limit_);
	memset(hero_SS_ext[H->Number], 0, _skill_limit_ - _SOD_SS_Count_);
	return EXEC_DEFAULT;
}

_LHF_(Hook_004DBE8B) {
	HERO* H = (HERO*) c->esi;
	c->edx = get_hero_SS(H, c->eax);

	c->return_address = 0x004DBE93;
	return NO_EXEC_DEFAULT;
}

extern H3SecondarySkillInfoNew* newSkillsDescriptions;
int __stdcall _get_hero_SS_(int Hero, int skilllnum);

_LHF_(Hook_004DE7D9) {
	auto H = /*(HERO*)(char*)*/ c->esi;
	c->ecx = _get_hero_SS_(H, c->edx);

	//c->return_address = 0x004DE7E1;

	c->Push(0); c->Push(-1);
	c->Push(0); c->Push(-1);
	c->edi = (int) newSkillsDescriptions[c->edx].description[c->ecx - 1];
	c->Push(0); c->Push(-1);
	c->esi = c->edi; c->edi = 0x697428;
	strcpy( (char*) c->edi, (char*)c->esi);

	c->Push(0x410 + c->edx*16 + c->ecx);
	c->Push(0x14); c->Push(-1); c->Push(-1);
	c->edx = (c->ebx & 0xff) ? 4 : 1 ; 
	c->ecx = 0x697428;
	c->return_address = 0x004DE843;
	
	return NO_EXEC_DEFAULT;
}



_LHF_(Hook_005AECC7) {
	HERO* H = (HERO*)c->edx;
	c->ecx = get_hero_SS(H, c->eax);

	c->eax = 0x10 + c->ecx + c->eax * 0x10;

	c->return_address = 0x005AECD6;

	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_005B0EA3) {

	HERO* H = (HERO*)c->edx;
	c->eax = get_hero_SS(H, c->eax);

	c->return_address = 0x005B0EAB;

	return NO_EXEC_DEFAULT;
}

_LHF_(Hook_005B0396) {
	HERO* H = (HERO*)c->eax;
	c->ecx = get_hero_SS(H, c->edx);

	c->eax = 0x410 + c->ecx + c->edx * 0x10;

	c->return_address = 0x005B03A5;

	return NO_EXEC_DEFAULT;
}

_LHF_(hook_0051E073) {
	c->ecx = c->esi << 4;
	c->edx += c->ecx + 16;

	// c->edx = 17; //debug

	c->Push(0);
	c->return_address = 0x0051E07C;
	return NO_EXEC_DEFAULT;
}
_LHF_(hook_0051F522) {
	long frame = c->ecx + (c->eax << 4);
	c->edx = frame;

	c->Push(0); c->Push(-1);
	c->Push(0); c->Push(-1);

	c->edx = frame +  1024 + 16;
	c->Push(c->edx);

	// c->eax = c->ecx + (c->eax << 4);// +1024 + 16;
	c->ecx = *(int*)(*(int*)(0x0067DCF0) + frame * 4);
	c->Push(20); c->edx = c->BL() ? 4 : 1;

	c->Push(-1); c->Push(-1);
	c->return_address = 0x0051F54E;
	return NO_EXEC_DEFAULT;
}

extern int  current_SS_count;
void Show_HERO_SS_Icons_Install() {

	/*
	for (int i = 0x004df7b8; i <= 0x004DFF49; i++)
		Zecondary_Skills->WriteByte(i, 0x90);
	*/
	Zecondary_Skills->WriteLoHook(0x004E21DC, Hook_004E21DC);
	Zecondary_Skills->WriteLoHook(0x004E2250, Hook_004E2250);
	Zecondary_Skills->WriteByte(0x004E21C3 + 2, current_SS_count);
	Zecondary_Skills->WriteLoHook(0x004E21B3, Hook_004E21B3); // TODO: fix it - it breaks Hero Dialog
	Zecondary_Skills->WriteLoHook(0x004DBE69, Hook_004DBE69);
	Zecondary_Skills->WriteLoHook(0x004DE7C5, Hook_004DE7C5);
	Zecondary_Skills->WriteLoHook(0x004D8A68, Hook_004D8A68);
	Zecondary_Skills->WriteLoHook(0x004DBE8B, Hook_004DBE8B);

	Zecondary_Skills->WriteLoHook(0x004DE7D9, Hook_004DE7D9);
	Zecondary_Skills->WriteByte(0x004DE7D1+2, current_SS_count);

	//----------------------------------------------------------

	Zecondary_Skills->WriteLoHook(0x005AECC7, Hook_005AECC7);
	Zecondary_Skills->WriteLoHook(0x005B0EA3, Hook_005B0EA3);

	Zecondary_Skills->WriteLoHook(0x005B0396, Hook_005B0396);

	//----------------------------------------------------------

	Zecondary_Skills->WriteLoHook(0x0051E073, hook_0051E073);
	Zecondary_Skills->WriteLoHook(0x0051F522, hook_0051F522);

}