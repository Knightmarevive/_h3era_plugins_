#pragma once
// typedef unsigned long UINT32;

// * The specialty structure of heroes
struct H3HeroSpecialty
{
	// * +0
	UINT32  type;
	enum SpecialtyType
	{
		ST_skill = 0,
		ST_creatureLevel = 1,
		ST_resource = 2,
		ST_spell = 3,
		ST_staticCreature = 4,
		ST_speed = 5,
		ST_conversion = 6,
		ST_dragon = 7,
	};
	// * +4
	// * the ID of skill, creature, resource, spell, creature to upgrade (Dracon/Gelu)
	UINT32  bonusID;
	// * +8
	// * to be used with creature bonus
	UINT32  attackBonus;
	// * +C
	// * to be used with creature bonus
	UINT32  defenseBonus;
	// * +10
	// * to be used with creature bonus
	UINT32  damageBonus;
	// * +14
	// * the ID of the second creature that can be upgraded
	UINT32  upgrade2;
	// * +18
	// * the ID of the upgraded creature (Enchanters/Sharpshooters)
	UINT32  upgradeTo;
	// * +1C
	// * short specialty name
	LPCSTR	spShort;
	// * +20
	// * full specialty name
	LPCSTR	spFull;
	// * +24
	// * specialty description
	LPCSTR	spDescr;
};

inline H3HeroSpecialty& ZP_HeroSpecialty(int id)
{
	H3HeroSpecialty* table = (H3HeroSpecialty*)(*(int*)0x00679C80);
	return table[id];
	// return (*(H3HeroSpecialty**)(*(int*)(0x4B8AF1 + 1)))[id];
}