// #include "patcher_x86.hpp"
#include "MyTypes.h"
#include <fstream>

extern SSP_Table SecondarySkillsPowerInternal;
extern PatcherInstance* Zecondary_Skills;

int Movement_Cost[13][16];

_LHF_(pathfinding_4B14B5) {
	*(int*)(c->ebp + 0xC) = SecondarySkillsPowerInternal[0][*(int*)(c->ebp + 0xC)].i;
	return EXEC_DEFAULT;
}

/*
_LHF_(pathfinding_4B14E5) {
	*(int*)(c->ebp + 0xC) = SecondarySkillsPowerInternal[0][*(int*)(c->ebp + 0xC)].i;
	return EXEC_DEFAULT;
}
*/
/*
_LHF_(pathfinding_4B14FE) {
	*(int*)(c->ebp + 0xC) = SecondarySkillsPowerInternal[0][*(int*)(c->ebp + 0xC)].i;
	return EXEC_DEFAULT;
}
*/
/*
_LHF_(pathfinding_4B1501) {
	c->edx = SecondarySkillsPowerInternal[0][c->edx].i;
	return EXEC_DEFAULT;
}
*/
/*
_LHF_(pathfinding_4B1583) {
	c->eax = SecondarySkillsPowerInternal[0][c->eax].i;
	return EXEC_DEFAULT;
}
*/
_LHF_(pathfinding_56b728) {
	c->eax = SecondarySkillsPowerInternal[0][c->eax].i;
	return EXEC_DEFAULT;
}

_LHF_(pathfinding_4B14FE) {
	c->edx = *(int*)(c->ebp + 0xC);
	c->ecx = (c->edx + c->edi * 16);// * 4 + (int) &(Movement_Cost[0][0]);
	c->return_address = 0x004B1504;// 0x004B150B;
	return NO_EXEC_DEFAULT;
}
_LHF_(pathfinding_4B14E8) {
	c->ecx = c->edx + c->ecx *16; 
	c->return_address = 0x004B1504;
	return NO_EXEC_DEFAULT;
}


_LHF_(pathfinding_4B1580) {
	c->eax = *(int*)(c->ebp + 0xC);
	c->edx = (c->eax + c->edi * 16);// * 4 + (int) &(Movement_Cost[0][0]);
	c->return_address = 0x004B1586;
	return NO_EXEC_DEFAULT;
}

void patch_movement(void) {
	//Zecondary_Skills->WriteLoHook(0x004B14B5, pathfinding_4B14B5);
	//Zecondary_Skills->WriteLoHook(0x004B14E5, pathfinding_4B14E5);
	//Zecondary_Skills->WriteLoHook(0x004B14FE, pathfinding_4B14FE);
	//Zecondary_Skills->WriteLoHook(0x004B1501, pathfinding_4B1501);
	//Zecondary_Skills->WriteLoHook(0x004B1583, pathfinding_4B1583);
	//Zecondary_Skills->WriteLoHook(0x0056b728, pathfinding_56b728);

	Zecondary_Skills->WriteLoHook(0x004B14E8, pathfinding_4B14E8);
	Zecondary_Skills->WriteLoHook(0x004B14FE, pathfinding_4B14FE);
	Zecondary_Skills->WriteDword(0x004B1504 + 3, (int) &(Movement_Cost[0][0]) );
	Zecondary_Skills->WriteLoHook(0x004B1580, pathfinding_4B1580);
	Zecondary_Skills->WriteDword(0x004B1586 + 3, (int)&(Movement_Cost[0][0]));
	std::ifstream LandFile("data\\Pathfinding16.txt");
	for (int i = 0; i < 13; ++i) for (int j = 0; j < 16; ++j)
		LandFile >> Movement_Cost[i][j];

	// Navigation
	Zecondary_Skills->WriteDword(0x004E4C64 + 3, (unsigned int)SecondarySkillsPowerInternal[5]);
	Zecondary_Skills->WriteDword(0x004D760A + 1, (unsigned int)SecondarySkillsPowerInternal[5]);
	Zecondary_Skills->WriteDword(0x004E4C3C + 3, (unsigned int)SecondarySkillsPowerInternal[5]);
}