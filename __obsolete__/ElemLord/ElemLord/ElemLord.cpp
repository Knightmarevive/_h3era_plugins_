

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
//#include "Tchar.h"
#include <io.h>
#include "..\era.h"

#include "..\heroes.h"
#include "..\patcher_x86_commented.hpp"


#define PINSTANCE_MAIN "ElemLord"
Patcher * globalPatcher;
PatcherInstance *ElemLord;

#define ARTIFACT_FIRST_TOME 86
#define ARTIFACT_AIR_TOME 87
#define ARTIFACT_WATER_TOME 88
#define ARTIFACT_FIRE_TOME 86
#define ARTIFACT_EARTH_TOME 89
#define ARTIFACT_LAST_TOME 89

#define CREATURE_WATER_LORD 122
#define CREATURE_EARTH_LORD 124
#define CREATURE_AIR_LORD 126
#define CREATURE_FIRE_LORD 128

struct spellrecord {
	Byte Spell[70]; //   +3EA db*46 = ���������� (����/���)
	Byte LSpell[70];//   +430 db*46 = ������� ���������� (>=1)
	Byte Tome[4]; 
	Byte ArmageddonBlade;
	Byte reserved[3];
};

struct GAMEDATA {
	spellrecord herospell[256]; // up to Xeron
	Byte ready;
	Byte AI_turn;
	Byte reserved[2];
};

GAMEDATA Save;

void CleanTomes(void) {
	Save.ready = false;
	Save.AI_turn = 1;
	Save.reserved[1] = 0;
	Save.reserved[0] = 0;
	for (int h = 0; h < 256; h++) {
		Save.herospell[h].reserved[2] = 0;
		Save.herospell[h].reserved[1] = 0;
		Save.herospell[h].reserved[0] = 0;
		Save.herospell[h].ArmageddonBlade = 0;
		for (int t = 0;t < 4;t++)
			Save.herospell[h].Tome[t] = 0;

	}
}

void __stdcall MakeReady(PEvent e) {
	Save.ready = true;
}

void __stdcall CheckAI(PEvent e) {

	int tmp = *(ErmV + 2);
	int tmp2 = *(ErmV + 3);
	ExecErmCmd("OW:C?v2;");
	ExecErmCmd("OW:Iv2/?v3;");

	//Save.ready = true;
	Save.AI_turn = *(ErmV + 3);

	*(ErmV + 2) = tmp;
	*(ErmV + 3) = tmp2;
}

void __stdcall MakeReady_NonAI(PEvent e) {
	
	if(Save.AI_turn == 0)
		Save.ready = true;
}

void __stdcall MakeNonReady(PEvent e) {
	Save.ready = false;
}

int spell_school(int spell_no) {
	char buff[1024]; int tmp = *(ErmV + 2);
	sprintf_s(buff, "SS%i:S?v2;", spell_no);
	ExecErmCmd(buff); int ret = *(ErmV + 2);
	*(ErmV + 2) = tmp; return ret;
}

bool hero_has_creature(HERO* hero, int creature_type) {
	for (int c = 0; c < 7; c++) {
		int cre = hero->Ct[c];
		if (cre == creature_type) {
			return true;
		}
	}
	return false;
}


bool hero_has_artifact(HERO* hero, int artifact_type) {
	for (int slot = 0; slot < 19; slot++) {
		int art = hero->IArt[slot][0];
		if (art == artifact_type) {
			return true;
		}
	}
	return false;
}


void apply_spell_tome(HERO* hero, int tome, bool enable = true) {
	if ( hero->Number < 0 || hero->Number > 255 ) return;
	if (tome >= ARTIFACT_FIRST_TOME && tome <= ARTIFACT_LAST_TOME && Save.ready ) {
		if (hero_has_artifact(hero, tome) ) return; //hero has the tome
		if (hero_has_artifact(hero,  170) ) return; //hero has 4x tome combo
		 {
			int TomeID = tome - ARTIFACT_FIRST_TOME;
				for (int s = 0; s < 70; s++) {
					if (s == 57) continue;
					int school = spell_school(s);
					int schooltome = -1;
					if (school & 1) schooltome = ARTIFACT_AIR_TOME;
					if (school & 4) schooltome = ARTIFACT_WATER_TOME;
					if (school & 2) schooltome = ARTIFACT_FIRE_TOME;
					if (school & 8) schooltome = ARTIFACT_EARTH_TOME;
					if (school == 15) schooltome = -1;

					// armageddons blade fix
					if (s == 26 && (hero_has_artifact(hero, 128) || hero_has_artifact(hero, 162))) {
						Save.herospell[hero->Number].ArmageddonBlade = true;
						continue; }

					if (schooltome == tome && !Save.herospell[hero->Number].Tome[TomeID]) {


						// armageddons blade fix
						// if (s == 26 && (hero_has_artifact(hero, 128) || hero_has_artifact(hero, 162))) { continue; }

						Save.herospell[hero->Number].Spell[s] = hero->Spell[s];
						Save.herospell[hero->Number].LSpell[s] = hero->LSpell[s];
					}

					// if (s == 26 && Save.herospell[hero->Number].ArmageddonBlade) { continue; }
				}
			
			{
				
				if ((tome == ARTIFACT_AIR_TOME && !hero_has_creature(hero, CREATURE_AIR_LORD))
					|| (tome == ARTIFACT_FIRE_TOME && !hero_has_creature(hero, CREATURE_FIRE_LORD))
					|| (tome == ARTIFACT_WATER_TOME && !hero_has_creature(hero, CREATURE_WATER_LORD))
					|| (tome == ARTIFACT_EARTH_TOME && !hero_has_creature(hero, CREATURE_EARTH_LORD))

					) {
					// // Save.herospell[hero->Number].Tome[TomeID] = false;
					if (Save.herospell[hero->Number].Tome[TomeID]) 
					{
						for (int s = 0; s < 70; s++) {
							if (s == 57) continue;
							int school = spell_school(s);
							int schooltome = -1;
							if (school & 1) schooltome = ARTIFACT_AIR_TOME;
							if (school & 4) schooltome = ARTIFACT_WATER_TOME;
							if (school & 2) schooltome = ARTIFACT_FIRE_TOME;
							if (school & 8) schooltome = ARTIFACT_EARTH_TOME;
							if (school == 15) schooltome = -1;
							if (schooltome == tome) {

								// armageddons blade fix
								if (s == 26 && (hero_has_artifact(hero, 128) || hero_has_artifact(hero, 162))) { 
									Save.herospell[hero->Number].ArmageddonBlade = true;
									continue; }

								hero->Spell[s] = Save.herospell[hero->Number].Spell[s];
								hero->LSpell[s] = Save.herospell[hero->Number].LSpell[s];
							}
						}


						Save.herospell[hero->Number].Tome[TomeID] = false;
					}

					
				}

				if (Save.herospell[hero->Number].ArmageddonBlade &&
					!(hero_has_artifact(hero, 128) || hero_has_artifact(hero, 162))
					) {
					hero->Spell[26] = Save.herospell[hero->Number].Spell[26];
					hero->LSpell[26] = Save.herospell[hero->Number].LSpell[26];
					Save.herospell[hero->Number].ArmageddonBlade = false;
				}

			}

		}
		for (int s = 0; s < 70; s++) {
			if (s == 57) continue;
			int school = spell_school(s);
			int schooltome = -1;
			if (school & 1) schooltome = ARTIFACT_AIR_TOME;
			if (school & 4) schooltome = ARTIFACT_WATER_TOME;
			if (school & 2) schooltome = ARTIFACT_FIRE_TOME;
			if (school & 8) schooltome = ARTIFACT_EARTH_TOME;
			if (school == 15) schooltome = -1;

			int TomeID = tome - ARTIFACT_FIRST_TOME;

			/* if (enable) */ {

				if (school == 15) {
					//Save.herospell[hero->Number].Spell[s] = hero->Spell[s];
					//Save.herospell[hero->Number].LSpell[s] = hero->LSpell[s];

					hero->Spell[s] = true; hero->LSpell[s] = 4;
				}
				else { if(schooltome == tome /* && Save.herospell[hero->Number].Tome[TomeID] == false */ )
					if ((tome == ARTIFACT_AIR_TOME &&  hero_has_creature(hero, CREATURE_AIR_LORD))
						|| (tome == ARTIFACT_WATER_TOME && hero_has_creature(hero, CREATURE_WATER_LORD))
						|| (tome == ARTIFACT_FIRE_TOME && hero_has_creature(hero, CREATURE_FIRE_LORD))
						|| (tome == ARTIFACT_EARTH_TOME && hero_has_creature(hero, CREATURE_EARTH_LORD))
						) {
						// armageddons blade fix
						if (s == 26 && (hero_has_artifact(hero, 128) || hero_has_artifact(hero, 162))) continue;

						// Save.herospell[hero->Number].Spell[s] = hero->Spell[s];
						// Save.herospell[hero->Number].LSpell[s] = hero->LSpell[s];

						hero->Spell[s] = true; hero->LSpell[s] = 4;
						Save.herospell[hero->Number].Tome[TomeID] = true;
					}
				}
			}

			if (s == 26 && Save.herospell[hero->Number].ArmageddonBlade) {
				hero->Spell[s] = true; hero->LSpell[s] = 4;
				//Save.herospell[hero->Number].Tome[TomeID] = true;
			}

		}

		/*
		// armageddons blade fix
		if (hero_has_artifact(hero, 128) || hero_has_artifact(hero, 162) )
			{ hero->Spell[26] = true; hero->LSpell[26] = 4; }
		*/
		
		if (!enable) Save.ready = false;

	}
}

int __stdcall MonAsArtifact(HiHook* h, HERO* hero, int art)
{
	int ret = CALL_2(int, __thiscall, h->GetDefaultFunc(), hero, art);

	//int test = HasCreature(hero, CREATURE_AIR_LORD);
	//if (test) MessageBoxA(0, "AIR_LORD", "AIR_LORD", 0);
	// if (art == ARTIFACT_AIR_TOME) return true; // MessageBoxA(0, "AIR_TOME", "AIR_TOME", 0);
	//MessageBoxA(0, "MonAsArtifact", "MonAsArtifact", 0);
	//char text[255]; 
	//sprintf_s(text, "hero %i art %i",hero, art );
	//sprintf_s(text, "HasCreature %i", test);
	//MessageBoxA(0,  text, "MonAsArtifact", 0);

	//if (h->GetReturnAddress() < 0x700000 && h->GetReturnAddress() > 0x400000) //������� ������ SoD;
	{
		//if ( HasCreature(hero, CREATURE_AIR_LORD)) MessageBoxA(0, "AIR_LORD", "AIR_LORD", 0);
		//if ( art == ARTIFACT_AIR_TOME) MessageBoxA(0, "AIR_TOME", "AIR_TOME", 0);

		apply_spell_tome(hero, ARTIFACT_AIR_TOME);
		apply_spell_tome(hero, ARTIFACT_FIRE_TOME);
		apply_spell_tome(hero, ARTIFACT_WATER_TOME);
		apply_spell_tome(hero, ARTIFACT_EARTH_TOME);

		/*
		for (int c = 0; c < 7; c++) {
			int cre = hero->Ct[c];
			// if (cre == CREATURE_AIR_LORD) MessageBoxA(0, "AIR_LORD", "AIR_LORD", 0); //this test works
			// if (art == ARTIFACT_AIR_TOME && cre== CREATURE_AIR_LORD)  return true;
			// if (art == ARTIFACT_WATER_TOME && cre == CREATURE_WATER_LORD ) return true;
			// if (art == ARTIFACT_FIRE_TOME && cre == CREATURE_FIRE_LORD)  return true;
			// if (art == ARTIFACT_EARTH_TOME && cre == CREATURE_EARTH_LORD ) return true;
			if (cre == CREATURE_AIR_LORD) {
				apply_spell_tome(hero, ARTIFACT_AIR_TOME);

			};
			if (cre == CREATURE_WATER_LORD) {
				apply_spell_tome(hero, ARTIFACT_WATER_TOME);

			};
			if (cre == CREATURE_FIRE_LORD) {
				apply_spell_tome(hero, ARTIFACT_FIRE_TOME);

			};
			if (cre == CREATURE_EARTH_LORD) {
				apply_spell_tome(hero, ARTIFACT_EARTH_TOME);

			};

		}
		*/


		/*
		for (int i = 0; i != 19; i++)
		{
			if (art == ARTIFACT_SPELLBOOK && hero->IArt[i][0] != -1 &&  //���� ����� ����� �� �������, �� 
				GetArtifactRecord(hero->IArt[i][0])->rank & 0x40)	//�� ��������� ������� ���������� �� ���� ������� � ��������� ����� 
			{
				//sprintf(buf,"NewHasArtifact::Spellbook found at slot %i", i);
				//WriteConsoleA(GetStdHandle(STD_OUTPUT_HANDLE),(const void*)buf,64,0,0);
				return true;
			}


			if (art == ARTIFACT_ANGELWINGS && hero->IArt[i][0] != -1 &&
				IsArtifactAllowFly(hero->IArt[i][0], hero->IArt[i][1]))
			{
				return true;
			}


			if (art == ARTIFACT_WATERBOOTS && hero->IArt[i][0] != -1 &&
				IsArtifactAllowWaterWalk(hero->IArt[i][0], hero->IArt[i][1]))
			{
				return true;
			}
		}
		*/
	}

	return ret;
}


int __stdcall HeroHasMon2(HiHook* h, HERO* hero, int creature)
{
	int ret = CALL_2(int, __thiscall, h->GetDefaultFunc(), hero, creature);


	if (h->GetReturnAddress() < 0x700000 && h->GetReturnAddress() > 0x400000) //������� ������ SoD;
	{
		apply_spell_tome(hero, ARTIFACT_AIR_TOME);
		apply_spell_tome(hero, ARTIFACT_FIRE_TOME);
		apply_spell_tome(hero, ARTIFACT_WATER_TOME);
		apply_spell_tome(hero, ARTIFACT_EARTH_TOME);
	}

	return ret;
}


void __stdcall StoreData(PEvent e)
{
	Save.ready = false;
	WriteSavegameSection(sizeof(Save), (void*)&Save, PINSTANCE_MAIN);
}


void __stdcall RestoreData(PEvent e)
{
	CleanTomes();
	ReadSavegameSection(sizeof(Save), (void*)&Save, PINSTANCE_MAIN);
	Save.AI_turn = 0;
}


void __stdcall HookNewGame(PEvent e) {
	CleanTomes();
}


BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{

		globalPatcher = GetPatcher();
		ElemLord = globalPatcher->CreateInstance(PINSTANCE_MAIN);
		ConnectEra();

		CleanTomes();
		Save.ready = false;

		
		RegisterHandler(HookNewGame, "OnAfterErmInstructions");

		RegisterHandler(StoreData, "OnSavegameWrite");
		RegisterHandler(RestoreData, "OnSavegameRead");


		//RegisterHandler(MakeReady, "OnHeroMove");
		
		RegisterHandler(MakeReady_NonAI, "OnOpenHeroScreen");
		RegisterHandler(MakeReady,		 "OnBeforeBattleUniversal");
		RegisterHandler(MakeReady_NonAI, "OnBeforeAdventureMagic");
		RegisterHandler(MakeReady_NonAI, "OnEquipArt");
		RegisterHandler(MakeReady_NonAI, "OnUnequipArt");
		RegisterHandler(MakeReady_NonAI, "OnBeforeHeroInteraction");
		RegisterHandler(MakeReady_NonAI, "OnAfterHeroInteraction");
		

		
		RegisterHandler(MakeReady_NonAI, "OnBeforeVisitObject 98/0");
		RegisterHandler(MakeReady_NonAI, "OnAfterVisitObject 98/0");
		RegisterHandler(MakeReady_NonAI, "OnBeforeVisitObject 98/1");
		RegisterHandler(MakeReady_NonAI, "OnAfterVisitObject 98/1");
		RegisterHandler(MakeReady_NonAI, "OnBeforeVisitObject 98/2");
		RegisterHandler(MakeReady_NonAI, "OnAfterVisitObject 98/2");
		RegisterHandler(MakeReady_NonAI, "OnBeforeVisitObject 98/3");
		RegisterHandler(MakeReady_NonAI, "OnAfterVisitObject 98/3");
		RegisterHandler(MakeReady_NonAI, "OnBeforeVisitObject 98/4");
		RegisterHandler(MakeReady_NonAI, "OnAfterVisitObject 98/4");
		RegisterHandler(MakeReady_NonAI, "OnBeforeVisitObject 98/5");
		RegisterHandler(MakeReady_NonAI, "OnAfterVisitObject 98/5");
		RegisterHandler(MakeReady_NonAI, "OnBeforeVisitObject 98/6");
		RegisterHandler(MakeReady_NonAI, "OnAfterVisitObject 98/6");
		RegisterHandler(MakeReady_NonAI, "OnBeforeVisitObject 98/7");
		RegisterHandler(MakeReady_NonAI, "OnAfterVisitObject 98/7");
		RegisterHandler(MakeReady_NonAI, "OnBeforeVisitObject 98/8");
		RegisterHandler(MakeReady_NonAI, "OnAfterVisitObject 98/8");
		

		
		RegisterHandler(MakeReady, "OnBeforeVisitObject 219/0");
		RegisterHandler(MakeReady, "OnAfterVisitObject 219/0");
		RegisterHandler(MakeReady, "OnBeforeVisitObject 33/0");
		RegisterHandler(MakeReady, "OnAfterVisitObject 33/0");
		RegisterHandler(MakeReady, "OnBeforeVisitObject 33/1");
		RegisterHandler(MakeReady, "OnAfterVisitObject 33/1");

		RegisterHandler(MakeReady, "OnBeforeVisitObject 26/0");
		RegisterHandler(MakeReady, "OnAfterVisitObject 26/0");
		
		RegisterHandler(CheckAI, "OnErmTimer 2");


		RegisterHandler(MakeNonReady, "OnGameEnter");
		RegisterHandler(MakeNonReady, "OnGameLeave");

		ElemLord->WriteHiHook(0x4D9460, SPLICE_, EXTENDED_, THISCALL_, (void*)MonAsArtifact); // HeroHasArtifact
		// ElemLord->WriteHiHook(0x44AA90, SPLICE_, EXTENDED_, THISCALL_, (void*)HeroHasMon2); // HeroHasMonster


	}
	return TRUE;
}
