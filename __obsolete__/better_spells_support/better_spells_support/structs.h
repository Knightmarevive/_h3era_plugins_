#pragma once

#define SPELLNUM 200
#define SPELLNUM_0 81

struct _New_Spell_ { // size:1d8h
	int   FRposNOzerENneg; // +0 -1=enemy, 0=square/global/area, 1=friend
	char* SoundFileName;   // +4 
	int   DefIndex;        // +8 used to get DefName
	int   Flags;            // +C
  //0x00000001 - BF spell
  //0x00000002 - MAP spell
  //0x00000004 - Has a time scale (3 rounds or so)
  //0x00000008 - Creature Spell
  //0x00000010 - target - single stack
  //0x00000020 - target - single shooting stack (???)
  //0x00000040 - has a mass version at expert level
  //0x00000080 - target - any location
  //0x00000100 - 
  //0x00000200 - 
  //0x00000400 - Mind spell
  //0x00000800 - friendly and has mass version
  //0x00001000 - cannot be cast on SIEGE_WEAPON
  //0x00002000 - Spell from Artifact
  //0x00004000 -
  //0x00008000 - AI 
  //0x00010000 - AI area effect
  //0x00020000 - AI
  //0x00040000 - AI
  //0x00080000 - AI number/ownership of stacks may be changed 
  //0x00100000 - AI
	char* Name;				   // +10h
	char* AbbrName;		       // +14h
	int    Level;	          // +18h
	int    SchoolBits;			// +1Ch Air=1,Fire=2,Water=4,Earth=8
	int    old_Cost[4];		    // +20h cost mana per skill level
	int    Eff_Power;		   // +30h
	int    old_Effect[4];       // +34h effect per skill level
	int    Chance2GetVar[9];	// +44h chance per class
	int    old_AIValue[4];      // +68h 
	char* old_Descript[4];		// +78h
	int    new_Cost[16];		// +88h
	int   new_Effect[16];			// +C8h
	int   new_AIValue[16];			// +108h
	char* new_Descript[16];		   // +148h
	char  internal_school_level[16];// +188h
	int   mimic_another_spell[16]; // +198h
	//+1d8h
};

constexpr long _0x20 = 0x0088;
constexpr long _0x34 = 0x00C8;
constexpr long _0x68 = 0x0108;
constexpr long _0x78 = 0x0148;

constexpr auto _new_spells_size_ = sizeof(_New_Spell_);

struct _old_Spell_ { // size:88h
	int   FRposNOzerENneg; // +0 -1=enemy, 0=square/global/area, 1=friend
	char* SoundFileName;   // +4 
	int   DefIndex;        // +8 used to get DefName
	int   Flags;            // +C
  //0x00000001 - BF spell
  //0x00000002 - MAP spell
  //0x00000004 - Has a time scale (3 rounds or so)
  //0x00000008 - Creature Spell
  //0x00000010 - target - single stack
  //0x00000020 - target - single shooting stack (???)
  //0x00000040 - has a mass version at expert level
  //0x00000080 - target - any location
  //0x00000100 - 
  //0x00000200 - 
  //0x00000400 - Mind spell
  //0x00000800 - friendly and has mass version
  //0x00001000 - cannot be cast on SIEGE_WEAPON
  //0x00002000 - Spell from Artifact
  //0x00004000 -
  //0x00008000 - AI 
  //0x00010000 - AI area effect
  //0x00020000 - AI
  //0x00040000 - AI
  //0x00080000 - AI number/ownership of stacks may be changed 
  //0x00100000 - AI
	char* Name;            // +10h
	char* AbbrName;        // +14h
	int    Level;           // +18h
	int    SchoolBits;      // +1Ch Air=1,Fire=2,Water=4,Earth=8
	int    Cost[4];         // +20h cost mana per skill level
	int    Eff_Power;       // +30h
	int    Effect[4];       // +34h effect per skill level
	int    Chance2GetVar[9];// +44h chance per class
	int    AIValue[4];      // +68h 
	char* Descript[4];     // +78h
};


constexpr auto _old_spells_size_ = sizeof(_old_Spell_);