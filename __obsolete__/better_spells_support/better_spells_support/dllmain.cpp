// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "structs.h"
#include "era.h"
#include "H3API/lib/H3API.hpp"
#include "H3API/lib/patcher_x86.hpp"
#define PINSTANCE_MAIN "Better_spells_support"

Patcher* globalPatcher;
PatcherInstance* SpellZ;

_New_Spell_ new_spells[SPELLNUM];
_New_Spell_ new_spells_backup[SPELLNUM];

int spells_wog_ptr = 0x7BD2C0;
int spells_vanilla_ptr = 0x006854A0;

void copy_vanilla(void) {
    _old_Spell_* spells_old = ((_old_Spell_*)spells_vanilla_ptr);
    for (int i = 0; i < SPELLNUM_0; ++i) {
        memset((void*)&new_spells[i],0,_new_spells_size_);
        memcpy((void*)&new_spells[i], (void*)&(spells_old[i]), _old_spells_size_); 
        /*
        for (int j = 0; j < 4; ++j) {
            new_spells[i].new_AIValue[j] = new_spells[i].old_AIValue[j];
            new_spells[i].new_Cost[j] = new_spells[i].old_Cost[j];
            new_spells[i].new_Effect[j] = new_spells[i].old_Effect[j];
            new_spells[i].new_Descript[j] = new_spells[i].old_Descript[j];
        }
        */
    }
}

_LHF_(hook_004255F8) {
    c->eax *= _new_spells_size_ / 8;
    c->return_address = 0x004255FD;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00425400) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x00425405;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00425282) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x00425287;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00424D44) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x00424D49;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00423D59) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x00423D5E;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00423CAD) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x00423CB2;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0041C567) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x0041C56C;
    return NO_EXEC_DEFAULT;
}

_LHF_ (hook_0040D91A) {
    c->eax *= _new_spells_size_ / 8;
    c->return_address = 0x0040D91F;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_007149D5) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x007149DA;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004F5940) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004F5945;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0059E45F) {
    c->eax *= _new_spells_size_ / 8;
    c->return_address = 0x0059E464;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_005CE6A6) {
    c->eax *= _new_spells_size_ / 8;
    c->return_address = 0x005CE6AB;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_005BEB20) {
    c->ebx += _new_spells_size_;
    ++(c->esi);
    c->ecx += _new_spells_size_/4;
    c->return_address = 0x005BEB2A;
    return NO_EXEC_DEFAULT;
}


_LHF_(hook_005BEBF9) {
    c->edi += _new_spells_size_;
    ++(c->esi);
    c->edx += _new_spells_size_ / 4;
    c->return_address = 0x005BEC03;
    return NO_EXEC_DEFAULT;
}


_LHF_(hook_00425758) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0042575D;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004258F4) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x004258F9;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00425A20) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x00425A25;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00425B90) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x00425B95;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00425D63) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x00425D68;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00425F35) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x00425F3A;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00425F95) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x00425F9A;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0042605B) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x00426060;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_00427055) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x0042705A;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00436584) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x00436589;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00439308) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0043930D;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00439658) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0043965D;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00439698) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0043969D;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0043B988) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0043B98D;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00484190) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x00484195;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004A01FE) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x004A0203;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004A26C2) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x004A26C7;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004A29C9) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x004A29CE;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004AFA57) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x004AFA5C;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004AFC05) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x004AFC0A;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_005A01A3) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x005A01A8;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_005A8B96) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x005A8B9B;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_005F52EC) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x005F52F1;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0059DEF2) {
    c->esi *= _new_spells_size_ / 8;
    c->return_address = 0x0059DEF7;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_0059DEFC) {
    c->edi *= _new_spells_size_ / 8;
    c->return_address = 0x0059DF01;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004B018F) {
    c->edi *= _new_spells_size_ / 8;
    c->return_address = 0x004B0194;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00501282) {
    c->edi *= _new_spells_size_ / 8;
    c->return_address = 0x00501287;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0043B64A) {
    c->edi *= _new_spells_size_ / 8;
    c->return_address = 0x0043B64F;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00439D91) {
    c->edi *= _new_spells_size_ / 8;
    c->return_address = 0x00439D96;
    return NO_EXEC_DEFAULT;
}


_LHF_(hook_004769CA) {
    c->edx *= _new_spells_size_ / 8;
    c->eax = *(int*)(c->ecx+0x20);
    c->ecx = *(int*)0x00687FA8;
    c->return_address = 0x004769D8;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_0059E013) {
    c->edx *= _new_spells_size_ / 8;
    c->ebx *= _new_spells_size_ / 8;
    c->ecx = *(int*) 0x00687FA8;
    c->return_address = 0x0059E023;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004C16CF) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004C16D4;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_004AFED2) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004AFED7;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004AFF9B) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004AFFA0;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004AFD6F) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004AFD74;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004DB785) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004DB78A;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_005A8CB6) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x005A8CBB;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_005A750E) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x005A7513;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_005A1C59) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x005A1C5E;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_005A4DB9) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x005A4DBE;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_005CE903) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x005CE908;
    return NO_EXEC_DEFAULT;
}


_LHF_(hook_005A1C4F) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x005A1C54;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_005A0E32) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x005A0E37;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0059EE13) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x0059EE18;
    return NO_EXEC_DEFAULT;
}


_LHF_(hook_0059E0C9) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x0059E0CE;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0059E0D6) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x0059E0DB;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00439A70) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x00439A75;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0043AC30) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x0043AC35;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0043A51A) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x0043A51F;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004398D6) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004398DB;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004A2BF4) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004A2BF9;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004A5467) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004A546C;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004A52CF) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004A52D4;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004A4A76) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004A4A7B;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004A270B) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x004A2710;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00483DC0) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x00483DC5;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00483D70) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x00483D75;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00447D5A) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x00447D5F;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00527B85) {
    c->ecx *= _new_spells_size_ / 8;
    c->return_address = 0x00527B8A;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004AFCD5) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x004AFCDA;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_004AFCBD) {
    c->ebx *= _new_spells_size_ / 8;
    c->return_address = 0x004AFCC2;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004B0171) {
    c->ebx *= _new_spells_size_ / 8;
    c->return_address = 0x004B0176;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_005A83CE) {
    c->ebx *= _new_spells_size_ / 8;
    c->return_address = 0x005A83D3;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_005A87F4) {
    c->ebx *= _new_spells_size_ / 8;
    c->return_address = 0x005A87F9;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0059D0D0) {
    c->ebx *= _new_spells_size_ / 8;
    c->return_address = 0x0059D0D5;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0059D1A6) {
    c->ebx *= _new_spells_size_ / 8;
    c->return_address = 0x0059D1AB;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_004A3FE3) {
    c->ebx *= _new_spells_size_ / 8;
    c->return_address = 0x004A3FE8;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0059BDC6) {
    c->ebx *= _new_spells_size_ / 8;
    c->return_address = 0x0059BDCB;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004E551F) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x004E5524;
    return NO_EXEC_DEFAULT;
}



_LHF_(hook_005A0E42) {
    c->esi *= _new_spells_size_ / 8;
    c->return_address = 0x005A0E47;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00586BFB) {
    c->esi *= _new_spells_size_ / 8;
    c->return_address = 0x00586C00;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0059E29E) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0059E2A3;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0058D1A0) {
    c->esi *= _new_spells_size_ / 8;
    c->return_address = 0x0058D1A5;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_005275D3) {
    c->esi *= _new_spells_size_ / 8;
    c->return_address = 0x005275D8;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_004AFD79) {
    c->esi *= _new_spells_size_ / 8;
    c->return_address = 0x004AFD7E;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00447350) {
    c->esi *= _new_spells_size_ / 8;
    c->return_address = 0x00447355;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0044A1C2) {
    c->esi *= _new_spells_size_ / 8;
    c->return_address = 0x0044A1C7;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_00436BBC) {
    c->edx *= _new_spells_size_ / 8;
    c->Push(c->edi);
    c->edi = c->eax * _new_spells_size_ / 8;
    c->return_address = 0x00436BC7;
    return NO_EXEC_DEFAULT;
}


_LHF_(hook_0043953F) {
    c->eax += _new_spells_size_ / 4;
    c->esi += _new_spells_size_ / 1;
    c->return_address = 0x00439548;
    return NO_EXEC_DEFAULT;
}


_LHF_(hook_0043A9E0) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0043A9E5;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0043A9F5) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0043A9FA;
    return NO_EXEC_DEFAULT;
}



_LHF_(hook_0043B7C5) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0043B7CA;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_0043B7DB) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0043B7E0;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_0043B9E0) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0043B9E5;
    return NO_EXEC_DEFAULT;
}




_LHF_(hook_0043BA93) {
    c->eax *= _new_spells_size_ / 8;
    c->return_address = 0x0043BA98;
    return NO_EXEC_DEFAULT;
}



_LHF_(hook_0043C2A4) {
    c->eax *= _new_spells_size_ / 8;
    c->return_address = 0x0043C2A9;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_0043C2BA) {
    c->eax *= _new_spells_size_ / 8;
    c->return_address = 0x0043C2BF;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_0043C2CF) {
    c->eax *= _new_spells_size_ / 8;
    c->return_address = 0x0043C2D4;
    return NO_EXEC_DEFAULT;
}
_LHF_(hook_0043C2E5) {
    c->eax *= _new_spells_size_ / 8;
    c->return_address = 0x0043C2EA;
    return NO_EXEC_DEFAULT;
}



_LHF_(hook_0043C340) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0043C345;
    return NO_EXEC_DEFAULT;
}

_LHF_(hook_0043C350) {
    c->edx *= _new_spells_size_ / 8;
    c->return_address = 0x0043C355;
    return NO_EXEC_DEFAULT;
}



_LHF_(hook_0043C3D0) {
    c->eax *= _new_spells_size_ / 8;
    c->edi = c->esi *= _new_spells_size_ / 8;
    c->edx = c->ecx + 2 * c->eax;

    c->eax = *(int*)0x00687FA8;

    c->return_address = 0x0043C3E4;
    return NO_EXEC_DEFAULT;
}


void __stdcall patch_spells(void) {
    SpellZ->WriteDword((_ptr_)0x004396E1 + 1, _new_spells_size_ * 10);
    SpellZ->WriteDword((_ptr_)0x0043C0FF + 1, _new_spells_size_ * 10);
    SpellZ->WriteDword((_ptr_)0x00447525 + 1, _new_spells_size_ * 10);
    SpellZ->WriteDword((_ptr_)0x00447C4D + 1, _new_spells_size_ * 10);
    SpellZ->WriteDword((_ptr_)0x00447C9A + 1, _new_spells_size_ * 10);
    SpellZ->WriteDword((_ptr_)0x005A1A9B + 1, _new_spells_size_ * 10);
    SpellZ->WriteDword((_ptr_)0x005A850D + 2, _new_spells_size_ * 10);
    SpellZ->WriteDword((_ptr_)0x00425C65 + 3, _new_spells_size_ * 10);
    //SpellZ->WriteDword((_ptr_)0x005943ED + 2, _new_spells_size_ * 10);

    //
    SpellZ->WriteDword((_ptr_)0x007757DD + 1, _new_spells_size_ / 4);
    SpellZ->WriteDword((_ptr_)0x0077596C + 1, _new_spells_size_ / 4);
    //copy_vanilla();
    SpellZ->WriteDword((_ptr_)0x007757D7 + 2, (char*)new_spells);
    SpellZ->WriteDword((_ptr_)0x00775957 + 2, (char*)new_spells);
    SpellZ->WriteDword((_ptr_)0x007757C8 + 2, (char*)new_spells_backup);
    SpellZ->WriteDword((_ptr_)0x00775966 + 2, (char*)new_spells_backup);

    SpellZ->WriteDword((_ptr_)0x59E467 + 3, (char*)new_spells);
    SpellZ->WriteDword((_ptr_)0x687FA8 + 0, (char*)new_spells);
    SpellZ->WriteLoHook((_ptr_)0x0059E45F, hook_0059E45F);
    SpellZ->WriteLoHook((_ptr_)0x005CE6A6, hook_005CE6A6);
    SpellZ->WriteLoHook((_ptr_)0x005BEB20, hook_005BEB20);
    SpellZ->WriteLoHook((_ptr_)0x005BEBF9, hook_005BEBF9);
    SpellZ->WriteLoHook((_ptr_)0x004F5940, hook_004F5940);
    SpellZ->WriteLoHook((_ptr_)0x007149D5, hook_007149D5);
    // 0x0071DE98, 0x0075C3D5, 0x0075D9C3
    SpellZ->WriteLoHook((_ptr_)0x0040D91A, hook_0040D91A);
    SpellZ->WriteLoHook((_ptr_)0x0041C567, hook_0041C567);
    SpellZ->WriteLoHook((_ptr_)0x00423CAD, hook_00423CAD);
    SpellZ->WriteLoHook((_ptr_)0x00423D59, hook_00423D59);
    SpellZ->WriteLoHook((_ptr_)0x00424D44, hook_00424D44);
    SpellZ->WriteLoHook((_ptr_)0x00425282, hook_00425282);
    SpellZ->WriteLoHook((_ptr_)0x00425400, hook_00425400);
    SpellZ->WriteLoHook((_ptr_)0x004255F8, hook_004255F8);
    SpellZ->WriteLoHook((_ptr_)0x00425758, hook_00425758);
    SpellZ->WriteLoHook((_ptr_)0x004258F4, hook_004258F4);
    SpellZ->WriteLoHook((_ptr_)0x00425A20, hook_00425A20);
    SpellZ->WriteLoHook((_ptr_)0x00425B90, hook_00425B90);
    SpellZ->WriteLoHook((_ptr_)0x00425D63, hook_00425D63);
    SpellZ->WriteLoHook((_ptr_)0x00425F35, hook_00425F35);
    SpellZ->WriteLoHook((_ptr_)0x00425F95, hook_00425F95);
    SpellZ->WriteLoHook((_ptr_)0x0042605B, hook_0042605B);
    SpellZ->WriteLoHook((_ptr_)0x00427055, hook_00427055);
    SpellZ->WriteLoHook((_ptr_)0x00436584, hook_00436584);
    SpellZ->WriteLoHook((_ptr_)0x00439308, hook_00439308);
    SpellZ->WriteLoHook((_ptr_)0x00439658, hook_00439658);
    SpellZ->WriteLoHook((_ptr_)0x00439698, hook_00439698);
    SpellZ->WriteLoHook((_ptr_)0x0043B988, hook_0043B988);
    SpellZ->WriteLoHook((_ptr_)0x00484190, hook_00484190);
    SpellZ->WriteLoHook((_ptr_)0x004A01FE, hook_004A01FE);
    SpellZ->WriteLoHook((_ptr_)0x004A26C2, hook_004A26C2);
    SpellZ->WriteLoHook((_ptr_)0x004A29C9, hook_004A29C9);
    SpellZ->WriteLoHook((_ptr_)0x004AFA57, hook_004AFA57);
    SpellZ->WriteLoHook((_ptr_)0x004AFC05, hook_004AFC05);
    SpellZ->WriteLoHook((_ptr_)0x005A01A3, hook_005A01A3);
    SpellZ->WriteLoHook((_ptr_)0x005A8B96, hook_005A8B96);
    SpellZ->WriteLoHook((_ptr_)0x005F52EC, hook_005F52EC);
    SpellZ->WriteLoHook((_ptr_)0x0059DEF2, hook_0059DEF2);
    SpellZ->WriteLoHook((_ptr_)0x0059DEFC, hook_0059DEFC);
    SpellZ->WriteLoHook((_ptr_)0x004B018F, hook_004B018F);
    SpellZ->WriteLoHook((_ptr_)0x00501282, hook_00501282);
    SpellZ->WriteLoHook((_ptr_)0x0043B64A, hook_0043B64A);
    SpellZ->WriteLoHook((_ptr_)0x00439D91, hook_00439D91);
    SpellZ->WriteLoHook((_ptr_)0x004C16CF, hook_004C16CF);
    SpellZ->WriteLoHook((_ptr_)0x004AFED2, hook_004AFED2);
    SpellZ->WriteLoHook((_ptr_)0x004AFF9B, hook_004AFF9B);
    SpellZ->WriteLoHook((_ptr_)0x004AFD6F, hook_004AFD6F);
    SpellZ->WriteLoHook((_ptr_)0x004DB785, hook_004DB785);
    SpellZ->WriteLoHook((_ptr_)0x005A8CB6, hook_005A8CB6);
    SpellZ->WriteLoHook((_ptr_)0x005A750E, hook_005A750E);
    SpellZ->WriteLoHook((_ptr_)0x005A1C59, hook_005A1C59);
    SpellZ->WriteLoHook((_ptr_)0x005A4DB9, hook_005A4DB9);
    SpellZ->WriteLoHook((_ptr_)0x005CE903, hook_005CE903);
    SpellZ->WriteLoHook((_ptr_)0x005A1C4F, hook_005A1C4F);
    SpellZ->WriteLoHook((_ptr_)0x005A0E32, hook_005A0E32);
    SpellZ->WriteLoHook((_ptr_)0x0059EE13, hook_0059EE13);
    SpellZ->WriteLoHook((_ptr_)0x0059E0C9, hook_0059E0C9);
    SpellZ->WriteLoHook((_ptr_)0x0059E0D6, hook_0059E0D6);
    SpellZ->WriteLoHook((_ptr_)0x00439A70, hook_00439A70);
    SpellZ->WriteLoHook((_ptr_)0x0043AC30, hook_0043AC30);
    SpellZ->WriteLoHook((_ptr_)0x0043A51A, hook_0043A51A);
    SpellZ->WriteLoHook((_ptr_)0x004398D6, hook_004398D6);
    SpellZ->WriteLoHook((_ptr_)0x004A2BF4, hook_004A2BF4);
    SpellZ->WriteLoHook((_ptr_)0x004A5467, hook_004A5467);
    SpellZ->WriteLoHook((_ptr_)0x004A52CF, hook_004A52CF);
    SpellZ->WriteLoHook((_ptr_)0x004A4A76, hook_004A4A76);
    SpellZ->WriteLoHook((_ptr_)0x004A270B, hook_004A270B);
    SpellZ->WriteLoHook((_ptr_)0x00483DC0, hook_00483DC0);
    SpellZ->WriteLoHook((_ptr_)0x00483D70, hook_00483D70);
    SpellZ->WriteLoHook((_ptr_)0x00447D5A, hook_00447D5A);
    SpellZ->WriteLoHook((_ptr_)0x00527B85, hook_00527B85);
    SpellZ->WriteLoHook((_ptr_)0x004AFCD5, hook_004AFCD5);
    SpellZ->WriteLoHook((_ptr_)0x004AFCBD, hook_004AFCBD);
    SpellZ->WriteLoHook((_ptr_)0x004B0171, hook_004B0171);
    SpellZ->WriteLoHook((_ptr_)0x005A83CE, hook_005A83CE);
    SpellZ->WriteLoHook((_ptr_)0x005A87F4, hook_005A87F4);
    SpellZ->WriteLoHook((_ptr_)0x0059D0D0, hook_0059D0D0);
    SpellZ->WriteLoHook((_ptr_)0x0059E013, hook_0059E013);
    SpellZ->WriteLoHook((_ptr_)0x004769CA, hook_004769CA);
    SpellZ->WriteLoHook((_ptr_)0x0059D1A6, hook_0059D1A6);
    SpellZ->WriteLoHook((_ptr_)0x004A3FE3, hook_004A3FE3);
    SpellZ->WriteLoHook((_ptr_)0x0059BDC6, hook_0059BDC6);
    SpellZ->WriteLoHook((_ptr_)0x004E551F, hook_004E551F);
    SpellZ->WriteLoHook((_ptr_)0x005A0E42, hook_005A0E42);
    SpellZ->WriteLoHook((_ptr_)0x0059E29E, hook_0059E29E);
    SpellZ->WriteLoHook((_ptr_)0x00586BFB, hook_00586BFB);
    SpellZ->WriteLoHook((_ptr_)0x0058D1A0, hook_0058D1A0);
    SpellZ->WriteLoHook((_ptr_)0x005275D3, hook_005275D3);
    SpellZ->WriteLoHook((_ptr_)0x004AFD79, hook_004AFD79);
    SpellZ->WriteLoHook((_ptr_)0x00447350, hook_00447350);
    SpellZ->WriteLoHook((_ptr_)0x0044A1C2, hook_0044A1C2);
    SpellZ->WriteLoHook((_ptr_)0x00436BBC, hook_00436BBC);
    SpellZ->WriteLoHook((_ptr_)0x0043A9E0, hook_0043A9E0);
    SpellZ->WriteLoHook((_ptr_)0x0043A9F5, hook_0043A9F5);
    SpellZ->WriteLoHook((_ptr_)0x0043B7C5, hook_0043B7C5);
    SpellZ->WriteLoHook((_ptr_)0x0043B7DB, hook_0043B7DB);
    SpellZ->WriteLoHook((_ptr_)0x0043B9E0, hook_0043B9E0);
    SpellZ->WriteLoHook((_ptr_)0x0043BA93, hook_0043BA93);
    SpellZ->WriteLoHook((_ptr_)0x0043C2A4, hook_0043C2A4);
    SpellZ->WriteLoHook((_ptr_)0x0043C2BA, hook_0043C2BA);
    SpellZ->WriteLoHook((_ptr_)0x0043C2CF, hook_0043C2CF);
    SpellZ->WriteLoHook((_ptr_)0x0043C2E5, hook_0043C2E5);
    SpellZ->WriteLoHook((_ptr_)0x0043C340, hook_0043C340);
    SpellZ->WriteLoHook((_ptr_)0x0043C350, hook_0043C350);
    /////
    SpellZ->WriteLoHook((_ptr_)0x0043953F, hook_0043953F);
    SpellZ->WriteDword((_ptr_)0x00439548 + 1, _new_spells_size_ * 70 / 4);
    /////
    SpellZ->WriteLoHook((_ptr_)0x0043C3D0, hook_0043C3D0);
      


    SpellZ->WriteDword((_ptr_)0x0041CF96 + 1, _new_spells_size_);

    //SpellZ->WriteDword((_ptr_)0x492B59 + 2, (char*)new_spells);
    SpellZ->WriteDword((_ptr_)0x492B5F + 2, _new_spells_size_);

    //SpellZ->WriteDword((_ptr_)0x00492BA0 + 2, (char*)new_spells);
    SpellZ->WriteDword((_ptr_)0x00492B9A + 2, _new_spells_size_);

    //SpellZ->WriteDword((_ptr_)0x00492C2B + 2, (char*)new_spells);
    SpellZ->WriteDword((_ptr_)0x00492C37 + 2, _new_spells_size_*32+0x10);

    /////
    // AI_Battle_GetXXX functions
    SpellZ->WriteDword((_ptr_)0x00436E0D + 2, _new_spells_size_ * 19 + 0x30);
    SpellZ->WriteDword((_ptr_)0x00436E13 + 3, _new_spells_size_ * 19 + 0x34);
    //
    SpellZ->WriteDword((_ptr_)0x004370CD + 3, _new_spells_size_ * 41 + 0x34);
    SpellZ->WriteDword((_ptr_)0x00437D92 + 3, _new_spells_size_ * 43 + 0x34);
    SpellZ->WriteDword((_ptr_)0x00437F8B + 3, _new_spells_size_ * 50 + 0x34);
    SpellZ->WriteDword((_ptr_)0x0043812A + 3, _new_spells_size_ * 51 + 0x34);
    SpellZ->WriteDword((_ptr_)0x00438725 + 3, _new_spells_size_ * 48 + 0x34);
    SpellZ->WriteDword((_ptr_)0x004387E2 + 3, _new_spells_size_ * 44 + 0x34);
    SpellZ->WriteDword((_ptr_)0x00438858 + 3, _new_spells_size_ * 28 + 0x34);
    SpellZ->WriteDword((_ptr_)0x004388F8 + 3, _new_spells_size_ * 27 + 0x34);
    SpellZ->WriteDword((_ptr_)0x004389AF + 3, _new_spells_size_ * 55 + 0x34);
    SpellZ->WriteDword((_ptr_)0x004389EC + 3, _new_spells_size_ * 46 + 0x34);
    SpellZ->WriteDword((_ptr_)0x00438A87 + 3, _new_spells_size_ * 47 + 0x34);
    SpellZ->WriteDword((_ptr_)0x00438B72 + 3, _new_spells_size_ * 45 + 0x34);
    SpellZ->WriteDword((_ptr_)0x00438C00 + 3, _new_spells_size_ * 52 + 0x34);
    SpellZ->WriteDword((_ptr_)0x00438DAF + 3, _new_spells_size_ * 62 + 0x34);
    SpellZ->WriteDword((_ptr_)0x00438F62 + 3, _new_spells_size_ * 54 + 0x34);
    SpellZ->WriteDword((_ptr_)0x004395FC + 3, _new_spells_size_ * 30 + 0x34);
    SpellZ->WriteDword((_ptr_)0x0043962C + 3, _new_spells_size_ * 31 + 0x34);
    //
    SpellZ->WriteDword((_ptr_)0x004398E7 + 3, _new_spells_size_ * 37 + 0x30);
    //
    SpellZ->WriteDword((_ptr_)0x004399E9 + 3, _new_spells_size_ * 34 + 0x34);
    SpellZ->WriteDword((_ptr_)0x00439B23 + 3, _new_spells_size_ * 58 + 0x34);
    SpellZ->WriteDword((_ptr_)0x0043B022 + 3, _new_spells_size_ * 42 + 0x34);
    SpellZ->WriteDword((_ptr_)0x0043C08E + 3, _new_spells_size_ * 43 + 0x34);
    //////
    
    //AdvMapSPell
    SpellZ->WriteDword((_ptr_)0x0041D05D + 3, _new_spells_size_ * 1 + 0x34);
    SpellZ->WriteDword((_ptr_)0x0041D235 + 2, _new_spells_size_ * 8 + 0x00);
    SpellZ->WriteDword((_ptr_)0x0041D50C + 1, _new_spells_size_ * 9 + 0x00);

    //BattleStack_Shot
    SpellZ->WriteDword((_ptr_)0x0043F73B + 2, _new_spells_size_ * 21 + 0x08);
    SpellZ->WriteDword((_ptr_)0x0043F76A + 2, _new_spells_size_ * 21 + 0x04);
    SpellZ->WriteDword((_ptr_)0x0043FB2D + 2, _new_spells_size_ * 76 + 0x08);
    SpellZ->WriteDword((_ptr_)0x0043FB59 + 2, _new_spells_size_ * 76 + 0x04);

    ///BattleStack_After_Attack_Abilities
    SpellZ->WriteDword((_ptr_)0x00440E40 + 2, _new_spells_size_ * 79 + 0x04);
    SpellZ->WriteDword((_ptr_)0x00441058 + 2, _new_spells_size_ * 17 + 0x10);
    SpellZ->WriteDword((_ptr_)0x00441234 + 2, _new_spells_size_ * 80 + 0x04);
    SpellZ->WriteDword((_ptr_)0x0044125F + 2, _new_spells_size_ * 80 + 0x10);

    ///BattleStack_After_Attack_Penalties
    SpellZ->WriteDword((_ptr_)0x00443A0E + 2, _new_spells_size_ * 62 + 0x3C);
    SpellZ->WriteDword((_ptr_)0x00443A8A + 2, _new_spells_size_ * 62 + 0x3C);
  
    //BattleStack_UseCureSpell
    SpellZ->WriteDword((_ptr_)0x00446305 + 2, _new_spells_size_ * 37 + 0x30);
    SpellZ->WriteDword((_ptr_)0x0044630B + 3, _new_spells_size_ * 37 + 0x34);

    // MISC 1
    SpellZ->WriteDword((_ptr_)0x00447525 + 1, _new_spells_size_ * 10);
    SpellZ->WriteDword((_ptr_)0x0044853A + 2, _new_spells_size_ * 36 + 0x34);
    SpellZ->WriteDword((_ptr_)0x00469173 + 2, _new_spells_size_ * 38 + 0x04);
    SpellZ->WriteDword((_ptr_)0x00492107 + 2, _new_spells_size_ * 43 + 0x10);

    //BattleMgr_Hint_Proc
    SpellZ->WriteDword((_ptr_)0x00492BF8 + 2, _new_spells_size_ * 30 + 0x10);
    SpellZ->WriteDword((_ptr_)0x00492C37 + 2, _new_spells_size_ * 32 + 0x10);
    SpellZ->WriteDword((_ptr_)0x00492C86 + 2, _new_spells_size_ * 31 + 0x10);
    SpellZ->WriteDword((_ptr_)0x00492CC5 + 2, _new_spells_size_ * 33 + 0x10);
    SpellZ->WriteDword((_ptr_)0x00492D4E + 2, _new_spells_size_ * 43 + 0x10);

    //MISC 2
    SpellZ->WriteDword((_ptr_)0x004E5CA3 + 3, _new_spells_size_ * 6 + 0x10);
    SpellZ->WriteDword((_ptr_)0x004E60D5 + 3, _new_spells_size_ * 2 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005275F0 + 2, _new_spells_size_ * 57 + 0x34);

    //BattleMgr_CastSpell
    SpellZ->WriteDword((_ptr_)0x005A05A5 + 2, _new_spells_size_ * 36 + 0x08);
    SpellZ->WriteDword((_ptr_)0x005A0856 + 2, _new_spells_size_ * 11 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A085C + 3, _new_spells_size_ * 11 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005A0B3C + 2, _new_spells_size_ * 13 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A0B42 + 3, _new_spells_size_ * 13 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005A0CDB + 2, _new_spells_size_ * 15 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A0CE1 + 3, _new_spells_size_ * 15 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005A0D81 + 2, _new_spells_size_ * 16 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A0D87 + 3, _new_spells_size_ * 16 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005A0EB4 + 2, _new_spells_size_ * 18 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A0EBA + 3, _new_spells_size_ * 18 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005A103D + 2, _new_spells_size_ * 24 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A1043 + 3, _new_spells_size_ * 24 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005A10FF + 2, _new_spells_size_ * 24 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A1105 + 3, _new_spells_size_ * 24 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005A123A + 2, _new_spells_size_ * 25 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A1240 + 3, _new_spells_size_ * 25 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005A1323 + 2, _new_spells_size_ * 25 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A1329 + 3, _new_spells_size_ * 25 + 0x34);
    ////
 
    //BattleMgr_Cast_Armageddon_0x5A4ED0
    SpellZ->WriteDword((_ptr_)0x005A4EDE + 1, _new_spells_size_ * 26 + 0x00);
    SpellZ->WriteDword((_ptr_)0x005A4F85 + 2, _new_spells_size_ * 26 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A4F91 + 3, _new_spells_size_ * 26 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005A5506 + 2, _new_spells_size_ * 26 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A550C + 3, _new_spells_size_ * 26 + 0x34);
    //
    
    //CombatMan_005A6670 
    SpellZ->WriteDword((_ptr_)0x005A669F + 2, _new_spells_size_ * 19 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A66A5 + 3, _new_spells_size_ * 19 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005A6968 + 2, _new_spells_size_ * 19 + 0x08);
    SpellZ->WriteDword((_ptr_)0x005A6980 + 2, _new_spells_size_ * 19 + 0x10);
    //

    // MISC 3
    SpellZ->WriteDword((_ptr_)0x005A7A50 + 2, _new_spells_size_ * 38 + 0x08);
    SpellZ->WriteDword((_ptr_)0x005A809A + 3, _new_spells_size_ * 14 + 0x34);

    // BattleMgr_CreatureMagicResist
    SpellZ->WriteDword((_ptr_)0x005A850D + 2, _new_spells_size_ * 10 + 0x00);
    SpellZ->WriteDword((_ptr_)0x005A8696 + 3, _new_spells_size_ * 60 + 0x30);
    SpellZ->WriteDword((_ptr_)0x005A869D + 3, _new_spells_size_ * 60 + 0x34);
    SpellZ->WriteDword((_ptr_)0x005A875B + 3, _new_spells_size_ * 65 + 0x34);
    //

    SpellZ->WriteDword((_ptr_)0x005C1F2E + 2, _new_spells_size_ * 9 + 0x10);

    // sub_005F4C00
    SpellZ->WriteDword((_ptr_)0x005F527F + 2, _new_spells_size_ * 72 + 0x10);
    SpellZ->WriteDword((_ptr_)0x005F52A9 + 2, _new_spells_size_ * 59 + 0x10);
    SpellZ->WriteDword((_ptr_)0x005F52D3 + 2, _new_spells_size_ * 47 + 0x10);
    // 


    //SpellZ->WriteDword((_ptr_)0x0077521C +2, (char*)new_spells);
    // SpellZ->WriteDword((_ptr_)0x007757D7 +2, (char*)new_spells);
    // SpellZ->WriteDword((_ptr_)0x00775957 +2, (char*)new_spells);
    //SpellZ->WriteDword((_ptr_)0x007759B5 +1, (char*)new_spells);
    //SpellZ->WriteDword((_ptr_)0x00775E67 +1, (char*)new_spells);
    //SpellZ->WriteDword((_ptr_)0x0077618D +2, (char*)new_spells);

    SpellZ->WriteDword((_ptr_)0x00775216 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x007759AF + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00775E61 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00776187 + 2, _new_spells_size_);
    //SpellZ->WriteDword((_ptr_)0x00776187 + 2, _new_spells_size_);

    //////
    // wog area imul
    SpellZ->WriteDword((_ptr_)0x0071CD84 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0071CDAC + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0071CF27 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0071CF47 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0071CF67 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0071CF87 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0071D13A + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0071D15F + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0071D17F + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0071D19F + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0071D1BF + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x007757C2 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x007757D1 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00775951 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00775960 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00776A88 + 2, _new_spells_size_);
    //////

    //SpellZ->WriteDword((_ptr_)0x028B3230 + 0, 0);

    /// Spells_WoG (0x7BD2C0)
    SpellZ->WriteDword((_ptr_)0x0077521C + 2, (char*) new_spells);
    SpellZ->WriteDword((_ptr_)0x007759B5 + 1, (char*) new_spells);
    SpellZ->WriteDword((_ptr_)0x00775E67 + 1, (char*) new_spells);
    SpellZ->WriteDword((_ptr_)0x0077618D + 2, (char*) new_spells);
    // SpellZ->WriteDword((_ptr_)0x00 + 2, (char*) new_spells);
    // SpellZ->WriteDword((_ptr_)0x00 + 2, (char*) new_spells);

    /// ADD & CMP (loop) 
    SpellZ->WriteDword((_ptr_)0x004C2550 + 1, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004C2556 + 1, _new_spells_size_* 70);
    SpellZ->WriteDword((_ptr_)0x004C25FA + 1, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004C260C + 1, _new_spells_size_ * 70);
 
     // reverting 7e93386d
    SpellZ->WriteDword((_ptr_)0x004C92BC + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004C92C3 + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x004C933E + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004C9345 + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x004C93B7 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004C93BE + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x00425E91 + 1, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00425E97 + 1, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x00432A3C + 1, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00432A42 + 1, _new_spells_size_ * 70);
     // reverting 7e93386d - nope

    SpellZ->WriteDword((_ptr_)0x00432D15 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00432D1C + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x0043C212 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0043C219 + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x0043C6E9 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0043C6F0 + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x00447548 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0044754F + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x00447C74 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00447C7B + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x00447CBF + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00447CC6 + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x004D9624 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004D962B + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x004D9678 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004D967F + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x004D96CD + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004D96D4 + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x004D9725 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004D972C + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x004D9768 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004D976F + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x004F50C5 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004F50CC + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x004F510B + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x004F5112 + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x00527AFF + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00527B06 + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x0052A9AD + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0052A9B4 + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x00534C42 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x00534C49 + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x0059CDB6 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x0059CDBD + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x005A1ACA + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x005A1AD4 + 2, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x005BE565 + 2, _new_spells_size_);
    SpellZ->WriteDword((_ptr_)0x005BE56C + 2, _new_spells_size_ * 70);
    //
    SpellZ->WriteDword((_ptr_)0x005BEAFC + 2, _new_spells_size_ * 70);
    //
    //SpellZ->WriteDword((_ptr_)0x005BEB20 + 2, _new_spells_size_);
    //SpellZ->WriteByte ((_ptr_)0x005BEB27 + 2, _new_spells_size_ / 4);
    SpellZ->WriteDword((_ptr_)0x005BEB2A + 2, _new_spells_size_ * 70);
    //SpellZ->WriteDword((_ptr_)0x005BEBF9 + 2, _new_spells_size_);
    //SpellZ->WriteByte((_ptr_) 0x005BEC00 + 2, _new_spells_size_ / 4);
    SpellZ->WriteDword((_ptr_)0x005BEC03 + 2, _new_spells_size_ * 70);
    //
    SpellZ->WriteDword((_ptr_)0x004C24C8 + 1, _new_spells_size_ * 70);
    SpellZ->WriteDword((_ptr_)0x005BE510 + 2, _new_spells_size_ * 70);

}



void __stdcall patch_spells(Era::TEvent* Event) {
    patch_spells();
}

void __stdcall copy_vanilla(Era::TEvent* Event) {
    copy_vanilla();
}
void __stdcall reload_sptraits(Era::TEvent* Event) {
    //copy_vanilla();

    SpellZ->WriteDword((_ptr_)0x028B3230 + 0, 0);
    int (*ParseSpTraitsTxt)(void)  = (int (*)(void)) 0x0077577F;
    ParseSpTraitsTxt();
}

DWORD spellz_dummy1, spellz_dummy2;

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {
        globalPatcher = GetPatcher();
        SpellZ = globalPatcher->CreateInstance((char*)PINSTANCE_MAIN);
        SpellZ->WriteDword((_ptr_)0x0078C590, 0);
        SpellZ->WriteDword((_ptr_)0x0078C594, 0);
        SpellZ->WriteDword((_ptr_)0x0078C598, 0);

        SpellZ->WriteDword((_ptr_)0x0078B99C, 0 /* (DWORD) &spellz_dummy1) */);
        SpellZ->WriteDword((_ptr_)0x0078B9A0, 0);
        SpellZ->WriteDword((_ptr_)0x0078B9A4, 0);

        SpellZ->WriteDword((_ptr_)0x0078B990, 0 /* (DWORD)&spellz_dummy2 */ );
        SpellZ->WriteDword((_ptr_)0x0078B994, 0);
        SpellZ->WriteDword((_ptr_)0x0078B998, 0);
        
        copy_vanilla();
        patch_spells();
        Era::ConnectEra();
        // Era::RegisterHandler(copy_vanilla, "OnAfterWoG");
        // Era::RegisterHandler(reload_sptraits, "OnAfterCreateWindow");
        // Era::RegisterHandler(patch_spells, "OnAfterCreateWindow");
        break;
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

