#include <windows.h>
//#include <windowsx.h>
#include "Era.h"

#include ".\Project1\resource.h"


using namespace Era;
  
const int ADV_MAP   = 37;
const int CTRL_LMB  = 4;
const int LMB_PUSH  = 12;


HWND hwnd, hwnd_edit, 
	hwnd_master, hwnd_button;  /* This is the handle for our window */
MSG messages;						/* Here messages to the application are saved */
WNDCLASSEX wincl;					/* Data structure for the windowclass */
HBRUSH window_brush;


char NewSkirmishName[512] = "";

						 /*  Declare Windows procedure  */

LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);
HINSTANCE DLL_hInst;

/*  Make the class name into a global variable  */
char szClassName[] = "SkirmishPluginWindow";

void ask_skirmish(void) {
	int nFunsterStil = SW_SHOWNORMAL; //were missing
	RECT RECT_Parent;
	GetWindowRect(hwnd_master, &RECT_Parent);

	/* The class is registered, let's create the program*/
	hwnd = CreateWindowEx(
		0,						/* Extended possibilites for variation */
		szClassName,			/* Classname */
		"Choose Skirmish Name", /* Title Text */
		
			/* window style */
		WS_VISIBLE | WS_CHILD |
		WS_OVERLAPPED /*| WS_CAPTION */,
		
		(RECT_Parent.right - RECT_Parent.left - 640) / 2,
		(RECT_Parent.bottom - RECT_Parent.top - 480) / 2,
		640,					/* The programs width */
		480,					/* and height in pixels */
		hwnd_master,			/* The window is a child-window to desktop */
		NULL,					/* No menu */
		DLL_hInst,				/* Program Instance handler */
		NULL					/* No Window Creation data */
	);

	hwnd_edit = CreateWindowEx(
		0,						/* Extended possibilites for variation */
		"EDIT",					/* Classname */
		"Skirmish",				/* Title Text */
		WS_VISIBLE | WS_CHILD |
		WS_CLIPCHILDREN | ES_CENTER,	/* style */
		64,						/* plugin decides the position */
		64,						/* where the window ends up on the screen */
		640 - 128,				/* The programs width */
		48,						/* and height in pixels */
		hwnd,					/* The window is a child-window to main window */
		NULL,					/* menu */
		DLL_hInst,				/* Program Instance handler */
		NULL					/* No Window Creation data */
	);


	hwnd_button = CreateWindow(
		"BUTTON",  // Predefined class; Unicode assumed 
		"Choose Skirmish Name", // Button text 
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | 
		BS_DEFPUSHBUTTON | BS_CENTER |
		WS_CLIPCHILDREN | BS_OWNERDRAW,  // Styles 
		64,         // x position 
		480-96,     // y position 
		640-128,    // Button width
		60,			// Button height
		hwnd,		// Parent window
		NULL,       // No menu.
		
		//(HINSTANCE)GetWindowLong(m_hwnd, GWL_HINSTANCE),
		DLL_hInst,

		NULL);      // Pointer not needed.

	/* Make the window visible on the screen */
	
	//ShowWindow(hwnd, nFunsterStil);

	//SetClassLongPtr(hwnd_edit,   GCLP_HBRBACKGROUND, (LONG_PTR) CreateSolidBrush(0) );
	//SetClassLongPtr(hwnd_button, GCLP_HBRBACKGROUND, (LONG_PTR) CreateSolidBrush(0) );
	//SetBkColor(hwnd_edit,   TRANSPARENT);
	//SetBkColor(hwnd_button, TRANSPARENT);

	ShowWindow(hwnd, nFunsterStil);

	HFONT hFont = CreateFont(48, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE, TEXT("Tahoma"));
	SendMessage(hwnd_edit,   WM_SETFONT, (WPARAM)hFont, TRUE);
	SendMessage(hwnd_button, WM_SETFONT, (WPARAM)hFont, TRUE);

	//HDC hdc_button = GetDC(hwnd_button);
	//HDC hdc_edit   = GetDC(hwnd_edit);


	//SetTextColor(hdc_edit, RGB(0xff, 0xaa, 0x00));
	//SetBkColor(  hdc_edit, TRANSPARENT);
	
	//SetTextColor(hdc_button, RGB(0xff, 0xaa, 0x00));
	//SetBkColor(  hdc_button, TRANSPARENT);


	//SendMessage(hwnd_edit,   WM_ERASEBKGND, 0, 0);
	//SendMessage(hwnd_button, WM_ERASEBKGND, 0, 0);

	UpdateWindow(hwnd);

	/* Run the message loop. It will run until GetMessage() returns 0 */
	while (GetMessage(&messages, NULL, 0, 0))
	{
		GetWindowText(hwnd_edit, NewSkirmishName, 511);

		/* Translate virtual-key messages into character messages */
		TranslateMessage(&messages);
		/* Send message to WindowProcedure */
		DispatchMessage(&messages);
	}

}

void __stdcall OnBeforeErmInstructions(TEvent* Event)
{
	GUITHREADINFO gui_info;
	gui_info.cbSize = sizeof(GUITHREADINFO);

	LPGUITHREADINFO gui_info_ptr = &gui_info;

	GetGUIThreadInfo(NULL, gui_info_ptr);
	hwnd_master = gui_info.hwndActive;

	ask_skirmish();

	//Sleep(3000);

	//strncpy_s(z[999],NewSkirmishName,511);
	//ExecErmCmd("UN:R6/1000;");
	ExecErmCmd("UN:R6/100;");
}

void __stdcall OnAfterErmInstructions(TEvent* Event)
{	
	strncpy_s(z[999], NewSkirmishName, 511);
	//ExecErmCmd("UN:R6/1000;"); 
}
/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT message, WPARAM wParam, LPARAM     lParam)
{
	//PAINTSTRUCT Ps;
	//HDC         hDC;
	//HPEN        hPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));

	HDC hdc_button = (HDC)wParam;
	HDC hdc_edit   = (HDC)wParam;

	switch (message)                  /* handle the messages */
	{
	case WM_DESTROY:
		PostQuitMessage(0);       /* send a WM_QUIT to the message queue */
		break;
	case WM_COMMAND:
		if ((HWND)lParam == hwnd_button)
			SendMessage(hwnd, WM_CLOSE, 0, 0);
		break; 

		
	case WM_CTLCOLOREDIT:
		//SetTextColor(hdc_edit, RGB(0xff, 0xaa, 0x00));
		//SetBkColor(  hdc_edit, 0);
		SetBkMode(hdc_edit, TRANSPARENT);
		if ((HWND)lParam == hwnd_edit) {
			static HBRUSH hBrushColor;
			if(!hBrushColor)hBrushColor = CreateSolidBrush(0);
			SetBkColor(hdc_button, /*0*/ RGB(0,0,0) );
			SetBkMode(hdc_button, TRANSPARENT);

			SetTextColor(hdc_button, RGB(0xff, 0xaa, 0x00));

			return(LRESULT)hBrushColor;
		}
		break;
		
	case WM_CTLCOLORBTN:
		if ((HWND)lParam == hwnd_button) {
			static HBRUSH hBrushColor;
			if (!hBrushColor)hBrushColor = CreateSolidBrush(0);
				SetBkColor(hdc_button, /*0*/ RGB(0,0,0) );
				SetBkMode(hdc_button, TRANSPARENT);
		
				SetTextColor(hdc_button, RGB(0xff, 0xaa, 0x00));
			return(LRESULT)hBrushColor;
		}
		break;
	case WM_DRAWITEM: {
		WORD wID = (WORD)wParam;
		const DRAWITEMSTRUCT& dis = *(DRAWITEMSTRUCT*)lParam;
			DrawFocusRect(dis.hDC, &dis.rcItem);


			LPRECT rt = const_cast<LPRECT>( &dis.rcItem );
			SetTextColor(dis.hDC, RGB(0xff, 0xaa, 0x00) );
			char strTemp[512];
			GetWindowText(dis.hwndItem,strTemp,511);
			// Get the caption which have been set
			DrawText(dis.hDC, strTemp, -1, rt, DT_CENTER | DT_VCENTER | DT_SINGLELINE);


			return (INT_PTR)TRUE;
		}


		/*
	case WM_PAINT:

		break;
	*/
	case WM_CHAR:
		//GetWindowText(hwnd_edit, NewSkirmishName, 511);
		//break;

	default:                      /* for messages that we don't deal with */
		return DefWindowProc(hwnd, message, wParam, lParam);
	}

	return 0;
}



extern "C" __declspec(dllexport) BOOL APIENTRY DllMain (HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
  if (reason == DLL_PROCESS_ATTACH)
  {
    ConnectEra();
    //RegisterHandler(OnAdventureMapLeftMouseClick, "OnAdventureMapLeftMouseClick");
    //ApiHook((void*) Hook_BattleMouseHint, HOOKTYPE_BRIDGE, (void*) 0x74fd1e);

	DLL_hInst = hInst; hwnd_master = HWND_DESKTOP;
	RegisterHandler(OnBeforeErmInstructions, "OnBeforeErmInstructions");
	RegisterHandler(OnAfterErmInstructions, "OnAfterErmInstructions");


	/* The Window structure */
	wincl.hInstance = hInst;
	wincl.lpszClassName = szClassName;
	wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
	wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
	wincl.cbSize = sizeof(WNDCLASSEX);

	/* Use default icon and mouse-pointer */
	wincl.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hCursor = LoadCursor(NULL, IDC_ARROW);
	wincl.lpszMenuName = NULL;                 /* No menu */
	wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
	wincl.cbWndExtra = 0;                      /* structure or the window instance */
											   /* Use Windows's default color as the background of the window */
	window_brush = CreatePatternBrush(LoadBitmap(hInst, /*"skirmish_plugin.bmp"*/ MAKEINTRESOURCE(IDB_BITMAP1)));

	//wincl.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wincl.hbrBackground = window_brush;

	/* Register the window class, and if it fails quit the program */
	if (!RegisterClassEx(&wincl))
		return 0;

	
	/* The class is registered, let's create the program*/
	//ask_skirmish();


  }
  return TRUE;
};
