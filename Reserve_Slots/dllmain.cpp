// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "../__include__/era_.h"

// #include "../__include__/patcher_x86_commented.hpp"

#pragma warning(disable:4996)

#define _H3API_PLUGINS_
#define _H3API_PATCHER_X86_
#include"../__include__/H3API/single_header/H3API.hpp"

typedef h3::H3Town _Town_;
typedef h3::H3Hero _Hero_;
#define o_GameMgr h3::H3Main::Get()
#define o_TownMgr h3::H3TownManager::Get()
#define o_TextBuffer ((char*)0x697428)
#define o_ActivePlayerID *(int*)0x69CCF4


#define PINSTANCE_MAIN "Z_Reserve_Slots"
Patcher* globalPatcher;
PatcherInstance* Z_Reserve_Slots;


#define DwordAt(address) (*(UINT*)(address))
#define IntAt(address) (*(INT32*)(address))

#define o_CrExpoSet_GetExp(type, crloc) CDECL_2(int, 0x718CCD, type, crloc)
#define o_CrExpoSet_FindType(army, slot_ix, p_type, p_crloc)  CDECL_4(void, 0x71A1B7, army, slot_ix, p_type, p_crloc)
#define o_CrExpoSet_SetN(type, crloc, cr_type, cr_count, exp) CDECL_5(void, 0x718AD0, type, crloc, cr_type, cr_count, exp)

inline void n_CrExpoSet_Set(int type, _ptr_ crloc, int cr_type, int cr_count, int exp)
{
    int crexpo_i = CDECL_2(int, 0x718579, type, crloc); // CrExpoSet::FindIt
    if (crexpo_i != -1)
    {
        _ptr_ crexpo = DwordAt(0x718B32 + 2) + 16 * crexpo_i;
        IntAt(crexpo + 0) = exp;
        IntAt(crexpo + 4) = cr_count;
        DwordAt(crexpo + 8) = ((((_dword_)(_byte_)cr_type) << 5) & 0x1FE0) | (DwordAt(crexpo + 8) & 0xFFFFE01F);
        THISCALL_1(void, 0x7176A1, 0x860550 + 16 * crexpo_i); // CrExpo::Check4Max
    }
    else
        o_CrExpoSet_SetN(type, crloc, cr_type, cr_count, exp);
}


inline void CrExpoSet_SetExp(h3::H3Army* army, int i, int exp)
{
    int type_s;
    _ptr_ crloc_s;
    o_CrExpoSet_FindType(army, i, &type_s, &crloc_s);
    n_CrExpoSet_Set(type_s, crloc_s, army->type[i], army->count[i], exp);
}


inline int CrExpoSet_GetExp(h3::H3Army* army, int i)
{
    int type_s;
    _ptr_ crloc_s;
    o_CrExpoSet_FindType(army, i, &type_s, &crloc_s);
    return army->type[i] < 0 ? 0 : o_CrExpoSet_GetExp(type_s, crloc_s);
}

constexpr int Recruitment = 20;
int slots_per_hero(h3::H3Hero* hero) {
    return ((hero->secSkill[Recruitment] + 1) >> 1) + 7;
}

struct armySlot {
    int type = -1;
    int count;
    int exp;
    char locked;
    char icon;
    char slot_id = -1;
    char hero_id = -1;
    inline bool accessible() {
        return(type >= 0 && count >0 && !locked );
    }
    inline bool empty() {
        return (type < 0 && count <= 0 && !locked);
    }
    inline void kill() {
        type = -1; count = 0;
    }
};

armySlot savedSlots[1024][8];
extern "C" __declspec(dllexport) armySlot * getBonusSlots(h3::H3Hero * hero) {
    return savedSlots[hero->id];
}

void __stdcall StoreData(Era::TEvent* e) {
    Era::WriteSavegameSection(sizeof(savedSlots), savedSlots, "saved_reserve_slots");
}

void __stdcall RestoreData(Era::TEvent* e) {
    Era::ReadSavegameSection(sizeof(savedSlots), savedSlots, "saved_reserve_slots");
}

armySlot to_slot(_Hero_* who, int slot) {
    armySlot ret;
    ret.hero_id = who->id;
    ret.count = who->army.count[slot];
    ret.type = who->army.type[slot];
    ret.exp = CrExpoSet_GetExp(&who->army, slot);
    ret.slot_id = slot;
    ret.locked = false;
    return ret;
}

struct Reserve_Dlg : public h3::H3Dlg {
    char txt_help[15][32] = {};
    armySlot current_slots[15];
    _Hero_* current_hero = nullptr;
    int current_slot = -1;
    void pin_slots(int i) {
        int frame_color = ((h3::H3BasePalette565*)*(int*)0x006AAD18)->color[45];
        int x = i < 7 ? 130 + i * 66 : 64 + (i - 7) * 66; 
        int y = 64; int t = current_slots[i].type; 
        
        CreateDef(x, (i < 7 ? 109 : 32) + y, 1000 + i, "TwCrPort.def", t<0 ? 0: t + 2);
        CreateFrame(x - 2, ( i < 7 ? 108 : 29) + y, 62, 68, 1100 + i, frame_color) ->SendCommand(6, 4);
        itoa(current_slots[i].count, txt_help[i],10);
        
        CreateText(x + 2, ( i < 7 ? 180 : 10 ) + y, 56, 16, txt_help[i], "smalfont.fnt", h3::NH3Dlg::NTextColor::SILVER, 
            1200+i, h3::NH3Dlg::NTextAlignment::MIDDLE_CENTER, h3::NH3Dlg::NTextColor::GOLD);

        current_slots[i].locked = (i >= slots_per_hero(current_hero));
        CreateDef(x, (i < 7 ? 109 : 32) + y, 1300 + i, "Cr_Lock_.def", current_slots[i].locked ? 2 : 0);

        
    }
    void mark() {
        // for (int i = 0; i < 15; ++i);
        for (auto it : this->dlgItems) {
            if(it->GetID() >= 1100 && it->GetID()< 1200)
                it->SendCommand(6 - (current_slot == it->GetID() - 1100), 4);

        }
    }

    Reserve_Dlg(_Hero_* who) : H3Dlg(640, 360, -1, -1, 0, 0), current_hero(who)
    {
        AddBackground(0, 0, widthDlg, heightDlg, false, false, 0, false);


        //CreateFrame(110, 15, 487, 34, frame_color);
        if (!current_hero) return;
        auto& info = h3::H3HeroInfo::Get()[current_hero->id];

        CreatePcx( 64, 108 + 64, 1001, info.largePortrait);

        for (int i = 0; i < 15; ++i) {
            current_slots[i] = (i < 7) ? to_slot(who, i) : savedSlots[who->id][i - 7];
            current_slots[i].slot_id = i; pin_slots(i);
        }
    }

    BOOL DialogProc(h3::H3Msg& msg) override;

    inline void changeframe(int i) {

        // auto monsters = (h3::H3CreatureInformation*)*(int*)0x006747B0;
        ((h3::H3DlgDef*) GetH3DlgItem(1000 + i))->SetFrame(
            current_slots[i].type >=0 ? current_slots[i].type + 2 : 0
        );
        itoa(current_slots[i].count, txt_help[i], 10);
        ((h3::H3DlgText*)GetH3DlgItem(1200 + i))->SetText(txt_help[i]);
    }
};


BOOL Reserve_Dlg::DialogProc(h3::H3Msg& msg) {
    int next_slot = -1;

    if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
        auto target = msg.GetDlg()->ItemAtPosition(msg);
        if (!target) return 0; 

        if (target->GetID() >= 1100 && target->GetID() < 1900)
            next_slot = target->GetID() % 100;
        
        if (next_slot < 0) return 0;

        if (current_slots[next_slot].locked) {
            current_slot = -1;

            mark();
            this->Redraw();

            return 0;
        }

        if (current_slot < 0 && current_slots[next_slot].accessible()) {
            current_slot = next_slot;
        }
        else if (current_slot >= 0 && current_slots[next_slot].empty()) {
            current_slots[next_slot] = current_slots[current_slot];
            current_slots[current_slot].kill();
            changeframe(next_slot); changeframe(current_slot);
            current_slot = -1;
        }
        else if (current_slots[current_slot].type != current_slots[next_slot].type) {
            auto temp = current_slots[current_slot];
            current_slots[current_slot] = current_slots[next_slot];
            current_slots[next_slot] = temp;
            changeframe(next_slot); changeframe(current_slot);
            current_slot = -1;
        }
        else if (current_slots[current_slot].accessible() && current_slots[next_slot].accessible())
        {
            auto temp = current_slots[next_slot];
            current_slots[next_slot].count += current_slots[current_slot].count;

            current_slots[next_slot].exp = 
                ((long long) temp.count * (long long)temp.exp
                + (long long)current_slots[current_slot].count * (long long)current_slots[current_slot].exp 
                )/ (long long)current_slots[next_slot].count;

            current_slots[current_slot].kill();
            changeframe(next_slot); changeframe(current_slot);
            current_slot = -1;
        }

        mark();
        this->Redraw();
    }
    return 0;
};

void show_reserve_dlg(h3::H3Hero* who){
    Reserve_Dlg wnd(who);
    wnd.CreateOKButton();
    /*
    wnd.current_hero = who;
    if (who) {
        for (int i = 0; i < 15; ++i) {
            wnd.current_slots[i] = (i < 7) ? to_slot(who, i)
                : savedSlots[who->id][i-7];
        }
    }*/
    wnd.Start();
    for (int i = 0; i < 7; ++i) {
        who->army.count[i]  = wnd.current_slots[i].count;
        who->army.type[i]   = wnd.current_slots[i].type;
        CrExpoSet_SetExp(&who->army, i, wnd.current_slots[i].exp);
    }

    for (int i = 7; i < 15; ++i) {
        savedSlots[who->id][i - 7] = wnd.current_slots[i];
    }
}

/*
void __stdcall OnCloseHeroScreen(Era::TEvent* e) {
    auto hero = h3::H3Main::Get()->GetPlayer()->GetActiveHero();
    show_reserve_dlg(hero);
}
*/

void __stdcall OnHeroScreenMouseClick(Era::TEvent* e) {
    Era::ExecErmCmd("SN:L^Era.dll^/?y1 Ay1/^GetButtonID^/?y2 Ey2/0/^Knightmare.Reserve^;");
    Era::ExecErmCmd("CM:I?y3;"); if (Era::v[1] == Era::y[3]) {
        Era::ExecErmCmd("HE-1:Z?y4;"); 
        show_reserve_dlg((h3::H3Hero*)Era::y[4]);
    }
}

int  __stdcall Army_AddCreatures_hook(HiHook* hook,h3::H3Army* MonArr, int a2, int Slot, int MonNum, int MonType) {
    int ret = FASTCALL_5(int, hook->GetDefaultFunc(), MonArr,a2,Slot,MonNum,MonType);
    if (!ret) {
        h3::H3Hero* hero_a = (h3::H3Hero*) (((char*)MonArr) - 0x91);

        // h3::H3Hero* hero_b = h3::H3Main::Get()->players[o_ActivePlayerID].GetActiveHero();
        // if (hero_a != hero_b) return false;

        h3::H3Hero* hero_zero = h3::H3Main::Get()->GetHero(0);
        if (hero_a < hero_zero || hero_a >= hero_zero + 156)
            return false;

        auto reserve = getBonusSlots(hero_a);
        auto z = slots_per_hero(hero_a) - 7;
        for (int i = 0; i < z;++i) {
            int &t = reserve[i].type;
            int &n = reserve[i].count;
            int &x = reserve[i].exp;
            if (t == Slot) {
                int new_num = n + MonNum;
                x = (LONG64)x * (LONG64)n / (LONG64)new_num;
                n = new_num;    return true;
            }
        }
        for (int i = 0; i < z; ++i) {
            int& t = reserve[i].type;
            int& n = reserve[i].count;
            int& x = reserve[i].exp;
            if (t < 0 || n <= 0) {
                t = Slot;
                n = MonNum;
                x = 0;
                return true;
            }
        }
    }
    return ret;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {

        globalPatcher = GetPatcher();
        Z_Reserve_Slots = globalPatcher->CreateInstance((char*) PINSTANCE_MAIN);
        Z_Reserve_Slots->WriteHiHook(0x0044A9B0, SPLICE_, EXTENDED_, FASTCALL_ , Army_AddCreatures_hook);

        Era::ConnectEra();

        // Era::RegisterHandler(OnCloseHeroScreen, "OnCloseHeroScreen"); //debug
        Era::RegisterHandler(OnHeroScreenMouseClick, "OnHeroScreenMouseClick");

        Era::RegisterHandler(StoreData, "OnSavegameWrite");
        Era::RegisterHandler(RestoreData, "OnSavegameRead");
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

