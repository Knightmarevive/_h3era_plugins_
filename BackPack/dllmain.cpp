// dllmain.cpp : Defines the entry point for the DLL application.
#pragma warning(disable : 4996)
typedef struct IUnknown IUnknown;

#define _H3API_PLUGINS_
#define _H3API_MESSAGES_
// #define _H3API_EXCEPTION
#define _H3API_PATCHER_X86_
// #include "../__include__/patcher_x86_commented.hpp"
#include "../__include__/H3API/single_header/H3API.hpp"
#include "../__include__/era.h"
// #include <cstdio>


inline int z_MsgBox(const char* Mes, int MType, int PosX, int PosY, int Type1, int SType1, int Type2, int SType2, int Par, int Time2Show, int Type3, int SType3) {
    FASTCALL_12(int, 0x004F6C00, Mes, MType, PosX, PosY, Type1, SType1, Type2, SType2, Par, Time2Show, Type3, SType3);

    int IDummy;
    __asm {
        mov eax, 0x6992D0
        mov ecx, [eax]
        mov eax, [ecx + 0x38]
        mov IDummy, eax
    }
    return IDummy;
}
void z_shout(const char* Mes) {
    z_MsgBox((char*)Mes, 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
}

#define o_BPP (*(char*)(0x5FA228 + 3)) << 3
#define MAX_SCROLLS 256
#define SOD_SCROLLS 70

h3::H3LoadedPcx16* Spell_Small[MAX_SCROLLS];

void BackPack_CreateResources(h3::H3LoadedPcx16* res_arr[], const char* src_def)
{
    h3::H3LoadedDef* srcLodedDef = h3::H3LoadedDef::Load(src_def);

    //int x = 20;
    //int y = 255;
    //int imgOffset = 19;

    auto GMM = GetModuleHandleA("Grandmaster_Magic.era");
    int count = GMM ? MAX_SCROLLS : SOD_SCROLLS;

    int width = srcLodedDef->widthDEF;
    int height = srcLodedDef->heightDEF;
    constexpr int scale = 2; // Scale of the pic
    int i_width = width / scale;
    int i_height = height / scale;
    h3::H3LoadedPcx16* buf = h3::H3LoadedPcx16::Create(width, height);

    int color_type = o_BPP;
    int it_height = i_height - (color_type != 32);
    for (INT32 i = 0; i < count; i++)
    {
        srcLodedDef->DrawToPcx16(0, i, 0, 0, width, height, buf, 0, 0, false);
        res_arr[i] = h3::H3LoadedPcx16::Create(i_width, i_height);

        int pix_atX = -scale / 2, pix_atY = -scale / 2;
        for (int k = 0; k < i_width; k++)
        {
            pix_atX += scale;

            for (int j = 0; j < it_height; j++)
            {
                pix_atY += scale;
                *res_arr[i]->GetPixel888(k, j) = h3::H3ARGB888(buf->GetPixel888(pix_atX, pix_atY)->GetColor());
            }
            pix_atY = -scale / 2;
        }

        // if (i >= 250) z_shout("hurra");
    }


    buf->Destroy();
    return;

}

void ShowArtInfo(h3::H3Artifact& art) {
    if (art.id < 0) return;
    if (art.subtype == -1) {
        LPCSTR text = h3::H3ArtifactSetup::Get()[art.id].description;
        z_MsgBox(text, 1, -1, -1, 8, art.id, -1, 0, -1, 0, -1, 0);
    }
    else if (art.id == 1) {
        LPCSTR text = h3::H3Spell::Get()[art.subtype].name;
        // LPCSTR text = "Spell Scroll";
        z_MsgBox(text, 1, -1, -1, 9, art.subtype, -1, 0, -1, 0, -1, 0);
    }
}

void fix_hero_bp_count(h3::H3Hero* her) {

    int c = 0;
    for (auto& a : her->backpackArtifacts)
        if (a.id >= 0) ++c;
    her->backpackCount = c;
}

h3::H3Hero* CurrentHero = nullptr;
bool (*Alchemy_Put_Into)(h3::H3Hero*, h3::H3Artifact*) = nullptr;
struct HeroBackPack {
    short NormalArtifacts[1000] = {};
    short Scrolls[256] = {};
    bool AddArtifact(h3::H3Artifact& art) {
        if (Alchemy_Put_Into && CurrentHero
            && Alchemy_Put_Into(CurrentHero, &art)) {
            return true;
        }

        if (art.id == 1) {
            ++(Scrolls[art.subtype]);
            return true;
        }

        if (art.subtype == -1) {
            ++(NormalArtifacts[art.id]);
            return true;
        }


        return false;
    }
    int HasArtifact(h3::H3Artifact& art) {
        if (art.id == 1) {
            // if(Scrolls[art.subtype]) z_shout("OK");
            return Scrolls[art.subtype];
        }

        if (art.subtype == -1) {
           return NormalArtifacts[art.id];
        }

        return false;
    }
    bool RemoveArtifact(h3::H3Artifact& art) {
        if (!HasArtifact(art)) return false;

        if (art.id == 1) {
            // z_shout("");
            --(Scrolls[art.subtype]);
            return true;
        }

        if (art.subtype == -1) {
            --(NormalArtifacts[art.id]);
            return true;
        }


        return false;
    }
    void clean() {
        for (auto& i : NormalArtifacts) i = 0;
        for (auto& i : Scrolls) i = 0;
    }
};

HeroBackPack BackPacks[1024];


struct Real2Virtual : public h3::H3Dlg {
    h3::H3Hero* myHero; static constexpr int Empty = 145;
    Real2Virtual(h3::H3Hero* her) :H3Dlg(480,480), myHero(her){
        for (int j = 0; j < 8; ++j)for (int i = 0; i < 8; ++i) {
            int ij = j * 8 + i; int frame = her->backpackArtifacts[ij].id;
            
            if (frame == 1) {
                h3::H3DlgPcx16* dlgPcx = h3::H3DlgPcx16::Create(76 + i * 40, 76 + j * 40, 40, 30, 3000 + ij, nullptr);
                dlgPcx->SetPcx(Spell_Small[her->backpackArtifacts[ij].subtype]); this->AddItem(dlgPcx); continue;
            }
            
            if (frame == -1) frame = Empty;
            CreateDef(76 + i * 40, 76 + j * 40, 3000 + ij,"artifact.def",frame);
        }
    }
    BOOL DialogProc(h3::H3Msg& msg) override;
};
BOOL Real2Virtual::DialogProc(h3::H3Msg& msg) {
    if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
        if (msg.itemId >= 3000 && msg.itemId < 4000) {
            auto dlg = this->GetH3DlgItem(msg.itemId);
            int pos = msg.itemId - 3000; 
            auto& art = myHero->backpackArtifacts[pos];
            if (art.id < 0) return 0;
            if (BackPacks[myHero->id].AddArtifact(art)) {
                
                dlg->HideDeactivate();

                /*
                auto z_dlg = (h3::H3DlgDef*)dlg;
                z_dlg->SetFrame(Empty);
                // z_dlg->Refresh();
                */

                art = { -1,-1 };
                this->Redraw();
            }
        } 
    }
    else if (msg.subtype == h3::eMsgSubtype::RBUTTON_DOWN) {
        if (msg.itemId >= 3000 && msg.itemId < 4000) {
            auto dlg = this->GetH3DlgItem(msg.itemId);
            int pos = msg.itemId - 3000;
            auto& art = myHero->backpackArtifacts[pos];
            ShowArtInfo(art);
        }
    }

    return 0;
}

struct Virtual2Real : public h3::H3Dlg {
    h3::H3Hero* myHero; int begin; static constexpr int Empty = 145;
    Virtual2Real(h3::H3Hero* her, int _begin_ = 0) :H3Dlg(780, 576), myHero(her), begin(_begin_) {
        int i = 0; int j = 0;
        for (int z = begin; z < 9000; ++z) {
            if (j >= 12) break;

            if (z < 1000) {
                auto count = BackPacks[her->id].NormalArtifacts[z];
                if (count) {
                    char cnt[8]; itoa(count, cnt, 10);
                    CreateDef(30 + i * 40, 30 + j * 40, 3000 + z, "artifact.def", z);
                    CreateText(30 + i * 40, 54 + j * 40, 32, 16,cnt,"medfont.fnt", h3::eTextColor::GREEN, 13000 + z);

                    ++i; if (i >= 18) { i = 0; ++j; }
                }
            }
            else if (z < 1256) {
                auto count = BackPacks[her->id].Scrolls[z-1000];
                if (count) {
                    char cnt[8]; itoa(count, cnt, 10);
                    // CreateDef(30 + i * 36, 30 + j * 40, 3000 + z, "SpellInt.def", z - 999);

                    h3::H3DlgPcx16* dlgPcx = h3::H3DlgPcx16::Create(32 + i * 40, 34 + j * 40, 40, 30, 3000 + z, nullptr);
                    dlgPcx->SetPcx(Spell_Small[z-1000]); this->AddItem(dlgPcx);

                    CreateText(30 + i * 40, 54 + j * 40, 32, 16, cnt, "medfont.fnt", h3::eTextColor::GREEN, 13000 + z);

                    ++i; if (i >= 18) { i = 0; ++j; }
                }
            }
            else {
                CreateDef(30 + i * 40, 30 + j * 40, 3000 + z, "artifact.def", Empty);

                ++i; if (i >= 18) { i = 0; ++j; }
            }

            
        }
    }
    BOOL DialogProc(h3::H3Msg& msg) override;
};
BOOL Virtual2Real::DialogProc(h3::H3Msg& msg) {
    if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
        if (msg.itemId >= 3000 && msg.itemId < 9000) {
            auto dlg = this->GetH3DlgItem(msg.itemId);
            int pos = msg.itemId - 3000; 
            h3::H3Artifact art = { -1,-1 };
            if (pos<1000) art.id = pos;
            if (pos >= 1000 && pos < 2000) {
                art.subtype = pos - 1000; art.id = 1;
            }
            // auto& art = myHero->backpackArtifacts[pos];
            if (art.id < 0) return 0;
            int destination = -1;
            for (int d = 0; d < 64; ++d) {
                if (myHero->backpackArtifacts[d].id<0) {
                    destination = d; break;
                }
            }

            if (destination>=0 && BackPacks[myHero->id].RemoveArtifact(art)) {
                myHero->backpackArtifacts[destination] = art;
                auto z_dlg= (h3::H3DlgDef*)dlg; 
                auto t_dlg = this->GetText(msg.itemId+10000);
                int has_it = BackPacks[myHero->id].HasArtifact(art);
                if (!has_it) { 
                    if(art.subtype == -1)z_dlg->SetFrame(Empty);
                    else if(art.id == 1) dlg->HideDeactivate();
                    t_dlg->SetText("");
                }
                else {
                    char cnt[8]; itoa(has_it, cnt, 10);
                    t_dlg->SetText(cnt);
                }

                //z_dlg->Redraw();
                this->Redraw();
            }
        }
    }
    else if (msg.subtype == h3::eMsgSubtype::RBUTTON_DOWN) {
        if (msg.itemId >= 3000 && msg.itemId < 9000) {
            auto dlg = this->GetH3DlgItem(msg.itemId);
            int pos = msg.itemId - 3000;
            h3::H3Artifact art = { -1,-1 };// myHero->backpackArtifacts[pos];
            if (pos < 1000) art.id = pos;
            else if (pos < 1256) { art.id = 1; art.subtype = pos - 1000; }
            ShowArtInfo(art);
        }
    }
}


extern "C" __declspec(dllexport) void CallDumpReal2Virtual(h3::H3Hero * her) {
    for (auto &i : her->backpackArtifacts) {
        if (i.id < 0) continue;
        if (BackPacks[her->id].AddArtifact(i)) 
            { i.id = i.subtype = -1; }
    }

    fix_hero_bp_count(her);
}

extern "C" __declspec(dllexport) void CallReal2Virtual(h3::H3Hero* her) {
    CurrentHero = her;
    Real2Virtual wnd(her);

    wnd.CreateOKButton();
    wnd.Start();

    fix_hero_bp_count(her);

    CurrentHero = nullptr;
}

extern "C" __declspec(dllexport) void CallVirtual2Real(h3::H3Hero * her) {
    CurrentHero = her;
    Virtual2Real wnd(her);

    wnd.CreateOKButton();
    wnd.Start();

    fix_hero_bp_count(her);

    CurrentHero = nullptr;
}

extern "C" __declspec(dllexport) bool put_in_backpack(h3::H3Hero * her, h3::H3Artifact *art) {
    if (!her) return false; CurrentHero = her;
    auto answer = BackPacks[her->id].AddArtifact(*art);

    fix_hero_bp_count(her);

    CurrentHero = nullptr; return answer;
}

void __stdcall StoreData(Era::TEvent* e)
{
    Era::WriteSavegameSection(sizeof(BackPacks), & BackPacks, "Z_Backpacks");
}
void __stdcall RestoreData(Era::TEvent* e)
{
    Era::ReadSavegameSection(sizeof(BackPacks), &BackPacks, "Z_Backpacks");
}

void __stdcall loadUp(Era::TEvent* e) {
    BackPack_CreateResources(Spell_Small,"SpellScr.def");

    HMODULE Alchemy = GetModuleHandleA("Alchemy.era");
    Alchemy_Put_Into = (bool (__cdecl *)(h3::H3Hero*, h3::H3Artifact*))
        GetProcAddress(Alchemy, "put_in_alchemy_bag");
}

/*
_LHF_(loadGFX_hook) {
    BackPack_CreateResources(Spell_Small, "SpellScr.def");
    return EXEC_DEFAULT;
}
*/

void __stdcall Clean(Era::TEvent* e) {
    for (auto& i : BackPacks) i.clean();
}

Patcher* globalPatcher;
PatcherInstance* Z_BackPack;
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH: {
        globalPatcher = GetPatcher();
        Z_BackPack = globalPatcher->CreateInstance((char*)"Z_BackPack");
        Era::ConnectEra();

        Era::RegisterHandler(StoreData, "OnSavegameWrite");
        Era::RegisterHandler(RestoreData, "OnSavegameRead");
        Era::RegisterHandler(loadUp, "OnAfterCreateWindow");

        //Z_BackPack->WriteLoHook(0x4EEAF2, loadGFX_hook);
        Era::RegisterHandler(Clean, "OnAfterErmInstructions");

    } break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

