// dllmain.cpp : Defines the entry point for the DLL application.
// #include "pch.h"
#pragma warning(disable : 4996)

// #include "../__include__/patcher_x86_commented.hpp"
// #include "patcher_x86_commented.hpp"

#include "../Heroine/hero_limits.h"

#define _H3API_PLUGINS_
#define _H3API_PATCHER_X86_
#include "../__include__/H3API/single_header/H3API.hpp"

#include "framework.h"
#include "era.h"

#define PINSTANCE_MAIN "SS_Spec"

Patcher* globalPatcher;
PatcherInstance* Z_SS_Spec;

struct H3SecondarySkillInfoNew
{
    LPCSTR name;
    LPCSTR description[15];
};

auto f_GetHeroesSpecialtyTable = (h3::H3HeroSpecialty * (*)()) nullptr;
auto f_get_H3SecondarySkillInfoNew = (H3SecondarySkillInfoNew * (*)(int)) nullptr;;
auto f_Get_Spec44ptr = (h3::PH3LoadedPcx16(*)(int)) nullptr;
auto ss_bon_def = (h3::PH3LoadedDef) nullptr;

void Update_SS_Spec(int id, h3::H3HeroSpecialty& spec, char* name = "Hero") {
    if (spec.type != 0) return;

    long skill_id = spec.bonusId;


    if (f_get_H3SecondarySkillInfoNew) {
        auto skill_info = f_get_H3SecondarySkillInfoNew(skill_id);

        strcpy((char*)spec.spShort, skill_info->name);
        strcpy((char*)spec.spFull, skill_info->name);

        sprintf((char*)spec.spDescr,
            "%s recieves a 5%% per level bonus to %s skill percentage.",
            name, skill_info->name);
    }

    auto spec44 = f_Get_Spec44ptr(id);

    if(f_get_H3SecondarySkillInfoNew)
        ss_bon_def->DrawToPcx16(0, 17 + skill_id * 16, spec44, 0, 0);
    else ss_bon_def->DrawToPcx16(0, 3 + skill_id * 3, spec44, 0, 0);
}


_LHF_(hook_0058692A) {
    static bool done = false;
    if (done) return EXEC_DEFAULT;


    HMODULE h_More_SS = GetModuleHandleA("more_SS_levels.era");

    if (h_More_SS)
        ss_bon_def = h3::H3LoadedDef::Load("zecskill.def");
    else ss_bon_def = h3::H3LoadedDef::Load("secskill.def");

    HMODULE h_Heroine = GetModuleHandleA("Heroine.era");
    if (h_Heroine)
    {
        f_GetHeroesSpecialtyTable = (h3::H3HeroSpecialty * (*)())
            GetProcAddress(h_Heroine, "GetHeroesSpecialtyTable");

        f_Get_Spec44ptr = (h3::PH3LoadedPcx16(*)(int))
            GetProcAddress(h_Heroine, "Get_Spec44ptr");

        if (h_More_SS) 
        {
            f_get_H3SecondarySkillInfoNew = (H3SecondarySkillInfoNew * (*)(int))
                GetProcAddress(h_More_SS, "get_H3SecondarySkillInfoNew");
        }
        else
        {
            // f_get_H3SecondarySkillInfoNew = (H3SecondarySkillInfoNew * (*)(int))
        }
        auto SpecTable = f_GetHeroesSpecialtyTable();
        for (int i = 0; i < MaxHeroCount; ++i) {
            if (SpecTable[i].type == 0) 
            {
                Update_SS_Spec(i, SpecTable[i]);
            }
        }
    }

    done = true;
    return EXEC_DEFAULT;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH: {
        globalPatcher = GetPatcher();
        Z_SS_Spec = globalPatcher->CreateInstance(PINSTANCE_MAIN); 
        Z_SS_Spec->WriteLoHook(0x0058692A, hook_0058692A);
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

