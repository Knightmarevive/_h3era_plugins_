// dllmain.cpp : Defines the entry point for the DLL application.
// #include "pch.h"
#pragma warning(disable : 4996)

// #include "../__include__/patcher_x86_commented.hpp"
// #include "patcher_x86_commented.hpp"

#include "../Heroine/hero_limits.h"

#define _H3API_PLUGINS_
#define _H3API_PATCHER_X86_
#include "../__include__/H3API/single_header/H3API.hpp"

#include "framework.h"
#include "era.h"

#define PINSTANCE_MAIN "Spell_Spec"

Patcher* globalPatcher;
PatcherInstance* Z_Spell_Spec;

auto f_GetHeroesSpecialtyTable = (h3::H3HeroSpecialty * (*)()) nullptr;
auto f_Get_Spec44ptr = (h3::PH3LoadedPcx16(*)(int)) nullptr;
auto spellbon_def = (h3::PH3LoadedDef) nullptr;

void Update_Spell_Spec(int id, h3::H3HeroSpecialty& spec, char* name = "Hero") {
    if (spec.type != 3) return;

    long spell_id = spec.bonusId;
    auto spell_name = h3::H3Spell::Get()[spell_id].name;

    strcpy((char*)spec.spShort, spell_name);
    strcpy((char*)spec.spFull, spell_name);

    sprintf((char*)spec.spDescr,
        "%s recieves some bonus each level to %s spell Efficiency.",
        name, spell_name);

    auto spec44 = f_Get_Spec44ptr(id);
    spellbon_def->DrawToPcx16(0, spell_id, spec44, -7, -10);
}

_LHF_(hook_0058692A) {
    static bool done = false;
    if (done) return EXEC_DEFAULT;

    spellbon_def = h3::H3LoadedDef::Load("spellbon.def");
    HMODULE h_Heroine = GetModuleHandleA("Heroine.era");
    // HMODULE h_More_SS = GetModuleHandleA("more_SS_levels.era");
    // HMODULE h_GM_Magic = GetModuleHandleA("Grandmaster_Magic.era");

    if (h_Heroine /* && h_GM_Magic*/ ) {
        f_GetHeroesSpecialtyTable = (h3::H3HeroSpecialty * (*)())
            GetProcAddress(h_Heroine, "GetHeroesSpecialtyTable");
        f_Get_Spec44ptr = (h3::PH3LoadedPcx16(*)(int))
            GetProcAddress(h_Heroine, "Get_Spec44ptr");
        /*
        f_get_H3SecondarySkillInfoNew = (H3SecondarySkillInfoNew * (*)(int))
            GetProcAddress(h_More_SS, "get_H3SecondarySkillInfoNew");
        */
        auto SpecTable = f_GetHeroesSpecialtyTable();
        for (int i = 0; i < MaxHeroCount; ++i) {
            if (SpecTable[i].type == 3)
            {
                Update_Spell_Spec(i, SpecTable[i]);
            }
        }
    }

    done = true;
    return EXEC_DEFAULT;
}
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH: {
        globalPatcher = GetPatcher();
        Z_Spell_Spec = globalPatcher->CreateInstance(PINSTANCE_MAIN);
        Z_Spell_Spec->WriteLoHook(0x0058692A, hook_0058692A);
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

