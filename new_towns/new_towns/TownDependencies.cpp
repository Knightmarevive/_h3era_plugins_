#include "patcher_x86_commented.hpp"
#include <cstdio>
// #include "HoMM3.h"
#include <string>
#include <sstream>
#include <vector>
#include "common.hpp"

extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;

struct _MouseStr_ {
	long Type;
	long SubType;
	long Item;
	long Flags;
	long Xabs;
	long Yabs;
	long NewPar;
	long Dialog;
};

#pragma warning(disable:4996)

#include"../../__include__/H3API/single_header/H3API.hpp"

#include"_Town_.h"

//typedef h3::H3Town _Town_;
typedef h3::H3Hero _Hero_;
#define o_GameMgr h3::H3Main::Get()
#define o_TownMgr h3::H3TownManager::Get()
#define o_TextBuffer ((char*)0x697428)
#define o_ActivePlayerID *(int*)0x69CCF4

auto Player_HasCapitol = (int(__thiscall*)(void*)) 0x004B9C00; //5BF8E0

extern long long additional_buildings_per_town[48];

struct BuildFlags {
	uint32_t a, b;
	uint64_t c;
	BuildFlags(const _Town_* t, int field = 0) {
		char* f = ((char*)t) + 0x150 + field * 8;
		auto *tmp = (uint32_t*)f;
		a = tmp[0]; b = tmp[1]; c = 0;
		if (field) return;
		c = additional_buildings_per_town[t->number];
	}
	BuildFlags(uint32_t A, uint32_t B, uint64_t C) {
		a = A; b = B & 0x000fffff; c = C;
	}
	BuildFlags() { a = 0; b = 0; c = 0; }

	BuildFlags(char* cfg) {
		a = 0; b = 0; c = 0; int32_t t = 0;
		std::stringstream is(cfg);
		while (!is.eof()) {
			is >> t; if (t < 0) break;
			if (t < 32) a |= 1 << t;
			else if (t < 64) b |= 1 << (t - 32);
			else if (t < 128) c |= 1ll << (t - 64);
			else break;
		}
	}

	BuildFlags(int building) { 
		a = 0; b = 0; c = 0; 
		int t = building;
		if (t < 32) a |= 1 << t;
		else if (t < 64) b |= 1 << (t - 32);
		else if (t < 128) c |= 1ll << (t - 64);
	}

	operator bool() {
		return (a || b || c);
	}
};

bool operator == (BuildFlags& l, BuildFlags& r) {
	return (l.a == r.a) && (l.b == r.b) && (l.c == r.c);
}

BuildFlags operator &(BuildFlags& l, BuildFlags& r) {
	return BuildFlags{ l.a & r.a, l.b & r.b,l.c & r.c };
}
BuildFlags operator |(BuildFlags& l, BuildFlags& r) {
	return BuildFlags{ l.a | r.a, l.b | r.b,l.c | r.c };
}

BuildFlags requirements[max_towns_types][max_builds_new];

bool isBuildingRequired(const _Town_* town, int building, int required)
{
	BuildFlags requiredFlag = BuildFlags(required);
	return requirements[town->type][building] & requiredFlag;
}


inline bool canBuildSpecialRules(const _Town_* town, int building)
{
	if (building == 6)
		return (town->pos2PlaceBoatX != 0xFF || town->pos2PlaceBoatY != 0xFF);
	if (building == 13 && Player_HasCapitol(&o_GameMgr->players[town->owner]) && !town->IsBuildingBuilt(13))  // if player already has a Capitol
		return false;

	return true;
}

extern bool scriptorium_enabled;
extern bool cathedral_enabled;
extern bool azylum_enabled;
extern bool market_of_time_enabled;
extern long ThirdUpgradesInTowns_27x2x7[27 * 2 * 7];
extern int swapper[27][128];
extern int upper[27][128];

extern long EighthMonType[27][4];
extern bool EighthsSupported;


inline bool isBuildingEnabled(const _Town_* town, int building)
{
	const char BUILDING_DISABLED = 0;
	const char BUILDING_ENABLED = 1;

	if (!canBuildSpecialRules(town, building))
		return BUILDING_DISABLED;

	if (building < 44 && !town->CanBeBuilt((h3::eBuildings)building))
		return BUILDING_DISABLED;
	else
	{
		if (building >= 64 && building < 71)
			return ThirdUpgradesInTowns_27x2x7[town->type * 14 + building - 64] >= 0;

		if (building >= 71 && building < 78)
			return ThirdUpgradesInTowns_27x2x7[town->type * 14 + building - 64] >= 0;

		if (building >= 78 && building <= 84)
			return cathedral_enabled;

		if (building == 85)
			return scriptorium_enabled;

		if (building == 86)
			return azylum_enabled;

		if (building == 87)
			return market_of_time_enabled;

		if (building == 88)
			return EighthMonType[town->type][0] >= 0;
		if (building == 89)
			return EighthMonType[town->type][1] >= 0;
		if (building == 90)
			return EighthMonType[town->type][2] >= 0;
		if (building == 91)
			return EighthMonType[town->type][3] >= 0;

		for (int i = 0;i < 128;i++)
		{
			if (swapper[town->type][i] == building || upper[town->type][i] == building)
				return BUILDING_ENABLED;
		}

		return BUILDING_DISABLED;
	}

	return BUILDING_ENABLED;

	/*
	auto v4 = BuildFlags(_this_);
	auto Bbonus = BuildFlags(_this_,1);
	auto Bmask  = BuildFlags(_this_,2);
	auto deps = requirements[_this_ -> type][building];
	auto deps2 = deps; deps2.c = 0;
	BuildFlags BbonusBmask = (Bbonus | Bmask);
	if (
			(building != 13 || !Player_HasCapitol(&o_GameMgr->players[_this_->owner])
			)  // if player already has a Capitol
		&& !( (deps2 & BbonusBmask) == deps2)
			)
		/*	(BuildTownDepBits1[2 * v4] & (this->BMask | this->BuiltBonus)) == BuildTownDepBits1[2 * v4])
		&& (BuildTownDepBits1[2 * v4 + 1] & (this->BMask1 | this->BuiltBonus1)) == BuildTownDepBits1[2 * v4 + 1]) */
		/*return BUILDING_DISABLED;
	else
		return BUILDING_ENABLED;*/
}

char __stdcall isBuildingEnabled_005C1260(HiHook* h, _Town_* _this_, int building)
{
	return isBuildingEnabled(_this_, building);
}

bool hasAllRequiredBuildings(const _Town_* town, int building) 
{
	if (!town || building<0 || building>= max_builds_new) return false;

	if (town->type < 0 || town->type >= max_towns_types) return false;

	if (!isBuildingEnabled(town, building)) return false;                            // SadnessPower fix: disabled buildings cannot be built

	if (!canBuildSpecialRules(town, building))
		return false;

	// Exit if the previous building has not been built
	if (building >= 64 && building <= 70 && !town->IsBuildingBuilt(building - 27))
		return false;

	if (building >= 71 && building <= 77 && !town->IsBuildingBuilt(building - 7))
		return false;

	if ((building == 78 && !town->IsBuildingBuilt(9))
		|| (building > 78 && building <= 84 && !town->IsBuildingBuilt(building - 1))
		)
		return false;
	if (building >= 78 && building <= 84 && !cathedral_enabled)
		return false;

	if ((building == 88 && !true)
		|| (building > 88 && building <= 91 && !town->IsBuildingBuilt(building - 1))
		)
		return false;
	if (building >= 88 && building <= 91 && !EighthsSupported)
		return false;

	BuildFlags built0 = BuildFlags (town, 0);
	BuildFlags built1 = BuildFlags (town, 1);
	BuildFlags built2 = BuildFlags (town, 2);
	BuildFlags built = built0 | built1; // &built2;
	BuildFlags need = requirements[town->type][building];
	BuildFlags builtNeed = built & need;
	bool result = (builtNeed == need);
	return result;
}


bool hasAllRequiredBuildings(const h3::H3Town* town, int building) {
	return hasAllRequiredBuildings((const _Town_*)town, building);
}

bool set_depends(int town_type, int building_ID, char* str) {
	if (building_ID < 0 || building_ID >= max_builds_new) return false;
	if (town_type < 0 || town_type >= max_towns_types) return false;

	requirements[town_type][building_ID] = str;
	return true;
}

std::vector<int> get_depends(int town_type, int building_ID) {
	std::vector<int> dependentBuildings;

	if (building_ID < 0 || building_ID >= max_builds_new) {
		return dependentBuildings;
	}
	if (town_type < 0 || town_type >= max_towns_types) {
		return dependentBuildings;
	}

	const BuildFlags& buildFlag = requirements[town_type][building_ID];

	for (int i = 0; i < 32; ++i) {
		if (buildFlag.a & (1 << i)) {
			dependentBuildings.push_back(i);
		}
	}

	for (int i = 0; i < 32; ++i) {
		if (buildFlag.b & (1 << i)) {
			dependentBuildings.push_back(32 + i);
		}
	}

	for (int i = 0; i < 64; ++i) {
		if (buildFlag.c & (1LL << i)) {
			dependentBuildings.push_back(64 + i);
		}
	}

	return dependentBuildings;
}

_LHF_(hook_005C11C5) {
	_Town_* town = (_Town_*) c->ecx;
	int16_t build_id = *(int*) (c->ebp+8);
	bool result = hasAllRequiredBuildings(town, build_id);
	c->return_address = result ? 0x005C1247 : 0x005C1252;
	return SKIP_DEFAULT;
}


/*
int __fastcall ProcessDependanceList(int* a1, int a2, int a3)
{
	DWORD* v3; // edi
	int v4; // eax
	int* v5; // ecx
	int v6; // esi
	int v7; // ebx
	int v8; // ebx
	int v9; // edi
	int v10; // esi
	int result; // eax

	v3 = a3;
	*a3 = 0;
	*(a3 + 4) = 0;
	do
	{
		v4 = *a1;
		v5 = a1 + 1;
		v7 = HildLev1Mask2[2 * v4];
		*v3 |= HildLvl1Mask[2 * v4];
		v3[1] |= v7;
		*(a2 + 8 * v4) = 0;
		*(a2 + 8 * v4 + 4) = 0;
		v6 = *v5;
		if (*v5 >= 0)
		{
			do
			{
				v8 = HildLev1Mask2[2 * v6];
				*(a2 + 8 * v4) |= HildLvl1Mask[2 * v6];
				++v5;
				*(a2 + 8 * v4 + 4) |= v8;
				v9 = *(a2 + 8 * v6);
				v10 = *(a2 + 8 * v6 + 4);
				*(a2 + 8 * v4) |= v9;
				*(a2 + 8 * v4 + 4) |= v10;
				v6 = *v5;
			} while (*v5 >= 0);
			v3 = a3;
		}
		result = v5[1];
		a1 = v5 + 1;
	} while (result >= 0);
	return result;
}
*/

extern unsigned long new_BuildExclusions[27 * 44 * 2];
extern unsigned long new_TownsBuildingsDep[];
extern long new_AllEnStrBitsTown_[27 * 2 + 2];
extern int BuildDepends_[27][128];
extern int SpecBuildResetDepends_[27][32];

int __stdcall FormTownBuildDepends_004EB810(HiHook* h) 
{
	int(__fastcall * ProcessDependanceList)(int, int, int) =
		(int(__fastcall*)(int, int, int)) 0x004EBD30;
	int(__fastcall * ProcessResetList)(int, int) =
		(int(__fastcall*)(int, int)) 0x004EBBF0;

	int(__fastcall * ProcessResetList2)(int, int) =
		(int(__fastcall*)(int, int)) 0x004EBC50;
		
	auto BuildTownDepResetBits1 = new_BuildExclusions;
	int CommonBuildResetDepends = 0x0063FBC4;
	auto BuildTownDepBits1 = new_TownsBuildingsDep;
	auto AllEnStrBitsTown1 = new_AllEnStrBitsTown_;

	for (int i = 0; i < 27; ++i) {

		ProcessDependanceList((int)BuildDepends_[i], i * 44 * 2 * 4 + (int)BuildTownDepBits1, i * 2 * 4 + (int)AllEnStrBitsTown1);

		ProcessResetList((int)CommonBuildResetDepends, i * 44 * 2 * 4 + (int)BuildTownDepResetBits1);

		ProcessResetList((int)SpecBuildResetDepends_[i], i * 44 * 2 * 4 + (int)BuildTownDepResetBits1);
		ProcessResetList2((int)SpecBuildResetDepends_[i], i * 44 * 2 * 4 + (int)BuildTownDepResetBits1);
	}


	int (*TownHordeBuildings_Unit)(void) = (int (*)()) 0x005BE360;
	return TownHordeBuildings_Unit();

	//return CALL_0(int, __cdecl, h->GetDefaultFunc());
}

inline void ShowMessageBox(const char* title, const char* message) {
	MessageBoxA(NULL, message, title, MB_OK | MB_ICONINFORMATION);
}

inline void ShowMessageBoxWithInt(const char* title, const char* prefixMessage, int value) {
	char message[256];
	sprintf(message, "%s: %d", prefixMessage, value);
	ShowMessageBox(title, message);
}

int __stdcall TownBuildPopUpTextHandler_005D5FE0(HiHook* h, void *ecx_in, void* a2, int a3)
{
	// char *o_TextBuffer = (char*) 0x00697428;
	
	int  build_id = a3; int v27 = 0; 
	char* v26 = o_TextBuffer;
	char* v12 = o_TextBuffer;
	unsigned int v15 = 0;
	unsigned int v17 = 0;
	signed int v25 = 0;
	char* v16 = nullptr;
	char* v14 = nullptr;

	auto Town_GetBuildingName = (char*(__fastcall*)(int,int))0x00460CC0;
	auto sub_004B5530 = (int(__thiscall*)(void*, char*) ) 0x004B5530;
	auto v6v7 = requirements[o_TownMgr->town->type][build_id];
	//auto already = BuildFlags((_Town_*)o_TownMgr->town);
	auto flag0 = BuildFlags((_Town_*)o_TownMgr->town,0);
	auto flag1 = BuildFlags((_Town_*)o_TownMgr->town,1);
	auto already = flag0 | flag1;
	char* asc_006603BC = "\n";
	char* asc_0068C420 = "  ";

	int &ecx_24 = *(int*)(((int) ecx_in) + 0x60);
	int &ecx_24_64 = *(int*)(ecx_24 + 0x40);
	short &ecx_24_28 = *(short*)(ecx_24 + 0x1C);
	// int &ecx24_52 = *(int*)(((char*)ecx_24) + 0x34);
	// auto fun_ecx24_52 = (int(*)(const char*)) ecx24_52;
	auto fun_ret_addr = *(int*)((*(int*)ecx_24) + 0x34);
	auto fun_ret = (int(__thiscall*)(void*, const char*)) fun_ret_addr;
	void*  v23 = (void*)ecx_24_64;;
	auto town_ptr = (_Town_*)o_TownMgr->town;

	for (int i = 0; i < 128;++i) {
		BuildFlags ii = BuildFlags(i);
		if (bool(ii & v6v7) && !bool(ii & already)) {
			if (v27) {
				strcat(o_TextBuffer, ",");
				if (sub_004B5530(v23, v12) > ecx_24_28)
				{
					v12 = v26;
					*(v26 - 1) = 10;
				}
				v14 = asc_0068C420;
				v15 = strlen(asc_0068C420) + 1;
				v16 = &o_TextBuffer[strlen(o_TextBuffer) + 1];
			}
			else {
				v12 = o_TextBuffer;
				strcpy(o_TextBuffer, (P_GeneralText->GetText(1 + 212/4)));
				v14 = asc_006603BC;
				v15 = strlen(asc_006603BC) + 1;
				v16 = &o_TextBuffer[strlen(o_TextBuffer) + 1];
			}
			memcpy(v16 - 1, v14, v15);
			v17 = strlen(v12) + 1;
			++v27;
			v26 = &v12[v17 - 1];
			strcat(o_TextBuffer, Town_GetBuildingName(town_ptr->type, i /*v25*/));
			if (sub_004B5530(v23, v12) > ecx_24_28)
			{
				v12 += v17 - 1;
				*(v26 - 1) = 10;
			}
			// v9 = v21;
			// v13 = v24;
		}
	}

	auto sub_005C1260 = (char(__thiscall*)(_Town_*, int)) 0x005C1260;
    auto sub_005BF8E0 = (int(__thiscall*)(_Town_*)) 0x005BF8E0;

	void* ecx_ret = *(void**)(int(ecx_in) + 0x60);
	
	/*
	__asm {
		push ecx

		mov ecx, ecx_in
		mov ecx, [ecx + 60h]
		mov ecx_ret, ecx

		pop ecx
	}
	*/

	/*
	h3::H3BaseDlg* dlg = reinterpret_cast<h3::H3BaseDlg*>(ecx_in);

	DlgCustomButton::ButtonData buttonData = {
		166,                            // x
		445,                            // y
		64,                            // width
		32,                            // height
		12345,                         // id
		"ircbtns.def",                 // def name
		0,                             // frame
		1,                             // click frame
		true,                          // isEnabled
		"Hint"          // hint text
	};

	DlgCustomButton* button = DlgCustomButton::Create(buttonData);
	if (button) {
		dlg->AddItem(button);

		h3::H3DlgPcx* pcx = h3::H3DlgPcx::Create(165, 444, "box64x32.pcx");
		if (pcx)
			dlg->AddItem(pcx);
	}
	*/

	if (a3 == 13)
	{
		char* dword_006A7490 = (char*)*(int*)0x006A7490;

		if (Player_HasCapitol(&o_GameMgr->players[town_ptr->owner]))
			return fun_ret(ecx_ret, dword_006A7490);
	}
	else if (a3 == 6)
	{
		if (!sub_005BF8E0(town_ptr))
		{
			char* dword_006A7494 = (char*)*(int*)0x006A7494;
			return fun_ret(ecx_ret, dword_006A7494);
		}
	}
	else if (!sub_005C1260(town_ptr, build_id))
	{
		char* dword_006A7498 = (char*)*(int*)0x006A7498;
		return fun_ret(ecx_ret, dword_006A7498);
	}

	if (v27)
		return fun_ret(ecx_ret, o_TextBuffer);
	else
		return fun_ret(ecx_ret, P_GeneralText->GetText(221));

	return THISCALL_3(int, h->GetDefaultFunc(), ecx_in, a2, a3);
}



bool preset_hookForBuildDependencies() {
	static bool once = false;

	if (once) return true;

	Z_new_towns->WriteLoHook(0x005C11C5, hook_005C11C5);
	Z_new_towns->WriteHiHook(0x004EB810, SPLICE_, EXTENDED_, CDECL_, FormTownBuildDepends_004EB810);
	Z_new_towns->WriteHiHook(0x005D5FE0, SPLICE_, EXTENDED_, THISCALL_, TownBuildPopUpTextHandler_005D5FE0);
	Z_new_towns->WriteHiHook(0x005C1260, SPLICE_, EXTENDED_, THISCALL_, isBuildingEnabled_005C1260);
	once = true;
}

extern "C" __declspec(dllexport) void preset_buildDep(int town_type, int alias)
{
	static uint32_t* BuildTownDepBits_old = (uint32_t*)0x006977E8;

	for (int b = 0; b < max_builds_old; ++b) {
		int index = alias * max_builds_old + b;
		requirements[town_type][b] = BuildFlags{
			BuildTownDepBits_old[2 * index],
			BuildTownDepBits_old[2 * index + 1],
			0
		};
	}
}



