#include "patcher_x86_commented.hpp"
#include <cstdio>

extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;

char text_TownsMapObject0[27][16] = {
	"AVCcast0.def", "AVCramp0.def", "AVCtowr0.def", "AVCinft0.def", "AVCnecr0.def", "AVCdung0.def", "AVCstro0.def", "AVCftrt0.def", "AVChfor0.def",
	"AVCcast0.def", "AVCramp0.def", "AVCtowr0.def", "AVCinft0.def", "AVCnecr0.def", "AVCdung0.def", "AVCstro0.def", "AVCftrt0.def", "AVChfor0.def",
	"AVCcast0.def", "AVCramp0.def", "AVCtowr0.def", "AVCinft0.def", "AVCnecr0.def", "AVCdung0.def", "AVCstro0.def", "AVCftrt0.def", "AVChfor0.def",
	// "BAD"
};

char text_TownsMapObject1[27][16] = {
	"AVCcasx0.def", "AVCramx0.def", "AVCtowx0.def", "AVCinfx0.def", "AVCnecx0.def", "AVCdunx0.def", "AVCstrx0.def", "AVCftrx0.def", "AVChforx.def",
	"AVCcasx0.def", "AVCramx0.def", "AVCtowx0.def", "AVCinfx0.def", "AVCnecx0.def", "AVCdunx0.def", "AVCstrx0.def", "AVCftrx0.def", "AVChforx.def",
	"AVCcasx0.def", "AVCramx0.def", "AVCtowx0.def", "AVCinfx0.def", "AVCnecx0.def", "AVCdunx0.def", "AVCstrx0.def", "AVCftrx0.def", "AVChforx.def",
	// "BAD"
};

char text_TownsMapObject2[27][16] = {
	"AVCcasz0.def", "AVCramz0.def", "AVCtowz0.def", "AVCinfz0.def", "AVCnecz0.def", "AVCdunz0.def", "AVCstrz0.def", "AVCforz0.def", "AVChforz.def",
	"AVCcasz0.def", "AVCramz0.def", "AVCtowz0.def", "AVCinfz0.def", "AVCnecz0.def", "AVCdunz0.def", "AVCstrz0.def", "AVCforz0.def", "AVChforz.def",
	"AVCcasz0.def", "AVCramz0.def", "AVCtowz0.def", "AVCinfz0.def", "AVCnecz0.def", "AVCdunz0.def", "AVCstrz0.def", "AVCforz0.def", "AVChforz.def"
	// "BAD"
};

char* new_TownsMapObject0[27]; char* new_TownsMapObject1[27]; char* new_TownsMapObject2[27];

char itpa_def_plus[16] =  "ITPA.def";
char itpa_def_minus[16] = "ITPA.def";
char itpt_def_plus[16] =  "ITPT+.def";

void patch_AdventureTowns(void) {
	for (int i = 0; i < 27; ++i) {
		new_TownsMapObject0[i] = text_TownsMapObject0[i];
		new_TownsMapObject1[i] = text_TownsMapObject1[i];
		new_TownsMapObject2[i] = text_TownsMapObject2[i];
	}
	Z_new_towns->WriteDword(0x004C97C1 + 3, (long)new_TownsMapObject2);
	Z_new_towns->WriteDword(0x004C980D + 3, (long)new_TownsMapObject1);
	Z_new_towns->WriteDword(0x004C9820 + 3, (long)new_TownsMapObject0);

	memset( (void*) 0x0070A9B3, 0x90, 0x0070AA38 - 0x0070A9B3);

	Z_new_towns->WriteDword(0x00401EA2 + 1, long(itpa_def_plus));

	Z_new_towns->WriteDword(0x005C3C90 + 1, long(itpa_def_plus));
	Z_new_towns->WriteDword(0x005C3CEA + 1, long(itpa_def_plus));
	Z_new_towns->WriteDword(0x005C3D44 + 1, long(itpa_def_plus));
	Z_new_towns->WriteDword(0x005C3D9E + 1, long(itpa_def_plus));

	Z_new_towns->WriteDword(0x00568145 + 1, long(itpa_def_minus));
	Z_new_towns->WriteDword(0x0057C791 + 1, long(itpa_def_minus));

	Z_new_towns->WriteDword(0x0045204D + 1, long(itpt_def_plus));
	Z_new_towns->WriteDword(0x0051C604 + 1, long(itpt_def_plus));
	Z_new_towns->WriteDword(0x0053076A + 1, long(itpt_def_plus));
	Z_new_towns->WriteDword(0x005C3DF8 + 1, long(itpt_def_plus));

	Z_new_towns->WriteByte(0x5BDB30, 27 * 2);
	Z_new_towns->WriteByte(0x513821 + 2, 27);
	Z_new_towns->WriteByte(0x513854 + 2, 27);
	Z_new_towns->WriteByte(0x5134D4 + 2, 27);
}

extern "C" __declspec(dllexport) void set_town_adventure_look(long town, char* look0, char* look1, char* look2) {
	if (look0) strcpy_s(text_TownsMapObject0[town], look0);
	if (look1) strcpy_s(text_TownsMapObject1[town], look1);
	if (look2) strcpy_s(text_TownsMapObject2[town], look2);
}