#pragma warning(disable:4996)

#include "patcher_x86_commented.hpp"
#include <cstdio>
// #include "HoMM3.h"
// #include "lib/H3API.hpp"
#include"../../__include__/H3API/single_header/H3API.hpp"

#include"_Town_.h"
//typedef h3::H3Town _Town_;
typedef h3::H3SimulatedCombat _AIQuickBattle_;

extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;
long SummonTG_Artifact = 2;


extern long ThirdUpgradesInTowns_27x2x7[27 * 2 * 7];
extern long MonstersInTowns_27x2x7[27 * 2 * 7];
extern long long additional_buildings_per_town[48];

extern long EighthMonType[27][4];

struct _HordeBuildingData_ {
	long Monster;
	short Count;
	short Level;
	long UpgMonster;
	short UpgCount;
	short UpgLevel;
};

extern _HordeBuildingData_ new_Hordes[27 * 2];

inline long get_Cathedral_Level(long TownID) {
	long ret = 0;
	for (int i = 78; i <= 84; ++i) {
		ret += (bool) (additional_buildings_per_town[TownID] & (1LL << (i-64)) );
	}
	ret += (bool)(additional_buildings_per_town[TownID] & (1LL << (84 - 64)));
	return ret;
}

extern "C" __declspec(dllexport) void SetTownGuardianSummonArtifact(long art) {
	SummonTG_Artifact = art;
}

/*
struct _AIQuickBattle_ {// (sizeof = 0x34, mappedto_343); XREF: MainLoop + 473 / o

	long field_0;//             dd ?
	long field_4;//             dd ? ; XREF: AI_Virtual_Battle + D6 / r
	long field_8;//             dd ? ; XREF: AI_Virtual_Battle + E6 / w
//00000008; AI_Virtual_Battle + 126 / w
	long field_C;//             dd ? ; XREF: AI_Virtual_Battle + E9 / w
//0000000C; AI_Virtual_Battle + 129 / w
	long specialTerrain;//      dd ?
	long spellPoints;//         dd ?
	char canCastSpells;//      db ?
	char field_19[3];//            db 3 dup(? )
	long armyStrength;//        dd ? ; XREF: AI_Virtual_Battle + BD / r
	char tactics;//             db ?
	char field_21[3];//            db 3 dup(? )
	long heroA;//               dd ? ; offset
	long army;//                dd ? ; offset
	long heroD;//              dd ? ; offset
	char turrets;//             db ?
	char field_31;//           db ?
	short turretsLevel; 
};
*/

constexpr long _AIQuickBattle_sizeof = sizeof(_AIQuickBattle_);
long TownGuardianType[27] = { 
	-1, -1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1,
};


extern "C" __declspec(dllexport) void SetTownGuardianType(long townID, long CreatureType) {
	TownGuardianType[townID] = CreatureType;
}

long calculate_TG_Count_per_level(void) {
	// long Month = h3::H3Internal::Main()->date.month - 1;
	long date = h3::H3Main::Get()->date.CurrentDay();
	long timefactor = (date / 14) + 2; long base = 1;

	return base + sqrt(timefactor * timefactor * timefactor);
	/*
	long long y2 = (date/14) + 2; long long y3 = y2 * y2;
	long long y4 = y3 * y2;   long long y5 = y3 * y4;
	y4 *= 100; y3 *= 10000;	long long y9 = y3 + y4 - y5;
	y9 *= 177; y9 /= 1000000; if (y2 > 18) y9 = (y2 - 4) * 37;

	return y9;
	*/
}

extern "C" __declspec(dllexport) void get_TG_Count(long* ret, int Town_ID) {
	// auto town_mgr = h3::H3Internal::TownManager();
	// auto bat_man = h3::H3Internal::CombatManager();
	long level = 1;

	/*
	if (town_mgr) {
		long TownID = town_mgr->town->number;
		level = get_Cathedral_Level(TownID);
	}
	*/
	if(Town_ID>=0 && Town_ID<=48)
		level = get_Cathedral_Level(Town_ID);


	(*ret) = calculate_TG_Count_per_level() * (level ? level + 2 : 0);
}

/*
_AIQuickBattle_* __stdcall AI_AICalcBattleSide_Create_Hook(HiHook* hook, _AIQuickBattle_* _this_, int heroA, _Army_* armyD, double Agress, _Hero_* heroD, _Town_* townSetup, _MapItem_* mip) {
	_AIQuickBattle_* ret = CALL_7(_AIQuickBattle_*, __thiscall, hook->GetDefaultFunc(), _this_, heroA, armyD, Agress, heroD, townSetup, mip);
	if (townSetup) if (townSetup->IsBuildingBuilt(78,0)) {
		long TG_Type = TownGuardianType[townSetup->type];
		long TG_Count = calculate_TG_Count();
		if (TG_Type >= 0 && TG_Count > 0) {
			//calculate from TG_Type and TG_Count
			ret->armyStrength += 0;// o_CreatureInfo[TG_Type];
		}
	}
	return ret;
}
*/

h3::H3SimulatedCreature create_TG_QBC(long TG_Type, long TG_Count) {
	auto TG_Creature = h3::H3CreatureInformation::Get()[TG_Type];
	h3::H3SimulatedCreature TG_Stack;
	TG_Stack.count = TG_Count; TG_Stack.type = TG_Type;
	TG_Stack.speed = TG_Creature.speed;
	TG_Stack.unitPower = TG_Creature.fightValue;
	TG_Stack.stackPower = TG_Stack.unitPower * TG_Count;
	return TG_Stack;
}

long create_TG_AS(long TG_Type, long TG_Count) {
	auto TG_Creature = h3::H3CreatureInformation::Get()[TG_Type];
	return TG_Count* TG_Creature.fightValue;
}

_LHF_(hook_0042400E) {
	_AIQuickBattle_* ret =(_AIQuickBattle_*) c->esi;
	_Town_* townSetup =(_Town_*) *(int*) (c->ebp + 0x1c);

	if (townSetup) if (townSetup->IsBuildingBuilt(78)) {
		long TG_Type = TownGuardianType[townSetup->type];
		long TG_Count = 0; get_TG_Count(&TG_Count, townSetup->number);

		/*
		long TG_Count = calculate_TG_Count() 
			* get_Cathedral_Level(townSetup->number);
		*/
		
		if (TG_Type >= 0 && TG_Count > 0) {
			//calculate from TG_Type and TG_Count
			// ret->armyStrength -= 1000000;// o_CreatureInfo[TG_Type];
			// ret->armyStrength -= create_TG_AS(TG_Type,TG_Count);
			// ret->creatures.Push(TG_Stack);

			// ret->creatures.Push(create_TG_QBC(TG_Type, TG_Count));
			ret->armyStrength -= create_TG_AS(TG_Type, TG_Count);
		}
	}

	c->esi = (long) ret;
	return EXEC_DEFAULT;
}


typedef char Byte;
typedef h3::H3Hero HERO;
Byte* PutStack(Byte* Bm, int Type, int Num, int Pos, int Side, HERO* hp, int Placed, int Stack);
_LHF_(hook_0046359E) {
	h3::H3CombatManager* Bm; int Side; int zPlaced;
	Bm = (h3::H3CombatManager*)c->ebx;
	Side = c->edi;

	//zPlaced = Context->ESI;
	//int *Placed = &zPlaced;
	int* Placed = &(c->esi);

	HERO* Hp; // Hp = (HERO*)*(Dword*)&Bm[0x53CC + Side * 4];
	Hp = Bm->hero[Side]; long base_count = calculate_TG_Count_per_level();

	if (Side == 1 && Bm->town) {
		long mult = 0;
		if (Bm->town->IsBuildingBuilt(78)) {
			long TG_Type = TownGuardianType[Bm->town->type];
			long TG_Count = 0; get_TG_Count(&TG_Count, Bm->town->number);
			/*
			long TG_Count = calculate_TG_Count();
			long mult = get_Cathedral_Level(Bm->town->number);
			*/
			if (Hp) {
				for (int i = 0; i < 19; ++i) {
					if (Hp->bodyArtifacts[i].id == SummonTG_Artifact) ++ mult;
				}
			}
			if (TG_Type >= 0 && TG_Count > 0) {

				PutStack((Byte*)Bm, TG_Type, TG_Count + base_count * mult, 149, Side, Hp, *Placed, -1);
				++* Placed;
			}
		}
	}
	else if (Hp) {
		long mult = 0;
		for (int i = 0; i < 19; ++i) {
			if (Hp->bodyArtifacts[i].id == SummonTG_Artifact) mult = 1;
		}
		if (mult > 0) {

			long TG_Type = TownGuardianType[Hp->hero_class / 2] ;
			long TG_Count = 0; get_TG_Count(&TG_Count, -1);
			//long TG_Count = calculate_TG_Count();


			if (TG_Type >= 0 && TG_Count > 0) {

				PutStack((Byte*)Bm, TG_Type, TG_Count + base_count * mult, Side ? 149 : 139, Side, Hp, *Placed, -1);
				++* Placed;
			}
		}
	}
	return EXEC_DEFAULT;
}

/*
_LHF_(hook_0042714C) {
	_Town_* townSetup = (_Town_*)*(int*) (c->ebp+0x0c);
	_AIQuickBattle_ * QB = (_AIQuickBattle_*) *(int*)(c->ebp - 0x7c);

	if (townSetup) if (townSetup->IsBuildingBuilt(78)) {
		long TG_Type = TownGuardianType[townSetup->type];
		long TG_Count = calculate_TG_Count();
		if (TG_Type >= 0 && TG_Count > 0) {
			QB->creatures.Push(create_TG_QBC(TG_Type, TG_Count));
		}
	}

	return EXEC_DEFAULT;
}
*/

extern bool cathedral_enabled;
extern bool EighthsSupported;

short __stdcall Town_GetGrowth_Hook(HiHook* hook, _Town_* town, int building) 
{
	long ret = 0;

	if (cathedral_enabled || (EighthsSupported && building >= 14))
	{ 
		long montype = -1; 
		long growth = 0;
		if (building < 14) 
		{
			if (!town->IsBuildingBuilt(building + 30)) return 0;
			montype = MonstersInTowns_27x2x7[14 * town->type + building];
		}
		else if (building < 16)
		{
			if (!town->IsBuildingBuilt(88)) return 0;
			montype = EighthMonType[town->type][0];
		}
		
		if (montype < 0 || montype >= 1024)
			return 0;
		ret = growth = h3::H3CreatureInformation::Get()[montype].grow;
		if (town->IsBuildingBuilt(h3::eBuildings::CASTLE)) {
			ret += growth;
		}
		else if (town->IsBuildingBuilt(h3::eBuildings::CITADEL)) {
			ret += growth >> 1;
		}
		if (town->owner >= 0) {
			int v9 = 0; int v11 = 0;
			if (THISCALL_2(char, 0x004BA970, &P_Game->players[town->owner], 133)) {
				if (town->IsBuildingBuilt(h3::eBuildings::CASTLE)) {
					v11 = growth;
				}
				else if (town->IsBuildingBuilt(h3::eBuildings::CITADEL)) {
					v11 = growth >> 1;
				}
				v9 = (growth + v11) >> 1;
			}
			ret += v9 + THISCALL_2(int, 0x005BFD00, town, building);
		}

		if (town->IsBuildingBuilt(h3::eBuildings::HORDE1)
			|| town->IsBuildingBuilt(h3::eBuildings::HORDE1U)) {
			auto& data = new_Hordes[2 * town->type];
			int horde_mon = building < 7 ? data.Monster : data.UpgMonster;
			if (horde_mon == montype)
				ret += building < 7 ? data.Count : data.UpgCount;
		}

		if (town->IsBuildingBuilt(h3::eBuildings::HORDE2)
			|| town->IsBuildingBuilt(h3::eBuildings::HORDE2U)) {
			auto& data = new_Hordes[2 * town->type + 1];
			int horde_mon = building < 7 ? data.Monster : data.UpgMonster;
			if (horde_mon == montype)
				ret += building < 7 ? data.Count : data.UpgCount;
		}

		if (town->IsBuildingBuilt(h3::eBuildings::GRAIL)) {
			ret += (ret >> 1);
		}

		ret *= (100 + get_Cathedral_Level(town->number) * 25);
		ret = ret / 100;
	}
	else
	    ret = CALL_2(short, __thiscall, hook->GetDefaultFunc(), town, building);

	return ret;
}

/*
_LHF_(NPCOnlyInCastle_Proxy) {

	return EXEC_DEFAULT;
}
*/

_LHF_(NPCOnlyInCastle2_Proxy) {
	int TownID = *(char*)c->esi;
	if (get_Cathedral_Level(TownID)) {
		c->return_address = 0x004AAF4A;
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}


_LHF_(sub_005BE3E0_proxy) {
	int TownID = *(char*) c->ecx;
	if (get_Cathedral_Level(TownID)) {
		c->return_address = 0x005BE428;
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}

_LHF_(hook_005D39A5_Clicked_Castle) {
	auto town_mgr = h3::H3TownManager::Get();
	if (get_Cathedral_Level(town_mgr->town->number)) {
		char buf[512]; long count; 
		get_TG_Count(&count, town_mgr->town->number);
		sprintf(buf,"You Have %i Guardians in this Town.", count);
		h3::H3Messagebox((char*)buf);
	}

	return EXEC_DEFAULT;
}

void apply_TownDefender(void) {
	// memset(TownGuardianType, -1,sizeof(TownGuardianType));
	// Z_new_towns->WriteHiHook(0x00423EE0, SPLICE_, EXTENDED_, THISCALL_, AI_AICalcBattleSide_Create_Hook);
	 Z_new_towns->WriteLoHook(0x0042400E, hook_0042400E);
	 Z_new_towns->WriteLoHook(0x0046359E, hook_0046359E);
	// Z_new_towns->WriteLoHook(0x0042714C, hook_0042714C	);

	Z_new_towns->WriteHiHook(0x005BFF60, SPLICE_, EXTENDED_, THISCALL_, Town_GetGrowth_Hook);


	//Z_new_towns->WriteLoHook(0x5BE41C, NPCOnlyInCastle_Proxy);
	Z_new_towns->WriteLoHook(0x4AADD1, NPCOnlyInCastle2_Proxy);

	Z_new_towns->WriteLoHook(0x005BE3E0, sub_005BE3E0_proxy);

	Z_new_towns->WriteLoHook(0x005D39A5, hook_005D39A5_Clicked_Castle);
}