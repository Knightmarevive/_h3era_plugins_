#include "patcher_x86_commented.hpp"
#include <cstdio>
#include <string>
#include "common.hpp"

extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;

// #include "HoMM3.h"
// #include"lib/H3API.hpp"
#pragma warning(disable : 4996)

// #define _H3API_EXCEPTION_
#include"../../__include__/H3API/single_header/H3API.hpp"
#include "_Town_.h"

inline int z_MsgBox(char* Mes, int MType, int PosX, int PosY, int Type1, int SType1, int Type2, int SType2, int Par, int Time2Show, int Type3, int SType3) {
	CALL_12(int, __fastcall, 0x004F6C00, Mes, MType, PosX, PosY, Type1, SType1, Type2, SType2, Par, Time2Show, Type3, SType3);

	int IDummy;
	__asm {
		mov eax, 0x6992D0
		mov ecx, [eax]
		mov eax, [ecx + 0x38]
		mov IDummy, eax
	}
	return IDummy;
}

extern bool Investment_enabled;
extern bool market_of_time_enabled;
extern long ThirdUpgradesInTowns_27x2x7[27 * 2 * 7];
extern int ArtifactMerchant[27];
extern int University[27][10];
int university_skill[10][4] = {
	{14,15,16,17},
	{3,5,11,25},
	{1,10,19,27}
};
extern int upper[27][128];

struct _HordeBuildingData_ {
	long Monster;
	short Count;
	short Level;
	long UpgMonster;
	short UpgCount;
	short UpgLevel;
};

extern _HordeBuildingData_ new_Hordes[27 * 2];
extern char text_TownsHallDefs[27][16];

extern long multiple_builds_in_town[48];
extern int bonus_gold_per_town[48];
int income_upgrade_in_town[48] = {};

bool check_horde_unit(h3::H3Town* town, int building, int level) {
	// First set of horde buildings (18, 19)
	if ((building == 18 && town->IsBuildingBuilt(18)) ||
		(building == 19 && town->IsBuildingBuilt(19))) {
		_HordeBuildingData_* horde = new_Hordes + 2 * (town->type);
		if (horde->Monster < 0) return false;
		return (level == horde->Level);
	}

	// Second set of horde buildings (24, 25)
	if ((building == 24 && town->IsBuildingBuilt(24)) ||
		(building == 25 && town->IsBuildingBuilt(25))) {
		_HordeBuildingData_* horde = new_Hordes + 2 * (town->type);
		if (horde->Monster < 0) return false;
		return (level == horde->Level);
	}

	return false;
}

extern "C" __declspec(dllexport) void fix_null_town_names() {

	for (auto& town : P_Main->towns) {
		if (town.name.Empty())
			town.name = "Unnamed";
	}
}


extern "C" __declspec(dllexport) long mon_upgraded_in_town(long non_upgraded);
_LHF_(hook_0054FFFA) {
	static int last_mon_id = 139;

	int& mon_id = c->edi;
	if ((mon_id >= 1024 || mon_id < 0) && last_mon_id != 139)
		mon_id = mon_upgraded_in_town(last_mon_id);

	if (mon_id >= 1024 || mon_id < 0)
		mon_id = 139;

	last_mon_id = mon_id;
	return EXEC_DEFAULT;
}

extern long TownGuardianType[27];
INT16 grail_creature_amount[48];
void recruit_in_grail(h3::H3TownManager* _this_) {
	h3::H3Town* town = _this_->town;
	h3::H3Army* army = nullptr;
	h3::H3Hero* hero = town->GetGarrisonHero();
	if (hero) army = &hero->army;
	else army = &town->guards;
	INT32 creature = TownGuardianType[town->type];
	if (creature < 0) return;

	//return;
	h3::H3RecruitMgr recruit(*army, false, creature, grail_creature_amount[town->number]);
	recruit.Run();

	_this_->RefreshScreen();
}

inline int Town_GetGrowth(_Town_* town, int building) {
	return CALL_2(short, __thiscall, 0x005BFF60, town, building);
}

long EighthMonType[27][4];
INT16 eighth_creature_amount[48];
void recruit_in_grail__income() {
	for (INT16& amount : grail_creature_amount) {
		++amount;
	}

	for (auto& it : h3::H3Main::Get()->towns) {
		if (it.IsBuildingBuilt(88))
		{
			/*
			eighth_creature_amount[it.number]
				+= h3::H3CreatureInformation::Get()
				[EighthMonType[it.number]].grow;
			*/
			eighth_creature_amount[it.number] +=
				Town_GetGrowth((_Town_*) &it, 15);
		}
	}
}

int recruit_eighths(h3::H3Msg* arg) {
	h3::H3Town* town = P_TownManager->town;
	h3::H3Army* army = nullptr;
	h3::H3Hero* hero = town->GetGarrisonHero();
	if (hero) army = &hero->army;
	else army = &town->guards;
	INT32 *creatures = (INT32*) EighthMonType[town->type];
	if (*creatures < 0) return 0;
	INT16& amount = eighth_creature_amount[town->number];
	int ret = 0;

	/*
	if (false);
	else if (town->IsBuildingBuilt(89)) {
		h3::H3RecruitMgr recruit(*army, false, creatures[1], amount, creatures[0], amount);
		recruit.Run();
	}
	else if (town->IsBuildingBuilt(88)) {
		h3::H3RecruitMgr recruit(*army, false, creatures[0], amount);
		recruit.Run();
	}
	*/
	if (town->IsBuildingBuilt(91) && creatures[3] >= 0)
		ret = THISCALL_11(int, 0x551750, arg, army,
			0, creatures[3], &amount, creatures[2], &amount, creatures[1], &amount, creatures[0], &amount);
	else if (town->IsBuildingBuilt(90) && creatures[2] >= 0)
		ret = THISCALL_11(int, 0x551750, arg, army,
			0, creatures[2], &amount, creatures[1], &amount, creatures[0], &amount, -1, 0);
	else if (town->IsBuildingBuilt(89) && creatures[1] >= 0)
		ret = THISCALL_11(int, 0x551750, arg, army,
			0, creatures[1], &amount, creatures[0], &amount, -1, 0, -1, 0);
	else if (town->IsBuildingBuilt(88) && creatures[0] >= 0)
		ret = THISCALL_11(int, 0x551750, arg, army, 
			0, creatures[0], &amount, -1, 0, -1, 0, -1, 0);

	// _this_->RefreshScreen();
	return ret;
}

// 0x005D4194
void LABEL_133_134(h3::H3TownManager* _this_, bool L133, h3::H3GarrisonInterface* v41) {
	if (L133) {
		v41->ownerId = -2;
	}
	auto v55 = _this_->bottom;
	((char*)_this_)[0x1c4] = 0;
	CALL_2(void, __thiscall, 0x005AA090, v55, -1);
	CALL_2(void, __thiscall, 0x005AA090, _this_->top, -1);
	_this_->recipientPage = 0; _this_->source = 0;
	_this_->recipientPageStack = _this_->sourcePageStack = -2;
}

void town_university(int subtype = 0) {
	Z_new_towns->WriteDword(0x005D330A + 3, university_skill[subtype][0]);
	Z_new_towns->WriteDword(0x005D3311 + 3, university_skill[subtype][1]);
	Z_new_towns->WriteDword(0x005D3318 + 3, university_skill[subtype][2]);
	Z_new_towns->WriteDword(0x005D331F + 3, university_skill[subtype][3]);

	auto _this_ = h3::H3TownManager::Get();

	Z_new_towns->WriteByte(0x005EFAA5 + 1, University[_this_->town->type][subtype]);
	Z_new_towns->WriteDword(0x005EFAA7 + 1, text_TownsHallDefs[_this_->town->type]);

	CALL_1(void, __thiscall, 0x005D31A0, _this_);
	_this_->RefreshScreen();
}

extern long last_town_ID;
extern long last_town_click;
// extern long clickedDwelling;
bool town_apply_building_click(h3::H3TownManager* _this_, INT32 building_ID) {
	if (((_Town_*)(_this_->town))->IsBuildingBuilt(building_ID)==0)
		return false;

	auto ttt = _this_->townType;
	if (building_ID == ArtifactMerchant[ttt]) {
		CALL_0(void*, __cdecl, 0x005EA120);
		_this_->RefreshScreen();
		auto v54 = _this_->source;
		if (v54) v54->ownerId = -2;
		auto v41 = _this_->recipientPage;
		if (!v41) {
			LABEL_133_134(_this_, false, v41);
		}
		else {
			LABEL_133_134(_this_, true, v41);
		}
		if (upper[ttt][building_ID] >= 0) {
			town_apply_building_click(_this_, upper[ttt][building_ID]);
		}
		return true;
	}

	for(int i=0;i<10;++i)
		if (building_ID == University[ttt][i]) {
			auto hero = _this_->town->GetVisitingHero();
			if (hero)
				town_university(i);
			if (upper[ttt][building_ID] >= 0) {
				town_apply_building_click(_this_, upper[ttt][building_ID]);
			}
			return true;
		}


	if (upper[ttt][building_ID] >= 0) {
		return town_apply_building_click(_this_, upper[ttt][building_ID]);
	}
	return false;
}


extern "C" __declspec(dllexport) int Market_of_Time_default(h3::H3Hero * Hero);
int zMarketOfTime(h3::H3Hero* h) {
	typedef int(*fun)(h3::H3Hero*);
	static fun ptr = nullptr;
	if (ptr) return ptr(h);

	auto lib1 = GetModuleHandleA("more_SS_levels.era");
	if (lib1) ptr = (fun)GetProcAddress(lib1, "Market_of_Time");
	else ptr = Market_of_Time_default;

	if (ptr) return ptr(h);
	else return 0;
}

inline void ShowMessageBox(const char* title, const char* message) {
	MessageBoxA(NULL, message, title, MB_OK | MB_ICONINFORMATION);
}

inline void ShowMessageBoxWithInt(const char* title, const char* prefixMessage, int value) {
	char message[256];
	sprintf(message, "%s: %d", prefixMessage, value);
	ShowMessageBox(title, message);
}

// Updated function - Trebuchet
signed int __stdcall town_TownDlg_Proc_hook(HiHook* h, h3::H3TownManager* _this_, h3::H3Msg* msg) {
	last_town_click = msg->itemId;
	last_town_ID = _this_->town->number;
	auto ttt = _this_->townType;

	int real_click = msg->itemId;
	__asm {
		mov ebx, _this_
		push[0x8912A8]// msg
		push ebx
		mov eax, 0x74F48A
		call eax
		add esp, 8
		mov real_click, eax;
	}

	if (real_click == 158 && Investment_enabled &&
		( msg->IsLeftDown() || msg->IsLeftClick() )) {
		if (!(_this_->town->IsBuildingBuilt(12)|| _this_->town->IsBuildingBuilt(13))) {
			h3::H3Messagebox("Cannot build Investment without City Hall");
			return 1;
		}

		int next_level = income_upgrade_in_town[_this_->town->number] + 1;
		int gold_cost = 6600 + 100*(3+next_level) * next_level;
		int mithril_cost = (next_level+3) / 2;
		int player = _this_->town->owner;
		int& current_gold = h3::H3ActivePlayer::Get()->playerResources.gold;
		int& current_mithril = ((int*)0x027F9A00) [player];
		long& building_count = multiple_builds_in_town[_this_->town->number];

		bool can_afford = current_gold >= gold_cost && current_mithril >= mithril_cost;
		bool can_build = can_afford && (building_count > 0);
		if (can_build) {
			if (z_MsgBox("Do You want to Invest these resources for 1000 income",2,-1,-1,
				6,gold_cost,7,mithril_cost,-1,0,-1,0) == 0x7805) {
				
				current_gold -= gold_cost;
				current_mithril -= mithril_cost;
				income_upgrade_in_town[_this_->town->number] = next_level;
				bonus_gold_per_town[_this_->town->number] += 1000;

				--building_count;
				if (!building_count) 
					_this_->town->builtThisTurn = true;

				_this_->RefreshScreen();
			}
		}
		else {
			if (!can_afford) z_MsgBox("Not enough Resources", 1, -1, -1,
				6, gold_cost, 7, mithril_cost, -1, 0, -1, 0);
			else h3::H3Messagebox("Cannot build more buildings now");
		}

		return 1;
	}
	if (msg->itemId == 14 && msg->IsLeftDown() && _this_->town->GetVisitingHero() &&
		msg->IsCtrlPressed() && _this_->town->IsBuildingBuilt(87)) {

		auto hero = _this_->town->GetVisitingHero();
		int points = zMarketOfTime(hero);
		CALL_4(int, __thiscall, 0x004E3620, hero, points * 250, 1, 1);

		return 1;
	}

	if (msg->itemId >= 0 && msg->itemId < 44 && msg->IsLeftDown()) {
		/*
		if (cmd[2] < 17 && town_apply_building_click(_this_, cmd[2] + 10))
			return 1;
			*/
		

		if (town_apply_building_click(_this_, real_click))
			return 1;
	}

	if (msg->itemId == 26 && msg->IsLeftDown()) {
		/*
		h3::H3SEHandler seh;
		try {
			auto man = h3::H3ExecutiveMgr::Get()->first_mgr;
			h3::H3Messagebox(std::to_string((long)man).c_str());
			recruit_in_grail(_this_);

		}
		catch (const h3::H3Exception& e) {
			e.ShowInGame();
		}
		*/ recruit_in_grail(_this_);
		return CALL_2(int, __thiscall, h->GetDefaultFunc(), _this_, msg);
	}

	/*
	if (msg->itemId >= 0 && msg->itemId < 44 && msg->IsLeftDown() && 
		upper[_this_->townType][msg->itemId] == 88 && msg->IsCtrlPressed()
		&& _this_->town->IsBuildingBuilt(88)) {
			recruit_eighths(_this_);
			return CALL_2(int, __thiscall, h->GetDefaultFunc(), _this_, msg);
	}
	*/

	if ( msg->IsLeftDown() || msg->IsLeftClick()) {
		int mouseX = msg->GetCoords().x;
		int mouseY = msg->GetCoords().y;

		const int itemWidth = 32;
		const int itemHeight = 32;
		const int buttonWidth = 48;
		const int buttonHeight = 34;
		const int offsetX = (buttonWidth - itemWidth) / 2;
		const int offsetY = (buttonHeight - itemHeight) / 2;

		bool foundDwellingClick = false;
		long clickedDwelling = -1;

		for (int i = 0; i < 8; ++i) {
			h3::H3DlgItem* item = _this_->dlg->GetH3DlgItem(164 + i);
			if (!item) continue;

			int absX = item->GetAbsoluteX() - offsetX;
			int absY = item->GetAbsoluteY() - offsetY;

			if ((mouseX >= absX && mouseX <= absX + buttonWidth) &&
				(mouseY >= absY && mouseY <= absY + buttonHeight)) {
				last_town_click = 164 + i;
				if (i == 7 && EighthMonType[ttt][0] >= 0
					&& _this_->town->IsBuildingBuilt(88)) {
						void* arg = h3::H3Malloc(0xbc);
						int ret = recruit_eighths((h3::H3Msg*)arg); 
						THISCALL_2(VOID, 0x4B0770, h3::H3ExecutiveMgr::Get(), ret);
						h3::H3Free(arg); _this_->RefreshScreen();	break;
				}
				foundDwellingClick = true;
				
				/*
				for (int j = i + 1; j; --j)
					if(_this_->town->IsBuildingBuilt(30+j)==0)
						++i;
				*/
				clickedDwelling = i;
				// last_town_click = clickedDwelling + 30;
				/*
				msg->itemId = _this_->town->IsBuildingBuilt(i+37)
					? i + 37 : i + 30;
				*/
				break;
			}
		}

		out:
		if (!foundDwellingClick) {
			last_town_click = msg->itemId;
		}

		last_town_ID = _this_->town->number;
	}

	return CALL_2(signed int, __thiscall, h->GetDefaultFunc(), _this_, msg);
}

// 0x005D3640
signed int __stdcall town_TownDlg_Proc_hook_BAK(HiHook* h, h3::H3TownManager* _this_, h3::H3Msg* msg) {
	if (last_town_click == -2) {
		last_town_click = -1;
		return 1;
	}

	last_town_click = msg->itemId;
	last_town_ID = _this_->town->number;
	auto ttt = _this_->townType;

	if (msg->itemId == 14 && msg->IsLeftDown() && _this_->town->GetVisitingHero() &&
		msg->IsCtrlPressed() && _this_->town->IsBuildingBuilt(87)) {
		
		auto hero = _this_->town->GetVisitingHero();
		int points = zMarketOfTime(hero);
		CALL_4(int, __thiscall, 0x004E3620, hero, points * 250, 1, 1);

		return 1;
	}

	if (msg->itemId >=0 && msg->itemId < 44 && msg->IsLeftDown()) {
		/*
		if (cmd[2] < 17 && town_apply_building_click(_this_, cmd[2] + 10))
			return 1;
			*/
		int real_click = msg->itemId;
		__asm {
			mov ebx, _this_
			push [0x8912A8]// msg
			push ebx
			mov eax, 0x74F48A
			call eax
			add esp, 8
			mov real_click, eax;
		}

		if (town_apply_building_click(_this_, real_click))
			return 1;
	}

	if (msg->itemId == 26 && msg->IsLeftDown()) {
		/*
		h3::H3SEHandler seh;
		try {
			auto man = h3::H3ExecutiveMgr::Get()->first_mgr;
			h3::H3Messagebox(std::to_string((long)man).c_str());
			recruit_in_grail(_this_);

		}
		catch (const h3::H3Exception& e) {
			e.ShowInGame();
		}
		*/ recruit_in_grail(_this_);
		return CALL_2(int, __thiscall, h->GetDefaultFunc(), _this_, msg);
	}
	else if (msg->itemId >= 164 && msg->itemId <= 170 && msg->IsLeftDown()) {
		// cmd[2] -= (164 - 30); cmd[0] = 0x0;
		/*
		if (cmd[0] != 0x512) {
			h3::F_MessageBox("debug");
		}
		*/

		int btn = msg->itemId - 164; int level = 0;
		for (int i = 0; i <= btn; ++i)
		{
			int dwellingLevel = 0;
			do
			{

				if (_this_->town->IsBuildingBuilt(dwellingLevel + i + 30)
					|| _this_->town->IsBuildingBuilt(dwellingLevel + i + 37)
					|| check_horde_unit(_this_->town, 18, dwellingLevel + i))
				{
					level += dwellingLevel + 1;
					break;
				}
				dwellingLevel++;
			} while ((dwellingLevel + i + 37) <= 43);
		}

		if (level) --level;
		if (level < 0 || level>6) {
			// level = 0; 
			return 1;
		}
		if (!_this_->town->IsBuildingBuilt(level + 30)
			&& !_this_->town->IsBuildingBuilt(level + 37)
			&& !check_horde_unit(_this_->town, 18, level))
			return 1;

		last_town_click = 30 + level;


		if (_this_->town->IsBuildingBuilt(last_town_click + 7)) {
			level += 7; last_town_click += 7;
		}


		char* v56 = new char[188];
		INT64* o_RecuitArmy = THISCALL_4(INT64*, 0x00551960, v56, &_this_->town->number, level, 1);
		THISCALL_2(int, 0x004B0770, *(int*)0x00699550, o_RecuitArmy);
		delete[] v56;
		_this_->RefreshScreen();
		return 1;
	}

	return CALL_2(int, __thiscall, h->GetDefaultFunc(), _this_, msg);
}

// creature speciality
_LHF_(hook_004E6502) {
	if (c->ecx == c->eax) {
		c->ebx = c->ecx;
		c->return_address = 0x004E650C;
		return NO_EXEC_DEFAULT;
	}
	else if (c->eax < 0) {
		c->return_address = 0x004E65EA;
		return NO_EXEC_DEFAULT;
	}
	else {
		c->ecx = c->eax;
		c->return_address = 0x004E64FA;
		return NO_EXEC_DEFAULT;
	}

	/*
	if ( c->ebx == c->eax || c->eax < 0) {
		c->return_address = 0x004E65EA;
		return NO_EXEC_DEFAULT;
	} else if (c->ecx == c->eax) {
		c->ebx = c->ecx;
		c->return_address = 0x004E650C;
		return NO_EXEC_DEFAULT;
	}
	else {
		c->ebx = c->ecx;
		c->ecx = c->eax;
		c->return_address = 0x004E64FA;
		return NO_EXEC_DEFAULT;
	}
	*/
}

_LHF_(hook_005C651F) {
	h3::H3TownDialog* dlg = (h3::H3TownDialog*)*(int*)(c->esp + 0xB8);
	h3::H3Town* town = (h3::H3Town*)(c->esi);
	auto TUM_Base = ThirdUpgradesInTowns_27x2x7 + 14 * town->type;

	//dlg->GetDef(164)->SetFrame(198);
	for (int btn = 0; btn < 7; ++btn) {
		int level = 0;
		for (int i = 0; i <= btn; ++i)
			if (town->IsBuildingBuilt(i + 30)
				|| town->IsBuildingBuilt(i + 37)
				|| check_horde_unit(town, 18, i))
				++level; else level += 2;

		/* if (level) */ --level;
		if (level < 0 || level>6) {
			// level = 0; 
			return EXEC_DEFAULT;
		}

		if (TUM_Base[level] >= 0 && town->IsBuildingBuilt(level + 64))
			dlg->GetDef(164 + btn)->SetFrame(2 + TUM_Base[level]);
		if (TUM_Base[level + 7] >= 0 && town->IsBuildingBuilt(level + 71))
			dlg->GetDef(164 + btn)->SetFrame(2 + TUM_Base[level + 7]);
	}

	return EXEC_DEFAULT;
}


extern "C" __declspec(dllexport) void apply_all_town_fixes_late() {
	static bool once = true;
	fix_null_town_names();
	if (once) {
		Z_new_towns->WriteLoHook(0x0054FFFA, hook_0054FFFA);
		Z_new_towns->WriteHiHook(0x005D3640, SPLICE_, EXTENDED_, THISCALL_, town_TownDlg_Proc_hook);

		Z_new_towns->WriteLoHook(0x004E6502, hook_004E6502);
		Z_new_towns->WriteLoHook(0x005C651F, hook_005C651F);
	}

	// Fixing built-in error check and exception thrown if town > 9 	
	Z_new_towns->WriteByte(0x0042C778 + 2, max_towns_types + 1);
	Z_new_towns->WriteByte(0x0042C8AD + 2, max_towns_types + 1);
	Z_new_towns->WriteByte(0x0042C8D1 + 2, max_towns_types + 1);	
	Z_new_towns->WriteByte(0x004331FA + 2, max_towns_types + 1);  
	Z_new_towns->WriteByte(0x004332E8 + 2, max_towns_types + 1);

	once = false;
}