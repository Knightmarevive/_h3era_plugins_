#pragma once

#include "HoMM3_ids.h"
#include "HoMM3_Base.h"
#include "HoMM3_Res.h"
#include "HoMM3_GUI.h"


//#include <ddraw.h>



#define EXE_VERSION *(_dword_*)0x588479
#define TE 0x001F0E12
#define WOG 0x001EC3A3
#define SOD 0xFFFFE403

// ���������� ���� � �������� ����� ����.
#define MAIN_ABSPATH *(_cstr_)0x698614

//#define o_HEROES_COUNT 156
//#define o_CREARURES_COUNT 150
#define o_HEROES_COUNT (*(_int_*)0x4BD145)
#define o_CREARURES_COUNT (*(_int_*)0x5C8047)




struct _Army_;
struct _CreatureInfo_;
struct _HeroInfo_;
struct _TurnTimeStruct_;
struct _MapItem_;
struct _Artifact_;
struct _ArtInfo_;
struct _Hero_;
struct _Player_;
struct _Town_;
struct _MapGlobalEvent_;
struct _BattleAnim_;
// ����� ����.
struct _GameMap_;
// ������ �� ����� �����������.
struct _Object_;
// ������ - ������ ����� (��� �������� �����).
struct _PlaceHolder_;
// ������ ������.
struct _HeroesList_;
// ������� ������ �� �����.
struct _Dwelling_;

// ��������� ���� ���������� �������� ����.
struct _ZoneSettings_;




// ���������� ����. ��������� �� ��� ��� �� ����������.
// _bool32_ __cdecl sub_4EE1D0() - ������������� ����������. �� ��� ��������� �� ������.

// Some Mgr �� 0x699550, ������: 16

// �������� �����, ����������� ��������� �����.
struct _InputMgr_; // ������: 2400
#define o_InputMgr (*(_InputMgr_**)0x699530)

// �������� ����.
struct _MouseMgr_; // ������: 144
#define o_MouseMgr (*(_MouseMgr_**)0x6992B0)

// �������� ���� ���������.
struct _WndMgr_; // ������: 96
#define o_WndMgr (*(_WndMgr_**)0x6992D0)

// �������� �����.
struct _SoundMgr_; // ������: 216
#define o_SoundMgr (*(_SoundMgr_**)0x699414)

// Some Mgr �� 0x69941C, ������: 2260

// �������� ����.
struct _GameMgr_; // ������: 321488
#define o_GameMgr (*(_GameMgr_**)0x699538)

// �������� ����� �����������.
struct _AdvMgr_; // ������: 952
#define o_AdvMgr (*(_AdvMgr_**)0x6992B8)

// �������� �����.
struct _BattleMgr_; // ������: 82156
#define o_BattleMgr (*(_BattleMgr_**)0x699420)

// �������� ���� ������.
struct _TownMgr_; // ������: 472
#define o_TownMgr (*(_TownMgr_**)0x69954C)

// Some Mgr �� 0x6992D4, ������: 112

// Some Mgr �� 0x6992DC, ������: 1










#define o_ScreenBPP (*(_int_*)0x68C8C0)
#define o_ScreenWidth (*(_int_*)0x68C8C4)
#define o_ScreenHeight (*(_int_*)0x68C8C8)
#define o_DD (*(LPDIRECTDRAW*)0x6AAD20)
#define o_DDSurfacePrimary (*(LPDIRECTDRAWSURFACE*)0x6AAD24)
#define o_DDSurfaceBackBuffer (*(LPDIRECTDRAWSURFACE*)0x6AAD28)
#define o_DDSurface6AAD2C (*(LPDIRECTDRAWSURFACE*)0x6AAD2C)
#define o_DDSurface6AAD30 (*(LPDIRECTDRAWSURFACE*)0x6AAD30)
#define o_DDSurface6AAD34 (*(LPDIRECTDRAWSURFACE*)0x6AAD34)
#define o_FullScreenMode (*(_bool_*)0x698808)
#define o_hWnd (*(HWND*)0x699650)

#define HERO_INFO_OFFSET (*(_ptr_*)0x67DCE8)
//#define o_HeroInfo ((_HeroInfo_*)0x679DD0)
#define o_HeroInfo ((_HeroInfo_*)HERO_INFO_OFFSET)
#define o_pHeroInfo (*(_HeroInfo_**)0x67DCE8)


#define o_NetworkGame *(_bool_*)0x69959C
//#define o_CampaignGame *(_bool_*)0x69779C
#define o_ActivePlayer (*(_Player_**)0x69CCFC)
#define o_ActivePlayerID *(_int_*)0x69CCF4
#define o_MeID *(_int_*)0x6995A4

#define o_Market_Hero (*(_Hero_**)0x6AAAE0)
#define o_DirectPlayCOMObject *(_ptr_*)0x69D858

//#define CREATURE_INFO_OFFSET (*(_ptr_*)0x47ADD1)
#define CREATURE_INFO_OFFSET (*(_ptr_*)0x6747B0)
//#define o_CreatureInfo ((_CreatureInfo_*)0x6703B8)
#define o_CreatureInfo ((_CreatureInfo_*)CREATURE_INFO_OFFSET)
#define o_pCreatureInfo (*(_CreatureInfo_**)0x6747B0)

#define o_GameMgrType (*(_int_*)0x698A40)
#define o_QuickBattle (*(_int_*)0x6987CC)
#define o_AutoSolo *(_byte_*)0x691259

#define o_CombatOptionsDlg (*(_Dlg_**)0x694FE0)
#define o_MapWidth *(_dword_*)0x6783C8
#define o_MapHeight *(_dword_*)0x6783CC
#define o_ViewingWorldEarthAirNow *(_dword_*)0x6AACA4
#define o_ViewWorldEarthAirMapCellSize *(_float_*)0x68C708
#define o_MusicVolume (*(_byte_*)0x6987B0)
#define o_TextBuffer ((char*)0x697428)
#define o_TurnTimer ((_TurnTimeStruct_*)0x69D680)
#define o_ScreenLogStruct ((_ptr_)0x69D800)
#define o_ArtInfo (*(_ArtInfo_**)0x660B68)
#define o_RegKeyPath ((_char_*)0x67FE78)
#define o_RegKey_AppPath ((_char_*)0x67FC10)
#define o_RegKey_CDDrive ((_char_*)0x67FC08)

#define o_CurrentDlg (*(_Dlg_**)0x698AC8)

#define o_HeroDlg_Hero (*(_Hero_**)0x698B70)
#define o_HeroDlg_SelStackIndex (*(_int_*)0x697788)


//#define o_BattleAnimation ((_BattleAnim_*)0x641E18)
//extern _BattleAnim_* o_BattleAnimation;
#define o_BattleAnimation (*(_BattleAnim_**)0x5A5036)

extern _int_ BattleAnims_Count;


#define p_NewScenarioDlg (*(_ptr_*)0x69FC44)

#define b_unpack_z(xyz) (((_int16_)(((_dword_)(xyz)) >> 14)) >> 12)
#define b_unpack_y(xyz) (((_int16_)(((_dword_)(xyz)) >> 10)) >> 6)
#define b_unpack_x(xyz) (((_int16_)(((_dword_)(xyz)) << 6)) >> 6)
#define b_pack_xyz(x, y, z) (_dword_)( (((_dword_)(((_word_)(y)) & 0x3FF)) << 16) | ((_dword_)(((_word_)(x)) & 0x3FF)) | (((_dword_)(((_word_)(z)) & 0xF)) << 26) )


// ������� ������������� ������.
#define Heroes_Spec_Table (_ptr_)0x679C80


// ������� ���� - ���������� ��������.
#define Game_Is_BuiltInCampaing (*(_bool8_*)0x69779C)


// ������� ����������.
#define o_Spell (*(_Spell_**)0x687FA8)


// ����������� ����� ����� ������� (user).
#define MIN_FRAME_PERIOD 10




// ���������� ������ ���� ���.
#define BATTLE_HEXES_COUNT 187


// ���������� ������ � ������ ������� �� ���� ���.
#define BATTLE_SIDE_STACKS_COUNT 21


// ���������� �������� �������� ��������� ����������� � ���.
#define BATTLE_MAPPING_PRIORITIES_COUNT 8



// ���������� ��������� �����.
#define CASTLE_ELEMENTS_COUNT 8




// ����� �������� ������ ����� ���������� ����� ����������������� �������� ��� ����������.
#define ShotFrameTime (*(_float_*)0x63B8A0)




// �����, �� �������� ����� ������������� �������� ��� ��������� ���������.
// ��������������� ������ ���������� ��� ���������...
// ...�����, ���� �� �������� ����� ���� ���������� �����, ��� ������������� ������� �� ������� ��������.
#define DrawingWaitTime (*(_dword_*)0x6989E8)



// ����� ���������� ��������� �������� �������� � ���.
#define WaitAnimTime (*(_dword_*)0x698A08)



// ������ ���������� �������� �������� �������� ��� ��������� �������� ���.
#define BattleAnimPeriodFactors ((_float_*)0x63CF7C)

// ��������� �������� ��� (����� ������� ��������� �������� �������� � �������).
#define Settind_BattleFast (*(_dword_*)0x69883C)


// ��������� ��������� ����������. ������: 768 ����.
#define H3TempStr ((char*)0x697428)


// ������ ����������.
#define EmptyVar (*(DWORD*)0x691260)


// ������ ����.
#define EmptySample (*(_Sample_*)0x6992A8)


// ����������� ������ ��������.
#define STD_FRAME_PERIOD 100




// ������� ����������� ���� ���.
#define BattleRedraw_Borders (*(_RedrawBorders_*)0x694F68)




// ������ �������� �����.
#define CastleElements ((_CastleElement_*)0x63BE60)


// ������ ������ ���������� ����� �� ����� ���� ���.
#define CastleWall_Gexes ((_byte_*)0x63BD00)



//NOALIGN struct _XYZ_
//{
// _dword_ z : 6;
// _dword_ y : 10;
// _dword_ x : 10;
//}



NOALIGN struct _Resources_
{
 _int_ wood;
 _int_ mercury;
 _int_ ore;
 _int_ sulfur;
 _int_ crystal;
 _int_ jems;
 _int_ gold;
};

NOALIGN struct _Army_
{
 _int_ type[7];
 _int_ count[7];
 
 // ����������.
 inline _Army_* Construct()
 {
   return CALL_1(_Army_*, __thiscall, 0x44A750, this);
 }
 
 // normal
 inline int GetStacksCount() {return CALL_1(int, __thiscall, 0x44A990, this);}
 inline int GetCreaturesCount() {return CALL_1(int, __thiscall, 0x44AA70, this);}
 inline void SwapStackTo(int ix, _Army_* dst_army, int dst_ix) {CALL_4(void, __thiscall, 0x44AA30, this, ix, dst_army, dst_ix);}
 inline void SplitStackByDlgTo(int i, _Army_* dst_army, int dst_i, _bool8_ is_hero, _bool8_ dst_is_hero)
  {CALL_6(void, __thiscall, 0x449B60, this, i, dst_army, dst_i, is_hero, dst_is_hero);}
};

NOALIGN struct _CreatureInfo_
{
 _int_ town;
 _int_ level;
 _char_* sound_name;
 _char_* def_name;
 _int_ flags;
 _char_* name_single;
 _char_* name_plural;
 _char_* specification_description;

 //_int_ cost_wood;
 //_int_ cost_mercury;
 //_int_ cost_ore;
 //_int_ cost_sulfur;
 //_int_ cost_crystal;
 //_int_ cost_jems;
 //_int_ cost_gold;
 _Resources_ cost;

 _int_ fight_value;
 _int_ AI_value;
 _int_ growth;
 _int_ horde_growth;
 _int_ hit_points;
 _int_ speed;
 _int_ attack;
 _int_ defence;
 _int_ damage_min;
 _int_ damage_max;
 _int_ shots;
 _int_ spells_count;
 _int_ advmap_low;
 _int_ advmap_high;
};

NOALIGN struct _HeroInfo_
{
 // ��� (0 - �������, 1 - �������)
 _dword_ sex;
 _dword_ field_04[8];
 _dword_ army_type[3];
 _ptr_ hps_name;
 _ptr_ hpl_name;
 _byte_ allowed_in_roe; // �������� �� � ����������� ������.
 _byte_ allowed_in_not_roe; // �������� �� �� � ����������� ������.
 _byte_ is_campaing; // ������������ ��.
 _byte_ field_38[5];
 _ptr_ name;
 _dword_ army_count[6];
};


NOALIGN struct _TurnTimeStruct_
{
 _dword_ last_shown;
 _dword_ turn_start;
 _dword_ turn_limit;
 _dword_ next_shown;
 _dword_ battle_start;

 inline void NewTurn() { CALL_1(void, __thiscall, 0x558130, this); }
};









NOALIGN struct _HString_
{
 _char_* c_str;
 _int_ length;
};
NOALIGN struct _HStringA_
{
 _HString_ h_str;
 _int_ a;
};
NOALIGN struct _HStringF_
{
 _bool8_ is_memory_allocated;
 _byte_ dummy_f1[3];
 _HString_ h_str;
 _int_ size;
};
NOALIGN struct _GlobalEvent_
{
 _HStringF_ message_f;
 _Resources_ resouces;
 _byte_ players_bits;
 _byte_ for_human;
 _byte_ for_ai;
 _word_ day;
 _word_ repeat;
};
NOALIGN struct _MapItem_
{
 _dword_  setup; //+0
 _byte_  land; // +4
 _byte_  land_type;//+5
 _byte_  river;//+6
 _byte_  river_type;//+7
 _byte_  road;//+8
 _byte_  road_type;//+9
 _word_  field_0A;//+10
 _byte_  mirror;//+12
 _byte_  attrib;//+13
 _word_  field_0E_bits;//+14

 _word_  field_10;//+16
 _dword_  draw;//+18
 _dword_  draw_end;//+22
 _dword_  draw_end_2;//+26
 _word_  object_type;//+30

 _word_  _u3;//+32
 _word_  os_type; //+34
 _word_  draw_num;//+36

 _dword_ GetReal__setup() { return CALL_1(_dword_, __thiscall, 0x4FD280, this);}
 _dword_ GetReal__object_type() { return CALL_1(_dword_, __thiscall, 0x4FD220, this);}
 
 // ������� ������ ���������� �������.
 inline void SetAsVisited(_int_ player_ix)
 {
   CALL_2(void, __thiscall, 0x4FC620, this, player_ix);
 }
 
 // ������� ������ ���������� �������.
 inline _bool8_ IsVisited(_int_ player_ix)
 {
   return CALL_2(_bool8_, __thiscall, 0x529B70, this, player_ix);
 }
 
 
};



NOALIGN struct _Artifact_
{
 _int_ id;
 _int_ mod;
};

// 32 bytes.
NOALIGN struct _ArtInfo_
{
 char* name;
 _int_ cost;
 _int_ position; // ��� �����, � �� ����� (��������, ��� ����� ������ - 1 ���)
 _int_ type;
 char* description;
 _int_ supercomposite;
 _int_ part_of_supercomposite;
 _byte_ disabled;
 _byte_ new_spell;
 _byte_ field_1E;
 _byte_ field_1F;
};


// 8 bytes. ���� ��� ���������.
NOALIGN struct _ArtSlotInfo_
{
  // +0. ��� �����.
  _cstr_ name;
  
  // +4. ��� �����.
  _int_ type;
};



NOALIGN struct _Hero_
{
 _int16_ x; // +0
 _int16_ y; // +2
 _int16_ z; // +4
 _byte_ visible; // +6
 _dword_ mui_xyz; // +7
 _byte_ field_0B; // +11
  // miu = map_item under hero
 _dword_ miu_object_type; // +12
 _dword_ miu_object_c_flag; // +16
 _dword_ miu_setup; // +20
 _word_ spell_points; // +24
 _dword_ id; // +26
 _dword_ id_wtf; // +30
 _int8_ owner_id; // +34
 _char_ name[13]; // +35
 _dword_ _class; // +48
 _byte_ pic; // +52
 _dword_ aim_x; // +53
 _dword_ aim_y; // +57
 _dword_ aim_z; // +61
 _byte_ field_41[3]; // +65
 _byte_ x_0; // +68
 _byte_ y_0; // +69
 _byte_ run; // +70
 _byte_ field_47; // +71
 _byte_ flags; // +72
 _dword_ movement_points_max; // +73
 _dword_ movement_points; // +77
 _dword_ expa; // +81
 _word_ level; // +85
 _dword_ visited[10]; // +87
 _byte_ field_7F[18]; // +127
 _Army_ army; // +145
 _byte_ second_skill[28]; // +201
 _byte_ second_skill_show[28]; // +229
 _dword_ second_skill_count; // +257
 _dword_ temp_mod_flags; // + 261
 _byte_ field_109[4]; // +265
 _byte_ dd_cast_this_turn; // +269
 _dword_ disguise; // +270
 _dword_ field_112; // +274
 _byte_ d_morale; // +278
 _byte_ field_117[3]; // +279
 _byte_ d_morale_1; // +282
 _byte_ d_luck; // +283
 _byte_ is_sleeping_byte11C; // +284
 _byte_ field_11E[3]; // +284
 _dword_ field_121; //
 _byte_ field_125[2]; //
 _byte_ field_127[7]; // 
 _Artifact_ doll_art[19]; // +301
 _byte_ free_add_slots; // +453
 _byte_ locked_slot[14]; // +454
 _Artifact_ backpack_art[64]; // +468
 _byte_ backpack_arts_count; // +980
 _dword_ sex; // +981
 _bool8_ has_biography; // +985
 _HStringF_ biography; // +986
 _byte_ spell[70]; // +1002
 _byte_ spell_level[70]; // +1072
 //_byte_ primary_skill[4];
 _byte_ attack; // +1142
 _byte_ defence; // +1141
 _byte_ power; // +1142
 _byte_ knowledge; // +1143
 _byte_ field_47A[24]; // +1144

 // normal
 void Hide() {CALL_1(void, __thiscall, 0x4D7950, this);}
 void Show(_int_ mapitem_type, _int_ mapitem_setup) {CALL_3(void, __thiscall, 0x4D7840, this, mapitem_type, mapitem_setup);}
 char* Get_className() {return CALL_1(char*, __thiscall, 0x4D91E0, this);}
 int GetLandModifierUnder() {return CALL_1(int, __thiscall, 0x4E5210, this);}

 void GiveArtifact(_Artifact_* art, int a3, int a4) {CALL_4(void, __thiscall, 0x4E32E0, this, art, a3, a4);}
 void GiveArtifact(_int_ art_id, int a3, int a4) 
 {
  _Artifact_ art;
  art.id = art_id;
  art.mod = 0xFFFF;
  GiveArtifact(&art, a3, a4);
 }

 _bool_ DoesHasArtifact(int art_id) {return CALL_2(_bool_, __thiscall, 0x4D9420, this, art_id);}
 _bool_ DoesWearArtifact(int art_id) {return CALL_2(_bool_, __thiscall, 0x4D9460, this, art_id);}
 void RemoveBattleMachine(int creature_id) {CALL_2(void, __thiscall, 0x4D94D0, this, creature_id);}
 int RemoveDollArtifact(int doll_slot_index) {return CALL_2(int, __thiscall, 0x4E2E40, this, doll_slot_index);}
 int AddDollArtifact(_Artifact_* art, int doll_slot_index) {return CALL_3(int, __thiscall, 0x4E2C70, this, art, doll_slot_index);}
 int RemoveBackpackArtifact(int index) {return CALL_2(int, __thiscall, 0x4E2FC0, this, index);}
 int AddBackpackArtifact(_Artifact_* art, int index) {return CALL_3(int, __thiscall, 0x4E3200, this, art, index);}
 
 
 // ��������� ������ � ������� ���������� �� �������������.
 _int_ GetSpell_Specialisation_Bonuses(_int_ Spell_id, _int_ Creature_level, _int_ BaseMidif)
 {
   return CALL_4(_int_, __thiscall, 0x4E6260, this, Spell_id, Creature_level, BaseMidif);
 }
 
 
 
 //my

 //inline _bool_ DoesHasDollArtifact(int art_id)
 //{
 // for (int i = 0; i < 19; i++)
 // {
 //  if (this->doll_art[i].id == art_id)
 //   return TRUE;
 // }
 // return FALSE;
 //}

 void ShowSpellBookDlg(int a1, int a2, int land_modifier)
 {
  if (this->doll_art[17].id != -1)
  {
   _Dlg_* dlg = (_Dlg_*)o_New(0xD0);
   // create spellbook dlg
   CALL_5(void, __thiscall, 0x59C0F0, dlg, this, a1, a2, land_modifier);
   dlg->Run();
   // destroy spellbook dlg
   CALL_1(void, __thiscall, 0x59CBF0, dlg);
   o_Delete(dlg);
  }
 }
 void ShowSpellBookDlg(int a1, int a2) 
  {ShowSpellBookDlg(a1, a2, this->GetLandModifierUnder());}
};



NOALIGN struct _Player_
{
 _int8_ id;
 _int8_ heroes_count;
 _word_ field_2;
 _int_ selected_hero_id;
 _int_ heroes_ids[8];

 _int_ tavern_heroes[2];

 _byte_ field_30[13];

 _byte_ days_to_kill; //������� ���� �� �������� ����� (����� ���� >7)
       //���� �� FF, �� ������ ������ ���������

 _int8_ towns_count;
 _int8_ selected_town_id;
 _int8_ towns_ids[48];
 _byte_ field_70[44];
 _Resources_ resourses;

 _byte_ field_B8[45];

 _byte_ field_E5;
 _byte_ human;

 _byte_ IsHumanAndWTF() {return CALL_1(_byte_, __thiscall, 0x4BAA40, this);}
 _byte_ IsHuman() {return CALL_1(_byte_, __thiscall, 0x4BAA60, this);}

 void MoveHeroToTownUp(_Town_* town) { CALL_2(void, __thiscall, 0x4B9C80, this, town);}

 //my
 inline _bool_ IsActive() {return (_bool_)(this == o_ActivePlayer);}
};


NOALIGN struct _Town_ : _Struct_
{
 _int8_ id;
 _int8_ owner_id;
 _int8_ built_this_turn;
 _byte_ field_03;
 _int8_ type;
 _byte_ x;
 _byte_ y;
 _byte_ z;
 _byte_ boat_x;
 _byte_ boat_y;

 _word_ field_0A;

 _int_ up_hero_id;
 _int_ down_hero_id;

 _int8_ mag_level;
 _byte_ field_15;

 _word_ available_creatures[14];

 _byte_ fields_32[6];
 _dword_ field_38;
 _dword_ field_3C;
 _word_ field_40;
 _word_ field_42;

 _dword_ spells[5][6];
 _byte_ magic_hild[5];

 _byte_ fields_C1[7];

 _char_ name[12];

  int   _u8[3];         //* +D4 = 0

  _Army_ guards; //+E0 = ������ �����
  _Army_ guards0; //+118 = ������ �����

  _dword_   built_bits; //*B +150h = ��� ����������� ������ (0400)
  _dword_   built_bits2;
  _dword_   bonus_bits;//*B +158h = ����� �� �������, ������� � �.�., ��������� ����������
  _dword_   bonus_bits2;
  _dword_   available_bits;      //*B- +160h = ����� ��������� ��� �������� ��������
  _dword_   available_bits2;     

 // normal

 char* GetTypeName() {return CALL_1(char*, __thiscall, 0x5C1850, this);}
 _Army_* GetUpArmy() {return CALL_1(_Army_*, __thiscall, 0x5C1860, this);}
 void SwapHeroes() {CALL_1(void, __thiscall, 0x5BE850, this);}
 void MoveHeroDown() {CALL_1(void, __thiscall, 0x5BE790, this);}
    
    // ��������� �� � ������ ������.
    inline _bool32_ IsBuildingBuilt(_int32_ building_id, _bool32_ unk_unused)
    {
      return CALL_3(_bool32_, __thiscall, 0x4305A0, this, building_id, unk_unused);
    }
    
    
 // my

 //void MoveHeroUp() {o_GameMgr->GetPlayer(o_GameMgr->GetHero(this->down_hero_id)->owner_id)->MoveHeroToTownUp(this);}

};

NOALIGN struct _Spell_
{
 _int_ type; // -1 -enemy, 0 -area, +1 -friend
 _cstr_ wav_name;//+4
 _int_ animation_ix;//+8
 _dword_ flags; //+C
 _cstr_   name;            // +10h
 _cstr_   short_name;        // +14h
 _int_    level;           // +18h
 _dword_  school_flags; // +1Ch Air=1,Fire=2,Water=4,Earth=8
 _int_    mana_cost[4];         // +20h cost mana per skill level
 _int_    eff_power;       // +30h
 _int_    effect[4];       // +34h effect per skill level
 _int_    chance2get_var[9];// +44h chance per class
 _int_    ai_value[4];      // +68h 
 _cstr_   description[4];     // +78h
};


NOALIGN struct _CreatureAnim_ // size = 84 Cranim.txt
{ 
  _int16_ i[6];
  _float_ f[18];  
};

NOALIGN struct _BattleStack_ : _Struct_ // ������ 0x548
{
 _byte_ already_attack; // +0 ?
 _byte_ field_01;  // +1 ?
 _byte_ field_02;  // +2 ?
 _byte_ field_03;  // +3 ?
 _byte_ field_04[4];  // +4 ?

 _int32_ all_stacks_count; // +8  //������ ����� ������ � ������

 _byte_ field_0C[4];  // +12 0x0C 
 _dword_ field_10;  // +16 0x10  //=-1 ����� ����� �/��� ������(????)
 _byte_ field_14[8];  // +20 0x14  ?

 _int32_ aim_to_move_or_shoot; // 28 +0x1C  //������� �� ���� ��� (���� ������/��������)
 _byte_ fireshield;  // +32 0x20 // �������� ���

 _byte_ field_21[3];  // +33 0x21  ?
 
 // ����� ����� - ������� ������� ����� (-1 - ��� �������).
 _int32_ clone_owner_stack_ix; //+36 
 
 //_int32_ clone_index; // +40 0x28 //����� ����� ����� �����
 _List_<_int32_>* clones; // �������� ����. ������ ��� ������ ������ �����.
 
 _byte_ field_2C[4];  // +44 0x2C  ?
 _dword_ field_30;  // +48 0x30  ?

 _int32_ creature_id;    // +52 0x34 // ��� �������
 _int32_ hex_ix;   // +56 0x38  //������� �� ���� ��� 
 _int32_ def_group_ix; // +60 0x3C
 _int32_ def_frame_ix; // +64 0x40

 _dword_ field_44;  // +68 0x44 //(=1) ����� � ������� ������ ������� ������ ��� ������� � ����� ��������
 _dword_ field_48;  // +72 0x48 ?

 _int32_ count_current; // +76 0x4C // ����� ��������
 _int32_ count_before_attack; // +80 +0x50 // ����� �������� �� ����� �� ��� � ���. �����
 
 _dword_ field_54;  // +84 0x54 ?
 
 _int32_ lost_hp;  // +88 0x58 ������ �������� ���������� �������
 _int32_ army_slot_ix; // +92 0x5C ����� ����� of _Army_ (0...6), -1 - ����� ������ ����� �����
 _int32_ count_at_start; // +96 0x60 ����� �������� � ������ �����
 
 _dword_ field_64;  // +100 0x64 ?
 
 _dword_ anim_value;  // +104 0x68 = cranim.f[15] ��� �������������
 _int32_ full_hp;  // +108 0x6C ������ �������� (���. ��� ���� ��� �������)
 
 _bool32_ field_70;  // +112 0x70 ?

//////////////////////////////////////////////////////////////////////////////////
 _CreatureInfo_ creature;
//////////////////////////////////////////////////////////////////////////////////
 //_int_ town;  //+116 0x74
 //_int_ level;  //+120 0x78
 //_char_* sound_name;  //+124 0x7C
 //_char_* def_name;  //+128 0x80
 //_int_ flags;  //+132 0x84
 //_char_* name_single;  //+136 0x88
 //_char_* name_plural;  //+140 0x8C
 //_char_* specification_description;  //+144 0x90
 //_int_ cost_wood;  //+148 0x94
 //_int_ cost_mercury;  //+152 0x98
 //_int_ cost_ore;  //+156 0x9C
 //_int_ cost_sulfur;  //+160 0xA0
 //_int_ cost_crystal;  //+164 0xA4
 //_int_ cost_jems;  //+168 0xA8
 //_int_ cost_gold;  //+172 0xAC
 //_int_ fight_value;  //+176 0xB0
 //_int_ AI_value;  //+180 0xB4
 //_int_ growth;  //+184 0xB8
 //_int_ horde_growth;  //+188 0xBC
 //_int_ hit_points;  //+192 0xC0
 //_int_ speed;  //+196 0xC4   
 //_int_ attack;  //+200 0xC8
 //_int_ defence;  //+204 0xCC
 //_int_ damage_min;  //+208 0xD0
 //_int_ damage_max;  //+212 0xD4
 //_int_ shots;  //+216 0xD8
 //_int_ spells_count;  //+220 0xDC
 //_int_ advmap_low;  //+224 0xE0
 //_int_ advmap_high;  //+228 0xE4
///////////////////////////////////////////////////////////////////////////////

 _byte_ field_E8;  // +232 0xE8  ?

 _bool8_ is_1_killed; // +233 0xE9   =1, ���� ������ ���� ����
 _bool8_ is_killed;  // +234 0xEA   =1, ���� ��� ���� ���� ����

 _byte_ field_EB;  // +235 0xEB  ?

 _int32_ current_creatures_spell_id; // +236 0xEC  ����� ���������� �������� � ��� ������ 0x50 Acid breath
 
 _int32_ field_F0;  // +240 0xF0  _byte_=1 ����� ������ �� ���� 441434
 
 _int32_ side;   // +244 0xF4  0 - attacker, 1 - defender
 
 _dword_ index_on_side;    // +248 F8 dd = ����� ����� � ������� �� ���� ���
 _dword_ field_FC;  // +252 FC dd = ? ���-�� � ������
 _dword_ field_100;  // +256 100 dd 43DEA4
 _dword_ field_104;  // +260 104 dd 43DEAD
 _dword_ field_108;  // +264 108 dd
 _dword_ field_10C;  // +268 10C dd
 
 _CreatureAnim_ cr_anim; // +272 0x110 
 _Def_* def;    // +356 0x164 def �������, ��������: 43DA8E
 _Def_* def_shot;  // +360 0x168 def ��������, ��������: 43DA8E
 _dword_ field_16C;  // +364 0x16� ?
 _Wav_* wav_move;  // +368 0x170 ���� ����������� (move), ��������: 43DA8E
 _Wav_* wav_attack;  // +372 0x174 ���� ����� (attk/wnce/shot), ��������: 43DA8E
 _Wav_* wav_wnce;  // +376 0x178 ���� '�������' (wnce), ��������: 43DA8E
 _Wav_* wav_shot;  // +380 0x17C ���� �������� (shot), ��������: 43DA8E
 _Wav_* wav_kill;  // +384 0x180 ���� ������ (kill), ��������: 43DA8E
 _Wav_* wav_defend;  // +388 0x184 ���� ������� (wnce/dfnd), ��������: 43DA8E
 _Wav_* wav_ext1;  // +392 0x188 ���� �������������� 1 (ext1), ��������: 43DA8E
 _Wav_* wav_ext2;  // +396 0x18� ���� �������������� 2 (ext2), ��������: 43DA8E
 
 _dword_ field_190;  // +400 0x190 ?
 
 _int32_ active_spells_count; // +404 0x194 dd = ���������� ��� ���������� ����������
 _int32_ active_spell_duration[81]; // +408 0x198  ���� ���������� (������������) ��� ���
 _int32_ active_spells_power[81]; // +732 0x2DC (���� �������� ����������)
 
 _byte_ field_420[52]; // +1056 0x420 ?
 
 _int32_ retaliations; // +1108 0x454 // 441B17 (���-�� ������� �� ����� 0= �� ���. �� �����)// ��������� ��� �������� 46D6A0
 _int32_ bless_value; // +1112 0x458 Bless ������� � Max. Damage
 _int32_ curse_value; // +1116 0x45C Curse ������ � Min. Damage
 _int32_ field_560;  // +1120 0x460 ?
 _int32_ bloodlast_value;// +1124 0x464 Bloodlast ������� � ����� � ��������
 _int32_ precision_value;// +1128 0x468 Precision ������� � ����� � ��������
 
 _byte_ field_46C[32]; // +1132 0x46C ?
 
 _int32_ king_type;  // +1164 0x48C dd KING_123 ��� (1=KING_1,2=KING_2,3=KING_3) //   ��� ��� ������� Slayer. ����� 8 � �����: 0x4421D2     
 _int32_ field_490;  // +1168 0x490 dd ����� ������� �� �������??? ��� ��������??? (������������ ����� ������� �����)
 _int32_ counerstrike_retaliations; // +1172 0x494 dd ���. ���. ������� �� �����, ����������� Counerstrike ������
 
 _byte_ field_498[40]; // +1176 0x498 ?
 
 _bool8_ blinded;   // +1216 0x4C0 db Blinded - ������� ������ (�������� �����?) ��� ����� �� ���� (���. ����� ������)
 _bool8_ paralized;   // +1217 0x4C1 db Paralized - ������� ������ (�������� �����?) ��� ����� �� ���� (���. ����� ������)
 _bool8_ forgetfulness;  // +1218 0x4C2 dd Forgetfulness - ������� (>2 - �� ����� ��������)
 
 _byte_ field_4C3[25];  // +1219 0x4C3 ?
 
 _int32_ defend_bonus;  // +1244 0x4DC dd = �������� ������ ��� ������ ������
 _int32_ faerie_dragon_spell;// +1248 0x4E0 dd ���������� ��� ���� �������

 _byte_ field_4E4[100];

                     // +4EC dd 44152A
                     // +4F1 db 43DF88

                     // +514 dd
                     // +518 dd -> dd first \ adjusted stacks pointers
                     // +51C dd -> dd last  /

                     // +524 dd
                     // +528 dd -> dd first \ adjusted to wich stacks pointers
                     // +52C dd -> dd last  /


 // normal
 inline _bool_ CanShoot(_BattleStack_* aim = NULL) {return CALL_2(_bool_, __thiscall, 0x442610, this, aim);}
 inline void InitDefsAndWavs() {CALL_1(void, __thiscall, 0x43D710, this);}
 inline void Construct(int creature_id, int count, _Hero_* hero_owner, int side, int index_on_side/*0 - 21*/, int position_hex_ix,  int army_slot_ix)
  {CALL_8(void, __thiscall, 0x43D5D0, this, creature_id, count, hero_owner, side, index_on_side, position_hex_ix, army_slot_ix);}
 
 
 
 // ������� ������� � ���������� ����� (������������ ����� ����).
 // Enemy - ��������� �� ��������� ����.
 // BaseDamage - ��������� ����.
 // IsShot - �������� �� ����� ��������� (TRUE - ��������).
 // Virtual - �������� �� ������� ������������ �� (TRUE - ��������).
 // WayLength - ���������� ���� �� ���� (��� ������� �������������� ������).
 // out_FireshieldDamage - ��������� �� ����������, � ������� � ���������� ������ ������� ��������� ���� �� ��������� ���� (0 - �� ������������).
 inline _int32_ Calc_Damage_Bonuses(_BattleStack_* Enemy, _int32_ BaseDamage, _bool8_ IsShot, _bool8_ Virtual, _int32_ WayLength, _int32_* out_FireshieldDamage)
 {
   return CALL_7(_int32_, __thiscall, 0x443C60, this, Enemy, BaseDamage, IsShot, Virtual, WayLength, out_FireshieldDamage);
 }
 
 inline int GetX() {return CALL_1(int,__thiscall,0x446380, this); }
 inline int GetY() {return CALL_1(int,__thiscall,0x446350, this); }
 
 
 // ��������� �������� ����� �� ���������� � ����� �������.
 inline void Draw_Shot(_BattleStack_* Enemy)
 {
   CALL_2(void, __thiscall, 0x43EFE0, this, Enemy);
 }
 
 
 // ��������� ������� �����, ����������� ������ (���� ���� 1-���������, �������).
 inline _int32_ Get_SecondHex_ix()
 {
   return CALL_1(_int32_, __thiscall, 0x4463C0, this);
 }
 
 
 // ����������� �����.
 // dead_itself - ���� �� ���� ��� ����� (��������, ��� ���� ��� ������ ������� ��� ��������� ������������ ����������).
 inline void Die(_bool8_ dead_itself)
 {
   CALL_2(void, __thiscall, 0x443E40, this, dead_itself);
 }
 
 
 
 
 
 // User
 
 // ��������� ������ ���������� ����� ����� (-1 - ���� �� �������� ���������� ������).
 inline _int32_ Get_ArrowTowerNum()
 {
   // �������� ����� ���������� ����� � ����������� �� � ������� (����������� ������ ����).
   switch (this->hex_ix)
   {
     case 254:
       return 0;
     case 251:
       return 1;
     case 255:
       return 2;
     default:
       return -1;
   }
 }
 
};

NOALIGN struct _TowerPresets_
{
  _int32_ CreatureType; // +0h; ��� �������� ����� 
  _int32_ CentralShooterX; // +Ch; ������� ����������� ������ �������� ����� �� ����������� � ��������
  _int32_ CentralShooterY; // +10h; ������� ����������� ���� �������� ����� �� ��������� � ��������
  _int32_ LowerShooterX; // +Ch; ������� ����������� ������ �������� ����� �� ����������� � ��������
  _int32_ LowerShooterY; // +10h; ������� ����������� ���� �������� ����� �� ��������� � ��������
  _int32_ HigherShooterX; // +Ch; ������� ����������� ������ �������� ����� �� ����������� � ��������
  _int32_ HigherShooterY; // +10h; ������� ����������� ���� �������� ����� �� ��������� � �������� 
  _char_* MissileDefName; // +4h; ����������� def �������� �����
};

#define o_CastleTowers ((_TowerPresets_*)0x63CF88)

// ��������� ���������� ����� � ���.
NOALIGN struct _ArrowTower_
{
  _int32_ CreatureType; // +0h; ��� �������� �����
  _Def_* Def; // +4h; ����������� def �������� �����
  _Def_* BulletDef; // +8h; ����������� def ������� �����
  _int32_ X_Position; // +Ch; ������� ����������� ������ �������� ����� �� ����������� � ��������
  _int32_ Y_Position; // +10h; ������� ����������� ���� �������� ����� �� ��������� � ��������
  _int32_ Orientation; // +14h; ����������� (0 - ������, 1 - �����)
  _int32_ AnimSectionNum; // +18h; ����� ������ ������ �������� �����
  _int32_ AnimFrameNum; // +1Ch; ������� ���� ������ �������� �����
  _int32_ StackNum; // +20h; ����� ����� ����� (�������-�������� - ������ 1)
};

#define o_HeroCombatDefs ((_HeroCombatDef_*)0x63BD40)

NOALIGN struct _HeroCombatDef_
{
  void *DEFName;
  int CastX;
  int CastY;
  int CastCadre;
};




// ��������� ������� �������� ����� (������ �� 8-�� ��������� �� 0x63BE60).
NOALIGN struct _CastleElement_
{
  _int16_ GexNum; // +0h; ������� �������� (������������ ��� ��� ������ ��� ���� �����)
  _int16_ SecondGex_Row; // +2h; ��� ������ ������� �������� (-1 - ���)
  _int16_ X_Position; // +4h; �������������� ������� �������� � ��������
  _int16_ Y_Position; // +6h; ������������ ������� �������� � ��������
  _int32_ CastleDrawingPartNum; // +8h; ����� �������� ����� ����� ���� ���������������� ������ �����
};











NOALIGN struct _BattleHex_
{
 //_byte_ field_0[112]; //?
 _int16_ X_Position; // +0h; ������� ������ ����� �� ����������� � ��������
 _int16_ Y_Position; // +2h; ������� ���� ����� �� ��������� � ��������
 _int16_ Left; // +4h; X-���������� ����� ������� �����
 _int16_ Top; // +6h; Y-���������� ������� ������� �����
 _int16_ Right; // +8h; X-���������� ������ ������� �����
 _int16_ Bottom; // +Ah; Y-���������� ������ ������� �����
 _byte_ field_C[4]; //?
 _int32_ Flags; // +10h; ����� ����� (1 - ���� �� �����������)
 _int_ ObstacleNum; // +14h; ����� �����������, ������������ �� ����� (-1 - ���)
 _byte_ bstack_side;
 _byte_ bstack_index;
 _byte_ field_1A[86]; //?
 
 
 // normal
 _BattleStack_* GetCreature() {return CALL_1(_BattleStack_*, __thiscall, 0x4E7230, this);}
};


// ��������� ������ ��������.
NOALIGN struct _BattleAnim_
{
  _cstr_ DefName; // ��� def`� ��������
  _cstr_ TouchEffect_Name; // ��� ����������� ������� �������� (��� ��� ��������������� �� ������������� ��������� ���������� � ������� IFC20.dll)
  _dword_ Properties; // �������� ������ �������� (�� ��������� �����)
  // ***
  // ������ �������� ������� ��������
  // ***
    // (����� - ������ � ����� - ��������) 
    // 0 - 4 - ������� ����������� (0 - �� ����� �� ����� ��� �����, 1 - ���������� ����� ��� �����, 2 - ���� �����, 3 - ����� ������, 4 - � ������ � ����� ������� �����, -1 - ��� �������)
    // 4 - 4 - �� ������������
    // 8 - 1 - ������������ (0 - ���, 1 - ����)
    // 9 - 23 - �� ������������
  // ***
  // ����� �������� ������� ��������
  // ***
};





NOALIGN struct _BattleMgr_ : _Struct_ 
{
 _byte_ field_0[452]; // + 0 ?
 _BattleHex_ hex[187]; // + 0x1c4 187=17*11
 
 
 _byte_ field_5394[44]; //?
 
 // ��� ���������� (-1 - ���, 0 - ���������, 1 - ������� �����, 2 - ��������� ����� � �. �.)
 _int32_ spec_terr_type;
 
 _byte_ field_53C4[8]; //?

 _Hero_* hero[2]; // + 21452 // 0 - attacker, 1 - defender
 _byte_ field_53D4[212]; // + 21460d�
 _int32_ owner_id[2]; // + 21672d // 0 - attacker, 1 - defender
 _byte_ field_54B0[12];  // + 21680
 _int32_ stacks_count[2];//+0x54BC  // 0 - attacker, 1 - defender
 _Army_* army[2]; // + 21700 // 0 - attacker, 1 - defender
 //_BattleStack_ stack[42]; //+ 21708
 _BattleStack_ stack[2][21]; //+ 21708

 _byte_ field_1329C[28]; // + 0x1329C
 _int32_ unk_side; // +78520 0x132B8
 _int32_ current_stack_ix; // +78524 0x132BC
 _int32_ current_side; // +78528 0x132C0

 //_byte_ field_132C4[56]; // + 0x132C4
 _byte_ field_132C4[36]; // + 0x132C4
 _Def_* current_spell_def; // + 0x132E8
 _int_  current_spell_id; // + 0x132EC
 _dword_ field_132F0; // + 0x132F0
 _int32_ town_fort_type; // + 0x132F4
 _dword_ field_132F8; // + 0x132F8

 _Dlg_* dlg;    // + 0x132FC
 
 _byte_ f13300[3564]; // +13300h

 // normal
 //inline _Def_* LoadSpellAnimation(int spell_anim_ix)
 inline int GetHexIxAtXY(int x, int y) {return CALL_2(int, __stdcall, 0x464380, x - dlg->x, y - dlg->y);}
 inline void CastSpell(int spell_id, int hex_ix, int cast_type_012, int hex2_ix /*��� ��������� � ������*/, int skill_level, int spell_power)
  {CALL_7(void, __thiscall, 0x5A0140, 
    this, spell_id, hex_ix, cast_type_012, hex2_ix, skill_level, spell_power);}
 
 
 // �������� ������������� ��������� �����.
 // ��� ������������� ��������� ���������� FALSE, ����� TRUE.
 inline _bool8_ ShouldNotRenderBattle()
 {
   return CALL_1(_bool8_, __thiscall, 0x46A080, this);
 }
 
 
 
 // ����� ����� ������������� �����������.
 inline void ClearRedrawFields()
 {
   return CALL_1(void, __thiscall, 0x493290, this);
 }
 
 
 // �������� ������������� ����������� ��� ��������� ��������� ��� ����� - ���������� �����.
 inline void SetArrowTowerToRedraw(_BattleStack_* Stack)
 {
   return CALL_2(void, __thiscall, 0x46A040, this, Stack);
 }
 
 
 
 // ����������� ������ ����������� ��� ����� def`�.
 inline void SetSpecRedrawBorders(_Def_* Def, _int_ DefGroupNum, _int_ FrameNum, _int_ X_Pos, _int_ Y_Pos, _RedrawBorders_* out_Borders, _bool_ Reflected, _bool_ NeedAddToGlobalBorders)
 {
   CALL_9(void, __thiscall, 0x495AD0, this, Def, DefGroupNum, FrameNum, X_Pos, Y_Pos, out_Borders, Reflected, NeedAddToGlobalBorders);
 }
 
 
 
 
 
 // ���������� ������ ������������� ���������� ������ �� ������ ����� ������������� ����������� ������ ���������.
 inline void SetRedrawBorders()
 {
   CALL_1(void, __thiscall, 0x495770, this);
 }
 
 
 // ��������� ���� ���.
 // Flip - ������������� ���������� ������.
 // SetBattleRedraws - ������������� ��������� ������ ���������� ������.
 // UseBattleRedraws - ������������� ������������� ������ ���������� ������ (��� SetBattleRedraws = TRUE ������������, �������� TRUE).
 // WaitingTime - �����, �� �������� �������� ���� ��������� ��������� ��������� (������������ ��� Wait = FALSE).
 // RedrawBackground - ������������� ����������� ������� ����� (� ����� ������� �����������).
 // Wait - ������������� ��������. � ������ FALSE ����� �������� ��� ��������� ��������� �� ��������.
 inline void RedrawBattlefield(_bool8_ Flip, _bool8_ SetBattleRedraws, _bool8_ UseBattleRedraws, _int_ WaitingTime, _bool8_ RedrawBackground, _bool8_ Wait)
 {
   return CALL_7(void, __thiscall, 0x493FC0, this, Flip, SetBattleRedraws, UseBattleRedraws, WaitingTime, RedrawBackground, Wait);
 }
 
 
 // ���������� ����� ���� ���.
 inline void FlipBattlefield()
 {
   CALL_1(void, __fastcall, 0x493300, this);
 }
 
 
 
 
 // ������������� ����������, ����������� �������� ��������� ��������.
 inline void InitRandAnimsTimes()
 {
   CALL_1(void, __thiscall, 0x479860, this);
 }
 
 
 // ������������ ������ ����� �������� �������� ���� ���.
 // ��� �������� ������ ������, �������� ����� ������, ��������� �������� �������, �������� ����� ������ ������.
 inline void PlayWaitAnim()
 {
   CALL_1(void, __thiscall, 0x495C50, this);
 }
 
 
 // ������������ ������ ����� �������� �������� ���� ��� ������ � ������ ����������� �������.
 inline void PlayWaitAnimOnce()
 {
   CALL_1(void, __thiscall, 0x473970, this);
 }
 
 
 // ��������� ����� �������� ����� �� ��� ������.
 inline void DamageCastleElement(_int_ CastleElementNum, _int_ Damage)
 {
   CALL_3(void, __thiscall, 0x465570, this, CastleElementNum, Damage);
 }
 
 
 
 // �������� ����� � �����, �� ������� �� �����.
 inline void RemoveStackFromGexes(_BattleStack_* Stack)
 {
   CALL_2(void, __thiscall, 0x468310, this, Stack);
 }
 
 
 // ���������� ����� �� ����.
 inline void PutStackToGex(_BattleStack_* Stack, _int_ GexNum)
 {
   CALL_3(void, __thiscall, 0x4683A0, this, Stack, GexNum);
 }
 
 // �������� �����������.
 inline void RemoveObstacle(_int_ ObstacleNum)
 {
   CALL_2(void, __thiscall, 0x466710, this, ObstacleNum);
 }
 
 
 
 
 // my
 inline _BattleStack_* GetCurrentStack()
 {
  return &(stack[current_side][current_stack_ix]);
 }

 inline _BattleStack_* GetCreatureAtXY(int x, int y)
 {
  int hex_ix = GetHexIxAtXY(x, y);
  if (hex_ix != ID_NONE) 
   return hex[hex_ix].GetCreature();
  else
   return NULL;
 }
 
 
 
 
  // ��������� ���� ����������� ��� �����.
 inline void Set_Stack_Redrawable(_BattleStack_* Stack)
 {
   // ����������� ������� ��� ���������� �����.
   if (Stack->creature_id == CID_ARROW_TOWER)
   {
     this->SetArrowTowerToRedraw(Stack);
   }
   // ����������� ���� ��� ������.
   else
   {
     *(_bool8_*)((_dword_)this + 81920 + Stack->side*20 + Stack->index_on_side) = TRUE;
   }
 }
 
 
 
 
 
 
 // ���������� ������� � �������� �����������. ������� ������������� ��������� ����������� (�� ����������).
 // ����������� ������������� �����������.
 inline _bool_ AddUpdateArea(_int_ X_Pos, _int_ Y_Pos, _int_ Width, _int_ Height)
 {
   
   // ������� �����������.
   _RedrawBorders_ Brd;
   
   
   if (*(_bool8_*)((_ptr_)this + 81196) || *(_bool32_*)((_ptr_)this + 81200))
   {
     // ����� ������� �����������.
     Brd.Left = max(X_Pos, BattleRedraw_Borders.Left);
     // ������� ������� �����������.
     Brd.High = max(Y_Pos, BattleRedraw_Borders.High);
     // ������ ������� �����������.
     Brd.Right = min(X_Pos + Width - 1, BattleRedraw_Borders.Right);
     // ������ ������� �����������.
     Brd.Low = min(Y_Pos + Height - 1, BattleRedraw_Borders.Low);
     
     if (*(_bool8_*)((_ptr_)this + 81196))
     {
       // ��������� ���������� ������ �����������.
       (*(_int_*)((_ptr_)this + 81208)) = min(*(_int_*)((_ptr_)this + 81208), Brd.Left);
       (*(_int_*)((_ptr_)this + 81212)) = min(*(_int_*)((_ptr_)this + 81212), Brd.High);
       (*(_int_*)((_ptr_)this + 81216)) = max(*(_int_*)((_ptr_)this + 81216), Brd.Right);
       (*(_int_*)((_ptr_)this + 81220)) = max(*(_int_*)((_ptr_)this + 81220), Brd.Low);
     }
     
     if (*(_bool32_*)((_ptr_)this + 81204)) return FALSE;
   }
   
   
   // ���������� ������������� ��������� (���� ���� �������� ����������� ������ � ������� �����������).
   if (*(_bool32_*)((_ptr_)this + 81200)
       && (Brd.Left > ((_RedrawBorders_*)((_ptr_)this + 81208))->Right || Brd.Right < ((_RedrawBorders_*)((_ptr_)this + 81208))->Left
           || Brd.High > ((_RedrawBorders_*)((_ptr_)this + 81208))->Low || Brd.Low < ((_RedrawBorders_*)((_ptr_)this + 81208))->High))
   {
     return FALSE;
   }
   else
   {
     return TRUE;
   }
 }
 
 
 // ���������� ������� � �������� ����������� �������� �����.
 // ����������� ������������� �����������.
 inline _bool_ AddSpecUpdateArea(_Def_* Def, _int_ DefGroupNum, _int_ FrameNum, _int_ X_Pos, _int_ Y_Pos, _bool_ Reflected)
 {
   // ������� �����������.
   _RedrawBorders_ Brd;
   
   if (*(_bool8_*)((_ptr_)this + 81196) || *(_bool32_*)((_ptr_)this + 81200))
   {
     this->SetSpecRedrawBorders(Def, DefGroupNum, FrameNum, X_Pos, Y_Pos, &Brd, Reflected, *(_bool8_*)((_ptr_)this + 81196));
     if (*(_bool32_*)((_ptr_)this + 81204)) return FALSE;
   }
   // ���������� ������������� ��������� (���� ���� �������� ����������� ������ � ������� �����������).
   if (*(_bool32_*)((_ptr_)this + 81200)
       && (Brd.Left > ((_RedrawBorders_*)((_ptr_)this + 81208))->Right || Brd.Right < ((_RedrawBorders_*)((_ptr_)this + 81208))->Left
           || Brd.High > ((_RedrawBorders_*)((_ptr_)this + 81208))->Low || Brd.Low < ((_RedrawBorders_*)((_ptr_)this + 81208))->High))
   {
     return FALSE;
   }
   else
   {
     return TRUE;
   }
 }
 
 
 
 
};

NOALIGN struct _TownStartInfo_ //size = 0x88
{
 _int_ id; //+0
 _byte_ owner_id;//+4
 _byte_ field_5[19];//+5
 _bool8_ has_fort; //+0x18
 _byte_ field_19[3];//+0x19
 _Army_ army;//+0x1C
 _byte_ field_54[20];//+0x54
 _dword_ type;//+0x68
 _byte_ field_6C[28];//+0x54
};

NOALIGN struct _ArmyStartInfo_
{
 _int32_ type[7];
 _int16_ count[7];
};

NOALIGN struct _HeroStartInfo_ //size = 0x334 bytes
{
 _int8_ owner_id;   //! = ������ (����) ff - �����
 _byte_ _u1[3];
 _dword_ id;  // = ����� ������� (���������� �����)
 _dword_ id_wtf;      // = id
 _bool8_ has_name;    // = 1-���� ���
 _char_ name[13];//! = ���,0
 _byte_ has_exp;    // = 1-���� ����
 _byte_ _u2;
 _int_  exp;     //   +1c  dd   = ����
 _bool8_   has_picture;    //   +20  db   = 1-���� ��������
 _byte_    picture_id;     //   +21  db   = ����� ��������
 _bool8_   has_second_skills;   //   +22  db   = 1-���� 2-� �����
 _byte_ _u3;
 _dword_ second_skills_count;   //!   +24  dd   = ���. ������ ������
 _byte_ second_skill[8];//  +28  dd*8 = ������ ������ ������
 _byte_ second_skill_level[8];//   +30  db*8 = ������ ������ ������
 _bool8_ has_army;    //   +38  db   = 1-���� ��������
 _byte_ _u4[3];

 _ArmyStartInfo_ army; //+3c
 _bool8_ army_is_group;   //   +66  db   = ������/�������

 _bool8_ has_arts;    //   +67  db   = 1-���� ���������
 _Artifact_ doll_art[19];//+68  dd*2*13 = ��������� dd-�����,dd-(ff) +e8 -�����(3,ff)
 _Artifact_ backpack_art[64];//+100 dd*2*40 = ��� � ������� dd-�����, dd-(ff)
 _byte_ backpack_arts_count;   //   +300 db   = ����� ���������� � �������
 _dword_ xyz;   //   +301 2*dw = ���. ������� �� �����
 _byte_ run_radius;     //!   +305 db   = ������ ��������
 _bool8_ has_biography;    //   +306 db   = 1-���� ���������
 _byte_ _u6;   //+307
 _HStringF_ biography;      //   +308 dd   = 1-�������� ������ ��� ���������
 _int8_ sex;     //   +318 dd   = 0-�,1-�,ff-���������
 _byte_ _u9[3];
 _bool8_ has_spells;   //   +31c db   = 1-���� ����������
 _byte_ _u10[3];
 _byte_ spell[10];//  +320 db*a = ����������
 _word_ _u11;
 _byte_ has_primary_skills;   //   +32c db   = 1-���� ��������� ������
 _byte_ primary_skill[4];//  +32d db*4 = 4-�� ��������� ������
 _byte_ _u12[3];
};

#define GAME_HEROES_AVAILABILITY_OFFSET (*(_ptr_*)0x412F1C)
#define GAME_PLAYERS_AVAILABILITY_OFFSET (*(_ptr_*)0x4868FB)

#define GAME_HEROES_OFFSET (*(_ptr_*)0x41C6B5)






// ******************************




// ������ ������.
NOALIGN struct _HeroesList_: public _List_<_Hero_>
{
  // ��� �������������� �����, ������ ������.
  
  // �������� ������ ������.
  inline void Delete()
  {
    CALL_1(void, __thiscall, 0x45F830, this);
  }
};




// ��������� ������� ����� ������������ � ��������� �������.
NOALIGN struct _TmplPosMsk_
{
  // ������ 32 ����.
  _dword_ bits0; // +0
  // ��������� 32 ���� (������������ ������ ������ 16).
  _dword_ bits32; // +4
  
  
  // ��������� ���� ����� �����.
  void SetAll(_dword_ value)
  {
    CALL_2(void, __thiscall, 0x4E6770, this, value);
  }
  
  
  // ��������� ���������� ���� �����.
  void SetBit(_int_ bit_ix, _bool8_ value)
  {
    CALL_3(void, __thiscall, 0x506DD0, this, bit_ix, value);
  }
  
  
  // ��������� ���������� ���� �����.
  _bool8_ GetBit(_int_ bit_ix)
  {
    return CALL_2(_bool8_, __thiscall, 0x506E30, this, bit_ix);
  }
  
};





// ��������� ������� ����� ����������� ���������� �������.
NOALIGN struct _TmplTerrMsk_
{
  // ������ 32 ����.
  _dword_ bits0; // +0
  
  
  // ��������� ���� ����� �����.
  void SetAll(_dword_ value)
  {
    CALL_2(void, __thiscall, 0x506ED0, this, value);
  }
  
  
  // ��������� ���������� ���� �����.
  void SetBit(_int_ bit_ix, _bool8_ value)
  {
    return CALL_3(void, __thiscall, 0x506E70, this, bit_ix, value);
  }
  
  
  
  // user
  
  // ��������� ���������� ���� �����.
  _bool8_ GetBit(_int_ bit_ix)
  {
    return (((1 << (bit_ix & 0x1F)) & *(&this->bits0 + (bit_ix >> 5))) != 0);
  }
  
};





// ������ ������� �� �����.
// �������� �� ��� ������� � ������������ ��������, ������� ����� ���� ������� � ������ ���� - ������������ � �. �.
NOALIGN struct _Template_
{
  // ��� def`� �������.
  _HStringF_ DefName; // + 0
  // ������ ������� ��-����������� (� ������� �����).
  _byte_ Size_x; // +16
  // ������ ������� ��-��������� (� ������� �����).
  _byte_ Size_y; // +17
  _byte_ f12[10]; // +18
  // ������� ����� ���������� ������ (6*6, 2 ���� � ������ ����� � 2 ����� ��������� �� ������������).
  _TmplPosMsk_ ImpassBitMask; // +28
  _byte_ f24[8]; // +36
  // ������� ����� ������������ ������ (6*6, 2 ���� � ������ ����� � 2 ����� ��������� �� ������������).
  _TmplPosMsk_ TriggerBitMask; // +44
  _TmplTerrMsk_ terrs; // +52
  // ��� ������� �������.
  _int_ ObjType; // +56
  // ������ ������� ������.
  _int_ ObjSubtype; // +60
  _byte_ f40; // +64
  _byte_ f41[3]; // +65
};



// ������ �� ����� �����������.
NOALIGN struct _Object_
{
  _byte_ f0[4]; // +0h
  // X-���������� ��������� �� �����
  _byte_ x; // +4h
  // Y-���������� ��������� �� �����
  _byte_ y; // +5h
  // Z-���������� ��������� �� �����
  _byte_ z; // +6h
  // ��������� ������������, �� ������������.
  _byte_ dummy_f7[1]; // +7h
  // ����� ������� �������.
  _int16_ template_id; // +8h
  _byte_ dummy_fA[2]; // +Ah
};



// ������ - ������ �����.
NOALIGN struct _PlaceHolder_
{
  // ����� ����� ��������� ������� ������.
  _Object_* object; // +0h
  // ����� ������ - ������� ������.
  _byte_ player_ix; // +4h
  // ��������� ������������, �� ������������.
  _byte_ dummy_f5[3]; // +5h
  // ����� ����� ������ (-1 - ������������).
  _int_ hero_id; // +8h
  // �������� ���� ����� � ������ ��� ������������� �����.
  _byte_ hero_power; // +Ch
  // ��������� ������������, �� ������������.
  _byte_ dummy_fD[3]; // +Dh
};





// ����� ���� (������ 3932 ��� 3936 - ����������, ��������� �� ��������� dd � ����� ��� ������ � GameMgr).
NOALIGN struct _GameMap_
{
  // ������ �������� �������� �����.
  _List_<_Template_> Templates; // +0
  // ������ �������� �� �����.
  _List_<_Object_> Objects; // +16
  
  _byte_ f20[128]; // +32
  
  // ������ ������� ����� �� �����.
  _List_<_PlaceHolder_> PlaceHolders; // +160
  
  _byte_ fB0[32]; // +176
  
  // ����� 3-������� ������� [L][Y][X], ��������� �������� ������� ������ ������ �����.
  _MapItem_* items; // +208
  // ������ ����� (��-����������� � ��-���������).
  _int_ size; // +212
  // ���� �� � ����� ��������� �������.
  _bool8_ has_underground; // +216
  
  _byte_ fD9[3715]; // +217
  
  
  //normal
  // ��������� ������� ����� �� ����������� (�� ������� items).
  inline _MapItem_* GetItem(_int_ x, _int_ y, _int_ z)
  {
     return CALL_4(_MapItem_*, __thiscall, 0x4086D0, this, x, y, z);
  }
};


// ������������ ���������� �����. ������: 720 ����.
NOALIGN struct _MapHeader_
{
  // +0.
  _byte_ f0[772];
};




NOALIGN struct _GameMgr_: _Struct_
{
 _dword_ field_0;//+0 ???
 _byte_ disabled_shrines[70]; //+4 //??? //����� ������� ��� ����� ��������� �� ����� (0)
 _byte_ disabled_spells[70];//+0x4A //��������� ���� (0) �� ����� ��� ��������� (1)
 LPCRITICAL_SECTION cs_bink;//+0x90 //CriticalSection Object ��� �������/��������� ����� BINKW32.DLL
 _List_<_TownStartInfo_> town_start_info;//+0x94
 _HeroStartInfo_ hero_start_info[156];//+0xA4
 _dword_ field_1F454;//+0x1F454
 _bool8_ is_cheater;//+0x1F458
 
 _byte_ f1F459[1]; // +1F459h
 
 // ����� �������� � �������� (� 1).
 _byte_ campaing_scenario_ix; // +1F45Ah
 
 _byte_ f1F45B[1]; // +1F45Bh
 
 // ����� �������� (������������ � ����� � ��.)
 _dword_ campaing_ix;  // +1F45Ch
 
 
 _byte_ f1F460[52]; // +1F460h
 
 
 // ������ ����������� ������ � �������� ��� �������� � ��������� ��������.
 _List_<_HeroesList_> saved_heroes_lists; // +1F494h
 
 
 _byte_ f1F4A4[402]; // +1F4A4h


 _byte_ is_player_absent[8]; // +1F4A4h
 
 
 // ����� �������� ��� (1-7).
 _word_ curr_day_ix; // +1F63Eh
 
 // ����� ������� ������ (1-4).
 _word_ curr_week_ix; // +1F640h
 
 // ����� �������� ������.
 _word_ curr_month_ix; // +1F642h
 
 
 _byte_ f1F644[84]; // +1F644h
 
 
 // ������ ����� (0 - RoE).
 _dword_ map_version; // +1F698h
 
 _byte_ f1F69C[1]; // +1F69Ch
 
 // �������� �� ������� ���� ���������.
 _bool8_ is_tutorial; // +1F69Dh
 
 _byte_ f1F69E[59]; // +1F69Eh
 
 // �������� ����� �����.
 char map_name[248]; // +1F6D9h
 
 _byte_ f1F7D1[3]; // +1F6D1h
 
 // ���� � ����� �����.
 char map_path[100]; // +1F7D4h
 
 
  _byte_ f1F838[52]; // +1F838h
  
  // ������������ ���������� ����� ����.
  _MapHeader_ map_header; // +1F86Ch
  
  // ����� ����.
  _GameMap_ Map; // +1FB70h
 
  _byte_ f20ACC[4]; // +20ACCh
  
  // ��������� �������.
  _Player_ player[8]; // +20AD0h
  
  // ������ �������.
  _List_<_Town_> town; // +21610h
  
  // ����� (����������).
  _Hero_ hero[156]; // +21620h
  
  // ��������� ������ (����������).
  _byte_ heroes_aval_abyte4DF18[156]; // +4DF18h
  
  // ����������� ������ ������� (����������).
  _dword_ hero_player_aval_f4DFBD[156]; // +4DFBDh
  
  _byte_ f4E22D[144]; // +4E22Dh
  
  _byte_ arts_aval_abyte4E2BD[144]; // +4E2BDh
  
  _byte_ f4E34D[1164]; // +4E34Dh
  
 
 inline _Player_* GetMe() {return CALL_1(_Player_*, __thiscall, 0x4CE670, this);}
 inline _int_  GetMeID() {return CALL_1(_int_, __thiscall, 0x4CE6E0, this);}
 inline char*  GetPlayerName(_int_ player_id) {return (char*)( ((_ptr_)this) + 0x20AD0 + player_id*0x168 + 0xCC );}

 inline _int_  FindTownIdByXYZ(_int_ x, _int_ y, _int_ z) {return CALL_4(_int_, __thiscall, 0x4BB530,this, x, y, z);}

 inline _Hero_*  GetHero(_int_ hero_id) {return ((_Hero_ *)(((_ptr_)this) + GAME_HEROES_OFFSET /*0x21620*/ + 1170 * hero_id));}
 inline _Player_* GetPlayer(_int_ player_id) {return ((_Player_ *)(((_ptr_)this) + 0x20AD0 + 360 * player_id));}
 inline _Town_*  GetTown(_int_ town_id) {return ((_Town_*)(*(_ptr_*)(((_ptr_)this) + 0x21614) + 360 * town_id));}
 inline _int_  GetTownsCount() {return ((_int_)((*(_dword_*)(((_ptr_)this) + 0x21618) - *(_dword_*)(((_ptr_)this) + 0x21614)) / 360));}
 inline _GlobalEvent_* GetGlobalEvent(_int_ index) {return ((_GlobalEvent_*)(*(_ptr_*)(((_ptr_)this) + 0x1fbf4) + 52 * index));}
 inline _int_  GetGlobalEventsCount() {return ((_int_)((*(_dword_*)(((_ptr_)this) + 0x1fbf8) - *(_dword_*)(((_ptr_)this) + 0x1fbf4)) / 52));}
 inline _int_  GetMapType() {return ( *(_dword_*)(((_ptr_)this) + 0x1f698) );}
 inline char*  GetScenarioName() {return *(char**)((_ptr_)this +  0x1F86C + 0x2D4);}
 inline _word_  GetDay() {return *(_word_*)((_ptr_)this + 0x01F63E);}
 inline _word_  GetWeek() {return *(_word_*)((_ptr_)this + 0x01F640);}
 inline _word_  GetMonth() {return *(_word_*)((_ptr_)this + 0x01F642);}
 inline _int_*  GetMerchantsArts() {return (_int_*)((_ptr_)this + 0x01F664);}


 inline _dword_  GetMapWidth() {return *(_dword_*)((_ptr_)this + 0x1FC44);}
 inline _byte_  GetMapDepth() {return *(_byte_*)((_ptr_)this + 0x1FC48);}

 inline _bool_  IsPlayerIngame(_int_ player_id) { return (_bool_)(1 - *(_byte_*)(((_ptr_)this) + 0x1F636 + player_id)); }
 inline _bool_  IsPlayerOutgame(_int_ player_id) { return (_bool_)(*(_byte_*)(((_ptr_)this) + 0x1F636 + player_id)); }

 inline void ReplayOpponentTurn() {CALL_1(void, __thiscall, 0x49D410, this);}
 inline void ShowScenarioInfo() {CALL_1(void, __thiscall, 0x513950, this);}

 inline _byte_ PlayerIsInGameHuman(_int_ player_id) {return CALL_2(_byte_, __thiscall, 0x4CE630, this, player_id);}

 inline void  ShowFullCreatureInfoDlg(_Army_* army, _int_ index, _Hero_* hero, _int_ e0, _int_ x, _int_ y, _int8_ can_fired, _int8_ right_click)
  { CALL_9(void, __thiscall, 0x4C6910, this, army, index, hero, e0, x, y, can_fired, right_click); }

 // my
 inline void ShowFullCreatureInfoDlg(_Army_* army, _int_ index, _Hero_* hero, _int_ x, _int_ y, _bool8_ right_click)
 {

  if (hero)
   CALL_9(void, __thiscall, 0x4C6910, this, army, index, hero, 0, x, y, hero->army.GetStacksCount() > 1, right_click);
  else
   CALL_9(void, __thiscall, 0x4C6910, this, army, index, hero, 0, x, y, TRUE, right_click);
 }
 
  
  
  // user
  
  // ��������� ����� �� �����������.
  _Struct_* GetBoatAtCoords(_int_ x, _int_ y, _int_ z)
  {
    // �������� �� ���� ������������ ������.
    if (this->Field<_List_<_byte_>>(320440).Data)
    {
      for (_ptr_ i = (_ptr_)this->Field<_List_<_byte_>>(320440).Data; i < (_ptr_)this->Field<_List_<_byte_>>(320440).EndData; i += 40)
      {
        // ����� ���������� � �� ����� �����.
        if (((_Struct_*)i)->Field<_bool8_>(24) && !((_Struct_*)i)->Field<_bool8_>(36))
        {
           // ����� - �� ������� �����������.
           if (((_Struct_*)i)->Field<_int16_>(0) == x && ((_Struct_*)i)->Field<_int16_>(2) == y && ((_Struct_*)i)->Field<_int16_>(4) == z)
           {
             return (_Struct_*)i;
           }
        }
      }
    }
    
    // �� �����.
    return NULL;
  }
  
  
  // ������� ����� � ����� �� ��� �����, ��� �� ����� (����� ������ ���� �� ����� � �� � �����, ���� ����� ��� �� ���, ��� ��������, ���� ���� - ��� ������ ��� ������ � ������ ����).
  _bool32_ HeroPlaceToBoat(_Hero_* hero, _int_ boat_subtype)
  {
    
    // �����.
    _Struct_* boat;
    
    // ������ �����.
    _int_ boat_ix = CALL_7(_int_, __thiscall, 0x4BAF10, this, hero->x, hero->y, hero->z, hero->owner_id, FALSE, boat_subtype);
    
    // �� ������ ������� �����.
    if (boat_ix < 0)
    {
      return FALSE;
    }
    // ������������� �����.
    else
    {
      boat = (_Struct_*)(this->Field<_List_<_byte_>>(320440).Data + 40*boat_ix);
    }
    
    
    
    // ������ ����� � �����.
    
    
    // ������� ����� �� ���������� ������.
    CALL_1(void, __thiscall, 0x4D7950, boat);
    
    // ����������� �����.
    hero->temp_mod_flags |= 0x40000;
    hero->field_112 = -1;
    *(_int32_*)&hero->d_morale = -1;
    
    // ������������� ������ ����� � ������ (new_mp = old_mp*new_max/old_max).
    if (!(hero->temp_mod_flags & 0x1000000))
    {
      _int_ new_max = CALL_2(_int32_, __thiscall, 0x4E4C00, hero, TRUE);
      hero->movement_points = hero->movement_points*new_max/hero->movement_points_max;
      hero->movement_points_max = new_max;
    }
    
    // ����������� �����.
    boat->Field<_int32_>(32) = hero->id;
    boat->Field<_bool8_>(36) = TRUE;
    boat->Field<_int8_>(28) = hero->owner_id;
    
    return TRUE;
  }
 
 
};



// ******************************






// *************************************


#define o_GetTime() CALL_0(_dword_, __cdecl, 0x4F8970)

typedef void (__thiscall *_func_MouseMan_SetCursor)(_ptr_ this_, _int_ frame, _int_ type);
#define b_MouseMan_SetCursor(frame, type) ((_func_MouseMan_SetCursor)0x50CEA0)(o_MouseMgr,(_int_)(frame),(_int_)(type))

#define o_GetMapCellVisability(x,y,z) CALL_3(_dword_, __fastcall, 0x4F8040, x, y, z)


// ������������ ��������� �����.
#define RandMax 0x7FFF

// ��������� ����� ��� ���������.
#define Rand() CALL_0(_dword_, __cdecl, 0x61842C)

// ��������� ���������� double.
inline double DoubleRand(double low, double high)
{
  return low + (double)Rand()/(double)RandMax*(high - low);
}


// ������������������ �������.
// � ���������� �� ������� ����� ����������� ���������� ���������� ��������� ����� ����� � ��������� �������� �� seed ��� �������� �������.
#define TRandint(Low, High) CALL_2(_int_, __fastcall, 0x50B3C0, (_int_)Low, (_int_)High)


// ��������� ����� ����� � ���������.
#define Randint(Low, High) CALL_2(_int_, __fastcall, 0x50C7C0, (_int_)Low, (_int_)High)


// �������� �� ���������� ������� (��� �������, ������������ ���������).
#define WaitTill(Time) CALL_1(void, __fastcall, 0x4F8980, (_dword_)Time);


// ������ ����������� ������� (IFC20.dll, �� ����������� ��� ����������� ��������� ����������).
#define TouchEffectStart(Name, a2) CALL_2(void, __fastcall, 0x4B6750, (_cstr_)Name, (_dword_)a2)

// ��������� ����� �������� �� ��� ������ � ����������.
#define GetCreatureName(Type, Count) CALL_2(_cstr_, __fastcall, 0x43FE20, (_int_)Type, (_int_)Count)

// ��������� ������ �������� (-1 - ��� ������).
#define GetCreatureGrade(Type) CALL_1(_int_, __fastcall, 0x47AAD0, (_int_)Type)


// *************************************








NOALIGN struct _GarrisonBar_
{
 _byte_ field_0[28];
 _int_ x; //+28
 _int_ y; //+32
 _bool_ is_down_bar; //+36
 _int_ owner_id; //+40
 _int_ sel_slot_index;
 _byte_ field_2C[56];
 _Dlg_* parent_dlg;
 _Army_* army; //+108
 _int_ hero_pic; //+112
 _Hero_* hero; //+116

 // normal
 inline void Update(_bool_ a2, _int_ a3) { CALL_3(void, __thiscall, 0x5AA0C0, this, a2, a3); }
 inline void UpdateRedraw(_int_ a2) { CALL_2(void, __thiscall, 0x5AA090, this, a2); }
};

NOALIGN struct _TownMgr_
{
 _byte_ field_0[56];
 
 _Town_* town; //+56
 _byte_ field_48[220];
 _Dlg_* dlg; // +280
 _GarrisonBar_* garribar_up; //+284
 _GarrisonBar_* garribar_down; //+288

 _GarrisonBar_* garribar_sel; //+292
 _int_ garribar_slot_index_sel; //+296

 _GarrisonBar_* garribar_src; //+300
 _int_ garribar_slot_index_src; // +304
 _GarrisonBar_* garribar_dst; //+308
 _int_ garribar_slot_index_dst; //+312

 _ptr_ resources_bar;//+316

 _dword_ field_140;//+320

 _char_ statusbar_text[88]; // +324

 _int_ command_code; //+412
 
 _byte_ f1A0[56]; // +1A0h


 // normal

 void CreateNewGarriBars() {CALL_1(void, __thiscall, 0x5C7210, this);}
 void MoveHeroUp() {CALL_1(void, __thiscall, 0x5D5550, this);}
 void MoveHeroDown() {CALL_1(void, __thiscall, 0x5D5620, this);}

 // my 

 inline void DeleteGarriBars()
 {
  if (this->garribar_up) o_Delete(this->garribar_up); this->garribar_up = NULL;
  if (this->garribar_up) o_Delete(this->garribar_down); this->garribar_down = NULL;
 }

 inline void SwapHeroes() {this->town->SwapHeroes(); DeleteGarriBars(); CreateNewGarriBars();}

};


typedef void (__thiscall * _func_AdvMgr_ActivateHero)(_ptr_ this_, int hero_id, int a3, char a4, char a5);
#define o_AdvMgr_ActivateHero(advmgr, hero_id, a3, a4, a5) ((_func_AdvMgr_ActivateHero)0x417A80)((_ptr_)(advmgr), (int)(hero_id), (int)(a3), (char)(a4), (char)(a5))

typedef _MapItem_* (__thiscall * _func_GetMapItem)(_ptr_ this_, int x, int y, int z);
#define b_GetMapItem(x,y,z) ((_func_GetMapItem)0x4086D0)(*(_ptr_*)(o_AdvMgr + 92),(int)(x),(int)(y),(int)(z))



NOALIGN struct _AdvMgr_
{
 _byte_ field_0[68]; //+0
 _Dlg_* dlg; //+68
 _byte_ field_48[20]; //+72
 _GameMap_* map; //+92
 _byte_ field_60[488]; //+96
 // ������� ����� (������ �� ���� ��� 0, ���� �� �������������).
 _Wav_* loop_sounds[70]; // +584
 _byte_ field_360[52]; //+864
 _int_ current_info_panel_id;//+916
 
 _byte_ f398[32]; // +398h
 
 
 //inline _Dlg_* GetDlg() {return *(_Dlg_**)((_ptr_)this + 68);}
 //inline _int_ GetCurrentInfoPanelID() {return *(_int_*)((_ptr_)this + 916);}
 inline _dword_ FullUpdate(_bool_ redraw_screen) {return CALL_3(_dword_, __thiscall, 0x417380, this, redraw_screen, 0/*not used*/);}
 inline _dword_ RebuildHeroInfoPanel(_bool_ even_if_on_top) {return CALL_2(_dword_, __thiscall, 0x4163B0, this, even_if_on_top);}
 inline _dword_ RebuildTownInfoPanel(_bool_ even_if_on_top) {return CALL_2(_dword_, __thiscall, 0x416450, this, even_if_on_top);}
 inline _dword_ UpdateInfoPanel(_bool_ even_if_on_top, _bool_ redraw, _bool_ redraw_screen)
  {return CALL_4(_dword_, __thiscall, 0x415D40, this, even_if_on_top, redraw, redraw_screen);}
 inline _dword_ RedrawInfoPanel(_bool_ redraw_screen) {return CALL_2(_dword_, __thiscall, 0x402BC0, dlg, redraw_screen);}
 inline _dword_ ViewWorld(_int_ a2, _int_ a3) {return CALL_3(_dword_, __thiscall, 0x5FC340, this, a2, a3);}
 inline _dword_ ShowPuzzleMap() {return CALL_1(_dword_, __thiscall, 0x41A750, this);}
 inline _dword_ DrawMap(_int_ x, _int_ y, _int_ z, _int_ a5, _bool_ update_info_panel)
  { return CALL_6(_dword_, __thiscall, 0x40F350, this, x,y,z, a5, update_info_panel);}
 inline _dword_ DrawMap(_dword_ xyz, _int_ a5, _bool_ update_info_panel)
  { return DrawMap(b_unpack_x(xyz), b_unpack_y(xyz), b_unpack_z(xyz), a5, update_info_panel);}
 inline _dword_ Dig(_int_ x, _int_ y, _int_ z) { return CALL_4(_dword_, __thiscall, 0x40EBF0, this, x,y,z); }
 inline _dword_ Dig() { return Dig(-1,0,0); }
 
 
};


#define o_SwapMan_SplitArmyStack (*(_bool_*)0x6A3D68)
NOALIGN struct _SwapMan_
{
 _byte_ field_0[56];
 _Dlg_* dlg; //+56
 _Pcx8_* pcx8_sel_rect; //+60
 _Hero_* hero[2]; //+64
 _int_ src_hero_ix; //+72
 _int_ dst_hero_ix; //+76
 _int_ src_slot_ix; //+80
 _int_ dst_slot_ix; //+84
 _bool_ slot_selected; //+88
};

//////////////////////////////////////////////////////////////////////////////////////////
//typedef _int8_ (__thiscall *_func_AdvMgr_FullUpdate)(_ptr_ this_, _byte_ redraw, _dword_ unused);
//#define b_AdvMgr_FullUpdate(redraw) ((_func_AdvMgr_FullUpdate)0x417380)(o_AdvMgr, (_byte_)(redraw), 0)

//typedef char (__thiscall * _func_AdvMgr_RebuildHeroInfoPanel)(_ptr_ advmgr, _int8_ a2);
//typedef char (__thiscall * _func_AdvManInfoPanel_Redraw)(_ptr_ this_, _int8_ a2);
//#define o_AdvMgr_RebuildHeroInfoPanel(this_, even_if_not_on_top) ((_func_AdvMgr_RebuildHeroInfoPanel)0x4163B0)((_ptr_)(this_),(_int8_)(even_if_not_on_top))
//#define b_AdvMgr_RedrawInfoPanel(this_, redraw_on_primary_buf) ((_func_AdvManInfoPanel_Redraw)0x402BC0)(*(_ptr_*)(this_ + 68), (_int8_)(redraw_on_primary_buf))

//#define b_AdvmanHeroInfoPanel_Update(draw) {((_func_RebuildAdvmanHeroInfoPanel)0x4163B0)(o_AdvMgr, 1); ((_func_RedrawAdvmanInfoPanel)0x402BC0)(*(_ptr_*)(o_AdvMgr + 68), (_int8_)(draw));}
//#define b_AdvMgr_CurrentInfoPanelID (*(_dword_*)(o_AdvMgr + 916))

//typedef void (__thiscall * _func_AdvMgr_ViewWorld)(_ptr_ this_, _int_ a2, _int_ a3);
//#define b_AdvMgr_ViewWorld(a2, a3) ((_func_AdvMgr_ViewWorld)0x5FC340)(o_AdvMgr, (_int_)(a2), (_int_)(a3))
//#define b_AdvMgr_ShowPuzzleMap() ((_func_this)0x41A750)(o_AdvMgr)

//typedef void (__thiscall *_func_AdvMgr_DrawMap)(_ptr_ this_, _int_ x, _int_ y, _int_ z, _bool_ a5, _bool_ a6);
//#define o_AdvMgr_DrawMap(this_,x,y,z,a5,a6) ((_func_AdvMgr_DrawMap)0x40F350)((_ptr_)(this_),(_int_)(x),(_int_)(y),(_int_)(z),(_bool_)(a5),(_bool_)(a6))
//#define b_AdvMgr_DrawMap(this_,xyz,a5,a6) o_AdvMgr_DrawMap(this_, b_unpack_x(xyz), b_unpack_y(xyz), b_unpack_z(xyz),(_bool_)(a5),(_bool_)(a6))


#define o_Terminate(pchar_reason_text) CALL_1(void, __fastcall, 0x4F3D20, pchar_reason_text)

//typedef void (* _func_PrintToScreenLog)(_ptr_ screen_log, char* format, ...);
//#define b_PrintToScreenLog(format, ...) ((_func_PrintToScreenLog)0x553C40)(o_ScreenLogStruct, (char*)(format), __VA_ARGS__)
#define b_PrintToScreenLog(format, ...) ((void (__cdecl *)(_ptr_, char*, ...))0x553C40)(o_ScreenLogStruct, (char*)(format), __VA_ARGS__)

typedef int (__cdecl * _func__beginthread)(_ptr_ func, SIZE_T dwStackSize, _ptr_ arg);
#define o__beginthread(func, stack_size, arg) ((_func__beginthread)0x61A56C)((_ptr_)(func), (SIZE_T)(stack_size), (_ptr_)(arg))




//////typedef LRESULT (CALLBACK *_func_WndProc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
//////#define o_WndProc(hWnd, message, wParam, lParam) ((_func_WndProc)0x4F8290)((HWND)(hWnd),(UINT)(message),(WPARAM)(wParam),(LPARAM)(lParam))


NOALIGN struct _InputMgr_
{
 __declspec(noinline) void SendEventMsg(int type, int subtype, int id, int x, int y, int flags, int flags_2, int new_param) // +0h
 {
  if ( (_ptr_)this )
  {
   if ( *((_ptr_ *)(_ptr_)this + 13) == 1 )
   {
    _ptr_ v = 32 * *((_ptr_ *)(_ptr_)this + 527);
    _EventMsg_ * msg = (_EventMsg_ *)((_ptr_)this + v + 56);
    *(_ptr_ *)((_ptr_)this + v + 68) = 0;

    ///
    msg->type = type;
    msg->subtype = subtype;
    msg->item_id = id;
    msg->x_abs = x;
    msg->y_abs = y;
    msg->flags = flags;
    msg->flags_2 = flags_2;
    msg->new_param = new_param;
    ///

    ++*((_ptr_ *)(_ptr_)this + 527);
    *((_ptr_ *)(_ptr_)this + 527) %= 64;
    if ( *((_ptr_ *)(_ptr_)this + 526) == *((_ptr_ *)(_ptr_)this + 527) )
    {
     *((_ptr_ *)(_ptr_)this + 526) += 1;
     *((_ptr_ *)(_ptr_)this + 526) %= 64;
    }
    *((_ptr_ *)(_ptr_)this + 597) = 0;
   }
  }
 }
 
 
 _byte_ f4[48]; // +4h
 
 // �������� �� ���� �������.
 _bool32_ CanRecieveMsg; // +34h
 // �������� �������.
 // ��� ��������� ������� �� �������� ����������� ������� (�. �. ����� ���������� ����� ������ �������).
 _EventMsg_ Msg[64]; // +38h
 // ����� ������� ������� ��� ���������.
 // ��� ������������ ������� ����� ����������, ����� ������ ��������� �� ����� ������ ���������.
 _int_ FirstMsg_ix; // +838h
 // ����� �������, � ������� ��������� ��������� ��������.
 _int_ NextMsg_ix; // +83Ch
 
 _byte_ f840[288]; // +840h
 
 
 // ������ ������� ������� �� �������.
 // out_event_msg - ����� ���������, � ������� ��������� ����������.
 inline void Peek_Event(_EventMsg_* out_event_msg)
 {
   CALL_2(void, __thiscall, 0x4EC660, this, out_event_msg);
 }
 
};





// �������� ����� (������ ����������, ���� - ��� �������� �������).
NOALIGN struct _SoundMgr_
{
  _byte_ f0[216]; // +0h
  
  // ����� ������������ ����� (����������� ������� ����� ������������� ������).
  inline _dword_ StartSample(_Wav_* Wav)
  {
    return CALL_2(_dword_, __thiscall, 0x59A510, this, Wav);
  }
};





// ********************************************


// ��������� ����� �������.
// ������ ���� ����� ���� � ����� �� ������ ���������, �������� ������������ � ������ ����.
// ��������� ���������� ������ � ������� �����.
NOALIGN struct _CrBankState_
{
  // ��������� �����.
  _Army_ defenders; // +0h
  
  // ���������� ��������, �������� � ������� �������.
  _int_ resources_award[7]; // +38h
  
  // ��� ��������, ��������� � ������ �������.
  _int_ creatures_award_ix; // +54h
  // ���������� ������� ���������������� ����, �������� � ������ �������.
  _byte_ creatures_award_count; // +58h
  
  // ���������� �������� ����� ��������� ������� ���������.
  // �������� ���� ������� �� ���������� �������� ���� ���������, �� ������ ��� � ����� ���� 100, �. �. ��� ��������.
  _byte_ chance; // +59h
  
  // ���� ��������� ����������� ����� ����� ���������� ����� ��� ������ � ���������.
  _byte_ upgrade_chance; // +5Ah
  
  // ���������� ����������-��������, �������� � ������ �������.
  _byte_ arts_treasure_count; // +5Bh
  // ���������� ����� ����������, �������� � ������ �������.
  _byte_ arts_minor_count; // +5Ch
  // ���������� ������� ����������, �������� � ������ �������.
  _byte_ arts_major_count; // +5Dh
  // ���������� ����������-��������, �������� � ������ �������.
  _byte_ arts_relic_count; // +5Eh
  
  // ��������� ������������, �� ������������.
  _byte_ dummy_f5F[1]; // +5Fh
};



// ��� ����� ������� ��� ������� �� �����.
// ������ � ���� ��������� ��������� ����� ������� (�. �. ��� ������ � �������).
NOALIGN struct _CrBankType_
{
  // ������ 1.
  _byte_ setup; // +0h
  
  // ��������� ������������, �� ������������.
  _byte_ dummy_f1[3]; // +1h
  
  // ��� �����.
  _HStringA_ name; // +4h
  
  // ��������� ��������� �����.
  _CrBankState_ States[4]; // +Ch
  
  
  
  
  
  
  // �����������.
  inline _CrBankType_* Contruct()
  {
    return CALL_1(_CrBankType_*, __thiscall, 0x47A400, this);
  }
};



// ********************************************


// ������� ������ �� �����.
NOALIGN struct _Dwelling_
{
  // ��� ������ ��� ������� (17 - �������, 20 - � 4 ����������).
  _byte_ type;
  // ������ ������ ��� �������.
  _byte_ subtype;
  _byte_ f2;
  _byte_ f3;
  // ���� �������, ��������� ��� ����� (-1 - ���).
  _int_  creature_types[4];
  // ���������� �������, ��������� ��� �����.
  _word_ creature_counts[4];
  // ��������� ������.
  _Army_ defenders;
  // X-���������� �� �����.
  _byte_ x;           // +54 db (3)
  // Y-���������� �� �����.
  _byte_ y;           // +55 db
  // ��������� �� � ����������.
  _bool8_ l;
  // �����-������ (-1 - ���).
  _int8_ owner_ix;
  _byte_ f58;
  _byte_ f59;
  _byte_ f5A;
  _byte_ f5B;
  
  // �����������.
  inline _Dwelling_* Contruct()
  {
    return CALL_1(_Dwelling_*, __thiscall, 0x4B8250, this);
  }
};

// ********************************************



















// 12 bytes. ��������� ���� ���������� �������� ����.
NOALIGN struct _ZoneTreasure_
{
  // +0. ����������� ��������.
  _int32_ min_value;
  
  // +4. ����������� ��������.
  _int32_ max_value;
  
  // +8. ������� ���������.
  _int32_ density;
};


// 28 bytes. ����� ����  ���������� �������� ���� � ������.
NOALIGN struct _ZoneConnection_
{
  // +0. ��������� ����, � ������� ������� �������.
  _ZoneSettings_ *another_zone_settings;
  
  // +4. �������� �������.
  _int32_ value;
  
  // +8. ������� �� ������.
  _bool8_ is_wide;
  
  // +9. ���������� �� ������� �������.
  _bool8_ has_border_guard;
  
  // +10. ���� �� ��� ������� �� ��������� �����.
  _bool8_ is_created;
  
  // +11. ��������� ������������, �� ������������.
  _byte_ dummy_f11[1];
  
  // +12. ����������� ���������� ��� ��������, ����������� ��� ��������� �����.
  _int32_ min_human_pos;
  
  // +16. ������������ ���������� ��� ��������, ���������� ��� ��������� �����.
  _int32_ max_human_pos;
  
  // +20. ����������� ���������� ��� �������, ����������� ��� ��������� �����.
  _int32_ min_total_pos;
  
  // +24. ������������ ���������� ��� �������, ���������� ��� ��������� �����.
  _int32_ max_total_pos;
};


// 212 bytes. ��������� ���� ���������� �������� ����.
NOALIGN struct _ZoneSettings_
{
  // +0. ����� ����.
  _int32_ id;
  
  // +4. ��� ���� (0 - ���� ��������, 1 - ���� ��, 2 - ������������, 3 - ������).
  _int32_ type;
  
  // +8. ������� ������ ����.
  _int32_ base_size;
  
  // +12. ����������� ���������� ��� ��������, ����������� ��� ��������� ����.
  _int32_ min_human_pos;
  
  // +16. ������������ ���������� ��� ��������, ���������� ��� ��������� ����.
  _int32_ max_human_pos;
  
  // +20. ����������� ���������� ��� �������, ����������� ��� ��������� ����
  _int32_ min_ai_pos;
  
  // +24. ������������ ���������� ��� �������, ���������� ��� ��������� ����.
  _int32_ max_ai_pos;
  
  // +28. ����� - �������� ����.
  _int32_ owner;
  
  // +32. ����������� ���������� ������� ��� ����� ������.
  _int32_ min_player_towns;
  
  // +36. ����������� ���������� ������� � ������ ������.
  _int32_ min_player_castles;
  
  // +40. ������� ��������� ������� ��� ����� ������.
  _int32_ player_towns_density;
  
  // +44. ������� ��������� ������� � ������ ������.
  _int32_ player_castles_density;
  
  // +48. ����������� ���������� ����������� ������� ��� �����.
  _int32_ min_neutral_towns;
  
  // +52. ����������� ���������� ����������� ������� � ������.
  _int32_ min_neutral_castles;
  
  // +56. ������� ��������� ����������� ������� ��� �����.
  _int32_ neutral_towns_density;
  
  // +60. ������� ��������� ����������� ������� � ������.
  _int32_ neutral_castles_density;
  
  // +64. ������ �� ��� ������ ���� ������������ ������ ����.
  _bool8_ towns_are_the_same_type;
  
  // +65. ����������� �������.
  _bool8_ towns_aval[9];
  
  // +74. ��������� ������������, �� ������������.
  _byte_ dummy_f4A[2];
  
  // +76. ����������� ���������� ���� ������� ����.
  _int32_ min_mines[7];
  
  // +104. ������� ��������� ���� ������� ����.
  _int32_ mines_density[7];
  
  // +132. ������������� �� ��� ���������� ������ ����� ������.
  _bool8_ terr_match_to_town;
  
  // +133. ����������� ����� ����������.
  _bool8_ terrs_aval[8];
  
  // +141. ��������� ������������, �� ������������.
  _byte_ dummy_f8D[3];
  
  // +144. ���� �������� (0 - ��� ��������, 1 - ������, 2 - �������, 3 - �������).
  _int32_ monsters_strength;
  
  // +148. ������������� �� ���� �������� ���� ������.
  _bool8_ monsters_match_to_town;
  
  // +149. ����������� �������� �� �������.
  _bool8_ monsters_towns_aval[9];
  
  // +158. ��������� ������������, �� ������������.
  _byte_ dummy_f9E[2];
  
  // +160. ��������� ����.
  _ZoneTreasure_ treasure[3];
  
  // +196. ����� � ������� ������.
  _List_<_ZoneConnection_> connections;
};







// ********************************************// ********************************************

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//typedef _ptr_ (__thiscall * _func_GetNetMessage)(_ptr_ this_, _int8_ a2, _int_ a3);
//#define b_GetNetMessage() ((_func_GetNetMessage)0x553440)(o_DirectPlayCOMObject, 0, 0);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




// ������������ ��������� ������� �������� � ������������ �����.
_Sample_ __fastcall rwr_Load_And_Start_Sample(_cstr_ WavName);




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
