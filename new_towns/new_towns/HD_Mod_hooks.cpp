#include "patcher_x86_commented.hpp"
#include <cstdio>
// #include "HoMM3.h"
// #define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)
#include "../../__include__/H3API/single_header/H3API.hpp"
#define TxtTownColour 1

extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;
extern long ThirdUpgradesInTowns_27x2x7[27*2*7];
extern long long additional_buildings_per_town[48];

extern long EighthMonType[27][4];
extern INT16 eighth_creature_amount[48];
extern int recruit_eighths(h3::H3Msg* arg);

extern "C" __declspec(dllexport) void TownInfo_Dlg_Show
	(int color, int town_id, int RMB);

int __stdcall Hook_Creatures_in_HD_mod_UI(HiHook* hook, int a1, int a2) {
	// __asm pushad
	int ret = CALL_2(int, __stdcall, hook->GetDefaultFunc(),a1, a2);
	
	int town_type = *(char*)(a1 + 4); int town_ID = *(char*) a1;
	if (a2 > 6 && a2 < 14) {
		if (additional_buildings_per_town[town_ID] & (1LL << (a2-7))) {
			int mon = ThirdUpgradesInTowns_27x2x7[14 * town_type + a2 -7];
			if (mon >= 0) ret = mon;
		}

		if (additional_buildings_per_town[town_ID] & (1LL << a2)) {
			int mon = ThirdUpgradesInTowns_27x2x7[14 * town_type + a2];
			if ( mon >= 0) ret=mon;
		}
	}
	
	/*
	DWORD* availible2buy = (DWORD*)0x0CDE352C;
	h3::H3TownManager* mgr = (h3::H3TownManager*) a1;
	*/
	// __asm popad
	return ret;
}

BOOL8 TownChooseable[27];
extern char text_Towns2[27 + 1][32];

struct ChooseTown_Dlg : public h3::H3Dlg {
	int plr = -1; int choice = -1;

	ChooseTown_Dlg(int _plr) : H3Dlg(720, 480, -1, -1, 0, 0), plr(_plr), choice(-1)
	{
		AddBackground(0, 0, widthDlg, heightDlg, true, false, plr, false);

		int j = 0;
		for (int i = 0; i < 27; ++i) {
			if (TownChooseable[i]) {
				CreateDef(32 + 80 * (j / 3), 32 + 96 * (j % 3), 3000 + i,
					"itpt+.def",i*2,0);
				LPCSTR text = text_Towns2[i+1];
				CreateText(28 + 80 * (j / 3), 96 + 96 * (j % 3),
					64, 32, text, "MedFont.fnt", TxtTownColour, 4000 + i);
				++j;
			}
		}

	}

	BOOL DialogProc(h3::H3Msg& msg) override;
};


BOOL ChooseTown_Dlg::DialogProc(h3::H3Msg& msg) {
	if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
		// auto target = msg.GetDlg()->ItemAtPosition(msg);
		if (msg.itemId >= 3000 && msg.itemId < 4000) {
			choice = msg.itemId - 3000;

			auto z_parent = (ChooseTown_Dlg*)msg.parentDlg;
			z_parent->Stop();
		}
	}

	if (msg.subtype == h3::eMsgSubtype::RBUTTON_DOWN) {
		// auto target = msg.GetDlg()->ItemAtPosition(msg);
		if (msg.itemId >= 3000 && msg.itemId < 4000) {
			// choice = msg.itemId - 3000;
			TownInfo_Dlg_Show(plr, msg.itemId - 3000, 0);
		}
	}
	return 0;
}

#define NETMSG_ID 0x5CE5E1EC
struct H3SelectScenario_packet {
	int player_id;
	int town;
	int hero;
};

h3::H3ScenarioPlayer* find_player_with_id(int player_id) {
	auto dlg = h3::H3SelectScenarioDialog::Get();
	char slot[8] = {}; int humans = 0;
	auto plr = dlg->mapPlayersHuman;
	auto end1 = dlg->mapPlayersHuman + 8;
	auto end2 = dlg->mapPlayersHuman + 16;
	while (plr != end1 && plr->player2 >=0 ) {
		if (plr->player2 == player_id)
			return plr;
		slot[plr->player2] = 1;
		++plr; ++humans;
	} 

	int i = 0; int j = 0; plr = end1;
	while (plr != end2) {
		if (slot[i]) { ++i; continue; }
		if (j == player_id) return plr;
		++plr; ++i; ++j;
	}

	return nullptr;
	
}

_LHF_(hook_00588AC3) {
	if (c->eax != NETMSG_ID)
		return EXEC_DEFAULT;

	auto dlg = h3::H3SelectScenarioDialog::Get();
	if (dlg) {
		auto packet = (h3::H3NetworkData<H3SelectScenario_packet>*) c->edi;

		/*
		auto plr = dlg->mapPlayersHuman;
		auto end = dlg->mapPlayersHuman + 16;
		while (plr->player2 != packet->data.player_id
			&& plr != end) ++plr;

		if (plr == end)
			return EXEC_DEFAULT;
		plr->gameVersion = packet->data.hero; 
*/
		auto plr = find_player_with_id(packet->data.player_id);
		if(!plr) return EXEC_DEFAULT;

		plr->gameVersion = packet->data.hero;
		plr->town = packet->data.town;

		if(packet->data.hero<0)
			THISCALL_1(void, 0x00583B60, dlg); // FillStartingHeroesList

		if (dlg->_f_37F) THISCALL_4(void, 0x0058D7E0, dlg, packet->data.player_id, 1, -1);
		else THISCALL_3(void, 0x005857D0, dlg, -1, 1);
	}

	c->return_address = 0x00589305;
	return SKIP_DEFAULT;
}

void installMoreTownsForRandomMaps()
{
	int chooseableTowns = 0;
	int chooseableTownsMask = 0;
	int chooseableTownsLast = 0;

	for (int i = 0; i < 27; i++)
		if (TownChooseable[i])
		{
			chooseableTownsLast = i;
			chooseableTowns++;
			chooseableTownsMask += 1 << i; // pow(2, i);
		}

	unsigned int townMax[]
	{
		0x00586C59 + 2,
		0x00513521 + 2,
		0x005134F7 + 2,
		0x005134D4 + 2,
		0
	};   // 5877A7

	for (int i = 0; townMax[i]; ++i)
		Z_new_towns->WriteByte(townMax[i], chooseableTowns);

	Z_new_towns->WriteDword(0x005808DA + 1, chooseableTownsMask);
	Z_new_towns->WriteByte(0x00586ADA + 1, chooseableTownsLast);
}

void installRandomTownCanBeNewFaction()
{
	int chooseableTowns = 0;
	for (int i = 0; i < 27; i++)
		if (TownChooseable[i])
		{
			chooseableTowns++;
		}

	Z_new_towns->WriteByte(0x00532F1F + 2, chooseableTowns);     // Random Maps
	Z_new_towns->WriteByte(0x00532F40 + 2, chooseableTowns);     // Random Maps
	Z_new_towns->WriteByte(0x004CA99E + 1, chooseableTowns - 1); // Scenario
}

_LHF_(installMoreTownsForScenarios_0058DB8E)
{
	int chooseableTownsMask = 0;
	for (int i = 9; i < 27; i++)
		if (TownChooseable[i])
			chooseableTownsMask += 1 << i; // pow(2, i);

	if (*(UINT16*)(c->ebx + 0x08) == 511)
		*(UINT16*)(c->ebx + 0x08) += chooseableTownsMask;

	
	return EXEC_DEFAULT;
}


_LHF_(hook_0058692A) {
	auto me = (h3::H3Msg*)c->esi;
	auto dlg = me ? (h3::H3SelectScenarioDialog*) me->GetDlg() : nullptr;
	int my_id = c->eax;

	if (c->ecx >= 0x0107 && c->ecx <= 0x010E) {
		int player_id = c->ecx - 0x0107;

		/*
		//auto plr = (h3::H3ScenarioPlayer*)(0x1064 + (char*)dlg);
		auto plr = dlg->mapPlayersHuman;
		auto end1 = dlg->mapPlayersHuman + 8;
		auto end2 = dlg->mapPlayersHuman + 16;
		while (plr->player2 != player_id
			&& plr != end2) ++plr;

		if (plr == end2)
			return EXEC_DEFAULT;
		*/
		auto plr = find_player_with_id(player_id);
		if(!plr) return EXEC_DEFAULT;

		ChooseTown_Dlg window(player_id);
		window.CreateOKButton(); window.Start();
		int result = window.choice;

		auto flag_item = dlg->GetDefButton(0x0107 + player_id);
		auto my_item = dlg->GetH3DlgItem(my_id);
		plr->gameVersion = -1; plr->town = result;

		auto wnd_mgr = h3::H3WindowManager::Get();
		// auto def2 = h3::H3LoadedDef(); def2.Load("ITPA+.def");

		if (h3::H3Network::Multiplayer()) {
			H3SelectScenario_packet packet = { player_id, plr->town, -1 };
			auto msg = h3::H3NetworkData<H3SelectScenario_packet>(-1, NETMSG_ID, packet);

			FASTCALL_4(INT32, 0x5549E0, &msg, 127, false, 1);// data.SendData(0);
		}
		if(dlg->_f_37F)
		{
			auto hero_slot = dlg->GetH3DlgItem(0x016a + player_id);
			// THISCALL_1(void, 0x00584F10, dlg);
			THISCALL_1(void, 0x00583B60, dlg);
			auto def2 = h3::H3LoadedDef::Load("ITPA+.def");
			int last = def2->groups[0]->count - 1;// 110;
			auto random_pcx = dlg->hpsrandPcx;

			/*
			random_pcx->DrawToPcx16(wnd_mgr->screenPcx16,
				hero_slot->GetAbsoluteX(), hero_slot->GetAbsoluteY(), 0);

			def2->DrawToPcx16(0, result < 0 ? last : 2 + (result * 2), wnd_mgr->screenPcx16,
				my_item->GetAbsoluteX(), my_item->GetAbsoluteY());

			char* result_name = result<0 ? "Random" : text_Towns2[result + 1];

			auto col = wnd_mgr->screenPcx16->GetPixel(my_item->GetAbsoluteX() - 7, my_item->GetAbsoluteY() + 36);
			wnd_mgr->screenPcx16->FillRectangle(my_item->GetAbsoluteX() - 11, my_item->GetAbsoluteY() + 34,
				my_item->GetWidth() + 22, 10, col );

			wnd_mgr->screenPcx16->TextDraw(P_TinyFont, result_name,
				my_item->GetAbsoluteX() - 11, my_item->GetAbsoluteY()+33,
				my_item->GetWidth() + 22, 12);
			*/
			THISCALL_1(void, 0x00583B60, dlg); // FillStartingHeroesList
			THISCALL_4(void, 0x0058D7E0, dlg, player_id, 1, -1);

		}	else {

			// THISCALL_1(void, 0x00584F10, dlg);
			THISCALL_1(void, 0x00583B60, dlg); // FillStartingHeroesList
			THISCALL_3(void, 0x005857D0, dlg, -1, 1); // UpdateDlgNewGameMap
		}
		

		auto subtitle = dlg->GetH3DlgItem(player_id+0x66);

		wnd_mgr->H3Redraw(my_item->GetAbsoluteX() - 16, my_item->GetAbsoluteY(),256,64);

		// my_item
		// THISCALL_4(void,0x0058D7E0, dlg, plr->player, 1, my_item);

		// flag_item->SetFrame(3);
		// THISCALL_2(int, 0x005FFA50, dlg, 0x0107 + player_id);
		// flag_item->AddState(h3::eControlState::FOCUSED);
		
		// flag_item->SendCommand(h3::eMsgSubtype::SET_FRAME, 3);
		// flag_item->Refresh();
		// my_item->SendCommand(h3::eMsgSubtype::SET_FRAME, (result+1) * 2);
		// THISCALL_3(char, 0x005857D0, dlg, -1, 1);
	}

	return EXEC_DEFAULT;
}

int __stdcall Y_NewScenarioDlg_Proc_hook(HiHook* hook, h3::H3SelectScenarioDialog* this_, h3::H3Msg* msg) {
	if (msg->itemId >= 0x8e && msg->itemId <= 0x9f) 
		return THISCALL_2(int, hook->GetDefaultFunc(), this_, msg);

	if (msg->itemId > 0x6b && msg->itemId <= 0x010e + 0x6b 
		// && msg->command == h3::eMsgCommand::MOUSE_BUTTON
		)
		return THISCALL_2(int, hook->GetOriginalFunc(), this_, msg);
	
	return THISCALL_2(int, hook->GetDefaultFunc(), this_, msg);
}

/*
_LHF_(hook_005828E9) {
	int player_color = *(int*)(c->ebp + 8) - 370;
	auto Game = h3::H3Game::Get();
	auto& v31 = Game->mapInfo.playerAttributes[player_color];
	auto& v27 = 

}
*/
_LHF_(hook_0058297C) {
	c->ebx = -1;
	if (c->edi == -1) {
		c->return_address = 0x00582983;
		return SKIP_DEFAULT;
	}
	int player_color = *(int*)(c->ebp+8) - 370;
	TownInfo_Dlg_Show(player_color, c->edi, 0);

	c->return_address = 0x00582A39;
	return SKIP_DEFAULT;
}

_LHF_(hook_005D8BD8) {
	auto town = (h3::H3Town*)(c->ecx);
	if (town->IsBuildingBuilt(88)) {
		c->return_address = 0x005D8C3C;
	}
	else {
		c->return_address = 0x005D8C95;
	}
	return SKIP_DEFAULT;
}

_LHF_(hook_005D931B) {
	auto town = (h3::H3Town*)(c->eax);
	c->eax = EighthMonType[town->type][0];
	c->return_address = 0x005D9341;
	return SKIP_DEFAULT;
}

_LHF_(hook_005DA12D) {
	auto town = (h3::H3Town*)(c->edx);
	auto &mon = h3::H3CreatureInformation::Get()[EighthMonType[town->type][0]];

	c->edx = (int) mon.defName;
	c->return_address = 0x005DA146;
	return SKIP_DEFAULT;
}

_LHF_(hook_005DDA43) {
	auto town = (h3::H3Town*)(c->ecx);
	c->eax = EighthMonType[town->type][0];
	if (c->eax < 0) c->return_address = 0x005DDA69;
	else c->return_address = 0x005DDA51;
	return SKIP_DEFAULT;
}

_LHF_(hook_005DDCC3) {
	auto town = (h3::H3Town*)(c->eax);
	auto& mon = h3::H3CreatureInformation::Get()[EighthMonType[town->type][0]];
	memcpy((void*)(c->ebp-0xA4),&mon, sizeof(h3::H3CreatureInformation));
	c->return_address = 0x005DDCE1;
	return SKIP_DEFAULT;
}

_LHF_(hook_005DD9FA) {
	auto town = (h3::H3Town*)(c->eax);
	c->ecx = eighth_creature_amount[town->number];
	c->eax = *(int*)(c->edx + 0x20);

	return SKIP_DEFAULT;
}

/*
_LHF_(hook_005DD4E7) {
	auto town = (h3::H3Town*)(c->ecx);
	c->eax = EighthMonType[town->type][0];
	c->edx = (int) & eighth_creature_amount[town->number];
	return SKIP_DEFAULT;
}
*/
_LHF_(hook_005DD4D8) {
	c->eax = recruit_eighths((h3::H3Msg*)c->edi);
	c->return_address = 0x005DD502; 
	return SKIP_DEFAULT;
}

/*
_LHF_(hook_0054B32D) {
	if (c->esi > 0x10000) return EXEC_DEFAULT;

	c->return_address = 0x0054B5DE;
	return SKIP_DEFAULT;
}
*/

extern bool newTownSelectionDialog;
extern bool EighthsSupported;

extern int town_hall_posx_index[27][20];
extern int town_hall_posy_index[27][20];

void hook_HD_Mod(void) {
	// return; // disabled
	static bool first_time = true;

	// Z_new_towns->WriteHexPatch(0x00587FD0, "558BEC83EC08");

	if (first_time) 
	{
		// Z_new_towns->WriteLoHook(0x0054B32D, hook_0054B32D);


		installMoreTownsForRandomMaps();

		installRandomTownCanBeNewFaction();

		Z_new_towns->WriteLoHook(0x0058DB8E, installMoreTownsForScenarios_0058DB8E);
	
		if (newTownSelectionDialog)
		{
			//Z_new_towns->WriteLoHook(0x005828E9, hook_005828E9);
			Z_new_towns->WriteLoHook(0x0058297C, hook_0058297C);

			Z_new_towns->WriteHiHook(0x587FD0, SPLICE_, SAFE_, THISCALL_, Y_NewScenarioDlg_Proc_hook);
			Z_new_towns->WriteLoHook(0x00588AC3, hook_00588AC3);
			Z_new_towns->WriteLoHook(0x0058692A, hook_0058692A);			
		}

		if (EighthsSupported)
		{
			Z_new_towns->WriteLoHook(0x005D8BD8, hook_005D8BD8);
			Z_new_towns->WriteLoHook(0x005D931B, hook_005D931B);
			Z_new_towns->WriteLoHook(0x005DA12D, hook_005DA12D);
			Z_new_towns->WriteLoHook(0x005DDA43, hook_005DDA43);
			Z_new_towns->WriteLoHook(0x005DDCC3, hook_005DDCC3);
			Z_new_towns->WriteLoHook(0x005DD9FA, hook_005DD9FA);
			// Z_new_towns->WriteLoHook(0x005DD4E7, hook_005DD4E7);
			Z_new_towns->WriteLoHook(0x005DD4D8, hook_005DD4D8);

			for (int i = 0; i < 27; ++i)
			{
				for (int j = 4; j < 7; ++j)
				{
					-- (town_hall_posx_index[i][j]);
					// -- (town_hall_posy_index[i][j]);
				}
				town_hall_posx_index[i][19] = 6;
				town_hall_posy_index[i][19] = 4;
			}
		}
		HMODULE h_HD_mod = GetModuleHandle(L"HD_WoG.dll");

		//if (!h_HD_mod) MessageBoxA(0, "cannot link to HD_WoG.dll", "debug", MB_OK);
		//if (!h_HD_mod) h3::H3Messagebox("cannot link to HD_WoG.dll");
		//else 
		{
			char debug[512];

			//sprintf_s(debug,"h_HD_mod is %x",h_HD_mod);
			//MessageBoxA(0, debug, "debug", MB_OK);
			h3::H3DLL HD_WoG = h3::H3DLL::H3DLL("HD_WoG.dll");
			int HookAddress = HD_WoG.NeedleSearch<16>({ 0x55, 0x8B, 0xEC, 0x51, 0x8B, 0x45, 0x08, 0x0F, 0xBE, 0x48, 0x04, 0x6B, 0xD1, 0x0E, 0x03, 0x55 }, 0);
			if (HookAddress) {
				Z_new_towns->WriteHiHook(HookAddress, SPLICE_, EXTENDED_, STDCALL_, Hook_Creatures_in_HD_mod_UI);
			}

			/*
			long address_hook_1 = reinterpret_cast<long>(h_HD_mod) + 0x498f0;
			long address_check_1 = reinterpret_cast<long>(h_HD_mod) + 0x49902;
			if (*((long*)address_hook_1) == long(0x51ec8b55) &&
				*((long*)address_check_1) == long(0x47AA82)) {
				Z_new_towns->WriteHiHook(address_hook_1, SPLICE_, EXTENDED_, STDCALL_, Hook_Creatures_in_HD_mod_UI);
			}
			else {
				//sprintf_s(debug, "h_HD_mod is %x", h_HD_mod);
				// // silent mismatch
				//MessageBoxA(0, "Incompatible HD_WoG.dll", "debug", MB_OK);
			}
			*/

		}
	}
	// if ();
	// Z_new_towns->
	first_time = false;
}