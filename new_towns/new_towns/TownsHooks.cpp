#include "patcher_x86_commented.hpp"
#include <cstdio>
// #include "HoMM3.h"
#include <string>
#include <vector>
#include "common.hpp"
extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;

struct _MouseStr_ {
	long Type;
	long SubType;
	long Item;
	long Flags;
	long Xabs;
	long Yabs;
	long NewPar;
	long Dialog;
};

#pragma warning(disable:4996)

#include"../../__include__/H3API/single_header/H3API.hpp"
#include"_Town_.h"
//typedef h3::H3Town _Town_;
typedef h3::H3Hero _Hero_;
#define o_GameMgr h3::H3Main::Get()
#define o_TownMgr h3::H3TownManager::Get()
#define o_TextBuffer ((char*)0x697428)
#define o_ActivePlayerID *(int*)0x69CCF4

inline int z_MsgBox(char* Mes, int MType, int PosX, int PosY, int Type1, int SType1, int Type2, int SType2, int Par, int Time2Show, int Type3, int SType3) {
	CALL_12(int, __fastcall, 0x004F6C00, Mes, MType, PosX, PosY, Type1, SType1, Type2, SType2, Par, Time2Show, Type3, SType3);

	int IDummy;
	__asm {
		mov eax, 0x6992D0
		mov ecx, [eax]
		mov eax, [ecx + 0x38]
		mov IDummy, eax
	}
	return IDummy;
}
void z_shout(const char* Mes) {
	z_MsgBox((char*)Mes, 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
}

extern long ThirdUpgradesInTowns_27x2x7[27 * 2 * 7];

bool cathedral_enabled = true;
bool scriptorium_enabled = true;
bool azylum_enabled = true;
bool market_of_time_enabled = true;

long multiple_builds_in_town[48];
extern long town_subtype[27];
long Azylum_Heroes[48];
char Azylum_Owners[48];

extern int Libraries[27];
extern int swapper[27][128];
extern int upper[27][128];

extern bool isBuildingEnabled(const _Town_* town, int building);


extern long EighthMonType[27][4];
extern INT16 eighth_creature_amount[48];
extern bool EighthsSupported;

inline _Hero_* GetHeroRecord(int number)
{
	int offset_21620 = *(int*)(0x0062C9C3 + 1); // 0x21620 is SoD value
	return (_Hero_*)((*(int*)0x699538) + offset_21620 + number * 0x492);
}
inline char z_Hero_Hide(_Hero_* ecx) {
	return CALL_1(char, __thiscall, 0x004D7950, ecx);
}
inline int z_Town_CreateNewGarriBars(void* TownMng) {
	return CALL_1(char, __thiscall, 0x005C7210, TownMng);
}
inline void AdvMgr_DemobilizeCurrHero(void* ecx, char a2, char a3) {
	CALL_3(void, __thiscall, 0x004175E0, ecx, a2, a3);
}

extern "C" __declspec(dllexport) h3::H3Hero * get_azylum_hero(h3::H3Town * town) {
	if (!town) return nullptr;
	int townID = town->number;
	if (Azylum_Heroes[townID] < 0) return nullptr;
	if (town->owner != Azylum_Owners[townID]) return nullptr;
	return GetHeroRecord(Azylum_Heroes[townID]);
}

_LHF_(hook_005D86CC) {
	h3::H3TownManager* mgr = (h3::H3TownManager*)(c->esi);
	_Town_* town = (_Town_*)mgr->town;
	auto plr = h3::H3ActivePlayer::Get();
	char max_heroes = *(char*)(0x005D566F);

	if (GetKeyState(VK_CONTROL) < 0 && town->IsBuildingBuilt(86)) {
		auto z_hero_owner = ((char*)o_GameMgr)+*(int*)(0x004CAB67 + 3);
		if (Azylum_Heroes[town->number] >= 0 && town->visitingHero < 0) {
			if (plr->numberHeroes >= max_heroes) {
				z_shout("Already max Adventuring Heroes. Hero in Azylum cannot join you.");
				c->return_address = 0x005D8871;
				return NO_EXEC_DEFAULT;
			}

			_Hero_* me = GetHeroRecord(Azylum_Heroes[town->number]);

			// if(me->owner >= 0 && me->owner < 64 && me->owner != plr->ownerID)
			if (Azylum_Owners[town->number] != plr->ownerID)
			{
				z_shout("Azylum is Locked from inside by enemy Hero.");
				c->return_address = 0x005D8871;
				return NO_EXEC_DEFAULT;
			}

			town->visitingHero = Azylum_Heroes[town->number];
			Azylum_Heroes[town->number] = -1; // me->isVisible = true;
			Azylum_Owners[town->number] = -1;

			z_Town_CreateNewGarriBars(mgr);
			mgr->RefreshScreen();
			for (int i = 0; i < 8; ++i) {
				if (plr->heroIDs[i] == -1) {
					plr->heroIDs[i] = me->id;
					break;
				}
			}
			// plr->hasHeroes = true;
			// ++(((char*)plr)[1]);
			++(plr->numberHeroes);

			me->owner = plr->ownerID;
			z_hero_owner[me->id] = plr->ownerID;
			z_shout("Hero Leaves Azylum");
		}
		else if ((Azylum_Heroes[town->number] < 0 && town->visitingHero >= 0)
			|| (Azylum_Heroes[town->number] == town->visitingHero && town->visitingHero >= 0)) {

			_Hero_* me = GetHeroRecord(town->visitingHero);
			Azylum_Heroes[town->number] = town->visitingHero;
			Azylum_Owners[town->number] = plr->ownerID;

			z_Hero_Hide(me); z_Town_CreateNewGarriBars(mgr);
			z_hero_owner[me->id] = 64;
			me->owner = -1; // me->owner = 64;

			mgr->RefreshScreen();

			plr->currentHero = -1; int count = 0; int hole = -1;
			// currentTown->visitingHero = -1; me->isVisible = false;
			for (int i = 0; i < 8; ++i) {
				if (plr->heroIDs[i] == me->id) {
					plr->heroIDs[i] = -1;
					hole = i;
				}
				else if (plr->heroIDs[i] >= 0)
					++count;
			}

			if (hole >= 0) {
				plr->heroIDs[hole] = plr->heroIDs[count];
				plr->heroIDs[count] = -1;
			}

			/*
			if (!count) plr->hasHeroes = false;
			-- (((char*)plr)[1]);
			*/ --(plr->numberHeroes);

			z_shout("Hero Hides in Azylum");
		}
		else z_shout("Azylum is inaccesible");
		c->return_address = 0x005D8871;
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}

void __stdcall RefreshAzylum() {
	return; // debug

	for (int t = 0; t < 48; t++) {
		auto plr = h3::H3ActivePlayer::Get();
		if (Azylum_Heroes[t] >= 0) {
			h3::H3Hero* h = P_Main->GetHero(Azylum_Heroes[t]);
			if (h->owner == plr->ownerID)
			{
				int own = h->owner;
				z_Hero_Hide(h);
				h->owner = own;

				plr->currentHero = -1; int count = 0; int hole = -1;
				// currentTown->visitingHero = -1; me->isVisible = false;
				for (int i = 0; i < 8; ++i) {
					if (plr->heroIDs[i] == Azylum_Heroes[t]) {
						plr->heroIDs[i] = -1;
						hole = i;
					}
					else if (plr->heroIDs[i] >= 0)
						++count;
				}
				if (hole >= 0) {
					plr->heroIDs[hole] = plr->heroIDs[count];
					plr->heroIDs[count] = -1;
				}
				/*
				if (!count) plr->hasHeroes = false;
				--(((char*)plr)[1]);
				*/ --(plr->numberHeroes);

			}
		}
	}
}

/*
_LHF_(hook_005BECD1) {
	char town_type = *(char*)(c->esi + 4);
	if (town_type % 9 == 2)
		c->return_address = 0x005BECD7;
	else
		c->return_address = 0x005BECF6;
	return NO_EXEC_DEFAULT;
}
*/


_LHF_(hook_005BF3E7) {
	_Town_* town = (_Town_*)c->esi;
	int lib = Libraries[town->type];

	if (lib >= 0 && town->IsBuildingBuilt(lib))
		++(c->ebx);

	c->return_address = 0x005BF3FD;
	return SKIP_DEFAULT;
}

_LHF_(hook_005BF3A6) {
	// char town_type = *(char*)(c->esi + 4);
	_Town_* town = (_Town_*)c->esi;
	int lib = Libraries[town->type];


	if (lib >= 0 /* && c->eax == lib*/) {
		c->return_address = 0x005BF3B5;
		return SKIP_DEFAULT;
	}
	else {
		c->return_address = 0x005BF430;
		return SKIP_DEFAULT;
	}
	/*
	if (lib>=0 && currentTown->IsBuildingBuilt(lib))
		c->return_address = 0x005BF3B0;
	else
		c->return_address = 0x005BF430;
	*/
	return NO_EXEC_DEFAULT;
}

_LHF_(hook_00462FD8) {
	char town_type = *(char*)(c->eax + 4);
	if (town_type % 9 == 6)
		c->return_address = 0x00462FDE;
	else
		c->return_address = 0x00462FE3;
	return NO_EXEC_DEFAULT;
}

_LHF_(hook_00462FB1) {
	// todo: replaced later with proper support 
	c->eax %= 9;
	return EXEC_DEFAULT;
}

_LHF_(hook_004947B5) {
	// todo: replaced later with proper support 
	c->eax %= 9;
	// *(int*)(c->ebp + 0xc) %= 9;
	return EXEC_DEFAULT;
}

_LHF_(hook_004AD163) {
	// todo: replaced later with proper support 
	// seems no longer needed
	char* town = (char*)*(int*)(c->esp + 0x18);
	if (town) {
		char town_type = town[4];
		if (town_type > 8) {

			*(int*)(c->esp + 0x18) = 0;
		}

	}

	return EXEC_DEFAULT;
}

/*
_LHF_(hook_0047AAD0) {
	// todo: replaced later with proper support
	if (c->ecx > 1024 || c->ecx < -2) {
		// c->ecx = 139;

		c->eax = -2;
		c->return_address = 0x0047AB40;
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}
*/


extern "C" __declspec(dllexport) long mon_upgraded_in_town(long non_upgraded);
// extern long mon_downgraded_in_town(long upgraded);
// long mon_downgraded_in_town_buf = 139;

// extern long RD_Slot;
// extern long RD_Creature;
// extern long RD_Base;

_LHF_(hook_0054FFC3) {

	long tmp = *(long*)(c->ebp + 0x14);
	long position = *(long*)(c->ebp + 0x18);

	// if (RD_Slot >= 0) {
		/*
		switch (position)
		{
		case 0: tmp = RD_Creature;
		case 1: tmp = mon_downgraded_in_town(tmp);
		case 2: tmp = mon_downgraded_in_town(tmp);
		case 3: tmp = mon_downgraded_in_town(tmp);
		default:
			break;
		}
		*/
		/*
		switch ( position + RD_Slot)
		{
		case 0: tmp = RD_Base;
		case 1: tmp = mon_upgraded_in_town(tmp);
		case 2: tmp = mon_upgraded_in_town(tmp);
		case 3: tmp = mon_upgraded_in_town(tmp);
			break;
		default:
			tmp = 139;
			break;
		}
		*/
		// tmp = RD_Base;
		// for (int i = position; i < RD_Slot; ++i)
			tmp = mon_upgraded_in_town(tmp);

		*(long*)(c->ebp + 0x14) = tmp;
	// }

	return EXEC_DEFAULT;
}


_LHF_(hook_005507D3) {

	int& ret = *(int*)(c->ebx + 0x50);

	// if (ret < 0 || ret > 1024)
		// ret = RD_Base;

	/*
	long position = *(long*)(c->ebp + 0xC);
	if(position == RD_Slot)
		*(int*)(c->ebx+0x50) = RD_Base;
	*/

	return EXEC_DEFAULT;
}


_LHF_(hook_005F45EE) {
	auto& monster = *(int*)(c->ebp + 0x8);

	// if (monster > 1024 || monster < 0) {
		// monster = RD_Base;
		//monster = mon_downgraded_in_town(last_monster);
	// }

	return EXEC_DEFAULT;
}

/*
_LHF_(hook_0054FFFA) {
	// TODO

	//static long buf = 139;
	// long &buf = mon_downgraded_in_town_buf;
	long& buf = RD_Creature;
	long position = *(long*)(c->ebp+0x18);

	if (RD_Slot >= 0) {

		//if (!position) c->edi = buf;
		//else c->edi = mon_downgraded_in_town(buf);
		//buf = c->edi;

		switch (position)
		{
		case 0: c->edi = RD_Creature;
		case 1: c->edi = mon_downgraded_in_town(c->edi);
		case 2: c->edi = mon_downgraded_in_town(c->edi);
		case 3: c->edi = mon_downgraded_in_town(c->edi);
		default:
			break;
		}
	}

	// todo: replaced later with proper support
	if (c->edi > 1024 || c->edi < 0) {
		//c->edi = 139;
		c->edi = mon_downgraded_in_town(buf);
	}
	else {
		buf = c->edi;
	}


	return EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_005507DF) {

	//static long buf = 139;
	long& buf = mon_downgraded_in_town_buf;

	// todo: replaced later with proper support
	if (c->eax > 1024 || c->eax < 0) {
		//c->eax = 139;
		c->eax = mon_downgraded_in_town(buf);
	}
	//else {
		buf = c->eax;
	//}

	return EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_005508BF) {

	//static long buf = 139;
	long& buf = mon_downgraded_in_town_buf;

	// todo: replaced later with proper support
	if (c->eax > 1024 || c->eax < 0) {
		//c->eax = 139;
		c->eax = mon_downgraded_in_town(buf);
	}
	//else {
		buf = c->eax;
	//}

	return EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_005F45F1) {
	//static long buf = 139;
	long& buf = mon_downgraded_in_town_buf;

	// todo: replaced later with proper support
	if (c->edi > 1024 || c->edi < 0) {
		//c->eax = 139;
		c->edi = mon_downgraded_in_town(buf);
	}
	//else {
		buf = c->edi;
	//}

	return EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_00550F1D) {
	//static long buf = 139;
	long& buf = mon_downgraded_in_town_buf;

	// todo: replaced later with proper support
	if (c->edx > 1024 || c->edx < 0) {
		//c->eax = 139;
		c->edx = mon_downgraded_in_town(buf);
	}
	//else {
		buf = c->edx;
	//}

	return EXEC_DEFAULT;

} */

/*
_LHF_(hook_004C6982) {

	//static long buf = 139;
	long& buf = mon_downgraded_in_town_buf;

	// todo: replaced later with proper support
	if (c->eax > 1024 || c->eax < 0) {
		//c->eax = 139;
		c->eax = mon_downgraded_in_town(buf);
	}
	//else {
		buf = c->eax;
	//}

	return EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_005F3FB5) {

	//static long buf = 139;
	long& buf = mon_downgraded_in_town_buf;

	// todo: replaced later with proper support
	if (c->eax > 1024 || c->eax < 0) {
		//c->eax = 139;
		c->eax = mon_downgraded_in_town(buf);
	}
	//else {
		buf = c->eax;
	//}

	return EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_00550F11) {

	//static long buf = 139;
	long& buf = mon_downgraded_in_town_buf;

	// todo: replaced later with proper support
	if (c->eax > 1024 || c->eax < 0) {
		//c->eax = 139;
		c->eax = mon_downgraded_in_town(buf);
	}
	//else {
		buf = c->eax;
	//}

	return EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_00550F8D) {

	//static long buf = 139;
	long& buf = mon_downgraded_in_town_buf;

	// todo: replaced later with proper support
	if (c->eax > 1024 || c->eax < 0) {
		//c->eax = 139;
		c->eax = mon_downgraded_in_town(buf);
	}
	//else {
		buf = c->eax;
	//}

	return EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_004C6976) {

	//static long buf = 139;
	long& buf = mon_downgraded_in_town_buf;

	// todo: replaced later with proper support
	if (c->edi > 1024 || c->edi < 0) {
		//c->eax = 139;
		c->edi = mon_downgraded_in_town(buf);
	}
	else {
		//buf = c->edi;
	}

	return EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_0055101C) {

	//static long buf = 139;
	long& buf = mon_downgraded_in_town_buf;

	// todo: replaced later with proper support
	if (c->eax > 1024 || c->eax < 0) {
		//c->eax = 139;
		c->eax = mon_downgraded_in_town(buf);
	}
	//else {
		buf = c->eax;
	//}

	return EXEC_DEFAULT;
}
*/

/*
_LHF_(hook_005BF3A6) {
	char town_type = *(char*)(c->esi + 0x4);
	if (town_type % 9 == 2) {
		c->return_address = 0x005BF3B0;
		return NO_EXEC_DEFAULT;
	}
	else {
		c->return_address = 0x005BF430;
		return NO_EXEC_DEFAULT;
	}
}
*/

extern long MonstersInTowns_27x2x7[27 * 2 * 7];

_LHF_(hook_005C802B) {
	long town = c->eax;
	long offset = c->edi - 0x1e;

	c->eax = MonstersInTowns_27x2x7[town * 14 + offset];

	c->return_address = 0x005C803E;
	return NO_EXEC_DEFAULT;
}

_LHF_(hook_0047ABA7) {
	long town = c->edi;
	long offset = c->edx - 0x1e;

	c->edi = c->Pop();
	c->esi = c->Pop();

	c->eax = MonstersInTowns_27x2x7[town * 14 + offset];

	c->return_address = 0x0047ABBC;
	return NO_EXEC_DEFAULT;
}

char old_mage_levels[9] = {
	4,5,5,5,5,5,3,3,5,
};

char mage_levels[27] = {
	4,5,5,5,5,5,3,3,5,
	4,5,5,5,5,5,3,3,5,
	4,5,5,5,5,5,3,3,5,
};

_LHF_(hook_00460F32) {
	// h3::H3Messagebox(std::to_string(mage_levels[c->esi]).c_str());
	// char* CurrentTownHallStructures = (char*)*(int*)0x00694EC0;   -> this one doesn't work, should be = (char*)
	// CurrentTownHallStructures[*(int*)(c->ebp+8)] = 0;

	return EXEC_DEFAULT;
}

_LHF_(hook_00460F4A)
{
	// h3::H3Messagebox(std::to_string(mage_levels[c->esi]).c_str());

	c->return_address = 0x00461025;
	return NO_EXEC_DEFAULT;

	char tmp = *(char*)(c->esi + 4);
	int level = mage_levels[tmp];
	if (level > 2) {
		c->return_address = 0x00460F80;
		return NO_EXEC_DEFAULT;
	}
	c->return_address = 0x00461025;
	return NO_EXEC_DEFAULT;

	//char tmp = *(char*)(c->esi + 4);
	/*
	if (mage_levels[tmp] == 4)
	{
		c->return_address = 0x460FB1;
		return NO_EXEC_DEFAULT;
	}
	else if (mage_levels[tmp] == 3)
	{
		c->return_address = 0x460F80;
		return NO_EXEC_DEFAULT;
	}
	*/
	//return EXEC_DEFAULT;

	//int level = mage_levels[tmp];
	int* HildLvl1Mask = (int*)0x0066CD98;
	int* HildLvl1Mask2 = HildLvl1Mask + 1;
	for (int i = 0; i < level - 1; ++i) {
		if (o_TownMgr->town->IsBuildingBuilt(i)) continue;
		else {
			c->ecx = *(int*)(c->ebp + 0x8);
			c->esi = *(int*)(c->ebp - 0x4);
			Z_new_towns->WriteByte(c->ecx + 0x694EC0, i);
			c->return_address = 0x0046102B;
			return NO_EXEC_DEFAULT;
		}

		/*
		c->edi = *(HildLvl1Mask  + i + i);
		c->ebx = *(HildLvl1Mask2 + i + i);

		c->eax = c->ecx; c->eax &= c->edi;
		c->edi = c->edx; c->edi &= c->ebx;
		c->eax |= c->edi;


		if (c->eax) {
			//++i; break;
			c->ecx = *(int*)(c->ebp + 0x8);
			c->esi = *(int*)(c->ebp - 0x4);
			Z_new_towns->WriteByte(c->ecx + 0x694EC0, i + 1);
			c->return_address = 0x0046102B;
			return NO_EXEC_DEFAULT;
			/// break;
		}
		// else break;
		*/
	}


	c->ecx = *(int*)(c->ebp + 0x8);
	c->esi = *(int*)(c->ebp - 0x4);
	Z_new_towns->WriteByte(c->ecx + 0x694EC0, level - 1);
	c->return_address = 0x0046102B;
	return NO_EXEC_DEFAULT;

	// Z_new_towns->WriteByte(c->ecx + 0x694EC0, level);

	c->return_address = 0x00461025;
	return NO_EXEC_DEFAULT;
}

_LHF_(hook_005D4059) {

	long town_type = c->eax;
	switch (town_type % 9) {

	case 2:
	case 3:
	case 5:
	case 6:
		c->return_address = 0x005D406C; break;
	default:
		c->return_address = 0x005D4617; break;
	}
	return NO_EXEC_DEFAULT;
}


_LHF_(hook_00460DE9)
{
	long lvl = mage_levels[o_TownMgr->town->type];
	// char* structures = (char*) 0x00694EC0;
	// char* current_struct = structures + c->ecx;

	c->edx &= 0xFF;

	if (c->ecx == 19) return EXEC_DEFAULT;

	_Town_* t = (_Town_*)o_TownMgr->town;

	/*
	int default_building =  c->edx & 0xff;
	if (default_building > 16) {
		// c->edx = swapper[t->type][(c->edx + 7) & 0xff] - 7;
		c->edx = swapper[t->type][default_building];
		if (c->edx != default_building) {


		}
	}
	*/

	for (int i = 0; i < lvl - 1; ++i)
		if (c->edx >= 0 && c->edx <= i
			&& t->IsBuildingBuilt(i))
			c->edx = i + 1; // fix mage guild


	if (scriptorium_enabled && c->edx >= 0 && c->edx <= 4
		&& t->IsBuildingBuilt(lvl - 1)) {
		// c->edx = lvl - 1;
		c->edx = 85 - 7;
	}


	for (int i = 7; i < 9; ++i)
		if (c->edx >= 7 && c->edx <= i
			&& t->IsBuildingBuilt(i))
			c->edx = i + 1; // fix fort

	for (int i = 10; i < 13; ++i)
		if (c->edx >= 10 && c->edx <= i
			&& t->IsBuildingBuilt(i))
			c->edx = i + 1; // fix village hall


	if (c->edx == 9 && cathedral_enabled
		&& t->IsBuildingBuilt(9))
		c->edx = 78 - 7;


	if (c->edx == 78 - 7 && t->IsBuildingBuilt(78))
		c->edx = 79 - 7;
	for (int i = 79; i <= 82; ++i)
		if (c->edx > 78 - 7 && c->edx < i - 7
			&& t->IsBuildingBuilt(i - 1))
			c->edx = i - 7; // Cathedral

	if (c->edx == 14 && market_of_time_enabled
		&& t->IsBuildingBuilt(15))
		c->edx = 87 - 7;

	/*
	for (int i = 89; i <= 91; ++i) {
		if (c->edx == i - 7 - 1 && t->IsBuildingBuilt(i - 1)) {
			c->edx = i - 7;
		}
	}
	*/

	/*
	if ((c->edx == 22 && town_subtype[o_TownMgr->currentTown->type] == 0) ||
		(c->edx == 5 && town_subtype[o_TownMgr->currentTown->type] != 0) )
	{
		c->edx = 86 - 7;
	}
	*/

	/*
	if (default_building > 16) {
		// c->edx = swapper[t->type][(c->edx + 7) & 0xff] - 7;
		c->edx = swapper[t->type][default_building];
	}
	*/

	return EXEC_DEFAULT;
}


_LHF_(hook_0046102B) {
	char* structures = (char*)0x00694EC0;
	char* current_struct = structures + c->ecx;
	// *current_struct = 9;
	_Town_* t = (_Town_*)o_TownMgr->town;

	/*
	if (cathedral_enabled && t->IsBuildingBuilt(9)) {
		*current_struct = 78;
		for (int i = 79; i <= 84; ++i)
			if (t->IsBuildingBuilt(i - 1))
				*current_struct = i; // Cathedral
	}
	*/

	if (*current_struct > 16) {
		// c->edx = swapper[t->type][(c->edx + 7) & 0xff] - 7;
		*current_struct = swapper[t->type][*current_struct];

	}

	while (upper[t->type][*current_struct] >= 0 &&
		t->IsBuildingBuilt(*current_struct))
		*current_struct = upper[t->type][*current_struct];

	return EXEC_DEFAULT;

	c->return_address = 0x0046102B;
	return NO_EXEC_DEFAULT;
}

/*
_LHF_(hook_0047AAEC) {
	if (!c->flags.ZF) return EXEC_DEFAULT;

	c->eax = mon_upgraded_in_town(c->ecx);

	c->edi = c->Pop(); c->esi = c->Pop();
	c->return_address = 0x0047AB40;
	return NO_EXEC_DEFAULT;
}
*/
_LHF_(hook_004612E2) {
	_Town_* town = (_Town_*)c->ecx;
	char* CurrentTownHallStructures = (char*)0x00694EC0;

	if ((town->IsBuildingBuilt(22) && town_subtype[town->type] == 0) ||
		(town->IsBuildingBuilt(5) && town_subtype[town->type] != 0))
	{
		CurrentTownHallStructures[8] = 86;
	}

	if (EighthsSupported) {
		CurrentTownHallStructures[19] = 88;
		for (int i = 89; i <= 91; ++i) {
			if (town->IsBuildingBuilt(i - 1)) {
				CurrentTownHallStructures[19] = i;
			}
		}
	}
	return EXEC_DEFAULT;
}

//================================================================================

//int hook_0047AAD0_last_grade_ID = 0;

// Creature_GetGradeID 
_LHF_(hook_0047AAD7) {
	//int v1; // esi
	//_CreatureInfo_* v2; // edi
	//signed int v3; // edx
	//int result; // eax
	long input = c->ecx;
	//v1 = o_pCreatureInfo[input].currentTown;
	//v2 = &o_pCreatureInfo[input];

	//c->eax = result; 
	c->eax = mon_upgraded_in_town(input);
	if (c->eax == input) c->eax = -1;

	c->return_address = 0x0047AB40;
	return NO_EXEC_DEFAULT;

	/*	c->Push(c->esi); c->Push(c->edi);

		c->eax = input * 7;


		if (input < 0 || input>1024) {
			c->ecx = 139;
			c->eax = -1;

			c->edi = c->Pop();
			c->return_address = 0x0047AB3F;
			return NO_EXEC_DEFAULT;
		}


		c->return_address = 0x0047AADB;
		return NO_EXEC_DEFAULT;*/
}

/*
_LHF_(hook_0054FFC0) {
	static int last_monster = 139;
	auto& monster = *(int*)(c->ebp + 0x14);


	if (monster > 1024 || monster < 0) {

		monster = mon_downgraded_in_town(last_monster);
	}

	last_monster = monster;
	return EXEC_DEFAULT;
}

///*
_LHF_(hook_005F45EE) {
	static int last_monster = 139;
	auto& monster = *(int*)(c->ebp + 0x8);

	if (monster > 1024 || monster < 0) {
		monster = last_monster;
		//monster = mon_downgraded_in_town(last_monster);
	}

	last_monster = monster;
	return EXEC_DEFAULT;
}
//*//*

_LHF_(hook_005507DF) {

	static long last_monster = 139;
	long monster = c->eax;

	// todo: replaced later with proper support
	if (c->eax > 1024 || c->eax < 0) {
		//c->eax = 139;
		c->eax = mon_downgraded_in_town(last_monster);
		monster = c->eax;
	}
	last_monster = monster;
	return EXEC_DEFAULT;
}

///*
_LHF_(hook_005F45F1) {
	static long last_monster = 139;
	long monster = c->edi;

	// todo: replaced later with proper support
	if (c->edi > 1024 || c->edi < 0) {
		//c->eax = 139;
		c->edi = mon_downgraded_in_town(last_monster);
		monster = c->edi;
	}

	last_monster = monster;
	return EXEC_DEFAULT;
}
//*//*
_LHF_(hook_005508BF) {
	static long last_monster = 139;
	long monster = c->eax;

	// todo: replaced later with proper support
	if (c->eax > 1024 || c->eax < 0) {
		//c->eax = 139;
		c->eax = mon_downgraded_in_town(last_monster);
		monster = c->eax;
	}


	last_monster = monster;
	return EXEC_DEFAULT;
}


_LHF_(hook_00550F11) {

	static long last_monster = 139;
	long monster = c->eax;

	// todo: replaced later with proper support
	if (c->eax > 1024 || c->eax < 0) {
		//c->eax = 139;
		c->eax = mon_downgraded_in_town(last_monster);
		monster = c->eax;
	}


	last_monster = monster;
	return EXEC_DEFAULT;
}
*/

long custom_creature_upgrade = -1;

///long creatureLevel2BuildingID[7] = {22,25,28,31,34,37,39};
_LHF_(hook_004C6946) {
	int current_creature = c->edi;
	_Hero_* hero = (_Hero_*)*(long*)(c->ebp + 0x10);
	_Town_* town = (_Town_*)*(long*)(c->ebp + 0x14);

	if (town) {
		long TT = town->type;
		long* ptr = MonstersInTowns_27x2x7 + 2 * 7 * TT;
		long* ptr2 = ThirdUpgradesInTowns_27x2x7 + 2 * 7 * TT;
		for (int i = 0; i < 7; ++i)
			if (ptr[i] == current_creature &&
				town->IsBuildingBuilt(i + 37))
			{
				c->eax = ptr[i + 7]; goto found;
			}
		for (int i = 0; i < 7; ++i)
			if (ptr[i + 7] == current_creature &&
				town->IsBuildingBuilt(i + 64))
			{
				c->eax = ptr2[i]; goto found;
			}
		for (int i = 0; i < 7; ++i)
			if (ptr2[i] == current_creature &&
				town->IsBuildingBuilt(i + 64 + 7))
			{
				c->eax = ptr2[i + 7]; goto found;
			}
	}

found:
	if (custom_creature_upgrade >= 0)
		c->eax = custom_creature_upgrade;

	custom_creature_upgrade = -1;

	return EXEC_DEFAULT;
}

extern "C" __declspec(dllexport) void set_custom_creature_upgrade(long arg) {
	custom_creature_upgrade = arg;
}

long long additional_buildings_per_town[48] = {};
signed int __stdcall Town_IsBuildingBuilt_hook(HiHook* hook, _Town_* _this_, int a2, char a3) {
	int ret = 0; // 0x004305A0

	if (a2 < 0) return false;
	if (a2 < 44 && a2 >= 0) return CALL_3(int, __thiscall, hook->GetDefaultFunc(), _this_, a2, a3);
	if (a2 >= 64 && a2 < 128) return (additional_buildings_per_town[_this_->number] & (1LL << (a2 - 64)) ? 1 : 0);


	return ret;
}


int __stdcall Town_BuildStructureInTown_hook(HiHook* hook, _Town_* _this_, const int building_id) {
	int ret = building_id;
	// if(building_id>0 && building_id <5) CALL_2(int, __thiscall, 0x005BED30, _this_, building_id - 1);

	if (building_id == 13 && cathedral_enabled) CALL_2(int, __thiscall, 0x005BED30, _this_, 78);

	if (building_id < 44 && building_id >= 0) return CALL_2(int, __thiscall, hook->GetDefaultFunc(), _this_, building_id);
	if (building_id >= 64 && building_id < 128) additional_buildings_per_town[_this_->number] |= 1LL << (building_id - 64);
	if (building_id >= 64 + 7 && building_id < 64 + 14)
		if (!_this_->IsBuildingBuilt(building_id - 7))
			CALL_2(int, __thiscall, 0x005BED30, _this_, building_id - 7);
	if (building_id >= 64 && building_id < 64 + 7)
		if (!_this_->IsBuildingBuilt(building_id - 64 + 37))
			CALL_2(int, __thiscall, 0x005BED30, _this_, building_id - 64 + 37);
	if (building_id >= 37 && building_id < 44)
		if (!_this_->IsBuildingBuilt(building_id - 7))
			CALL_2(int, __thiscall, 0x005BED30, _this_, building_id - 7);
	if (building_id > 78 && building_id <= 84)
		CALL_2(int, __thiscall, 0x005BED30, _this_, building_id - 1);

	if (building_id == 88) {
		eighth_creature_amount[_this_->number] =
			P_CreatureInformation[EighthMonType[_this_->type][0]].grow;
	}

	return ret;
}

_LHF_(hook_00732C74) {
	if (c->eax == 2 || c->eax == 3) {
		// long level = *(long*)(c->ebp - 0x2c);
		long town = *(long*)(c->ebp - 0x44); c->eax -= 2;
		// *(long*)(c->ebp - 8) = /* currentTown*14*4 + */ (long)ThirdUpgradesInTowns_27x2x7;
		*(long*)(c->ebp - 8) += (long)ThirdUpgradesInTowns_27x2x7 - (long)MonstersInTowns_27x2x7 - 14 * 4;
	}

	return EXEC_DEFAULT;
}

_LHF_(hook_0070E71B) {
	// _Town_* (*GetCastleBase)() = (_Town_ * (*)()) 0x00711BD4;
	int building_number = *(int*)(c->ecx + 0x2b0);
	int CA_B_case = *(int*)(c->ecx + 0x2ac);
	// _Town_* dp = GetCastleBase();
	if (building_number >= 64 && building_number < 128) {
		//_Town_* CurrentTown = (_Town_*) (*(int*)(c->ebp - 0x10) + *(int*)(c->ebp - 8));
		_Town_* CurrentTown = (_Town_*)(*(int*)(c->ebp - 0x10));
		if (CA_B_case == 6) {
			c->return_address = 0x0070E73B;
			return NO_EXEC_DEFAULT;
		}
		if (CA_B_case == 3) {
			char* ERMFlags = (char*)0x91F2E0;
			*ERMFlags = CurrentTown->IsBuildingBuilt(building_number) ? 1 : 0;
			c->return_address = 0x70E965;
			return NO_EXEC_DEFAULT;
		}
		if (CA_B_case == 2) {
			additional_buildings_per_town[CurrentTown->number] &= ~(1LL << (building_number - 64));
			c->return_address = 0x70E965;
			return NO_EXEC_DEFAULT;

		}
		if (CA_B_case == 1) {
			additional_buildings_per_town[CurrentTown->number] |= 1LL << (building_number - 64);
			c->return_address = 0x70E965;
			return NO_EXEC_DEFAULT;

		}
	}

	return EXEC_DEFAULT;
}

_LHF_(hook_00428964) {
	if (c->esi < 14)
		c->eax = MonstersInTowns_27x2x7[c->edx];
	else {
		c->eax = ThirdUpgradesInTowns_27x2x7[c->edx - 14];
	}
	if (c->eax < 0) {
		c->return_address = 0x00428977;
		return NO_EXEC_DEFAULT;
	}

	c->return_address = 0x0042896B;
	return NO_EXEC_DEFAULT;
}

_LHF_(hook_005BFFDF) {
	if (*(int*)(c->ebp - 12) < 7)
		c->eax = MonstersInTowns_27x2x7[c->edx]; // Fix growth display on unupgraded creatures by Trebuchet
	else if (*(int*)(c->ebp + 8) < 14)
		c->eax = MonstersInTowns_27x2x7[c->edx];
	else {
		int building = 64 + *(int*)(c->ebp + 8) - 14;
		_Town_* Town = (_Town_*)c->esi;
		if (!(Town->IsBuildingBuilt(building))) {
			c->return_address = 0x005BFFBD;
			return NO_EXEC_DEFAULT;
		}
		// c->eax = ThirdUpgradesInTowns_27x2x7[c->edx - 14];
		c->eax = MonstersInTowns_27x2x7[c->edx - 14];
	}
	if (c->eax < 0) {
		c->return_address = 0x005C0049;
		return NO_EXEC_DEFAULT;
	}

	c->return_address = 0x005BFFE6;
	return NO_EXEC_DEFAULT;
}


_LHF_(hook_005C0098) {
	if (*(int*)(c->ebp - 12) < 7)
		c->eax = MonstersInTowns_27x2x7[c->edx]; // Fix growth display on unupgraded creatures by Trebuchet
	else if (*(int*)(c->ebp + 8) < 14)
		c->eax = MonstersInTowns_27x2x7[c->edx];
	else {
		int building = 64 + *(int*)(c->ebp + 8) - 14;
		_Town_* Town = (_Town_*)c->esi;
		if (!(Town->IsBuildingBuilt(building))) {
			c->return_address = 0x005BFFBD;
			return NO_EXEC_DEFAULT;
		}
		// c->eax = ThirdUpgradesInTowns_27x2x7[c->edx - 14];
		c->eax = MonstersInTowns_27x2x7[c->edx - 14];
	}

	if (c->eax < 0) {
		c->edx = 0;
		c->return_address = 0x005C00FC;
		return NO_EXEC_DEFAULT;
	}

	c->return_address = 0x005C009F;
	return NO_EXEC_DEFAULT;
}

/*
_LHF_(AI_Town_BuyCreatures_00428602) {

	if (*(int*)(c->ebp + 8) < 14)
		c->eax = MonstersInTowns_27x2x7[c->edx];
	else
		c->eax = ThirdUpgradesInTowns_27x2x7[c->edx - 14];

	if (c->eax < 0) {

		c->return_address = 0x00428623;
		return NO_EXEC_DEFAULT;

	}

	c->return_address = 0x00428609;
	return NO_EXEC_DEFAULT;
}
*/

/* Commented by SadnessPower 20241029
_LHF_(hook_005BFF6C) {
	if (c->ebx > 14) c->ebx -= 14;
	return EXEC_DEFAULT;
	if (c->ebx > 14)
		c->ebx -= 14;
}
*/
_LHF_(hook_005C015B) {
	if ((c->eax & 65535) > 14)
		c->eax -= 14;
	return EXEC_DEFAULT;
}

extern int new_buildings_cost[27][64][7];
bool can_afford_new_building(int town_type, int building_ID, int owner = -1) {
	int* cost = new_buildings_cost[town_type][building_ID - 64];
	int* have = owner < 0
		? (int*)&h3::H3Main::Get()->players[o_ActivePlayerID].playerResources
		: (int*)&h3::H3Main::Get()->players[owner].playerResources;
	for (int i = 0; i < 7; ++i)
		if (have[i] < cost[i])
			return false;
	return true;
}

extern long new_TownsBuildingsCost0[];
bool can_afford_old_dwellings(int town_type, int building_ID) {
	long* cost = new_TownsBuildingsCost0 + 98 * town_type + (building_ID - 30) * 7;
	int* have = (int*)&h3::H3Main::Get()->players[o_ActivePlayerID].playerResources;
	for (int i = 0; i < 7; ++i)
		if (have[i] < cost[i])
			return false;
	return true;
}

bool can_afford_common_building(int building_ID) {
	int* TownNormalBuildingResourcesReq = (int*)(0x006A8160);
	int* cost = TownNormalBuildingResourcesReq + building_ID * 7;
	// int* have = (int*)&h3::H3Main::Get()->GetPlayer()->playerResources;
	int* have = (int*)&h3::H3Main::Get()->players[o_ActivePlayerID].playerResources;

	for (int i = 0; i < 7; ++i)
		if (have[i] < cost[i])
			return false;
	return true;
}

extern long new_TownsBuildingsCost1[27 * 9 * 7];
extern "C" __declspec(dllexport) void set_TownsBuildingsCost1(long town_type, long building_ID, long res0, long res1, long res2, long res3, long res4, long res5, long res6) {
	if (building_ID < 17 || building_ID > 26) return;
	long* res_ptr = new_TownsBuildingsCost1 + town_type * 9 * 7 + (building_ID - 17) * 7;;
	res_ptr[0] = res0; res_ptr[1] = res1; res_ptr[2] = res2; res_ptr[3] = res3; res_ptr[4] = res4; res_ptr[5] = res5; res_ptr[6] = res6;
}
bool can_afford_special_building(int town_type, int building_ID) {
	long* cost = new_TownsBuildingsCost1 + town_type * 9 * 7 + (building_ID - 17) * 7;
	// int* have = (int*)&h3::H3Main::Get()->GetPlayer()->playerResources;
	int* have = (int*)&h3::H3Main::Get()->players[o_ActivePlayerID].playerResources;

	for (int i = 0; i < 7; ++i)
		if (have[i] < cost[i])
			return false;
	return true;
}

bool can_afford_the_building(int town_type, int building_ID) {
	if (building_ID >= 0 && building_ID < 17) return can_afford_common_building(building_ID);
	if (building_ID >= 17 && building_ID <= 26) return can_afford_special_building(town_type, building_ID);
	if (building_ID >= 30 && building_ID <= 43) return can_afford_old_dwellings(town_type, building_ID);
	if (building_ID >= 64 && building_ID <= 127) return can_afford_new_building(town_type, building_ID);

	return false;
}

extern char new_buildings_names[27][64][512];
extern char new_buildings_descriptions[27][64][2048];
_LHF_(hook_00460E24) {
	long building = c->edx & 0xff;
	long town_manager = c->esi;
	long town_slot = c->ecx;

	_Town_* town = (_Town_*)*(int*)(town_manager + 0x38);
	if (town_slot < 7 && building > 36 && building < 44)
		if (town->IsBuildingBuilt(building)) {
			int mon_Level = building - 37;
			if (0 <= ThirdUpgradesInTowns_27x2x7[14 * town->type + mon_Level])
				building = mon_Level + 64;
			/*
			if (currentTown->IsBuildingBuilt(building, 0)) {
				building += 7;
			}
			*/
		}
	if (town_slot < 7 && building >= 64 && building <= 70)
		if (town->IsBuildingBuilt(building)) {
			int mon_Level = building - 64;
			if (0 <= ThirdUpgradesInTowns_27x2x7[14 * town->type + mon_Level + 7])
				building = mon_Level + 71;
		}

	c->edx &= 0xffffff00;
	c->edx |= building;
	return EXEC_DEFAULT;
}
char hook_00461491_text[64] = "Unknown Building";
_LHF_(hook_00461491) {
	if (c->ecx < 44) return EXEC_DEFAULT;
	int& v32 = c->eax; int& v33 = c->ecx;
	//c->ecx = (int) hook_00461491_text;
	c->ecx = (int)new_buildings_names[v32][v33 - 64];
	c->return_address = 0x004614A4;
	return NO_EXEC_DEFAULT;
}
char hook_0046174C_text[64] = "Unknown Building";
_LHF_(hook_0046174C) {
	long building = c->esi;
	if (building >= 64 && building < 256) {
		int town = o_TownMgr->town->type;
		//strcpy_s(o_TextBuffer, 64, hook_0046174C_text);
		//sprintf_s(o_TextBuffer, 200, "%s (%d)", hook_0046174C_text, building);
		strcpy_s(o_TextBuffer, 64, new_buildings_names[town][building - 64]);

		c->return_address = 0x00461B30;
		return NO_EXEC_DEFAULT;
	}
	else return EXEC_DEFAULT;
}

/*
_LHF_(hook_00461360)   // fix requirement costs retrieval for buildings
					   // badfix: it seemed working at first but not afterwards
{
	c->return_address = 0x00461372;
	return SKIP_DEFAULT;

	char* CurrentTownHallStructures = (char*)0x00694EC0;

	h3::H3Town* currentTown = o_TownMgr->currentTown;
	int building_ID = CurrentTownHallStructures[c->edi];
	bool can_afford = can_afford_the_building(currentTown->type, building_ID);

	c->return_address = can_afford ? 0x004613A4 : 0x00461372;

	return SKIP_DEFAULT;
}
*/


int __stdcall town_SetupStructsEnDisOnScreen_hook(HiHook* h, _Town_* _this_) {
	return CALL_1(int, __thiscall, h->GetDefaultFunc(), _this_);
}

auto Player_HasCapitol2 = (int(__thiscall*)(void*)) 0x004B9C00; //5BF8E0
_LHF_(hook_00461507) {
	int building = c->eax & 0xff;
	_Town_* v36 = (_Town_*)c->ecx;


	/*
	if (building < 64 ) { // should work with new buildings too ?


		if (c->ebx != 0 && c->ebx != 1 &&                   // c->ebx: -1: you can build now, 0: already done 2: you can't build now
			!can_afford_the_building(v36->type, building))  // if you can't afford the building, set the red background
			c->ebx = 2;
		return EXEC_DEFAULT;
	}
	*/
	/*
	c->ebx = v36->IsBuildingBuilt(building) ? 0 : (v36->builtThisTurn ? 1 :
		can_afford_new_building(v36->type, building)? -1 : 2);
	*/
	/*
	if (c->ebx == -1 && !v36->CanBeBuiltNow(building))
		// !hasAllRequiredBuildings(v36, building))
			c->ebx = 1;
	*/

	if (building == 13 && Player_HasCapitol2(&o_GameMgr->players[v36->owner]) && !v36->IsBuildingBuilt(13))
		return _Town_::RED_CANTBUILDNOW;      // if player already has a Capitol
	if (building < 44 && !v36->CanBeBuilt((h3::eBuildings)building))
		c->ebx = _Town_::RED_CANTBUILDNOW;
	else
		c->ebx = v36->CanBuildIcon(building);

	c->return_address = 0x00461563;// 0x0046156C;
	return NO_EXEC_DEFAULT;
}

extern bool hasAllRequiredBuildings(const h3::H3Town* town, int building);
extern bool hasAllRequiredBuildings(const _Town_* town, int building);

_LHF_(hook_00461C8B)         // fix building without resource requirement
{                            // !!! THIS HOOK is never catched
	int& v33_isBuyButtonDisabled = c->eax;
	int build_id = c->esi;
	_Town_* v29 = *(_Town_**)(c->ecx + 56);

	v33_isBuyButtonDisabled =
		!can_afford_the_building(v29->type, build_id)
		|| !hasAllRequiredBuildings(v29, build_id);

	return EXEC_DEFAULT;
}

_LHF_(hook_00461BE5) {
	long building = c->esi;

	_MouseStr_* cmdstr = (_MouseStr_*)c->ebx; // *(int*) (c->ebp+8);// c->ebx;
	_MouseStr_* cmdstra = (_MouseStr_*)(((unsigned int)cmdstr->Flags >> 9) & 1);
	/*
	bool can_build = !o_TownMgr->currentTown->IsBuildingBuilt(building) && !o_TownMgr->currentTown->builtThisTurn
		&& can_afford_new_building(o_TownMgr->currentTown->type, building)
		&& hasAllRequiredBuildings((_Town_*)o_TownMgr->currentTown, building);
		*/
	bool can_build = ((_Town_*)o_TownMgr->town)->CanBeBuiltNow(building);
	if (building >= 0 && building < 44) {
		if (can_build) {
			// c->return_address = 0x00461D2F;
			// return SKIP_DEFAULT;
			int v33 = 0;
			if (CALL_4(int, __thiscall, 0x005D6330, o_TownMgr, building, v33, cmdstra)) {
				if (cmdstr) {
					cmdstr->Item = 10; cmdstr->SubType = 10;
				}

				o_TownMgr->town->built.value |= (1LL << building);
			}
		}
		else {
			int v33 = 1; CALL_4(int, __thiscall, 0x005D6330, o_TownMgr, building, v33, cmdstra);
		}

		c->eax = 2;

		c->return_address = 0x00461D2F;
		return NO_EXEC_DEFAULT;

	}
	else
		if (building >= 64 && building < 256) {

			if (can_build) {
				int v33 = 0;
				if (CALL_4(int, __thiscall, 0x005D6330, o_TownMgr, building, v33, cmdstra)) 
				{
					if (building < 128) additional_buildings_per_town[o_TownMgr->town->number] |= (1LL << (building - 64));
					if (cmdstr) {
						cmdstr->Item = 10; cmdstr->SubType = 10;
					}
				}
			}
			else {
				int v33 = 1; CALL_4(int, __thiscall, 0x005D6330, o_TownMgr, building, v33, cmdstra);
			}

			c->eax = 2;

			c->return_address = 0x00461D2F;
			return NO_EXEC_DEFAULT;
		}
		else return EXEC_DEFAULT;
}
short __stdcall town_All_res_num_cost_build_hook(HiHook* h, int _this_, signed int a2, int a3, void* a4) {
	if (a2 < 64) return CALL_4(short, __thiscall, h->GetDefaultFunc(), _this_, a2, a3, a4);

	__int16 v4; // bx
	// int* v5; // esi
	// int v6; // ecx
	int v7; // edx
	int* v8; // ecx
	int v9; // eax
	signed int v11;

	v4 = 0;
	v7 = 0;
	memset(a4, 0, 0x1Cu);

	char town = *(char*)(_this_ + 4);
	//v8 = veryspecialcosts;// v5;
	v8 = new_buildings_cost[town][a2 - 64];

	v11 = 7;
	do
	{
		if (*v8 > 0)
		{
			v9 = 4 * v4++;
			*(__int32*)(v9 + a3) = v7;
			*(__int32*)((char*)a4 + v9) = *v8;
		}
		++v7;
		++v8;
		--v11;
	} while (v11);
	return v4;
}
/*
char __stdcall isBuildingEnabled_005C1260(HiHook* h, int _this_, int a2) {
	// wrong hook
	if (a2 < 64)
		return CALL_2(char, __thiscall, h->GetDefaultFunc(), _this_, a2);

	return 1;
}
*/
_LHF_(hook_005D5B08) {
	int& cadre = c->edx;
	if (cadre >= 64 && cadre <= 127) {
		// if (cadre < 78) cadre = (cadre - 64) % 7 + 37;
	}
	return EXEC_DEFAULT;
}
char Town_GetBuildingName_hook_text[64] = "Inside Unknown Building";
/*
int __stdcall Town_GetBuildingName_hook(HiHook* h, int a1, signed int a2) {
	if (a2 < 64) return CALL_2(int, __fastcall, h->GetDefaultFunc(), a1, a2);
	// return ((int)Town_GetBuildingName_hook_text);
	return (int)new_buildings_names[a1][a2 - 64];
}
*/
_LHF_(hook_005D5B56) {
	return EXEC_DEFAULT;
}
_LHF_(hook_005D3134) {
	int building = c->edx;
	if (building < 64) return EXEC_DEFAULT;
	int town = c->ebx;

	// todo: add proper descriptions
	c->edi = (int)new_buildings_names[town][building - 64];
	if (new_buildings_descriptions[town][building - 64][0])
		c->edi = (int)new_buildings_descriptions[town][building - 64];

	c->return_address = 0x005D3147;
	return NO_EXEC_DEFAULT;
}
_LHF_(hook_004613EC) {
	int& cadre = c->ecx; int& slot = c->edi;
	h3::H3Town* town = h3::PH3TownManager(c->esi)->town;
	char* CurrentTownHallStructures = (char*)0x00694EC0;

	if (EighthsSupported) {
		CurrentTownHallStructures[19] = 88;
		for (int i = 89; i <= 91; ++i) {
			if (town->IsBuildingBuilt(i - 1)) {
				CurrentTownHallStructures[19] = i;
			}
		}
	}

	/*
	if (cadre >= 64 && cadre <= 77) {
		cadre = 37 + (cadre - 64) % 7;
	}
	*/
	return EXEC_DEFAULT;
}

char __stdcall town_CanAIBuildThis_hook(HiHook* h, _Town_* town, __int16 building_id)
{
	if (!isBuildingEnabled(town, building_id) || town->IsBuildingBuilt(building_id)) return 0;

	if (swapper[town->type][building_id] != building_id) return 0;

	if (building_id < 44) return CALL_2(char, __thiscall, h->GetDefaultFunc(), town, building_id);
	if (o_GameMgr->towns[town->number].builtThisTurn) return 0;

	return hasAllRequiredBuildings(town, building_id);
}

_LHF_(hook_005BF811) {
	if (c->edi >= 64 && c->edi <= 127) {
		char town_type = c->eax;
		int* cost = new_buildings_cost[town_type][c->edi - 64];
		c->edi = (int)cost;
		c->return_address = 0x005BF84C;
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}


_LHF_(hook_005C1486)
{
	if (c->eax < 44)return EXEC_DEFAULT;

	char town = *(char*)(c->ecx + 4);
	c->eax = (int)new_buildings_cost[town][c->eax - 64];
	c->return_address = 0x005C14E0;
	return NO_EXEC_DEFAULT;
}

char __stdcall Town_AIBuildingBuilt_hook(HiHook* h, _Town_* town, int a2, signed int BuildingType) { // 0x005BF7C0
	if (town->IsBuildingBuilt(BuildingType) || !isBuildingEnabled(town, BuildingType))
		return false;

	if (BuildingType < 44) return FASTCALL_3(char, h->GetDefaultFunc(), town, a2, BuildingType);
	if (BuildingType < 64) return false;

	// auto v5 = new_buildings_cost[town->type][BuildingType - 64];
	auto v7 = &o_GameMgr->players[town->owner];
	if (can_afford_new_building(town->type, BuildingType, town->owner))
	{
		THISCALL_4(int, 0x005BF1E0, town, BuildingType, 1, 1);
		return true;
	}
	else
		return false;
}


/* @SadnessPower hook disabled on 20241022
_LHF_(hook_0042B2C4) {
	if (c->esi < 44)
		return EXEC_DEFAULT;

	c->return_address = 0x0042B2CA;
	return NO_EXEC_DEFAULT;
}
*/

int __stdcall Town_CanBuild_ByCost(HiHook* h, _Town_* town, signed int build_id) {
	if (build_id < 44) return FASTCALL_2(int, h->GetDefaultFunc(), town, build_id);
	if (build_id < 64) return false;
	if (build_id >= 128) return false;

	return can_afford_new_building(town->type, build_id, *(char*)0x0069CCF4);
}

/*
_LHF_(hook_0042B275) {
	c->edx = c->esi + (c->eax << 7);
	c->return_address = 0x0042B27E;
	return NO_EXEC_DEFAULT;
}
*/
// long multiple_builds_in_town[48];

_LHF_(MultipleBuildsHandlerForBuildStructureInTown_Full_005BF265)
{
	auto town = (h3::H3Town*)c->esi;
	auto& count = multiple_builds_in_town[town->number];
	if (count > 1) {
		--count;
		c->return_address = 0x005BF2A4;
		return NO_EXEC_DEFAULT;
	}
	return EXEC_DEFAULT;
}

// int veryspecialcosts[7] = {0,0,0,0,0,0,777};
int __stdcall town_GetResourcesRequirements_hook(HiHook* h, int _this_, signed int a2, int* a3) {
	// needed by AI, for new buildings, do not remove
	char town_type = *(char*)(_this_ + 4);
	if (a2 >= 64 && a2 <= 127) {
		//memcpy(a3, veryspecialcosts, 28u);
		memcpy(a3, new_buildings_cost[town_type][a2 - 64], 28u);
		return a2; //  (a2 - 64);
	}
	//else return CALL_3(int, __thiscall, h->GetDefaultFunc(), _this_, a2, a3);
	else {
		int ret = CALL_3(int, __thiscall, h->GetDefaultFunc(), _this_, a2, a3);
		return ret;
	}
}

extern int Libraries[27];
_LHF_(hook_005BECD1) {
	h3::H3Town* town = (h3::H3Town*)c->esi;
	int building = Libraries[town->type];
	if (building >= 0) {
		bool built = ((_Town_*)town)->IsBuildingBuilt(building);
		if (built) ++(c->eax);
	}

	c->return_address = 0x005BECF6;
	return SKIP_DEFAULT;
}
typedef h3::H3CreatureInformation _CreatureInfo_;
#define o_pCreatureInfo h3::H3CreatureInformation::Get()

extern char new_TownHorde1MonLevel[27 * 2];
extern char new_TownHorde2MonLevel[27 * 2];

// Trebuchet Hooks Start

// a1 - townType | a2 - buildingID
int __stdcall Town_GetBuildingName_hook(HiHook* h, int a1, signed int a2) {
	if (a2 > 43 && a2 < 128) {
		return (DWORD)new_buildings_names[a1 - 1][a2]; // Bug? Why must we subtract 1?
	}
	
	return CALL_2(int, __fastcall, h->GetDefaultFunc(), a1, a2);
}

// Fort - Building Names
_LHF_(hook_005DD8C6)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	UINT8* slotAddress = (UINT8*)(c->edi);
	int slot = *slotAddress;

	int offset = (slot > 6) ? (slot - 7) : slot;

	for (int i = 0; i < 7; ++i)
	{
		if (offset == i)
		{
			if (town->IsBuildingBuilt(71 + i))
			{
				char* buildingName = new_buildings_names[townType - 1][71 + i];
				strncpy(o_TextBuffer, buildingName, 511);
				o_TextBuffer[511] = '\0';
			}
			else if (town->IsBuildingBuilt(64 + i))
			{
				char* buildingName = new_buildings_names[townType - 1][64 + i];
				strncpy(o_TextBuffer, buildingName, 511);
				o_TextBuffer[511] = '\0';
			}
		}
	}

	return EXEC_DEFAULT;
}

// Fort - Creature Names
_LHF_(hook_005DD96B)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	int* slotAddress = (int*)(c->ebp - 12);
	int slot = *slotAddress;

	for (int i = 0; i < 7; ++i)
	{
		if (slot == 33 + i)
		{
			if (town->IsBuildingBuilt(71 + i))
			{
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7 + i];
				return SKIP_DEFAULT;
			}
			else if (town->IsBuildingBuilt(64 + i))
			{
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + i];
				return SKIP_DEFAULT;
			}

		}
	}

	return EXEC_DEFAULT;
}

// Fort - Creature Stats
_LHF_(hook_005DDAD6)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	int* slotAddress = (int*)(c->ebp - 4);
	int slot = *slotAddress;

	for (int i = 0; i < 7; ++i)
	{
		if (slot == 49 + i)
		{
			if (town->IsBuildingBuilt(71 + i))
			{
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7 + i];
				return SKIP_DEFAULT;
			}
			else if (town->IsBuildingBuilt(64 + i))
			{
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + i];
				return SKIP_DEFAULT;
			}

		}
	}

	return EXEC_DEFAULT;
}

// Fort - Dwelling 0 Creature Display
_LHF_(hook_005D9DE4)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	if (town->IsBuildingBuilt(71)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7];
		return NO_EXEC_DEFAULT;
	}
	else if (town->IsBuildingBuilt(64)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14];
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}

// Fort - Dwelling 1 Creature Display
_LHF_(hook_005D9E5D)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	if (town->IsBuildingBuilt(72)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 8];
		return NO_EXEC_DEFAULT;
	}
	else if (town->IsBuildingBuilt(65)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 1];
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}

// Fort - Dwelling 2 Creature Display
_LHF_(hook_005D9ED3)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	if (town->IsBuildingBuilt(73)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 9];
		return NO_EXEC_DEFAULT;
	}
	else if (town->IsBuildingBuilt(66)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 2];
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}

// Fort - Dwelling 3 Creature Display
_LHF_(hook_005D9F4C)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	if (town->IsBuildingBuilt(74)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 10];
		return NO_EXEC_DEFAULT;
	}
	else if (town->IsBuildingBuilt(67)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 3];
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}

// Fort - Dwelling 4 Creature Display
_LHF_(hook_005D9FC5)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;
	if (town->IsBuildingBuilt(75)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 11];
		return NO_EXEC_DEFAULT;
	}
	else if (town->IsBuildingBuilt(68)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 4];
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}

// Fort - Dwelling 5 Creature Display
_LHF_(hook_005DA03E)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	if (town->IsBuildingBuilt(76)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 12];
		return NO_EXEC_DEFAULT;
	}
	else if (town->IsBuildingBuilt(69)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 5];
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}

// Fort - Dwelling 6 Creature Display
_LHF_(hook_005DA1BA)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	if (town->IsBuildingBuilt(77)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 13];
		return NO_EXEC_DEFAULT;
	}
	else if (town->IsBuildingBuilt(70)) {
		c->edx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 6];
		return NO_EXEC_DEFAULT;
	}

	return EXEC_DEFAULT;
}

// Fort - Hint
_LHF_(hook_005DD099)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;
	int slot = c->edx % 7;

	for (int i = 0; i < 7; ++i)
	{
		if (slot == i)
		{
			if (town->IsBuildingBuilt(71 + i))
			{
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7 + i];
				return NO_EXEC_DEFAULT;
			}
			else if (town->IsBuildingBuilt(64 + i))
			{
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + i];
				return NO_EXEC_DEFAULT;
			}

		}
	}

	return EXEC_DEFAULT;
}

// Town - 3rd/4th Upgrade Creature Right Click Display in Bottom-left Corner
_LHF_(hook_005C6023)
{
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	int* slotAddress = (int*)(c->ebp + 8);
	int slot = *slotAddress;

	for (int i = 0; i < 7; ++i)
	{
		if (slot == 7 + i)
		{
			if (town->IsBuildingBuilt(71 + i))
			{
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7 + i];
				return NO_EXEC_DEFAULT;
			}
			else if (town->IsBuildingBuilt(64 + i))
			{
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + i];
				return NO_EXEC_DEFAULT;
			}
		}
	}

	return EXEC_DEFAULT;
}

// Town - Hint
_LHF_(hook_005C803E) {
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;
	int building = c->edi;
	int monId = c->eax;

	for (int i = 0; i < 7; ++i) {
		if (building == 37 + i) {
			if (town->IsBuildingBuilt(71 + i)) {
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7 + i];
				return NO_EXEC_DEFAULT;
			}
			else if (town->IsBuildingBuilt(64 + i)) {
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + i];
				return NO_EXEC_DEFAULT;
			}
		}
	}

	int horde1 = new_TownHorde1MonLevel[townType * 2];
	int horde2 = new_TownHorde2MonLevel[townType * 2];

	if (building == 18 || building == 19) {
		if (town->IsBuildingBuilt(71 + horde1)) {
			c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7 + horde1];
			return NO_EXEC_DEFAULT;
		}
		else if (town->IsBuildingBuilt(64 + horde1)) {
			c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + horde1];
			return NO_EXEC_DEFAULT;
		}
	}
	
	if (building == 24 || building == 25) {
		if (town->IsBuildingBuilt(71 + horde2)) {
			c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7 + horde2];
			return NO_EXEC_DEFAULT;
		}
		else if (town->IsBuildingBuilt(64 + horde2)) {
			c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + horde2];
			return NO_EXEC_DEFAULT;
		}
	}

	return EXEC_DEFAULT;
}

// Town - RMB Click Dwelling
_LHF_(hook_00551B68) {
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;
	int slot = c->eax % 7;

	for (int i = 0; i < 7; ++i) {
		if (slot == i) {
			if (town->IsBuildingBuilt(71 + i)) {
				c->ecx = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7 + i];
				return NO_EXEC_DEFAULT;
			}
			else if (town->IsBuildingBuilt(64 + i)) {
				c->ecx = ThirdUpgradesInTowns_27x2x7[townType * 14 + i];
				return NO_EXEC_DEFAULT;
			}
		}
	}

	return EXEC_DEFAULT;
}

_LHF_(hook_005519A7) {
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	int* slotAddress = (int*)(c->ebp + 12);
	int slot = *slotAddress;

	for (int i = 0; i < 7; ++i) {
		if (slot == i) {
			if (town->IsBuildingBuilt(37 + i)) {
				c->eax = MonstersInTowns_27x2x7[townType * 14 + 7 + i];
				return NO_EXEC_DEFAULT;
			}
			else if (town->IsBuildingBuilt(30 + i)) {
				c->eax = MonstersInTowns_27x2x7[townType * 14 + i];
				return NO_EXEC_DEFAULT;
			}
		}
	}

	return EXEC_DEFAULT;
}

_LHF_(hook_00551A14) {
	h3::H3Town* town = o_TownMgr->town;
	int townType = o_TownMgr->townType;

	int* slotAddress = (int*)(c->ebp + 12);
	int slot = *slotAddress;

	for (int i = 0; i < 7; ++i) {
		if (slot == 7 + i) {
			if (town->IsBuildingBuilt(37 + i)) {
				c->edx = MonstersInTowns_27x2x7[townType * 14 + i];
				return NO_EXEC_DEFAULT;
			}
		}
	}

	return EXEC_DEFAULT;
}

// Kingdom Overview - 3rd/4th Upgrade Creatures Display
_LHF_(hook_0051CFD8) {
	int* townSetupAddress = reinterpret_cast<int*>(c->ebp - 0x14);
	h3::H3Town* town = reinterpret_cast<h3::H3Town*>(*townSetupAddress);
	int townType = town->type;

	int slot = c->esi;
	int offset = (slot > 6) ? (slot - 7) : slot;

	for (int i = 0; i < 7; ++i) {
		if (offset == i) {
			if (town->IsBuildingBuilt(71 + i)) {
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7 + i];
				return NO_EXEC_DEFAULT;
			}
			else if (town->IsBuildingBuilt(64 + i)) {
				c->eax = ThirdUpgradesInTowns_27x2x7[townType * 14 + i];
				return NO_EXEC_DEFAULT;
			}
		}
	}

	return EXEC_DEFAULT;
}

// Kingdom Overview - Hint
_LHF_(hook_00521C7E) {
	h3::H3Game* gameMgr = h3::H3Game::Get();
	if (gameMgr) {
		h3::H3Player* currentPlayer = gameMgr->GetPlayer();
		if (currentPlayer) {
			int townId = *reinterpret_cast<int*>(c->ebp + 0x8);
			h3::H3Town* town = &gameMgr->towns[townId];
			int townType = town->type;

			int slot = c->ebx;

			for (int i = 0; i < 7; ++i) {
				if (slot == 26 + i || slot == 40 + i) {
					if (town->IsBuildingBuilt(71 + i)) {
						long monsterId = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7 + i];
						_CreatureInfo_* monsterInfo = &o_pCreatureInfo[monsterId];
						char* monsterName = (char*)monsterInfo->nameSingular;

						const char* formatStr = *reinterpret_cast<const char**>(0x006A8080);
						sprintf(o_TextBuffer, formatStr, monsterName);
					}
					else if (town->IsBuildingBuilt(64 + i)) {
						long monsterId = ThirdUpgradesInTowns_27x2x7[townType * 14 + i];
						_CreatureInfo_* monsterInfo = &o_pCreatureInfo[monsterId];
						char* monsterName = (char*)monsterInfo->nameSingular;

						const char* formatStr = *reinterpret_cast<const char**>(0x006A8080);
						sprintf(o_TextBuffer, formatStr, monsterName);
					}
				}
				else if (slot == 76 + i || slot == 90 + i) {
					DWORD baseAddress = 0x006A5DC4;
					DWORD firstPtr = *reinterpret_cast<DWORD*>(baseAddress);
					firstPtr += 32;
					DWORD secondPtr = *reinterpret_cast<DWORD*>(firstPtr);
					DWORD finalAddress = secondPtr + (589 * 4);
					LPCSTR textPtr = *reinterpret_cast<LPCSTR*>(finalAddress);

					if (town->IsBuildingBuilt(71 + i)) {
						long monsterId = ThirdUpgradesInTowns_27x2x7[townType * 14 + 7 + i];
						_CreatureInfo_* monsterInfo = &o_pCreatureInfo[monsterId];
						char* monsterName = (char*)monsterInfo->nameSingular;

						sprintf(o_TextBuffer, textPtr, monsterName);
					}
					else if (town->IsBuildingBuilt(64 + i)) {
						long monsterId = ThirdUpgradesInTowns_27x2x7[townType * 14 + i];
						_CreatureInfo_* monsterInfo = &o_pCreatureInfo[monsterId];
						char* monsterName = (char*)monsterInfo->nameSingular;

						sprintf(o_TextBuffer, textPtr, monsterName);
					}
				}
			}
		}
	}

	return EXEC_DEFAULT;
}

// Trebuchet Hooks End

int getPriorityFor3rdAnd4thUpgradeBuildings(_Town_* town, int building, h3::H3Resources& creatureBuyMaxResourceCost);
/*
_LHF_(hook_0042AF0F) {
	if(c->edi < 44)	return EXEC_DEFAULT;
	if (c->edi > 127) {
		c->return_address = 0x0042B04D;
		return NO_EXEC_DEFAULT;
	}
	else {
		int new_v42_offset = -(0x694 + 128 * 4); // old var_114
		int new_v40_offset = -(0x694 + 128 * 4 + 28 * 128);
		int* ptr = (int*)(c->ebp + new_v42_offset);
		_Town_* currentTown = (_Town_*) c->ebx;
		int* res = (int*)(c->ebp + new_v40_offset + 28 * c->edi);
		//todo
		ptr[c->edi] = getPriorityFor3rdAnd4thUpgradeBuildings(c->edi, currentTown, res);

		c->return_address = 0x0042B1CB;
		return NO_EXEC_DEFAULT;
	}

}
*/


/* @SadnessPower hook disabled on 20241022
_LHF_(hook_0042B2C1) {
	if (c->esi < 44) return EXEC_DEFAULT;
	if (c->esi > 127) return EXEC_DEFAULT;

	// todo
	if (c->esi >= 64) {

	}

	c->return_address = 0x0042B2C0;
	return NO_EXEC_DEFAULT;

}
*/

void fixBuggedBuiltMask(h3::H3Town* town)  /* The town->built mask has 2 known bugs :
									        *
									        * 1) Sometimes when a castle is built, the build mask is erroneously like this:
									        *    Citadel = 1 and Castle = 1. This will cause a siege having Citadel defense instead of Castle
									        *
									        * 2) Sometimes dwellingX and dwellingUX are both 0 but the building is built.
									        *    This will cause AI not building dwelling upgrade anymore for that town
									        */
{
	// fixing (1)
	if (town->built.castle && town->built.citadel)
		town->built.citadel = false;

	// fixing (2) from built2 mask, which is correct
	town->built.dwelling1 = town->built2.dwelling1u ? false : town->built2.dwelling1;
	town->built.dwelling1u = town->built2.dwelling1u;
	town->built.dwelling2 = town->built2.dwelling2u ? false : town->built2.dwelling2;
	town->built.dwelling2u = town->built2.dwelling2u;
	town->built.dwelling3 = town->built2.dwelling3u ? false : town->built2.dwelling3;
	town->built.dwelling3u = town->built2.dwelling3u;
	town->built.dwelling4 = town->built2.dwelling4u ? false : town->built2.dwelling4;
	town->built.dwelling4u = town->built2.dwelling4u;
	town->built.dwelling5 = town->built2.dwelling5u ? false : town->built2.dwelling5;
	town->built.dwelling5u = town->built2.dwelling5u;
	town->built.dwelling6 = town->built2.dwelling6u ? false : town->built2.dwelling6;
	town->built.dwelling6u = town->built2.dwelling6u;
	town->built.dwelling7 = town->built2.dwelling7u ? false : town->built2.dwelling7;
	town->built.dwelling7u = town->built2.dwelling7u;
}

_LHF_(fixBuggedBuiltMask_0042AE6B)
{
	h3::H3Town* town = (h3::H3Town*)(int)(c->ebx);
	if (town != NULL)
		fixBuggedBuiltMask(town);

	return EXEC_DEFAULT;
}

_LHF_(fixBuggedBuiltMask_00463712)
{
	h3::H3Town* town = (h3::H3Town*)*(int*)(c->ebp + 0x18);
	if (town != NULL)
		fixBuggedBuiltMask(town);

	return EXEC_DEFAULT;
}


int buildingPrioritiesRaw[128];                 // Priority: Usefulness,without cost
int buildingPrioritiesWithBuildingCosts[128];   // Priority: Usefulness taking into account the cost
int buildingPrioritiesAfterCalculations[128];   // Priority: only buildable building dependency-wise, even if not affordable
                                                // if a building cannot be built due to dependency, his priority will be added to the required buildings and then set to zero on itself            


_LHF_(SkipOriginalMaxPriorityCalculation_0042B3A6)
{
	c->return_address = 0x0042B3C9;
	return SKIP_DEFAULT;
}

_LHF_(SkipOriginalPriorityCalculationForRequiredBuildings_0042B3A6)
{
	c->return_address = 0x0042B3C9;
	return SKIP_DEFAULT;
}


_LHF_(BuildPrioritiesReset_0042AE99)
{
	for (int i = 0;i < 128;i++)
	{
		buildingPrioritiesRaw[i] = 0;
		buildingPrioritiesWithBuildingCosts[i] = 0;
		buildingPrioritiesAfterCalculations[i] = 0;
	}

	return EXEC_DEFAULT;
}

_LHF_(StoringBuildingPrioritiesAndSkipOriginalCode_0042B340)
{
	int building = (*(int*)(c->ebp - 0x14) - 0x0066CD98) / 2 / 4;
	buildingPrioritiesWithBuildingCosts[building] = c->eax;

	c->return_address = 0x0042B36C;
	return EXEC_DEFAULT;
}

_LHF_(StoringBuildingPriorities_0042B378)
{
	buildingPrioritiesRaw[c->esi] = *(int*)(c->ebp - 0x114 + (c->esi * 4));
	// buildingPrioritiesWithBuildingCosts[c->esi] = *(int*)(c->ebp - 0x1C4 + (c->esi * 4));
	return EXEC_DEFAULT;
}

_LHF_(StoringBuildingPriorities_0042B3B9)
{
	buildingPrioritiesAfterCalculations[c->esi] = c->eax;
	return EXEC_DEFAULT;
}


extern bool isBuildingRequired(const _Town_* t, int building, int required);
extern bool isBuildingEnabled(const _Town_* town, int building);

_LHF_(AI_Player_BuildBestBuildingOfAllTowns_0042B3D8)
{
	// int* &v31 = (int*)(c->ebp - (0x694 + 128 * 4 + 28 * 128 + 4*128));
	//int    v41 = *(int*)(c->ebp - (0x694 + 128 * 4 + 28 * 128 + 4 * 128));

	int& buildingToBuild = *(int*)(c->ebp - 0x18);
	_Town_*& buildingToBuild_Town = *(_Town_**)(c->ebp - 0x28);
	int& priorityMax = *(int*)(c->ebp - 0x30);
	_Town_* currentTown = (_Town_*)c->ebx;

	if (currentTown->builtThisTurn)
		return EXEC_DEFAULT;

	// setting priority of TUM Upgrade buildings, only if there are buildable
	for (int building = 64; building < 128; building++)
	{
		if (currentTown->IsBuildingBuilt(building))
			continue;

		if (!isBuildingEnabled(currentTown, building))
			continue;

		// Exit if the previous building has not been built
		if (building >= 64 && building <= 70 && !currentTown->IsBuildingBuilt(building - 27))
			continue;

		if (building >= 71 && building <= 77 && !currentTown->IsBuildingBuilt(building - 7))
			continue;

		h3::H3Resources creatureBuyMaxResourceCost;
		int currentPriority = getPriorityFor3rdAnd4thUpgradeBuildings(currentTown, building, creatureBuyMaxResourceCost);
		buildingPrioritiesRaw[building] = currentPriority;

		if (currentPriority > 0)
		{
			// add the building costs to the calculations
			int* cost = new_buildings_cost[currentTown->type][building - 64];
			for (int i = 0; i < 7; ++i)
				creatureBuyMaxResourceCost[i] += cost[i];

			// adjusting the priority according to building and creatures' costs
			auto currentPlayerNumber_1 = *(char**)(c->ebp - 0x2C);
			int AI_AIPlayer_Get_Blgd_Built_ValWithCost = 0x0042A150;
			buildingPrioritiesWithBuildingCosts[building] = THISCALL_3(int, AI_AIPlayer_Get_Blgd_Built_ValWithCost, currentPlayerNumber_1, currentPriority, &creatureBuyMaxResourceCost);
		}
	}

	// if a building has dependency, its priority is added to the required building
	for (int building = 0; building < 128; building++)
	{
		if (currentTown->IsBuildingBuilt(building))
			continue;

		if (!isBuildingEnabled(currentTown, building))
			continue;

		// Exit if the previous building has not been built
		if (building >= 37 && building <= 43 && !currentTown->IsBuildingBuilt(building - 7))
			continue;

		if (building >= 64 && building <= 70 && !currentTown->IsBuildingBuilt(building - 27))
			continue;

		if (building >= 71 && building <= 77 && !currentTown->IsBuildingBuilt(building - 7))
			continue;


		if (buildingPrioritiesWithBuildingCosts[building] > 0)
		{
			if (hasAllRequiredBuildings(currentTown, building))
			{				
				// if (building > 43)  // for building < 44 the priority has already been added to required building by internal code
				buildingPrioritiesAfterCalculations[building] += buildingPrioritiesWithBuildingCosts[building];
			}
			else
			{				
				//int requiredBuilding = building < 44 ? 64 : 0;  // for building < 44 the priority has already been added to required building by internal code

				for (int requiredBuilding = 0;requiredBuilding < 128;requiredBuilding++)
				{
					if (currentTown->IsBuildingBuilt(requiredBuilding) || !isBuildingRequired(currentTown, building, requiredBuilding))
						continue;

					if (hasAllRequiredBuildings(currentTown, requiredBuilding))
						buildingPrioritiesAfterCalculations[requiredBuilding] += buildingPrioritiesWithBuildingCosts[building];
					else
						if (requiredBuilding <= 4)        // Mage guild level handling
						{
							for (int mageGuildLevel = requiredBuilding - 1;mageGuildLevel >= 0;mageGuildLevel--)
								if (!currentTown->IsBuildingBuilt(mageGuildLevel) && hasAllRequiredBuildings(currentTown, mageGuildLevel))
								{
									buildingPrioritiesAfterCalculations[mageGuildLevel] += buildingPrioritiesWithBuildingCosts[building];
									break;
								}
						}
				}
			}
		}
	}

	// completely recalculate from scratch maxPriority buildings
	for (int building = 0; building < 128; building++)
		if (!currentTown->IsBuildingBuilt(building) && hasAllRequiredBuildings(currentTown, building) && buildingPrioritiesAfterCalculations[building] > priorityMax)
		{
			priorityMax = buildingPrioritiesAfterCalculations[building];
			buildingToBuild_Town = currentTown;
			buildingToBuild = building;
		}

	return EXEC_DEFAULT;
}

_LHF_(AIBuildOrUpgradeMageGuild_UseProperFunctionForCheckingResourcesRequirement_0042B449)    /* In AI_Player_BuildBestBuildingOfAllTowns there is special exception on 42B449 for verifying resource requirements
																							   * for upgradable non-dwelling building (10..15). Mage Guild falls in such category, but is not included
																							   * This hook fixes this.
																							   * On original code Mage Guild priority is not evaluated, so it will never build here and the code is corrected
																							   * Mods can modify building dependencies so this correction is needed
																							   * @SadnessPower
																							   */
{
	int building = (int)(c->ebx);

	if ((building > 4 && building < 10) || building > 15)
		return EXEC_DEFAULT;
	else
	{
		c->return_address = 0x0042B453;
		return SKIP_DEFAULT;
	}
}



int __stdcall BuildStructureInTown_Full_hook(HiHook* h, _Town_* Town, int Structure2Build, char a3, char a4) {


	int ret = THISCALL_4(int, h->GetDefaultFunc(), Town, Structure2Build, a3, a4);
	if (Structure2Build < 44) return ret;
	if (Structure2Build < 64) return false;

	return isBuildingEnabled(Town, Structure2Build) & THISCALL_2(int, 0x005BED30, Town, Structure2Build);
}

typedef h3::H3CreatureInformation _CreatureInfo_;
#define o_pCreatureInfo h3::H3CreatureInformation::Get()

long debugHookAddress = 0x004C70E5;
_LHF_(debugHook)
{
	return EXEC_DEFAULT;
	// *(int*)(c->ebp - 0x04) * ((*(_CreatureInfo_*)*(int*)(c->ebp - 0x08)).aiValue -  (*(_CreatureInfo_*)(int)(c->esi)).aiValue)
	int monsterId = c->edi;
	_CreatureInfo_* monsterInfo = &o_pCreatureInfo[monsterId];
	//_CreatureInfo_* v42 = (_CreatureInfo_)c->ebp - 0x80;
	int returnSub = *(int*)(c->eax);
}

long debugHookAddress2 = 0x004E5000;
_LHF_(debugHook2)
{
	return EXEC_DEFAULT;
	int monsterId = c->edi;
	_CreatureInfo_* monsterInfo = &o_pCreatureInfo[monsterId];
	int v17 = c->ebx;
	return EXEC_DEFAULT;
}

void patch_AI_Player_BuildBestBuildingOfAllTowns() 
{
	// return; //debug only

//	int new_v42_offset = -(0x694 + 128 * 4); // old var_114
//	int new_v40_offset = -(0x694 + 128 * 4 + 28 * 128);
//	int new_v41_offset = new_v40_offset - (128 * 4);
//	Z_new_towns->WriteDword(0x0042AE03 + 2, 0x694 + 128 * 4 + 28 * 128 + 128 * 4);
/*	int v42_access[] = {0x0042AF31 + 3,0x0042AF46 + 3, 0x0042AF5C + 3, 0x0042AF83 + 3,
		0x0042AF99 + 3, 0x0042AFB3 + 3, 0x0042AFC9 + 3, 0x0042AFE3 + 3, 0x0042AFF9 + 3,
		0x0042B014 + 3, 0x0042B02A + 3, 0x0042B098 + 3, 0x0042B0B5 + 3, 0x0042B0CF + 3,
		0x0042B0FF + 3, 0x0042B15C + 3, 0x0042B16F + 3, 0x0042B1AC + 3, 0x0042B1B7 + 3,
		0x0042B1C0 + 3, 0x0042B1E6 + 2,
		0 };
	for (int i = 0; v42_access[i]; ++i)
		Z_new_towns->WriteDword(v42_access[i], new_v42_offset);*/

		/*	int v40_access[] = {0x0042AEA0 + 2, 0x0042AEF2 + 3, 0x0042B1EC + 2,
				0 };
			for (int i = 0; v40_access[i]; ++i)
				Z_new_towns->WriteDword(v40_access[i], new_v40_offset);
			Z_new_towns->WriteDword(0x0042AE99 + 1, 7 * 128);*/

			// Z_new_towns->WriteLoHook(0x0042AF0F, hook_0042AF0F);
			// Z_new_towns->WriteByte(0x0042B1CC + 2, 0x7f);    // commented by SadnessPower: the building priority for new buildings is calculated later, so it's fine to leave the cycle < 44
			//Z_new_towns->WriteByte(0x0042B1D0, 0x8e);

			//Z_new_towns->WriteLoHook(0x0042B2C1, hook_0042B2C1);42B2C4


	Z_new_towns->WriteLoHook(0x0042AE6B, fixBuggedBuiltMask_0042AE6B);
	Z_new_towns->WriteLoHook(0x00463712, fixBuggedBuiltMask_00463712);
	
	Z_new_towns->WriteLoHook(0x0042AE99, BuildPrioritiesReset_0042AE99);
	
	Z_new_towns->WriteLoHook(0x0042B340, StoringBuildingPrioritiesAndSkipOriginalCode_0042B340);
	Z_new_towns->WriteLoHook(0x0042B378, StoringBuildingPriorities_0042B378);
	Z_new_towns->WriteLoHook(0x0042B3B9, StoringBuildingPriorities_0042B3B9);

	Z_new_towns->WriteLoHook(0x0042B3A6, SkipOriginalMaxPriorityCalculation_0042B3A6);

	Z_new_towns->WriteLoHook(0x0042B3D8, AI_Player_BuildBestBuildingOfAllTowns_0042B3D8);

	Z_new_towns->WriteLoHook(0x0042B449, AIBuildOrUpgradeMageGuild_UseProperFunctionForCheckingResourcesRequirement_0042B449);

	/*	int v41_access[] = {0x0042B1DC + 2, 0x0042B340 + 2, 0x0042B39B + 2,
			0 };
		for (int i = 0; v41_access[i]; ++i)
			Z_new_towns->WriteDword(v41_access[i], new_v41_offset);
		Z_new_towns->WriteDword(0x0042B1D5 + 1, 128);*/

	if (azylum_enabled)
		Z_new_towns->WriteLoHook(0x004612E2, hook_004612E2);
}

char entries_00642BE8[27+1][0x1e8];
int table_00642BE8[27+1] = {
	0x006A4998,	0x006A45C0, 0x006A3E50, 0x006A4038, 0x006A47A8, 0x006A4B80, 0x006A5138, 0x006A4F50, 0x006A4D68, 0x006A4990,
	
};

char /* h3::H3IndexVector */ TownNameTxtIndex[27][0x18];

void fill_towns(void) {
	for (int i = 9; i < 27;++i) {
		memcpy(entries_00642BE8[1 + i], entries_00642BE8[1 + i - 9], 0x1e8);
		memcpy(TownNameTxtIndex[i], TownNameTxtIndex[i - 9], 0x18);
	}
}
int RMG_TownNativeLand[27] = {
	2,2,3,7,0,0,5,4,2,
	2,2,3,7,0,0,5,4,2,
	2,2,3,7,0,0,5,4,2,
};

extern char towns_enabled_in_RMG[27][4];
_LHF_(town_names_004CA98A) {
	c->esi %= 9;
	return EXEC_DEFAULT;
}
char __stdcall RMG_ToPlaceTowns_00544F40_hook(HiHook* hook, h3::H3RmgRandomMapGenerator* _this_, h3::H3RmgZoneGenerator* arg) {
	auto& town_prototypes = _this_->objectPrototypes[98];

	if (town_prototypes.Count() == 9) {
		for (int i = 9; i < 27; ++i) {

			auto town_properties = new h3::H3RmgObjectProperties(**town_prototypes[i % 9]);

			town_properties->subtype = i;
			town_prototypes.Push(&town_properties);
		}
	}

	if (town_prototypes.Count() > 9) {
		while (!towns_enabled_in_RMG[arg->townType][arg->zoneInfo->type]) {
			arg->townType = h3::H3Random::RandBetween(0, 26);
		}
	}

	return THISCALL_2(char, hook->GetDefaultFunc(), _this_, arg);
}

int bonus_gold_per_town[48] = {};
int __stdcall income_hook_005BFA00(HiHook* hook, h3::H3Town* town, bool include_silo) {
	int ret = THISCALL_2(int, hook->GetDefaultFunc(), town, include_silo);
	ret += bonus_gold_per_town[town->number];
	return ret;
}

void hook_towns(void) 
{
	{
		DWORD Addresses[] = { 0x5B4169 + 3, 0x5B4179 + 3, 0x5B4D63 + 3, 0x5B4E58 + 3,
			0x5B4F1C + 3, 0x5B5252 + 3, 0x5B533A + 3, 0x5B53CA + 3, 0x5B55B8 + 3, 0x5B5608 + 3, 
			0x5B566E + 3, 0x5B5737 + 3, 0x5B57F6 + 3, 0x5B5881 + 3, 0x5B5A36 + 3, 0x5B5AC1 + 3, 
			0x5B5BEF + 3, 0x5B63C1 + 3, 0x5B63E7 + 3, 0x5B6544 + 3, 0x5B6C43 + 3, 0x5B6C51 + 3, 
			0x5B6C9F + 3, 0x5B6CAD + 3, 0x5B6CFA + 3, 0x5B6D08 + 3, 0x5B6D55 + 3, 0x5B6D63 + 3, 
			0x5B6DAD + 3, 0x5B6DBB + 3, 0x5B6E05 + 3, 0x5B6E13 + 3, 0x5B6E5D + 3, 0x5B6E6B + 3, 
			0x5B7304 + 3, 0x5B7708 + 3, 0x5B7950 + 3, 0x5B7AF5 + 3, };
		for (auto i : Addresses) {
			if (*(int*)i == 0x00642BE8)
				Z_new_towns->WriteDword(i,(int)table_00642BE8);
			else {
				continue;
			}
		}
		Z_new_towns->WriteDword(0x4CA6AE + 1, (int)TownNameTxtIndex);
		Z_new_towns->WriteDword(0x4CA6E9 + 1, (int)TownNameTxtIndex);
		Z_new_towns->WriteDword(0x4CA756 + 1, 8 + (int)TownNameTxtIndex);
		Z_new_towns->WriteDword(0x4CA98D + 3, (int)TownNameTxtIndex);
		Z_new_towns->WriteDword(0x4CA9AF + 3, (int)TownNameTxtIndex);
		Z_new_towns->WriteByte(0x004CA6AA + 1, 27);
		Z_new_towns->WriteByte(0x004CA6E9 + 1, 27);

		if (true) {
			Z_new_towns->WriteDword(0x005B3E8D + 1, (int)entries_00642BE8[0]);
			Z_new_towns->WriteDword(0x005B3EB0 + 1, (int)entries_00642BE8[0]);

			Z_new_towns->WriteDword(0x005B3ECD + 1, (int)entries_00642BE8[1]);
			Z_new_towns->WriteDword(0x005B3EF0 + 1, (int)entries_00642BE8[1]);

			Z_new_towns->WriteDword(0x005B3F0D + 1, (int)entries_00642BE8[2]);
			Z_new_towns->WriteDword(0x005B3F30 + 1, (int)entries_00642BE8[2]);

			Z_new_towns->WriteDword(0x005B3F4D + 1, (int)entries_00642BE8[3]);
			Z_new_towns->WriteDword(0x005B3F70 + 1, (int)entries_00642BE8[3]);

			Z_new_towns->WriteDword(0x005B3F8D + 1, (int)entries_00642BE8[4]);
			Z_new_towns->WriteDword(0x005B3FB0 + 1, (int)entries_00642BE8[4]);

			Z_new_towns->WriteDword(0x005B3FCD + 1, (int)entries_00642BE8[5]);
			Z_new_towns->WriteDword(0x005B3FF0 + 1, (int)entries_00642BE8[5]);

			Z_new_towns->WriteDword(0x005B400D + 1, (int)entries_00642BE8[6]);
			Z_new_towns->WriteDword(0x005B4030 + 1, (int)entries_00642BE8[6]);

			Z_new_towns->WriteDword(0x005B404D + 1, (int)entries_00642BE8[7]);
			Z_new_towns->WriteDword(0x005B4070 + 1, (int)entries_00642BE8[7]);

			Z_new_towns->WriteDword(0x005B408D + 1, (int)entries_00642BE8[8]);
			Z_new_towns->WriteDword(0x005B40B0 + 1, (int)entries_00642BE8[8]);

			// Z_new_towns->WriteDword(0x005B40C0 + 1, (int)entries_00642BE8[27]);
			// Z_new_towns->WriteDword(0x005B40E0 + 1, (int)entries_00642BE8[27]);

			Z_new_towns->WriteDword(0x005B40C0 + 1, (int)entries_00642BE8[9]);
			Z_new_towns->WriteDword(0x005B40E0 + 1, (int)entries_00642BE8[9]);
			for (int i = 0; i <= 27; ++i)
				table_00642BE8[i] = (int)entries_00642BE8[i];
		}
	}

//	Z_new_towns->WriteLoHook(debugHookAddress, debugHook);   // floating hook for debugging purpose
//	Z_new_towns->WriteLoHook(debugHookAddress2, debugHook2);   // floating hook for debugging purpose

//	Z_new_towns->WriteLoHook(0x005C805D, hook_005C805D);

//	Z_new_towns->WriteLoHook(0x005C7196, hook_005C7196);

//	Z_new_towns->WriteLoHook(0x005C7D1E, hook_005C7D1E);

//	Z_new_towns->WriteLoHook(0x005C7CE5, hook_005C7CE5);

	Z_new_towns->WriteLoHook(0x00521C7E, hook_00521C7E); // Kingdom Overview - Hint
	Z_new_towns->WriteLoHook(0x0051CFD8, hook_0051CFD8); // Kingdom Overview - 3rd/4th Upgrade Creatures Display

//	Z_new_towns->WriteLoHook(0x005519A7, hook_005519A7);
//	Z_new_towns->WriteLoHook(0x00551A14, hook_00551A14);

	Z_new_towns->WriteLoHook(0x00551B68, hook_00551B68); // Town - RMB Click Dwelling
	Z_new_towns->WriteLoHook(0x005C803E, hook_005C803E); // Town - Hint
	Z_new_towns->WriteLoHook(0x005C6023, hook_005C6023); // Town - 3rd/4th Upgrade Creature Right Click Display in Bottom-left Corner

	Z_new_towns->WriteLoHook(0x005D9DE4, hook_005D9DE4); // Fort - Dwelling 0 Creature Display
	Z_new_towns->WriteLoHook(0x005D9E5D, hook_005D9E5D); // Fort - Dwelling 1 Creature Display
	Z_new_towns->WriteLoHook(0x005D9ED3, hook_005D9ED3); // Fort - Dwelling 2 Creature Display
	Z_new_towns->WriteLoHook(0x005D9F4C, hook_005D9F4C); // Fort - Dwelling 3 Creature Display
	Z_new_towns->WriteLoHook(0x005D9FC5, hook_005D9FC5); // Fort - Dwelling 4 Creature Display
	Z_new_towns->WriteLoHook(0x005DA03E, hook_005DA03E); // Fort - Dwelling 5 Creature Display
	Z_new_towns->WriteLoHook(0x005DA1BA, hook_005DA1BA); // Fort - Dwelling 6 Creature Display

	Z_new_towns->WriteLoHook(0x005DD099, hook_005DD099); // Fort - Hint
	Z_new_towns->WriteLoHook(0x005DD8C6, hook_005DD8C6); // Fort - Building Names
	Z_new_towns->WriteLoHook(0x005DD96B, hook_005DD96B); // Fort - Creature Names
	Z_new_towns->WriteLoHook(0x005DDAD6, hook_005DDAD6); // Fort - Creature Stats

	// needed for AI
	Z_new_towns->WriteHiHook(0x005C14F0, SPLICE_, EXTENDED_, THISCALL_, town_GetResourcesRequirements_hook);

	// Multiple builds per turn
    Z_new_towns->WriteLoHook(0x005BF265, MultipleBuildsHandlerForBuildStructureInTown_Full_005BF265);

	memset(multiple_builds_in_town, 0, sizeof(multiple_builds_in_town));

	Z_new_towns->WriteHiHook(0x005BF7C0, SPLICE_, EXTENDED_, FASTCALL_, Town_AIBuildingBuilt_hook);
	Z_new_towns->WriteHiHook(0x005BF1E0, SPLICE_, EXTENDED_, THISCALL_, BuildStructureInTown_Full_hook);
	// Z_new_towns->WriteLoHook(0x0042B2C4, hook_0042B2C4);
	Z_new_towns->WriteHiHook(0x00460D10, SPLICE_, EXTENDED_, FASTCALL_, Town_CanBuild_ByCost);
	// Z_new_towns->WriteLoHook(0x0042B275, hook_0042B275);

	Z_new_towns->WriteHiHook(0x005C1120, SPLICE_, EXTENDED_, THISCALL_, town_CanAIBuildThis_hook);
	Z_new_towns->WriteLoHook(0x005BF811, hook_005BF811);
	patch_AI_Player_BuildBestBuildingOfAllTowns();
	Z_new_towns->WriteLoHook(0x005C1486, hook_005C1486);

	Z_new_towns->WriteLoHook(0x004613EC, hook_004613EC);
	Z_new_towns->WriteLoHook(0x005D3134, hook_005D3134);
	Z_new_towns->WriteHiHook(0x00460CC0, SPLICE_, EXTENDED_, FASTCALL_, Town_GetBuildingName_hook);
	Z_new_towns->WriteLoHook(0x005D5B08, hook_005D5B08);
	// Z_new_towns->WriteHiHook(0x005C1260, SPLICE_, EXTENDED_, THISCALL_, isBuildingEnabled_005C1260);
	Z_new_towns->WriteHiHook(0x005C1580, SPLICE_, EXTENDED_, THISCALL_, town_All_res_num_cost_build_hook);
	Z_new_towns->WriteLoHook(0x00461BE5, hook_00461BE5);
	Z_new_towns->WriteLoHook(0x00461507, hook_00461507);
	Z_new_towns->WriteHiHook(0x005C1320, SPLICE_, EXTENDED_, THISCALL_, town_SetupStructsEnDisOnScreen_hook);

	Z_new_towns->WriteLoHook(0x0046174C, hook_0046174C);
	Z_new_towns->WriteLoHook(0x00461491, hook_00461491);
	Z_new_towns->WriteLoHook(0x00460E24, hook_00460E24);

	// Z_new_towns->WriteLoHook(0x005BFF6C, hook_005BFF6C);
	Z_new_towns->WriteLoHook(0x005C015B, hook_005C015B);

	// fix it please not disable 2021-09-27 22:00
	Z_new_towns->WriteLoHook(0x00428964, hook_00428964);
	//Z_new_towns->WriteByte(0x0042897E + 2, 28);

	Z_new_towns->WriteLoHook(0x005BFFDF, hook_005BFFDF);
	Z_new_towns->WriteLoHook(0x005C0098, hook_005C0098);

	/*
	// fix it please not disable 2021-09-25 12:36
	Z_new_towns->WriteLoHook(0x00428602, AI_Town_BuyCreatures_00428602);
	Z_new_towns->WriteDword(0x004285E2 + 3, 28);
	Z_new_towns->WriteDword(0x00428885 + 1, 1740 * 2);
	Z_new_towns->WriteDword(0x004288C4 + 2, 1740 * 2);
	*/

	Z_new_towns->WriteLoHook(0x0070E71B, hook_0070E71B);

	Z_new_towns->WriteLoHook(0x00732C74, hook_00732C74);
	Z_new_towns->WriteByte(0x00732C4B + 3, 3);

	Z_new_towns->WriteHiHook(0x005BED30, SPLICE_, THISCALL_, Town_BuildStructureInTown_hook);
	Z_new_towns->WriteHiHook(0x004305A0, SPLICE_, EXTENDED_, THISCALL_, Town_IsBuildingBuilt_hook);
	Z_new_towns->WriteLoHook(0x004C6946, hook_004C6946);

	//Z_new_towns->WriteLoHook(0x00550F11, hook_00550F11);
	//Z_new_towns->WriteLoHook(0x005508BF, hook_005508BF);
	// // Z_new_towns->WriteLoHook(0x005F45F1, hook_005F45F1);
	// Z_new_towns->WriteLoHook(0x005507DF, hook_005507DF);
	// // Z_new_towns->WriteLoHook(0x005F45EE, hook_005F45EE);
	// Z_new_towns->WriteLoHook(0x0054FFC0, hook_0054FFC0);
	Z_new_towns->WriteLoHook(0x0047AAD7, hook_0047AAD7);

	// Z_new_towns->WriteLoHook(0x0047AAEC, hook_0047AAEC);

	Z_new_towns->WriteLoHook(0x005BECD1, hook_005BECD1);
	Z_new_towns->WriteLoHook(0x005BF3E7, hook_005BF3E7);
	Z_new_towns->WriteLoHook(0x005BF3A6, hook_005BF3A6);
	Z_new_towns->WriteLoHook(0x00462FD8, hook_00462FD8);

	// Z_new_towns->WriteLoHook(0x00462FB1, hook_00462FB1);
	// Z_new_towns->WriteLoHook(0x004947B5, hook_004947B5);

	// Z_new_towns->WriteLoHook(0x004AD163, hook_004AD163);

	// Z_new_towns->WriteLoHook(0x0047AAD0, hook_0047AAD0);


	// Z_new_towns->WriteLoHook(0x0054FFFA, hook_0054FFFA);
	// Z_new_towns->WriteLoHook(0x005507DF, hook_005507DF);
	// Z_new_towns->WriteLoHook(0x005508BF, hook_005508BF);
	//Z_new_towns->WriteLoHook(0x005F45F1, hook_005F45F1);

	///// Z_new_towns->WriteLoHook(0x00550F1D, hook_00550F1D);
	///// Z_new_towns->WriteLoHook(0x004C6982, hook_004C6982);
	///// Z_new_towns->WriteLoHook(0x005F3FB5, hook_005F3FB5);
	//Z_new_towns->WriteLoHook(0x00550F11, hook_00550F11);
	///// Z_new_towns->WriteLoHook(0x004C6976, hook_004C6976);
	//Z_new_towns->WriteLoHook(0x00550F8D, hook_00550F8D);
	//Z_new_towns->WriteLoHook(0x0055101C, hook_0055101C);


	// Z_new_towns->WriteLoHook(0x005BF3A6, hook_005BF3A6);

	Z_new_towns->WriteLoHook(0x005C802B, hook_005C802B);
	Z_new_towns->WriteLoHook(0x0047ABA7, hook_0047ABA7);

	Z_new_towns->WriteLoHook(0x00460F32, hook_00460F32);
	Z_new_towns->WriteLoHook(0x00460F4A, hook_00460F4A);

	Z_new_towns->WriteLoHook(0x005D4059, hook_005D4059);

	Z_new_towns->WriteLoHook(0x00460DE9, hook_00460DE9);

	// Please do not hook these, there is no reason for this - Trebuchet
	// Z_new_towns->WriteLoHook(0x0054FFC3, hook_0054FFC3);
	// Z_new_towns->WriteLoHook(0x005507D3, hook_005507D3);
	// Z_new_towns->WriteLoHook(0x005F45EE, hook_005F45EE);

	// Z_new_towns->WriteLoHook(0x00460EA7, hook_00460EA7);
	Z_new_towns->WriteLoHook(0x0046102B, hook_0046102B);

	Z_new_towns->WriteLoHook(0x005D86CC, hook_005D86CC);
	//	Z_new_towns->WriteLoHook(0x00461360, hook_00461360);   // badfix1 currentTown requirement for default buildings
	Z_new_towns->WriteLoHook(0x00461C8B, hook_00461C8B);   // fix2 currentTown requirement for default buildings



	memset(Azylum_Heroes, -1, sizeof(Azylum_Heroes));
	memset(Azylum_Owners, -1, sizeof(Azylum_Owners));

	Z_new_towns->WriteDword(0x00532F9E + 3, (int)RMG_TownNativeLand);
	Z_new_towns->WriteLoHook(0x004CA98A, town_names_004CA98A);
	Z_new_towns->WriteHiHook(0x00544F40, SPLICE_, EXTENDED_, THISCALL_, RMG_ToPlaceTowns_00544F40_hook);
	Z_new_towns->WriteHiHook(0x005BFA00, SPLICE_, EXTENDED_, THISCALL_, income_hook_005BFA00);
}