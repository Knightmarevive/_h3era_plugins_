#include "patcher_x86_commented.hpp"

#pragma warning(disable : 4996)
#include "../../__include__/H3API/single_header/H3API.hpp"

#define o_ActivePlayerID *(int*)0x69CCF4

#include <windows.h>
#include "Era.h"
  
using namespace Era;
  
const int ADV_MAP   = 37;
const int CTRL_LMB  = 4;
const int LMB_PUSH  = 12;

#define PINSTANCE_MAIN "Z_new_towns"

#include<sstream>

int swapper[27][128];
int upper[27][128];

Patcher* globalPatcher;
PatcherInstance* Z_new_towns;
#include "_Town_.h"
#include "Base.h"

extern int recruit_eighths(h3::H3Msg* arg);

extern void patch_MonstersInTowns(void);
extern void patch_BuildingsInTowns(void);
extern void patch_SiegeInTowns_late(void);
extern void patch_AdventureTowns(void);
extern void patch_TownsText_late(void);
extern void patch_TownsNumTables_late(void);
extern void patch_BuildingsInTowns_late(void);

extern void hook_HD_Mod(void);

extern void FixTownConfig(int target);

extern void hook_towns(void);
extern long long additional_buildings_per_town[48];
extern long multiple_builds_in_town[48];

extern learn_fun_T teach;

extern void fill_towns(void);

int (*get_town_bonus_build_count_FROM_SS)(h3::H3Town* town) = nullptr;
int (*get_town_bonus_build_count_FROM_Difficulty)(h3::H3Town* town) = nullptr;
long buildings_daily = 1;


extern int bonus_gold_per_town[48];
extern int income_upgrade_in_town[48];

/*
void __stdcall Refresh_Additional_Buildings_Daily(TEvent* Event) {
    auto main = h3::H3Main::Get();
    // auto player_id = main->GetPlayerID();
    auto player_id = o_ActivePlayerID;
    auto& towns = main->towns;
    for (auto& i : towns) {
        if (i.owner == player_id) {
            multiple_builds_in_town[i.number] = buildings_daily;
            if (get_town_bonus_build_count_FROM_SS) {
                multiple_builds_in_town[i.number]
                    += get_town_bonus_build_count_FROM_SS(&i);
            }
            if (get_town_bonus_build_count_FROM_Difficulty) {
                multiple_builds_in_town[i.number]
                    += get_town_bonus_build_count_FROM_Difficulty(&i);
            }
        }
    }
}
*/

_LHF_(hook_004C7D7E) {
    auto town = (h3::H3Town*)(c->ecx + c->esi);
    multiple_builds_in_town[town->number] = buildings_daily;
    if (get_town_bonus_build_count_FROM_SS) {
        multiple_builds_in_town[town->number]
            += get_town_bonus_build_count_FROM_SS(town);
    }
    if (get_town_bonus_build_count_FROM_Difficulty) {
        multiple_builds_in_town[town->number]
            += get_town_bonus_build_count_FROM_Difficulty(town);
    }
    return EXEC_DEFAULT;
}

// extern long mon_downgraded_in_town_buf;

/*
void __stdcall OnAdventureMapLeftMouseClick (TEvent* Event)
{
  ExecErmCmd("CM:I?y1 F?y2 S?y3;");
  if ((y[1] == ADV_MAP) && (y[2] == CTRL_LMB) && (y[3] == LMB_PUSH))
  {
    ExecErmCmd("CM:R0 P?y1/?y2/?y3;");
    ExecErmCmd("UN:Ey1/y2/y3;");
    if (f[1])
    {
      ExecErmCmd("UN:Oy1/y2/y3/1;");
      ExecErmCmd("IF:L^{~red}Object was deleted!{~}^;");
    }
  }
}
*/

/*
BOOL __stdcall Hook_BattleMouseHint (THookContext* Context)
{
  ExecErmCmd("IF:L^{~gold}This is a battle hint!{~}^;");
  return EXEC_DEF_CODE;
}
*/

void patch_spell_chance_to_get();
bool preset_hookForBuildDependencies();
void LoadTownConfig(int target);
void apply_TownDefender();
void __stdcall LateLoad(TEvent* Event)
{
    fill_towns();
    // for (int t = 0; t < 27; ++t) LoadTownConfig(t);

    patch_TownsText_late();
    patch_TownsNumTables_late();
    patch_BuildingsInTowns_late();
    patch_SiegeInTowns_late();
    preset_hookForBuildDependencies();

    for (int t = 0; t < 27; ++t) 
        for (int i = 0; i < 128; ++i) {
            swapper[t][i] = i;
            upper[t][i] = -1;
        }

    for (int t = 0; t < 27; ++t) LoadTownConfig(t);

    apply_TownDefender();

    auto more_SS_levels = LoadLibraryA("more_SS_levels.era");
    if (more_SS_levels) {
        get_town_bonus_build_count_FROM_SS =
            (int(__cdecl*)(h3::H3Town*))
            GetProcAddress(more_SS_levels, "get_town_bonus_build_count");
        teach = (learn_fun_T)GetProcAddress(more_SS_levels, "_set_hero_SS_");
    }
    auto Skirmish = LoadLibraryA("Skirmish.era");
    if (Skirmish) {
        get_town_bonus_build_count_FROM_Difficulty =
            (int(__cdecl*)(h3::H3Town*))
            GetProcAddress(Skirmish, "get_town_bonus_build_count");
    }

    auto GM_Magic = LoadLibraryA("Grandmaster_Magic.era");
    if(!GM_Magic) patch_spell_chance_to_get();

    hook_HD_Mod();
}

/*
void __stdcall patch_BuildingsInTowns_late(TEvent* Event) {
    patch_BuildingsInTowns_late();
}
*/

void __stdcall OnOpenTownScreen(TEvent* e)
{
    // mon_downgraded_in_town_buf = 139;
    for (int t = 0; t < 27; ++t)
        FixTownConfig(t);
}


extern long last_town_ID;
extern long last_town_click;

extern h3::H3VisitedTownsBitset GrandTeacher[1024];
extern INT16 grail_creature_amount[48];
extern INT16 eighth_creature_amount[48];
extern long Azylum_Heroes[48];
extern char Azylum_Owners[48];
void __stdcall StoreData(TEvent* e)
{
    WriteSavegameSection(sizeof(income_upgrade_in_town), (void*)&income_upgrade_in_town, "income_upgrade_in_town");
    WriteSavegameSection(sizeof(bonus_gold_per_town), (void*)&bonus_gold_per_town, "bonus_gold_per_town");

    WriteSavegameSection(sizeof(additional_buildings_per_town), (void*)&additional_buildings_per_town, "additional_buildings_per_town");
    WriteSavegameSection(sizeof(multiple_builds_in_town), (void*)&multiple_builds_in_town, "multiple_builds_in_town");
    WriteSavegameSection(sizeof(buildings_daily), (void*)&buildings_daily, "buildings_daily");
    WriteSavegameSection(sizeof(Azylum_Heroes), (void*)&Azylum_Heroes, "Azylum_Heroes");
    WriteSavegameSection(sizeof(Azylum_Owners), (void*)&Azylum_Owners, "Azylum_Owners");

    WriteSavegameSection(sizeof(grail_creature_amount), (void*)&grail_creature_amount, "grail_creature_amount");
    WriteSavegameSection(sizeof(eighth_creature_amount), (void*)&eighth_creature_amount, "eighth_creature_amount");
    WriteSavegameSection(sizeof(GrandTeacher), (void*)&GrandTeacher, "Town_GrandTeacher");
}


void __stdcall RestoreData(TEvent* e)
{
    last_town_ID = last_town_click = -2;

    ReadSavegameSection(sizeof(income_upgrade_in_town), (void*)&income_upgrade_in_town, "income_upgrade_in_town");
    ReadSavegameSection(sizeof(bonus_gold_per_town), (void*)&bonus_gold_per_town, "bonus_gold_per_town");

    ReadSavegameSection(sizeof(additional_buildings_per_town), (void*)&additional_buildings_per_town, "additional_buildings_per_town");
    ReadSavegameSection(sizeof(multiple_builds_in_town), (void*)&multiple_builds_in_town, "multiple_builds_in_town");
    ReadSavegameSection(sizeof(buildings_daily), (void*)&buildings_daily, "buildings_daily");
    ReadSavegameSection(sizeof(Azylum_Heroes), (void*)&Azylum_Heroes, "Azylum_Heroes");
    ReadSavegameSection(sizeof(Azylum_Owners), (void*)&Azylum_Owners, "Azylum_Owners");

    ReadSavegameSection(sizeof(grail_creature_amount), (void*)&grail_creature_amount, "grail_creature_amount");
    ReadSavegameSection(sizeof(eighth_creature_amount), (void*)&eighth_creature_amount, "eighth_creature_amount");
    ReadSavegameSection(sizeof(GrandTeacher), (void*)&GrandTeacher, "Town_GrandTeacher");

}

void __stdcall OnAfterErmInstructions(TEvent* e)
{
    // memset(&additional_buildings_per_town, 0, sizeof(additional_buildings_per_town));
    // memset(grail_creature_amount, 0, sizeof(grail_creature_amount));
    for (int i = 0; i < 48; ++i) {
        multiple_builds_in_town[i] = buildings_daily;
        grail_creature_amount[i] = 0;

        income_upgrade_in_town[i] = 0;
        bonus_gold_per_town[i] = 0;
        
    }
}

extern "C" __declspec(dllexport) void apply_all_town_fixes_late();
void __stdcall Apply_Fixes_Late(TEvent* e) {
    apply_all_town_fixes_late();
}

// extern long mon_downgraded_in_town_buf;
// extern long mon_upgraded_in_town_buf;
long RD_Slot = -1; long RD_Creature = 139; long RD_Base = 139;
long last_town_click = -1; long last_town_ID = 0; 
// long clickedDwelling = -1;

bool town_apply_building_click(h3::H3TownManager* _this_, INT32 building_ID);

void __stdcall OnTownMouseClick(TEvent* e) {
    // ExecErmCmd("CM:I?y11");
    // last_town_click = y[11];
    // ExecErmCmd("IF:M^BOO^");

    return;
}

#define o_TownMgr h3::H3TownManager::Get()

extern long MonstersInTowns_27x2x7[27 * 2 * 7];
extern long ThirdUpgradesInTowns_27x2x7[27 * 2 * 7];
extern char new_TownHorde1MonLevel[27 * 2];
extern char new_TownHorde2MonLevel[27 * 2];

// Rewritten by Trebuchet
void __stdcall OnOpenRecruitDlg(TEvent* e) {
    // TEventParams* X_vars = Era::GetArgXVars();
    // ExecErmCmd("IF:L^OnOpenRecruitDlg %X1 %X2 %X3 ^");
    // SaveEventParams();
    // int tmp[10]; for (int i = 1; i < 10; ++i) tmp[i] = v[i];

    /*
    std::stringstream zzz = std::stringstream();
    zzz << "IF:L^ OnOpenRecruitDlg";
    for (int i = 0; i < 16; ++i) { zzz <<  ((*X_vars)[i])<< " "; };
    zzz << " ^"; ExecErmCmd(zzz.str().c_str());
    */
    // RD_Slot = -1;
    // ExecErmCmd("RD:I?y3/?y4/?y5/?y6");
    // int town = y[4]; int dwelling = y[5];

    /*
    if (last_town_click == 26) {
        last_town_click = -1;
        return;
    }
    */
    // int town = last_town_ID; int dwelling = -1;


    /*
    if (last_town_click >= 30 && last_town_click < 44)
       dwelling = last_town_click - 30;
    else 
    */

    /*
    if (town >= 0) {
        ExecErmCmd("RD:I?y3/?y4/?y5/?y6");
        dwelling = y[5];
    }
    */

    /*
    ExecErmCmd("RD:I?y3/?y4/?y5/?y6");
    dwelling = y[5];
    */

    /*
    if (dwelling == -1)  // needed for showing 3rd/4th upgrade creature when clicking on small portraits on Town screen
        if (last_town_click >= 30 && last_town_click < 44)
           dwelling = last_town_click - 30;
    */

    // int TownType = -1;
    // if (town >= 0 /*  && y[4] >= 0 */) {
        // ExecErmCmd("CA0/y4:T?y7");
        // TownType = h3::H3Main::Get()->towns[town].type ; // y[7];

        // RD_Base = MonstersInTowns_27x2x7[TownType * 14 + dwelling % 7];
        
    /*
        if (last_town_click == 19)
            dwelling = new_TownHorde1MonLevel[TownType * 2] + 7;

        if (last_town_click == 25)
            dwelling = new_TownHorde2MonLevel[TownType * 2] + 7;      
    }
    */

    /*
    if (town >= 0 && dwelling >=7 && dwelling < 14 && TownType>=0 && TownType<27) {
        RD_Slot = 1;

        ExecErmCmd("RD:C1/?t/?y2");
        int offset = dwelling - 7; 
        //ExecErmCmd("CA0/y4:T?y7");
        //int TownType = y[7];

        RD_Creature = MonstersInTowns_27x2x7 [TownType * 14 + dwelling];
        // RD_Base = MonstersInTowns_27x2x7[TownType * 14 + dwelling - 7];

        if (additional_buildings_per_town[town] & (1LL << offset)) {
            int creature = ThirdUpgradesInTowns_27x2x7[TownType * 14 + offset];
            int source = 14 * town + offset;
            y[8] = creature; y[9] = source;
            if (creature >= 0) {
                ExecErmCmd("RD:S1");
                ExecErmCmd("RD:C0/y8/y2/y9");
                RD_Slot = 2; RD_Creature = creature;
                //mon_upgraded_in_town_buf = creature;
                //mon_downgraded_in_town_buf = creature;
            }
        }
        if (additional_buildings_per_town[town] & (1LL << (offset + 7))) {
            int creature = ThirdUpgradesInTowns_27x2x7[TownType * 14 + offset + 7];
            int source = 14 * town + offset;
            y[8] = creature; y[9] = source;
            if (creature >= 0) {
                ExecErmCmd("RD:S1");
                ExecErmCmd("RD:C0/y8/y2/y9");
                RD_Slot = 3; RD_Creature = creature;
                //mon_upgraded_in_town_buf = creature;
                //mon_downgraded_in_town_buf = creature;
            }
        }
    }
    */
    // for (int i = 1; i < 10; ++i) v[i] = tmp[i];
    h3::H3Town* town = o_TownMgr->town;
    int townType = o_TownMgr->townType;

    if (!town || (town->number < 0 || town->number >= 48) || (townType < 0 || townType >= 27))
        return;

    ExecErmCmd("RD:I?y3/?y4/?y5/?y6");
    int dwelling = y[5];

    long horde1_level = new_TownHorde1MonLevel[townType * 2];
    long horde2_level = new_TownHorde2MonLevel[townType * 2];

    if (last_town_click == 18 || last_town_click == 19)
        dwelling = horde1_level;
    else if (last_town_click == 24 || last_town_click == 25)
        dwelling = horde2_level;
    /*
    else if (last_town_click >= 164 && last_town_click <= 170)
        dwelling = last_town_click - 164;
        */
    /*
    else if (last_town_click == 171) {
        // TODO:
        recruit_eighths(o_TownMgr);
    }
    */


    if (last_town_click >= 164 && last_town_click <= 170) {
        ExecErmCmd("RD:C0/?y20/?t/?t/0");
        for (int i = 0; i < 14; ++i) {
            if (y[20] == MonstersInTowns_27x2x7[townType * 14 + i]) {
                dwelling = i; break;
            }
        }
    }

    int offset = (dwelling > 6) ? (dwelling - 7) : dwelling;

    /*
    if (last_town_click >= 164 && last_town_click <= 170) {
        int ret = 0; int k = 0;
        for (int j = 0; j <= offset; ++j)
            if (k && town->IsBuildingBuilt(29 + k))
            {
                ++k;
            }
            else { 
                ++k; ++ret; --j;
            }

        offset += ret;
    }
    */

    for (int i = 0; i < 7; ++i) {
        if (i != offset) continue;

        int baseIdx = townType * 14 + i;
        int upgradeIdx = townType * 14 + 7 + i;

        long tier1 = MonstersInTowns_27x2x7[baseIdx];
        long tier2 = MonstersInTowns_27x2x7[upgradeIdx];
        long tier3 = ThirdUpgradesInTowns_27x2x7[baseIdx];
        long tier4 = ThirdUpgradesInTowns_27x2x7[upgradeIdx];

        y[7] = tier1; y[8] = tier2; y[9] = tier3; y[10] = tier4;

        if (town->IsBuildingBuilt(71 + i)) {
            ExecErmCmd("RD:C0/?y11/?y12/?y13/0");

            ExecErmCmd("RD:C0/y10/?t/?t/0");
            ExecErmCmd("RD:C1/y9/?t/?t/0");
            ExecErmCmd("RD:C2/y8/?t/y13/0");
            ExecErmCmd("RD:C3/y7/?t/y13/0");
            break;
        }
        else if (town->IsBuildingBuilt(64 + i)) {
            ExecErmCmd("RD:C0/?y11/?y12/?y13/0");

            ExecErmCmd("RD:C0/y9/?t/?t/0");
            ExecErmCmd("RD:C1/y8/?t/?t/0");
            ExecErmCmd("RD:C2/y7/?t/y13/0");
            break;
        }
        else if (town->IsBuildingBuilt(37 + i)) {
            ExecErmCmd("RD:C0/y8/?t/?t/0");
            ExecErmCmd("RD:C1/y7/?t/?t/0");
            break;
        }
        else if (town->IsBuildingBuilt(30 + i)) {
            ExecErmCmd("RD:C0/y7/?t/?t/0");
            ExecErmCmd("RD:C1/-1/?t/?t/0");
            break;
        }
    }
}

void __stdcall OnOpenRecruitDlg_BAK(TEvent* e) {
    TEventParams* X_vars = Era::GetArgXVars();
    //ExecErmCmd("IF:L^OnOpenRecruitDlg %X1 %X2 %X3 ^");
    //SaveEventParams();
    //int tmp[10]; for (int i = 1; i < 10; ++i) tmp[i] = v[i];

    /*
    std::stringstream zzz = std::stringstream();
    zzz << "IF:L^ OnOpenRecruitDlg";
    for (int i = 0; i < 16; ++i) { zzz <<  ((*X_vars)[i])<< " "; };
    zzz << " ^"; ExecErmCmd(zzz.str().c_str());
    */
    RD_Slot = -1;
    // ExecErmCmd("RD:I?y3/?y4/?y5/?y6");
    // int town = y[4]; int dwelling = y[5];

    /*
    if (last_town_click == 26) {
        last_town_click = -1;
        return;
    }
    */
    int town = last_town_ID; int dwelling = -1;


    /*
    if (last_town_click >= 30 && last_town_click < 44)
       dwelling = last_town_click - 30;
    else
    */

    if (town >= 0) {
        ExecErmCmd("RD:I?y3/?y4/?y5/?y6");
        dwelling = y[5];
    }

    /*
    ExecErmCmd("RD:I?y3/?y4/?y5/?y6");
    dwelling = y[5];
    */

    if (dwelling == -1)  // needed for showing 3rd/4th upgrade creature when clicking on small portraits on Town screen
        if (last_town_click >= 30 && last_town_click < 44)
            dwelling = last_town_click - 30;


    int TownType = -1;
    if (town >= 0 /*  && y[4] >= 0 */) {
        // ExecErmCmd("CA0/y4:T?y7");
        TownType = h3::H3Main::Get()->towns[town].type; // y[7];

        RD_Base = MonstersInTowns_27x2x7[TownType * 14 + dwelling % 7];

        /*        if (last_town_click == 19)
                    dwelling = new_TownHorde1MonLevel[TownType * 2] + 7;

                if (last_town_click == 25)
                    dwelling = new_TownHorde2MonLevel[TownType * 2] + 7;
                    */
    }

    if (town >= 0 && dwelling >= 7 && dwelling < 14 && TownType >= 0 && TownType < 27) {
        RD_Slot = 1;

        ExecErmCmd("RD:C1/?t/?y2");
        int offset = dwelling - 7;
        //ExecErmCmd("CA0/y4:T?y7");
        //int TownType = y[7];

        RD_Creature = MonstersInTowns_27x2x7[TownType * 14 + dwelling];
        // RD_Base = MonstersInTowns_27x2x7[TownType * 14 + dwelling - 7];

        if (additional_buildings_per_town[town] & (1LL << offset)) {
            int creature = ThirdUpgradesInTowns_27x2x7[TownType * 14 + offset];
            int source = 14 * town + offset;
            y[8] = creature; y[9] = source;
            if (creature >= 0) {
                ExecErmCmd("RD:S1");
                ExecErmCmd("RD:C0/y8/y2/y9");
                RD_Slot = 2; RD_Creature = creature;
                //mon_upgraded_in_town_buf = creature;
                //mon_downgraded_in_town_buf = creature;
            }
        }
        if (additional_buildings_per_town[town] & (1LL << (offset + 7))) {
            int creature = ThirdUpgradesInTowns_27x2x7[TownType * 14 + offset + 7];
            int source = 14 * town + offset;
            y[8] = creature; y[9] = source;
            if (creature >= 0) {
                ExecErmCmd("RD:S1");
                ExecErmCmd("RD:C0/y8/y2/y9");
                RD_Slot = 3; RD_Creature = creature;
                //mon_upgraded_in_town_buf = creature;
                //mon_downgraded_in_town_buf = creature;
            }
        }
    }

    //for (int i = 1; i < 10; ++i) v[i] = tmp[i];
}

void __stdcall  OnCloseRecruitDlg(TEvent* e) {

}

void __stdcall  OnRecruitDlgRecalc(TEvent* e) {

}

// Is not needed - Trebuchet
void __stdcall  OnRecruitDlgAction(TEvent* e) {
    // TEventParams* X_vars = Era::GetArgXVars();
    // int tmp[10]; for (int i = 1; i < 10; ++i) tmp[i] = v[i];

    /*
    int min = MAXINT32 >> 1;
    for (int i = 0; i <= RD_Slot; ++i) {
        y[4] = i; ExecErmCmd("RD:Cy4/?t/?y2");
        min = y[2] < min ? y[2] : min;
    }

    for (int i = 0; i <= RD_Slot; ++i) {
        y[4] = i; y[2] = min;
        ExecErmCmd("RD:Cy4/?t/y2");
    }
    */

    // for (int i = 1; i < 10; ++i) v[i] = tmp[i];

    // RD_Slot = -1;
}

void __stdcall  OnRecruitDlgAction_BAK(TEvent* e) {
    TEventParams* X_vars = Era::GetArgXVars();
    //int tmp[10]; for (int i = 1; i < 10; ++i) tmp[i] = v[i];

    int min = MAXINT32 >> 1;
    for (int i = 0; i <= RD_Slot; ++i) {
        y[4] = i; ExecErmCmd("RD:Cy4/?t/?y2");
        min = y[2] < min ? y[2] : min;
    }

    for (int i = 0; i <= RD_Slot; ++i) {
        y[4] = i; y[2] = min;
        ExecErmCmd("RD:Cy4/?t/y2");
    }

    //for (int i = 1; i < 10; ++i) v[i] = tmp[i];

    RD_Slot = -1;
}

void __stdcall RefreshAzylum();
void __stdcall RefreshAzylum(TEvent* e) {
    RefreshAzylum();
}


extern void recruit_in_grail__income();
void __stdcall  recruit_in_grail__income_event(TEvent* e) {
    auto main = h3::H3Main::Get();
    auto &date = main->date;
    if (date.day != 1) return;

    auto &info = main->playersInfo;

    int firstplayer = 0;
    for (auto& plr : info.playerType)
        if (plr >= 0) break;
        else ++firstplayer;

    if (firstplayer != o_ActivePlayerID)
        return;

    recruit_in_grail__income();
}

void __stdcall OnAdventureMapMouseClick_event(TEvent* e) {
    // last_town_ID = -1;
}

void RemapBuildings_init();

void LoadGlobalConfig(void);
int __stdcall Y_AutoGradeMonInTown(LoHook* h, HookContext* c);
// signed int __stdcall town_TownDlg_Proc_hook(HiHook* h, h3::H3TownManager* _this_, h3::H3Msg* msg);
// void LoadTownConfig(int target);
void apply_TownDefender(void);


inline signed int __cdecl ApplyERMArgument(int a1, char a2, int a3, char a4) {
    return CALL_4(signed int, __cdecl, 0x0074195D, a1, a2, a3, a4);
}

inline void __cdecl ErrorMessage(int a1, int a2, int a3) {
    return CALL_3(void, __cdecl, 0x00712333, a1, a2, a3);
}


#define ret0 c->return_address = 0x0077520C; return NO_EXEC_DEFAULT;
UINT32 spell_chance_to_get[256][27];
#define o_Spell (*(h3::H3Spell**)0x687FA8)
_LHF_(ERM_00775249) {
    char letter = c->ecx + 'A';
    if (letter == 'H') {
        int a2 = *(int*)(c->ebp + 0x0C);
        int a3 = *(int*)(c->ebp + 0x10);
        int a4 = *(int*)(c->ebp + 0x14);

        int& v6 = *(int*)(c->ebp - 0x0C); // [esp+14h] [ebp-Ch]
        int& v7 = *(int*)(c->ebp - 0x08); // [esp+18h] [ebp-8h]
        int& v8 = *(int*)(c->ebp - 0x04); // [esp+1Ch] [ebp-4h]


        if (a2 < 2)
        {
            ErrorMessage(21, 92, (int)"\"SS:H\"- wrong syntax.");
            ret0;
        }
        ApplyERMArgument((int)&v6, 4, a4, 0);
        if (v6 < 0 || v6 > 27)
        {
            ErrorMessage(21, 94, (int)"\"SS:H\"- wrong index.");
            ret0;
        }
        // ApplyERMArgument((int)&v5[v6 + 17], 4, a4, 1);
        ApplyERMArgument((int)& spell_chance_to_get[v8][v6], 4, a4, 1);

        c->return_address = 0x77569D;
        return NO_EXEC_DEFAULT;
    }

    return EXEC_DEFAULT;
}


_LHF_(hook_005BEAF1) {
    // c->edx = Z_Spells[c->ebx / 0x88].chance_to_get[*(char*)((*(int*)(c->ebp - 4)) + 4)];
    h3::H3Town* town = (h3::H3Town*)(*(int*)(c->ebp - 4));
    //int spell_ID = (*(int*)(c->ebp - 14) / 34);
    int spell_ID = c->esi;
    // c->edx = Z_Spells[spell_ID].chance_to_get[town->type];
    c->edx = spell_chance_to_get[spell_ID][town->type];

    return EXEC_DEFAULT;
}
_LHF_(hook_005BEBEE) {
    //c->ebx -= Z_Spells[c->edi / 0x88].chance_to_get[*(char*)((*(int*)(c->ebp - 4))+4)];
    h3::H3Town* town = (h3::H3Town*)(*(int*)(c->ebp - 4));
    int spell_ID = c->esi;
    // c->ebx -= Z_Spells[spell_ID].chance_to_get[town->type];
    c->ebx -= spell_chance_to_get[spell_ID][town->type];

    if (c->ebx <= 0)
        c->return_address = 0x005BEC0E;
    else c->return_address = 0x005BEBF6;
    return NO_EXEC_DEFAULT;
}

void patch_spell_chance_to_get()
{
    // SS:H format T
    // Z_new_towns->WriteByte(0x00775511 + 3, 27);
    Z_new_towns->WriteLoHook(0x00775249, ERM_00775249);

    for (int j = 0; j < 27; ++j) for (int i = 0; i < 70; ++i)
        spell_chance_to_get[i][j] = o_Spell[i].chanceToGet[j % 9];

    Z_new_towns->WriteLoHook(0x005BEAF1, hook_005BEAF1);
    Z_new_towns->WriteLoHook(0x005BEBEE, hook_005BEBEE);
}

extern "C" __declspec(dllexport) BOOL APIENTRY DllMain (HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
  if (reason == DLL_PROCESS_ATTACH)
  {

      globalPatcher = GetPatcher();
      Z_new_towns = globalPatcher->CreateInstance(PINSTANCE_MAIN);

      LoadGlobalConfig();
      // for (int t = 0; t < 27; ++t) LoadTownConfig(t);

      patch_MonstersInTowns();
      patch_BuildingsInTowns();
      // patch_SiegeInTowns();
      patch_AdventureTowns();

      RemapBuildings_init();
      hook_towns();
      // patch_spell_chance_to_get();

      // apply_TownDefender();
      Z_new_towns->WriteLoHook(0x005D4465, Y_AutoGradeMonInTown);

      // Z_new_towns->WriteHiHook(0x005D3640, SPLICE_, EXTENDED_, THISCALL_, town_TownDlg_Proc_hook);

      // for (int t = 0; t < 27; ++t) LoadTownConfig(t);

    ConnectEra();
    // RegisterHandler(OnAdventureMapLeftMouseClick, "OnAdventureMapLeftMouseClick");
    // ApiHook((void*) Hook_BattleMouseHint, HOOKTYPE_BRIDGE, (void*) 0x74fd1e);

    RegisterHandler(LateLoad, "OnAfterCreateWindow");
    // RegisterHandler(patch_BuildingsInTowns_late, "OnOpenTownScreen");

    RegisterHandler(OnOpenTownScreen, "OnOpenTownScreen");

    RegisterHandler(StoreData, "OnSavegameWrite");
    RegisterHandler(RestoreData, "OnSavegameRead");

    RegisterHandler(Apply_Fixes_Late, "OnAfterLoadGame");
    RegisterHandler(Apply_Fixes_Late, "OnErmTimer 19");

    RegisterHandler(OnOpenRecruitDlg, "OnOpenRecruitDlg");
    RegisterHandler(OnCloseRecruitDlg, "OnCloseRecruitDlg");
    RegisterHandler(OnRecruitDlgRecalc, "OnRecruitDlgRecalc");
    RegisterHandler(OnRecruitDlgAction, "OnRecruitDlgAction");

    RegisterHandler(OnAfterErmInstructions, "OnAfterErmInstructions");

    RegisterHandler(OnTownMouseClick, "OnTownMouseClick");
    RegisterHandler(OnAdventureMapMouseClick_event, "OnAdventureMapLeftMouseClick");

    RegisterHandler(RefreshAzylum, "OnEveryDay");

    // RegisterHandler(Refresh_Additional_Buildings_Daily, "OnEveryDay");
    Z_new_towns->WriteLoHook(0x004C7D7E, hook_004C7D7E);

    RegisterHandler(recruit_in_grail__income_event, "OnEveryDay");
  }
  return TRUE;
};
