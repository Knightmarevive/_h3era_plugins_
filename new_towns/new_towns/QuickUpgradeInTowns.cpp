#include"patcher_x86_commented.hpp"
#define o_ActivePlayerID *(int*)0x69CCF4
#include"lib/H3API.hpp"


#include"_Town_.h"
//typedef h3::H3Town _Town_;
typedef h3::H3Player _Player_;
typedef h3::H3Army _Army_;
typedef h3::H3Main _GameMgr_;
typedef h3::H3Hero _Hero_;
typedef h3::H3CreatureInformation _CreatureInfo_;
typedef h3::H3Resources _Resources_;

typedef int _int_; typedef char _bool8_;
#define CID_SKELETON   56

extern long MonstersInTowns_27x2x7[27 * 2 * 7];
extern long ThirdUpgradesInTowns_27x2x7[27 * 2 * 7];
extern long EighthMonType[27][4];

#define o_CrExpoSet_GetExp(type, crloc) CALL_2(int, __cdecl, 0x718CCD, type, crloc)
#define o_CrExpoSet_FindType(army, slot_ix, p_type, p_crloc) CALL_4(void, __cdecl, 0x71A1B7, army, slot_ix, p_type, p_crloc)
#define o_CrExpoSet_SetN(type, crloc, cr_type, cr_count, exp) CALL_5(void, __cdecl, 0x718AD0, type, crloc, cr_type, cr_count, exp)

inline void n_CrExpoSet_Set(int type, _ptr_ crloc, int cr_type, int cr_count, int exp)
{
	int crexpo_i = CALL_2(int, __cdecl, 0x718579, type, crloc); // CrExpoSet::FindIt
	if (crexpo_i != -1)
	{
		_ptr_ crexpo = DwordAt(0x718B32 + 2) + 16 * crexpo_i;
		IntAt(crexpo + 0) = exp;
		IntAt(crexpo + 4) = cr_count;
		DwordAt(crexpo + 8) = ((((_dword_)(_byte_)cr_type) << 5) & 0x1FE0) | (DwordAt(crexpo + 8) & 0xFFFFE01F);
		CALL_1(void, __thiscall, 0x7176A1, 0x860550 + 16 * crexpo_i); // CrExpo::Check4Max
	}
	else
		o_CrExpoSet_SetN(type, crloc, cr_type, cr_count, exp);
}

inline int CrExpoSet_GetExp(_Army_* army, int i)
{
	int type_s;
	_ptr_ crloc_s;
	o_CrExpoSet_FindType(army, i, &type_s, &crloc_s);
	return army->type[i] < 0 ? 0 : o_CrExpoSet_GetExp(type_s, crloc_s);
}

inline void CrExpoSet_SetExp(_Army_* army, int i, int exp)
{
	int type_s;
	_ptr_ crloc_s;
	o_CrExpoSet_FindType(army, i, &type_s, &crloc_s);
	n_CrExpoSet_Set(type_s, crloc_s, army->type[i], army->count[i], exp);
}

_bool8_ UpgradeArmyStack(_Army_* army, _Town_* town, _Player_* me, _int_ index, _int_ isUpgradeSkeleton)
{
	_bool8_ result = false;

	_int_ type = army->type[index];
	_int_ count = army->count[index];

	if (count < 1)
		return result;

	if (!isUpgradeSkeleton && type == CID_SKELETON)
		return result;

	_CreatureInfo_* mon = &h3::P_Creatures(type);
	char TT = town->type; char MA = -1; char MS = -1;
	for (int i = 0; i < 7; ++i) if (MonstersInTowns_27x2x7[14 * TT + i + 0] == type) { MA = 0; MS = i; }
	for (int i = 0; i < 7; ++i) if (MonstersInTowns_27x2x7[14 * TT + i + 7] == type) { MA = 1; MS = i; }
	for (int i = 0; i < 7; ++i) if (ThirdUpgradesInTowns_27x2x7[14 * TT + i + 0] == type) { MA = 2; MS = i; }
	for (int i = 0; i < 3; ++i) if (EighthMonType[TT][i] == type) { MA = 9; MS = i; };

	int upgType = -1;

	if (MA < 0) return result;
	else if (MA == 0 && town->IsBuildingBuilt(37 + MS)) upgType = MonstersInTowns_27x2x7[14 * TT + MS + 7];
	else if (MA == 1 && town->IsBuildingBuilt(64 + MS)) upgType = ThirdUpgradesInTowns_27x2x7[14 * TT + MS + 0];
	else if (MA == 2 && town->IsBuildingBuilt(71 + MS)) upgType = ThirdUpgradesInTowns_27x2x7[14 * TT + MS + 7];
	else if (MA == 9 && town->IsBuildingBuilt(89 + MS)) upgType = EighthMonType[TT][MS+1];

	if (upgType >= 0) {

		_CreatureInfo_* upgMon = &h3::P_Creatures(upgType);

		_Resources_ costRes;
		// mon->GetUpradeCost(type, upgType, count, &costRes);
		costRes = mon->UpgradeCost(upgMon, count);

		for (int i = 0; i < 7; ++i) costRes[i] *= count;

		_Resources_* pRes = &me->playerResources;
		if (pRes->EnoughResources(costRes))
		{
			pRes->RemoveResources(costRes);
			int expo_deGrade = CrExpoSet_GetExp(army, index);
			army->type[index] = upgType;
			if (expo_deGrade > 0)
				CrExpoSet_SetExp(army, index, (expo_deGrade * 3) >> 2 /* 75:100 */);

			result = true;
		}
	}

	return result;
}

void upgrade_units_quick(h3::H3Army* army, int index, int upgType) {
	int expo_deGrade = CrExpoSet_GetExp(army, index);
	army->type[index] = upgType;
	if (expo_deGrade > 0)
		CrExpoSet_SetExp(army, index, (expo_deGrade * 3) >> 2  );  // 75:100
}

_bool8_ TryToUpgrade(_Town_* town, _Player_* me, int clickID)
{
	_bool8_ result = false;
	_Army_* army;
	_GameMgr_* gm = P_Main; // P_GameMgr;

	if (clickID >= 115 && clickID <= 121 && town->owner == me->ownerID)
	{
		if (_Hero_* hero = gm->GetHero(town->garrisonHero))
			result = UpgradeArmyStack(&hero->army, town, me, clickID - 115, 1);
		else
			result = UpgradeArmyStack(&town->Guards, town, me, clickID - 115, 1);
	}
	else if (clickID >= 140 && clickID <= 146)
	{
		if (_Hero_* hero = gm->GetHero(town->visitingHero))
			if (hero->owner == me->ownerID)
				result = UpgradeArmyStack(&hero->army, town, me, clickID - 140, 1);
	}
	else if (clickID == 123 && town->owner == me->ownerID)
	{
		if (_Hero_* hero = gm->GetHero(town->garrisonHero))
			army = &hero->army;
		else
			army = &town->Guards;
		for (int i = 0; i < 7; i++)
			result += UpgradeArmyStack(army, town, me, i, 0);
	}
	else if (clickID == 125)
	{
		if (_Hero_* hero = gm->GetHero(town->visitingHero))
		{
			if (hero->owner== me->ownerID) {
				army = &hero->army;
				for (int i = 0; i < 7; i++)
					result += UpgradeArmyStack(army, town, me, i, 0);
			}
		}
	}

	return result;
}

_int_ __stdcall Y_AutoGradeMonInTown(LoHook* h, HookContext* c)    // new hook at 5D4465 - old hook at 0x5D45FD
{
	if (c->eax != 0)
		return EXEC_DEFAULT;
	else
    if (GetKeyState(65) < 0 || GetKeyState(97) < 0)    // Ascii for 'A' and 'a'
	{
		// _Player_* me = P_Main->GetPlayer();
		auto me = P_Main->players + o_ActivePlayerID;

		//if (me->IsActive())
		//if(me->ownerID == P_Main->)
		if(me == h3_ActivePlayer)
		{
			_Town_* town = (_Town_*) P_TownMgr->town;
			int clickID = c->edi;

			if (TryToUpgrade(town, me, clickID)) {
				// P_TownMgr->UnHighlightArmy();
				// P_TownMgr->;

				// P_TownMgr->Redraw();
				P_TownMgr->RefreshScreen();
			}
		}

		c->return_address = 0x5D460F;
		return NO_EXEC_DEFAULT;
	}
	return EXEC_DEFAULT;
}