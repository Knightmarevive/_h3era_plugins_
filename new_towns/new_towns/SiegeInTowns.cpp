#include "patcher_x86_commented.hpp"
#include <cstdio>
#pragma warning(disable : 4996)
#include"../../__include__/H3API/single_header/H3API.hpp"

extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;

char tower_ShotDef[27][16];

struct TownTower {
	long CrType2Shot;
	long Tower1X;
	long Tower1Y;
	long Tower2X;
	long Tower2Y;
	long Tower3X;
	long Tower3Y;
	char* ShotDef;
};

TownTower SiegeTowers[27];

extern "C" __declspec(dllexport) void ChangeSiegeTowerCreature(int town, int creature, char* missle) {
	SiegeTowers[town].CrType2Shot = creature;
	SiegeTowers[town].ShotDef = missle;
}

extern "C" __declspec(dllexport) void ChangeSiegeTowerPosition(int town, int tower, int x, int y) {
	switch (tower) {
	case 1:
		SiegeTowers[town].Tower1X = x;
		SiegeTowers[town].Tower1Y = y;
		break;
	case 2:
		SiegeTowers[town].Tower2X = x;
		SiegeTowers[town].Tower2Y = y;
		break;
	case 3:
		SiegeTowers[town].Tower3X = x;
		SiegeTowers[town].Tower3Y = y;
		break;
	}

}

//

long new_MoatDamageByTownType[27] = {
	70, 70,150, 90, 70, 90, 70, 90, 70,
	70, 70,150, 90, 70, 90, 70, 90, 70,
	70, 70,150, 90, 70, 90, 70, 90, 70
	// -1
};


char text_TownSiegeMoatName[27][32] = {
	"Moat", "Bramble Hedge", "Land Mines", "Lava", "Boneyard", "Boiling Oil", "Spike Barrier", "Boiling Tar", "Moat",
	"Moat", "Bramble Hedge", "Land Mines", "Lava", "Boneyard", "Boiling Oil", "Spike Barrier", "Boiling Tar", "Moat",
	"Moat", "Bramble Hedge", "Land Mines", "Lava", "Boneyard", "Boiling Oil", "Spike Barrier", "Boiling Tar", "Moat",

};

long new_TownSiegeMoatName[27 + 1];

//

char text_TownSiegeBackground[27][16] = {
	"SgCsBack.pcx", "SgRmBack.pcx","SgTwBack.pcx", "SgInBack.pcx", "SgNcBack.pcx", "SgDnBack.pcx", "SgStBack.pcx", "SgFrBack.pcx", "SgElBack.pcx",
	"SgCsBack.pcx", "SgRmBack.pcx","SgTwBack.pcx", "SgInBack.pcx", "SgNcBack.pcx", "SgDnBack.pcx", "SgStBack.pcx", "SgFrBack.pcx", "SgElBack.pcx",
	"SgCsBack.pcx", "SgRmBack.pcx","SgTwBack.pcx", "SgInBack.pcx", "SgNcBack.pcx", "SgDnBack.pcx", "SgStBack.pcx", "SgFrBack.pcx", "SgElBack.pcx"
	// "bad"
};
long new_TownSiegeBackground[27 + 1];

char new_CasBattlefieldTable[27 * 648];
char new_CasBattlefield_Text[27][648 / 4][16] = {};

extern "C" __declspec(dllexport) void ChangeSiegePictures(int town, char* marker) {
	for (auto dest : new_CasBattlefield_Text[town]) {
		int& target = *(int*) dest;
		if (!target) continue;
		if (strstr(dest, ".pcx") == nullptr) continue;
		target = *(int*) marker;
	}
}

char wallElements[32][16]
{
		"SGCVDRW3.pcx",
		"SGCVDRW2.pcx",
		"SGCVDRW1.pcx",
		"SGCVDRWC.pcx",
		"SGCVMOAT.pcx",
		"SGCVMLIP.pcx",
		"SGCVTPWL.pcx",
		"SGCVTW22.pcx",
		"SGCVTW21.pcx",
		"SGCVWA62.pcx",
		"SGCVWA61.pcx",
        "SGCVWA5.pcx",
        "SGCVWA42.pcx",
		"SGCVWA41.pcx",
		"SGCVARCH.pcx",
		"SGCVWA32.pcx",
		"SGCVWA31.pcx",
		"SGCVWA2.pcx",
		"SGCVWA12.pcx",
		"SGCVWA11.pcx",
		"SGCVTW12.pcx",
		"SGCVTW11.pcx",
		"SGCVMAN2.pcx",
		"SGCVMAN1.pcx",
		"SGCVMANC.pcx",
		"SGCVTW1C.pcx",
		"SGCVTW2C.pcx",
};
/* not used
"SGCVMNBR.pcx",
"SGCVMNOK.pcx",
"SGCVWA13.pcx",
"SGCVWA33.pcx",
"SGCVWA43.pcx",
"SGCVWA5.pcx",
"SGCVWA63.pcx",
*/

_LHF_(fixCitadelCastleBitMask_00463712)  // sometimes during a siege, only 1 Tower is present even though the town has a Castle. This is because it happens the erroneous situation in which
										 // Citadel = 1 and Castle = 1. This hook will fix this
{
	h3::H3Town* town = (h3::H3Town*)*(int*)(c->ebp + 0x18);
	if (town!=NULL && town->built.castle && town->built.citadel)
		town->built.citadel = false;

	return EXEC_DEFAULT;
}

void patch_SiegeInTowns_late(void) {

	memcpy(SiegeTowers, (void*)0x0063CF88, 9 * sizeof(TownTower));
	memcpy(&SiegeTowers[9], (void*)0x0063CF88, 9 * sizeof(TownTower));
	memcpy(&SiegeTowers[18], (void*)0x0063CF88, 9 * sizeof(TownTower));

	for (int i = 0; i < 27; ++i) {
		strcpy_s(tower_ShotDef[i], SiegeTowers[i].ShotDef);
		SiegeTowers[i].ShotDef = tower_ShotDef[i];
	}

	Z_new_towns->WriteDword(0x00466866 + 2, (long) SiegeTowers);

	//

	Z_new_towns->WriteDword(0x0042175D + 3, (long)new_MoatDamageByTownType);
	Z_new_towns->WriteDword(0x004217C0 + 3, (long)new_MoatDamageByTownType);
	Z_new_towns->WriteDword(0x00469A85 + 3, (long)new_MoatDamageByTownType);
	Z_new_towns->WriteDword(0x004B31CD + 3, (long)new_MoatDamageByTownType);
	Z_new_towns->WriteDword(0x004B3201 + 3, (long)new_MoatDamageByTownType);
	Z_new_towns->WriteDword(0x00465FDD + 2, 8 + (long)new_MoatDamageByTownType);


	for (int i = 0; i < 27; ++i) new_TownSiegeMoatName[i] = (long)text_TownSiegeMoatName[i];
	new_TownSiegeMoatName[27] = 0;
	Z_new_towns->WriteDword(0x00469AA9 + 3, (long)new_TownSiegeMoatName);
	Z_new_towns->WriteDword(0x005BA2CB + 1, (long)new_TownSiegeMoatName);

	//

	for (int i = 0; i < 27; ++i) new_TownSiegeBackground[i] = (long) text_TownSiegeBackground[i];
	new_TownSiegeBackground[27] = 0; Z_new_towns->WriteDword(0x004642C8 + 3, (long) new_TownSiegeBackground);

	memcpy(new_CasBattlefieldTable, (char*)0x66D848, 9 * 648);
	memcpy(new_CasBattlefieldTable + 9 * 648, (char*)0x66D848 , 9 * 648);
	memcpy(new_CasBattlefieldTable + 2 * 9 * 648, (char*)0x66D848 , 9 * 648);


	for (int i = 0; i < 27; ++i) {
		char* base = new_CasBattlefieldTable + i * 648; 
		int* source = (int*) base;
		for (auto target : new_CasBattlefield_Text[i]) {
			if (*source > 0x00400000) {
				strcpy(target, (char*) *source);
				*source = (int)target;
			}
			++source;
		}
	}

	/*
	int* Cove = (int*) (new_CasBattlefieldTable + (9 * 648));
	Cove[0x08 / 4] = (int)wallElements[0];
	Cove[0x0c / 4] = (int)wallElements[1];
	Cove[0x10 / 4] = (int)wallElements[2];
	Cove[0x30 / 4] = (int)wallElements[3];
	Cove[0x50 / 4] = (int)wallElements[4];
	Cove[0x74 / 4] = (int)wallElements[5];
	Cove[0x98 / 4] = (int)wallElements[6];
	Cove[0xbc / 4] = (int)wallElements[7];
	Cove[0xc0 / 4] = (int)wallElements[8];
	Cove[0xc4 / 4] = (int)wallElements[8];
	Cove[0xc8 / 4] = (int)wallElements[8];
	Cove[0xcc / 4] = (int)wallElements[8];
	Cove[0xe4 / 4] = (int)wallElements[9];
	Cove[0xe8 / 4] = (int)wallElements[10];
	Cove[0xec / 4] = (int)wallElements[10];
	Cove[0xf0 / 4] = (int)wallElements[10];
	Cove[0x104 / 4] = (int)wallElements[11];
	Cove[0x12c / 4] = (int)wallElements[12];
	Cove[0x130 / 4] = (int)wallElements[13];
	Cove[0x134 / 4] = (int)wallElements[13];
	Cove[0x138 / 4] = (int)wallElements[13];
	Cove[0x14c / 4] = (int)wallElements[14];
	Cove[0x150 / 4] = (int)wallElements[14];
	Cove[0x154 / 4] = (int)wallElements[14];
	Cove[0x158 / 4] = (int)wallElements[14];
	Cove[0x15c / 4] = (int)wallElements[14];
	Cove[0x174 / 4] = (int)wallElements[15];
	Cove[0x178 / 4] = (int)wallElements[16];
	Cove[0x17c / 4] = (int)wallElements[16];
	Cove[0x180 / 4] = (int)wallElements[16];
	Cove[0x194 / 4] = (int)wallElements[17];
	Cove[0x1bc / 4] = (int)wallElements[18];
	Cove[0x1c0 / 4] = (int)wallElements[19];
	Cove[0x1c4 / 4] = (int)wallElements[19];
	Cove[0x1c8 / 4] = (int)wallElements[19];
	Cove[0x1dc / 4] = (int)wallElements[20];
	Cove[0x1e0 / 4] = (int)wallElements[21];
	Cove[0x1e4 / 4] = (int)wallElements[21];
	Cove[0x1e8 / 4] = (int)wallElements[21];
	Cove[0x1ec / 4] = (int)wallElements[21];
	Cove[0x200 / 4] = (int)wallElements[22];
	Cove[0x204 / 4] = (int)wallElements[23];
	Cove[0x208 / 4] = (int)wallElements[23];
	Cove[0x20c / 4] = (int)wallElements[23];
	Cove[0x210 / 4] = (int)wallElements[23];
	Cove[0x228 / 4] = (int)wallElements[24];
	Cove[0x24c / 4] = (int)wallElements[25];
	Cove[0x270 / 4] = (int)wallElements[26];
	*/

	Z_new_towns->WriteDword(0x462FBA, new_CasBattlefieldTable);
	Z_new_towns->WriteDword(0x49437C, new_CasBattlefieldTable);
	Z_new_towns->WriteDword(0x4947C3, new_CasBattlefieldTable);
	Z_new_towns->WriteDword(0x494384, new_CasBattlefieldTable + 2);  //66D84A

	Z_new_towns->WriteDword(0x479C39, new_CasBattlefieldTable + 28);    //66D864
	Z_new_towns->WriteDword(0x479C86, new_CasBattlefieldTable + 28);
	Z_new_towns->WriteDword(0x4929F3, new_CasBattlefieldTable + 28);

	Z_new_towns->WriteDword(0x4625B8, new_CasBattlefieldTable + 32);  //66D868
	Z_new_towns->WriteDword(0x465EE9, new_CasBattlefieldTable + 32);

	Z_new_towns->WriteDword(0x4945C9, new_CasBattlefieldTable + 36);  //66D86C

	Z_new_towns->WriteDword(0x4945D1, new_CasBattlefieldTable + 38);  //66D86E

	Z_new_towns->WriteDword(0x493923, new_CasBattlefieldTable + 72);
	Z_new_towns->WriteDword(0x4955C2, new_CasBattlefieldTable + 72);  //66D890

	Z_new_towns->WriteDword(0x493967, new_CasBattlefieldTable + 108);   //66D8B4
	Z_new_towns->WriteDword(0x494260, new_CasBattlefieldTable + 120 + 24);   //66D8C0 + 0x18
	Z_new_towns->WriteDword(0x494259, new_CasBattlefieldTable + 146);   //66D8DA

	Z_new_towns->WriteLoHook(0x00463712, fixCitadelCastleBitMask_00463712);

}