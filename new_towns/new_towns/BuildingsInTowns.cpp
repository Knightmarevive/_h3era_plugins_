#include "patcher_x86_commented.hpp"
#include <vector>
#include <cstdio>


#pragma warning(disable:4996)
//#include"lib/H3API.hpp"
#include"../../__include__/H3API/single_header/H3API.hpp"

extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;

// short Internal_Building_Pos[27 * 44 * 3];

short New_Building_Pos[27 * 44 * 3];
short* frames = New_Building_Pos + 0;
short* posX = New_Building_Pos + 1;
short* posY = New_Building_Pos + 2;

constexpr long  Old_Building_Pos_bytesize = 9 * 44 * 6;

//

char text_TownsBackGroundPrefix_ST[27][16] = {
	"TBCs", "TBRm" , "TBTw", "TBIn", "TBNc",  "TBDn", "TBSt", "TBFr", "TBEl",
	"TBCs", "TBRm" , "TBTw", "TBIn", "TBNc",  "TBDn", "TBSt", "TBFr", "TBEl",
	"TBCs", "TBRm" , "TBTw", "TBIn", "TBNc",  "TBDn", "TBSt", "TBFr", "TBEl"
	// "BAD"
};
long new_TownsBackGroundPrefix_ST[27];
extern "C" __declspec(dllexport) void set_town_background(long town, char* prefix) {

	strcpy_s(text_TownsBackGroundPrefix_ST[town], prefix);
}

long town_subtype[27] = {
	0,1,2,3,4,5,6,7,8,
	0,1,2,3,4,5,6,7,8,
	0,1,2,3,4,5,6,7,8,
};

int Libraries[27];
int MysticPonds[27];
int Treasuries[27];

//


constexpr long old_TownsBildingsDefs_ST_bytesize = 0x006436A4 - 0x00643074;

char text_TownsBildingsDefs_ST[27 * 44][16];
long new_TownsBildingsDefs_ST[27 * 44 + 1];






extern "C" __declspec(dllexport) void set_town_building_def(long town, long building, char* text) {

	strcpy_s(text_TownsBildingsDefs_ST[town * 44 + building], text);
}


//

char text_TownMP3[27][16] = {
	"CstleTown", "Rampart", "TowerTown", "InfernoTown", "necroTown", "dungeon", "StrongHold", "FortressTown", "ElemTown",
	"CstleTown", "Rampart", "TowerTown", "InfernoTown", "necroTown", "dungeon", "StrongHold", "FortressTown", "ElemTown",
	"CstleTown", "Rampart", "TowerTown", "InfernoTown", "necroTown", "dungeon", "StrongHold", "FortressTown", "ElemTown"
	// "BAD"
};
extern "C" __declspec(dllexport) void set_town_mp3(long town, char* mp3) {

	strcpy_s(text_TownMP3[town], mp3);
}

long new_TownMP3[27];
//

long new_TownPreBuildStrIndexTable[27 * 41 + 1];

//
char text_TownBuildingsSelection[27 * 44][16];
long new_TownBuildingsSelection[27 * 44 + 1];


extern "C" __declspec(dllexport) void set_town_building_outline(long town, long building, char* text) {

	strcpy_s(text_TownBuildingsSelection[town * 44 + building], text);
}
//

/*
long jmptable_00461668[27] = {
	  0x0046105D, 0x004610E1, 0x00461177, 0x004611A0,  0x004611FA, 0x00461223, 0x00461223, 0x00461253, 0x004612B8,
	  0x0046105D, 0x004610E1, 0x00461177, 0x004611A0,  0x004611FA, 0x00461223, 0x00461223, 0x00461253, 0x004612B8,
	  0x0046105D, 0x004610E1, 0x00461177, 0x004611A0,  0x004611FA, 0x00461223, 0x00461223, 0x00461253, 0x004612B8
	  // 0xBADBAD00
};
*/

void patch_BuildingsInTowns_late(void) {


	memcpy(New_Building_Pos, (void*)0x68AA0C, Old_Building_Pos_bytesize);
	memcpy(New_Building_Pos + 1 * Old_Building_Pos_bytesize / 2, (void*)0x68AA0C, Old_Building_Pos_bytesize);
	memcpy(New_Building_Pos + 2 * Old_Building_Pos_bytesize / 2, (void*)0x68AA0C, Old_Building_Pos_bytesize);

	// memcpy(New_Building_Pos, Internal_Building_Pos, sizeof(New_Building_Pos));

	Z_new_towns->WriteDword(0x005C32E6 + 3, (long)posX);
	Z_new_towns->WriteDword(0x005C32F3 + 3, (long)posY);

}

extern "C" __declspec(dllexport) void set_town_building_position(long town, long building, long _PosX_, long _PosY_) {
	short* ptr = New_Building_Pos + town * 44 * 3 + building * 3;
	*(++ptr) = _PosX_; *(++ptr) = _PosY_;
}


extern unsigned long new_BuildExclusions[27 * 44 * 2];
extern unsigned long new_TownsBuildingsDep[];
extern long new_AllEnStrBitsTown_[27 * 2 + 2];

int SpecBuildResetDepends_[27][32] = {
	{22, 5, -1, 18, 32, -1, 19, 39, 18, -1, -100},
	{18, 31, -1, 19, 38, 18, -1, 24, 34, -1, 25, 41, 24, -1, 21, 17, -1, -100},
	{18, 31, -1, 19, 38, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, 24, 32, -1, 25, 39, 24, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},

	{22, 5, -1, 18, 32, -1, 19, 39, 18, -1, -100},
	{18, 31, -1, 19, 38, 18, -1, 24, 34, -1, 25, 41, 24, -1, 21, 17, -1, -100},
	{18, 31, -1, 19, 38, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, 24, 32, -1, 25, 39, 24, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},

	{22, 5, -1, 18, 32, -1, 19, 39, 18, -1, -100},
	{18, 31, -1, 19, 38, 18, -1, 24, 34, -1, 25, 41, 24, -1, 21, 17, -1, -100},
	{18, 31, -1, 19, 38, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, 24, 32, -1, 25, 39, 24, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},
	{18, 30, -1, 19, 37, 18, -1, -100},


};

int BuildDepends_[27][128] = {
	// from 0 to 8
	{0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 6, -1, 5, -1, 22, 5, -1, 14, -1, 15, 14, -1, 16, -1, 17, 6, -1,
	7, -1, 8, 7, -1, 9, 8, -1, 11, 5, -1, 12, 11, 16, 0, 14, -1, 13, 12, 9, -1, 30, 7, -1, 37, 30, -1,
	31, 30, -1, 38, 31, -1, 33, 30, 16, -1, 40, 33, -1, 32, 33, -1, 18, 32, -1, 39, 32, -1, 19, 39, -1,
	21, 33, -1, 35, 21, -1, 42, 35, -1, 34, 33, 0, -1, 41, 34, -1, 36, 34, -1, 43, 36, -1, 26, -1, -100},
	{7, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1,
	4, 3, -1, 17, -1, 21, 17, -1, 11, 5, -1, 12, 11, 0, 16, 14, -1, 13, 12, 9, -1, 30, 7, -1, 37, 30,
	-1, 31, 30, -1, 18, 31, -1, 22, 18, -1, 38, 31, -1, 19, 38, -1, 32, 30, -1, 39, 32, -1, 34, 32, -1,
	24, 34, -1, 41, 34, -1, 25, 41, -1, 33, 32, -1, 40, 33, -1, 35, 33, 34, -1, 42, 35, -1, 36, 35, 1,
	-1, 43, 36, 2, -1, 26, -1, -100},
	{7, -1, 21, 7, -1, 8, 7, -1, 9, 8, -1, 0, -1, 22, 0, -1, 23, 0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 4,
	3, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 17, 14, -1, 11, 5, -1, 12, 11, 14, 16, 0, -1, 13, 12, 9,
	 -1, 30, 7, -1, 37, 30, -1, 31, 30, -1, 18, 31, -1, 38, 31, -1, 19, 38, -1, 32, 30, -1, 39, 32, -1,
	 33, 31, 32, 0, -1, 40, 33, 22, -1, 34, 33, -1, 41, 34, -1, 35, 33, -1, 42, 35, -1, 36, 34, 35, -1,
	 43, 36, -1, 26, -1, -100
	},
	{7, -1, 21, 7, -1, 8, 7, -1, 22, 8, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 0, -1, 23, 0,
	-1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 4, 3, -1, 11, 5, -1, 12, 11, 16, 14, 0, -1, 13, 12, 9, -1, 30, 7,
	-1, 18, 30, -1, 37, 30, -1, 19, 37, -1, 32, 30, -1, 24, 32, -1, 39, 32, -1, 25, 39, -1, 31, 30, -1,
	38, 31, -1, 33, 31, -1, 40, 33, -1, 35, 33, 0, -1, 42, 35, -1, 34, 33, -1, 41, 34, 1, -1, 36, 34,
	35, -1,
	43, 36, -1, 26, -1, -100
	},
	{7, -1,
	17, 7, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 6, -1, 0, -1, 21, 0, -1, 1, 0, -1,
	2, 1, -1, 3, 2, -1, 4, 3, -1, 11, 5, -1, 12, 11, 14, 16, 0, -1, 13, 12, 9, -1, 30, 7, -1, 22, 30,
	-1, 18, 22, -1, 37, 30, -1, 19, 37, 22, -1, 32, 30, -1, 39, 32, -1, 31, 30, -1, 38, 31, -1, 33, 31,
	-1, 40, 33, 21, -1, 34, 31, 0, -1, 41, 34, -1, 35, 33, 34, -1, 42, 35, -1, 36, 35, -1, 43, 36, -1,
	26, -1, -100,
	},
	{ 7, -1, 8, 7, -1, 9, 8, -1, 22, -1, 23, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 17, 14, -1, 0, -1, 21,
	0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 4, 3, -1, 11, 5, -1, 12, 11, 16, 14, 0, -1, 13, 12, 9, -1, 30,
	7, -1, 18, 30, -1, 37, 30, -1, 19, 37, -1, 32, 30, -1, 39, 32, -1, 31, 30, -1, 38, 31, -1, 33, 32,
	31, -1, 40, 33, -1, 34, 33, -1, 41, 34, -1, 35, 33, -1, 42, 35, -1, 36, 34, 35, 1, -1, 43, 36, 2,
	-1, 26, -1, -100,},
	{ 7, -1, 17, 7, -1, 23, 7, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 22, 16, -1, 14, -1, 15, 14, -1, 21,
	14, -1, 0, -1, 1, 0, -1, 2, 1, -1, 11, 5, -1, 12, 11, 0, 14, 16, -1, 13, 12, 9, -1, 30, 7, -1, 18,
	30, -1, 37, 30, -1, 19, 37, -1, 31, 30, -1, 38, 31, 37, -1, 34, 31, -1, 41, 34, -1, 36, 34, -1, 43,
	36, -1, 32, 30, -1, 39, 32, 16, -1, 33, 32, -1, 40, 33, 0, -1, 35, 33, -1, 42, 35, -1, 26, -1, -100,
	},{ 7, -1, 21, 7, -1, 22, 21, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 0, -1, 1, 0,
	-1, 2, 1, -1, 11, 5, -1, 17, 11, 21, -1, 12, 11, 14, 16, 0, -1, 6, -1, 13, 12, 9, -1, 30, 7, -1, 18,
	30, -1, 37, 30, 5, -1, 19, 37, -1, 31, 30, -1, 38, 31, -1, 35, 31, -1, 42, 35, -1, 32, 30, -1, 39,
	32, -1, 33, 32, -1, 40, 33, -1, 34, 32, 31, -1, 41, 34, 15, -1, 36, 35, 33, -1, 43, 36, -1, 26, -1,
	-100 },
	{
	7, -1, 8, 7, -1, 9, 8, -1, 6, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 17, 14, -1, 0, -1, 1, 0, -1,
	2, 1, -1, 3, 2, -1, 4, 3, -1, 21, 0, -1, 11, 5, -1, 12, 11, 14, 16, 0, -1, 13, 12, 9, -1, 30, 7, -1,
	18, 30, -1, 37, 30, -1, 19, 37, -1, 31, 30, 0, -1, 38, 31, -1, 32, 30, 0, -1, 39, 32, -1, 33, 31,
	-1, 40, 33, 38, -1, 34, 32, -1, 41, 34, -1, 35, 33, 34, -1, 42, 35, 1, -1, 36, 35, -1, 43, 36, -1,
	26, -1, -100,
	},

	// from 9 to 17
		{0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 6, -1, 5, -1, 22, 5, -1, 14, -1, 15, 14, -1, 16, -1, 17, 6, -1,
	7, -1, 8, 7, -1, 9, 8, -1, 11, 5, -1, 12, 11, 16, 0, 14, -1, 13, 12, 9, -1, 30, 7, -1, 37, 30, -1,
	31, 30, -1, 38, 31, -1, 33, 30, 16, -1, 40, 33, -1, 32, 33, -1, 18, 32, -1, 39, 32, -1, 19, 39, -1,
	21, 33, -1, 35, 21, -1, 42, 35, -1, 34, 33, 0, -1, 41, 34, -1, 36, 34, -1, 43, 36, -1, 26, -1, -100},
	{7, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1,
	4, 3, -1, 17, -1, 21, 17, -1, 11, 5, -1, 12, 11, 0, 16, 14, -1, 13, 12, 9, -1, 30, 7, -1, 37, 30,
	-1, 31, 30, -1, 18, 31, -1, 22, 18, -1, 38, 31, -1, 19, 38, -1, 32, 30, -1, 39, 32, -1, 34, 32, -1,
	24, 34, -1, 41, 34, -1, 25, 41, -1, 33, 32, -1, 40, 33, -1, 35, 33, 34, -1, 42, 35, -1, 36, 35, 1,
	-1, 43, 36, 2, -1, 26, -1, -100},
	{7, -1, 21, 7, -1, 8, 7, -1, 9, 8, -1, 0, -1, 22, 0, -1, 23, 0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 4,
	3, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 17, 14, -1, 11, 5, -1, 12, 11, 14, 16, 0, -1, 13, 12, 9,
	 -1, 30, 7, -1, 37, 30, -1, 31, 30, -1, 18, 31, -1, 38, 31, -1, 19, 38, -1, 32, 30, -1, 39, 32, -1,
	 33, 31, 32, 0, -1, 40, 33, 22, -1, 34, 33, -1, 41, 34, -1, 35, 33, -1, 42, 35, -1, 36, 34, 35, -1,
	 43, 36, -1, 26, -1, -100
	},
	{7, -1, 21, 7, -1, 8, 7, -1, 22, 8, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 0, -1, 23, 0,
	-1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 4, 3, -1, 11, 5, -1, 12, 11, 16, 14, 0, -1, 13, 12, 9, -1, 30, 7,
	-1, 18, 30, -1, 37, 30, -1, 19, 37, -1, 32, 30, -1, 24, 32, -1, 39, 32, -1, 25, 39, -1, 31, 30, -1,
	38, 31, -1, 33, 31, -1, 40, 33, -1, 35, 33, 0, -1, 42, 35, -1, 34, 33, -1, 41, 34, 1, -1, 36, 34,
	35, -1,
	43, 36, -1, 26, -1, -100
	},
	{7, -1,
	17, 7, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 6, -1, 0, -1, 21, 0, -1, 1, 0, -1,
	2, 1, -1, 3, 2, -1, 4, 3, -1, 11, 5, -1, 12, 11, 14, 16, 0, -1, 13, 12, 9, -1, 30, 7, -1, 22, 30,
	-1, 18, 22, -1, 37, 30, -1, 19, 37, 22, -1, 32, 30, -1, 39, 32, -1, 31, 30, -1, 38, 31, -1, 33, 31,
	-1, 40, 33, 21, -1, 34, 31, 0, -1, 41, 34, -1, 35, 33, 34, -1, 42, 35, -1, 36, 35, -1, 43, 36, -1,
	26, -1, -100,
	},
	{ 7, -1, 8, 7, -1, 9, 8, -1, 22, -1, 23, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 17, 14, -1, 0, -1, 21,
	0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 4, 3, -1, 11, 5, -1, 12, 11, 16, 14, 0, -1, 13, 12, 9, -1, 30,
	7, -1, 18, 30, -1, 37, 30, -1, 19, 37, -1, 32, 30, -1, 39, 32, -1, 31, 30, -1, 38, 31, -1, 33, 32,
	31, -1, 40, 33, -1, 34, 33, -1, 41, 34, -1, 35, 33, -1, 42, 35, -1, 36, 34, 35, 1, -1, 43, 36, 2,
	-1, 26, -1, -100,},
	{ 7, -1, 17, 7, -1, 23, 7, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 22, 16, -1, 14, -1, 15, 14, -1, 21,
	14, -1, 0, -1, 1, 0, -1, 2, 1, -1, 11, 5, -1, 12, 11, 0, 14, 16, -1, 13, 12, 9, -1, 30, 7, -1, 18,
	30, -1, 37, 30, -1, 19, 37, -1, 31, 30, -1, 38, 31, 37, -1, 34, 31, -1, 41, 34, -1, 36, 34, -1, 43,
	36, -1, 32, 30, -1, 39, 32, 16, -1, 33, 32, -1, 40, 33, 0, -1, 35, 33, -1, 42, 35, -1, 26, -1, -100,
	},{ 7, -1, 21, 7, -1, 22, 21, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 0, -1, 1, 0,
	-1, 2, 1, -1, 11, 5, -1, 17, 11, 21, -1, 12, 11, 14, 16, 0, -1, 6, -1, 13, 12, 9, -1, 30, 7, -1, 18,
	30, -1, 37, 30, 5, -1, 19, 37, -1, 31, 30, -1, 38, 31, -1, 35, 31, -1, 42, 35, -1, 32, 30, -1, 39,
	32, -1, 33, 32, -1, 40, 33, -1, 34, 32, 31, -1, 41, 34, 15, -1, 36, 35, 33, -1, 43, 36, -1, 26, -1,
	-100 },
	{
	7, -1, 8, 7, -1, 9, 8, -1, 6, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 17, 14, -1, 0, -1, 1, 0, -1,
	2, 1, -1, 3, 2, -1, 4, 3, -1, 21, 0, -1, 11, 5, -1, 12, 11, 14, 16, 0, -1, 13, 12, 9, -1, 30, 7, -1,
	18, 30, -1, 37, 30, -1, 19, 37, -1, 31, 30, 0, -1, 38, 31, -1, 32, 30, 0, -1, 39, 32, -1, 33, 31,
	-1, 40, 33, 38, -1, 34, 32, -1, 41, 34, -1, 35, 33, 34, -1, 42, 35, 1, -1, 36, 35, -1, 43, 36, -1,
	26, -1, -100,
	},

	// from 18 to 26
	{ 0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 6, -1, 5, -1, 22, 5, -1, 14, -1, 15, 14, -1, 16, -1, 17, 6, -1,
	7, -1, 8, 7, -1, 9, 8, -1, 11, 5, -1, 12, 11, 16, 0, 14, -1, 13, 12, 9, -1, 30, 7, -1, 37, 30, -1,
	31, 30, -1, 38, 31, -1, 33, 30, 16, -1, 40, 33, -1, 32, 33, -1, 18, 32, -1, 39, 32, -1, 19, 39, -1,
	21, 33, -1, 35, 21, -1, 42, 35, -1, 34, 33, 0, -1, 41, 34, -1, 36, 34, -1, 43, 36, -1, 26, -1, -100 },
	{ 7, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1,
	4, 3, -1, 17, -1, 21, 17, -1, 11, 5, -1, 12, 11, 0, 16, 14, -1, 13, 12, 9, -1, 30, 7, -1, 37, 30,
	-1, 31, 30, -1, 18, 31, -1, 22, 18, -1, 38, 31, -1, 19, 38, -1, 32, 30, -1, 39, 32, -1, 34, 32, -1,
	24, 34, -1, 41, 34, -1, 25, 41, -1, 33, 32, -1, 40, 33, -1, 35, 33, 34, -1, 42, 35, -1, 36, 35, 1,
	-1, 43, 36, 2, -1, 26, -1, -100 },
	{ 7, -1, 21, 7, -1, 8, 7, -1, 9, 8, -1, 0, -1, 22, 0, -1, 23, 0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 4,
	3, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 17, 14, -1, 11, 5, -1, 12, 11, 14, 16, 0, -1, 13, 12, 9,
	 -1, 30, 7, -1, 37, 30, -1, 31, 30, -1, 18, 31, -1, 38, 31, -1, 19, 38, -1, 32, 30, -1, 39, 32, -1,
	 33, 31, 32, 0, -1, 40, 33, 22, -1, 34, 33, -1, 41, 34, -1, 35, 33, -1, 42, 35, -1, 36, 34, 35, -1,
	 43, 36, -1, 26, -1, -100
	},
	{ 7, -1, 21, 7, -1, 8, 7, -1, 22, 8, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 0, -1, 23, 0,
	-1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 4, 3, -1, 11, 5, -1, 12, 11, 16, 14, 0, -1, 13, 12, 9, -1, 30, 7,
	-1, 18, 30, -1, 37, 30, -1, 19, 37, -1, 32, 30, -1, 24, 32, -1, 39, 32, -1, 25, 39, -1, 31, 30, -1,
	38, 31, -1, 33, 31, -1, 40, 33, -1, 35, 33, 0, -1, 42, 35, -1, 34, 33, -1, 41, 34, 1, -1, 36, 34,
	35, -1,
	43, 36, -1, 26, -1, -100
	},
	{ 7, -1,
	17, 7, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 6, -1, 0, -1, 21, 0, -1, 1, 0, -1,
	2, 1, -1, 3, 2, -1, 4, 3, -1, 11, 5, -1, 12, 11, 14, 16, 0, -1, 13, 12, 9, -1, 30, 7, -1, 22, 30,
	-1, 18, 22, -1, 37, 30, -1, 19, 37, 22, -1, 32, 30, -1, 39, 32, -1, 31, 30, -1, 38, 31, -1, 33, 31,
	-1, 40, 33, 21, -1, 34, 31, 0, -1, 41, 34, -1, 35, 33, 34, -1, 42, 35, -1, 36, 35, -1, 43, 36, -1,
	26, -1, -100,
	},
	{ 7, -1, 8, 7, -1, 9, 8, -1, 22, -1, 23, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 17, 14, -1, 0, -1, 21,
	0, -1, 1, 0, -1, 2, 1, -1, 3, 2, -1, 4, 3, -1, 11, 5, -1, 12, 11, 16, 14, 0, -1, 13, 12, 9, -1, 30,
	7, -1, 18, 30, -1, 37, 30, -1, 19, 37, -1, 32, 30, -1, 39, 32, -1, 31, 30, -1, 38, 31, -1, 33, 32,
	31, -1, 40, 33, -1, 34, 33, -1, 41, 34, -1, 35, 33, -1, 42, 35, -1, 36, 34, 35, 1, -1, 43, 36, 2,
	-1, 26, -1, -100, },
	{ 7, -1, 17, 7, -1, 23, 7, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 22, 16, -1, 14, -1, 15, 14, -1, 21,
	14, -1, 0, -1, 1, 0, -1, 2, 1, -1, 11, 5, -1, 12, 11, 0, 14, 16, -1, 13, 12, 9, -1, 30, 7, -1, 18,
	30, -1, 37, 30, -1, 19, 37, -1, 31, 30, -1, 38, 31, 37, -1, 34, 31, -1, 41, 34, -1, 36, 34, -1, 43,
	36, -1, 32, 30, -1, 39, 32, 16, -1, 33, 32, -1, 40, 33, 0, -1, 35, 33, -1, 42, 35, -1, 26, -1, -100,
	}, { 7, -1, 21, 7, -1, 22, 21, -1, 8, 7, -1, 9, 8, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 0, -1, 1, 0,
	-1, 2, 1, -1, 11, 5, -1, 17, 11, 21, -1, 12, 11, 14, 16, 0, -1, 6, -1, 13, 12, 9, -1, 30, 7, -1, 18,
	30, -1, 37, 30, 5, -1, 19, 37, -1, 31, 30, -1, 38, 31, -1, 35, 31, -1, 42, 35, -1, 32, 30, -1, 39,
	32, -1, 33, 32, -1, 40, 33, -1, 34, 32, 31, -1, 41, 34, 15, -1, 36, 35, 33, -1, 43, 36, -1, 26, -1,
	-100 },
	{
	7, -1, 8, 7, -1, 9, 8, -1, 6, -1, 5, -1, 16, -1, 14, -1, 15, 14, -1, 17, 14, -1, 0, -1, 1, 0, -1,
	2, 1, -1, 3, 2, -1, 4, 3, -1, 21, 0, -1, 11, 5, -1, 12, 11, 14, 16, 0, -1, 13, 12, 9, -1, 30, 7, -1,
	18, 30, -1, 37, 30, -1, 19, 37, -1, 31, 30, 0, -1, 38, 31, -1, 32, 30, 0, -1, 39, 32, -1, 33, 31,
	-1, 40, 33, 38, -1, 34, 32, -1, 41, 34, -1, 35, 33, 34, -1, 42, 35, 1, -1, 36, 35, -1, 43, 36, -1,
	26, -1, -100,
	},


};
/*
int __stdcall FormTownBuildDepends_004EB810_old(HiHook* h) {
	int(__fastcall * ProcessDependanceList)(int, int, int) =
		(int(__fastcall*)(int, int, int)) 0x004EBD30;
	int(__fastcall * ProcessResetList)(int, int) =
		(int(__fastcall*)(int, int)) 0x004EBBF0;

	int(__fastcall * ProcessResetList2)(int, int) =
		(int(__fastcall*)(int, int)) 0x004EBC50;

	auto BuildTownDepResetBits1 = new_BuildExclusions;
	int CommonBuildResetDepends = 0x0063FBC4;
	auto BuildTownDepBits1 = new_TownsBuildingsDep;
	auto AllEnStrBitsTown1 = new_AllEnStrBitsTown_;

	for (int i = 0; i < 27; ++i) {

		ProcessDependanceList((int)BuildDepends_[i], i * 44 * 2 * 4 + (int)BuildTownDepBits1, i * 2 * 4 + (int)AllEnStrBitsTown1);

		ProcessResetList((int)CommonBuildResetDepends, i * 44 * 2 * 4 + (int)BuildTownDepResetBits1);

		ProcessResetList((int)SpecBuildResetDepends_[i], i * 44 * 2 * 4 + (int)BuildTownDepResetBits1);
		ProcessResetList2((int)SpecBuildResetDepends_[i], i * 44 * 2 * 4 + (int)BuildTownDepResetBits1);
	}


	int (*TownHordeBuildings_Unit)(void) = (int (*)()) 0x005BE360;
	return TownHordeBuildings_Unit();
}*/

/*
char Hall___def[27][16] = {
	"hallcstl.def","hallramp.def", "halltowr.def", "hallinfr.def", "hallnecr.def", "halldung.def", "hallstrn.def", "hallfort.def", "hallelem.def",
	"hallcstl.def","hallramp.def", "halltowr.def", "hallinfr.def", "hallnecr.def", "halldung.def", "hallstrn.def", "hallfort.def", "hallelem.def",
	"hallcstl.def","hallramp.def", "halltowr.def", "hallinfr.def", "hallnecr.def", "halldung.def", "hallstrn.def", "hallfort.def", "hallelem.def",
};
*/

extern char text_TownsHallDefs[27][16];
bool Unlock20Slots = true;

int town_hall_posx_index[27][20] = {
	{0,2,4,6,1,3,5,3,4,5,1,2,0,6,4,2,-1,-1,-1,-1},
	{0,2,4,6,1,3,5,3,4,1,2,0,6,5,3,1,5,-1,-1,-1},
	{0,2,4,6,1,3,5,2,4,4,0,2,0,6,5,3,6,1,-1,-1},
	{0,2,4,6,1,3,5,2,4,0,2,0,6,3,6,1,4,5,-1,-1},
	{0,2,4,6,1,3,5,2,4,6,0,2,0,6,5,4,3,1,-1,-1},
	{0,2,4,6,1,3,5,2,4,0,2,0,6,5,1,4,6,3,-1,-1},
	{0,2,4,6,1,3,5,2,4,0,2,0,6,5,1,3,4,6,-1,-1},
	{0,2,4,6,1,3,5,3,4,5,1,2,0,6,5,3,1,-1,-1,-1},
	{0,2,4,6,1,3,5,3,4,5,1,2,0,6,5,3,1,-1,-1,-1},

	{0,2,4,6,1,3,5,3,4,5,1,2,0,6,4,2,-1,-1,-1,-1},
	{0,2,4,6,1,3,5,3,4,1,2,0,6,5,3,1,5,-1,-1,-1},
	{0,2,4,6,1,3,5,2,4,4,0,2,0,6,5,3,6,1,-1,-1},
	{0,2,4,6,1,3,5,2,4,0,2,0,6,3,6,1,4,5,-1,-1},
	{0,2,4,6,1,3,5,2,4,6,0,2,0,6,5,4,3,1,-1,-1},
	{0,2,4,6,1,3,5,2,4,0,2,0,6,5,1,4,6,3,-1,-1},
	{0,2,4,6,1,3,5,2,4,0,2,0,6,5,1,3,4,6,-1,-1},
	{0,2,4,6,1,3,5,3,4,5,1,2,0,6,5,3,1,-1,-1,-1},
	{0,2,4,6,1,3,5,3,4,5,1,2,0,6,5,3,1,-1,-1,-1},

	{0,2,4,6,1,3,5,3,4,5,1,2,0,6,4,2,-1,-1,-1,-1},
	{0,2,4,6,1,3,5,3,4,1,2,0,6,5,3,1,5,-1,-1,-1},
	{0,2,4,6,1,3,5,2,4,4,0,2,0,6,5,3,6,1,-1,-1},
	{0,2,4,6,1,3,5,2,4,0,2,0,6,3,6,1,4,5,-1,-1},
	{0,2,4,6,1,3,5,2,4,6,0,2,0,6,5,4,3,1,-1,-1},
	{0,2,4,6,1,3,5,2,4,0,2,0,6,5,1,4,6,3,-1,-1},
	{0,2,4,6,1,3,5,2,4,0,2,0,6,5,1,3,4,6,-1,-1},
	{0,2,4,6,1,3,5,3,4,5,1,2,0,6,5,3,1,-1,-1,-1},
	{0,2,4,6,1,3,5,3,4,5,1,2,0,6,5,3,1,-1,-1,-1},
};
int town_hall_posy_index[27][20] = {
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,-1,-1,-1,-1}, // 257
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,2,2,1,-1,-1,-1}, // 258
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,1,2,-1,-1}, // 259
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,1,2,1,2,-1,-1}, // 260
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,1,2,2,-1,-1}, // 261
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,2,1,1,2,-1,-1}, // 262
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,2,2,1,1,-1,-1}, // 263
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,2,-1,-1,-1}, // 264
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,2,-1,-1,-1}, // 265
	
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,-1,-1,-1,-1}, // 257
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,2,2,1,-1,-1,-1}, // 258
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,1,2,-1,-1}, // 259
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,1,2,1,2,-1,-1}, // 260
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,1,2,2,-1,-1}, // 261
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,2,1,1,2,-1,-1}, // 262
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,2,2,1,1,-1,-1}, // 263
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,2,-1,-1,-1}, // 264
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,2,-1,-1,-1}, // 265

	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,-1,-1,-1,-1}, // 257
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,2,2,1,-1,-1,-1}, // 258
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,1,2,-1,-1}, // 259
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,1,2,1,2,-1,-1}, // 260
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,1,2,2,-1,-1}, // 261
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,2,1,1,2,-1,-1}, // 262
	{3,3,3,3,4,4,4,1,0,1,0,0,0,2,2,2,1,1,-1,-1}, // 263
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,2,-1,-1,-1}, // 264
	{3,3,3,3,4,4,4,1,0,1,1,0,0,0,2,2,2,-1,-1,-1}, // 265


};

char TPTHBk[27][16] = {
	"TPTHBkCs.pcx","TPTHBkRm.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkRm.pcx","TPTHBkRm.pcx",
	"TPTHBkCs.pcx","TPTHBkRm.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkRm.pcx","TPTHBkRm.pcx",
	"TPTHBkCs.pcx","TPTHBkRm.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkTw.pcx","TPTHBkRm.pcx","TPTHBkRm.pcx",
};

_LHF_(hook_005CA020) {
	int town_type = *(int*)(c->ebp + 0x08);
	Z_new_towns->WriteDword(0x005CAA81 + 1, text_TownsHallDefs[town_type]);
	Z_new_towns->WriteDword(0x005CACC8 + 1, text_TownsHallDefs[town_type]);
	Z_new_towns->WriteDword(0x005CAF10 + 1, text_TownsHallDefs[town_type]);
	Z_new_towns->WriteDword(0x005CB152 + 1, text_TownsHallDefs[town_type]);
	Z_new_towns->WriteDword(0x005CB39A + 1, text_TownsHallDefs[town_type]);
	Z_new_towns->WriteDword(0x005CB5ED + 1, text_TownsHallDefs[town_type]);
	Z_new_towns->WriteDword(0x005CBD15 + 1, text_TownsHallDefs[town_type]);
	Z_new_towns->WriteDword(0x005CC575 + 1, text_TownsHallDefs[town_type]);
	Z_new_towns->WriteDword(0x005CCA20 + 1, text_TownsHallDefs[town_type]);

	// todo: PCXs

	return EXEC_DEFAULT;
}


_LHF_(hook_005CA905) {
	int posX[] = { 34, 131, 228, 325, 422, 519, 616 };
	int posY[] = { 37, 141, 245, 349, 453 };

	auto  p_DlgItemsStruct = (int*) c->esi;

	auto v12 = (void*) CDECL_1(int,0x00617492,52);
	int townType = c->eax; // (int)v12;
	// LOBYTE(v317) = 1;
	int v13 = 0; int &v317 =*(int*)(c->ebp-4);
	v317 = 1;

	if (v12)
		v13 = THISCALL_8(int,0x0044FFA0, v12, 0, 0, 800, 600, 0, TPTHBk[townType], 2048);
		// v13 = DlgStaticPcx8_Create(v12, 0, 0, 800, 600, 0,/* aTpthbkcs_pcx*/ "TPTHBkCs.pcx", 2048);
	else
		v13 = 0;
	int item = (int)v13;
	auto v219 = (void**)p_DlgItemsStruct[2];
	v317 = 0;// LOBYTE(v317) = 0;
	
	// AddItemToList(p_DlgItemsStruct, v219, 1u, (void**)&item);
	THISCALL_4(int, 0x005FE2D0,p_DlgItemsStruct, v219, 1u, (void**)&item);
	
	int v14 = 600;
	int v15 = 0;
	do
	{
		if (town_hall_posx_index[townType][v15] < 0
			&& town_hall_posy_index[townType][v15] < 0)
		{
			++v15;
			++v14;
			continue;
		}

		// auto v16 = (_DlgDef1_*) new(72);
		auto v16 = (void*)CDECL_1(int, 0x00617492, 72);

		// townType = (int)v16;
		v317 = 2;//LOBYTE(v317) = 2;
		int Def = 0;
		if (v16)
			//Def = DlgDef_BuildAndLoadDef(
			Def = THISCALL_12(int, 0x004EA800,
				v16,
				posX[town_hall_posx_index[townType][v15]] - 1,
				posY[town_hall_posy_index[townType][v15]] + 71,
				150,
				17,
				v14 - 200,
				"TPTHBar.def", // aTpthbar_def,
				0,
				0,
				0,
				0,
				16);
		else
			Def = 0;
		int v284 = (int)Def;
		auto v220 = (void**)p_DlgItemsStruct[2];
		v317 = 0;// LOBYTE(v317) = 0;
		
		// AddItemToList(p_DlgItemsStruct, v220, 1u, (void**)&v284);
		THISCALL_4(int, 0x005FE2D0, p_DlgItemsStruct, v220, 1u, (void**)&v284);



		//v18 = (_DlgText_*)new(80);
		auto v18 = (void*)CDECL_1(int, 0x00617492, 80);

		// townType = (int)v18;
		v317 = 3;//LOBYTE(v317) = 3; 
		int v19 = 0;

		if (v18)
			// v19 = DlgItem_BuildTextItem(
			v19 = THISCALL_12(int, 0x005BC6A0,
				v18,
				posX[town_hall_posx_index[townType][v15]],
				posY[town_hall_posy_index[townType][v15]] + 71,
				150,
				17,
				0,
				"smalfont.fnt", // (int)aSmalfont_fnt,
				1,
				v14,
				1,
				0,
				8);
		else
			v19 = 0;
		int v282 = v19;
		auto v221 = (void**)p_DlgItemsStruct[2];
		v317 = 0;//LOBYTE(v317) = 0;
		
		// AddItemToList(p_DlgItemsStruct, v221, 1u, (void**)&v282);
		THISCALL_4(int, 0x005FE2D0, p_DlgItemsStruct, v221, 1u, (void**)&v282);

		//v20 = (_DlgDef1_*)new(72);
		auto v20 = (void*)CDECL_1(int, 0x00617492, 72);

		// townType = (int)v20;
		v317 = 4;//LOBYTE(v317) = 4;

		int v21 = 0;
		if (v20)
			// v21 = DlgDef_BuildAndLoadDef(
			v21 = THISCALL_12(int, 0x004EA800,
				v20,
				posX[town_hall_posx_index[townType][v15]],
				posY[town_hall_posy_index[townType][v15]],
				150,
				70,
				v14 + 100,
				text_TownsHallDefs[townType], //aHallcstl_def,
				0,
				0,
				0,
				0,
				16);
		else
			v21 = 0;
		auto v272 = v21;
		auto v222 = (void**)p_DlgItemsStruct[2];
		v317 = 0;//LOBYTE(v317) = 0;
		
		// AddItemToList(p_DlgItemsStruct, v222, 1u, (void**)&v272);
		THISCALL_4(int, 0x005FE2D0, p_DlgItemsStruct, v222, 1u, (void**)&v272);

		//v22 = (_DlgDef1_*)new(72);
		auto v22= (void*)CDECL_1(int, 0x00617492, 72);

		// townType = (int)v22;
		v317 = 5;//LOBYTE(v317) = 5;

		int v23 = 0;
		if (v22)
			// v23 = DlgDef_BuildAndLoadDef(
			v23 = THISCALL_12( int, 0x004EA800,
				v22,
				posX[town_hall_posx_index[townType][v15]] + 135,
				posY[town_hall_posy_index[townType][v15]] + 54,
				16,
				16,
				v14 + 200,
				"TPTHChk.def",//aTpthchk_def,
				0,
				0,
				0,
				0,
				16);
		else
			v23 = 0;
		auto v270 = v23;
		auto v223 = (void**)p_DlgItemsStruct[2];
		v317 = 0;//LOBYTE(v317) = 0;
		
		// AddItemToList(p_DlgItemsStruct, v223, 1u, (void**)&v270);
		THISCALL_4(int, 0x005FE2D0, p_DlgItemsStruct, v223, 1u, (void**)&v270);
		
		++v15;
		++v14;
	} while (v14 - 600 < 20);

	c->return_address = 0x005CCAE1;
	return SKIP_DEFAULT;
}

extern "C" __declspec(dllexport) void change_hall_def(int town, char* def_name) {
	// strcpy_s(Hall___def[town], def_name);
	strcpy_s(text_TownsHallDefs[town], def_name);
}

inline int z_MsgBox(char* Mes, int MType, int PosX, int PosY, int Type1, int SType1, int Type2, int SType2, int Par, int Time2Show, int Type3, int SType3) {
	CALL_12(int, __fastcall, 0x004F6C00, Mes, MType, PosX, PosY, Type1, SType1, Type2, SType2, Par, Time2Show, Type3, SType3);

	int IDummy;
	__asm {
		mov eax, 0x6992D0
		mov ecx, [eax]
		mov eax, [ecx + 0x38]
		mov IDummy, eax
	}
	return IDummy;
}

typedef h3::H3Main _GameMgr_;
inline int z_GetRandomSpell_ForShrine(_GameMgr_* ecx, int a2) {
	return CALL_2(int, __thiscall, 0x004C9260, ecx, a2);
}

#define o_GameMgr (*(_GameMgr_**)0x699538)
inline int z_GetRandomSpell_ForShrine(int level) {	
	return  z_GetRandomSpell_ForShrine(o_GameMgr, level);
}

#include"_Town_.h"
//typedef h3::H3Town _Town_;
extern char mage_levels[27];

_LHF_(mage_guild_005CE802_hook) {
	if (GetKeyState(/*VK_CONTROL*/'Z') >= 0
		&& GetKeyState(VK_CONTROL) >= 0)
		return EXEC_DEFAULT;
	_Town_* town = (_Town_*)(c->ecx);
	int spell_shelf_ID = c->edi;
	int* spell_table = (int*)town->spells;
	int spell_level = spell_shelf_ID / 6 + 1;
	int owner = town->owner;

	//bool has_tower = (town_subtype[town->type] == 2 && town->IsBuildingBuilt(22));
	bool has_library = (Libraries[town->type] >= 0) && town->IsBuildingBuilt(Libraries[town->type]);


	if (spell_level > mage_levels[town->type] ||
		(!has_library && (spell_shelf_ID % 6 >= (6 - spell_level))))
		return EXEC_DEFAULT;

	if (!town->IsBuildingBuilt(85)) {
		z_MsgBox((char*)"Scriptorium not Built", 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
		return EXEC_DEFAULT;
	}

	char message[512];
	// sprintf_s(message, "spell %i at level %i", spell_table[spell_shelf_ID], spell_level);
	sprintf_s(message, "Do you wish to change this spell \n to another spell at level at level %i \n "
		"(for %i mithril and %i gold) ???", spell_level, spell_level, spell_level * 1000);
	int answer = z_MsgBox(message, 2, -1, -1, 9, spell_table[spell_shelf_ID], 7, spell_level, -1, 0, 6, spell_level * 1000);


	if (answer == 0x7805) {
		int& gold = o_GameMgr->players[owner].playerResources[6];
		int& mithril = ((int*)(0x27F9A00))[owner];

		if (mithril < spell_level || gold < spell_level * 1000) {
			z_MsgBox((char*)"Not Enough Resources", 1, -1, -1, -1, 0, -1, 0, -1, 0, -1, 0);
			return EXEC_DEFAULT;
		}

		int new_spell = -1;
		h3::H3Spell* z_Spells = h3::H3Spell::Get();
		do {
			new_spell = z_GetRandomSpell_ForShrine(spell_level);
		} while (((z_Spells[new_spell].battlefieldSpell == 0)
			&& (z_Spells[new_spell].mapSpell == 0))
			// || z_Spells[new_spell].school == 0
			// || (spell_level == 1 && new_spell > 70)
			/*
			|| strstr(o_Spells[new_spell].name, "WoG Spell")
			|| strstr(o_Spells[new_spell].shortName, "WoG Spell")
			*/);

		spell_table[spell_shelf_ID] = z_GetRandomSpell_ForShrine(spell_level);

		gold -= 1000 * spell_level; mithril -= spell_level;
	}

	return EXEC_DEFAULT;
}

// int Libraries[27];

_LHF_(library_005D735A) {
	auto town = (_Town_*)c->ecx;

	if (Libraries[town->type] >= 0 && town->IsBuildingBuilt(Libraries[town->type]))
		++(c->esi);

	c->return_address = 0x005D737F;
	return SKIP_DEFAULT;

	/*
	if (town_subtype[*(char*)(c->ecx + 4)] == 2)
		c->return_address = 0x005D7360;
	else
		c->return_address = 0x005D737F;
	*/
	return NO_EXEC_DEFAULT;
}

_LHF_(hook_004B8952) {   
	if (1 != *(short*)(c->ebx + 0x1F63E))
		return EXEC_DEFAULT;
	h3::H3Town* town = (h3::H3Town*)c->esi;
	int* resources = (int*)c->edi;
	h3::H3Player* plr = (h3::H3Player*)*(int*)(c->ebp - 0x20);

	if (Treasuries[town->type] >= 0 && town->IsBuildingBuilt(Treasuries[town->type])) {
		resources[6] += plr->playerResources.gold / 10LL;
	}


	if (MysticPonds[town->type] >= 0 && town->IsBuildingBuilt(MysticPonds[town->type])) {
		resources[town->mysticPondResourceType] += town->mysticPondResourceCount;
	}

	c->return_address = 0x004B89D6;
	return NO_EXEC_DEFAULT;
}

_LHF_(hook_004C7E20) {
	h3::H3Town* town = (h3::H3Town*)c->esi;
	if (MysticPonds[town->type] >= 0 && town->IsBuildingBuilt(MysticPonds[town->type]))
		c->return_address = 0x004C7E46;
	else c->return_address = 0x004C7E7F;

	return NO_EXEC_DEFAULT;
}

void __stdcall Town_ShowStructureBuilt_hook(HiHook* hook, h3::H3TownManager* ecx, int build_id) {
	CALL_2(void, __thiscall, hook->GetDefaultFunc(), ecx, build_id);

	if (build_id >= 64 && build_id < 128) {
		// auto PlayWAVFile = LoadPlayWAVFile("Buildtwn.82m");
		auto PlayWAVFile = FASTCALL_1(__int64, 0x0059A770, "Buildtwn.82m");
		// Sound_WaitForWavToPlay(-1, PlayWAVFile, v43);

	}
}

_LHF_(hook_005D6EAD) {
	int build_id = *(int*)(c->ebp + 0x8);

	if (build_id > 43) {
		c->eax = 0;
		c->ecx = 1;

		return SKIP_DEFAULT;
	}

	return EXEC_DEFAULT;
}

extern std::vector<int> get_depends(int town_type, int building_ID);

_LHF_(hook_005D7244) {
	h3::H3TownManager* townMgr = reinterpret_cast<h3::H3TownManager*>(c->esi);
	h3::H3Town* town = townMgr->town;
	int townType = town->type;
	uint8_t screenId = c->ebx;
	auto* buildingInfo = townMgr->buildingDrawing[screenId];
	int build_id = *(int*)(c->ebp + 0x8);

	int mappedId = -1;

	if (build_id >= 64 && build_id <= 70) {
		if (town->IsBuildingBuilt(19)) {
			std::vector<int> dependencies = get_depends(townType, 19);

			for (int dependency : dependencies) {
				if (dependency == build_id - 27) {
					mappedId = 19;
					break;
				}
			}
		}
		else if (town->IsBuildingBuilt(25)) {
			std::vector<int> dependencies = get_depends(townType, 25);

			for (int dependency : dependencies) {
				if (dependency == build_id - 27) {
					mappedId = 25;
					break;
				}
			}
		}
		else {
			mappedId = build_id - 27;
		}
	}
	else if (build_id >= 71 && build_id <= 77) {
		if (town->IsBuildingBuilt(19)) {
			std::vector<int> dependencies = get_depends(townType, 19);

			for (int dependency : dependencies) {
				if (dependency == (build_id - 71) + 37) {
					mappedId = 19;
					break;
				}
			}
		}
		else if (town->IsBuildingBuilt(25)) {
			std::vector<int> dependencies = get_depends(townType, 25);

			for (int dependency : dependencies) {
				if (dependency == (build_id - 71) + 37) {
					mappedId = 25;
					break;
				}
			}
		}
		else {
			mappedId = (build_id - 71) + 37;
		}
	}

	int mappedScreenId = -1;
	if (mappedId != -1) {
		for (int i = 0; i < 44; ++i) {
			if (townMgr->buildingDrawing[i] && townMgr->buildingDrawing[i]->id == mappedId) {
				mappedScreenId = i;
				break;
			}
		}
	}

	if (mappedId != -1 && mappedScreenId != -1) {
		c->eax = 1;
		c->ebx = mappedScreenId;
	}

	return EXEC_DEFAULT;
}

void patch_BuildingsInTowns(void) {
	Z_new_towns->WriteLoHook(0x005D6EAD, hook_005D6EAD);
	
	Z_new_towns->WriteLoHook(0x005D7244, hook_005D7244);

	// Z_new_towns->WriteHiHook(0x005D6E80, SPLICE_, EXTENDED_, THISCALL_, Town_ShowStructureBuilt_hook);

	Z_new_towns->WriteLoHook(0x005CE802, mage_guild_005CE802_hook);
	Z_new_towns->WriteLoHook(0x005D735A, library_005D735A);
	// memset(Libraries, -1, 27 * 4);	Libraries[2] = 22;

	Z_new_towns->WriteLoHook(0x004B8952, hook_004B8952);
	Z_new_towns->WriteLoHook(0x004C7E20, hook_004C7E20);
	memset(Treasuries, -1, 27 * 4);	Treasuries[1] = 22;
	memset(MysticPonds, -1, 27 * 4);	MysticPonds[1] = 17;

	Z_new_towns->WriteLoHook(0x005CA020, hook_005CA020);
	if (Unlock20Slots) {
		Z_new_towns->WriteDword(0x004616DE + 1, 600 + 20);
		Z_new_towns->WriteDword(0x004616F4 + 1, 700 + 20);
		Z_new_towns->WriteDword(0x0046170A + 1, 800 + 20);
		// Z_new_towns->WriteByte(0x005D59E1 + 1, 20);
		Z_new_towns->WriteByte(0x005D5A21 + 2, 20);
	}
	/*
	memcpy(Internal_Building_Pos, (void*)0x68AA0C, Old_Building_Pos_bytesize);
	memcpy(Internal_Building_Pos + 1 * Old_Building_Pos_bytesize/2, (void*)0x68AA0C, Old_Building_Pos_bytesize);
	memcpy(Internal_Building_Pos + 2 * Old_Building_Pos_bytesize/2, (void*)0x68AA0C, Old_Building_Pos_bytesize);
	*/

	for (int i = 0; i < 27; ++i) new_TownsBackGroundPrefix_ST[i] = (long)text_TownsBackGroundPrefix_ST[i];
	Z_new_towns->WriteDword(0x005C6E40 + 3, (long)new_TownsBackGroundPrefix_ST);

	for (int i = 0; i < 9 * 44; ++i) {
		strcpy_s(text_TownsBildingsDefs_ST[i], (char*)*(int*)(0x00643074 + i * 4));
		strcpy_s(text_TownsBildingsDefs_ST[i + 9 * 44], (char*)*(int*)(0x00643074 + i * 4));
		strcpy_s(text_TownsBildingsDefs_ST[i + 18 * 44], (char*)*(int*)(0x00643074 + i * 4));

	}

	for (int i = 0; i < 27 * 44; ++i) new_TownsBildingsDefs_ST[i] = (long)&text_TownsBildingsDefs_ST[i][0];

	new_TownsBildingsDefs_ST[27 * 44] = -1;

	Z_new_towns->WriteDword(0x005C6EE9 + 3, (long)new_TownsBildingsDefs_ST);

	for (int i = 0; i < 27; ++i) new_TownMP3[i] = (long)text_TownMP3[i];

	Z_new_towns->WriteDword(0x005C70F2 + 3, (long)new_TownMP3);

	memcpy(new_TownPreBuildStrIndexTable, (void*)0x00688910, 0x5c4);
	memcpy(new_TownPreBuildStrIndexTable + 9 * 41, (void*)0x00688910, 0x5c4);
	memcpy(new_TownPreBuildStrIndexTable + 18 * 41, (void*)0x00688910, 0x5c4);
	new_TownPreBuildStrIndexTable[27 * 41] = 0;
	Z_new_towns->WriteDword(0x00484324 + 3, (long)new_TownPreBuildStrIndexTable);
	Z_new_towns->WriteDword(0x005C0320 + 3, (long)new_TownPreBuildStrIndexTable);
	Z_new_towns->WriteDword(0x005C0E20 + 3, (long)new_TownPreBuildStrIndexTable);
	Z_new_towns->WriteDword(0x005C0EFA + 3, (long)new_TownPreBuildStrIndexTable);

	for (int i = 0; i < 27 * 44; ++i) new_TownBuildingsSelection[i] = (long)text_TownBuildingsSelection[i];
	for (int i = 0; i < 9 * 44; ++i) {
		strcpy_s(text_TownBuildingsSelection[i], (char*)*(int*)(0x0068A3DC + i * 4));
		strcpy_s(text_TownBuildingsSelection[i + 9 * 44], (char*)*(int*)(0x0068A3DC + i * 4));
		strcpy_s(text_TownBuildingsSelection[i + 18 * 44], (char*)*(int*)(0x0068A3DC + i * 4));
	}	new_TownBuildingsSelection[27 * 44] = 0;
	Z_new_towns->WriteDword(0x005C3393 + 3, (long)new_TownBuildingsSelection);

	// Z_new_towns->WriteDword(0x00461056 + 3, (long)jmptable_00461668);
	// Z_new_towns->WriteByte(0x0046104D + 2, 27 - 1);

	//// should not be needed in new dependency system
	//Z_new_towns->WriteHiHook(0x004EB810, SPLICE_, EXTENDED_, CDECL_, FormTownBuildDepends_004EB810);
}


// #include"lib/H3API.hpp"
typedef h3::H3CreatureInformation _CreatureInfo_;
// typedef h3::H3Town _Town_;
typedef h3::H3Hero _Hero_;
#define o_pCreatureInfo h3::H3CreatureInformation::Get()
#define o_GameMgr h3::H3Main::Get()
#define o_TownMgr h3::H3TownManager::Get()
extern long ThirdUpgradesInTowns_27x2x7[27 * 2 * 7];
extern long MonstersInTowns_27x2x7[27 * 2 * 7];



inline int Town_GetGrowth(_Town_* town, int building) {
	return CALL_2(short, __thiscall, 0x005BFF60, town, building);
}

int getPriorityFor3rdAnd4thUpgradeBuildings(_Town_* town, int building, h3::H3Resources &creatureBuyMaxResourceCost)
{
	int town_type = town->type; 
	int tumUpgradeCreatureType, currentUpgradeCreatureType, normalizedBuilding;
	if (building >= 64 && building < 71) 
	{
		currentUpgradeCreatureType = MonstersInTowns_27x2x7[town_type * 14 + building - 64 + 7];
		tumUpgradeCreatureType = ThirdUpgradesInTowns_27x2x7[town_type * 14 + building - 64];
		normalizedBuilding = building - 64; if (currentUpgradeCreatureType < 0) return -1;
	}
	else 
	if (building >= 71 && building < 78) 
	{
		currentUpgradeCreatureType = ThirdUpgradesInTowns_27x2x7[town_type * 14 + building - 64 - 7];
		tumUpgradeCreatureType = ThirdUpgradesInTowns_27x2x7[town_type * 14 + building - 64];
		normalizedBuilding = building - 71; if (tumUpgradeCreatureType < 0) return -1;
	}
	else
		return 0;

	int ret;

	_CreatureInfo_* creatureWithCurrentUpgradeInfo = &o_pCreatureInfo[currentUpgradeCreatureType];
	_CreatureInfo_* creatureAfterUpgradeInfo =&o_pCreatureInfo[tumUpgradeCreatureType];

	int growth = town->recruits[1][normalizedBuilding];
	if (o_GameMgr->date.day >= 5u)
		growth += Town_GetGrowth(town, normalizedBuilding+7);

	for (int i=0;i<7;i++)
	{
		int costDifference = creatureAfterUpgradeInfo->cost.asArray[i] - creatureWithCurrentUpgradeInfo->cost.asArray[i]; // SadnessPower: this could be a bug from original code:
		                                                                                                                  // it should take in consideration the new costs of creatures, not the cost difference
		creatureBuyMaxResourceCost.asArray[i] = growth * costDifference;
	} 

	ret = growth * (creatureAfterUpgradeInfo->aiValue - creatureWithCurrentUpgradeInfo->aiValue);

	return ret;
}
