
#include <windows.h>
#include <cstdio>
#include <vector>
#include <string>
//#include"lib/H3API.hpp"

#pragma warning(disable:4996)
#include"../../__include__/H3API/single_header/H3API.hpp"


#define char_table_size 512
#define int_tuple_size 256

// #pragma warning(disable : 4996)

extern int Libraries[27];
extern int MysticPonds[27];
extern int Treasuries[27];
extern int swapper[27][128];
extern int upper[27][128];

extern "C" __declspec(dllexport) void set_town_resource_silo(long town, long res0, long res1, long res2, long res3, long res4, long res5, long res6);

void ParseArray(char* buf, char* name, int* cortage)
{

}


void ParseFloat(char* buf, char* name, float* result)
{
	char* c = strstr(buf, name);
	if (c != NULL)
		*result = atof(c + strlen(name));
}

bool ParseInt(char* buf, char* name, int* result)
{
	char* c = strstr(buf, name);
	if (c != NULL) {
		*result = atoi(c + strlen(name));
		return true;
	}
	else return false;
}

void ParseByte(char* buf, char* name, char* result)
{
	char* c = strstr(buf, name);
	if (c != NULL)
		*result = (char)atoi(c + strlen(name));
}


bool ParseStr(char* buf, char* name, /*char* &target */ char res[char_table_size])
{
	// char res[char_table_size];
	char* c = strstr(buf, name);
	int l = strlen(name);
	if (c != NULL)
	{
		char* cend = strstr(c + l, "\"");
		if (cend != NULL) //���� ��, ����� ������ �����������
		{
			//*res = (char*)malloc(cend-c+1);

			//memset(*res,0,cend-c+1);
			memset(res, 0, cend - c + 1);

			for (int i = 0, j = 0; j < (cend - c) - l; i++, j++)
			{
				if (c[l + j] != '\\')
				{
					//(*res)[i] = c[l+j];
					res[i] = c[l + j];
				}
				else
				{
					if (c[l + j + 1] == 'r')
					{
						//(*res)[i] = '\r';
						res[i] = '\r';

						j++;
						continue;
					}
					else if (c[l + j + 1] == 'n')
					{
						//(*res)[i] = '\n';
						res[i] = '\n';
						j++;
						continue;
					}
					else
					{
						//(*res)[i] = c[l+j];
						res[i] = c[l + j];

					}
				}
			}

			//strncpy(*res,c+l,cend-1-c);

			//(*res)[cend-c-l] = 0;

			res[cend - c - l] = 0;

			//res[cend - c - l] = 13; //end of WoG erm
			//res[cend - c - l +1] = 0;

			//sprintf(*res,*res);

			/*
			if (strcmp(target, res) != 0 ) {
				strcpy(target, res);
			}
			*/
		}
		return true;
	}
	else return false;
}
void ParseStr2(char* buf, char* name, char*& target_1, char*& target_2
	/* char res[char_table_size]*/, char* target_static, int& target_int) {

	char res[char_table_size];
	if (ParseStr(buf, name, res)) {
		target_1 = target_2 = target_static;
		strcpy(target_static, res);
		target_int = 0;
	}

	/*
	if (target && target!= target2 && !*target)
		strcpy(target2, target);
	ParseStr(buf, name, target2);
	target = target2;
	target3 = 0;
	*/

	/*
	char res[char_table_size];
	ParseStr(buf, name, res);
	if (!target || strcmp(target, res) != 0) {
		//char* tmp = new char[char_table_size];
		//target = tmp;
		target = target2;
		strcpy(target, res);
	}
	*/
}


int ParseTuple(char* buf, char* name, /* int** tuple */ int tuple[int_tuple_size])
{
	int len = 0;
	//char *tmp = (char*)malloc(strlen(buf));
	static char tmp[32768];
	char* c = strstr(buf, name);
	if (c != NULL)
	{
		strncpy(tmp, c + strlen(name), int_tuple_size);
		c = strpbrk(tmp, "\r\n\0}");
		if (c != NULL)
		{
			tmp[c - tmp] = 0;

			char* p = strtok(tmp, ",");

			do
			{
				if (p)
				{
					len++;
					// *tuple = (int*)realloc(*tuple,len*sizeof(int));
					// (*tuple)[len-1]=atoi(p);
					tuple[len - 1] = atoi(p);
				}
				p = strtok(NULL, ", ");
			} while (p);
		}
	}
	//free(tmp);
	return len;
}

#include <sstream>

int new_buildings_cost[27][64][7] = {};
char new_buildings_names[27][64][512] = {};
char new_buildings_descriptions[27][64][2048];
extern char mage_levels[27];

extern long ThirdUpgradesInTowns_27x2x7[27 * 2 * 7];
extern long MonstersInTowns_27x2x7[27 * 2 * 7];
extern long TownGuardianType[27];

extern long grails[27][9];

extern "C" __declspec(dllexport) void set_town_building_position(long town, long building, long _PosX_, long _PosY_);
extern "C" __declspec(dllexport) void set_town_adventure_look(long town, char* look0, char* look1, char* look2);

extern "C" __declspec(dllexport) void set_town_mp3(long town, char* mp3);
extern "C" __declspec(dllexport) void set_town_background(long town, char* prefix);
extern "C" __declspec(dllexport) void change_hall_def(int town, char* def_name);

extern "C" __declspec(dllexport) void set_town_building_outline(long town, long building, char* text);
extern "C" __declspec(dllexport) void set_town_building_def(long town, long building, char* text);

extern "C" __declspec(dllexport) void set_town_type_name(long town, char* name);
extern "C" __declspec(dllexport) void set_TownsBackGroundCreatures0(long town, char* name);
extern "C" __declspec(dllexport) void set_TownsBackGroundCreatures1(long town, char* name);
extern "C" __declspec(dllexport) void set_townhall_def(long town, char* name);
extern "C" __declspec(dllexport) void set_town_dwelling_names(long town, long dwelling, long is_upgraded, char* name);
extern "C" __declspec(dllexport) void set_town_dwelling_descriptions(long town, long dwelling, long is_upgraded, char* desc);
extern "C" __declspec(dllexport) void set_town_spec_building_names(long town, long spec, char* name);
extern "C" __declspec(dllexport) void set_town_spec_building_descriptions(long town, long spec, char* desc);

extern "C" __declspec(dllexport) void change_TownsBuildingsDrawQueue(long town_type, long index, long value);
extern "C" __declspec(dllexport) void set_town_dwelling_cost(long town, long dwelling, long is_upgraded, long res0, long res1, long res2, long res3, long res4, long res5, long res6);
extern "C" __declspec(dllexport) void set_TownsBuildingsCost1(long town_type, long building_ID, long res0, long res1, long res2, long res3, long res4, long res5, long res6);

extern char text_TownsBlackSmithDesc[27][512];

extern h3::H3BuildingsBitfield m_dependency[h3::limits::BUILDINGS];

extern BOOL8 TownChooseable[27];
extern int RMG_TownNativeLand[27];

bool set_depends(int town_type, int building_ID, char* str);
void RemapBuildings_Configure(int town_type, char* config);
void change_num_Tables(int town_type, char* config);

char towns_enabled_in_RMG[27][4];

extern long EighthMonType[27][4];

extern char new_TownsBuildingsSpecNum[27];
extern char text_TownSpecBuildingsDesc[27 * 11][512];
extern char text_TownsBackGroundMage[27][16];


extern "C" __declspec(dllexport) void ChangeSiegePictures(int town, char* marker);
extern "C" __declspec(dllexport) void ConfigureTown(int town_type, char* config) 
{
	char pattern[256]; char* c; char temp[4096]; int temp_int = 0;
	RemapBuildings_Configure(town_type, config);
	change_num_Tables(town_type, config);

	ParseInt(config, "TownNativeLand=", RMG_TownNativeLand + town_type);
	ParseByte(config, "Chooseable=", TownChooseable + town_type);


	if (ParseStr(config, "SiegePicturePattern=\"", temp)) {
		ChangeSiegePictures(town_type, temp);
	}

	ParseByte(config, "RMG_Human=",		towns_enabled_in_RMG[town_type] + 0);
	ParseByte(config, "RMG_Computer=",	towns_enabled_in_RMG[town_type] + 1);
	ParseByte(config, "RMG_Treasure=",	towns_enabled_in_RMG[town_type] + 2);
	ParseByte(config, "RMG_Junction=",	towns_enabled_in_RMG[town_type] + 3);

	sprintf(pattern, "BuildingDrawQueue_all=\"");
	if (ParseStr(config, pattern, temp)) {
		std::stringstream SS(temp);
		for (int i = 0; i < 44; ++i) {
			SS >> temp_int;
			change_TownsBuildingsDrawQueue(town_type, i, temp_int);
		}
	}

	if (ParseInt(config, (char*)"EighthMon=", &temp_int)) {
		EighthMonType[town_type][0] = temp_int;
	}
	else EighthMonType[town_type][0] = -1;

	if (ParseStr(config, "BuildingDesc0015=\"", temp)) {
		// strcpy_s(text_TownSpecBuildingsDesc[town_type*11], temp);
		set_town_spec_building_descriptions(town_type, 10, temp);
	}

	if (ParseStr(config, "BuildingDesc0016=\"", temp)) {
		strcpy_s(text_TownsBlackSmithDesc[town_type], temp);
	}

	for (int i = 0; i < 128; ++i) {
		sprintf(pattern, "Swapper%04d=", i);
		if (ParseInt(config, pattern, &temp_int)) {
			swapper[town_type][i] = temp_int;
		}
		sprintf(pattern, "Upper%04d=", i);
		if (ParseInt(config, pattern, &temp_int)) {
			upper[town_type][i] = temp_int;
		}
	}

	for (int i = 0; i < 44; ++i) {
		sprintf(pattern, "BuildingDrawQueue%04d=", i);
		if (ParseInt(config, pattern, &temp_int)) {
			change_TownsBuildingsDrawQueue(town_type, i, temp_int);
		}

		sprintf(pattern, "BuildingPosition%04d=\"", i);
		if (ParseStr(config, pattern, temp)) {
			std::stringstream SS(temp);
			int x, y; SS >> x >> y;
			set_town_building_position(town_type, i, x, y);
		}
		sprintf(pattern, "BuildingOutline%04d=\"", i);
		if (ParseStr(config, pattern, temp)) {
			set_town_building_outline(town_type, i, temp);
		}
		sprintf(pattern, "BuildingGraphics%04d=\"", i);
		if (ParseStr(config, pattern, temp)) {
			set_town_building_def(town_type, i, temp);
		}


		if (i >= 17 && i <= 26) {
			sprintf(pattern, "BuildingName%04d=\"", i);
			if (ParseStr(config, pattern, temp)) {
				set_town_spec_building_names(town_type, i - 17, temp);
			}
			sprintf(pattern, "BuildingDesc%04d=\"", i);
			if (ParseStr(config, pattern, temp)) {
				set_town_spec_building_descriptions(town_type, i - 17, temp);
			}

			sprintf(pattern, "BuildingCost%04d=\"", i);
			if (ParseStr(config, pattern, temp)) {
				int temp_cost[7]; std::stringstream SS(temp);
				SS >> temp_cost[0] >> temp_cost[1] >> temp_cost[2] >> temp_cost[3] >> temp_cost[4] >> temp_cost[5] >> temp_cost[6];
				// set_town_dwelling_cost(town_type, dwelling, is_upgraded, temp_cost[0], temp_cost[1], temp_cost[2], temp_cost[3], temp_cost[4], temp_cost[5], temp_cost[6]);
				set_TownsBuildingsCost1(town_type, i, temp_cost[0], temp_cost[1], temp_cost[2], temp_cost[3], temp_cost[4], temp_cost[5], temp_cost[6]);
			}
		}

		if (ParseStr(config, "ResourceSilo=\"", temp)) {
			int temp_income[7]; std::stringstream SS(temp);
			SS >> temp_income[0] >> temp_income[1] >> temp_income[2] >> temp_income[3] >> temp_income[4] >> temp_income[5] >> temp_income[6];
			set_town_resource_silo(town_type, temp_income[0], temp_income[1], temp_income[2], temp_income[3], temp_income[4], temp_income[5], temp_income[6]);
		}

		if (i >= 30 && i < 44) {
			long is_upgraded = (i > 36); long dwelling = (i - 30) % 7;
			sprintf(pattern, "BuildingName%04d=\"", i);
			if(ParseStr(config, pattern, temp)){
				set_town_dwelling_names(town_type, dwelling, is_upgraded, temp);
			}
			sprintf(pattern, "BuildingDesc%04d=\"", i);
			if (ParseStr(config, pattern, temp)) {
				set_town_dwelling_descriptions(town_type, dwelling, is_upgraded, temp);
			}
			sprintf(pattern, "BuildingCost%04d=\"", i);
			if (ParseStr(config, pattern, temp)) {
				int temp_cost[7]; std::stringstream SS(temp); 
				SS >> temp_cost[0] >> temp_cost[1] >> temp_cost[2] >> temp_cost[3] >> temp_cost[4] >> temp_cost[5] >> temp_cost[6];
				set_town_dwelling_cost(town_type, dwelling, is_upgraded, temp_cost[0], temp_cost[1], temp_cost[2], temp_cost[3], temp_cost[4], temp_cost[5], temp_cost[6]);
			}
		}
	}

	for (int i = 0; i < 128; ++i) {
		char tmp[512] = "";
		sprintf(pattern, "BuildingDeps%04d=\"", i);
		if (ParseStr(config, pattern, tmp))
			set_depends(town_type, i, tmp);
	}

	for (int i = 0; i < 64; ++i) {

		sprintf(pattern, "BuildingName%04d=\"", i + 64);
		ParseStr(config, pattern, new_buildings_names[town_type][i]);
		sprintf(pattern, "BuildingDesc%04d=\"", i + 64);
		ParseStr(config, pattern, new_buildings_descriptions[town_type][i]);
		sprintf(pattern, "BuildingCost%04d=\"", i + 64);
		if (ParseStr(config, pattern, temp)) {
			std::stringstream SS(temp);
			for (int j = 0; j < 7; ++j)
				SS >> new_buildings_cost[town_type][i][j];
		}
	}

	for (int i = 0; i < 7; ++i) {
		sprintf(pattern, "level_%d_creatures=\"", i);
		if (ParseStr(config, pattern, temp)) {
			std::stringstream SS(temp);
			SS >> MonstersInTowns_27x2x7[town_type * 14 + 0 + i];
			SS >> MonstersInTowns_27x2x7[town_type * 14 + 7 + i];
			SS >> ThirdUpgradesInTowns_27x2x7[town_type * 14 + 0 + i];
			SS >> ThirdUpgradesInTowns_27x2x7[town_type * 14 + 7 + i];
		}
	}

	if (ParseStr(config, "level_7_creatures=\"", temp)) {
		std::stringstream SS(temp);
		SS >> EighthMonType[town_type][0];
		SS >> EighthMonType[town_type][1];
		SS >> EighthMonType[town_type][2];
		SS >> EighthMonType[town_type][3];
	}

	if (ParseInt(config, (char*)"Treasuries=", &temp_int)) {
		Treasuries[town_type] = temp_int;
	}
	if (ParseInt(config, (char*)"MysticPonds=", &temp_int)) {
		MysticPonds[town_type] = temp_int;
	}

	if (ParseInt(config, (char*)"Libraries=", &temp_int)) {
		Libraries[town_type] = temp_int;
	}

	if (ParseInt(config, (char*)"mage_levels=", &temp_int)) {
		mage_levels[town_type] = temp_int;
	}

	if (ParseInt(config, (char*)"town_guardian=", &temp_int)) {
		TownGuardianType[town_type] = temp_int;
	}
	else TownGuardianType[town_type] = -1;

	if (ParseStr(config, (char*)"adventure_look=\"", temp)) {
		std::stringstream SS(temp);
		char look0[32], look1[32], look2[32];
		SS >> look0 >> look1 >> look2;
		set_town_adventure_look(town_type, look0, look1, look2);
	}

	if (ParseStr(config, (char*)"town_type_name=\"", temp)) {
		set_town_type_name(town_type, temp);
	}
	if (ParseStr(config, (char*)"town_mp3=\"", temp)) {
		set_town_mp3(town_type, temp);
	}
	if (ParseStr(config, (char*)"town_background=\"", temp)) {
		set_town_background(town_type, temp);
	}
	if (ParseStr(config, (char*)"mage_guild_background=\"", temp)) {
		strcpy(text_TownsBackGroundMage[town_type], temp);
	}
	if (ParseStr(config, (char*)"townhall_def=\"", temp)) {
		change_hall_def(town_type, temp);
		set_townhall_def(town_type, temp);
	}
	if (ParseStr(config, (char*)"CreatureBG0=\"", temp))
	{
		set_TownsBackGroundCreatures0(town_type, temp);
	}
	if (ParseStr(config, (char*)"CreatureBG1=\"", temp))
	{
		set_TownsBackGroundCreatures1(town_type, temp);
	}
}

void RemapBuildings_Reset(int town_type);
extern "C" __declspec(dllexport) void ResetTown(int town_type) {
	char temp[512];
	RemapBuildings_Reset(town_type);


	for (int i = 0; i < 64; ++i) {
		sprintf(temp, "Building(%i)", i + 64);
		strcpy(new_buildings_names[town_type][i], temp);
		strcpy(new_buildings_descriptions[town_type][i], temp);
	}

	for (int i = 78; i <= 84; ++i) {
		int level = i - 77;
		sprintf(temp, "Cathedral Lv %i", level);
		strcpy(new_buildings_names[town_type][i - 64], temp);
		strcpy(new_buildings_descriptions[town_type][i - 64],
			"Increases creature growths and summons Town Guardians under siege.");
		new_buildings_cost[town_type][i - 64][6] =
			((13 << level) - (level * level * level)) * 400;
		new_buildings_cost[town_type][i - 64][0] = 32;
		new_buildings_cost[town_type][i - 64][2] = 32;
	}

	strcpy(new_buildings_names[town_type][85 - 64], "Scriptorium");
	strcpy(new_buildings_descriptions[town_type][85 - 64], "Allows to reroll spell in Mage Guild... for a price.");
	new_buildings_cost[town_type][85 - 64][6] = 5000;
	new_buildings_cost[town_type][85 - 64][0] = 10;
	new_buildings_cost[town_type][85 - 64][2] = 10;


	strcpy(new_buildings_names[town_type][86 - 64], "Azylum");
	strcpy(new_buildings_descriptions[town_type][86 - 64], "Allows to hide hero safe from war... until time arise.");
	new_buildings_cost[town_type][86 - 64][6] = 5000;
	new_buildings_cost[town_type][86 - 64][0] = 10;
	new_buildings_cost[town_type][86 - 64][2] = 10;

	strcpy(new_buildings_names[town_type][87 - 64], "Market of Time");
	strcpy(new_buildings_descriptions[town_type][87 - 64], "Allows to trade Memories of your Heroes.");
	new_buildings_cost[town_type][87 - 64][6] = 5000;
	new_buildings_cost[town_type][87 - 64][0] = 10;
	new_buildings_cost[town_type][87 - 64][2] = 10;


	for (int i = 88; i <= 91; ++i) {
		int grade = i - 87;
		sprintf(temp, "Eighth Dwelling - grade %i", grade);
		strcpy(new_buildings_names[town_type][i - 64], temp);
		strcpy(new_buildings_descriptions[town_type][i - 64], "Allows to recruit mystical creatures.");
		new_buildings_cost[town_type][i - 64][6] = 5000;
		new_buildings_cost[town_type][i - 64][0] = 10;
		new_buildings_cost[town_type][i - 64][2] = 10;
	}
}

void LoadTownConfig(int target)
{
	char* buf, * c, fname[256];
	FILE* fdesc;

	TownChooseable[target] = (target < 9);
	towns_enabled_in_RMG[target][0] = (target < 9);
	towns_enabled_in_RMG[target][1] = (target < 9);
	towns_enabled_in_RMG[target][2] = (target < 9);
	towns_enabled_in_RMG[target][3] = (target < 9);

	ResetTown(target);

	/*
	sprintf(fname,"Mods\\Amethyst Upgrades\\Data\\creatures\\%u.cfg",target);
	if(!FileExists(fname)) */
	/*    sprintf(fname,"Mods\\Knightmare Kingdoms\\Data\\creatures\\%u.cfg",target);
	if(!FileExists(fname))*/
	sprintf(fname, "Data\\towns\\%u.cfg", target);
	if (fdesc = fopen(fname, "r"))
	{
		//----------
		fseek(fdesc, 0, SEEK_END);
		int fdesc_size = ftell(fdesc);
		rewind(fdesc);
		//----------
		buf = (char*)malloc(fdesc_size + 1);
		fread(buf, 1, fdesc_size, fdesc);
		buf[fdesc_size] = 0;
		fclose(fdesc);
		//begin of majaczek

		ConfigureTown(target, buf);


		free(buf);
	}
	else
		ConfigureTown(target, ""); // init configuration with default values if no cfg file is found

	
}

extern char text_TownDwellingsNames[27 * 14][32];
extern char text_TownDwellingsDesc[27 * 14][512];
void FixTownConfig(int target) {
	for (int i = 0; i < 14; ++i) {
		char temp[512];
		sprintf(temp, "Building(%i)", i + 64);
		auto& desc = new_buildings_descriptions[target][i];
		auto& mon_id = ThirdUpgradesInTowns_27x2x7[14 * target + i];
		if (strcmp(desc, temp) == 0 && mon_id >= 0 && mon_id < 1024) {
			auto& mon = P_Creatures[mon_id];
			sprintf(desc, "The %s allows you to recruit %s.",
				new_buildings_names[target][i], mon.namePlural);
		}
	}

	if(target >8) for (int i = 0; i < 14; ++i) {
		char temp[512];
		sprintf(temp, "Building(%i)", i + 30);
		auto& desc = text_TownDwellingsDesc[target*14+i];
		auto& mon_id = MonstersInTowns_27x2x7[14 * target + i];
		if (strcmp(desc, temp) == 0 && mon_id >= 0 && mon_id < 1024) {
			auto& mon = P_Creatures[mon_id];
			sprintf(desc, "The %s allows you to recruit %s.",
				text_TownDwellingsNames[target * 14 + i], mon.namePlural);
		}
	}
}

extern bool scriptorium_enabled;
extern bool cathedral_enabled;
extern bool azylum_enabled;
extern bool market_of_time_enabled;
extern long buildings_daily;
extern bool newTownSelectionDialog;
extern bool EighthsSupported;
extern bool Unlock20Slots;

extern long multiple_builds_in_town[48];
extern "C" __declspec(dllexport) void set_buildings_daily(int arg)
{
	buildings_daily = arg;
}

extern "C" __declspec(dllexport) void set_buildings_daily_and_update(int arg)
{
	buildings_daily = arg;
	for (int i = 0;i < 48;i++)
		multiple_builds_in_town[i] = buildings_daily;
}

extern "C" __declspec(dllexport) long get_buildings_daily() {
	return buildings_daily;
}

bool Investment_enabled = true;
void LoadGlobalConfig(void) {
	char* buf, * c; // fname[256];
	FILE* fdesc; int answer = 0;
	if (fdesc = fopen("Data\\towns.cfg", "r"))
	{

		//----------
		fseek(fdesc, 0, SEEK_END);
		int fdesc_size = ftell(fdesc);
		rewind(fdesc);
		//----------
		buf = (char*)malloc(fdesc_size + 1);
		fread(buf, 1, fdesc_size, fdesc);
		buf[fdesc_size] = 0;
		fclose(fdesc);

		if (ParseInt(buf, (char*)"disable_investment=", &answer))
			if (answer) Investment_enabled = false;

		if (ParseInt(buf, (char*)"disable_scriptorium=", &answer))
			if (answer) scriptorium_enabled = false;


		if (ParseInt(buf, (char*)"disable_cathedral=", &answer))
			if (answer) cathedral_enabled = false;


		if (ParseInt(buf, (char*)"disable_azylum=", &answer))
			if (answer) azylum_enabled = false;

		if (ParseInt(buf, (char*)"disable_market_of_time=", &answer))
			if (answer) market_of_time_enabled = false;

		if (ParseInt(buf, (char*)"buildings_daily=", &answer))
			if (answer) set_buildings_daily_and_update(answer);

		if(ParseInt(buf, (char*)"disable_newTownSelectionDialog=", &answer))
			newTownSelectionDialog = !answer;
		
		if (ParseInt(buf, (char*)"EighthsSupported=", &answer))
			EighthsSupported = answer;


		if (ParseInt(buf, (char*)"Unlock20Slots=", &answer))
			Unlock20Slots = answer;

		free(buf);
	}
}
