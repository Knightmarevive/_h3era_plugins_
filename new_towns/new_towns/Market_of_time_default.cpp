int  current_SS_count = 28;
int  max_SS_level = 3;

#include <windows.h>
#include <cstdio>
#include <stdlib.h>
//#include "Tchar.h"
#include <io.h>

// #include "Storage.h"

#pragma warning(disable : 4996)

#define _H3API_PATCHER_X86_
#define _H3API_PLUGINS_
#include "../../__include__/H3API/single_header/H3API.hpp"

extern PatcherInstance* Zecondary_Skills;

auto oldSkillsDescriptions = (h3::H3SecondarySkillInfo*)0x00698d88;

struct Market_of_Time_Dlg : public h3::H3Dlg
{
    h3::H3Hero* Hero = nullptr; int counter; bool marked[256]; char text[512];
    Market_of_Time_Dlg(h3::H3Hero* He) : H3Dlg(800, 600, -1, -1, 0, 0),
        Hero(He), counter(0), marked(), text("Click skills to Forget") {
        AddBackground(0, 0, widthDlg, heightDlg, false, false, 0, false);
        CreateText(128, 550, 512, 32, text, h3::NH3Dlg::Text::MEDIUM, 1, 888);

        int frame_color = ((h3::H3BasePalette565*)*(int*)0x006AAD18)->color[45];

        int j = 0;
        for (int i = 0; i < current_SS_count; ++i) {
            // int Skill_Level = get_hero_SS(Hero->id, i);
            int Skill_Level = Hero->secSkill[i];
            if (!Skill_Level) continue;
            CreateFrame(16 + 86 * (j % 9), 16 + 97 * (j / 9), 86, 97, 4000 + i, frame_color);
            CreateDef(18 + 86 * (j % 9), 18 + 97 * (j / 9), 3000 + i, "Secsk82.def", 2 + 3 * i + Skill_Level);
            // CreateFrame(16 + 86 * (j % 9), 16 + 97 * (j / 9), 86 , 97, 4000 + i, frame_color)->Hide();
            ++j;
        }

    }

    BOOL DialogProc(h3::H3Msg& msg) override;
};

BOOL Market_of_Time_Dlg::DialogProc(h3::H3Msg& msg) {
    if (msg.subtype == h3::eMsgSubtype::LBUTTON_CLICK) {
        auto target = msg.GetDlg()->ItemAtPosition(msg);
        if (msg.itemId >= 3000 && msg.itemId < 4000) {
            auto dlg = this->GetH3DlgItem(msg.itemId);
            // int val = get_hero_SS(Hero->id, msg.itemId - 3000);
            int val = Hero->secSkill[msg.itemId - 3000];
            if (dlg->IsVisible()) {
                dlg->Hide();
                counter += val;
                marked[msg.itemId - 3000] = true;
            }
            else {
                dlg->Show();
                counter -= val;
                marked[msg.itemId - 3000] = false;
            }
            sprintf_s(text, "Forget %d Secondary Skill Points", counter);
            this->GetText(888)->SetText(text);
            this->Redraw();
        }
    }
    if (msg.subtype == h3::eMsgSubtype::RBUTTON_DOWN) {
        auto target = msg.GetDlg()->ItemAtPosition(msg);
        if (msg.itemId >= 3000 && msg.itemId < 4000) {
            auto skill_id = msg.itemId - 3000;
            auto skill_LV = Hero->secSkill[skill_id];
            LPCSTR text = oldSkillsDescriptions[skill_id].description[skill_LV - 1];
            auto frame = 2 + 3 * skill_id + skill_LV;
            //long D8D_Struct[4] = {1};
            //FASTCALL_5(int, 0x004F7D20, "", D8D_Struct, -1, -1, 15000);
            FASTCALL_12(void*, 0x004F6C00, text, 1, -1, -1, 20, frame, -1, 0, -1, 0, -1, 0);
        }
    }
    return 0;
}

extern "C" __declspec(dllexport) int Market_of_Time_default(h3::H3Hero * Hero) {
    if (!Hero) return 0;

    Market_of_Time_Dlg wnd(Hero);
    wnd.CreateOKButton();
    wnd.Start();

    for (int i = 0; i < current_SS_count; ++i) {
        /*
        if (wnd.marked[i])
            set_hero_SS(Hero->id, i, 0);
        */

        if (wnd.marked[i])
            Hero->secSkill[i] = 0;
    }

    return wnd.counter;
}