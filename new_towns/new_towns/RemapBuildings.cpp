#include "patcher_x86_commented.hpp"
#include <cstdio>
#include <string>

extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;

// #include "HoMM3.h"
// #include"lib/H3API.hpp"
#pragma warning(disable : 4996)

#define _H3API_EXCEPTION_
#include"../../__include__/H3API/single_header/H3API.hpp"
#include"_Town_.h"


void z_shout(const char* Mes);
long grails[27][9]; long lookout_tower[27];

long brimstone_clouds[27];
long glyphs_of_fear[27];
long blood_obelisk[27];
long brotherhood_of_the_sword[27];
extern int Libraries[27];

int mana_vortex[27];
int stables[27];
int wall_of_knowledge[27];
int order_of_fire[27];
int academy_of_battle[27];
int hall_of_vallhalla[27];
int cage_of_warlords[27];

int ArtifactMerchant[27];
int University[27][10];
int GreatTeacherBuilding[27][10];

char* get_building_description(int town_type, int build_id);

_LHF_(conflux_005BE4F3) {
	_Town_* town = (_Town_*)c->edi;
	int town_type = town->type;// *(char*)(c->edi + 4);
	int building_type = grails[town_type][8];
	if (building_type <= 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x005BE576;
	else c->return_address = 0x005BE50C;
	return NO_EXEC_DEFAULT;
}

_LHF_(conflux_005CE81C) {
	_Town_* town = (_Town_*)c->ecx;
	int town_type = town->type;
	int building_type = grails[town_type][8];
	if (building_type <= 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x005CE8D6;
	else c->return_address = 0x005CE848;
	return NO_EXEC_DEFAULT;
}

_LHF_(conflux_005CE613) {
	_Town_* town = (_Town_*)c->ecx;
	int town_type = town->type;
	int building_type = grails[town_type][8];
	if (building_type <= 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x005CE669;
	else c->return_address = 0x005CE637;
	return NO_EXEC_DEFAULT;
}

_LHF_(conflux_005D743B) {
	_Town_* town = (_Town_*)c->eax;
	int town_type = town->type;
	int building_type = grails[town_type][8];
	if (building_type <= 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x005D746A;
	else c->return_address = 0x005D7461;
	return NO_EXEC_DEFAULT;
}

extern "C" __declspec(dllexport) long mon_upgraded_in_town(long non_upgraded);
long inferno_grail_creature = 42;
_LHF_(inferno_004C85D3) {
	_Town_* town = (_Town_*)c->ecx;
	int town_type = town->type;
	int building_type = grails[town_type][3];
	if (building_type <= 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x004C85F9;
	// else c->return_address = 0x004C8602;
	else {
		auto z_pCreatureInfo = h3::H3CreatureInformation::Get();
		int growth = z_pCreatureInfo[inferno_grail_creature].grow;
		int next = mon_upgraded_in_town(inferno_grail_creature);
		*(int*)(0x006977A0) = 2;
		*(int*)(0x0069844C) = inferno_grail_creature;

		*(int*)(c->ebp - 0x08) = inferno_grail_creature; 
		*(int*)(c->ebp - 0x10) = next;
		*(int*)(c->ebp - 0x0C) = growth;

		c->return_address = 0x004C862F;
	}
	return NO_EXEC_DEFAULT;
}

void open_area(int x, int y, int z, int plr, int rad) {
	THISCALL_7(char, 0x0049CDD0, *(int*)0x00699538, x, y, z, plr, rad, 0);
}

_LHF_(tower_004C614C) {
	_Town_* town = (_Town_*)c->edi;
	int town_type = town->type;
	int building_type = grails[town_type][2];
	int building_minor = lookout_tower[town_type];

	int player = town->owner;
	if (player >= 0)
	{
		if (building_type >= 0 && town->IsBuildingBuilt(building_type)) {
			int MapSize = *(int*)(0x006783C8);
			open_area(MapSize / 2, MapSize / 2, 0, player, MapSize);
			if (h3::H3Main::Get()->mainSetup.hasUnderground)
				open_area(MapSize / 2, MapSize / 2, 1, player, MapSize);
		}
		else if (building_minor >= 0 && town->IsBuildingBuilt(building_minor)) {
			open_area(town->x, town->y, town->z, player, 20);
		}
	}

	c->return_address = 0x004C6223;
	return NO_EXEC_DEFAULT;
}

_LHF_(tower_004C79AB) {
	_Town_* town = (_Town_*)c->eax;
	int town_type = town->type;
	int building_type = grails[town_type][2];
	int building_minor = lookout_tower[town_type];

	int player = town->owner;
	if(player >=0)
	{
		if (building_type >= 0 && town->IsBuildingBuilt(building_type)) {
			int MapSize = *(int*)(0x006783C8);
			open_area(MapSize / 2, MapSize / 2, 0, player, MapSize);
			if (h3::H3Main::Get()->mainSetup.hasUnderground)
				open_area(MapSize / 2, MapSize / 2, 1, player, MapSize);
		}
		else if (building_minor >= 0 && town->IsBuildingBuilt(building_minor)) {
			open_area(town->x, town->y, town->z, player, 20);
		}
	}
	c->return_address = 0x004C7AC2;
	return NO_EXEC_DEFAULT;
}

_LHF_(Siege_00463910) {
	_Town_* town = (_Town_*)c->eax;
	int town_type = town->type;
	h3::H3Hero* hero = (h3::H3Hero*)c->edx;

	if (grails[town_type][2] >= 0 && town->IsBuildingBuilt(grails[town_type][2])) {
		hero->spellPoints += 150;
	}
	if (grails[town_type][5] >= 0 && town->IsBuildingBuilt(grails[town_type][5])) {
		hero->primarySkill[2] += 12;
	}
	if (grails[town_type][6] >= 0 && town->IsBuildingBuilt(grails[town_type][6])) {
		hero->primarySkill[0] += 20;
	}
	if (grails[town_type][7] >= 0 && town->IsBuildingBuilt(grails[town_type][7])) {
		hero->primarySkill[0] += 10;
		hero->primarySkill[1] += 10;
	}

	if (brimstone_clouds[town_type] >= 0 && town->IsBuildingBuilt(brimstone_clouds[town_type])) {
		hero->primarySkill[2] += 2;
	}
	if (glyphs_of_fear[town_type] >= 0 && town->IsBuildingBuilt(glyphs_of_fear[town_type])) {
		hero->primarySkill[0] += 2;
	}
	if (blood_obelisk[town_type] >= 0 && town->IsBuildingBuilt(blood_obelisk[town_type])) {
		hero->primarySkill[1] += 2;
	}

	c->return_address = 0x00463A98;
	return NO_EXEC_DEFAULT;
}

_LHF_(Castle_004E3E2D) {
	_Town_* town = (_Town_*)c->edx;
	int town_type = town->type;
	int building_type = grails[town_type][0];

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x004E3E52;
	else c->return_address = 0x004E3E59;

	return NO_EXEC_DEFAULT;
}


_LHF_(Castle_004DCB31) {
	_Town_* town = (_Town_*)c->edx;
	int town_type = town->type;
	int building_type = grails[town_type][0];

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x004DCB56;
	else {
		c->ecx = town_type;
		c->edx = building_type;
		c->return_address = 0x004DCB67;
	}

	return NO_EXEC_DEFAULT;
}

_LHF_(Castle_0044AD56) {
	_Town_* town = (_Town_*)c->ecx;
	int town_type = town->type;
	int building_type = brotherhood_of_the_sword[town_type];

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x0044AD80;
	else c->return_address = 0x0044AD7D;

	return NO_EXEC_DEFAULT;
}

_LHF_(rampart_004E3BA0) {
	_Town_* town = (_Town_*)c->esi;
	int town_type = town->type;
	int building_type = grails[town_type][1];

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x004E3BB5;
	else c->return_address = 0x004E3BC0;

	return NO_EXEC_DEFAULT;
}

_LHF_(rampart_004DD302) {
	_Town_* town = (_Town_*)c->edx;
	int town_type = town->type;
	int building_type = grails[town_type][1];

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x004DD326;
	else {
		c->ecx = town_type;
		c->edx = building_type;
		c->return_address = 0x004DD33A;
	}

	return NO_EXEC_DEFAULT;
}

_LHF_(Town_GiveBonusesForHero_005BDCEB) {
	_Town_* town = (_Town_*)c->esi;
	int town_type = town->type;
	int building_type = mana_vortex[town_type];

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x005BDD9A;
	else c->return_address = 0x005BDD20;

	return NO_EXEC_DEFAULT;
}


_LHF_(Town_GiveBonusesForHero_005BDD9A) {
	_Town_* town = (_Town_*)c->esi;
	int town_type = town->type;
	int building_type = stables[town_type];

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x005BDE32;
	else c->return_address = 0x005BDDC5;

	return NO_EXEC_DEFAULT;
}


_LHF_(Town_GiveBonusesForHero_005BDE32) {
	_Town_* town = (_Town_*)c->esi;
	int town_type = town->type;
	int building_type = wall_of_knowledge[town_type];

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x005BDEE7;
	else c->return_address = 0x005BDE60;

	return NO_EXEC_DEFAULT;
}


_LHF_(Town_GiveBonusesForHero_005BDEE7) {
	_Town_* town = (_Town_*)c->esi;
	int town_type = town->type;
	int building_type = order_of_fire[town_type];
	// c->edi = *(int*)(c->ebp + 8);

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x005BDF9C;
	else c->return_address = 0x005BDF15;

	return NO_EXEC_DEFAULT;
}

_LHF_(Town_GiveBonusesForHero_005BDF9C) {
	_Town_* town = (_Town_*)c->esi;
	int town_type = town->type;
	int building_type = academy_of_battle[town_type];

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x005BE068;
	else c->return_address = 0x005BDFCA;

	return NO_EXEC_DEFAULT;
}


_LHF_(Town_GiveBonusesForHero_005BE068) {
	_Town_* town = (_Town_*)c->esi;
	int town_type = town->type;
	int building_type = hall_of_vallhalla[town_type];

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x005BE149;
	else c->return_address = 0x005BE096;

	return NO_EXEC_DEFAULT;
}


_LHF_(Town_GiveBonusesForHero_005BE149) {
	_Town_* town = (_Town_*)c->esi;
	int town_type = town->type;
	int building_type = cage_of_warlords[town_type];

	if (building_type < 0 || !town->IsBuildingBuilt(building_type))
		c->return_address = 0x005BE22A;
	else c->return_address = 0x005BE177;

	return NO_EXEC_DEFAULT;
}

learn_fun_T teach = (learn_fun_T) nullptr;
void GreatTeach(h3::H3Hero* H, int skilllnum) {
	if (teach) teach(H, skilllnum, 15);
	else if (skilllnum < 28) H->secSkill[skilllnum] = 3;
}
h3::H3VisitedTownsBitset GrandTeacher[1024] = {};
void __stdcall Town_GiveBonusesForHero_005BDCE0(HiHook* hook, _Town_* town, h3::H3Hero* hero) {
	auto v10 = town->number;
	if (v10 >= 0x30) {
		z_shout("bug at Town_GiveBonusesForHero_005BDCE0 \n wrong Town ID");
		return;
	}
	
	auto town_type = town->type;
	bool town_affected = false;
	int current_building = -1;
	//if (!((1 << (v10 & 0x1F)) & *(&hero->visitedTowns + (v10 >> 5))));
	if(!hero->visitedTowns[v10])
	{
		auto b1 = cage_of_warlords[town_type];
		auto b2 = hall_of_vallhalla[town_type];
		auto b3 = academy_of_battle[town_type];
		auto b4 = order_of_fire[town_type];
		auto b5 = wall_of_knowledge[town_type];

		if (b1 >= 0 && town->IsBuildingBuilt(b1)) {
			++(hero->primarySkill[1]);
			current_building = b1;
			town_affected = true;
		}
		if (b2 >= 0 && town->IsBuildingBuilt(b2)) {
			++(hero->primarySkill[0]);
			current_building = b2;
			town_affected = true;
		}
		if (b3 >= 0 && town->IsBuildingBuilt(b3)) {
			auto v13 = (hero->GetLearningPower() * 1000.0);
			CALL_4(int, __thiscall, 0x004E3620, hero, v13, 1, 1);
			current_building = b3;
			town_affected = true;
		}
		if (b4 >= 0 && town->IsBuildingBuilt(b4)) {
			++(hero->primarySkill[3]);
			current_building = b4;
			town_affected = true;
		}
		if (b5 >= 0 && town->IsBuildingBuilt(b5)) {
			++(hero->primarySkill[2]);
			current_building = b5;
			town_affected = true;
		}
	}

	if (town_affected) {
		hero->visitedTowns[v10] = true;
		if(CALL_2(BOOL8,__thiscall,0x004CE630,h3::H3Game::Get(),hero->owner))
			z_shout(get_building_description(town_type, current_building));
	}

	if (!GrandTeacher[hero->id][v10]) {
		bool GreatTeaching = false;

		for (int i = 0; i < 10; ++i) {
			int b = GreatTeacherBuilding[town_type][i];
			if (b < 0) continue; if (!town->IsBuildingBuilt(b)) continue;
			switch (i) {
			case 0: GreatTeach(hero, 26); GreatTeach(hero, 6); GreatTeach(hero, 4);
				hero->learnedSpells[25] = hero->learnedSpells[37]
					= hero->learnedSpells[38] = hero->learnedSpells[40] = 1;
				break;
			case 1:  GreatTeach(hero, 4); GreatTeach(hero, 7); GreatTeach(hero, 8);
				GreatTeach(hero, 13); GreatTeach(hero, 21); break;
			case 2: GreatTeach(hero, 7); GreatTeach(hero, 15);
				GreatTeach(hero, 24); GreatTeach(hero, 25);
				for (int j = 0; j < 10; ++j) hero->learnedSpells[j] = 1;
				break;
			case 3: GreatTeach(hero, 7); GreatTeach(hero, 14); GreatTeach(hero, 25);
				hero->learnedSpells[13] = hero->learnedSpells[21]
					= hero->learnedSpells[22] = hero->learnedSpells[29] = 1;
				break;
			case 4: GreatTeach(hero, 12);  GreatTeach(hero, 17);
				GreatTeach(hero, 24);  GreatTeach(hero, 25);
				hero->learnedSpells[24] = hero->learnedSpells[39] = 1;
				break;
			case 5:  GreatTeach(hero, 5); GreatTeach(hero, 13);
				GreatTeach(hero, 17);  GreatTeach(hero, 24);  GreatTeach(hero, 25);
				break;
			case 6: GreatTeach(hero, 10); GreatTeach(hero, 13); GreatTeach(hero, 19);
				GreatTeach(hero, 20); GreatTeach(hero, 26); break;
			case 7: GreatTeach(hero, 0); GreatTeach(hero, 2); GreatTeach(hero, 3);
				GreatTeach(hero, 16); hero->learnedSpells[53] = hero->learnedSpells[54] = 1;
				break;
			case 8: for (int j = 66; j < 70; ++j) hero->learnedSpells[j] = 1;
				for (int j = 14; j < 18; ++j) GreatTeach(hero, j);
				break;
			}
			GreatTeaching = true;
		}

		if (GreatTeaching) {
			GrandTeacher[hero->id][v10] = true;
			if (CALL_2(BOOL8, __thiscall, 0x004CE630, h3::H3Game::Get(), hero->owner))
				z_shout("Hero visited Great Teacher for the first time");
		}
	}

	CALL_2(void, __thiscall, hook->GetDefaultFunc(), town, hero);
}

void RemapBuildings_init() {
	Z_new_towns->WriteHiHook(0x005BDCE0, SPLICE_, EXTENDED_, THISCALL_, Town_GiveBonusesForHero_005BDCE0);

	Z_new_towns->WriteLoHook(0x005BE4F3, conflux_005BE4F3);
	Z_new_towns->WriteLoHook(0x005CE613, conflux_005CE613);
	Z_new_towns->WriteLoHook(0x005CE81C, conflux_005CE81C);
	Z_new_towns->WriteLoHook(0x005D743B, conflux_005D743B);

	Z_new_towns->WriteLoHook(0x004C85D3, inferno_004C85D3);
	Z_new_towns->WriteLoHook(0x004C614C, tower_004C614C);
	Z_new_towns->WriteLoHook(0x004C79AB, tower_004C79AB);
	Z_new_towns->WriteLoHook(0x004E3E2D, Castle_004E3E2D);
	Z_new_towns->WriteLoHook(0x004DCB31, Castle_004DCB31);

	Z_new_towns->WriteLoHook(0x00463910, Siege_00463910);

	Z_new_towns->WriteLoHook(0x0044AD56, Castle_0044AD56);

	Z_new_towns->WriteLoHook(0x004DD302, rampart_004DD302);
	Z_new_towns->WriteLoHook(0x004E3BA0, rampart_004E3BA0);

	Z_new_towns->WriteLoHook(0x005BDCEB, Town_GiveBonusesForHero_005BDCEB);
	Z_new_towns->WriteLoHook(0x005BDD9A, Town_GiveBonusesForHero_005BDD9A);
	Z_new_towns->WriteLoHook(0x005BDE32, Town_GiveBonusesForHero_005BDE32);
	Z_new_towns->WriteLoHook(0x005BDEE7, Town_GiveBonusesForHero_005BDEE7);
	Z_new_towns->WriteLoHook(0x005BDF9C, Town_GiveBonusesForHero_005BDF9C);
	Z_new_towns->WriteLoHook(0x005BE068, Town_GiveBonusesForHero_005BE068);
	Z_new_towns->WriteLoHook(0x005BE149, Town_GiveBonusesForHero_005BE149);

	memset(GreatTeacherBuilding, -1, sizeof(GreatTeacherBuilding));

	// Z_new_towns->WriteLoHook();
}

void RemapBuildings_Reset(int town_type) {
	char temp[512];

	for (int i = 0; i < 9; ++i) {
		if (town_type == i)
			grails[town_type][i] = 26;
		else grails[town_type][i] = -1;
	}

	if (town_type == 2) lookout_tower[town_type] = 21;
	else lookout_tower[town_type] = -1;

	if (town_type == 3) brimstone_clouds[town_type] = 21;
	else brimstone_clouds[town_type] = -1;


	if (town_type == 7) glyphs_of_fear[town_type] = 21;
	else glyphs_of_fear[town_type] = -1;
	if (town_type == 7) blood_obelisk[town_type] = 22;
	else blood_obelisk[town_type] = -1;

	if (town_type == 0) brotherhood_of_the_sword[town_type] = 22;
	else brotherhood_of_the_sword[town_type] = -1;

	if (town_type == 2) Libraries[town_type] = 22;
	else Libraries[town_type] = -1;

	if (town_type == 5) mana_vortex[town_type] = 21;
	else mana_vortex[town_type] = -1;

	if (town_type == 0) stables[town_type] = 21;
	else stables[town_type] = -1;


	if (town_type == 2) wall_of_knowledge[town_type] = 23;
	else wall_of_knowledge[town_type] = -1;

	if (town_type == 3) order_of_fire[town_type] = 23;
	else order_of_fire[town_type] = -1;

	if (town_type == 5) academy_of_battle[town_type] = 23;
	else academy_of_battle[town_type] = -1;

	if (town_type == 6) hall_of_vallhalla[town_type] = 23;
	else hall_of_vallhalla[town_type] = -1;

	if (town_type == 7) cage_of_warlords[town_type] = 17;
	else cage_of_warlords[town_type] = -1;

	if (town_type == 2 || town_type == 5 || town_type == 8)
		ArtifactMerchant[town_type] = 17;
	else ArtifactMerchant[town_type] = -1;

	if (town_type == 8) University[town_type][0] = 21;
	else University[town_type][0] = -1;
	for(int i=1;i<10;++i) 
		University[town_type][i] = -1;

	for (int i = 1; i < 10; ++i)
		GreatTeacherBuilding[town_type][i] = -1;

}

bool ParseInt(char* buf, char* name, int* result);
void RemapBuildings_Configure(int town_type, char* config) {
	char pattern[256]; char* c; char temp[4096]; int temp_int = 0;


	for (int i = 0; i < 9; ++i) {
		sprintf(pattern, "Grail%04d=", i);
		if (ParseInt(config, pattern, &temp_int)) {
			grails[town_type][i] = temp_int;
		}
	}

	if (ParseInt(config, "cage_of_warlords=", &temp_int))
		cage_of_warlords[town_type] = temp_int;
	if (ParseInt(config, "hall_of_vallhalla=", &temp_int))
		hall_of_vallhalla[town_type] = temp_int;
	if (ParseInt(config, "academy_of_battle=", &temp_int))
		academy_of_battle[town_type] = temp_int;
	if (ParseInt(config, "order_of_fire=", &temp_int))
		order_of_fire[town_type] = temp_int;
	if (ParseInt(config, "wall_of_knowledge=", &temp_int))
		wall_of_knowledge[town_type] = temp_int;

	if (ParseInt(config, "stables=", &temp_int))
		stables[town_type] = temp_int;

	if (ParseInt(config, "mana_vortex=", &temp_int))
		mana_vortex[town_type] = temp_int;

	if (ParseInt(config, "Library=", &temp_int))
		Libraries[town_type] = temp_int;

	if (ParseInt(config, "ArtifactMerchant=", &temp_int))
		ArtifactMerchant[town_type] = temp_int;
	if (ParseInt(config, "University=", &temp_int))
		University[town_type][0] = temp_int;
	for (int i = 1; i <= 9; ++i) {
		sprintf(pattern, "University%04d=", i);
		if (ParseInt(config, pattern, &temp_int)) {
			University[town_type][i] = temp_int;
		}
	}

	for (int i = 0; i <= 9; ++i) {
		sprintf(pattern, "GreatTeacher%04d=", i);
		if (ParseInt(config, pattern, &temp_int)) {
			GreatTeacherBuilding[town_type][i] = temp_int;
		}
	}

	if (ParseInt(config, "LookoutTower=", &temp_int))
		lookout_tower[town_type] = temp_int;


	if (ParseInt(config, "brimstone_clouds=", &temp_int))
		brimstone_clouds[town_type] = temp_int;
	if (ParseInt(config, "glyphs_of_fear=", &temp_int))
		glyphs_of_fear[town_type] = temp_int;
	if (ParseInt(config, "blood_obelisk=", &temp_int))
		blood_obelisk[town_type] = temp_int;



	if (ParseInt(config, "brotherhood_of_the_sword=", &temp_int))
		brotherhood_of_the_sword[town_type] = temp_int;

}