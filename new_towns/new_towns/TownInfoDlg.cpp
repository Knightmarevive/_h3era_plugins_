#include "patcher_x86_commented.hpp"
#include <cstdio>
// #include "HoMM3.h"
// #define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)
#include "../../__include__/H3API/single_header/H3API.hpp"
#define TxtTownColour 1

extern Patcher* globalPatcher;
extern PatcherInstance* Z_new_towns;

extern long ThirdUpgradesInTowns_27x2x7[27 * 2 * 7];
extern long MonstersInTowns_27x2x7[27 * 2 * 7];

bool newTownSelectionDialog = true;
bool EighthsSupported = true;

struct TownInfo_Dlg : public h3::H3Dlg {
	int plr = -1; int town_id = -1; long long counter = 0;
	TownInfo_Dlg(int _plr, int _townid) : H3Dlg(800, 600, -1, -1, 0, 0),
		plr(_plr), town_id(_townid), counter(0)
	{
		AddBackground(0, 0, widthDlg, heightDlg, true, false, plr, false);

		auto mon = h3::H3CreatureInformation::Get();
		for (int i = 0; i < 7; ++i) {
			int d = 142;

			int id = MonstersInTowns_27x2x7[town_id*14+i];
			if(id>=0) CreateDef(32 + i * 105 - 180, 32 - 150, 3000 + i, mon[id].defName, 0, 1);
			id = MonstersInTowns_27x2x7[town_id * 14 + i + 7];
			if (id >= 0) CreateDef(32 + i * 105 - 180, 32 + d - 150, 3007 + i, mon[id].defName, 0, 1);
		
			id = ThirdUpgradesInTowns_27x2x7[town_id * 14 + i];
			if (id >= 0) CreateDef(32 + i * 105 - 180, 32 + 2*d - 150, 3014 + i, mon[id].defName, 0, 1);
			id = ThirdUpgradesInTowns_27x2x7[town_id * 14 + i + 7];
			if (id >= 0) CreateDef(32 + i * 105 - 180, 32 + 3*d - 150, 3021 + i, mon[id].defName, 0, 1);

		}
	}

	BOOL DialogProc(h3::H3Msg& msg) override;
};


BOOL TownInfo_Dlg::DialogProc(h3::H3Msg& msg) {
	if (msg.IsLeftClick() )
	{
		auto z_parent = (TownInfo_Dlg*)msg.parentDlg;
		z_parent->Stop();
	}
	++counter;
	if (counter % 192 == 0) {
		for (int i = 3000; i < 3028; ++i) {
			auto Def = GetDef(i);
			if (Def) {
				int count = Def->GetDef()->groups[1]->count;
				int nextframe = Def->GetFrame() + 1;
				if (nextframe >= count) nextframe = 0;
				Def->SetFrame(nextframe);
			}
		}
		Redraw();
	}
	if (msg.IsRightClick()) {
		if (msg.GetX() - 32 < GetX()) return 0;
		if (msg.GetY() - 32 < GetY()) return 0;
		int i = (msg.GetX()-32 - GetX()) / (736 / 7);
		int j = (msg.GetY()-32 - GetY()) / (536 / 4);
		if (i >= 7 || j >= 4) return 0;
		// char temp[512]; sprintf(temp, "%d %d", i, j);
		// h3::H3Messagebox((LPCSTR)temp);
		int creature_id = j < 2 ? MonstersInTowns_27x2x7[town_id * 14 + i + 7 * j]
			: ThirdUpgradesInTowns_27x2x7[town_id * 14 + i + 7 * (j - 2)];
		if (creature_id >= 0) {
			
			h3::H3Army army; memset(&army, -1, sizeof(army));
			army.AddStack(creature_id,1,0);
			
			THISCALL_9(VOID, 0x4C6910, h3::H3Main::Get(), &army, 
				0, 0, 0, 250, 150, 0, true);
			
			/*
			auto zDlg= CDECL_1(h3::H3Dlg*, 0x00617492, 184);
			THISCALL_11(void*, 0x005F3EF0, zDlg, &army, 0,
				0, -1, 0, 0, -1, 0, 0, 0);
			zDlg->RMB_Show();
			*/
		}
	}

	return 0;
}

extern "C" __declspec(dllexport) 
	void TownInfo_Dlg_Show
	(int color, int town_id, int RMB) 
{
	if (!newTownSelectionDialog)
		return;

	TownInfo_Dlg window(color, town_id);
	if(RMB) window.RMB_Show();
	else window.Start();
}