// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
// #include "../../../__include__/patcher_x86_commented.hpp"
// #include "../../../__include__/H3API/single_header/H3API.hpp"
#include "../../../__include__/H3API/include/patcher_x86.hpp"

Patcher* globalPatcher;
PatcherInstance* Z_Kroniki_VIP;


_LHF_(wyniki_scenariusza) {
    c->ecx /= 5;
    return EXEC_DEFAULT;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    static int once = 1;
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        if (once) {
            MessageBoxA(0, "kroniki", "kroniki", 0);
            globalPatcher = GetPatcher();
            Z_Kroniki_VIP = globalPatcher->CreateInstance((char*)"Z_KRONIKI_VIP");

            Z_Kroniki_VIP->WriteLoHook(0x004E9C14, wyniki_scenariusza);

            once = 0;
        }
        break;
    }
    return TRUE;
}

