// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"

#include "patcher_x86_commented.hpp"
  

Patcher* _P;
PatcherInstance* _PI;


typedef long Dword;
typedef char Byte;


#define DP(X)  (long)X
#define DS(X)  (long)X
#define DS0(X) (long)X

struct _VidInfo_ {
    char* Name;
    Dword Po;
    Byte  Atr[4];
    Dword Add[2];
};

#define VIDNUM_0 141
#define VIDNUM_A 10
_VidInfo_ VidArr[VIDNUM_0 + VIDNUM_A];
_VidInfo_ VidArrAdd[VIDNUM_A] = {
  {(char*)"AZVid000",0x691260,{0,0,0,0},{0,0}},
  {(char*)"AZVid001",0x691260,{0,0,0,0},{0,0}},
  {(char*)"AZVid002",0x691260,{0,0,0,0},{0,0}},
  {(char*)"AZVid003",0x691260,{0,0,0,0},{0,0}},
  {(char*)"AZVid004",0x691260,{0,0,0,0},{0,0}},
  {(char*)"AZVid005",0x691260,{0,0,0,0},{0,0}},
  {(char*)"AZVid006",0x691260,{0,0,0,0},{0,0}},
  {(char*)"AZVid007",0x691260,{0,0,0,0},{0,0}},
  {(char*)"AZVid008",0x691260,{0,0,0,0},{0,0}},
  {(char*)"AZVid009",0x691260,{0,0,0,0},{0,0}}
};

#define _EAX(X) __asm mov X,eax
#define _EBX(X) __asm mov X,ebx
#define _ECX(X) __asm mov X,ecx
#define _EDX(X) __asm mov X,edx
#define _ESI(X) __asm mov X,esi
#define _EDI(X) __asm mov X,edi
#define _EBP(X) __asm mov X,ebp

#define BASE         0x699538

void __stdcall ShowIntro(int y_, int dy_, int dx_, int p1_, int p2_, int p3_)
{
    int smk_; _ECX(smk_);
    int x_; _EDX(x_);
    int ind;
    ////
    switch (smk_) {
        case 22:
            // p3_ = 1;
            smk_ = 140; // 141;
            goto just_smack;
            break;
        /*
        case 33:
            smk_ = 142;
            break;
        */
    }
    ///

    __asm {
        push   p3_
        push   p2_
        push   p1_
        push   dx_
        push   dy_
        push   y_
        mov    edx, x_
        mov    ecx, smk_
        mov    eax, 0x597870
        call   eax
    }
    return;

just_smack:
    __asm {
    push   p3_
    push   p2_
    push   p1_
    push   dx_
    push   dy_
    push   y_
    mov    edx, x_
    mov    ecx, smk_
    mov    eax, 0x598DF0
    call   eax
    }

}


struct __Accessers {
    long  where;
    long  what;
    long  len;
} Accessers[] = {
    // ��������� ������ ����� ��� ����� ������
    ///////////////////////////////////////  {0x4F80FA,DS0(0xB8),1},
    // ����� ������� �����
      {0x44D5C3 + 2,DS0(VidArr),4},
      {0x598FF8 + 2,DS0(VidArr),4},
      {0x44D561 + 2,DS0(&VidArr[0].Po),4},
      {0x598EFD + 2,DS0(&VidArr[0].Po),4},
      {0x5975EB + 2,DS0(&VidArr[0].Atr[0]),4},
      {0x59787C + 3,DS0(&VidArr[0].Atr[0]),4},
      {0x44D873 + 3,DS0(&VidArr[0].Atr[1]),4},
      {0x599288 + 3,DS0(&VidArr[0].Atr[1]),4},
      {0x44D828 + 3,DS0(&VidArr[0].Atr[2]),4},
      {0x44D937 + 3,DS0(&VidArr[0].Atr[2]),4},
      {0x44DBA9 + 3,DS0(&VidArr[0].Atr[2]),4},
      {0x5977E3 + 2,DS0(&VidArr[0].Atr[2]),4},
      {0x599249 + 3,DS0(&VidArr[0].Atr[2]),4},
      {0x59930B + 3,DS0(&VidArr[0].Atr[2]),4},
      {0x44D5BD + 2,DS0(&VidArr[0].Atr[3]),4},
      {0x598EC8 + 3,DS0(&VidArr[0].Atr[3]),4},
      ////

        {0,0}
};

struct __Copiers {
    Byte* from;
    Byte* to;
    long  len;
} Copiers[] = {

  {(Byte*)0x683A10,(Byte*)VidArr,sizeof(_VidInfo_) * VIDNUM_0},
  {(Byte*)&VidArrAdd[0],(Byte*)&VidArr[VIDNUM_0],sizeof(VidArrAdd)},
  ///////////////


    {0,0,0}
};
struct __Callers{
  long  where;
  long  forig;
  long  fnew;
} Callers[] = {

  {0x45E3E6,0,DP(ShowIntro)},
  {0x471574,0,DP(ShowIntro)},
  {0x488B4E,0,DP(ShowIntro)},
  {0x4EE86A,0,DP(ShowIntro)},
  {0x4EE95F,0,DP(ShowIntro)},
  {0x4EEF02,0,DP(ShowIntro)},
  {0x4F013D,0,DP(ShowIntro)},
  {0x4F022B,0,DP(ShowIntro)},
  {0x4F0823,0,DP(ShowIntro)},
  {0x4F08CC,0,DP(ShowIntro)},
  {0x4F097A,0,DP(ShowIntro)},
  {0x4F09EA,0,DP(ShowIntro)},
  {0x4F488B,0,DP(ShowIntro)},
  {0x5D81AC,0,DP(ShowIntro)},
  {0x5D823C,0,DP(ShowIntro)},

  {0,0,0}
};


void Initialize(void)
{
    int   i, j;
    long  del;
    //Byte* s, * d;
    long s, d;

    //InitWoGSetup();
    //UseWogSetup(&DlgSetup);

    for (i = 0;; i++) {
        if (Accessers[i].where == 0) break;
        s = Accessers[i].what;
        d = Accessers[i].where;
        // for (j = 0; j < Accessers[i].len; j++) *d++ = *s++;

        /*
        for (j = 0; j < Accessers[i].len; j++) { 
            //_PI->WriteByte(d,*(Byte*)s);
            // d++; s++; 
        }*/
        _PI->WriteDword(d, s);
    }
    for (i = 0;; i++) {
        if (Copiers[i].from == 0) break;
        j = Copiers[i].len;
        // do { *Copiers[i].to++ = *Copiers[i].from++; --j; } while (j != 0);
        do { *Copiers[i].to++ = *Copiers[i].from++; --j; } while (j != 0);
    }

    for (i = 0;; i++) {
        if (Callers[i].where == 0) break;
        if (Callers[i].fnew == 0) continue; // ���� ����� ���������=0, �� ���������
        del = Callers[i].fnew - Callers[i].where - 5;
        // *(long*)(Callers[i].where + 1) = del;
        _PI->WriteDword(Callers[i].where + 1, del);
    }
}

_LHF_(initialize_hook) {
    Initialize();
    return EXEC_DEFAULT;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    static bool plugin_On = false;
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        if (!plugin_On)
        {
            plugin_On = true;
            _P = GetPatcher();
            _PI = _P->CreateInstance((char*)"HD.Plugin.Chronicles_For_Movies_SMK");
            // _PI->WriteLoHook(0x004F80C6, initialize_hook);
            Initialize();
        }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

