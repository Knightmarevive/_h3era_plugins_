// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
// #include <cstdlib>
#include<shellapi.h>
// #include<processthreadsapi.h>
#include<cstdio>
#include<thread>

#include "patcher_x86_commented.hpp"

Patcher* _P;
PatcherInstance* _PI;

struct action {
    char caller[256];
    char command[256];
    char parameters[16];
};

action actions[] = {
    {"Wojownicy Pustkowi.exe", "Mini_Launcher_Kronik.exe", "as"},
    {"Podb�j Podziemi.exe", "Mini_Launcher_Kronik.exe", "bs"},
    {"W�adcy �ywio��w.exe", "Mini_Launcher_Kronik.exe", "cs"},
    {"Szar�a Smok�w.exe", "Mini_Launcher_Kronik.exe", "ds"},
    {"Starcie Smok�w.exe", "Mini_Launcher_Kronik.exe", "es"},
    {"Drzewo �ycia.exe", "Mini_Launcher_Kronik.exe", "fs"},
    {"Ognisty Ksi�yc.exe", "Mini_Launcher_Kronik.exe", "gs"},
    {"Bunt W�adc�w Bestii.exe", "Mini_Launcher_Kronik.exe", "hs"},
    {"Miecz Mrozu.exe", "Mini_Launcher_Kronik.exe", "is"},
    {"Chwa�a Wojny.exe", "Mini_Launcher_Kronik.exe", "js"},
    {"Wodzowie Pustkowi.exe", "Mini_Launcher_Kronik.exe", "ks"},
    {"Heroes3.exe","Mini_Launcher_Kronik.exe", "zs"},
    {"Heroes3 HD.exe","Mini_Launcher_Kronik.exe", "zs"},
    {"H3blade.exe","Mini_Launcher_Kronik.exe", "zs"},
    {"Warlords of the Wasteland.exe", "Mini_Launcher_Kronik.exe", "as"},
    {"Conquest of the Underworld.exe", "Mini_Launcher_Kronik.exe", "bs"},
    {"Masters of the Elements.exe", "Mini_Launcher_Kronik.exe", "cs"},
    {"Clash of the Dragons.exe", "Mini_Launcher_Kronik.exe", "ds"},
    {"The World Tree.exe", "Mini_Launcher_Kronik.exe", "fs"},
    {"The Fiery Moon.exe", "Mini_Launcher_Kronik.exe", "gs"},
    {"Revolt of the Beastmasters.exe", "Mini_Launcher_Kronik.exe", "hs"},
    {"The Sword of Frost.exe", "Mini_Launcher_Kronik.exe", "is"},
    {"The Glory of War.exe", "Mini_Launcher_Kronik.exe", "js"},
    {"","",""}
};

#pragma warning(disable : 4996)
HWND hwnd = nullptr;
char last_run = 0;
int chosen_action = -1;

void call_launcher(void) {
    int i = chosen_action; if (i < 0) return;
    auto result = ShellExecuteA(hwnd, "open", actions[i].command, actions[i].parameters, "", SW_MINIMIZE);
    if ((int)result <= 32) {
        char code[16]; // _itoa((int)result,code, 10);
        sprintf_s(code, "code %i", (int)result);
        MessageBoxA(0, code, "Failed to Launch Chronicles Launcher", 0);
    }
    //ShellExecuteA(nullptr, nullptr, "Mini_Launcher_Kronik.exe", nullptr, NULL, SW_MAXIMIZE);
    // Sleep(10000);

    Sleep(5000);
}

void prepare_action(void) {
    char name[256]; 
    GetModuleFileNameA(nullptr, name, 256);
    // MessageBoxA(0, name, "debug", 0);
    auto plik=fopen("..\\..\\Kroniki.dat", "rb");
    fseek(plik, 17, SEEK_SET);
    last_run = fgetc(plik);
    fclose(plik);

    for (int i = 0; actions[i].caller[0]; ++i) {
        if (strstr(name, actions[i].caller) != NULL && last_run != actions[i].parameters[0]) {
            // system(actions[i].command);
            chosen_action = i;
            // call_launcher();
        }
    }
}


_LHF_(wait_hook) {
    // Sleep(2000);
    //run_action();
    /// Sleep(5000);
    call_launcher();
    return EXEC_DEFAULT;
}

/*
_LHF_(apply_hook) {
    run_action();
    return EXEC_DEFAULT;
}
*/

void wait_msg(void) {
    MessageBoxA(0, "Please wait a while \n if you don't want to wait run via chronicles launcher next time. \n Press OK now.", "Chroniles Launcher Info", 0);
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    static bool plugin_On = false;
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        if (!plugin_On)
        {
            plugin_On = true;
             _P = GetPatcher();
             _PI = _P->CreateInstance((char*)"HD.Plugin.chronicles_caller");
             prepare_action();
             _PI->WriteLoHook(0x0061A95F, wait_hook);
            // _PI->WriteLoHook(0x004F80C6, wait_hook);
            // auto parent = GetModuleHandleA(nullptr);
            
            // hwnd = GetAncestor(() GetCurrentProcess(),GA_ROOT);
            //hwnd = GetForegroundWindow();// FindWindow();
            //run_action();
             if (chosen_action >= 0) {
                 std::thread t1(wait_msg);
                 //t1.join();
                 t1.detach();

                 //wait_msg();

             }
        }
        break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

