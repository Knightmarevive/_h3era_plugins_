#include<Windows.h>
//#include "..\__include\era.h"
#include "..\__include\heroes.h"
#include<cstdio>

typedef char  undefined1;
typedef short undefined2;
typedef int undefined4;
typedef char undefined;
// #define NULL nullptr
typedef unsigned int uint;
typedef char code;


#include "cheats.h"
#define ttt thecheattext


bool b_jbtvyhingne = false;
bool b_jbtlninaan = false;

extern "C" {

//#define _ 0

	/*
	int FUN_0055a070_H() {
		int FUN_0055a070_ECX;
		__asm {
			CALL FUN_0055a070;
			mov FUN_0055a070_ECX, ECX;
		}
		return FUN_0055a070_ECX;
	} 
	*/
	
	/*
	int FUN_0055a260_H() {
		int FUN_0055a260_ECX;
		__asm {
			CALL FUN_0055a260;
			mov FUN_0055a260_ECX, ECX;
		}
		return FUN_0055a260_ECX;
	}
	*/
	void ExecErmString(char* str) {
		char * here = str;
		char buf[4096] = "";
		while (*here) {
			if (*here == '!') {
				here++;
				if (*here == '!') {
					here++; int index = 0;
					while (*here && *here != ';' && index < 4096) {
						buf[index] = *here;
						here++; index++;
					}
					buf[index] = ';';
					buf[index + 1] = 0;
					if(*here) Era::ExecErmCmd(buf);
					else Era::ExecErmCmd("IF:M^ ExecErmString \n\n Syntax Error \n\n missing semicolon ^;");
				}
			}
			here++;
		}
	}


	bool __fastcall KK_NewCheats(int iParm1) {

		undefined1 *puVar2;
		int iVar3;
		undefined auStack216[200];
		int iStack16;
		int iVar10;
		int current_hero_num;
		int current_hero_ptr;
		int current__MonArr_;
		//int returned_ECX_1;
		//bool debug;

		current_hero_num = *(int *)(*(int*)DAT_0069ccfc + 4);
		if (current_hero_num == -1) {
			iVar10 = 0;
			current_hero_ptr = iVar10;
		}
		else {
			iVar10 = (int)GetHeroRecord(current_hero_num);
			current_hero_ptr = iVar10;
		}
		puVar2 = *(undefined1 **)(iParm1 + 4);
		if (puVar2 == (undefined1 *)0x0) {
			//puVar2 = &DAT_0063a608;
			//puVar2 = (undefined1*) DAT_0063a608;
			MessageBox(0, "chat_problem:NULL", "debug", MB_OK);
			return false;
		}
		iStack16 = iParm1;
		FUN_00402a30((int)auStack216, puVar2);

		if (*puVar2 == '!' && puVar2[1]=='!') { 
			if (b_jbtvyhingne) {
				ExecErmString(puVar2);
				strcpy_s(puVar2, 200, "Yes Sir. Command Executed.");
			} else strcpy_s(puVar2, 200, "Sorry Sir. No Commands.");
			return false;
		}
		if (FUN_006197c0(auStack216, ttt.s_jbtvyhingne) == 0) {
			b_jbtvyhingne = true; return true;
		}
		if (FUN_006197c0(auStack216, ttt.s_jbtzbetbgu) == 0 ) {
			strcpy_s(puVar2, 200, "Sorry Sir. No Commands.");
			b_jbtvyhingne = false; return false;
		}

		if (FUN_006197c0(auStack216, ttt.s_jbtlninaan) == 0) {
			b_jbtlninaan = true; return true;
		}

		if (iVar10 && FUN_006197c0(auStack216, ttt.s_jbtyrtbynf) == 0) {
			void* more_SS_levels = GetModuleHandleA("more_SS_levels.era");
			HERO* Hp = (HERO*)current_hero_ptr;
			int hero_town = Hp->Spec / 2;
			for (int i = 0; i < 28; i++) Hp->SSkill[i]= more_SS_levels ? 15 : 3;
			if (hero_town != 4) Hp->SSkill[12] = 0; // no Necromancy for non necromancers
			// if (hero_town == 3 || hero_town == 4 || hero_town == 5 ) Hp->SSkill[26] = 0; // no Exorcism for bad guys
			FUN_00415d40(*(int*)DAT_006992b8, 1, 1, 1);
			return true;
		}

		if (iVar10 && FUN_006197c0(auStack216, ttt.s_jbttvzyv) == 0) {
			HERO* Hp = (HERO*)current_hero_ptr;
			for (int i = 0; i < 4; i++) Hp->PSkill[i] = 77;
			FUN_00415d40(*(int*)DAT_006992b8, 1, 1, 1);
			return true;
		}


		if (iVar10 && FUN_006197c0(auStack216, ttt.s_jbtjubnzv) == 0) {
			using namespace Era;
			char buf[4096];
			HERO* Hp = (HERO*)current_hero_ptr;
			sprintf_s(buf, 4096, "IF:M^Hero %d:%s Level %d Position %d:%d:%d ^;",current_hero_num,Hp->Name, Hp->ExpLevel, Hp->x, Hp->y, Hp->l);
			ExecErmCmd(buf);
			return false;
		}


		iVar3 = FUN_006197c0(auStack216, ttt.s_jbtznvne);
		if ((iVar3 == 0) && (iVar10 != 0)) {
			int hero_town = ((HERO*)current_hero_ptr)->Spec /2;
			int *piVar8 = (int *)(iVar10 + 0x91);
			current__MonArr_ = (int)piVar8;	
			iVar10 = 0;
			do {
				if (*piVar8 == -1) {
					FUN_0044a9b0(current__MonArr_,150 +hero_town, 19, iVar10);
				}
				iVar10 = iVar10 + 1;
				piVar8 = piVar8 + 1;
			} while (iVar10 < 7);
			FUN_00415d40(*(int*)DAT_006992b8, 1, 1, 1);
			return true;
		}



		iVar3 = FUN_006197c0(auStack216, ttt.s_jbtinyne);
		if ((iVar3 == 0) && (iVar10 != 0)) {
			int hero_town = ((HERO*)current_hero_ptr)->Spec / 2;
			int *piVar8 = (int *)(iVar10 + 0x91);
			current__MonArr_ = (int)piVar8;
			iVar10 = 0;
			do {
				if (*piVar8 == -1) {
					FUN_0044a9b0(current__MonArr_, 243 + hero_town, 7, iVar10);
				}
				iVar10 = iVar10 + 1;
				piVar8 = piVar8 + 1;
			} while (iVar10 < 7);
			FUN_00415d40(*(int*)DAT_006992b8, 1, 1, 1);
			return true;
		}



		return false;
	}

	void  __fastcall  UndefinedFunction_00402450(int iParm1/*, int _*/)
		//void __thiscall UndefinedFunction_00402450(int* iParm1)
		//void __fastcall UndefinedFunction_00402450(int* iParm1)
	{
		char cVar1;
		undefined1 *puVar2;
		int iVar3;
		undefined4 uVar4;
		int iVar5;
		uint uVar6;
		uint uVar7;
		int *piVar8;
		undefined4 *puVar9;
		int iVar10;
		undefined4 *puVar11;
		bool bVar12;
		undefined auStack216[200];
		int iStack16;
		undefined4 uStack12;
		undefined4 uStack8;

		int current_hero_num;
		int current_hero_ptr;
		int current__MonArr_;
		//int returned_ECX_1;
		bool debug;

		current_hero_num = *(int *)(*(int*)DAT_0069ccfc + 4);
		if ( current_hero_num == -1) {
			iVar10 = 0;
			current_hero_ptr = iVar10;
		}
		else {
			//iVar10 = int ((int*)DAT_00699538 + 0x21620 + current_hero_num * 0x492);
			iVar10 = (int)GetHeroRecord(current_hero_num);
			current_hero_ptr = iVar10;
		}
		puVar2 = *(undefined1 **)(iParm1 + 4);
		if (puVar2 == (undefined1 *)0x0) {
			//puVar2 = &DAT_0063a608;
			//puVar2 = (undefined1*) DAT_0063a608;
			MessageBox(0,"chat_problem:NULL","debug",MB_OK);
			return;
		}
		iStack16 = iParm1;
		
		//// done: restored
		//FUN_00402a30(iParm1, puVar2);
		//FUN_00402a30( (int) puVar2, (char*) iParm1 );
		FUN_00402a30( (int) auStack216, puVar2);
		
		//////////////////////////////////
		debug = false;
		if (FUN_006197c0(auStack216, ttt.s_ajpajpajp)==0) {
			return; // disabled
			sprintf_s(ttt.s_msg,200,"puVar2=%X hero=%d@%X arg=%X ",(int)puVar2,current_hero_num,current_hero_ptr,iParm1);
			iVar3= ( IDNO == MessageBox(0,ttt.s_msg,"debug",MB_YESNO) );
			debug = true; if (iVar3 == 0) goto LAB_00402827;
		}
		//////////////////////////////////

		iVar3 = FUN_006197c0(auStack216, ttt.s_ajpgevavgl_0063a480);
		if ((iVar3 == 0) && (iVar10 != 0)) {
			piVar8 = (int *)(iVar10 + 0x91);
			current__MonArr_ = (int) piVar8;
			iVar10 = 0;
			do {
				if (*piVar8 == -1) {
					FUN_0044a9b0(current__MonArr_,/* 0xd */150, 9, iVar10);
				}
				iVar10 = iVar10 + 1;
				piVar8 = piVar8 + 1;
			} while (iVar10 < 7);
		LAB_00402905:

			////todo: remove "return" here
			//return;

			FUN_00415d40( *(int*)DAT_006992b8, 1, 1, 1);


			////todo: remove "return" here
			//return;

		}
		else {
			iVar3 = FUN_006197c0(auStack216, ttt.s_ajpntragf_0063a48c);
			if ((iVar3 == 0) && (iVar10 != 0)) {
				piVar8 = (int *)(iVar10 + 0x91);
				current__MonArr_ = (int) piVar8;
				iVar10 = 0;
				do {
					if (*piVar8 == -1) {
						FUN_0044a9b0(current__MonArr_,  /* 0x42 */ 235, 13, iVar10);
					}
					iVar10 = iVar10 + 1;
					piVar8 = piVar8 + 1;
				} while (iVar10 < 7);

				goto LAB_00402905;
			}
			iVar3 = FUN_006197c0(auStack216, ttt.s_ajpybgfbsthaf_0063a498);
			if ((iVar3 == 0) && (iVar10 != 0)) {
				cVar1 = HasArtifactInBackpack( (void*) current_hero_ptr ,924); //ammo cart
				if (cVar1 == 0) {
					uStack12 = 924;
					uStack8 = 0xffffffff;
					FUN_004e32e0(current_hero_ptr, &uStack12, 0, 0);
					//MessageBox(0, "924", "art", MB_OK);
				}
				cVar1 = HasArtifactInBackpack( (void*)current_hero_ptr,999); //ballista
				if (cVar1 == 0) {
					uStack12 = 999;
					uStack8 = 0xffffffff;
					FUN_004e32e0(current_hero_ptr, &uStack12, 0, 0);
				}
				cVar1 = HasArtifactInBackpack( (void*)current_hero_ptr,849); //the tent
				if (cVar1 == 0) {
					uStack12 = 849;
					uStack8 = 0xffffffff;
					FUN_004e32e0(current_hero_ptr, &uStack12, 0, 0);
				}

				////todo: remove "return" here
				//return;

			}
			else {
				iVar3 = FUN_006197c0(auStack216, ttt.s_ajparb_0063a4a8);
				if ((iVar3 == 0) && (iVar10 != 0)) {
					uVar4 = FUN_004da690(*(uint *)(iVar10 + 0x55));//uVar4 = FUN_004da690();
					FUN_004e3620(iVar10, uVar4, 1, 1);
				}
				else {
					iVar3 = FUN_006197c0(auStack216, ttt.s_ajpsbyybjgurjuvgrenoovg_0063a4b0);
					if ((iVar3 == 0) && (iVar10 != 0)) {
						*(uint *)(iVar10 + 0x105) = *(uint *)(iVar10 + 0x105) | 0x400000;
						goto LAB_00402905;
					}
					iVar3 = FUN_006197c0(auStack216, ttt.s_ajparohpunqarmmne_0063a4c8);
					if ((iVar3 == 0) && (iVar10 != 0)) {
						*(uint *)(iVar10 + 0x105) = *(uint *)(iVar10 + 0x105) | 0x1000000;
						uVar4 = FUN_004e5000(iVar10);//uVar4 = FUN_004e5000();

						*(undefined4 *)(iVar10 + 0x4d) = uVar4;
						*(undefined4 *)(iVar10 + 0x49) = uVar4;
					}
					else {
						iVar3 = FUN_006197c0(auStack216, ttt.s_ajpzbecurhf_0063a4dc);
						if ((iVar3 == 0) && (iVar10 != 0)) {
							*(uint *)(iVar10 + 0x105) = *(uint *)(iVar10 + 0x105) | 0x800000;
							goto LAB_00402905;
						}
						iVar3 = FUN_006197c0(auStack216, ttt.s_ajpbenpyr_0063a4e8);
						if (iVar3 == 0) {
							*(undefined *)((int*)DAT_0069ccfc + 0x38) = 0x30;
							FUN_0041a750( *(int*)DAT_006992b8 );//FUN_0041a750();
						}
						else {
							iVar3 = FUN_006197c0(auStack216, ttt.s_ajpjungvfgurzngevk_0063a4f4);
							if (iVar3 == 0) {

								using namespace Era;
								int tmp2; int tmp3; int tmp4;
								tmp2 = v[2]; tmp3 = v[3]; tmp4 = v[4];
								ExecErmCmd("OW:C?v2;"); 
								ExecErmCmd("UN:X?v3/?v4;"); v[3] *= 2;
								ExecErmCmd("UN:S0/0/0/v2/v3;");
								ExecErmCmd("UN:S0/0/v4/v2/v3;");
								ExecErmCmd("UN:R1;");
								v[2] = tmp2; v[3] = tmp3; v[4] = tmp4;

								goto LAB_00402905;
							}
							else {
								iVar3 = FUN_006197c0(auStack216, ttt.s_ajpvtabenaprvfoyvff_0063a508);
								if (iVar3 == 0) {

									using namespace Era;
									int tmp2; int tmp3; int tmp4;
									tmp2 = v[2]; tmp3 = v[3]; tmp4 = v[4];
									//ExecErmCmd("OW:C?v2;"); v[2] = -1;
									ExecErmCmd("UN:X?v3/?v4;"); v[3] *= 2;
									ExecErmCmd("UN:H0/0/0/-1/v3;");
									ExecErmCmd("UN:H0/0/v4/-1/v3;");
									ExecErmCmd("UN:R1;");
									v[2] = tmp2; v[3] = tmp3; v[4] = tmp4;

									goto LAB_00402905;
								}
								else {
									iVar3 = FUN_006197c0(auStack216, ttt.s_ajpgurpbafgehpg_0063a51c);
									if (iVar3 == 0) {
										iVar3 = 0;
										iVar10 = 0x9c;
										do {
											bVar12 = iVar3 != 6;
											iVar5 = iVar10 + 4;
											iVar3 = iVar3 + 1;
											*(int *)(iVar10 + *(int*) DAT_0069ccfc) = bVar12 ? 777 : 7777777;
												// *(int *)(iVar10 + DAT_0069ccfc) + ( (uint) - (int)bVar12 & 0xfffe79c4) + 100000;
											iVar10 = iVar5;
										} while (iVar5 < 0xb8);

										{
											using namespace Era;
											int tmp2; // int tmp3, tmp4;
											tmp2 = v[2]; // tmp3 = v[3]; tmp4 = v[4];
											ExecErmCmd("OW:C?v2;"); ExecErmCmd("OW:Rv2/7/777;");
											v[2] = tmp2; // v[3] = tmp3; v[4] = tmp4;
										}

										int _arg_ =*(int*)(*(int*) ((*(int*) DAT_006992b8)+0x44)+0x5c);
										FUN_00559170(_arg_, 1, 1);
									}
									else {
										iVar3 = FUN_006197c0(auStack216, ttt.s_ajpoyhrcvyy_0063a52c);
										if ((iVar3 == 0) ||
											(iVar3 = FUN_006197c0(auStack216, ttt.s_ajperqcvyy_0063a538), iVar3 == 0)) {
											FUN_004f3370(0x2);//FUN_004f3370();
										}
										else {
											iVar3 = FUN_006197c0(auStack216, ttt.s_ajpgurervfabfcbba_0063a544);
											if ((iVar3 == 0) && (iVar10 != 0)) {
												
												*(undefined2 *)(iVar10 + 0x18) = 777;
												cVar1 = HasArtifact((void*) (char*) current_hero_ptr, 0);
												if (cVar1 == 0) {
													uStack12 = 0;
													uStack8 = 0xffffffff;
													FUN_004e32e0(current_hero_ptr, &uStack12, 1, 1);
												}
												
												iVar10 = 0;
												do {
													FUN_004d95a0(current_hero_ptr, iVar10);
													iVar10 = iVar10 + 1;
												} while (iVar10 < 0x46);
												goto LAB_00402905;
											}
											iVar10 = FUN_006197c0(auStack216, ttt.s_ajpmvba_0063a558);

											
											if (iVar10 != 0) {
												iVar10 = FUN_006197c0(auStack216, ttt.s_ajpcuvfurecevpr_0063a560);
												if (iVar10 != 0) {
													if (KK_NewCheats(iParm1)) goto LAB_00402827;
													return;
												}

												return; // change_colors is disabled 

												//*(char*)DAT_0069e600 = *(char*) DAT_0069e600 == false;
												*(char*)DAT_0069e600 = ! *(char*) DAT_0069e600;
												
												if (! *(char*)DAT_0069e600) {
													FUN_0055a070();
													//int FUN_0055a070_ret = FUN_0055a070_H();
													FUN_00417380( *(int*) DAT_006992b8 /*FUN_0055a070_ret*/,1,0);
													return;
												}
												
												////todo: restore
												FUN_0055a260();

												//int FUN_0055a260_ret = FUN_0055a260_H();
												//FUN_00417380(1, 0);
												FUN_00417380( *(int*)DAT_006992b8 /* FUN_0055a260_ret*/, 1, 0);
												return;
											}

											//*(char*)DAT_006aaac4 = *(char*)DAT_006aaac4 == false;
											*(char*)DAT_006aaac4 = ! *(char*)DAT_006aaac4;
										}
									}
									goto LAB_00402827;
								}
								iVar3 = 0;
								if ((uint)*(byte *)( *(int*)DAT_00699538 + 0x1fc48) != 0xffffffff) {
									do {

										FUN_0049d040(*(int*)DAT_00699538, 0, 0, iVar3, 0xffffffff, 200);
										iVar3 = iVar3 + 1;
									} while (iVar3 < (int)((uint)*(byte *)( *(int*)DAT_00699538 + 0x1fc48) + 1));
								}
								FUN_004c7910(*(int*)DAT_00699538,0);
							}

							//todo: fix it

	if (debug) { char hlp[200] = "Line:  "; _itoa_s(__LINE__, hlp + 6, 194, 10); MessageBox(0, hlp, "debug", MB_OK); }
							if (iVar10 != 0) {

	if (debug) { char hlp[200] = "Line:  "; _itoa_s(__LINE__, hlp + 6, 194, 10); MessageBox(0, hlp, "debug", MB_OK); }
								//FUN_0040ebe0(0, 0);
								FUN_0040ebe0(*(int*)DAT_006992b8, 0);

	if (debug) { char hlp[200] = "Line:  "; _itoa_s(__LINE__, hlp + 6, 194, 10); MessageBox(0, hlp, "debug", MB_OK); }
							}
							//FUN_00417380(1, 0);
							FUN_00417380(*(int*)DAT_006992b8, 1, 0);

	if(debug){ char hlp[200] = "Line:  "; _itoa_s(__LINE__, hlp + 6, 194, 10); MessageBox(0, hlp, "debug", MB_OK); }
						}
					}
				}
			}
		}

	//after str check
	LAB_00402827:
		if (debug) MessageBox(0, "LAB_00402827", "debug", MB_OK);
		uVar6 = 0xffffffff;
		//puVar9 = *(undefined4 **)(*(int *)( (int*) DAT_006a5dc4 + 0x20) + 0x414);
		FUN_00402a30((int)auStack216, ttt.s_cheater);
		puVar9 = (undefined4*) auStack216;
		puVar11 = puVar9;

//		if (debug) { char hlp[200] = "Line:  "; _itoa_s(__LINE__, hlp + 6, 194, 10); MessageBox(0, hlp, "debug", MB_OK); }
		if (debug) { char hlp[200]; sprintf_s(hlp,200, "Line: %i    puVar9=%X",__LINE__,(uint) puVar9); MessageBox(0, hlp, "debug", MB_OK); }

		//ttt.s_cheater;
		
		do {
			if (uVar6 == 0) break;
			uVar6 = uVar6 - 1;
			cVar1 = *(char *)puVar11;
			puVar11 = (undefined4 *)((int)puVar11 + 1);
		} while (cVar1 != 0);
		

		if (debug) { char hlp[200] = "Line:  "; _itoa_s(__LINE__, hlp + 6, 194, 10); MessageBox(0, hlp, "debug", MB_OK); }
		
		uVar6 = ~uVar6 - 1;
		if (0xfffffffd < uVar6) {
			FUN_0060b0fb();
		}

		if (debug) { char hlp[200] = "Line:  "; _itoa_s(__LINE__, hlp + 6, 194, 10); MessageBox(0, hlp, "debug", MB_OK); }
		
		iVar10 = *(int *)(iStack16 + 4);
		if (((iVar10 == 0) || (cVar1 = *(char *)(iVar10 + -1), cVar1 == 0)) || (cVar1 == -1)) {
		

			if (debug) { char hlp[200] = "Line:  "; _itoa_s(__LINE__, hlp + 6, 194, 10); MessageBox(0, hlp, "debug", MB_OK); }
		
			if (uVar6 == 0) {
				FUN_00404130(iStack16, 1);
				goto LAB_004029f9;
			}
			if ((0x1f < *(uint *)(iStack16 + 0xc)) || (*(uint *)(iStack16 + 0xc) < uVar6)) {
				FUN_00404130(iStack16, 1);
				goto LAB_004029d1;
			}
		}
		else {

			if (debug) { char hlp[200] = "Line:  "; _itoa_s(__LINE__, hlp + 6, 194, 10); MessageBox(0, hlp, "debug", MB_OK); }

			if (uVar6 == 0) {
				*(char *)(iVar10 + -1) = cVar1 + -1;
				*(undefined4 *)(iStack16 + 4) = 0;
				*(undefined4 *)(iStack16 + 8) = 0;
				*(undefined4 *)(iStack16 + 0xc) = 0;
				goto LAB_004029f9;
			}
		LAB_004029d1:

			if (debug) MessageBox(0, "LAB_004029d1", "debug", MB_OK);
			FUN_00404b80(iStack16, uVar6);
		}

		if (debug) { char hlp[200] = "Line:  "; _itoa_s(__LINE__, hlp + 6, 194, 10); MessageBox(0, hlp, "debug", MB_OK); }

		uVar7 = uVar6 >> 2;
		puVar11 = *(undefined4 **)(iStack16 + 4);
		while (uVar7 != 0) {
			uVar7 = uVar7 - 1;
			*puVar11 = *puVar9;
			puVar9 = puVar9 + 1;
			puVar11 = puVar11 + 1;
		}


		if (debug) { char hlp[200] = "Line:  "; _itoa_s(__LINE__, hlp + 6, 194, 10); MessageBox(0, hlp, "debug", MB_OK); }

		uVar7 = uVar6 & 3;
		while (uVar7 != 0) {
			uVar7 = uVar7 - 1;
			*(undefined *)puVar11 = *(undefined *)puVar9;
			puVar9 = (undefined4 *)((int)puVar9 + 1);
			puVar11 = (undefined4 *)((int)puVar11 + 1);
		}
		*(uint *)(iStack16 + 8) = uVar6;
		*(undefined *)(*(int *)(iStack16 + 4) + uVar6) = 0;
	LAB_004029f9:

		if (debug) MessageBox(0, "LAB_004029f9", "debug", MB_OK);

			//// todo: remove this "return"
			//return;

		*(undefined *) (*(int*) DAT_00699538 + 0x1f69c) = 1;
		if (*(char*)DAT_0069779c != 0) {
			*(undefined *) (*(int*)DAT_00699538 + 0x1f458) = 1;
		}
		return;
	}

}