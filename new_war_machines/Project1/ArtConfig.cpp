#include<fstream>
#define DWORD int

extern DWORD New_Machines_Begin;

static void write_it(int id, char* values, char* name, long level) {
	char file_name[256];
	sprintf_s(file_name, "Mods\\Knightmare Kingdoms\\Data\\artifacts\\%i.cfg", id);
	std::ofstream z_file(file_name);
	z_file << "Name=\"" << name << "Level "<< level << "\"\n";
	z_file << "AI_Properties=\"" << values << " -1\"\n";
}

// !!SN:L^Knightmare War Machines.era^/?v3; !!SN:Av3/^ArtConfig^/?v4; !!SN:Ev4/1;
extern "C" __declspec(dllexport) void ArtConfig(void) {
	for (int i = New_Machines_Begin; i < 1000;++i) {
		long type = (i - New_Machines_Begin) / 75;
		long level = (i - New_Machines_Begin) % 75;
		char values[512] = "";

		switch (type)
		{
		case 0: // sage tent
			sprintf(values, "16 %i 6 10 %i", level * 10, (long)sqrt(4 * level));
			write_it(i, values, "Sage Tent", level );
			break;
		case 1: // ammo cart
			sprintf(values, "16 %i 6 9 %i", level * 10, (long) sqrt(3*level));
			write_it(i, values, "Ammo Cart", level);
			break;
		case 2: // balista
			sprintf(values, "16 %i 6 7 %i", level * 10, (long)sqrt(2*level) );
			write_it(i, values, "Ballista", level);
			break;
		default:
			break;
		}
	}
}