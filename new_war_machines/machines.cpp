#include <windows.h>
#include "era.h"

#include "heroes.h"

#include "patcher_x86_commented.hpp"
  
#include "H3CombatMonster.hpp"
typedef H3CombatMonster _BattleStack_;

#include <cstdio>

#define PINSTANCE_MAIN "majaczek_forge"
Patcher * globalPatcher;
PatcherInstance *majaczek_forge;

void (*ChangeCreatureTable)(int target, char* buf);
void (*ConfigureArt)(int target, char* buf);

bool Fun_set_Machine_Cache(HERO* Hp);
void Fun_set_machine_SN_vars(HERO* Hp);

using namespace Era;

//extern void __stdcall _OnAfterCreateWindow_2_(TEvent* Event);

const int ADV_MAP   = 37;
const int CTRL_LMB  = 4;
const int LMB_PUSH  = 12;

//DWORD what_is_patched = 0x46359E;
//DWORD patched_value = 0;
//DWORD patched_before = 0x75E161;

DWORD what_is_patched = 0x75E167;

DWORD New_Machines_Begin = 1000 - 75 * 3; // 775

// DWORD ballista[3], ammo_cart[3], first_aid_tent[3];

extern Byte *GetMPos(Byte *Bm, int Side, int Pos);
extern Byte *PutStack(Byte *Bm, int Type, int Num, int Pos, int Side, HERO *hp, int Placed, int Stack);

#include "PlaceNPC2.h"

struct saveStruct {
	//DWORD ballista[255], ammo_cart[255], first_aid_tent[255];
	DWORD town_charges[255];
};
saveStruct SaveGame;

int* ballista(HERO* Hp) {
	return(&Hp->IArt[13][0]);
}

int* ammo_cart(HERO* Hp) {
	return(&Hp->IArt[14][0]);
}

int* first_aid_tent(HERO* Hp) {
	return(&Hp->IArt[15][0]);
}

//bool in_hero_menu=false;
bool at_external_blacksmith;
long current_hero, current_town;
bool heroes_initialized;

void link_amethyst(void)
{
	//HMODULE Amethyst = LoadLibraryA("Mods\\Knightmare Kingdoms\\eraplugins\\amethyst2.3.dll");
	HMODULE Amethyst = 0;
	//DWORD flags = 0; // GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS;
	//::GetModuleHandleEx(flags, reinterpret_cast<LPCTSTR>("Mods\\Knightmare Kingdoms\\eraplugins\\amethyst2.3.dll"), &Amethyst);
	Amethyst = GetModuleHandleA("amethyst2_4.era"); 
	if (!Amethyst) Amethyst = GetModuleHandleA("amethyst2_4.dll");
	if (!Amethyst) Amethyst = GetModuleHandleA("amethyst2_5.era");
	if (!Amethyst) Amethyst = GetModuleHandleA("amethyst2_5.dll");

	if (Amethyst) {
		ChangeCreatureTable = (void(*)(int, char*)) GetProcAddress(Amethyst, "ChangeCreatureTable");
	} else { MessageBoxA(0,"couldn't link to Amethyst","Knightmare  War Machines",MB_OK ); }
}


void link_emerald(void)
{
	//HMODULE Amethyst = LoadLibraryA("Mods\\Knightmare Kingdoms\\eraplugins\\amethyst2.3.dll");
	HMODULE Emerald = 0;
	//DWORD flags = 0; // GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS;
	//::GetModuleHandleEx(flags, reinterpret_cast<LPCTSTR>("Mods\\Knightmare Kingdoms\\eraplugins\\amethyst2.3.dll"), &Amethyst);
	Emerald = GetModuleHandleA("emerald3_3.era");
	if (!Emerald) Emerald = GetModuleHandleA("emerald3_4.era");
	if (!Emerald) Emerald = GetModuleHandleA("emerald3_5.era");
	if (!Emerald) Emerald = GetModuleHandleA("emerald3_6.era");

	if (Emerald) {
		ConfigureArt = (void(*)(int, char*)) GetProcAddress(Emerald, "ConfigureArt");
	}
	else { MessageBoxA(0, "couldn't link to Emerald", "Knightmare  War Machines", MB_OK); }
}


void Fun_fix_arts_after_upgrade(HERO* Hp);
void AI_wants_buy_machine(void);

void try_heroes_init(void) {
	//return;

	if (!heroes_initialized)
	{
		current_hero = -1;	current_town = -1; at_external_blacksmith = false;
		
		for (long i = 0; i <= 155; i++) // supported heroes
		{
			HERO* Hp = (HERO*)GetHeroRecord(i);

			/*
			char buf[65535];
			sprintf_s(buf, "IF:M^HERO_INIT_%i^",i);
			ExecErmCmd(buf);
			*/

			if (Hp && Hp->Number == i)
				Fun_fix_arts_after_upgrade(Hp);
		} 
		
		heroes_initialized = true;
	}
}


void __stdcall try_heroes_init(TEvent* e) {
	try_heroes_init();
}

long get_upgrade_cost(long artifact_from) {
	if (artifact_from < New_Machines_Begin) return 0;
	if (artifact_from == 999) return 0; // maxed ballista
	if (artifact_from == 924) return 0; // maxed cart
	if (artifact_from == 849) return 0; // maxed Tent
	int tmp3, tmp4, tmp5; tmp3 = v[3]; tmp4 = v[4]; tmp5 = v[5];
	v[5] = artifact_from;
	ExecErmCmd("UN:Av5/1/?v3;");
	(v[5])++;
	ExecErmCmd("UN:Av5/1/?v4;");
	long ret = v[4] - v[3];
	v[3] = tmp3; v[4] = tmp4; v[5] = tmp5;
	return ret;
}

long get_upgrade_cost(HERO* Hp, long artSlot) {
	long artifact_from = Hp->IArt[artSlot][0];
	if (artifact_from >= New_Machines_Begin) return get_upgrade_cost(artifact_from);
	long artifact_to = -1;
	if (artSlot == 13) artifact_to = 926;
	if (artSlot == 14) artifact_to = 851;
	if (artSlot == 15) artifact_to = 776;
	if (artifact_to < 0) return 0;

	int tmp3, tmp4, tmp5; tmp3 = v[3]; tmp4 = v[4]; tmp5 = v[5];
	long ret = 0; v[5] = artifact_to;
	ExecErmCmd("UN:Av5/1/?v3;"); ret = v[3];
	v[3] = tmp3; v[4] = tmp4; v[5] = tmp5;

	return ret;
}

long artifact_to_level(long ART_ID) {
	if (ART_ID < 0) return 0;
	if (ART_ID < New_Machines_Begin) return 0;// 65535;
	if (ART_ID >= 1000) return 0; //return 65535;  
	if (ART_ID <  850)  return ART_ID - 775; //tent
	if (ART_ID >= 925)  return ART_ID - 925; //ballista
	return ART_ID - 850;					 //cart
}

bool is_upgrade_possible(int heroID, int artSlot, int townID) {
	//if (heroID < 0 || townID < 0) return false;

	if (heroID < 0) return false;
	HERO* Hp = (HERO*)GetHeroRecord(heroID);
	if (!Hp) return false;
	long Art = Hp->IArt[artSlot][0];
	
	if (townID >= 0 && SaveGame.town_charges[townID]<=0) return false;
	if (townID <  0 && !at_external_blacksmith) return  false;

	//HERO* Hp = (HERO*)GetHeroRecord(heroID);
	//long Art = Hp->IArt[artSlot][0];
	if (Art < 0) switch (artSlot) {
	case 13:
		Art = 925;
		break;
	case 14:
		Art = 850;
		break;
	case 15:
		Art = 775;
		break;
	}
	if (Hp->ExpLevel <= artifact_to_level(Art)) return false;
	long Cost = get_upgrade_cost(Hp,artSlot);//get_upgrade_cost(Art);
	if (Cost <= 0) return false;
	long own = Hp->Owner;

	int tmp3, tmp4;
	tmp3 = v[3]; tmp4 = v[4];
	v[3] = own; ExecErmCmd("OW:Rv3/6/?v4;");
	bool ret = (v[4] >= Cost);
	v[3] = tmp3; v[4] = tmp4;
	return ret;

}

bool show_upgrade_menu(void); bool repeat = false;
void try_upgrade_machine(int heroID, int artSlot, int townID) {
	if (heroID < 0) return;
	HERO* Hp = (HERO*)GetHeroRecord(heroID);
	Fun_fix_arts_after_upgrade(Hp);
	if (!is_upgrade_possible(heroID, artSlot, townID)) return;
	int& Art = Hp->IArt[artSlot][0];
	long Cost = get_upgrade_cost(Hp, artSlot);// get_upgrade_cost(Art); 
	long own = Hp->Owner; 
	if (Art < 0) switch (artSlot) {
	case 13:
		Art = 925;
		break;
	case 14:
		Art = 850;
		break;
	case 15:
		Art = 775;
		break;
	}

	Art++;
	Fun_set_Machine_Cache(Hp);
	Fun_set_machine_SN_vars(Hp);

	int tmp3, tmp4;
	tmp3  = v[3]; tmp4 = v[4];
	v[3]  = own;  ExecErmCmd("OW:Rv3/6/?v4;");
	v[4] -= Cost; ExecErmCmd("OW:Rv3/6/v4;");
	ExecErmCmd("OW:C?v3;");
	ExecErmCmd("OW:Iv3/?v4;");
	bool human = v[4] == 0;
	v[3]  = tmp3; v[4] = tmp4;
	
	if(townID>=0)
		(SaveGame.town_charges[townID])--;

	//if (human) show_upgrade_menu();
}

//long current_hero, current_town;
bool show_upgrade_menu(void) {
	//ExecErmCmd("IF:M^TODO^;");
	//ExecErmCmd("HE-1:N?v3;"); if (v[3] < 0) return;
	long heroID = current_hero; //v[3];
	if (heroID < 0) return false;
	//current_hero = heroID;
	ExecErmCmd("DL24:N^0024_Blacksmith.txt^;");
	
	HERO* Hp = (HERO*)GetHeroRecord(heroID);

	
	if (Fun_set_Machine_Cache(Hp))
		Fun_set_machine_SN_vars(Hp);

	DWORD& ball = *ballista(Hp);// SaveGame.ballista[heroID];
	DWORD& tent = *first_aid_tent(Hp);//SaveGame.first_aid_tent[heroID];
	DWORD& cart = *ammo_cart(Hp);//SaveGame.ammo_cart[heroID];

	ExecErmCmd("UN:P904/1;");
	if (ball >= New_Machines_Begin) {
		v[3] = ball;
		ExecErmCmd("DL24:A1013/4/v3;");
		if (v[3] != 999) v[3] = ball + 1;
		ExecErmCmd("DL24:A2013/4/v3;");
	} {
		v[3] = get_upgrade_cost(Hp, 13);// get_upgrade_cost(ball);
		ExecErmCmd("VRz3:S^%V3^;");
		ExecErmCmd("DL24:A4013/3/z3;");
	}
	if (tent >= New_Machines_Begin) {
		v[3] = tent;     
		ExecErmCmd("DL24:A1015/4/v3;");
		if (v[3] !=849) v[3] = tent + 1;
		ExecErmCmd("DL24:A2015/4/v3;");
	} {
		v[3] = get_upgrade_cost(Hp, 15);//get_upgrade_cost(tent);
		ExecErmCmd("VRz3:S^%V3^;");
		ExecErmCmd("DL24:A4015/3/z3;");
	}
	if (cart >= New_Machines_Begin) {
		v[3] = cart;     
		ExecErmCmd("DL24:A1014/4/v3;");
		if (v[3] != 924) v[3] = cart + 1;
		ExecErmCmd("DL24:A2014/4/v3;");
	} {
		v[3] = get_upgrade_cost(Hp, 14);//get_upgrade_cost(cart);
		ExecErmCmd("VRz3:S^%V3^;");
		ExecErmCmd("DL24:A4014/3/z3;");
	}

	ExecErmCmd("UN:P905/0;");
	ExecErmCmd("UN:P904/0;");
	ExecErmCmd("DL24:S;");

	return repeat;
}


void __stdcall _Blacksmith_Dialog_(TEvent* e) {
	if(v[998]!=24)return;
	if (v[999] == 30721) { repeat = false;  ExecErmCmd("DL:C1;"); } // repeat = true;
	if (v[999] == 3013 && v[1000] == 13) try_upgrade_machine(current_hero, 13, current_town);
	if (v[999] == 3014 && v[1000] == 13) try_upgrade_machine(current_hero, 14, current_town);
	if (v[999] == 3015 && v[1000] == 13) try_upgrade_machine(current_hero, 15, current_town);
	if (v[999] == 3013 || v[999] == 3014 || v[999] == 3015) if(v[1000] == 13) {
		ExecErmCmd("DL:C1;");
		if (current_town >= 0) ExecErmCmd("UN:R4;"); 
		// show_upgrade_menu();
	}

}

void __stdcall _Town_Click_(TEvent* e) {

	int tmp3, tmp4, tmp5; tmp3 = v[3]; tmp4 = v[4]; tmp5 = v[5];
	ExecErmCmd("CM:I?v3;");
	ExecErmCmd("CM:F?v4;");
	//ExecErmCmd("HE-1:N?v5;");
	ExecErmCmd("CA-1:U?v5;");
	current_town = v[5];
	ExecErmCmd("CA-1:H1/?v5;");
	current_hero = v[5];

	repeat = true;

	if (v[3] == 16 /* && v[4]==512 */) {
		ExecErmCmd("CM:R0;");
		while(	show_upgrade_menu() );
		//ExecErmCmd("CM:R1;");
	}

	v[3] = tmp3; v[4] = tmp4; v[5] = tmp5;
}

void __stdcall _HERO_open_(TEvent* e) {
	//in_hero_menu = true;
}
void __stdcall _HERO_close_(TEvent* e) {
	//in_hero_menu = false;
	return;

	int tmp3, tmp4, tmp5; tmp3 = v[3]; tmp4 = v[4]; tmp5 = v[5];
	ExecErmCmd("HE-1:N?v4;");
	
	if(v[4]>=0){
		HERO* Hp = (HERO*)GetHeroRecord((int)v[4]);
		if (Fun_set_Machine_Cache(Hp))
			Fun_set_machine_SN_vars(Hp);

	}
	v[3] = tmp3; v[4] = tmp4; v[5] = tmp5;
}


void __stdcall _OnAfterHeroInteraction_(TEvent* e) {
	//in_hero_menu = false;

	int tmp3, tmp4, tmp5; tmp3 = v[3]; tmp4 = v[4]; tmp5 = v[5];
	ExecErmCmd("SN:X?v3/?v4;");

	if (v[3] >= 0) {
		HERO* Hp = (HERO*)GetHeroRecord((int)v[3]);

		if (Fun_set_Machine_Cache(Hp))
			Fun_set_machine_SN_vars(Hp);

	}

	if (v[4] >= 0) {
		HERO* Hp = (HERO*)GetHeroRecord((int)v[4]);

		if (Fun_set_Machine_Cache(Hp))
			Fun_set_machine_SN_vars(Hp);

	}

	v[3] = tmp3; v[4] = tmp4; v[5] = tmp5;
}

void Fun_fix_arts_after_upgrade(HERO* Hp) {
	if (!Hp) return;

	//int tmp2 = v[2]; int tmp3 = v[3];int tmp4 = v[4];
	//v[2] = Hp->Number; v[3] = 1;

	//==================================



	if (4 == Hp->IArt[13][0] || 926 == Hp->IArt[13][0]) //ballista
	{
		Hp->IArt[13][0] = 926;
		//SaveGame.ballista[Hp->Number] = 926;
	}	//else if (0>Hp->IArt[13][0]) SaveGame.ballista[Hp->Number] = 925;

	if (5 == Hp->IArt[14][0] || 851 == Hp->IArt[14][0]) //ammo cart
	{
		Hp->IArt[14][0] = 851;
		//SaveGame.ammo_cart[Hp->Number] = 851;
	}	//else if (0>Hp->IArt[14][0]) SaveGame.ammo_cart[Hp->Number] = 850;

	if (6 == Hp->IArt[15][0] || 776 == Hp->IArt[15][0]) //tent
	{
		Hp->IArt[15][0] = 776;
		//SaveGame.first_aid_tent[Hp->Number] = 776;
	}	//else if (0>Hp->IArt[15][0]) SaveGame.first_aid_tent[Hp->Number] = 775;


	//==================================


	// v[2] = tmp2; v[3] = tmp3; v[4] = tmp4;

}

/*
void Fun_get_Machine_Cache(HERO* Hp) {

	if (!Hp) return;

	int tmp2, tmp1;
	tmp2 = v[2];
	tmp1 = v[1];

	v[2] = Hp->Number;


	ExecErmCmd("SN:W^HE%V2 Has Ballista^/?v1;");
	if (v[1]>0 && Hp->IArt[13][0] >= 0 && Hp->IArt[13][0] != 144 && Hp->IArt[13][0] != 145 ) {
		//SaveGame.ballista[v[2]] = v[1] + 925;
		Hp->IArt[13][0] = (SaveGame.ballista[v[2]]);

	}
	else SaveGame.ballista[v[2]] = -1;


	ExecErmCmd("SN:W^HE%V2 Has Ammo Cart^/?v1;");
	if (v[1]>0  && Hp->IArt[14][0] >= 0 && Hp->IArt[14][0] != 144 && Hp->IArt[14][0] != 145 ) {
		//SaveGame.ammo_cart[v[2]] = v[1] + 850;
		Hp->IArt[14][0] = (SaveGame.ammo_cart[v[2]]);

	}
	else SaveGame.ammo_cart[v[2]] = -1;


	ExecErmCmd("SN:W^HE%V2 Has First Aid Tent^/?v1;");
	if (v[1]>0  && Hp->IArt[15][0] >= 0 && Hp->IArt[15][0] != 144 && Hp->IArt[15][0] != 145 ) {
		//SaveGame.first_aid_tent[v[2]] = v[1] + 775;
		Hp->IArt[15][0] = (SaveGame.first_aid_tent[v[2]]);

	}
	else SaveGame.first_aid_tent[v[2]] = -1;


	v[2] = tmp2;
	v[1] = tmp1;
}
*/

/*
void Fun_get_machine_SN_vars(HERO* Hp) {

	if (!Hp) return;

	int tmp2, tmp1;
	tmp2 = v[2]; 
	tmp1 = v[1];

	v[2] = Hp->Number;  

	
	ExecErmCmd("SN:W^HE%V2 Has Ballista^/?v1;");
	if (v[1]>0 && Hp->IArt[13][0] >=0 && Hp->IArt[13][0] != 144 && Hp->IArt[13][0] != 145) {
		SaveGame.ballista[v[2]] = v[1] + 925;
		//Hp->IArt[13][0] = (SaveGame.ballista[v[2]]);
		
	}
	else SaveGame.ballista[v[2]] = -1;


	ExecErmCmd("SN:W^HE%V2 Has Ammo Cart^/?v1;");
	if (v[1]>0 && Hp->IArt[14][0] >= 0 && Hp->IArt[14][0] != 144 && Hp->IArt[14][0] != 145) {
		SaveGame.ammo_cart[v[2]] = v[1] + 850;
		//Hp->IArt[14][0] = (SaveGame.ammo_cart[v[2]]);
		
	}
	else SaveGame.ammo_cart[v[2]] = -1;

		
	ExecErmCmd("SN:W^HE%V2 Has First Aid Tent^/?v1;");	
	if (v[1]>0 && Hp->IArt[15][0] >= 0 && Hp->IArt[15][0] != 144 && Hp->IArt[15][0] != 145) {
		SaveGame.first_aid_tent[v[2]] = v[1] + 775;
		//Hp->IArt[15][0] = (SaveGame.first_aid_tent[v[2]]);
		
	}
	else SaveGame.first_aid_tent[v[2]] = -1;


	v[2] = tmp2 ;
	v[1] = tmp1;
}
*/
bool Fun_set_Machine_Cache(HERO* Hp)
{

	if (!Hp) {
		ExecErmCmd("IF:M^Error: Fun_set_Machine_Cache - Invalid Hero^;");
		return false;
	}

	bool ret = false;
	//Fun_fix_Machine_arts(Hp);
	auto heronum = Hp->Number;
	return true;

	/*

	if (New_Machines_Begin <= Hp->IArt[13][0] && Hp->IArt[13][0] != SaveGame.ballista[heronum]) {
		(SaveGame.ballista[heronum]) = Hp->IArt[13][0];
		ret = true;
	}
	if (Hp->IArt[13][0] < 0 && (SaveGame.ballista[heronum]) != 925)
		{ (SaveGame.ballista[heronum]) = 925; ret = true; }


	if (New_Machines_Begin <= Hp->IArt[14][0] && Hp->IArt[14][0] != SaveGame.ammo_cart[heronum]){
		(SaveGame.ammo_cart[heronum]) = Hp->IArt[14][0];
		ret = true;
	}
	if (Hp->IArt[14][0] < 0 && (SaveGame.ammo_cart[heronum]) != 850)
		{ (SaveGame.ammo_cart[heronum]) = 850; ret = true; }


	if (New_Machines_Begin <= Hp->IArt[15][0] && Hp->IArt[15][0] != SaveGame.first_aid_tent[heronum]){
		(SaveGame.first_aid_tent[heronum]) = Hp->IArt[15][0];
		ret = true;
	}
	if (Hp->IArt[15][0] < 0 && (SaveGame.first_aid_tent[heronum]) != 775)
		{ (SaveGame.first_aid_tent[heronum]) = 775;  ret = true; }

	return ret;

	*/
}


void Fun_set_machine_SN_vars(HERO* Hp) {

	if (!Hp) return;

	int tmp2, tmp1;
	tmp2 = v[2];
	tmp1 = v[1];

	//(SaveGame.ballista[Hp->Number]) = Hp->IArt[13][0];
	//(SaveGame.ammo_cart[Hp->Number]) = Hp->IArt[14][0];
	//(SaveGame.first_aid_tent[Hp->Number]) = Hp->IArt[15][0];
	
	/*
	if (SaveGame.ballista[Hp->Number] == 4 || SaveGame.ballista[Hp->Number] == 925) {
		SaveGame.ballista[Hp->Number] = 926;
		v[2] = Hp->Number;  
		ExecErmCmd("SN:W^HE%V2 Has Ballista^/1;");
	}
	if (SaveGame.ammo_cart[Hp->Number] == 5 || SaveGame.ammo_cart[Hp->Number] == 850) {
		SaveGame.ammo_cart[Hp->Number] = 851;
		v[2] = Hp->Number;
		ExecErmCmd("SN:W^HE%V2 Has Ammo Cart^/1;");

	}
	if (SaveGame.first_aid_tent[Hp->Number] == 6 || SaveGame.first_aid_tent[Hp->Number] == 775) {
		SaveGame.first_aid_tent[Hp->Number] = 776;
		v[2] = Hp->Number;
		ExecErmCmd("SN:W^HE%V2 Has First Aid Tent^/1;");

	}
	*/


	if (*ballista(Hp) < 1000 && *ballista(Hp)>925) {
	//if (SaveGame.ballista[Hp->Number] < 1000 && SaveGame.ballista[Hp->Number]>925) {
		v[2] = Hp->Number;
		v[1] = /* SaveGame.ballista[v[2]] */ *ballista(Hp) - 925;
		ExecErmCmd("SN:W^HE%V2 Has Ballista^/v1;");
	} else ExecErmCmd("SN:W^HE%V2 Has Ballista^/0;");

	if (*ammo_cart(Hp)<925 && *ammo_cart(Hp)>850) {
	//if (SaveGame.ammo_cart[Hp->Number]<925 && SaveGame.ammo_cart[Hp->Number]>850) {
		v[2] = Hp->Number;
		v[1] = /*SaveGame.ammo_cart[v[2]]*/ *ammo_cart(Hp) - 850;
		ExecErmCmd("SN:W^HE%V2 Has Ammo Cart^/v1;");
	} else ExecErmCmd("SN:W^HE%V2 Has Ammo Cart^/0;");

	if (*first_aid_tent(Hp)<850 && *first_aid_tent(Hp)>775) {
	//if (SaveGame.first_aid_tent[Hp->Number]<850 && SaveGame.first_aid_tent[Hp->Number]>775) {
		v[2] = Hp->Number;
		v[1] = /*SaveGame.first_aid_tent[v[2]]*/ *first_aid_tent(Hp) - 775;
		ExecErmCmd("SN:W^HE%V2 Has First Aid Tent^/v1;");
	} else ExecErmCmd("SN:W^HE%V2 Has First Aid Tent^/0;");


	v[2] = tmp2;
	v[1] = tmp1;

}

// War Machines Level including bonuses
long double att_ball = 1; long double att_cart = 1; long double att_tent = 1;
long double def_ball = 1; long double def_cart = 1; long double def_tent = 1;

long double  def_cart_count = 0; long double att_cart_count = 0;

BOOL __stdcall Hook_BattleMachines_BeforeNPC(THookContext* Context)
{
	//DWORD ballista, ammo_cart, first_aid_tent;
	
	Byte* Bm; int Side; int zPlaced;
	Bm = (char*) Context->EBX;
	Side = Context->EDI;
	
	//zPlaced = Context->ESI;
	//int *Placed = &zPlaced;
	int *Placed = &(Context->ESI);

	//(*Placed)++;

	HERO *Hp; Hp = (HERO *)*(Dword *)&Bm[0x53CC + Side * 4];
	/*
	if (Hp == nullptr) {
		ballista[Hp->Number] = 0;
		ammo_cart[Hp->Number] = 0;
		first_aid_tent[Hp->Number] = 0;

		return EXEC_DEF_CODE;
	}
	*/
	if (Hp == nullptr) {
	
		return EXEC_DEF_CODE;
	}
	/*
	Fun_get_Machine_Cache(Hp);

	Fun_set_machine_SN_vars(Hp);
	*/ // commented for debug

	Byte* mon; int i, pos, num, tp, is_placed; // why it is here ?????

	int tmp2 = v[2]; int tmp3 = v[3];
	v[2] = Hp->Number;
	ExecErmCmd("HEv2:B2/?v3;");
	int HeroTownType = v[3] / 2;
	v[2] = tmp2; v[3] = tmp3;


	if (/*ballista[Side] == 4 ||*/
		//SaveGame.ballista[Hp->Number] >= New_Machines_Begin

		//artifact_to_level(SaveGame.ballista[Hp->Number]) >0
		artifact_to_level(*ballista(Hp)) >0


		// || HeroTownType == 6
		)
	{
		is_placed = 0; num = 1; 
		/*
		for (i = 0;i<21;i++) {
			mon = &Bm[0x54CC + 0x548 * (i + Side * 21)];
			tp = *(int *)&mon[0x34];
			if (tp == 146) { 
				// // *(int *)&mon[0x60] += num; 
				// // *(int *)&mon[0x4C] += num; 
				is_placed = 1; break; }
		}
		*/
		if (is_placed == 0) {
			if (Side == 0) pos = 51; else pos = 67; // 3.57m2
			int attacker[] = { 305,305,305,305,305,305,305,305,305,146,146,146,146,146,146 };
			int defender[] = { 316,316,316,316,316,316,316,316,316,146,146,146,146,146,146 };
			int *current = Side ? defender : attacker;

			PutStack(Bm, /*146*/ current[HeroTownType], num, pos, Side, Hp, *Placed, -1); ++*Placed;
		}
	}

	if (/* ammo_cart[Side] == 5 || */
		// SaveGame.ammo_cart[Hp->Number] >= New_Machines_Begin
		
		//artifact_to_level(SaveGame.ammo_cart[Hp->Number]) >0 
		artifact_to_level(*ammo_cart(Hp)) >0

		// || HeroTownType==2 
		)
	{
		is_placed = 0; num = 1;
		
		for (i = 0;i<21;i++) {
			mon = &Bm[0x54CC + 0x548 * (i + Side * 21)];
			tp = *(int *)&mon[0x34];
			if (/* tp == 148 */ isMonsterAmmoCart(tp)) {
				// // *(int *)&mon[0x60] += num; 
				// // *(int *)&mon[0x4C] += num; 
				is_placed = 1; break;
			}
		}
		
		if (is_placed == 0) {
			if (Side == 0) pos = 18; else pos = 32; // 3.57m2

			int attacker[] = { 330,330,330,330,330,330,330,330,330,330,330,330,330,330,330 };
			int defender[] = { 331,331,331,331,331,331,331,331,331,331,331,331,331,331,331 };
			int* current = Side ? defender : attacker;
			// char buf[1024]; long my_cart = (Side ? def_cart : att_cart);

			// sprintf_s(buf, "HP=%d \n", 250 + 100 * my_cart );
			// ChangeCreatureTable(148, buf);

			PutStack(Bm, /* 148 */ current[HeroTownType], num, pos, Side, Hp, *Placed, -1); ++*Placed;
		}

	}

	if (/*first_aid_tent[Side] == 6 ||*/
		// SaveGame.first_aid_tent[Hp->Number] >= New_Machines_Begin

		//artifact_to_level(SaveGame.first_aid_tent[Hp->Number]) >0 
		artifact_to_level(*first_aid_tent(Hp)) >0
		
		// || HeroTownType == 1
		)
	{
		is_placed = 0; num = 1;
		/*
		for (i = 0;i<21;i++) {
			mon = &Bm[0x54CC + 0x548 * (i + Side * 21)];
			tp = *(int *)&mon[0x34];
			if (tp == 147) { 
				// // *(int *)&mon[0x60] += num; 
				// // *(int *)&mon[0x4C] += num; 
				is_placed = 1; break; }
		}
		*/
		if (is_placed == 0) {
			int attacker[] = {313,308,307,306,303,309,310,311,312,147,147,147,147,147,147 };
			int defender[] = {324,319,318,317,314,320,321,322,323,147,147,147,147,147,147 };
			int *current = Side ? defender : attacker;
			if (Side == 0) pos = 153; else pos = 169; // 3.57m2
			PutStack(Bm, /*147*/ current[HeroTownType] , num, pos, Side, Hp, *Placed, -1); ++*Placed;
		}

	}

	//ExecErmCmd("IF:L^{~gold}This is a battle hint!{~}^;");
	//ExecErmCmd("FU");
	return EXEC_DEF_CODE;
}

void __stdcall _OnAfterCreateWindow_ (TEvent* Event)
{
	ApiHook((void*)Hook_BattleMachines_BeforeNPC,
		HOOKTYPE_BRIDGE, (void*)what_is_patched);
	ApiHook((void*)__PlaceNPC2__HOOK2,
		HOOKTYPE_BRIDGE, (void*)PlaceNPC2_what_is_patched);
	ApiHook((void*)__PlaceNPC2__HOOK3,
		HOOKTYPE_BRIDGE, (void*)PlaceNPC2_middle_2);

}

void __stdcall  _Artifact_Unequip_(TEvent* e) {
	//if (!in_hero_menu) return;

	int tmp4 = v[4];
	
	ExecErmCmd("HE-1:N?v4;");
	HERO* Hp = (HERO*) GetHeroRecord ( (int) v[4]);
	
	if (!Hp) { v[4] = tmp4; return; }
	

	if (Fun_set_Machine_Cache(Hp))
		Fun_set_machine_SN_vars(Hp);

	//Fun_set_Machine_Cache(Hp);

//	Fun_fix_Machine_arts(Hp, false); 
	//Fun_get_Machine_Cache(Hp);
	//Fun_set_machine_SN_vars(Hp); 
	//Fun_get_machine_SN_vars(Hp);
	//Fun_set_Machine_Cache(Hp);
	//Fun_get_Hero_Variables_from_Cache(Hp);

//	Fun_fix_Machine_arts(Hp, true);
	v[4] = tmp4;

	//Fun_set_machine_SN_vars(Hp);
}

void __stdcall  _Artifact_Equip_(TEvent* e) {
	//if (!in_hero_menu) return;

	int tmp4 = v[4];

	ExecErmCmd("HE-1:N?v4;");
	HERO* Hp = (HERO*)GetHeroRecord((int)v[4]);

	if (!Hp) { v[4] = tmp4; return; }


	//Fun_set_Machine_Cache(Hp);
//	Fun_get_Hero_Variables_from_Cache(Hp);
//	Fun_get_machine_SN_vars(Hp);

	if (Fun_set_Machine_Cache(Hp))
		Fun_set_machine_SN_vars(Hp);

	v[4] = tmp4;

	//Fun_set_machine_SN_vars(Hp);
}


void __stdcall  _HERO_Action_(TEvent* e) {
}
void __stdcall  _HERO_Move_(TEvent* e) {
	at_external_blacksmith = false;
	current_town = -1;
	//try_heroes_init();
	return;

	int tmp3, tmp4, tmp5; tmp3 = v[3]; tmp4 = v[4]; tmp5 = v[5];
	ExecErmCmd("HE-1:N?v4;");

	if (v[4] >= 0) {
		HERO* Hp = (HERO*)GetHeroRecord((int)v[4]);
		if (Fun_set_Machine_Cache(Hp))
			Fun_set_machine_SN_vars(Hp);

	}
	v[3] = tmp3; v[4] = tmp4; v[5] = tmp5;
}

void __stdcall  _Town_PreEnter_(TEvent* e) {
	int tmp3, tmp4, tmp5; tmp3 = v[3]; tmp4 = v[4]; tmp5 = v[5];
	
	//ExecErmCmd("IF:M^0^;"); //debug

	current_town = x[1];
	ExecErmCmd("CA0/x1:H1/?v4;");
	current_hero = v[4];
	
	if (v[4] >= 0) {
		HERO* Hp = (HERO*)GetHeroRecord((int)v[4]);
		
		AI_wants_buy_machine();

		if (Fun_set_Machine_Cache(Hp))
			Fun_set_machine_SN_vars(Hp);



	}

	v[3] = tmp3; v[4] = tmp4; v[5] = tmp5;
}

void __stdcall  _Town_Action_(TEvent* e) {

	//ExecErmCmd("IF:M^1^;"); //debug

	/*
	for (int i = 0;i <= 155;++i) {
		HERO* Hp = (HERO*)GetHeroRecord(i);
		if (!Hp) continue;
		//Fun_fix_Machine_arts(Hp,false);
		Fun_fix_arts_after_upgrade(Hp);
		Fun_set_Machine_Cache(Hp);
		Fun_set_machine_SN_vars(Hp);
	}
	*/

	int tmp3, tmp4, tmp5; tmp3 = v[3]; tmp4 = v[4]; tmp5 = v[5];
	//ExecErmCmd("HE-1:N?v4;");
	ExecErmCmd("CAv998/v999/v1000:U?v5;");
	current_town = v[5];
	ExecErmCmd("CAv998/v999/v1000:H1/?v4;");
	current_hero = v[4];

	if (v[4] >= 0) {
		HERO* Hp = (HERO*)GetHeroRecord((int)v[4]);

		//current_hero = v[4];
		//current_town = v[5];

		AI_wants_buy_machine();

		if(Fun_set_Machine_Cache(Hp))
			Fun_set_machine_SN_vars(Hp);


		
	}
	v[3] = tmp3; v[4] = tmp4; v[5] = tmp5;
}


void __stdcall  _Adventure_Blacksmith_Action_(TEvent* e) {

	//at_external_blacksmith = true;
	/*
	for (int i = 0; i <= 155; ++i) {
		HERO* Hp = (HERO*)GetHeroRecord(i);
		if (!Hp) continue;
		//Fun_fix_Machine_arts(Hp,false);
		Fun_fix_arts_after_upgrade(Hp);
		Fun_set_Machine_Cache(Hp);
	}
	*/

	AI_wants_buy_machine();

	int tmp3, tmp4, tmp5; tmp3 = v[3]; tmp4 = v[4]; tmp5 = v[5];
	ExecErmCmd("HE-1:N?v4;");

	if (v[4] >= 0) {
		HERO* Hp = (HERO*)GetHeroRecord((int)v[4]);

		if(Fun_set_Machine_Cache(Hp))
			Fun_set_machine_SN_vars(Hp);
	}
	v[3] = tmp3; v[4] = tmp4; v[5] = tmp5;
}

void __stdcall  _Adventure_Blacksmith_Action_Before_(TEvent* e) {
	at_external_blacksmith = true;	current_town = -1; long tmp;
	tmp = v[4]; ExecErmCmd("HE-1:N?v4;");
	current_hero = v[4]; v[4] = tmp;
	repeat = true;
	//if (f[1000]) show_upgrade_menu();
	_Adventure_Blacksmith_Action_(e);
	ExecErmCmd("OBv998/v999/v1000:S;");

	if (f[1000]) while (show_upgrade_menu());
}

void __stdcall  _Adventure_Blacksmith_Action_After_(TEvent* e) {
	_Adventure_Blacksmith_Action_(e);
	at_external_blacksmith = false;
	current_town = -1;
	ExecErmCmd("OBv998/v999/v1000:R;");
}
/*
void __stdcall  _HEROES_Interaction_(TEvent* e) {


	for (int i = 0;i <= 155;++i) {
		HERO* Hp = (HERO*)GetHeroRecord(i);
		if (!Hp) continue;

		Fun_set_Machine_Cache(Hp);
		//Fun_set_machine_SN_vars(Hp);
		//Fun_get_machine_SN_vars(Hp);
	}

}
*/

// bool heroes_initialized = false;
void __stdcall _Scenario_INIT_(TEvent* e) {
	heroes_initialized = false;


	for (long i = 0; i < 255; i++) {
		SaveGame.town_charges[i] = 15;
		
		//SaveGame.ballista[i] = 925;
		//SaveGame.ammo_cart[i] = 850;
		//SaveGame.first_aid_tent[i] = 775;
		
		/*
		if(i<=155) // supported heroes
			Fun_fix_arts_after_upgrade((HERO*)GetHeroRecord(i));
		*/
	}
	//ExecErmCmd("DL24:N^0024_Blacksmith.txt^;");
	at_external_blacksmith = false; current_town = -1;


}

/*
void __stdcall _HERO_INIT_(TEvent* e) {

	if (heroes_initialized) return;
	in_hero_menu = false;
	for (long i = 0; i <= 155; i++) {
		HERO* Hp = (HERO*)GetHeroRecord(i);
		if (!Hp) continue;
		//Fun_set_machine_SN_vars(Hp);
		//SaveGame.ammo_cart[i] = 144;
		SaveGame.ammo_cart[i] = Hp->IArt[14][0];
		SaveGame.first_aid_tent[i] = Hp->IArt[15][0];
		SaveGame.ballista[i] = Hp->IArt[13][0];
	}
	heroes_initialized = true;
}
*/

/*
void __stdcall _HERO_inside_town_(TEvent* e) {
	_HERO_Action_(e);
}
*/

void __stdcall StoreData(TEvent* e)
{
	//Save.ready = false;
	WriteSavegameSection(sizeof(SaveGame), (void*)&SaveGame, "Knightmare War Machines");
}


void __stdcall RestoreData(TEvent* e)
{
	//in_hero_menu = false;
	//CleanTomes();
	ReadSavegameSection(sizeof(SaveGame), (void*)&SaveGame, "Knightmare War Machines");
	//Save.AI_turn = 0;

	//ExecErmCmd("DL24:N^0024_Blacksmith.txt^;");
	at_external_blacksmith = false; current_town = -1; current_hero = -1;
	//heroes_initialized = true;
}


void __stdcall _DAILY_(TEvent* e) {
	at_external_blacksmith = false;
	current_hero = -1;
	current_town = -1;

	try_heroes_init();

	for (long i = 0; i < 255; i++) 
		if(SaveGame.town_charges[i]<7)
			SaveGame.town_charges[i] = 7;
	
	//at_external_blacksmith = false;
	//current_hero = -1;
	//current_town = -1;
}


void __stdcall _TM99_(TEvent* e) {

	//try_heroes_init();
}

int __stdcall AIMgr_Stack_SetHexes_WayToMoveLength_Hook(HiHook* h, void* z_this, _BattleStack_* monster, int side, signed int gex, signed int tactics, signed int speedMayBe0, int speed) {
	if (monster->info.flags.SIEGE_WEAPON) {
		speedMayBe0 = true; speed = 0;
	}
	int ret = CALL_7(int, __thiscall, h->GetDefaultFunc(), z_this, monster,side, gex, tactics, speedMayBe0, speed);
	return ret;
}

int __stdcall BattleStack_GetGexID_OnAttackType_Hook(HiHook* h, _BattleStack_* mon, signed int gex, int action_type) {

	//if (action_type == 7) return -1; // debug line

	//int ret = CALL_3(int, __thiscall, h->GetDefaultFunc(), mon,gex, action_type);


	if (mon->info.flags.SIEGE_WEAPON) {
		//mon->info.flags.DONE = 1;
		//mon->info.flags.DEFENDING = 1;
		//mon->info.flags.CANNOT_MOVE = 1;
		//return gex;
		//return gex;

		//ExecErmCmd("IF:L^SIEGE_WEAPON detected^"); //test okay

		//return CALL_3(int, __thiscall, h->GetDefaultFunc(), mon, gex, 12);
	}

	int ret = CALL_3(int, __thiscall, h->GetDefaultFunc(), mon, gex, action_type);

	return ret;
}

int __stdcall ForgeArtifact(HiHook* h, HERO* hero, int art)
{
	bool ai_machine_upgrade = (art == 4 || art == 5 || art == 6);
	//if (art == 4) art = 926;
	//if (art == 5) art = 851;
	//if (art == 6) art = 776;
	int ret = CALL_2(int, __thiscall, h->GetDefaultFunc(), hero, art);
	if (ai_machine_upgrade /* || art == 0 || art >= New_Machines_Begin */) {


		/*

		if(heroes_initialized)		{
			AI_wants_buy_machine();
			Fun_fix_arts_after_upgrade(hero);
			if (Fun_set_Machine_Cache(hero))
				Fun_set_machine_SN_vars(hero);
		}

		*/
		// if (art == 0) return ret;
		return 1;
	}
	//if (ai_machine_upgrade) return 1;
	return  ret;
}

void __stdcall _DoNotGenerateSpecialMonsters_(TEvent* e) {
	link_amethyst();
	if (!ChangeCreatureTable) return;

	for (int i = 294; i <= 324; ++i)
		ChangeCreatureTable(i, "DoNotGenerate=1 \n");
	ChangeCreatureTable(330, "DoNotGenerate=1 \n");
	ChangeCreatureTable(331, "DoNotGenerate=1 \n");
	ChangeCreatureTable(340, "DoNotGenerate=1 \n");
}

void __stdcall _checkAmethystLinkage_(TEvent* e)
{
	link_amethyst();
	if (!ChangeCreatureTable) ExecErmCmd("IF:M^Couldn't link to Amethyst^;");
}


void __stdcall _checkEmeraldLinkage_(TEvent* e)
{
	link_emerald();
	if (!ConfigureArt) ExecErmCmd("IF:M^Couldn't link to Emerald^;");
}

/*
void __stdcall _prepareCreatures_(TEvent* e) {
	int tmp2 = v[2]; int tmp3 = v[3]; int tmp4 = v[4]; int tmp5 = v[5]; int tmp6 = v[6]; int tmp7 = v[7]; int tmp8 = v[8];

	//ExecErmCmd("UN:P904/1;");
	v[2] = -99; ExecErmCmd("HE-10:N?v2;"); // Attacker
	ExecErmCmd("UN:P904/1;");
	v[3] = -99; ExecErmCmd("HE-20:N?v3;"); // Defender
	ExecErmCmd("UN:P905/0;");
	ExecErmCmd("UN:P904/0;");

	att_ball = 1; att_cart = 1; att_tent = 1;
	def_ball = 1; def_cart = 1; def_tent = 1;
	if (v[2] >= 0) {
		ExecErmCmd("HEv2:S27/?v4;");
		ExecErmCmd("HEv2:X?v5/?v6/?v8/?v8/?v8/?v8/?v8;");
		ExecErmCmd("HEv2:B2/?v7;"); v[7] /= 2;
		if (v[5] == 0 && v[6] == 27) {
			v[4] *= 2; v[4]++;
		}

		HERO* Hp = (HERO*) GetHeroRecord(v[2]);
		
		att_ball = 2 * v[4] + (v[7] == 6) + artifact_to_level(*ballista(Hp));
		att_cart = 3 * v[4] + (v[7] == 2) + artifact_to_level(*ammo_cart(Hp));
		att_tent = 2 * v[4] + (v[7] == 1) + artifact_to_level(*first_aid_tent(Hp));
	}
	if (v[3] >= 0) {
		ExecErmCmd("HEv3:S27/?v4;");
		ExecErmCmd("HEv3:X?v5/?v6/?v8/?v8/?v8/?v8/?v8;");
		ExecErmCmd("HEv3:B2/?v7;"); v[7] /= 2;
		if (v[5] == 0 && v[6] == 27) {
			v[4] *= 2; v[4]++;
		}

		HERO* Hp = (HERO*) GetHeroRecord(v[3]);

		def_ball = 2 * v[4] + (v[7] == 6) + artifact_to_level(*ballista(Hp));
		def_cart = 3 * v[4] + (v[7] == 2) + artifact_to_level(*ammo_cart(Hp));
		def_tent = 2 * v[4] + (v[7] == 1) + artifact_to_level(*first_aid_tent(Hp));
	}
	char buf[4096];
	
	sprintf_s(buf, "HP=%d \n DamageLow=%d \n DamageHigh=%d \n", 500 + 100 * att_ball, 20 * att_ball, 40 * att_ball);
	ChangeCreatureTable(305, buf);

	sprintf_s(buf, "HP=%d \n DamageLow=%d \n DamageHigh=%d \n", 500 + 100 * def_ball, 20 * def_ball, 40 * def_ball);
	ChangeCreatureTable(315, buf);

	sprintf_s(buf, "HP=%d \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d \n CreatureSpellPowerAdder=3 \n DamageLow=%d \n DamageHigh=%d \n Shots=%d \n", 500 + 100 * att_tent, 2 + 2 * att_tent, 3, 20 * att_tent, 30 * att_tent, att_tent + 1);
	ChangeCreatureTable(306, buf); ChangeCreatureTable(307, buf); ChangeCreatureTable(308, buf);
	ChangeCreatureTable(309, buf); ChangeCreatureTable(310, buf); ChangeCreatureTable(311, buf);
	ChangeCreatureTable(312, buf); ChangeCreatureTable(313, buf); ChangeCreatureTable(303, buf);

	sprintf_s(buf, "HP=%d \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d \n CreatureSpellPowerAdder=3 \n DamageLow=%d \n DamageHigh=%d \n Shots=%d \n", 500 + 100 * def_tent, 2 + 2 * def_tent, 3, 20 * def_tent, 30 * def_tent, def_tent + 1);
	ChangeCreatureTable(317, buf); ChangeCreatureTable(318, buf); ChangeCreatureTable(319, buf);
	ChangeCreatureTable(320, buf); ChangeCreatureTable(321, buf); ChangeCreatureTable(322, buf);
	ChangeCreatureTable(323, buf); ChangeCreatureTable(324, buf); ChangeCreatureTable(314, buf);


	sprintf_s(buf, "Spell 1=0 \n Spell 2=0 \n Spell 3=0  \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d \n",
		4 + 2 * att_tent, 5);	ChangeCreatureTable(308, buf); //rampart
	sprintf_s(buf, "Spell 1=0 \n Spell 2=0 \n Spell 3=0  \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d \n",
		4 + 2 * att_tent, 5);	ChangeCreatureTable(313, buf); //castle
	sprintf_s(buf, "Spell 1=0 \n Spell 2=9 \n Spell 3=0  \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d \n",
		4 + 2 * att_tent, 5);	ChangeCreatureTable(303, buf); //necro

	sprintf_s(buf, "Spell 1=0 \n Spell 2=0 \n Spell 3=0  \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d \n",
		4 + 2 * def_tent, 5);	ChangeCreatureTable(319, buf); //rampart
	sprintf_s(buf, "Spell 1=0 \n Spell 2=0 \n Spell 3=0  \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d \n",
		4+2*def_tent,5);	ChangeCreatureTable(324, buf); //castle
	sprintf_s(buf, "Spell 1=0 \n Spell 2=9 \n Spell 3=0  \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d \n",
		4 + 2 * def_tent, 5);	ChangeCreatureTable(314, buf); //necro


	sprintf_s(buf, " \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d \n isEnchanter=1 \n ",
		4 + 3 * att_tent, 1);	ChangeCreatureTable(307, buf); //tower
	sprintf_s(buf, " \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d \n isEnchanter=1 \n ",
		4 + 3 * def_tent, 1);	ChangeCreatureTable(318, buf); //tower

	sprintf_s(buf, "isSorceress=1 \n SpellsCostLess=%d", 1 + att_tent/7);
	ChangeCreatureTable(309, buf); //dungeon
	sprintf_s(buf, "isSorceress=1 \n SpellsCostLess=%d", 1 + def_tent/7);
	ChangeCreatureTable(320, buf); //dungeon

	sprintf_s(buf, "Spell 1=0 \n Spell 2=0 \n Spell 3=0  \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d Spells=%d \n ",
		4 + 3 * att_tent, 1, att_tent/5+1);	ChangeCreatureTable(306, buf); //inferno
	sprintf_s(buf, "Spell 1=0 \n Spell 2=0 \n Spell 3=0  \n CreatureSpellPowerMultiplier=%d \n CreatureSpellPowerDivider=%d Spells=%d \n ",
		4 + 3 * def_tent, 1, def_tent/5+1);	ChangeCreatureTable(317, buf); //inferno


	sprintf_s(buf, "MagicMirror=1");
	ChangeCreatureTable(311, buf); //fortress
	sprintf_s(buf, "MagicMirror=1");
	ChangeCreatureTable(322, buf); //fortress


	sprintf_s(buf, "SpellsCostDump=%d", 1 + att_tent / 5);
	ChangeCreatureTable(310, buf); //stronghold
	sprintf_s(buf, "SpellsCostDump=%d", 1 + def_tent / 5);
	ChangeCreatureTable(321, buf); //stronghold


	sprintf_s(buf, "Spell 1=8 \n Spell 2=7 \n Spell 3=2 \n"); // damage spell
	//ChangeCreatureTable(318, buf); ChangeCreatureTable(307, buf);
	//ChangeCreatureTable(320, buf); ChangeCreatureTable(309, buf);

	sprintf_s(buf, "Spell 1=8 \n Spell 2=7 \n Spell 3=2 \n"); // buff spell

	ChangeCreatureTable(320, buf); ChangeCreatureTable(309, buf);
	ChangeCreatureTable(318, buf); ChangeCreatureTable(307, buf);
	ChangeCreatureTable(317, buf); ChangeCreatureTable(306, buf);
	ChangeCreatureTable(321, buf); ChangeCreatureTable(310, buf);
	ChangeCreatureTable(322, buf); ChangeCreatureTable(311, buf);
	ChangeCreatureTable(323, buf); ChangeCreatureTable(312, buf);

	sprintf_s(buf, "isPassive=1 \n isAmmoCart=1 \n HP=%d \n", 250 + 100 * att_cart); ChangeCreatureTable(330, buf);
	sprintf_s(buf, "isPassive=1 \n isAmmoCart=1 \n HP=%d \n", 250 + 100 * def_cart); ChangeCreatureTable(331, buf);

	v[2] = tmp2; v[3] = tmp3; v[4] = tmp4; v[5] = tmp5; v[6] = tmp6; v[7] = tmp7; v[8] = tmp8;
}
*/

void __stdcall _prepareCreatures_Attacker(HERO* Hp);
void __stdcall _prepareCreatures_Attacker(TEvent* e) {

	/*
	//ExecErmCmd("UN:P904/1;");
	//v[2] = -99;
	ExecErmCmd("SN:W^Machines_Attacker_hero^/?v2;");
	if (v[2] < 0) ExecErmCmd("HE-10:N?v2;"); // Attacker
	//ExecErmCmd("UN:P904/1;");

	if (v[2] >= 0) ExecErmCmd("SN:W^Machines_Attacker_hero^/v2;");
	*/
	// ExecErmCmd("SN:W^Machines_Attacker_hero^/?v9992;");

	//ExecErmCmd("UN:P905/0;");
	//ExecErmCmd("UN:P904/0;");



	if (v[9992] >= 0) {

		HERO* Hp = (HERO*)GetHeroRecord(v[9992]);
		_prepareCreatures_Attacker(Hp);
	}

}


void __stdcall _prepareCreatures_Attacker(HERO* Hp){
	att_ball = 1; att_cart = 1; att_tent = 1;
	// def_ball = 1; def_cart = 1; def_tent = 1;
	char buf[4096];
	if (Hp) {
		/*
		ExecErmCmd("HEv9992:S27/?v9984;");
		ExecErmCmd("HEv9992:X?v9985/?v9986/?v8/?v8/?v8/?v8/?v8;");
		ExecErmCmd("HEv9992:B2/?v9987;"); v[9987] /= 2;
		//ExecErmCmd("IP:V9987/9987;");
		if (v[9985] == 0 && v[9986] == 27) {
			v[9984] *= 2; v[9984]++;
		} 
		*/
		int machinery = Hp->SSkill[27];// ExecErmCmd("HEv9993:S27/?v9994;");

		int spec0 = HeroSpecialty()[Hp->Number].type;
		int spec1 = HeroSpecialty()[Hp->Number].bonusID;
		// ExecErmCmd("HEv9992:X?v9995/?v9996/?v8/?v8/?v8/?v8/?v8;");

		int faction = Hp->Spec / 2;
		// ExecErmCmd("HEv9992:B2/?v9997;"); v[9997] /= 2;

		/*
		//ExecErmCmd("IP:V9997/9997;");
		if (v[9995] == 0 && v[9996] == 27) {
			v[9994] *= 2; v[9994]++;
		}
		*/

		if (spec0 == 0 && spec1 == 27) {
			machinery *= 2; ++machinery;
		}
		//ExecErmCmd("IP:V9984/9984;");

		// HERO* Hp = (HERO*)GetHeroRecord(v[9992]);
		/*
		att_ball = 2* v[4] + (v[7] == 6) + artifact_to_level(SaveGame.ballista[v[2]]);
		att_cart = 3* v[4] + (v[7] == 2) + artifact_to_level(SaveGame.ammo_cart[v[2]]);
		att_tent = 2* v[4] + (v[7] == 1) + artifact_to_level(SaveGame.first_aid_tent[v[2]]);
		*/
		/*
		att_ball = 2 * v[9984] + (v[9987] == 6) + artifact_to_level(*ballista(Hp));
		att_cart = 3 * v[9984] + (v[9987] == 2) + artifact_to_level(*ammo_cart(Hp));
		att_tent = 2 * v[9984] + (v[9987] == 1) + artifact_to_level(*first_aid_tent(Hp));
		*/
		att_ball = 0.75 * machinery + (faction / 2 == 6) + artifact_to_level(*ballista(Hp));
		att_cart = 1.25 * machinery + (faction / 2 == 2) + artifact_to_level(*ammo_cart(Hp));
		att_tent = 0.75 * machinery + (faction / 2 == 1) + artifact_to_level(*first_aid_tent(Hp));

		long tmp_v5 = v[5];
		sprintf_s(buf, "CO%d:T?v5;", Hp->Number);
		ExecErmCmd(buf);
		if (v[5] == 2 || v[5] == 8) {
			sprintf_s(buf, "CO%d:X2/?v5;", Hp->Number);
			ExecErmCmd(buf); long npc_Level = v[5];
			att_cart_count = powf(npc_Level * 3.0f, 0.4);// v[5];
		} else att_cart_count = 1;
		v[5] = tmp_v5;

	}


	sprintf_s(buf, "HP=%d \n DamageLow=%d \n DamageHigh=%d \n Attack=%d \n Defence=%d \n ", 
		long (500 + 100 * att_ball), long(20 * att_ball), long(40 * att_ball),
		long(att_ball), long(att_ball)
	);
	ChangeCreatureTable(305, buf);

	sprintf_s(buf, "HP=%d \n CreatureSpellPowerMultiplier=%d \n DamageLow=%d \n DamageHigh=%d \n Shots=%d \n MeleeResistance=%f \n ShootingResistance=%f \n Attack=%d \n Defence=%d \n ",
		long(500 + 100 * att_tent), long(2 + att_tent*0.75), long(15 * att_tent), long(25 * att_tent), long(att_tent + 1), 
		float(0.01 * att_tent + 0.15), float(0.01 * att_tent + 0.15),
		long(att_tent), long(att_tent)
	);
	ChangeCreatureTable(306, buf); ChangeCreatureTable(307, buf); ChangeCreatureTable(308, buf);
	ChangeCreatureTable(309, buf); ChangeCreatureTable(310, buf); ChangeCreatureTable(311, buf);
	ChangeCreatureTable(312, buf); ChangeCreatureTable(313, buf); ChangeCreatureTable(303, buf);

	ChangeCreatureTable(308, buf); //rampart
	ChangeCreatureTable(313, buf); //castle
	ChangeCreatureTable(303, buf); //necro
	ChangeCreatureTable(307, buf); //tower

	sprintf_s(buf, " CreatureSpellPowerDivider=12 \n CreatureSpellPowerAdder=1 \n");
	ChangeCreatureTable(306, buf); // Inferno sage tent nerf

	sprintf_s(buf, "isSorceress=1 \n SpellsCostLess=%d", long(1 + att_tent / 7));
	ChangeCreatureTable(309, buf); //dungeon

	sprintf_s(buf, "Spells=%d \n ", long(att_tent / 5 + 1));	ChangeCreatureTable(306, buf); //inferno



	sprintf_s(buf, "MagicMirror=1");
	ChangeCreatureTable(311, buf); //fortress

	sprintf_s(buf, "SpellsCostDump=%d", long(1 + att_tent / 5));
	ChangeCreatureTable(310, buf); //stronghold


	/*
	sprintf_s(buf, "Spell 1=8 \n Spell 2=7 \n Spell 3=1 \n"); // buff spell

	 ChangeCreatureTable(309, buf);
	ChangeCreatureTable(307, buf);
	ChangeCreatureTable(306, buf);
	ChangeCreatureTable(310, buf);
	ChangeCreatureTable(311, buf);
	
	sprintf_s(buf, "Spell 1=11 \n Spell 2=2 \n Spell 3=1 \n"); // summon spell
	ChangeCreatureTable(312, buf);
	*/

	sprintf_s(buf, "isPassive=1 \n isAmmoCart=1 \n HP=%d \n ManaRegen=%d \n Defence=%d \n ",
		long(250 + 100 * att_cart), int((att_cart *att_cart_count)/ 7 + 2), long(att_cart)); 
	ChangeCreatureTable(330, buf);
}

void	_prepareCreatures_Defender(HERO* Hp);
void __stdcall _prepareCreatures_Defender(TEvent* e) {
	//ExecErmCmd("UN:P904/1;");
	//v[2] = -99; ExecErmCmd("HE-10:N?v2;"); // Attacker
	//ExecErmCmd("UN:P904/1;");
	/*
	//v[3] = -99;
	ExecErmCmd("SN:W^Machines_Defender_hero^/?v3;");
	if (v[3] < 0)	ExecErmCmd("HE-20:N?v3;"); // Defender
	//ExecErmCmd("UN:P905/0;");
	//ExecErmCmd("UN:P904/0;");

	if (v[3]>=0) ExecErmCmd("SN:W^Machines_Defender_hero^/v3;");
	*/
	//ExecErmCmd("SN:W^Machines_Defender_hero^/?v9993;");

	//att_ball = 1; att_cart = 1; att_tent = 1;
	//def_ball = 1; def_cart = 1; def_tent = 1;

	if (v[9993] >= 0) {

		HERO* Hp = (HERO*)GetHeroRecord(v[9993]);
		_prepareCreatures_Defender(Hp);
	}

}
void	_prepareCreatures_Defender(HERO * Hp)
{

	def_ball = 1; def_cart = 1; def_tent = 1;
	char buf[4096];
	if (Hp) {
		int machinery = Hp->SSkill[27];// ExecErmCmd("HEv9993:S27/?v9994;");

		int spec0 = HeroSpecialty()[Hp->Number].type;
		int spec1 = HeroSpecialty()[Hp->Number].bonusID;
		// ExecErmCmd("HEv9993:X?v9995/?v9996/?v8/?v8/?v8/?v8/?v8;");
		
		int faction = Hp->Spec / 2;
		// ExecErmCmd("HEv9993:B2/?v9997;"); v[9997] /= 2;
		
		/*
		//ExecErmCmd("IP:V9997/9997;");
		if (v[9995] == 0 && v[9996] == 27) {
			v[9994] *= 2; v[9994]++;
		}
		*/

		if (spec0 == 0 && spec1 == 27) {
			machinery *= 2; ++machinery;
		}

		//ExecErmCmd("IP:V9994/9994;");

		// HERO* Hp = (HERO*)GetHeroRecord(v[9993]);
		/*
		def_ball = 2* v[4] + (v[7] == 6) + artifact_to_level(SaveGame.ballista[v[3]]);
		def_cart = 3* v[4] + (v[7] == 2) + artifact_to_level(SaveGame.ammo_cart[v[3]]);
		def_tent = 2* v[4] + (v[7] == 1) + artifact_to_level(SaveGame.first_aid_tent[v[3]]);
		*/
		/*
		def_ball = 2 * v[9994] + (v[9997] == 6) + artifact_to_level(*ballista(Hp));
		def_cart = 3 * v[9994] + (v[9997] == 2) + artifact_to_level(*ammo_cart(Hp));
		def_tent = 2 * v[9994] + (v[9997] == 1) + artifact_to_level(*first_aid_tent(Hp));
		*/
		def_ball = 0.75 * machinery + (faction == 6) + artifact_to_level(*ballista(Hp));
		def_cart = 1.25 * machinery + (faction == 2) + artifact_to_level(*ammo_cart(Hp));
		def_tent = 0.75 * machinery + (faction == 1) + artifact_to_level(*first_aid_tent(Hp));

		long tmp_v5 = v[5];
		sprintf_s(buf, "CO%d:T?v5;", Hp->Number);
		ExecErmCmd(buf);
		if (v[5] == 2 || v[5] == 8) {
			sprintf_s(buf, "CO%d:X2/?v5;", Hp->Number);
			ExecErmCmd(buf); long npc_Level = v[5];
			def_cart_count = powf(npc_Level * 3.0f, 0.4);// v[5];
		}
		else def_cart_count = 1;
		v[5] = tmp_v5;
	}


	sprintf_s(buf, "HP=%d \n DamageLow=%d \n DamageHigh=%d \n Attack=%d \n Defence=%d \n ",
		long(500 + 100 * def_ball), long(20 * def_ball), long(40 * def_ball),
		long(def_ball), long(def_ball)
	);
	ChangeCreatureTable(316, buf);

	sprintf_s(buf, "HP=%d \n CreatureSpellPowerMultiplier=%d \n DamageLow=%d \n DamageHigh=%d \n Shots=%d  \n MeleeResistance=%f \n ShootingResistance=%f \n Attack=%d \n Defence=%d \n ", 
		long(500 + 100 * def_tent), long(2 + def_tent*0.75), long(15 * def_tent), long(25 * def_tent), long(def_tent + 1),
		float(0.01 * def_tent + 0.15), float(0.01 * def_tent + 0.15),
		long(def_tent), long(def_tent)
	);
	ChangeCreatureTable(317, buf); ChangeCreatureTable(318, buf); ChangeCreatureTable(319, buf);
	ChangeCreatureTable(320, buf); ChangeCreatureTable(321, buf); ChangeCreatureTable(322, buf);
	ChangeCreatureTable(323, buf); ChangeCreatureTable(324, buf); ChangeCreatureTable(314, buf);

	ChangeCreatureTable(319, buf); //rampart
	ChangeCreatureTable(324, buf); //castle
	ChangeCreatureTable(314, buf); //necro
	ChangeCreatureTable(318, buf); //tower

	sprintf_s(buf, "isSorceress=1 \n SpellsCostLess=%d", long(1 + def_tent / 7));
	ChangeCreatureTable(320, buf); //dungeon

	sprintf_s(buf, "Spells=%d \n ", long(def_tent / 5 + 1));	
	ChangeCreatureTable(317, buf); //inferno


	sprintf_s(buf, "MagicMirror=1");
	ChangeCreatureTable(322, buf); //fortress

	sprintf_s(buf, "SpellsCostDump=%d", long(1 + def_tent / 5));
	ChangeCreatureTable(321, buf); //stronghold

	/*
	sprintf_s(buf, "Spell 1=8 \n Spell 2=7 \n Spell 3=1 \n"); // buff spell

	ChangeCreatureTable(320, buf); 
	ChangeCreatureTable(318, buf); 
	ChangeCreatureTable(317, buf); 
	ChangeCreatureTable(321, buf); 
	ChangeCreatureTable(322, buf); 

	sprintf_s(buf, "Spell 1=11 \n Spell 2=2 \n Spell 3=1 \n"); // summon spell
	ChangeCreatureTable(323, buf); 
	*/

	sprintf_s(buf, " CreatureSpellPowerDivider=12 \n CreatureSpellPowerAdder=1 \n");
	ChangeCreatureTable(317, buf); // Inferno sage tent nerf

	sprintf_s(buf, "isPassive=1 \n isAmmoCart=1 \n HP=%d \n ManaRegen=%d Defence=%d \n ", 
		long(250 + 100 * def_cart), int((def_cart * def_cart_count) / 7 + 2), long(def_cart));
	ChangeCreatureTable(331, buf);

}

void __stdcall _prepareCreatures_Both_sides_(TEvent* e) {
	_prepareCreatures_Attacker(e);
	_prepareCreatures_Defender(e);
}

void __stdcall _ResetHeroVars_Both_sides_(TEvent* e) {

	//ExecErmCmd("SN:W^Machines_Defender_hero^/-99;");
	//ExecErmCmd("SN:W^Machines_Attacker_hero^/-99;");

	ExecErmCmd("VRv9992:S-99;");
	ExecErmCmd("IP:V9992/9992;");

	ExecErmCmd("VRv9993:S-99;");
	ExecErmCmd("IP:V9993/9993;");
}


void __stdcall _PrepareHeroVars_Both_sides_(TEvent* e) {


	ExecErmCmd("HE-10:N?v9992;"); // Attacker
	//ExecErmCmd("IP:V9992/9992;");

	ExecErmCmd("SN:W^Machines_Attacker_hero^/v9992;");

	ExecErmCmd("VRv9993:S-99;");
	ExecErmCmd("UN:P904/1;");
	ExecErmCmd("HE-20:N?v9993;"); // Defender
	//ExecErmCmd("IP:V9993/9993;");

	ExecErmCmd("UN:P905/0;");
	ExecErmCmd("UN:P904/0;");

	ExecErmCmd("SN:W^Machines_Defender_hero^/v9993;");
}

void AI_wants_buy_machine(void) {
	//ExecErmCmd("IF:M^0^;");

	if (!heroes_initialized) return;	 //ExecErmCmd("IF:M^1^;");
	if (f[1000]) return;				 //ExecErmCmd("IF:M^2^;");
	if (current_hero < 0) return;		 //ExecErmCmd("IF:M^3^;");
	//if (current_town<0 && !at_external_blacksmith) return;
	
	/*
	for (int i = 0; i < 17; ++i ) {
		auto tmp3 = v[3]; auto tmp4 = v[4];
		ExecErmCmd("VRv3:S13 R2;");
		auto body_position = v[3];
		ExecErmCmd("VRv4:S0 R1000;");
		auto maybe_wants = v[4];
		v[3] = tmp3; v[4] = tmp4;

		if (maybe_wants < 127)
			try_upgrade_machine(current_hero, body_position, current_town);
	}
	*/

	ExecErmCmd("OW:R-1/6/?v3;"); if (v[3] >= 32000) {
		try_upgrade_machine(current_hero, 13, current_town);
		try_upgrade_machine(current_hero, 14, current_town);
		try_upgrade_machine(current_hero, 15, current_town);
		//ExecErmCmd("IF:M^gold %V3 wants to buy a machine^;");
	}

	ExecErmCmd("OW:R-1/6/?v3;"); if (v[3] >= 25000) {
		try_upgrade_machine(current_hero, 13, current_town);
		try_upgrade_machine(current_hero, 14, current_town);
		try_upgrade_machine(current_hero, 15, current_town);
		//ExecErmCmd("IF:M^gold %V3 wants to buy a machine^;");
	}

	ExecErmCmd("OW:R-1/6/?v3;"); if (v[3] >= 16000) {
		try_upgrade_machine(current_hero, 13, current_town);
		try_upgrade_machine(current_hero, 14, current_town);
		try_upgrade_machine(current_hero, 15, current_town);
		//ExecErmCmd("IF:M^gold %V3 wants to buy a machine^;");
	}
}

void __stdcall _Preset_AI_heroes_(TEvent* e) {
	ExecErmCmd("VRv2:Sc;"); if (v[2] > 1) return;
	ExecErmCmd("OW:C?v2;"); ExecErmCmd("OW:Iv2/?v3;"); if (v[3] == 0) return;
	//current_town = -1;
	for (int i = 0; i <= 155; ++i) if (HERO* h = (HERO*)GetHeroRecord(i))
		if (h->Owner == v[2]) for (int j = 0;j<5;++j) {
			//current_hero = i;

			at_external_blacksmith = true;
			try_upgrade_machine(i, 13, -1);
			try_upgrade_machine(i, 14, -1);
			try_upgrade_machine(i, 15, -1);
		}
}
void __stdcall change_war_machine_description(TEvent* e) {

	// return; //2020-04-10

	/*
	char buf[2048] = "";
	for (int i = 775; i < 1000; i++) {
		sprintf(buf, "UN:A%i/9/0", i); ExecErmCmd(buf);
		sprintf(buf, "UN:A%i/10/0", i); ExecErmCmd(buf);
	}
	*/
	for (int i = 1;i<75;i++) {
		int cart = 850+i; int ballista =925+i; int tent = 775 + i; char buf[2048] = "zzz";

		sprintf(buf, "Name=\"Ammo Cart level {%i} \" Description=\"Ammo Cart level {%i} \n \n Regeneates the mana during battle. \" ", i, i);								ConfigureArt(cart, buf);
		sprintf(buf, "Name=\"Ballista level {%i} \" Description=\"Ballista level {%i} \n \n Shoots units in a Battle. \n Destroy walls in the Siege. \" ", i, i);			ConfigureArt(ballista, buf);
		sprintf(buf, "Name=\"Sage Tent level {%i} \" Description=\"Sage Tent level {%i} \n \n Does diferent things based on Hero Class. It also can Shoot. \" ", i, i);		ConfigureArt(tent, buf);
	}

}

void fix_zero_level_machines(HERO* h) {
	if (!h) return;
	if (!artifact_to_level(*ammo_cart(h)))		*ammo_cart(h) = -1;
	if (!artifact_to_level(*ballista(h)))		*ballista(h) = -1;
	if (!artifact_to_level(*first_aid_tent(h)))	*first_aid_tent(h) = -1;
}

int __stdcall  hook_change_machines_creatures(LoHook* h, HookContext* c) {
	//// hooked at 0x00463833
	//_PrepareHeroVars_Both_sides_(nullptr);
	//_prepareCreatures_Both_sides_(nullptr);

	fix_zero_level_machines((HERO*)*(int*)(c->ebp + 0x0c));
	fix_zero_level_machines((HERO*)*(int*)(c->ebp + 0x1c));

	//------------------------------------------------------------
	_prepareCreatures_Attacker((HERO*) * (int*)(c->ebp + 0x0c));
	_prepareCreatures_Defender((HERO*) * (int*)(c->ebp + 0x1c));

	return EXEC_DEFAULT;
}

extern "C" __declspec(dllexport) BOOL APIENTRY DllMain (HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
  if (reason == DLL_PROCESS_ATTACH)
  {

	  globalPatcher = GetPatcher();
	  majaczek_forge = globalPatcher->CreateInstance(PINSTANCE_MAIN);
    ConnectEra();
    //RegisterHandler(OnAdventureMapLeftMouseClick, "OnAdventureMapLeftMouseClick");
    //ApiHook((void*) Hook_BattleMouseHint, HOOKTYPE_BRIDGE, (void*) 0x74fd1e);

	RegisterHandler(_DoNotGenerateSpecialMonsters_, "OnAfterCreateWindow");
	RegisterHandler(_OnAfterCreateWindow_, "OnAfterCreateWindow");
	//RegisterHandler(_OnAfterCreateWindow_2_, "OnAfterCreateWindow");

	// RegisterHandler(_HERO_Action_, "");
	
	/*
	RegisterHandler(_Town_Action_, "OnBeforeVisitObject 98/0");
	RegisterHandler(_Town_Action_, "OnBeforeVisitObject 98/1");
	RegisterHandler(_Town_Action_, "OnBeforeVisitObject 98/2");
	RegisterHandler(_Town_Action_, "OnBeforeVisitObject 98/3");
	RegisterHandler(_Town_Action_, "OnBeforeVisitObject 98/4");
	RegisterHandler(_Town_Action_, "OnBeforeVisitObject 98/5");
	RegisterHandler(_Town_Action_, "OnBeforeVisitObject 98/6");
	RegisterHandler(_Town_Action_, "OnBeforeVisitObject 98/7");
	RegisterHandler(_Town_Action_, "OnBeforeVisitObject 98/8");
	*/

	
	RegisterHandler(_Town_Action_, "OnAfterVisitObject 98/0");
	RegisterHandler(_Town_Action_, "OnAfterVisitObject 98/1");
	RegisterHandler(_Town_Action_, "OnAfterVisitObject 98/2");
	RegisterHandler(_Town_Action_, "OnAfterVisitObject 98/3");
	RegisterHandler(_Town_Action_, "OnAfterVisitObject 98/4");
	RegisterHandler(_Town_Action_, "OnAfterVisitObject 98/5");
	RegisterHandler(_Town_Action_, "OnAfterVisitObject 98/6");
	RegisterHandler(_Town_Action_, "OnAfterVisitObject 98/7");
	RegisterHandler(_Town_Action_, "OnAfterVisitObject 98/8");
	
	RegisterHandler(_Preset_AI_heroes_,"OnErmTimer 2");
	RegisterHandler(_Town_PreEnter_, "OnPreTownScreen");
	
	RegisterHandler(_Adventure_Blacksmith_Action_Before_ , "OnBeforeVisitObject 106/0");
	RegisterHandler(_Adventure_Blacksmith_Action_After_, "OnAfterVisitObject 106/0");
	

	//RegisterHandler(_HERO_Action_, "OnEquip");

	//RegisterHandler(_Artifact_Equip_, "OnEquipArt");
	//RegisterHandler(_Artifact_Unequip_, "OnUnequipArt");

	RegisterHandler(_HERO_Move_, "OnHeroMove");
	//RegisterHandler(_HERO_Action_, "OnOpenHeroScreen");
	//RegisterHandler(_Unknown_HERO_Action_, "OnBeforeHeroInteraction");
	//RegisterHandler(_Unknown_HERO_Action_, "OnAfterHeroInteraction");
	//RegisterHandler(_Unknown_HERO_Action_, "OnHeroesMeetScreenMouseClick");
	//RegisterHandler(_Unknown_HERO_Action_, "OnTownMouseClick");
	RegisterHandler(_Town_Click_, "OnTownMouseClick");
	//RegisterHandler(_HEROES_Interaction_, "OnAfterHeroInteraction");
	//RegisterHandler(_HEROES_Interaction_, "OnHeroesMeetScreenMouseClick");


	RegisterHandler(_HERO_open_, "OnOpenHeroScreen");
	RegisterHandler(_HERO_close_, "OnCloseHeroScreen");


	//RegisterHandler(_HERO_open_, "OnBeforeHeroInteraction");
	//RegisterHandler(_HERO_close_, "OnAfterHeroInteraction");


	//RegisterHandler(_OnBeforeHeroInteraction_, "OnBeforeHeroInteraction");
	RegisterHandler(_OnAfterHeroInteraction_, "OnAfterHeroInteraction");

	RegisterHandler(_Scenario_INIT_, "OnBeforeErmInstructions");
	RegisterHandler(_checkAmethystLinkage_, "OnBeforeErmInstructions");
	RegisterHandler(_checkEmeraldLinkage_, "OnBeforeErmInstructions");
	//RegisterHandler(_HERO_INIT_, "OnErmTimer 2");
	RegisterHandler(_DAILY_, "OnErmTimer 2");
	//RegisterHandler(_TM99_,  "OnErmTimer 99");
	//RegisterHandler(_HERO_inside_town_,"OnTownMouseClick");

	RegisterHandler(_Blacksmith_Dialog_, "OnCustomDialogEvent");

	RegisterHandler(StoreData, "OnSavegameWrite");
	RegisterHandler(RestoreData, "OnSavegameRead");
	RegisterHandler(_checkAmethystLinkage_, "OnSavegameRead");
	RegisterHandler(_checkEmeraldLinkage_, "OnSavegameRead");
	/*
	RegisterHandler(_ResetHeroVars_Both_sides_, "OnOpenHeroScreen");
	RegisterHandler(_ResetHeroVars_Both_sides_, "OnEveryDay");
	RegisterHandler(_ResetHeroVars_Both_sides_, "OnAfterBattleUniversal");
	*/
	
	/*
	RegisterHandler(_prepareCreatures_Both_sides_, "OnBeforeBattleUniversal");
	RegisterHandler(_PrepareHeroVars_Both_sides_, "OnBeforeBattle");
	RegisterHandler(_PrepareHeroVars_Both_sides_, "OnBeforeBattleForThisPcDefender");
	*/
	RegisterHandler(change_war_machine_description, "OnOpenHeroScreen");

	//majaczek_forge->WriteHiHook(0x4D9460, SPLICE_, EXTENDED_, THISCALL_, (void*)ForgeArtifact); // HeroHasArtifact
	majaczek_forge->WriteHiHook(0x4D9420, SPLICE_, EXTENDED_, THISCALL_, (void*)ForgeArtifact); // HeroHasArtifact
	majaczek_forge->WriteHiHook(0x524280, SPLICE_, EXTENDED_, THISCALL_, (void*)BattleStack_GetGexID_OnAttackType_Hook); //
	majaczek_forge->WriteHiHook(0x4B3160, SPLICE_, EXTENDED_, THISCALL_, (void*)AIMgr_Stack_SetHexes_WayToMoveLength_Hook);

	majaczek_forge->WriteLoHook(0x00463833, hook_change_machines_creatures);
	//RegisterHandler(try_heroes_init, "OnAdventureMapLeftMouseClick");

  }
  return TRUE;
};
