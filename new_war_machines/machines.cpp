#include <windows.h>
#include "era.h"

#include "heroes.h"

  

using namespace Era;

//extern void __stdcall _OnAfterCreateWindow_2_(TEvent* Event);

const int ADV_MAP   = 37;
const int CTRL_LMB  = 4;
const int LMB_PUSH  = 12;

//DWORD what_is_patched = 0x46359E;
//DWORD patched_value = 0;
//DWORD patched_before = 0x75E161;

DWORD what_is_patched = 0x75E167;

DWORD New_Machines_Begin = 1000 - 75 * 3;

DWORD ballista[3], ammo_cart[3], first_aid_tent[3];

extern Byte *GetMPos(Byte *Bm, int Side, int Pos);
extern Byte *PutStack(Byte *Bm, int Type, int Num, int Pos, int Side, HERO *hp, int Placed, int Stack);

#include "PlaceNPC2.h"


void Fun_get_machine_SN_vars(HERO* Hp, long Side) {

	int tmp2, tmp1;
	tmp2 = v[2]; 
	tmp1 = v[1];

	v[2] = Hp->Number;  

	
	ExecErmCmd("SN:W^HE%V2 Has Ballista^/?v1;");
	if (v[1]>0 && Hp->IArt[13][0] >=0 && Hp->IArt[13][0] != 144 && Hp->IArt[13][0] != 145) {
		ballista[Side] = v[1] + 925;
		Hp->IArt[13][0] = (ballista[Side]);
		//Hp->IArt[13][1] = (ballista[Side]);
	}
	else ballista[Side] = -1;


	ExecErmCmd("SN:W^HE%V2 Has Ammo Cart^/?v1;");
	if (v[1]>0 && Hp->IArt[14][0] >= 0 && Hp->IArt[14][0] != 144 && Hp->IArt[14][0] != 145) {
		ammo_cart[Side] = v[1] + 850;
		Hp->IArt[14][0] = (ammo_cart[Side]);
		//Hp->IArt[14][1] = (ammo_cart[Side]);
	}
	else ammo_cart[Side] = -1;

		
	ExecErmCmd("SN:W^HE%V2 Has First Aid Tent^/?v1;");	
	if (v[1]>0 && Hp->IArt[15][0] >= 0 && Hp->IArt[15][0] != 144 && Hp->IArt[15][0] != 145) {
		first_aid_tent[Side] = v[1] + 775;
		Hp->IArt[15][0] = (first_aid_tent[Side]);
		//Hp->IArt[15][1] = (first_aid_tent[Side]);
	}
	else first_aid_tent[Side] = -1;


	v[2] = tmp2 ;
	v[1] = tmp1;
}


void Fun_set_machine_SN_vars(HERO* Hp, long Side) {
	
	int tmp2, tmp1;
	tmp2 = v[2];
	tmp1 = v[1];

	(ballista[Side]) = Hp->IArt[13][0];
	(ammo_cart[Side]) = Hp->IArt[14][0];
	(first_aid_tent[Side]) = Hp->IArt[15][0];	
	
	if (ballista[Side] == 4 || ballista[Side] == 925) {
		ballista[Side] = 926;
		v[2] = Hp->Number;  
		ExecErmCmd("SN:W^HE%V2 Has Ballista^/1;");
	}
	if (ammo_cart[Side] == 5 || ammo_cart[Side] == 850) {
		ammo_cart[Side] = 851; 
		v[2] = Hp->Number;
		ExecErmCmd("SN:W^HE%V2 Has Ammo Cart^/1;");

	}
	if (first_aid_tent[Side] == 6 || first_aid_tent[Side] == 775) {
		first_aid_tent[Side] = 776;
		v[2] = Hp->Number;
		ExecErmCmd("SN:W^HE%V2 Has First Aid Tent^/1;");

	}

	if (ballista[Side] < 1000 && ballista[Side]>925) {
		v[1] = ballista[Side] - 925;
		v[2] = Hp->Number;  ExecErmCmd("SN:W^HE%V2 Has Ballista^/v1;");
	} else ExecErmCmd("SN:W^HE%V2 Has Ballista^/0;");
	if (ammo_cart[Side]<925 && ammo_cart[Side]>850) {
		v[1] = ammo_cart[Side] - 850;
		v[2] = Hp->Number;	ExecErmCmd("SN:W^HE%V2 Has Ammo Cart^/v1;");
	} else ExecErmCmd("SN:W^HE%V2 Has Ammo Cart^/0;");
	if (first_aid_tent[Side]<850 && first_aid_tent[Side]>775) {
		v[1] = first_aid_tent[Side] - 775;
		v[2] = Hp->Number;	ExecErmCmd("SN:W^HE%V2 Has First Aid Tent^/v1;");
	} else ExecErmCmd("SN:W^HE%V2 Has First Aid Tent^/0;");


	v[2] = tmp2;
	v[1] = tmp1;

}

BOOL __stdcall Hook_BattleMachines_BeforeNPC(THookContext* Context)
{
	//DWORD ballista, ammo_cart, first_aid_tent;
	
	Byte* Bm; int Side; int zPlaced;
	Bm = (char*) Context->EBX;
	Side = Context->EDI;
	
	//zPlaced = Context->ESI;
	//int *Placed = &zPlaced;
	int *Placed = &(Context->ESI);

	//(*Placed)++;

	HERO *Hp; Hp = (HERO *)*(Dword *)&Bm[0x53CC + Side * 4];
	if (Hp == nullptr) {
		ballista[Side] = 0;
		ammo_cart[Side] = 0;
		first_aid_tent[Side] = 0;

		return EXEC_DEF_CODE;
	}

	Fun_set_machine_SN_vars(Hp, Side);

	Byte* mon; int i, pos, num, tp, is_placed;




	if (/*ballista[Side] == 4 ||*/
		ballista[Side] >= New_Machines_Begin) {
		is_placed = 0; num = 1; 
		/*
		for (i = 0;i<21;i++) {
			mon = &Bm[0x54CC + 0x548 * (i + Side * 21)];
			tp = *(int *)&mon[0x34];
			if (tp == 146) { 
				// // *(int *)&mon[0x60] += num; 
				// // *(int *)&mon[0x4C] += num; 
				is_placed = 1; break; }
		}
		*/
		if (is_placed == 0) {
			if (Side == 0) pos = 51; else pos = 67; // 3.57m2
			PutStack(Bm, 146, num, pos, Side, Hp, *Placed, -1); ++*Placed;
		}
	}

	if (/* ammo_cart[Side] == 5 || */
		ammo_cart[Side] >= New_Machines_Begin) {
		is_placed = 0; num = 1;
		/*
		for (i = 0;i<21;i++) {
			mon = &Bm[0x54CC + 0x548 * (i + Side * 21)];
			tp = *(int *)&mon[0x34];
			if (tp == 148) {
				// // *(int *)&mon[0x60] += num; 
				// // *(int *)&mon[0x4C] += num; 
				is_placed = 1; break;
			}
		}
		*/
		if (is_placed == 0) {
			if (Side == 0) pos = 17; else pos = 33; // 3.57m2
			PutStack(Bm, 148, num, pos, Side, Hp, *Placed, -1); ++*Placed;
		}

	}

	if (/*first_aid_tent[Side] == 6 ||*/
		first_aid_tent[Side] >= New_Machines_Begin) {
		is_placed = 0; num = 1;
		/*
		for (i = 0;i<21;i++) {
			mon = &Bm[0x54CC + 0x548 * (i + Side * 21)];
			tp = *(int *)&mon[0x34];
			if (tp == 147) { 
				// // *(int *)&mon[0x60] += num; 
				// // *(int *)&mon[0x4C] += num; 
				is_placed = 1; break; }
		}
		*/
		if (is_placed == 0) {
			if (Side == 0) pos = 153; else pos = 169; // 3.57m2
			PutStack(Bm, 147, num, pos, Side, Hp, *Placed, -1); ++*Placed;
		}

	}

	//ExecErmCmd("IF:L^{~gold}This is a battle hint!{~}^;");
	//ExecErmCmd("FU");
	return EXEC_DEF_CODE;
}

void __stdcall _OnAfterCreateWindow_ (TEvent* Event)
{
	ApiHook((void*)Hook_BattleMachines_BeforeNPC,
		HOOKTYPE_BRIDGE, (void*)what_is_patched);
	ApiHook((void*)__PlaceNPC2__HOOK2,
		HOOKTYPE_BRIDGE, (void*)PlaceNPC2_what_is_patched);
	ApiHook((void*)__PlaceNPC2__HOOK3,
		HOOKTYPE_BRIDGE, (void*)PlaceNPC2_middle_2);

}

void __stdcall  _Artifact_Changed_(TEvent* e) {
	int tmp4 = v[4];
	
	ExecErmCmd("HE-1:N?v4;");
	HERO* Hp = (HERO*) GetHeroRecord ( (int) v[4]);
	Fun_set_machine_SN_vars(Hp, 2); 
	Fun_get_machine_SN_vars(Hp, 2);
	
	v[4] = tmp4;
}

void __stdcall  _HERO_Action_(TEvent* e) {
	int tmp4 = v[4];

	ExecErmCmd("HE-1:N?v4;");
	HERO* Hp = (HERO*) GetHeroRecord ( (int) v[4]);
	Fun_get_machine_SN_vars(Hp, 2);
	Fun_set_machine_SN_vars(Hp, 2);
	Fun_get_machine_SN_vars(Hp, 2);

	v[4] = tmp4;
}

void __stdcall  _Unknown_HERO_Action_(TEvent* e) {
	

	for (int i = 0;i <= 155;++i) {
		HERO* Hp = (HERO*)GetHeroRecord(i);
		Fun_get_machine_SN_vars(Hp, 2);
		Fun_set_machine_SN_vars(Hp, 2);
		Fun_get_machine_SN_vars(Hp, 2);
	}
	
}

void __stdcall  _HEROES_Interaction_(TEvent* e) {


	for (int i = 0;i <= 155;++i) {
		HERO* Hp = (HERO*)GetHeroRecord(i);
		Fun_set_machine_SN_vars(Hp, 2);
		Fun_get_machine_SN_vars(Hp, 2);
	}

}


bool heroes_initialized = false;
void __stdcall _Scenario_INIT_(TEvent* e) {
	heroes_initialized = false;
}

void __stdcall _HERO_INIT_(TEvent* e) {
	if (heroes_initialized) return;
	for (long i = 0; i <= 155; i++) {
		HERO* Hp = (HERO*)GetHeroRecord(i);
		Fun_set_machine_SN_vars(Hp, 2);
	}
	heroes_initialized = true;
}

void __stdcall _HERO_inside_town_(TEvent* e) {
	_HERO_Action_(e);
}

extern "C" __declspec(dllexport) BOOL APIENTRY DllMain (HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
  if (reason == DLL_PROCESS_ATTACH)
  {
    ConnectEra();
    //RegisterHandler(OnAdventureMapLeftMouseClick, "OnAdventureMapLeftMouseClick");
    //ApiHook((void*) Hook_BattleMouseHint, HOOKTYPE_BRIDGE, (void*) 0x74fd1e);

	RegisterHandler(_OnAfterCreateWindow_, "OnAfterCreateWindow");
	//RegisterHandler(_OnAfterCreateWindow_2_, "OnAfterCreateWindow");

	// RegisterHandler(_HERO_Action_, "");
	RegisterHandler(_HERO_Action_, "OnBeforeVisitObject 98/0");
	RegisterHandler(_HERO_Action_, "OnBeforeVisitObject 98/1");
	RegisterHandler(_HERO_Action_, "OnBeforeVisitObject 98/2");
	RegisterHandler(_HERO_Action_, "OnBeforeVisitObject 98/3");
	RegisterHandler(_HERO_Action_, "OnBeforeVisitObject 98/4");
	RegisterHandler(_HERO_Action_, "OnBeforeVisitObject 98/5");
	RegisterHandler(_HERO_Action_, "OnBeforeVisitObject 98/6");
	RegisterHandler(_HERO_Action_, "OnBeforeVisitObject 98/7");
	RegisterHandler(_HERO_Action_, "OnBeforeVisitObject 98/8");


	RegisterHandler(_HERO_Action_, "OnAfterVisitObject 98/0");
	RegisterHandler(_HERO_Action_, "OnAfterVisitObject 98/1");
	RegisterHandler(_HERO_Action_, "OnAfterVisitObject 98/2");
	RegisterHandler(_HERO_Action_, "OnAfterVisitObject 98/3");
	RegisterHandler(_HERO_Action_, "OnAfterVisitObject 98/4");
	RegisterHandler(_HERO_Action_, "OnAfterVisitObject 98/5");
	RegisterHandler(_HERO_Action_, "OnAfterVisitObject 98/6");
	RegisterHandler(_HERO_Action_, "OnAfterVisitObject 98/7");
	RegisterHandler(_HERO_Action_, "OnAfterVisitObject 98/8");



	RegisterHandler(_HERO_Action_, "OnBeforeVisitObject 106/0");
	RegisterHandler(_HERO_Action_, "OnAfterVisitObject 106/0");

	RegisterHandler(_HERO_Action_, "OnEquip");

	RegisterHandler(_Artifact_Changed_, "OnUnequip");

	RegisterHandler(_HERO_Action_, "OnHeroMove");
	//RegisterHandler(_HERO_Action_, "OnOpenHeroScreen");
	//RegisterHandler(_Unknown_HERO_Action_, "OnBeforeHeroInteraction");
	//RegisterHandler(_Unknown_HERO_Action_, "OnAfterHeroInteraction");
	//RegisterHandler(_Unknown_HERO_Action_, "OnHeroesMeetScreenMouseClick");
	RegisterHandler(_Unknown_HERO_Action_, "OnTownMouseClick");
	//RegisterHandler(_HEROES_Interaction_, "OnAfterHeroInteraction");
	//RegisterHandler(_HEROES_Interaction_, "OnHeroesMeetScreenMouseClick");

	RegisterHandler(_Scenario_INIT_, "OnBeforeErmInstructions");
	//RegisterHandler(_HERO_INIT_, "OnErmTimer 2");
	//RegisterHandler(_HERO_inside_town_,"OnTownMouseClick");
  }
  return TRUE;
};
