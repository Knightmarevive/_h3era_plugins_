// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "patcher_x86_commented.hpp"


Patcher* globalPatcher;
PatcherInstance* Z_Speed4Diozia;
long BasicSpeed[] = {
    1300,1360,1430,1500,1560,1630,1700,1760,1830,1900,1960,2000,2060,2100,
    2160,2200,2260,2300,2360,2400,2460,2500,2560,2600,2660,2700,2760,2800,
    2860,2900,2960,3000,3060,3100,3160,3200,3260,3300,3360,3400,3460,3500,
    3560,3600,3660,3700,3760,3800,3860,3900,3960,4000,4060,4100,4160,4200
        
};
bool reverse = false; long percentage = 100;

// you can call this from ERM or edit the calling of the function
extern "C" __declspec(dllexport) void do_patch(int percentage_new, bool reverse_new) {

    for (int i = 0; i <= 55; ++i) {
        BasicSpeed[i] *= 100;
        BasicSpeed[i] /= percentage;

        BasicSpeed[i] *= percentage_new;
        BasicSpeed[i] /= 100;
    }

    if (reverse_new) {
        Z_Speed4Diozia->WriteDword(0x004E4E28 + 3, 0);
        Z_Speed4Diozia->WriteByte(0x004E4EB3, 0x7E);
    }
    else
    {
        Z_Speed4Diozia->WriteDword(0x004E4E28 + 3, 55);
        Z_Speed4Diozia->WriteByte(0x004E4EB3, 0x7D);
    }
    percentage = percentage_new; reverse = reverse_new;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {

        globalPatcher = GetPatcher();
        Z_Speed4Diozia = globalPatcher->CreateInstance("Z_Speed4Diozia");

        Z_Speed4Diozia->WriteDword(0x004E4ED9 + 3, (int) BasicSpeed);
        Z_Speed4Diozia->WriteDword(0x0042CA57 + 3, (int)BasicSpeed);
        Z_Speed4Diozia->WriteDword(0x0042CA5E + 3, (int)BasicSpeed);

        // // should be left as is
        // Z_Speed4Diozia->WriteDword(0x004D75D9 + 1, (int)BasicSpeed);
       

        // edit this line to do different effect;
        // do_patch(150, false);


        break;
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

