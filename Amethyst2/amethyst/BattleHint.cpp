// #include "patcher_x86_commented.hpp"
#define _H3API_PATCHER_X86_
#define _H3API_PLUGINS_
#include "../../__include__/H3API/single_header/H3API.hpp"
#include <cstdio>
#include "stdafx.h"

extern PatcherInstance* Z_Amethyst;
extern DWORD  Spells_Table[MONSTERS_AMOUNT + 64];

extern char spell_1_table[MONSTERS_AMOUNT];
extern char spell_2_table[MONSTERS_AMOUNT];
extern char spell_3_table[MONSTERS_AMOUNT];

extern DWORD SpellsLegacy[42];

int damMin, damMax, MouseTypeCursor;
h3::H3CombatCreature* dst = nullptr;
// h3::H3CombatCreature* src = nullptr;

char HintTranslate[64][512] = {};
inline char* XLAT(int id, char* default) {
	return (HintTranslate[id][0]) ? (HintTranslate[id]) : default;
}

_LHF_(hook_00493058) {
	damMin = c->edi;
	damMax = c->esi;

	// src = (h3::H3CombatCreature*) c->edx;
	// dst = (h3::H3CombatCreature*)*(int*)(c->ebp + 8);

	return EXEC_DEFAULT;
}

/*
_LHF_(hook_004923E7) {
	MouseTypeCursor = *(int*)(c->ebp + 8);
	return EXEC_DEFAULT;
}
*/

_LHF_(hook_0049245E) {
	if (c->eax < 0 || c->eax>22)
		return EXEC_DEFAULT;

	MouseTypeCursor = *(int*)(c->ebp + 8);
	dst = (h3::H3CombatCreature*)c->esi;
	if (!dst) {
		// int unk_side = c->ecx; 
		// h3::H3CombatCreature* st_A = (h3::H3CombatCreature*) c->ebx;
		auto batman = h3::H3CombatManager::Get();
		// if(unk_side >= 0) {	}
		int coord = batman->mouseCoord;
		if (coord< 0 || coord>0xBA)
			return EXEC_DEFAULT;
		dst = batman->squares[coord].GetCreature();
		//dst = 0; 
	}

	return EXEC_DEFAULT;
}

void get_spell_name(char* name_out, h3::H3CombatCreature* src) {
	int combat_id = src->sideIndex + (src->side ? 21 : 0);
	if (SpellsLegacy[combat_id]) {
		// src->faerieDragonSpell = SpellsLegacy[combat_id]; // to check
		strcpy(name_out, h3::H3Spell::Get()[SpellsLegacy[combat_id]].name);
		return;
	}

	char spl1 = spell_1_table[src->type];
	char spl2 = spell_2_table[src->type];
	if ((spl1 >= 11 && spl1 <=15) || spl1 == 17 || spl1 == 18 || spl1 == 20 || spl1 == 21) {
		switch (spl1) {
		case 11:
			strcpy(name_out, XLAT(0, "Summon Random Elemental"));
			break;
		case 12:
			strcpy(name_out, XLAT(1, "Enchant(+2)"));
			break;
		case 13:
			strcpy(name_out, XLAT(2, "Enchant(+1)"));
			break;
		case 14:
			strcpy(name_out, XLAT(3, "Enchant(+3)"));
			break;
		case 15:
			strcpy(name_out, XLAT(4, "Inspiration"));
			break;
		case 17:
			strcpy(name_out, XLAT(5, "Rush"));
			break;
		case 18:
			strcpy(name_out, XLAT(6, "Dragon Will"));
			break;
		case 20:
			strcpy(name_out, XLAT(7, "Overclock"));
			break;
		case 21:
			strcpy(name_out, XLAT(8, "Repair and Overclock"));
			break;
		}
	}
	else if (spl2==0 || spl2 == 9 || spl2 == 10 || spl2 == 11) {
		switch (spl2) {
		case 0:
			strcpy(name_out, XLAT(9, "Ressurrection"));
			break;
		case 9:
			strcpy(name_out, XLAT(10, "Raise Undead"));
			break;
		case 10:
			strcpy(name_out, XLAT(11, "Repair"));
			break;
		case 11:
			strcpy(name_out, XLAT(12, "Gating"));
			break;
		}
	}
	else if (Spells_Table[src->type] > 0) {
		strcpy(name_out, h3::H3Spell::Get()[Spells_Table[src->type]].name);
	}
	else *name_out = 0;
}

void BattleHint() {
	auto batman = h3::H3CombatManager::Get();
	char* MM_txt = (char*)0x697428;
	// auto &a = batman->action;
	// if (a <= 0 || a >= 12) return;
	auto &s = batman->stacks;
	auto src = &s[batman->currentMonSide][batman->currentMonIndex];
	if (!src || src->type < 0 || src->numberAlive <=0) return;
	// h3::H3CombatCreature* dst = nullptr;
	/*
	if (batman->actionTarget >= 0 && batman->actionTarget <= 0xBA) {
		dst = batman->squares[batman->actionTarget].GetCreature();
	}
	*/
	auto cur = MouseTypeCursor;

	if (dst && (
		(cur == 3 || cur == 15) || //shoot
		(cur >=7 && cur <=14) // melee
		) ){
		int& dh = dst->baseHP;
		int& dl = dst->healthLost;
		int& dn = dst->numberAlive;
		int& dt = dst->type;

		int last_health = dh - dl;
		int min_killed = damMin / dh;
		int dmg_top_1 = damMin % dh;
		if (dmg_top_1 >= last_health) ++min_killed;
		int max_killed = damMax / dh;
		int dmg_top_2 = damMax % dh;
		if (dmg_top_2 >= last_health) ++max_killed;
		int total_hp = dh * dn - dl;
		if (min_killed > dn) min_killed = dn;
		if (max_killed > dn) max_killed = dn;

		char att_name[512]; char def_name[512];
		if (src->numberAlive == 1)
			sprintf(att_name, XLAT(13, "The %s"),src->GetCreatureName());
		else
			sprintf(att_name, "%d %s", src->numberAlive, src->GetCreatureName());
		
		if (dst->numberAlive == 1)
			sprintf(def_name, XLAT(14, "the %s"), dst->GetCreatureName());
		else
			sprintf(def_name, "%d %s", dst->numberAlive, dst->GetCreatureName());

		char dmg_name[512];
		if (damMax == damMin)
			sprintf(dmg_name, XLAT(15, " will do %d damage"),damMin);
		else
			sprintf(dmg_name, XLAT(16, " will do %d-%d damage"), damMin, damMax);
		
		char kill_name[512];
		if (min_killed >= dn) {
			if (dn == 1)
				sprintf(kill_name, XLAT(17, "and %s will be {~Yellow}DESTROYED{~}"), def_name);
			else
				sprintf(kill_name, XLAT(18, "and all {~Red}%d{~} %s will be {~Yellow}DESTROYED{~}"),dn , dst->GetCreatureName());
		}
		else
		{
			if (max_killed == 0)
				sprintf(kill_name, XLAT(19, " to %s"), def_name);
			else if (max_killed == min_killed)
				sprintf(kill_name, XLAT(20, " and will kill {~Red}%d{~} of %s"), min_killed, def_name);
			else
				sprintf(kill_name, XLAT(21, " and will kill {~Red}%d-%d{~} of %s"), min_killed, max_killed, def_name);
		}
		sprintf(MM_txt, "%s%s%s", att_name, dmg_name, kill_name);
		return;
		// int dmg = dst->
	}
	else if (cur == 20) {
		char src_name[512]; char dst_name[512]; char spl_name[512];
		if (src->numberAlive == 1)
			sprintf(src_name, XLAT(22, "The %s"), src->GetCreatureName());
		else
			sprintf(src_name, XLAT(23, "%d %s"), src->numberAlive, src->GetCreatureName());

		if (dst == nullptr)
			strcpy(dst_name, XLAT(24, "the area"));
		else if (dst->numberAlive == 1)
			sprintf(dst_name, XLAT(25, "the %s"), dst->GetCreatureName());
		else
			sprintf(dst_name, "%d %s", dst->numberAlive, dst->GetCreatureName());

		/*
		if (Spells_Table[src->type] > 0) {
			strcpy(spl_name, h3::H3Spell::Get()[Spells_Table[src->type]].name);
		}
		*/
		get_spell_name(spl_name, src);

		sprintf(MM_txt, XLAT(26, "%s will cast %s on %s (%d casts left)"),src_name,spl_name,dst_name,src->info.spellCharges);
	}
	else if (cur == 0 && dst) {

		char src_name[512]; char dst_name[512]; char spl_name[512];
		if (dst->numberAlive == 1)
			sprintf(dst_name, XLAT(27, "The %s is"), dst->GetCreatureName());
		else
			sprintf(dst_name, XLAT(28, "%d %s are"), dst->numberAlive, dst->GetCreatureName());

		get_spell_name(spl_name, src);
		if (*spl_name == 0) return;

		sprintf(MM_txt, XLAT(29, "%s unaffected by %s (%d casts left)"), dst_name, spl_name, src->info.spellCharges);
	}

	// sprintf(MM_txt, "test (failed)");
}

_LHF_(hook_0074FCDC) {
	BattleHint();
	return EXEC_DEFAULT;
}


_LHF_(hook_0074FD14) {
	BattleHint();
	return EXEC_DEFAULT;
}


void BattleHint_Apply() {


	// Z_Amethyst->WriteLoHook(0x004923E7, hook_004923E7);
	Z_Amethyst->WriteLoHook(0x0049245E, hook_0049245E);

	Z_Amethyst->WriteLoHook(0x00493058, hook_00493058);
	// Z_Amethyst->WriteLoHook(0x00492E4F, hook_00492E4F);
	
	// Z_Amethyst->WriteLoHook(0x0074FCDC, hook_0074FCDC);
	Z_Amethyst->WriteLoHook(0x0074FD14, hook_0074FD14);
}