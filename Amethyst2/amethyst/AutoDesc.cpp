#include "stdafx.h"

#include<cstdio>
// typedef long DWORD;



extern char HintTranslate[64][512];
inline char* XLAT(int id, char* default) {
	return (HintTranslate[id][0]) ? (HintTranslate[id]) : default;
}


extern char OverrideSpecialSpells[MONSTERS_AMOUNT + 64];
extern char StunChanceMelee[MONSTERS_AMOUNT + 64];
extern char StunChanceShot[MONSTERS_AMOUNT + 64];

//std::vector<int> StackSteps_Taken;
extern char StackStep_field_type[MONSTERS_AMOUNT + 64];

extern char strike_all_around_range[MONSTERS_AMOUNT + 64];
extern char counterstrike_fury[MONSTERS_AMOUNT + 64];
extern char counterstrike_twice[MONSTERS_AMOUNT + 64];

// DWORD demonolgy_Temp1;

extern DWORD GhostMultiplyer_028460f8[MONSTERS_AMOUNT + 64];

extern DWORD CreatureSpellPowerMultiplier[MONSTERS_AMOUNT + 64];
extern DWORD CreatureSpellPowerDivider[MONSTERS_AMOUNT + 64];
extern DWORD CreatureSpellPowerAdder[MONSTERS_AMOUNT + 64];
extern char CreatureSpellPowerScaled[MONSTERS_AMOUNT + 64];

extern DWORD CreatureMimicArtifact[MONSTERS_AMOUNT + 64];
extern DWORD CreatureMimicArtifact2[MONSTERS_AMOUNT + 64];

extern char isCommander[MONSTERS_AMOUNT + 64];

///// MOP Area Begin
extern DWORD RegenerationHitPoints_Table[MONSTERS_AMOUNT + 64];
extern char  RegenerationChance_Table[MONSTERS_AMOUNT + 64];
extern WORD  ManaDrain_Table[MONSTERS_AMOUNT + 64];
extern char  SpellsCostDump_Table[MONSTERS_AMOUNT + 64];
extern char  SpellsCostLess_Table[MONSTERS_AMOUNT + 64];
extern char  Counterstrike_Table[MONSTERS_AMOUNT + 64];
extern char  AlwaysPositiveMorale_Table[MONSTERS_AMOUNT + 64];
extern char  AlwaysPositiveLuck_Table[MONSTERS_AMOUNT + 64];
extern char  ThreeHeadedAttack_Table[MONSTERS_AMOUNT + 64];

extern char  DeathBlow_Table[MONSTERS_AMOUNT + 64];
extern char  Fear_Table[MONSTERS_AMOUNT + 64];
extern char  Fearless_Table[MONSTERS_AMOUNT + 64];
extern char  NoWallPenalty_Table[MONSTERS_AMOUNT + 64];
extern char  MagicAura_Table[MONSTERS_AMOUNT + 64];
extern char  StrikeAndReturn_Table[MONSTERS_AMOUNT + 64];
extern DWORD  Spells_Table[MONSTERS_AMOUNT + 64];
extern char  Hate_Table[MONSTERS_AMOUNT + 64];
extern char  JoustingBonus_Table[MONSTERS_AMOUNT + 64];
extern char  ImmunToJoustingBonus_Table[MONSTERS_AMOUNT + 64];
extern char  MagicChannel_Table[MONSTERS_AMOUNT + 64];
extern char  MagicMirror_Table[MONSTERS_AMOUNT + 64];
extern char  Sharpshooters_Table[MONSTERS_AMOUNT + 64];
extern char  ShootingAdjacent_Table[MONSTERS_AMOUNT + 64];
extern char  ReduceTargetDefense_Table[MONSTERS_AMOUNT + 64];
extern Word  Demonology_Table[MONSTERS_AMOUNT + 64];
extern char  ImposedSpells_Table[MONSTERS_AMOUNT + 64][6];
// extern char* ImposedSpells_ptr = (char*)ImposedSpells_Table;

extern char PreventiveCounterstrikeTable[MONSTERS_AMOUNT + 64];
extern char RangeRetaliation_Table[MONSTERS_AMOUNT + 64];
///// MOP Area End

extern DWORD PersonalHate[MONSTERS_AMOUNT + 64][MONSTERS_AMOUNT];
extern char isDragonSlayer_Table[MONSTERS_AMOUNT + 64];
extern char isFaerieDragon_Table[MONSTERS_AMOUNT + 64];
extern char isAimedCaster_Table[MONSTERS_AMOUNT + 64];
extern char isAmmoCart_Table[MONSTERS_AMOUNT + 64];
extern char isPassive_Table[MONSTERS_AMOUNT + 64];
extern char isTeleporter[MONSTERS_AMOUNT + 64];

extern DWORD Necromancy_without_artifacts[16];
extern DWORD Necromancy_with_artifacts[16];

extern char hasSantaGuards[MONSTERS_AMOUNT + 64];
//DWORD SantaGuardsType[MONSTERS_AMOUNT + 64];
//DWORD UpgradedSantaGuardsType[MONSTERS_AMOUNT + 64];
extern DWORD DalionsGuards[MONSTERS_AMOUNT + 64];

extern float ghost_fraction[MONSTERS_AMOUNT + 64];
extern char isGhost[MONSTERS_AMOUNT + 64];
extern char isRogue[MONSTERS_AMOUNT + 64];

extern char isEnchanter[MONSTERS_AMOUNT + 64];
extern char isSorceress[MONSTERS_AMOUNT + 64];
extern char isSorceressMelee[MONSTERS_AMOUNT + 64];

extern char FireWall_Table[MONSTERS_AMOUNT + 64];
extern char isHellSteed[MONSTERS_AMOUNT + 64];
extern char isHellSteed2[MONSTERS_AMOUNT + 64];
extern char isHellSteed3[MONSTERS_AMOUNT + 64];

extern char NoGolemOverflow[MONSTERS_AMOUNT + 64];
extern char isDragonResistant[MONSTERS_AMOUNT + 64];

extern char isHellHydra[MONSTERS_AMOUNT + 64];
extern char MovesTwice_Table[MONSTERS_AMOUNT + 64];
extern char Receptive_Table[MONSTERS_AMOUNT + 64];
extern char isLord_Table[MONSTERS_AMOUNT + 64];

extern char isHeroic_Table[MONSTERS_AMOUNT + 64];


extern long poison_table[MONSTERS_AMOUNT + 64];
extern long aging_table[MONSTERS_AMOUNT + 64];
extern long paralyze_table[MONSTERS_AMOUNT + 64];
extern long paralyze_chance[MONSTERS_AMOUNT + 64];


extern long poison_table[MONSTERS_AMOUNT + 64];
extern long aging_table[MONSTERS_AMOUNT + 64];
extern long paralyze_table[MONSTERS_AMOUNT + 64];
extern long paralyze_chance[MONSTERS_AMOUNT + 64];

//=============================================
typedef enum
{
	ATT_VAMPIRE,
	ATT_THUNDER,
	ATT_DEATHSTARE,
	ATT_DISPEL,
	ATT_DISRUPT,
	ATT_DEFAULT
}
ATTACK_ABILITY;

extern char attack_abilities_table[MONSTERS_AMOUNT + 64];
//=============================================
typedef enum
{
	RESIST_DWARF20 = 0,
	RESIST_DWARF40,
	RESIST_123LVL,
	RESIST_1234LVL,
	RESIST_MAGICIMMUNE,
	RESIST_ASAIR,
	RESIST_ASEARTH,
	RESIST_ASFIRE,
	RESIST_DEFAULT,
	RESIST_DWARF60,
	RESIST_DWARF80,
	RESIST_DWARF100,
	RESIST_1LVL,
	RESIST_12LVL,
	RESIST_SPEED,
	RESIST_TOXIC,
	RESIST_WILL,
	RESIST_NO_EYES,
	RESIST_MASS_DAMAGE,
	RESIST_TO_DISPEL,
	RESIST_TO_DEBUFF

}
MAGIC_RESISTANCE;

extern char magic_resistance_table[MONSTERS_AMOUNT + 64];
//=============================================

typedef enum
{
	VULN_HALF = 0,
	VULN_QUATER,
	VULN_LIGHTING,
	VULN_SHOWER,
	VULN_ICE,
	VULN_FIRE,
	VULN_GOLD,
	VULN_DIAMOND,
	VULN_DEFAULT,
	VULN_GOLEM_125,
	VULN_GOLEM_150,
	VULN_GOLEM_200,
	VULN_GOLEM_300,
	VULN_GOLEM_400,
	VULN_GOLEM_500,
	VULN_GOLEM_600,
	VULN_GOLEM_700,
	VULN_GOLEM_900
}
MAGIC_VULNERABILITY;

extern char magic_vulnerability_table[MONSTERS_AMOUNT + 64];
//=============================================
extern char missiles_table[MONSTERS_AMOUNT + 64];
//=============================================



extern char spell_1_table[MONSTERS_AMOUNT]; 
extern char spell_2_table[MONSTERS_AMOUNT]; 
extern char spell_3_table[MONSTERS_AMOUNT]; 
//=============================================

extern char special_missiles_table[MONSTERS_AMOUNT + 64];
extern char missile_size_table[MONSTERS_AMOUNT + 64];
extern long missile_anim_table[MONSTERS_AMOUNT + 64];


extern float fire_shield_table[MONSTERS_AMOUNT + 64];
//extern float respawn_table[MONSTERS_AMOUNT + 64];
extern float respawn_table_chance[MONSTERS_AMOUNT + 64];
extern float respawn_table_fraction[MONSTERS_AMOUNT + 64];
extern float respawn_table_sure[MONSTERS_AMOUNT + 64];


extern char  resource_type_table[MONSTERS_AMOUNT];
extern DWORD resource_amount_table[MONSTERS_AMOUNT];
extern DWORD resource_tax_table[MONSTERS_AMOUNT];

extern char isConstruct_Table[MONSTERS_AMOUNT + 64];


extern char Monster_Ban_Table[MONSTERS_AMOUNT];

extern int  after_wound__spell[MONSTERS_AMOUNT + 64];
extern int  after_melee__spell[MONSTERS_AMOUNT + 64];
extern int  after_shoot__spell[MONSTERS_AMOUNT + 64];
extern int  after_defend_spell[MONSTERS_AMOUNT + 64];
extern char after_action_spell_mastery[MONSTERS_AMOUNT + 64];

extern int  after_wound__spell2[MONSTERS_AMOUNT + 64];
extern int  after_melee__spell2[MONSTERS_AMOUNT + 64];
extern int  after_shoot__spell2[MONSTERS_AMOUNT + 64];
extern int  after_defend_spell2[MONSTERS_AMOUNT + 64];

extern float shooting_resistance[MONSTERS_AMOUNT + 64];
extern float melee_resistance[MONSTERS_AMOUNT + 64];

extern float mana_regen_table[MONSTERS_AMOUNT + 64];

extern char aftercast_abilities_table[MONSTERS_AMOUNT + 64];

inline const char* spellname(int id) {
	return P_Spell[id].name;
}
inline const char* crename(int id) {
	return P_CreatureInformation[id].namePlural;
}
inline const char* artname(int id) {
	return P_ArtifactSetup[id].name;
}

inline const char* resname(int id) {
	switch (id) {
	case 0: return "Wood";
	case 1: return "Mercury";
	case 2: return "Ore";
	case 3: return "Sulfur";
	case 4: return "Crystal";
	case 5: return "Gems";
	case 6: return "Gold";
	case 7: return "Mithril";

	default:		return "unknown";
	}
}

inline const char* vuln(char id) {
	switch (id) {
	case VULN_HALF: return "50% golem resistance";
	case VULN_QUATER:return "75% golem resistance";
	case VULN_LIGHTING: return "Vulneable to lighting and firestorm";
	case VULN_SHOWER:return "Vulnerable to meteor shower";
	case VULN_ICE:return "Vulnerable to ice";
	case VULN_FIRE:return "Vulnerable to fire";
	case VULN_GOLD: return "85% golem resistance";
	case VULN_DIAMOND: return "90% golem resistance";
	case VULN_DEFAULT: return "";
	case VULN_GOLEM_125: return "125% golem resistance";
	case VULN_GOLEM_150:return "150% golem resistance";
	case VULN_GOLEM_200:return "200% golem resistance";
	case VULN_GOLEM_300:return "300% golem resistance";
	case VULN_GOLEM_400:return "400% golem resistance";
	case VULN_GOLEM_500:return "500% golem resistance";
	case VULN_GOLEM_600:return "600% golem resistance";
	case VULN_GOLEM_700:return "700% golem resistance";
	case VULN_GOLEM_900:return "900% golem resistance";
	default: return nullptr;
	}
}
inline const char* resist(char id) {
	switch (id) {
	case RESIST_DWARF20: return "20% dwarf resistance";
	case RESIST_DWARF40: return "40% dwarf resistance";
	case RESIST_123LVL:  return "Level 1-3 spell resistance";
	case RESIST_1234LVL: return "Level 1-4 spell resistance";
	case RESIST_MAGICIMMUNE: return "Imune to all magic";
	case RESIST_ASAIR: return "Immune to meteor shower";//"lighting and firestorm vulnerability";
	case RESIST_ASEARTH : return "Lighting and armageddon immunity"; //"meteor shower vulnerability";
	case RESIST_ASFIRE : return "Ice Immunity"; //"vulnerable to ice";
	case RESIST_DEFAULT: return "";
	case RESIST_DWARF60: return "60% dwarf resistance";
	case RESIST_DWARF80: return "80% dwarf resistance";
	case RESIST_DWARF100: return "100% dwarf resistance";
	case RESIST_1LVL:  return "Level 1 spell resistance";
	case RESIST_12LVL: return "Level 1-2 spell resistance";
	case RESIST_SPEED: return "Speed effect resistance";
	case RESIST_TOXIC: return "Toxicity resistance";
	case RESIST_WILL: return "Will resistance";
	case RESIST_NO_EYES:return "Vision resistance";
	case RESIST_MASS_DAMAGE: return "MASS_DAMAGE resistance";
	case RESIST_TO_DISPEL: return "Dispel resistance";
	case RESIST_TO_DEBUFF: return "Debuff resistance";
	default: return nullptr;
	}
}

inline const char* processFlags(int id) {
	static char buf[65536]; *buf = 0;
	auto flags = P_CreatureInformation[id].flags;
	if (flags & 1) strcat(buf, "Wide ");
	if (flags & 2) strcat(buf, "Fly ");
	if (flags & 4) strcat(buf, "Shooter ");
	if (flags & 8) strcat(buf, "LongAttack ");
	if (flags & 16) strcat(buf, "Alive ");
	if (flags & 32) strcat(buf, "Catapult ");
	if (flags & 64) strcat(buf, "SiegeWeapon ");
	if (flags & 1024) strcat(buf, "MindImmune ");
	if (flags & 2048) strcat(buf, "RayShooting ");
	if (flags & 4096) strcat(buf, "NoMeleePenalty ");
	if (flags & 16384) strcat(buf, "FireImmune ");

	if (flags & 32768) strcat(buf, "Attacks Twice ");
	if (flags & 65536) strcat(buf, "NoEnemyRetaliation ");
	if (flags & 131072) strcat(buf, "NeutralMorale ");

	if (flags & 262144) strcat(buf, "Undead ");
	if (flags & 524288) strcat(buf, "AtackAllAround ");

	if (flags & 0x80000000) strcat(buf, "DragonNature ");

	return buf;
}

inline const char* missle(int id) {
	switch (id) {
	case 0: return nullptr;
	case 1: return "Fireball ";
	case 2: return "DeathCloud ";
	case 3: return "ChainShot ";
	case 4: return "ChainShot(safe) ";
	case 5: return "InfernoShot ";
	case 6: return "InfernoShot(safe) ";
	default: return nullptr;
	}
}

inline const char* spell_special(int id)
{
	char spl1 = spell_1_table[id];
	char spl2 = spell_2_table[id];
	if (spl1 == 1 && spl2 == 1) return "Random Genie Spell";

	if ((spl1 >= 11 && spl1 <= 15) || spl1 == 17 || spl1 == 18 || spl1 == 20 || spl1 == 21) {
		switch (spl1) {
		case 11:
			return XLAT(0, "Summon Random Elemental");
			break;
		case 12:
			return XLAT(1, "Enchant(+2)");
			break;
		case 13:
			return XLAT(2, "Enchant(+1)");
			break;
		case 14:
			return XLAT(3, "Enchant(+3)");
			break;
		case 15:
			return XLAT(4, "Inspiration");
			break;
		case 17:
			return XLAT(5, "Rush");
			break;
		case 18:
			return XLAT(6, "Dragon Will");
			break;
		case 20:
			return XLAT(7, "Overclock");
			break;
		case 21:
			return XLAT(8, "Repair and Overclock");
			break;
		}
	}
	else if (spl2 == 0 || spl2 == 9 || spl2 == 10 || spl2 == 11) {
		switch (spl2) {
		case 0:
			return XLAT(9, "Ressurrection");
			break;
		case 9:
			return XLAT(10, "Raise Undead");
			break;
		case 10:
			return XLAT(11, "Repair");
			break;
		case 11:
			return XLAT(12, "Gating");
			break;
		}
	}
	else if (Spells_Table[id])
		return spellname(Spells_Table[id]);
	else return nullptr;
}

extern DWORD Creature_ACAST_CUSTOM_Spell[MONSTERS_AMOUNT + 64];
inline const char* Spell_effect(int mon_id) {
	switch (aftercast_abilities_table[mon_id])
	{
	case 0: return "Binds";
	case 1: return "Blinds";
	case 2: return "Diseases";
	case 3: return "Curses";
	case 4: return "Ages";
	case 5: return "Stones";
	case 6: return "Paralizes";
	case 7: return "Poizonous";
	case 8: return "Acidic1";
	case 10: return spellname(Creature_ACAST_CUSTOM_Spell[mon_id]); ;//"CustomCasts";
	default:
		return nullptr;
		break;
	}
}
inline const char* Attack_effect(int id) {
	switch (id)
	{
	case 0: return "BloodSucker";
	case 1: return "Thunderstorm";
	case 2: return "DeathStare";
	case 3: return "DiseplFriendlySpells";
	case 4: return "Acidic2";
	default:
		return nullptr;
		break;
	}
}

extern "C" __declspec(dllexport) bool writeCreatureDesc(char* ret, int id) {
	bool forced = false; char* out = ret;
	if (!out) return false;

	out += sprintf(out, "(%d) ", id);

	out += sprintf(out, "%s ", processFlags(id));


		// if (forced) out += sprintf(out, "ForceSaveAllCustomFields=1\n ");


		// if (do_not_generate) out += sprintf(out, "DoNotGenerate=%d ", (int)do_not_generate);
		/*
		for (int i = 0; i < 1024; ++i) if (PersonalHate[i])
			out += sprintf(out, "Hate%04d=%d ", i, PersonalHate[i]);
	
		if (Wog_Spell_Immunites[0]) {
			out += sprintf(out, "Wog_Spell_Immunites=\"%s\" ", Wog_Spell_Immunites);
		}
		*/

		if (isTeleporter[id]) out += sprintf(out, "Teleports " /*, (int)isTeleporter */);


		// if (Creature_ACAST_CUSTOM_Chance) out += sprintf(out, "ACAST_CUSTOM_Chance=%d ", Creature_ACAST_CUSTOM_Chance);
		// if (Creature_ACAST_CUSTOM_Spell) out += sprintf(out, "ACAST_CUSTOM_Spell=%d ", Creature_ACAST_CUSTOM_Spell);


		if (shooting_resistance[id])	out += sprintf(out, "%f ShootingResistance ", shooting_resistance[id]);
		if (melee_resistance[id])		out += sprintf(out, "%f MeleeResistance ", melee_resistance[id]);

		if (after_melee__spell[id]) out += sprintf(out, "AfterMelee casts %s ", spellname(after_melee__spell[id]));
		if (after_shoot__spell[id]) out += sprintf(out, "AfterShoot casts %s ", spellname(after_shoot__spell[id]));
		if (after_defend_spell[id]) out += sprintf(out, "AfterDefend casts %s ", spellname(after_defend_spell[id]));
		if (after_wound__spell[id]) out += sprintf(out, "AfterWound casts %s ", spellname(after_wound__spell[id]));
		// if (after_action_spell_mastery != 2) out += sprintf(out, "AfterActionSpellMastery=%d ", (int)after_action_spell_mastery);
		if (after_melee__spell2[id]) out += sprintf(out, "AfterMelee casts %s ", spellname(after_melee__spell2[id]));
		if (after_shoot__spell2[id]) out += sprintf(out, "AfterShoot casts %s ", spellname(after_shoot__spell2[id]));
		if (after_defend_spell2[id]) out += sprintf(out, "AfterDefend casts %s ", spellname(after_defend_spell2[id]));
		if (after_wound__spell2[id]) out += sprintf(out, "AfterWound casts %s ", spellname(after_wound__spell2[id]));



		if (ReduceTargetDefense_Table[id])	out += sprintf(out, "ReduceTargetDefense=%d ", ReduceTargetDefense_Table[id]);

		if (resource_tax_table[id])		out += sprintf(out, "Tax=%d ", resource_tax_table[id]);

		/*
		if (resource_type_table[id] < 8)  out += sprintf(out, "ResGen_Type=%d ", resource_type_table[id]);
		if (resource_amount_table[id])  out += sprintf(out, "ResGen_Amount=%d ", resource_amount_table[id]);
		*/
		if (resource_amount_table[id])  out += sprintf(out, "Generates %d %s ", resource_amount_table[id], resname(resource_type_table[id]));


		// if (new_monsters[id].flags)     out += sprintf(out, "Flags=%d ", new_monsters[id].flags);
		// out += sprintf(out, "Level=%d ", new_monsters.level);
		// if (new_monsters.town != -1)  out += sprintf(out, "Town=%d ", new_monsters.town);
		// if (upgtable >= 0)			out += sprintf(out, "UpgradesTo=%d ", upgtable);

		//if (aftercast_abilities_table[id] != 9) out += sprintf(out, "Spell effect=%d ", aftercast_abilities_table[id]);
		if (aftercast_abilities_table[id] != 9) out += sprintf(out, "%s ", Spell_effect(id));
		// if (attack_abilities_table[id] != 5) out += sprintf(out, "Attack effect=%d ", attack_abilities_table[id]);
		if (attack_abilities_table[id] != 5) out += sprintf(out, "%s ", Attack_effect(attack_abilities_table[id]));

		if (magic_resistance_table[id] != 8) out += sprintf(out, "%s ", resist(magic_resistance_table[id]));
		if (magic_vulnerability_table[id] != 8) out += sprintf(out, "%s ", vuln(magic_vulnerability_table[id]));

		/*
		if (spell_1_table[id] != 9) out += sprintf(out, "Spell 1=%d ", spell_1_table[id]);
		if (spell_2_table[id] != 8) out += sprintf(out, "Spell 2=%d ", spell_2_table[id]);
		if (spell_3_table[id] != 3) out += sprintf(out, "Spell 3=%d ", spell_3_table[id]);
		*/
		if(spell_special(id)) out += sprintf(out, "%s_x%d ", spell_special(id), P_CreatureInformation[id].spellCharges);

		/*
		if (CreatureSpellPowerMultiplier[id] != 1)	out += sprintf(out, "CreatureSpellPowerMultiplier=%d ", CreatureSpellPowerMultiplier[id]);
		if (CreatureSpellPowerDivider[id] != 1)	out += sprintf(out, "CreatureSpellPowerDivider=%d ", CreatureSpellPowerDivider[id]);
		if (CreatureSpellPowerAdder[id] != 0)	out += sprintf(out, "CreatureSpellPowerAdder=%d ", CreatureSpellPowerAdder[id]);
		*/

		/*
		if (ImposedSpells_Table[0])		out += sprintf(out, "ImposedSpell1Number=%d\nImposedSpell1Level=%d ", ImposedSpells_Table[0], ImposedSpells_Table[3]);
		if (ImposedSpells_Table[1])		out += sprintf(out, "ImposedSpell2Number=%d\nImposedSpell2Level=%d ", ImposedSpells_Table[1], ImposedSpells_Table[4]);
		if (ImposedSpells_Table[2])		out += sprintf(out, "ImposedSpell3Number=%d\nImposedSpell3Level=%d ", ImposedSpells_Table[2], ImposedSpells_Table[5]);
		*/

		/*
		if (RegenerationHitPoints_Table[id])	out += sprintf(out, "RegenerationHitPoints=%d ", RegenerationHitPoints_Table[id]);
		if (RegenerationChance_Table[id])		out += sprintf(out, "RegenerationChance=%d ", RegenerationChance_Table[id]);
		*/
		if (RegenerationChance_Table[id])		out += sprintf(out, "%d%% chance to regenerate %d HP ", RegenerationChance_Table[id], RegenerationHitPoints_Table[id]);

		if (ManaDrain_Table[id])				out += sprintf(out, "Drains %d mana ", ManaDrain_Table[id]);
		if (DeathBlow_Table[id])				out += sprintf(out, "%d chance DeathBlow ", DeathBlow_Table[id]);
		if (SpellsCostLess_Table[id])			out += sprintf(out, "SpellsCost %d Less ", SpellsCostLess_Table[id]);
		if (SpellsCostDump_Table[id])			out += sprintf(out, "MagicDumper=%d ", SpellsCostDump_Table[id]);
		if (Counterstrike_Table[id] != 1)		out += sprintf(out, "%d_Retaliations ", Counterstrike_Table[id]);
		// if (Spells_Table[id])					out += sprintf(out, "CastsSpell %s ", spellname(Spells_Table[id]));
		if (Demonology_Table[id])				out += sprintf(out, "Demonology(%s) ", crename(Demonology_Table[id]));

		if (RangeRetaliation_Table[id])		out += sprintf(out, "RangedRetaliation "/*, (int)RangeRetaliation_Table[id]*/);
		if (PreventiveCounterstrikeTable[id])	out += sprintf(out, "PreventiveStrike "/*, (int)PreventiveCounterstrikeTable[id]*/);

		if (Fear_Table[id])					out += sprintf(out, "CausesFear "/*, (int)Fear_Table[id]*/);
		if (Fearless_Table[id])				out += sprintf(out, "Fearless " /*, (int)Fearless_Table[id]*/);
		if (NoWallPenalty_Table[id])			out += sprintf(out, "NoWallPenalty "/*, (int)NoWallPenalty_Table[id]*/);
		if (Sharpshooters_Table[id])			out += sprintf(out, "Sharpshooters "/*, (int)Sharpshooters_Table[id] */ );
		if (ShootingAdjacent_Table[id])			out += sprintf(out, "ShootingAdjacent "/*, (int)ShootingAdjacent_Table[id] */ );
		if (StrikeAndReturn_Table[id])			out += sprintf(out, "StrikeAndReturn "/*, (int)StrikeAndReturn_Table[id]*/);
		if (JoustingBonus_Table[id])			out += sprintf(out, "JoustingBonus=%d ", (int)JoustingBonus_Table[id]);
		if (ImmunToJoustingBonus_Table[id])		out += sprintf(out, "ImmuneToJousting "/*, (int)ImmunToJoustingBonus_Table[id]*/);
		if (MagicAura_Table[id])				out += sprintf(out, "MagicAura=%d ", (int)MagicAura_Table[id]);
		if (MagicMirror_Table[id])				out += sprintf(out, "MagicMirror=%d ", (int)MagicMirror_Table[id]);
		if (MagicChannel_Table[id])				out += sprintf(out, "MagicChannel=%d ", (int)MagicChannel_Table[id]);


		if (isAimedCaster_Table[id])			out += sprintf(out, "AimedCaster " /*, (int)isAimedCaster_Table[id]*/);
		if (isPassive_Table[id])				out += sprintf(out, "Passive " /*, (int)isPassive_Table[id]*/);
		if (isAmmoCart_Table[id])				out += sprintf(out, "AmmoCart" /*, (int)isAmmoCart_Table[id]*/);
		if (isFaerieDragon_Table[id])			out += sprintf(out, "isFaerie " /*, (int)isFaerieDragon_Table[id]*/);
		if (isDragonSlayer_Table[id])			out += sprintf(out, "DragonSlayer "/*, (int)isDragonSlayer_Table[id]*/);
		/*
		if (hasSantaGuards[id])					out += sprintf(out, "hasSantaGuards=%d ", (int)hasSantaGuards[id]);
		if (DalionsGuards[id] >= 0)				out += sprintf(out, "DalionsGuards=%d ", DalionsGuards[id]);
		*/
		if (hasSantaGuards[id]) {
			out += sprintf(out, "hasGuards");
			if (DalionsGuards[id] >= 0) out += sprintf(out, "(%s)", crename(DalionsGuards[id]));
			out += sprintf(out, " ");
		}


		if (isRogue[id])       					out += sprintf(out, "Spies "/*, (int)isRogue[id]*/);
		if (isGhost[id])        				out += sprintf(out, "Ghostly "/*, (int)isGhost[id]*/);
		if (isEnchanter[id])    				out += sprintf(out, "Enchanter "/*, (int)isEnchanter[id]*/);
		if (isSorceress[id])    				out += sprintf(out, "SorceryShot "/*, (int)isSorceress[id]*/);

		/*
		if (isHellSteed)					out += sprintf(out, "isHellSteed=%d ", (int)isHellSteed);
		if (isHellSteed2)					out += sprintf(out, "isHellSteed2=%d ", (int)isHellSteed2);
		if (isHellSteed3)					out += sprintf(out, "isHellSteed3=%d ", (int)isHellSteed3);
		*/

		if (FireWall_Table[id])				out += sprintf(out, "hasFirewall " /*, (int)FireWall_Table[id]*/);
		if (AlwaysPositiveMorale_Table[id])	out += sprintf(out, "AlwaysPositiveMorale " /*, (int)AlwaysPositiveMorale_Table[id]*/);
		if (AlwaysPositiveLuck_Table[id])		out += sprintf(out, "AlwaysPositiveLuck " /*, (int)AlwaysPositiveLuck_Table[id]*/);
		if (ThreeHeadedAttack_Table[id])		out += sprintf(out, "ThreeHeadedAttack " /*, (int)ThreeHeadedAttack_Table[id]*/);

		// if (skeltrans != 56)			out += sprintf(out, "Sktransformer=%d ", skeltrans);
		/*
		if (special_missiles_table[id])	out += sprintf(out, "Shot type=%d ", special_missiles_table[id]);
		if (missile_size_table[id])		out += sprintf(out, "Shot size=%d ", missile_size_table[id]);
		if (missile_anim_table[id])		out += sprintf(out, "Shot anim=%d ", missile_anim_table[id]);
		*/
		if (special_missiles_table[id])	out += sprintf(out, "%s ", missle(special_missiles_table[id]));

		if (fire_shield_table[id] != 0.0) out += sprintf(out, "FireShield_%.2f%% ", 100 * fire_shield_table[id]);
		// if (respawn_table[id]     != 0.0) out += sprintf(out, "Self-resurrection=%f ", respawn_table[id]);

		/*
		if (respawn_table_chance[id] != 0.0)		out += sprintf(out, "rebirth chance=%f ", respawn_table_chance[id]);
		if (respawn_table_fraction[id] != 0.0)	out += sprintf(out, "rebirth fraction=%f ", respawn_table_fraction[id]);
		if (respawn_table_sure[id] != 0.0)		out += sprintf(out, "rebirth sure=%f ", respawn_table_sure[id]);
		*/
		if (respawn_table_chance[id] != 0.0
			|| respawn_table_fraction[id] != 0.0
			|| respawn_table_sure[id] != 0.0
			) out += sprintf(out, "rebirths ");


		if (CreatureMimicArtifact[id])	out += sprintf(out, "carries %s ", artname(CreatureMimicArtifact[id]));
		if (CreatureMimicArtifact2[id])	out += sprintf(out, "carries %s ", artname(CreatureMimicArtifact2[id]));

		if (mana_regen_table[id])			out += sprintf(out, "ManaRegen=%f ", mana_regen_table[id]);

		if (isConstruct_Table[id])		out += sprintf(out, "Construct "/*, isConstruct_Table[id]*/);
		if (MovesTwice_Table[id])			out += sprintf(out, "MovesTwice " /*, (int)MovesTwice_Table[id]*/);


		if (poison_table[id])			out += sprintf(out, "Poisonous " /*, poison_table[id]*/);
		if (aging_table[id])				out += sprintf(out, "Aging " /*, aging_table[id]*/);
		/*
		if (paralyze_table[id])			out += sprintf(out, "Paralyze=%d ", paralyze_table[id]);
		if (paralyze_chance[id])			out += sprintf(out, "ParalyzeChance=%d ", paralyze_chance[id]);
		*/
		if (paralyze_table[id] || paralyze_chance[id]) out += sprintf(out, "Paralyzes ");

		if (Receptive_Table[id])			out += sprintf(out, "Receptive " /*, (int)Receptive_Table[id]*/);
		// if (isHellHydra[id])				out += sprintf(out, "isHellHydra=%d ", (int)isHellHydra[id]);
		if (isLord_Table[id])				out += sprintf(out, "isLord " /*, (int)isLord_Table[id]*/);

		// if (ghost_fraction[id] != 1.0)	out += sprintf(out, "GhostFraction=%f ", ghost_fraction[id]);

		if (NoGolemOverflow[id])			out += sprintf(out, "NoGolemOverflow " /*, (int)NoGolemOverflow[id]*/);
		if (isDragonResistant[id])		out += sprintf(out, "isDragonResistant " /*, (int)isDragonResistant[id]*/);
		if (isHeroic_Table[id])					out += sprintf(out, "isHeroic " /*, (int)isHeroic_Table[id]*/);
		if (counterstrike_fury[id])		out += sprintf(out, "CounterstrikeFury " /*, (int)counterstrike_fury[id]*/);
		if (counterstrike_twice[id])		out += sprintf(out, "CounterstrikeTwice "/*, (int)counterstrike_twice[id]*/);

		// if (strike_all_around_range[id])	out += sprintf(out, "StrikeAllAroundRange=%d ", (int)strike_all_around_range[id]);
		// if (StackStep_field_type[id])		out += sprintf(out, "StackStepFieldType=%d ", (int)StackStep_field_type[id]);
		// if (OverrideSpecialSpells[id])	out += sprintf(out, "OverrideSpecialSpells=%d ", (int)OverrideSpecialSpells[id]);

		return true;
}