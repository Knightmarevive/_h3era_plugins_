#pragma once


#pragma warning(disable:4996)

#include"../../__include__/H3API/single_header/H3API.hpp"

struct armySlot {
    int type = -1;
    int count;
    int exp;
    char locked;
    char icon;
    char slot_id = -1;
    char hero_id = -1;
    inline bool accessible() {
        return(type >= 0 && count > 0 && !locked);
    }
    inline bool empty() {
        return (type < 0 && count <= 0 && !locked);
    }
    inline void kill() {
        type = -1; count = 0;
    }
};

inline armySlot* getBonusSlots(h3::H3Hero* hero) {
    static armySlot* (*getBonusSlots)(h3::H3Hero * hero) = nullptr;
    if (!getBonusSlots) {
        HMODULE slots = LoadLibraryA("Reserve_Slots.dll");
        getBonusSlots = (armySlot * (*)(h3::H3Hero * hero))
            GetProcAddress(slots, "getBonusSlots");
    }
    if (!getBonusSlots) return nullptr;
    return getBonusSlots(hero);
}